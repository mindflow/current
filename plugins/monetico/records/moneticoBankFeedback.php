<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';

//load test payment configuration if current_user is logged into the backend or if test mode is forced
//allows testing using simulated payments rather than real CB
if(isset($_SESSION['current_be_user']) || $GLOBALS['config']['plugins']['monetico']['forceTestPayments']==1){
    if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['test_config_file'];
}
else {
    if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['prod_config_file'];
}

require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/MoneticoPaiement_Ept.inc.php';

class moneticoBankFeedback extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_monetico_bank_feedback';
    static $oneTablePerLocale = true;

    //see document /monetico/ressources/doc/Monetico_Paiement_documentation_technique_v1_0e.pdf page 23 for field specification
    static $createTableSQL= "
        `MAC` varchar(40) ,
        `date` datetime NOT NULL,
        `tpe` int(11) unsigned NOT NULL DEFAULT '0',
        `montant` varchar(10) ,
        `reference` varchar(50) ,
        `texte-libre` varchar(255) ,
        `code-retour` varchar(20) ,
        `cvx` varchar(3) ,
        `vld` varchar(4) ,
        `brand` varchar(2) ,
        `status3ds` tinyint(4) NOT NULL DEFAULT '-1',
        `numauto` varchar(20) ,
        `motifrefus` varchar(20) ,
        `originecb` varchar(3) ,  #ISO 3166-1
        `bincb` varchar(128) ,
        `hpancb` varchar(64) , #verif 40 char
        `ipclient` varchar(128) ,
        `originetr` varchar(3) ,  #ISO 3166-1
        `veres` varchar(128) ,  #ISO 3166-1
        `pares` varchar(128) ,  #ISO 3166-1
        `montantech` varchar(10) ,
        `filtragecause` tinyint(4) unsigned NOT NULL DEFAULT '0',
        `filtragevaleur` varchar(128) ,
        `cbenregistree` tinyint(4) NOT NULL DEFAULT '-1',
        `cbmasquee` varchar(20) ,
        `modepaiement` varchar(10) ,

    ";

    static $createTableKeysSQL="
          KEY `k_mac` (`MAC`),
          KEY `k_montant` (`montant`),
          KEY `k_code` (`code-retour`),
          KEY `k_motifrefus` (`motifrefus`),
          KEY `k_status3ds` (`status3ds`),
          FULLTEXT KEY `vz_demande_search` (`reference`,`texte-libre`)
    ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init(){

        parent::init();

        global $mf,$l10n;

        $this->data = array_merge($this->data,array(
            'MAC' => array(
                'value' => '',
                'dataType' => 'hidden',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
                'editable' => false
            ),
            'date' => array(
                'value' => '',
                'dataType' => 'datetime',
                'valueType' => 'datetime',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),
            'tpe' => array(
                'value' => MONETICOPAIEMENT_EPTNUMBER,
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'montant' => array(
                'value' => '0.00',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'reference' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'texte-libre' => array(
                'value' => '',
                'dataType' => 'textarea 50 4',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'code-retour' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'payetest'    => $l10n->getLabel('moneticoBankFeedback','payetest'),
                    'paiement'    => $l10n->getLabel('moneticoBankFeedback','paiement'),
                    'Annulation'    => $l10n->getLabel('moneticoBankFeedback','Annulation'),
                    'paiement_pf2'    => $l10n->getLabel('moneticoBankFeedback','paiement_pf2'),
                    'paiement_pf3'    => $l10n->getLabel('moneticoBankFeedback','paiement_pf3'),
                    'paiement_pf4'    => $l10n->getLabel('moneticoBankFeedback','paiement_pf4'),
                    'Annulation_pf2'    => $l10n->getLabel('moneticoBankFeedback','Annulation_pf2'),
                    'Annulation_pf3'    => $l10n->getLabel('moneticoBankFeedback','Annulation_pf3'),
                    'Annulation_pf4'    => $l10n->getLabel('moneticoBankFeedback','Annulation_pf4'),
                ),
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'cvx' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'oui'    => $l10n->getLabel('moneticoBankFeedback','oui'),
                    'non'    => $l10n->getLabel('moneticoBankFeedback','non'),
                ),
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'vld' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'brand' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'AM'    => 'American Express',
                    'CB'    => 'GIE CB',
                    'MC'    => 'Mastercard',
                    'VI'    => 'Visa',
                    'na'     => 'Non disponible',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
            ),
            'status3ds' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '-1'    => $l10n->getLabel('moneticoBankFeedback','status3ds-1'),
                    '0'     => '',
                    '1'    => $l10n->getLabel('moneticoBankFeedback','status3ds1'),
                    '2'    => $l10n->getLabel('moneticoBankFeedback','status3ds2'),
                    '3'    => $l10n->getLabel('moneticoBankFeedback','status3ds3'),
                    '4'    => $l10n->getLabel('moneticoBankFeedback','status3ds4'),
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
            ),
            'numauto' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'motifrefus' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    ''    => '',
                    'Appel Phonie'    => $l10n->getLabel('moneticoBankFeedback','Appel Phonie'),
                    'Refus'         => $l10n->getLabel('moneticoBankFeedback','Refus'),
                    'Interdit'    => $l10n->getLabel('moneticoBankFeedback','Interdit'),
                    'filtrage'    => $l10n->getLabel('moneticoBankFeedback','filtrage'),
                    'scoring'    => $l10n->getLabel('moneticoBankFeedback','scoring'),
                    '3DSecure'    => $l10n->getLabel('moneticoBankFeedback','3DSecure'),
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-6'
                ),
            ),
            'originecb' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'bincb' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'hpancb' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'ipclient' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'originetr' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'veres' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'pares' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'montantech' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'filtragecause' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    ''    => '',
                    '1'    => $l10n->getLabel('moneticoBankFeedback','filtrage1'),
                    '2'    => $l10n->getLabel('moneticoBankFeedback','filtrage2'),
                    '3'    => $l10n->getLabel('moneticoBankFeedback','filtrage3'),
                    '4'    => $l10n->getLabel('moneticoBankFeedback','filtrage4'),
                    '5'    => $l10n->getLabel('moneticoBankFeedback','filtrage5'),
                    '6'    => $l10n->getLabel('moneticoBankFeedback','filtrage6'),
                    '7'    => $l10n->getLabel('moneticoBankFeedback','filtrage7'),
                    '8'    => $l10n->getLabel('moneticoBankFeedback','filtrage8'),
                    '9'    => $l10n->getLabel('moneticoBankFeedback','filtrage9'),
                    '11'    => $l10n->getLabel('moneticoBankFeedback','filtrage11'),
                    '12'    => $l10n->getLabel('moneticoBankFeedback','filtrage12'),
                    '13'    => $l10n->getLabel('moneticoBankFeedback','filtrage13'),
                    '14'    => $l10n->getLabel('moneticoBankFeedback','filtrage14'),
                    '15'    => $l10n->getLabel('moneticoBankFeedback','filtrage15'),
                    '16'    => $l10n->getLabel('moneticoBankFeedback','filtrage16'),
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-6'
                ),
            ),
            'filtragevaleur' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'cbenregistree' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    ''    => '',
                    '0'    => $l10n->getLabel('moneticoBankFeedback','autre'),
                    '1'    => $l10n->getLabel('moneticoBankFeedback','oui'),
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-6'
                ),
            ),
            'cbmasquee' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'modepaiement' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    ''    => '',
                    'CB'        => 'CB',
                    'paypal'    => 'paypal',
                    '1euro'     => '1euro',
                    '3xcb'      => '3xcb',
                    'audiotel'  => 'audiotel',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
            ),
        ));

        //No field should be editable
     /*   foreach($this->data as $key => &$value){
            $value['editable'] = false;
        }*/



        $this->showInEditForm = array(
            'tab_main'=>array(
                'parent_uid','MAC','date','tpe','montant','reference','texte-libre','code-retour','cvx','vld',
                'brand','status3ds','numauto','motifrefus','originecb','bincb','hpancb','ipclient','originetr','veres',
                'pares','montantech','filtragecause','filtragevaleur','cbenregistree','cbmasquee','modepaiement',
            ),
        );



        //static::$postFormHTML[] = '<script>';
        //static::$postFormHTML[] = "</script>";


    }


    static function formatCodeRetour($key, $value, $locale, $row){
        global $mf,$l10n;
        return $l10n->getLabel('moneticoBankFeedback',$value.'_short');
    }


}















