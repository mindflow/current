<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
require_once DOC_ROOT.SUB_DIR.'/mf/core/records/simpleRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

/**
 * Base class for handling database records and their forms.
 *
 * You can subclass dbRecord to create your own type of record. Inside it, you will define the database table and the form fiels configuration. Then the dbRecord class will handle easy storage of your record using the store() method and loading using the load() method.
 * An editable list of your records can also easily be generated using the showRecordEditTable() function.
 */
abstract class dbRecord extends simpleRecord {

   // private static $tableName;                      //name of the SQL table
  //  static $oneTablePerLocale = false;      //if set to true, mindflow will create and manage one table per locale. Each table will feature a suffix for the locale, such as _fr, _en or _de
  //  static $createTableSQL;                 //SQL for the inherited class
  //  static $createTableKeysSQL;             //SQL keys for the inherited class
  //  static $enableImportInitializationData = 0;  //Enables initializing the table with data imported from function importInitializationData()

	/*
	 * When $enableMultiDBHosts is enabled, when creating records using the ->store() functions, the uid will be determined according to the $autoIncrementIncrement and  $autoIncrementOffset values
	 * This mimics the behavior of the MySQL variables auto_increment_increment and auto_increment_offset, but at the MindFlow application level.
	 * This is useful when you can't modify your MySQL server configuration or you only want to do this on a per-table level
	 * This likely musn't be used in conjunction with a server that already has auto_increment_increment and auto_increment_offset tweaked in its my.cnf
	 * And also INSERTS must not be performed directly in SQL unless you really know what you do and take account of the $autoIncrementIncrement and  $autoIncrementOffset values in your INSERT request.
	 */
	static $enableMultiDBHosts = false;
	static $autoIncrementIncrement = 1;
	static $autoIncrementOffset = 1;

    static $dummyFieldTypes = array('fieldset','html','dummy-input','dummy-date','dummy-datetime','dummy-time','dummy-color','dummy-hidden','dummy-urlinput','dummy-checkbox','dummy-select','dummy-select','dummy-textarea'); //fields that have no value and should not be inserted in the database (usually page layout fields)

	// Array of chunks of HTML code to be displayed after the object's forms
    static $postFormHTML = array();

	//functions to be executed in third party plugins when a trigger is called on a class
	static private $hooks = array(
		'preCreate' => array(),
		'postCreate' => array(),
		'preStore' => array(),
		'postStore' => array(),
		'preLoad' => array(),
		'postLoad' => array(),
		'preDelete' => array(),
		'postDelete' => array(),
		'preRestore' => array(),
		'postRestore' => array()
	);

	
    //class data
    var $data;              // this array stores the data of our object for quick manipulation before storing the object into the database
    var $showInEditForm;    // lists the fields from the data array to be shown in the edit form by default

    //Flags for disabling pre / post process functions
    //usefull when importing existing data, if you do not want these functions to be trigerred when creating records using dbRecord
    var $enablePreCreate=true;
    var $enablePostCreate=true;
    var $enablePreStore=true;
    var $enablePostStore=true;
    var $enablePreLoad=true;
    var $enablePostLoad=true;
    var $enablePreDelete=true;
    var $enablePostDelete=true;
    var $enablePreRestore=true;
    var $enablePostRestore=true;

    //flags for features
    var $enableHistory=false;       //storing record history impacts performance, so it is disabled by default
    var $reloadFormOnSave=false;    //forces re-evaluating and displaying the record's editing form throught AJAX feedback once the record is saved. Useful if some value affects the form aspect. Disabled by default for performance optimization.

    //SQL for the dbRecord class : all mindflow records must have these fields available
    static $baseTableSQL= "
          `uid` int(11) NOT NULL AUTO_INCREMENT,                # unique ID of the record
          `parent_uid` int(11) unsigned NOT NULL DEFAULT '0',   # when this record has been created from another record to which it is related, uid of the parent record
          `parent_class` varchar(50) NOT NULL DEFAULT '',       # class of the parent record (see above)
          `deleted` tinyint(4) NOT NULL DEFAULT '0',            # 0 when the record is active, 1 when the record is deleted, 2 when the record is in temporary state (under creation)
          `hidden` tinyint(1) NOT NULL DEFAULT '0',             # 0 when the record is active, 1 when the record is temporary disabled
          `sorting` int(11) NOT NULL DEFAULT '0',               # sorting index among other records of the same class
          `creation_date` datetime(6) NOT NULL,                 # creation datetime of the record
          `modification_date` datetime(6) NOT NULL,             # last modification datetime of the record
          `creator_uid` int(11) DEFAULT '0',                    # uid of the record of the user who created this record 
          `creator_class` varchar(50) NOT NULL DEFAULT '',      # class of the user who created this record (often mfUser)
          `modificator_uid` int(11) DEFAULT '0',                # uid of the record of the user who last modified this record 
          `modificator_class` varchar(50),                      # class of the user who last modified this record (often mfUser)
          `edit_lock` int(11) NOT NULL DEFAULT '0',             # flag intended to indicate the record is currently being edited by someone else. Not implemented yet.
          `editor_uid` int(11) NOT NULL DEFAULT '0',            # uid of the profile currently editing the record
          `editor_class` int(11) NOT NULL DEFAULT '0',          # class of the profile currently editing the record
          `edit_start_time` datetime NULL,                      # datetime when the editing has started
          `start_time` datetime NULL,                           # datetime when this record is due to go live. Not implemented yet.
          `end_time` datetime NULL,                             # datetime when this record is due to expire. Not implemented yet.
          `language` varchar(5) NOT NULL,                       # ISO2 code/locale of the language of the record 
          `alt_language` tinytext,                              # when this record is an alternate language of a main language record, indicates the uid of the main language record 
          `history_last` mediumblob DEFAULT '',                 # value of the data array of the current record at last save operation. requires enableHistory=true
          `history_past` longblob DEFAULT '',                   # diffs of the record since its creation. requires enableHistory=true
    ";

    //ALTER TABLE `vz_orders_CB_fr` ADD `history_last` TEXT NOT NULL DEFAULT '' AFTER `alt_language`, ADD `history_past` TEXT NOT NULL DEFAULT '' AFTER `history_last`;

    //SQL for the keys of the dbRecord class : all mindflow records must have these fields available
    static $baseTableKeysSQL = "
          PRIMARY KEY  (`uid`),
          KEY `k_dbrecord_language`(`language`),
          KEY `k_dbrecord_cdate`(`creation_date`),
          KEY `k_dbrecord_mdate`(`modification_date`),
          KEY `k_dbrecord_deleted`(`deleted`),
          KEY `k_dbrecord_sorting`(`sorting`),
          KEY `k_dbrecord_parent` (`parent_uid`),
          KEY `k_dbrecord_creator` (`creator_uid`),
    ";



    /*
    * Constructeur
    */
    function __construct(){
        $this->init();
    }


    function init(){
        global $mf,$l10n;

        $l10n->loadL10nFile('dbRecord', '/mf/languages/dbRecord_l10n.php');

        $this->dataType = array();
        $this->dataEditingEnable = array();
        $this->showInEditForm = array();

        $this->data = array(
            'uid'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'number',
                'field_attributes' => array('class' => 'input-md'),
                'div_attributes' => array('class' => 'col-lg-2'),
                'editable' => false,
            ),
            'parent_uid' => array(
                'value' => '0',
                'dataType' => 'hidden',
                'valueType' => 'number',
                'field_attributes' => array('class' => 'input-md'),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'parent_class' => array(
                'value' => '0',
                'dataType' => 'hidden',
                'valueType' => 'text',
                'field_attributes' => array('class' => 'input-md'),
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'deleted'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
            ),
            'hidden'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'div_attributes' => array('class' => 'mf_unhide_hidden col-lg-3'),
            ),
            'sorting'   =>  array(
                'value' => '0',
                'dataType' => 'input',
                'valueType' => 'number',
            ),
            'creation_date'   =>  array(
                'value' => '',
                'dataType' => 'datetime.ms',
                'valueType' => 'datetime.ms',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
                'editable' => false,
                'preDisplay' => 'dbRecord::formatCreationDate',
            ),
            'modification_date'   =>  array(
                'value' => '',
                'dataType' => 'datetime.ms',
                'valueType' => 'datetime.ms',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
                'editable' => false,
            ),
            'creator_uid'   =>  array(
                'value' => '',
                'dataType' => 'record',
                'editType' => 'single', //single or multiple
                'editClass' => 'mfUser',
                'addItemsSQL' => array(
                    'SELECT' => 'username,first_name,last_name',
                    'FROM' => 'mf_user',
                    'WHERE' => '1',
                    'ORDER BY' => 'username',
                    'ORDER DIRECTION' => 'ASC',
                ),
                'selectActions' => array(
                    'view' => 1,
                    'edit' => 1,
                    'create' => 0,
                    'delete' => 0,
                    'sort' => 0,
                    'clipboard' => 0
                ),
                'icon' => '<b class="glyphicon glyphicon-user"></b>',
                'valueType' => 'number' ,
                'div_attributes' => array('class' => 'col-lg-8'),
                'field_attributes' => array('class' => 'stat'),
                'debugSQL' => false,
                'keyProcessors' => array(
                    //'date'=>'depart::formatDateDepart',

                ),
            ),
            'creator_class'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
            ),
            'modificator_uid'   =>  array(
                'value' => '',
                'dataType' => 'record',
                'editType' => 'single', //single or multiple
                'editClass' => 'mfUser',
                'addItemsSQL' => array(
                    'SELECT' => 'username,first_name,last_name',
                    'FROM' => 'mf_user',
                    'WHERE' => '1',
                    'ORDER BY' => 'username',
                    'ORDER DIRECTION' => 'ASC',
                ),
                'selectActions' => array(
                    'view' => 1,
                    'edit' => 1,
                    'create' => 0,
                    'delete' => 0,
                    'sort' => 0,
                    'clipboard' => 0
                ),
                'icon' => '<b class="glyphicon glyphicon-user"></b>',
                'valueType' => 'number' ,
                'div_attributes' => array('class' => 'col-lg-8'),
                'field_attributes' => array('class' => 'stat'),
                'debugSQL' => false,
                'keyProcessors' => array(
                    //'date'=>'depart::formatDateDepart',

                ),
            ),
            'modificator_class'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
            ),
           'edit_lock'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
            ),
            'editor_uid'   =>  array(
	            'value' => '0',
	            'dataType' => 'record',
	            'editType' => 'single', //single or multiple
	            'editClass' => 'mfUser',
	            'addItemsSQL' => array(
		            'SELECT' => 'username,first_name,last_name',
		            'FROM' => 'mf_user',
		            'WHERE' => '1',
		            'ORDER BY' => 'username',
		            'ORDER DIRECTION' => 'ASC',
	            ),
	            'selectActions' => array(
		            'view' => 1,
		            'edit' => 1,
		            'create' => 0,
		            'delete' => 0,
		            'sort' => 0,
		            'clipboard' => 0
	            ),
	            'icon' => '<b class="glyphicon glyphicon-user"></b>',
	            'valueType' => 'number' ,
	            'div_attributes' => array('class' => 'col-lg-8'),
	            'field_attributes' => array('class' => 'stat'),
	            'debugSQL' => false,
	            'keyProcessors' => array(
		            //'date'=>'depart::formatDateDepart',

	            ),
            ),
            'editor_class'   =>  array(
	            'value' => '',
	            'dataType' => 'input',
	            'valueType' => 'text',
            ),
            'edit_start_time'   =>  array(
	            'value' => null,
	            'dataType' => 'datetime',
	            'valueType' => 'datetime',
            ),
            'start_time'   =>  array(
                'value' => null,
                'dataType' => 'datetime',
                'valueType' => 'datetime',
            ),
            'end_time'   =>  array(
                'value' => null,
                'dataType' => 'datetime',
                'valueType' => 'datetime',
            ),
            'language'   =>  array(
                'value' => '',
                'dataType' => 'select',
                'valueType' => 'number',

            ),
            'alt_language'   =>  array(
                'value' => '',
                'dataType' => 'altLang',
                'processor' => '',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'history_last'   =>  array(
                'value' => array(),
                'dataType' => 'textarea 50 5',
                'processor' => '',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'history_past'   =>  array(
                'value' => array(),
                'dataType' => 'textarea 50 5',
                'processor' => '',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
           /* 'record_status' => array(
                'value' => 'active',
                'dataType' => 'select',
                'possibleValues' => array(
                    'temp' => $l10n->getLabel('dbRecord','temp'),
                    'active' => $l10n->getLabel('dbRecord','active'),
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),*/
        );
    }


    /*
     * Loads the language file for the current class
     * @param $pluginRelativePath String the path to the plugin folder
     * @param $subDir String (optional) a folder name located below the languages folder, with a resulting path like :  $pluginRelativePath.'/languages/'.$subDir
     * @param $forceClassName String Sometimes, such as when you subclass a dbRecord, you may want to force the class name, such as supplying the parent name in the case of a subclass, which would avoid you duplicating all the parent l10n labels.
     */
    function loadL10n($pluginRelativePath, $subDir='', $forceClassName=''){


        if($forceClassName=='')$className = get_class($this);
        else $className = $forceClassName;

        echo "<strong>Warning : ".$className." : </strong>dbRecord->loadL10n() function is now obsolete. Use \$mf->l10n->loadL10nRecord(\$recordClassName, \$languageFilePath) from your plugin's index.php file now.<br /><br />".chr(10).chr(10);
        echo nl2br(generateCallTrace());

        //load l10n text for inherited record types
        if(get_class($this) != 'dbRecord' && !isset($GLOBALS['l10n_text'][$className])){
            //make sure base class dbRecord l10n text is loaded for any record type
            if(!isset($GLOBALS['l10n_text']['dbRecord']) || (isset($GLOBALS['l10n_text']['dbRecord'])&& count($GLOBALS['l10n_text']['dbRecord'])==0)){
                include DOC_ROOT.SUB_DIR.'/mf/languages/dbRecord_l10n.php';
                $GLOBALS['l10n_text']['dbRecord'] =  $l10nText;
            }
            //load the current record's translation files
            $subdir=trim($subDir);
            if(trim($subDir)!='')$subDir=$subDir.'/';

            include DOC_ROOT.SUB_DIR.$pluginRelativePath.'/languages/'.$subDir.$className.'_l10n.php';

            //merging base class dbRecord existing Labels
            if($l10nText != null) {
                $l10nWithParent = array_merge_recursive_distinct($GLOBALS['l10n_text']['dbRecord'], $l10nText);
                $GLOBALS['l10n_text'][$className] = $l10nWithParent;
            }
            else{
                $GLOBALS['l10n_text'][$className] = $GLOBALS['l10n_text']['dbRecord'];
            }
        }

    }





    /**
 * loads a database record given its uid and returns the count of records found
 * @param $uid the uid of the record to load
 * @param $forceLocale boolean allows forcing the load from a locale different than the one currently displayed in frontend of backend. Especially, this will affect the value of static::getTableName() when $oneTablePerLocale is true
 * @param $debug boolean outputs the SQL request to the debug log
 * @return int the count of data rows loaded for this record, or 0 if nothings has been loaded
 */
	function load($uid,$forceLocale=null,$debug=false){

		global $mf,$l10n,$pdo,$logger;

		//forcing a different locale if required. This will affect the value of static::getTableName() when $oneTablePerLocale is true
		if(isset($forceLocale)){
			//save and hold current locale
			$currentLocale = $mf->info['data_editing_locale'] ;
			//set new locale
			$mf->setDataEditingLanguage($forceLocale);
		}

		//execute custom user code prior presenting the data
		if($this->enablePreLoad){
			$this->preLoad();
			$this->playHooks('preLoad');
		}

		//preparing request only once
		try {
			$sql = "SELECT * FROM " . static::getTableName() . " WHERE uid = " . $pdo->quote(intval($uid));
			if($debug)$logger->debug($sql);

			$stmt = $pdo->query($sql);

			if($stmt->rowCount() >0) {
				$row = $stmt->fetch( PDO::FETCH_ASSOC );

				$this->loadRow( $row );

				//fix creator class in creator recordSelect field
				$this->data['creator_uid']['editClass'] = $this->data['creator_class']['value'];
				if ( isset( $this->data['modificator_class']['value'] ) ) {
					$this->data['modificator_uid']['editClass'] = $this->data['modificator_class']['value'];
				}

				if ( $this->enablePostLoad ) {
					$this->postLoad();
					$this->playHooks('postLoad');
				}

			}
			//restoring current locale if required.
			if(isset($forceLocale)){
				$mf->setDataEditingLanguage($currentLocale);
			}

			return $stmt->rowCount();
		}
		catch(PDOException $e){
			ob_start();
			echo "dbRecord->load()/ ERROR:".chr(10).$e->getMessage().chr(10).
			     "ERROR=".$pdo->errorInfo()[2].chr(10).
			     "SQL=".$sql.chr(10).chr(10);

			debug_print_backtrace();

			$logger->critical(ob_get_clean());
			die();
		}
	}


	/**
	 * loads multiple database records from a single request given the sqlCondition given.
	 * The SQL request will be ofrget like this :
	 * "SELECT * FROM " . static::getTableName() . " WHERE ".$sqlConditions;
	 *
	 * @param $sqlConditions the conditions after the WHERE in the SQL request
	 * @return array of loaded instances
	 */
	static function loadMultiple($sqlConditions){

		global $mf,$l10n,$pdo,$logger;

		$records = array();
		$tableName = static::getTableName();
		$className = static::getClassName();

		//preparing request only once
		try {
			$sql = "SELECT * FROM " . $tableName . " WHERE ".$sqlConditions;

			$stmt = $pdo->query($sql);

			if($stmt->rowCount() >0) {

				$record = new $className();

				//execute custom user code prior presenting the data
				if($record->enablePreLoad){
					$record->preLoad();
					$record->playHooks('preLoad');
				}

				$row = $stmt->fetch( PDO::FETCH_ASSOC );

				$record->loadRow( $row );

				//fix creator class in creator recordSelect field
				$record->data['creator_uid']['editClass'] = $record->data['creator_class']['value'];

				if ( isset( $record->data['modificator_class']['value'] ) ) {
					$record->data['modificator_uid']['editClass'] = $record->data['modificator_class']['value'];
				}

				if ( $record->enablePostLoad ) {
					$record->postLoad();
					$record->playHooks('postLoad');
				}
				$records[] = $record;
			}
			return $records;
		}
		catch(PDOException  $e){
			$comment='';

			if($e->getCode() == '42S02') $comment = $l10n->getLabel('main','plugin_table_error1').' '.$l10n->getLabel('main','plugin_table_error2');

			echo '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>';
			//echo $l10n->getLabel('main','error').' '.$e->getCode().'<br />'.chr(10);
			echo $e->getMessage().'<br />'.chr(10);
			if($comment!='')echo  $comment.'<br />'.chr(10);
			echo "</body></html>";

			$logger->error($l10n->getLabel('main','error').$e->getMessage());
			if($comment!='')$logger->error($comment);

			die();
		}
	}

    /**
     * Loads a row from the SQL table associated with the object into the object data array
     * @param $row array an SQL row
     */
    function loadRow($row){
        if($row) {
            if (!$this->data) $this->init();
            if (!isset($this->data) || count($this->data)==0 ) throw new RuntimeException('data array is not properly initialized for record of class '.get_class($this).chr(10));
            foreach ($this->data as $key => $value) {
                $this->loadKey($row, $key, $value);
            }

        }
    }

    /**
     * Reloads the current record from the database, in case it may have been updated in between
     */
    function reload(){
        $this->load($this->data['uid']['value']);
    }

    /**
     * Loads a key from  the given SQL row and stores its value into the object data['key'] array
     * @param $row array an SQL row
     * @param $key string the column identifier in the SQL row and in the data array (both must always match)
     * @param $value string the value to store in the key
     */
    function loadKey($row, $key,$value){
        global $mf;
        $locale = $mf->getDisplayedLocale();

        if(isset($this->data[$key]['dataType']))
        {
            $dataType = explode(' ',trim($this->data[$key]['dataType']));
            $fieldType = $dataType[0];

            if(!in_array($fieldType,self::$dummyFieldTypes)){

                if ($fieldType == 'date') {
                    if ($row[$key] != '') {
                        try {
                            if ($row[$key] != '0000-00-00' && $row[$key] != '') {
                                $date = new DateTime($row[$key]);
                                $this->data[$key]['value'] = $date->format($GLOBALS['site_locales'][$locale]['php_date_format']);
                                $this->data[$key]['RAW_value'] = $row[$key];
                            } else {
                                $this->data[$key]['value'] = '';
                                $this->data[$key]['RAW_value'] = '';
                            }
                        } catch (Exception $e) {
                            $this->data[$key]['value'] = '';
                        }
                    } else $this->data[$key]['value'] = '';
                }

                else if ($fieldType == 'datetime') {
                    if ($row[$key] != '') {
                        try {
                            if ($row[$key] != '0000-00-00 00:00:00' && $row[$key] != '') {

                                $date = new DateTime($row[$key]);
                                $this->data[$key]['value'] = $date->format($GLOBALS['site_locales'][$locale]['php_datetime_format']);
                                $this->data[$key]['RAW_value'] = $row[$key];
                            } else {
                                $this->data[$key]['value'] = '';
                                $this->data[$key]['RAW_value'] = '';
                            }
                        } catch (Exception $e) {
                            $this->data[$key]['value'] = '';
                        }
                    } else $this->data[$key]['value'] = '';
                }

                else if ($fieldType == 'datetime.ms') {
                    if ($row[$key] != '') {
                        try {
                            if ($row[$key] != '0000-00-00 00:00:00.000000' && $row[$key] != '') {
                                $date = new DateTime($row[$key]);
                                $this->data[$key]['value'] = $date->format($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']);
                                $this->data[$key]['RAW_value'] = $row[$key];
                            } else {
                                $this->data[$key]['value'] = '';
                                $this->data[$key]['RAW_value'] = '';
                            }
                        } catch (Exception $e) {
                            $this->data[$key]['value'] = '';
                        }
                    } else $this->data[$key]['value'] = '';

                }

                else if ($fieldType == 'files') {

                    //try to unserialize the value
                    $data = @unserialize($row[$key]);
                    if ($data !== false) {
                        //if ok, use it
                        $this->data[$key]['value'] = $data;
                    } else {
                        //mf < 1.4 compatibility check
                        $this->data[$key]['value'] = $row[$key];
                    }

                    /*
                    //mf < 1.4 compatibility check
                    if(isset($row[$key]))$this->data[$key]['value'] = $this->convertFilesValue($row[$key]);
                    else {
                        $mf->addErrorMessage(get_class($this).' key '.$key.' is not defined in the database or its default value should not be NULL');
                    }*/

                }

                else if ($key == 'alt_language') {
                    if (!is_array($row[$key])) $this->data[$key]['value'] = unserialize($row[$key]);
                    else $this->data[$key]['value'] = '';
                }

                else if ($key == 'history_last' || $key == 'history_past') {
                    if ($this->enableHistory && !is_array($row[$key])) $this->data[$key]['value'] = unserialize($row[$key]);
                }

                else if ($fieldType == 'template_data') {

                    //updating template field value with data glued with microtemplate definition
                    $this->data[$key]['value'] = self::glueWithTemplateInfo($this->data['template_name']['value'], unserialize($row[$key]));

                    //also glue microtemplates
                    foreach ($this->data[$key]['value'] as $key2 => $value2) {


                        if (isset($value2['dataType'])) {
                            $dataType2 = explode(' ', $value2['dataType']);
                            $fieldType2 = $dataType2[0];

                            if ($fieldType2 == 'microtemplate') {

                                foreach ($value2['value'] as $microIndex => $microDatastructure) {

                                    $microtemplateKey = $microDatastructure['microtemplate_key'];

                                    //updating microtemplate field value with data glued with microtemplate definition
                                    //$this->data[$key]['value'][$key2]['value'][$microIndex]['microtemplate_data'] = self::glueWithMicrotemplateInfo($microtemplateKey, $microDatastructure['microtemplate_data']);
                                    $this->data[$key]['value'][$key2]['value'][$microIndex] = self::glueWithMicrotemplateInfo($microtemplateKey, $microDatastructure);

                                    /*print_r($microDatastructure);
                                    echo('*******').chr(10).chr(10);
                                    print_r($this->data[$key]['value'][$key2]['value'][$microIndex]);
                                    echo('*********************************************').chr(10).chr(10).chr(10).chr(10);*/
                                }
                            }
                        }

                    }
                } //TODO : Also Glue microtemplates at record LEVEL adapting the code above. This code needs testing...
                else if ($fieldType == 'microtemplate') {
                    foreach ($value['value'] as $microIndex => $microDatastructure) {

                        $microtemplateKey = $microDatastructure['microtemplate_key'];

                        //updating microtemplate field value with data glued with microtemplate definition
                        //$this->data[$key]['value'][$microIndex]['microtemplate_data'] = self::glueWithMicrotemplateInfo($microtemplateKey, $microDatastructure['microtemplate_data']);
                        $this->data[$key]['value'][$microIndex] = self::glueWithMicrotemplateInfo($microtemplateKey, $microDatastructure);
                        //print_r($this->data[$key]['value'][$microIndex]);

                    }
                }

                else {
                    if (isset($row[$key])) $this->data[$key]['value'] = $row[$key];
                }
            }
        }

        //else throw key has no datatype set ?
    }

    /**************************************************
     *   MindFlow < v1.4 data conversion
     *  Converts old values stored as a string to an array
     *************************************************/

    function convertFilesValue($activeValue){

        //mf < 1.4 compatibility

        global $mf;

        if($activeValue=='')return array();

        else{
            //try to unserialize the value
            $data = @unserialize($activeValue);
            if ($data !== false) {
                //if ok, use it
                return $data;
            } else {
                //else mf < 1.4, try to explode array
                $oldValues = explode(',',ltrim($activeValue,','));
                $updatedValue = array();
                $i=0;

                foreach($oldValues as $index => $filename){

                    //convert filename and include timestamp in it
                    $timestamp = time();
                    $fileNamePart = strstr($filename, '.', true)."_".$timestamp;
                    $fileExtPart = strstr($filename, '.', false);

                    $filePath = static::getClassName().DIRECTORY_SEPARATOR.static::getClassName().'_'.$this->data['uid']['value'].DIRECTORY_SEPARATOR;
                    list($width, $height, $type, $attr)= getimagesize(DOC_ROOT.SUB_DIR.$filePath.$fileNamePart.$fileExtPart);

                    //upgrade the file data structure from a string to an array
                    $updatedValue[$i]=array(
                        'timestamp' => $timestamp,
                        'filename' => $fileNamePart.$fileExtPart,
                        'filepath' => $filePath,
                        'width' => $width,
                        'height' => $height
                    );

                    //rename the file
                    @rename(DOC_ROOT.SUB_DIR.$filePath.$filename, DOC_ROOT.SUB_DIR.$filePath.$fileNamePart.$fileExtPart);
                    //echo "rename(".DOC_ROOT.SUB_DIR.$filePath.$filename.", ".DOC_ROOT.SUB_DIR.$filePath.$fileNamePart.$fileExtPart.");";
                    $i++;
                }

                return $updatedValue;
            }
        }
    }




    /*
     * Strips keys such as 'dataType', processor', 'field_attributes' and keeps only 'value'
     * This reduces database storage requirements for records, and anyway these kind of settings have to be loaded dynamically from
     * the record specification in case they get updated manually by the website developper
     */
    static function stripTemplateInfo($templateData){

        $strippedTemplateData = array();

        foreach($templateData as $key => $spec){
            if(isset($spec['dataType']) && $spec['dataType']=='microtemplate') {
	            if ( isset( $spec['value'] ) ) {
		            $strippedTemplateData[ $key ]['value'] = self::stripMicroTemplateInfo( $spec['value'] );
	            }
            }
            else {
	            if ( isset( $spec['value'] ) ) {
		            $strippedTemplateData[ $key ]['value'] = $spec['value'];
	            }
            }
        }
        return $strippedTemplateData;
    }


    /*
     * Strips keys such as 'dataType', processor', 'field_attributes' and keeps only 'value'
     * This reduces database storage requirements for records, and anyway these kind of settings have to be loaded dynamically from
     * the record specification in case they get updated manually by the website developper
     */
    static function stripMicroTemplateInfo($microTemplateData){

        $strippedData = array();

        foreach($microTemplateData as $dataIndex => $dataEntry){

            $strippedData[$dataIndex] = array();

            foreach($dataEntry as $key => $value){
                switch($key){
                    case 'microtemplate_key':
                        $strippedData[$dataIndex]['microtemplate_key'] = $value;
                        break;
                    case 'microtemplate_data':
                        $strippedData[$dataIndex]['microtemplate_data'] = self::stripTemplateInfo($value);
                        break;
                    default:

                }
            }
        }

        return $strippedData;
    }



    /**
     * glues 'value' keys of template content with the rest of the record definition
     * This is required since only keys named 'value' are stored in the database to minimize space taken
     * @param $templateName String the name of the template to load the specification from
     * @param $strippedTemplateContent Array the content loaded from the database, stripped from the useless fields, keeping only value fields
     */
    static function glueWithTemplateInfo($templateName, $strippedTemplateContent){

        if($templateName!=''){
            global $mf;
            if(!is_array($strippedTemplateContent))return $mf->templates[$templateName]['template_data'];
            else {
                //echo $templateName.'<br/>';
                //if($templateName == 'Page d\'accueil v2')print_r($mf->templates[$templateName]['template_data']);die();
                if(isset($mf->templates[$templateName]) && is_array($mf->templates[$templateName]['template_data']))return array_merge_recursive_distinct($mf->templates[$templateName]['template_data'],$strippedTemplateContent);
                else return $strippedTemplateContent;
            }
        }
        else return array();
    }

    /**
     * glues 'value' keys of microtemplate content with the rest of the record definition
     * This is required since only keys named 'value' are stored in the database to minimize space taken
     * @param $templateName String the name of the template to load the specification from
     * @param $strippedTemplateContent Array the content loaded from the database, stripped from the useless fields, keeping only value fields
     */
    static function glueWithMicrotemplateInfo($microtemplateName, $strippedTemplateContent){

        if($microtemplateName!=''){
            global $mf;
            if(!is_array($strippedTemplateContent)){
                return $mf->microtemplates[$microtemplateName];
            }
            else {
                if(isset($mf->microtemplates[$microtemplateName]) && is_array($mf->microtemplates[$microtemplateName])){
                    /*print_r($mf->microtemplates[$microtemplateName]);
                    echo('*******').chr(10).chr(10);
                    print_r($strippedTemplateContent);
                    echo('*********************************************').chr(10).chr(10).chr(10).chr(10);
*/
                    return array_merge_recursive_distinct($mf->microtemplates[$microtemplateName],$strippedTemplateContent);
                }
                else return $strippedTemplateContent;
            }
        }
        else return array();
    }

    /**
     * Returns the next available Uid if you badly need it before storing your record.
     * USE WITH CAUTION : it DOES NOT RESERVE the uid, so there is a slight risk that a record can be created by another script between the time you ask for the Uid and the time you effectively create your record.
     * If possible prefer using the autoincrement feature by setting a void value "" to the uid field.
     * This can be usefull for performance reason sometimes though
     * @return int the uid of the next record created.
     */
    function getNextAvailableUid(){
        global $pdo;

        $sql = "SELECT MAX(uid) FROM ".static::getTableName();
        $stmt = $pdo->query($sql);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0]+1;
    }


    /**
     * Compute the difference between $arr1 and $arr2.
     * The result is an array (can be empty) giving the
     * differences between the 2 arrays.
     *
     * Note the differences are expressed in a form you
     * can use the resulting array to "patch" $arr1 to
     * obtain $arr2.
     *
     * This is basically the same stuff than for "diff"
     * command (from UNIX).
     *
     * @author wrey75@gmail.com https://gist.github.com/wrey75/c631f6fe9c975354aec7
     * @param array $arr1 the first array
     * @param array $arr2 the second array
     * @return array|null returns the diff array if both input arguments are arrays, null otherwise.
     */
    static function diff($arr1, $arr2) {

        $diff = array();

        if(is_array($arr1) && is_array($arr2)) {
            // Check the similarities
            foreach ($arr1 as $k1 => $v1) {
                if(array_key_exists ($k1, $arr2)){
                    $v2 = $arr2[$k1];
                    if (is_array($v1) && is_array($v2)) {
                        // 2 arrays: just go further...
                        // .. and explain it's an update!
                        $changes = self::diff($v1, $v2);
                        if (count($changes) > 0) {
                            // If we have no change, simply ignore
                            $diff[$k1] = array('upd' => $changes);
                        }
                        unset($arr2[$k1]); // don't forget
                    } else if ($v2 === $v1) {
                        // unset the value on the second array
                        // for the "surplus"
                        unset($arr2[$k1]);
                    } else {
                        // Don't mind if arrays or not.
                        $diff[$k1] = array('old' => $v1, 'new' => $v2);
                        unset($arr2[$k1]);
                    }
                } else {
                    // remove information
                    $diff[$k1] = array('old' => $v1);
                }
            }

            // Now, check for new stuff in $arr2
            reset($arr2); // Don't argue it's unnecessary (even I believe you)
            foreach ($arr2 as $k => $v) {
                // OK, it is quite stupid my friend
                $diff[$k] = array('new' => $v);
            }
            return $diff;
        }
        else return null;
    }


    /**
     * Patching an array using a diff
     *
     * @author wrey75@gmail.com
     * @param unknown $arr
     * @param unknown $patch
     */
    static function patch($arr, $patch) {
        $dest = $arr;
        foreach ($patch as $k=>$v){
            // $k is the key to change
            // $v contains 'old' and 'new'.

            if( !is_array($v) ){
                error_log('$patch is a bad argument.');
            }
            else if( isset( $v['upd'] )){
                // Update...!
                $dest[$k] = self::patch($arr[$k], $v['upd']);
            }
            else if( !isset( $v['new'] )){
                // Remove the entry
                unset( $dest[$k] );
            }
            else {
                // A new value (can be array by the way).
                $dest[$k] = $v['new'];
            }
        }
        return $dest;
    }

    /**
     * Computes and returns the diff() of the current record's state with its state during previous store() execution
     * @return array
     */
    function historyGetDiffWithPreviousStore(){

    	//keep only 'value' attributes of fields in order to save space and performance in the database
        $currentData = self::stripTemplateInfo($this->data);

        //we do not want to have these keys processed in the diff()
        unset($currentData['history_last']); // history_last = all record's values as they were on last store() operation
        unset($currentData['history_past']); // history_past = stores the differences between each store() operation

        return self::diff($this->data['history_last']['value'],$currentData);
    }

    /**
     * Stores an existing record or creates a new record
     * @param $updateDates boolean set to false if you don't want mindflow to automatically set creation_date and modification_date, thus overwriting values you may have set manually
     * @param $forceCreate boolean forces the creation of a new record with a new uid, even if the current record's uid already exists in the database.
     * @param $debug boolean echoes the SQL Request generated to the logger
     * @param $forceLocale boolean allows forcing the store in a locale different than the one currently displayed in frontend of backend. Especially, this will affect the value of static::getTableName() when $oneTablePerLocale is true
     * @return int the uid of the record stored or created. 0 if creation / storage has failed
     */
    function store($updateDates = true, $forceCreate = false, $debug = false, $forceLocale = null){

        global $mf,$l10n,$pdo,$logger;


	    //forcing a different locale if required. This will affect the value of static::getTableName() when $oneTablePerLocale is true
	    if(isset($forceLocale)){
		    //save and hold current locale
		    $currentLocale = $mf->info['data_editing_locale'] ;
		    //set new locale
		    $mf->setDataEditingLanguage($forceLocale);
		    $locale = $forceLocale;
	    }
		else $locale = $mf->getDisplayedLocale();

        //set record language
        if($this->data['language']['value'] == '') $this->data['language']['value'] = $mf->getDataEditingLanguage();

        if(is_array($this->data['alt_language']['value']))$this->data['alt_language']['value'] = serialize($this->data['alt_language']['value']);

        //preparing request only once
        //verifying the record exists in the database
        $sql = "SELECT COUNT(*) FROM ".static::getTableName()." WHERE uid = ".$pdo->quote(intval($this->data['uid']['value']));

        $countStmt = $pdo->query($sql);
        $row = $countStmt->fetch(PDO::FETCH_NUM);
        $entriesCount = $row[0];

		//UPDATE RECORD
        if($entriesCount>0 and !$forceCreate){

            //set the modificator user
	        if(isset($mf->currentUser)) {
		        $this->data['modificator_uid']['value']   = $mf->currentUser->data['uid']['value'];
		        $this->data['modificator_class']['value'] = get_class( $mf->currentUser );
	        }
	        else if($mf->mode=='frontend'){
		        $this->data['modificator_uid']['value']   = 0;
		        $this->data['modificator_class']['value'] = 'unidentified (frontend)';
	        }
	        else{
		        $this->data['modificator_uid']['value']   = 0;
		        $this->data['modificator_class']['value'] = 'unidentified (backend)';
	        }

            if($updateDates){
                //adjust the modification date
                $date = new DateTime('NOW');
                $this->data['modification_date']['value'] = $date->format($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']);
            }

            //execute custom user code prior recording
            if($this->enablePreStore){
            	$this->preStore();
	            $this->playHooks('preStore');
            }


            //store record history
            if($this->enableHistory){
                $currentData = $this->data;

                //we do not want to have these keys processed in the diff()
                unset($currentData['history_last']); // history_last = full record as it was on last store() operation
                unset($currentData['history_past']); // history_past = stores the differences between each store() operation

	            //we do not process dummy field types
	            foreach($currentData as $key => $data){
	            	if(in_array($data['dataType'],self::$dummyFieldTypes)){
			            unset($currentData[$key]);
		            }
	            }

	            //keep only value fields
	            $currentData = self::stripTemplateInfo($currentData);

//	            echo "HISTORY_LAST<br />";
//	            print_r($this->data['history_last']['value']);
//
//	            echo chr(10).chr(10)."CURRENT DATA<br />";
//	            print_r($currentData);

                //add current differences to history log. The index is a timestamp using microtime().
                if(is_array($this->data['history_last']['value'])){

                	if(!is_array($this->data['history_past']['value']))$this->data['history_past']['value']=array();

                	$history_last = $this->data['history_last']['value'];

                    $this->data['history_past']['value'][(string)microtime(true)] = self::diff($history_last,$currentData);
                    ksort($this->data['history_past']['value'], SORT_NUMERIC);
                    //var_dump($this->data['history_past']['value']);

	             //   echo "<br /><br />HISTORY_PAST<br />";
	             //   print_r($this->data['history_past']['value']);

                    $this->data['history_past']['value'] = serialize($this->data['history_past']['value']);
                }





	            //store the last version of the record in key 'history_last' for next record compare
                $this->data['history_last']['value'] = serialize(self::stripTemplateInfo($currentData));

            }


            //prepare SQL

            //if entry exists, UPDATE
            $sql="UPDATE ".static::getTableName()." SET ";

            foreach($this->data as $keyName => $keyData){

                if(isset($this->data[$keyName])){


                    if(isset($this->data[$keyName]['dataType'])){
                        $dataType = explode(' ',trim($this->data[$keyName]['dataType']));

                        if(!in_array($dataType,self::$dummyFieldTypes)){
                            if(!in_array($dataType[0],self::$dummyFieldTypes)){ //trim dummy records

	                            //set db field
	                            $sql .= $this->getKeySQL($keyName, $keyData);

                            }
                        }

                    }
                    else{
                        echo "Record ".get_class($this)." : key ".$keyName." has no property 'dataType' set. You must specify one\n<br />";
                        print_r($this->data[$keyName]);
                        die();
                    }
                }
                else{
                    echo "Record ".get_class($this)." : key ".$keyName." does not exist.";
                    die();
                }
            }
            $sql = rtrim($sql,", ");
            $sql .= " WHERE uid = ".$pdo->quote($this->data['uid']['value']).";";

            if($debug)$logger->debug("dbRecord->store()/UPDATE: SQL=".$sql.'<br>');

            try{

				$updateStmt = $pdo->query($sql);

                if($debug) {
	                ob_start();
	                echo chr(10);

	                $updateStmt->debugDumpParams();
	                echo chr(10);
	                var_dump($pdo->errorInfo());
	                echo chr(10);

	                debug_print_backtrace();
	                $logger->debug(ob_get_clean());
                }

                $status = intval($this->data['uid']['value']);

                //execute custom user code after recording
                if($this->enablePostStore){
                	$this->postStore();
	                $this->playHooks('postStore');
                }

            }
            catch(PDOException $e){

	            ob_start();
	            echo "dbRecord->store()/UPDATE ERROR:".chr(10).$e->getMessage().chr(10).
	                 "ERROR=".$pdo->errorInfo()[2].chr(10).
	                 "SQL=".$sql.chr(10).chr(10);

	            debug_print_backtrace();

	            $logger->critical(ob_get_clean());

                $status=0;
            }

	        if($this->enableHistory){
		        //unserialize those fields so they can be read if accessing the record again after the store
		        $this->data['history_past']['value'] = unserialize($this->data['history_past']['value']);
		        $this->data['history_last']['value'] = unserialize($this->data['history_last']['value']);
	        }

        }

        //CREATE RECORD
        else{
            //if no entry exists, CREATE

	        //set the modificator user
	        if(isset($mf->currentUser)) {
		        //set the creator user
		        $this->data['creator_uid']['value'] = $mf->currentUser->data['uid']['value'];
		        $this->data['creator_class']['value'] = get_class($mf->currentUser);

		        //set the modificator user
		        $this->data['modificator_uid']['value'] = $mf->currentUser->data['uid']['value'];
		        $this->data['modificator_class']['value'] = get_class($mf->currentUser);
	        }
	        else if($mf->mode=='frontend'){

		        $this->data['creator_uid']['value'] = 0;
		        $this->data['creator_class']['value'] = 'unidentified (frontend)';

		        $this->data['modificator_uid']['value']   = 0;
		        $this->data['modificator_class']['value'] = 'unidentified (frontend)';
	        }
	        else{

		        $this->data['creator_uid']['value'] = 0;
		        $this->data['creator_class']['value'] = 'unidentified (backend)';

		        $this->data['modificator_uid']['value']   = 0;
		        $this->data['modificator_class']['value'] = 'unidentified (backend)';
	        }
            if($updateDates){

                //adjust the creation date
                $date = new DateTime('NOW');
                $this->data['creation_date']['value'] = $date->format($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']);
                //modification date is the same as creation date
                $this->data['modification_date']['value'] = $this->data['creation_date']['value'];

                //echo "\n\$this->data['creation_date']['value']=".$this->data['creation_date']['value'];
            }

            //execute custom user code prior recording
            if($this->enablePreCreate){
            	$this->preCreate();
	            $this->playHooks('preCreate');
            }


	        //store record history
	        if($this->enableHistory){
		        $currentData = $this->data;

		        //we do not process dummy field types
		        foreach($currentData as $key => $data){
			        if(in_array($data['dataType'],self::$dummyFieldTypes)){
				        unset($currentData[$key]);
			        }
		        }

		        $currentData = self::stripTemplateInfo($currentData);

//		        echo "HISTORY_LAST<br />";
//		        print_r($currentData);

		        //add current differences to history log. The index is a timestamp using microtime().
		        if(is_array($this->data['history_last']['value'])){
			        $this->data['history_past']['value'][(string)microtime(true)] = self::diff($this->data['history_last']['value'],$currentData);
			        ksort($this->data['history_past']['value'], SORT_NUMERIC);
			        //var_dump($this->data['history_past']['value']);

//			        echo "<br /><br />HISTORY_PAST<br />";
//			        print_r($this->data['history_past']['value']);

			        $this->data['history_past']['value'] = serialize($this->data['history_past']['value']);
		        }

			    //store the last version of the record in key 'history_last' for next record compare
		        $this->data['history_last']['value'] = serialize($currentData);
            }






            //prepare SQL
            $sql="INSERT INTO ".static::getTableName()." SET ";

	        if(static::$enableMultiDBHosts){
		        //get current max uid
		        $sqlUid = "SELECT MAX(uid) FROM ".static::getTableName()." WHERE 1";
		        $stmt = $pdo->query($sqlUid);
		        $maxUid = $stmt->fetchColumn();

		        $mod = $maxUid/static::$autoIncrementIncrement;
		        $nextUid = floor($mod)*static::$autoIncrementIncrement + static::$autoIncrementIncrement + static::$autoIncrementOffset;

		        $sql .= "`uid`=".$nextUid.", ";
	        }

            foreach($this->data as $keyName => $keyData){

                if(isset($this->data[$keyName]['dataType'])){

                    $dataType = explode(' ',trim($this->data[$keyName]['dataType']));

                    if(!in_array($dataType,self::$dummyFieldTypes)){
                        if (!in_array($dataType[0], self::$dummyFieldTypes)) {//trim dummy records

	                        //set db field
                            $sql .= $this->getKeySQL($keyName, $keyData);
                        }
                    }

                }
                else {
					//debug_print_backtrace();
					//print_r($this->data);
					die($l10n->getLabel('backend','mf_message').$l10n->getLabel('backend','missing_datatype1').' \''.$keyName.'\' '.$l10n->getLabel('backend','missing_datatype2').' \''.get_class($this).'\'');
				}
            }

            $sql = rtrim($sql,", ").';';


            if($debug)$logger->debug("dbRecord->store()/CREATE SQL=".$sql.'<br>');




            try{
                $createStmt = $pdo->query($sql);

                if($debug) {
	                ob_start();
	                echo chr(10);
                    $createStmt->debugDumpParams();
                    echo chr(10);
                    var_dump($pdo->errorInfo());
	                echo chr(10);
	                debug_print_backtrace();
	                $logger->debug(ob_get_clean());
                    //echo chr(10).chr(10);

                }

                $status = $this->data['uid']['value'] = $pdo->lastInsertId();

                //execute custom user code after recording
                if($this->enablePostCreate){
                	$this->postCreate();
	                $this->playHooks('postCreate');
                }
            }
            catch(PDOException $e){
	            ob_start();
	            echo "dbRecord->store()/CREATE ERROR:".chr(10).$e->getMessage().chr(10).
	                 "ERROR=".$pdo->errorInfo()[2].chr(10).
	                 "SQL=".$sql.chr(10).chr(10);

	            debug_print_backtrace();

	            $logger->critical(ob_get_clean());

                $status=0;
            }

	        if($this->enableHistory){
		        //unserialize those fields so they can be read if accessing the record again after the store
		        $this->data['history_last']['value'] = unserialize($this->data['history_last']['value']);
	        }

        }

	    //restoring current locale if required.
	    if(isset($forceLocale)){
		    $mf->setDataEditingLanguage($currentLocale);
	    }

        return $status;
    }

    /**
     * Returns the SQL command for storing a given key from the data array, processing the values : dates are converted from localized format to SQL format, slug texts are slugified, arrays are serialized
     * @param $keyName string the name of the key
     * @param $keyData array the data associated with the key
     * @return string the sql segment prepared for storing the given key
     */
    function getKeySQL($keyName, &$keyData){
        global $mf,$l10n,$pdo,$logger;

        $locale = $mf->getLocale();
        $sql="";

      /*  if($keyName=='notes'){
            echo "notes=".chr(10);
            print_r($keyData);
            echo "isset(\$keyData['value'])=".isset($keyData['value']).chr(10);
            echo "isset(\$keyData['value'])==false".(isset($keyData['value'])==false).chr(10);
            echo "\$keyData['value'])==''".($keyData['value']=='').chr(10);
            echo "class=".get_class($this).chr(10);
            debug_print_backtrace();
            echo chr(10).chr(10);
            $val = '';
            echo "isset(\$val)=".isset($val).chr(10);
            echo "var_dump(\$keyData['value'])=";
            var_dump($keyData['value']);
        }*/

        //if(isset($keyData['value']) ){
        if(array_key_exists('value',$keyData) ){
            if(isset($keyData['dataType'])){

                $dataType = explode(' ',trim($keyData['dataType']));

                if(!in_array($dataType[0],self::$dummyFieldTypes)){

                	//specify uid only if already defined in record, else autoincrement
                    if($keyName != 'uid' or (isset($keyData) && $keyData['value'] != "")){

                        //echo "key=".$keyName." value=".$item['value']."<br/>\n";

                        if($dataType[0]=='date' || $dataType[0]=='datetime' || $dataType[0]=='datetime.ms' || $dataType[0]=='time'){
	                        $date = dateToSQL($keyData['value'], $keyData['dataType'], $locale);
                            $sql .= '`'.$keyName."`= ".(($keyData['value'] == null || $keyData['value'] == 'null' || $keyData['value'] == 'NULL' || $date==null)?'NULL':$pdo->quote($date)).", ";
                        }
                        else if($dataType[0]=='files'){

                            /**************************************************
                             *   MindFlow < v1.4 data conversion
                             *************************************************/
                            if($keyData['value']!='' && !is_array($keyData['value'])){
                                $keyData['value'] = $this->convertFilesValue($keyData['value']);
                            }
                            //END MindFlow < v1.4 data conversion
                            else{
	                            //performance fix : store width and height with the image to avoid parsing them from the file every time they are required
	                            if(is_array($keyData['value'])){
		                            foreach($keyData['value'] as &$image){
			                            if(!isset($image['width']) || ( isset($image['width']) && $image['width'] != '')){

				                            $imgExtensions = array( 'gif', 'jpg', 'jpeg', 'png' );

				                            $fileExtPart = strrchr ($image['filename'], '.');

				                            if ( in_array( ltrim( $fileExtPart, '.' ), $imgExtensions ) ) {

					                            list( $width, $height, $type, $attr ) = @getimagesize( DOC_ROOT . $mf->getUploadsDir() . DIRECTORY_SEPARATOR . $image['filepath'] . $image['filename'] );
					                            $image['width']  = $width;
					                            $image['height'] = $height;
				                            }
			                            }
		                            }
	                            }
                            }

                            $sql .= '`'.$keyName."`= ".$pdo->quote(serialize($keyData['value'])).", ";
                        }

//                        else if ($keyName=='slug' && isset($keyData['value']) || $dataType[0]=='slug') {
//                            //make sure a slug has a unique value when a key named 'slug' is defined
//                            $sql .= '`'.$keyName."`= ".$pdo->quote($this->makeSlug($keyData['value'])).", ";
//                        }
                        else if($dataType[0]=='template_data'){
                            /**************************************************
                             *   MindFlow < v1.4 data conversion
                             *************************************************/
                            foreach($keyData['value'] as $template_key =>$template_data){
                                if(isset($template_data['dataType']) && $template_data['dataType']=='microtemplate'){
                                    foreach($template_data['value'] as $mtKey => $mtValue){
                                        if(isset($mtValue['microtemplate_data'])){
                                            foreach($mtValue['microtemplate_data'] as $mtDataKey => $mtDataValue){
                                                if(isset($mtDataValue['dataType']) && substr($mtDataValue['dataType'],0,5)=='files'){

                                                    if($mtDataValue['value']!='' && !is_array($mtDataValue['value'])){
                                                        //print_r($keyData['value'][$template_key]['value'][$mtKey]['microtemplate_data'][$mtDataKey]['value']);
                                                        $keyData['value'][$template_key]['value'][$mtKey]['microtemplate_data'][$mtDataKey]['value'] = $this->convertFilesValue($mtDataValue['value']);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if(isset($template_data['dataType']) && substr($template_data['dataType'],0,5)=='files'){

                                    if($template_data['value']!='' && !is_array($template_data['value'])){
                                        $keyData['value'][$template_key]['value'] = $this->convertFilesValue($template_data['value']);
                                    }
                                }
                            }
                            //END MindFlow < v1.4 data conversion

                            //cleanup template information and keep only the values
                            $sql .= '`'.$keyName."`= ".$pdo->quote(serialize(self::stripTemplateInfo($keyData['value']))).", ";
                        }

                        else {
                            if(is_array($keyData['value'])){
                                $sql .= '`'.$keyName."`= ".$pdo->quote(serialize($keyData['value'])).", ";
                            }
                            else {
                                $sql .= '`'.$keyName."`= ".$pdo->quote($keyData['value']).", ";
                            }
                        }
                    }
                }
            }
        }
        else {
            $sql = '';
            $mf->addWarningMessage('dbRecord->getKeySQL() : '.$l10n->getLabel('dbRecord','key').' '.$keyName.' '.$l10n->getLabel('dbRecord','no_attr').' "value"');
            //echo('dbRecord->getKeySQL() : '.$l10n->getLabel('dbRecord','key').' '.$keyName.' '.$l10n->getLabel('dbRecord','no_attr').' "value"'.chr(10));
        }
        return $sql;
    }


    /**
     * desactivates a record in the database. The record can be recovered by switching its deleted field back to 0.
     */
    function delete(){

        if($this->enablePreDelete){
        	$this->preDelete();
	        $this->playHooks('preDelete');
        }
        $this->data['deleted']['value'] = '1';
        $this->store();
        if($this->enablePostDelete){
        	$this->postDelete();
	        $this->playHooks('postDelete');
        }
    }

    /**
     * reactivates a record in the database.
     */
    function restore(){
        if($this->enablePreRestore){
        	$this->preRestore();
	        $this->playHooks('preRestore');
        }
        $this->data['deleted']['value'] = '0';
        $this->store();
        if($this->enablePostRestore){
        	$this->postRestore();
	        $this->playHooks('postRestore');
        }
    }

    /**
     * This will definitely erase the record from the database. Prefer delete() in order to be able to recover records.
     */
    function delete_forever(){
        global $mf,$pdo,$logger;

        //prepare SQL
        if($this->enablePreDelete)$this->preDelete();

        $sql="DELETE FROM ".static::getTableName()." WHERE uid=".$pdo->quote(intval($this->data['uid']['value']));
        $pdo->query($sql);

        //delete record directory and associated files
        $dir = DOC_ROOT.$mf->getUploadsDir().$this->getFilesDir();
        //$dir = DOC_ROOT.$mf->getUploadsDir().get_class($this).DIRECTORY_SEPARATOR.get_class($this).'_'.$this->data['uid']['value'];

        $logger->debug("Unlink directory ".$dir);
        $status = delTree($dir);
        $logger->debug(($status)?"Directory unlink successful":"Directory unlink failed");

        if($this->enablePostDelete)$this->postDelete();
    }


    /**
     * Turns to string a list of the values contained in the record
     * @return string
     */
    function __toString(){

        $string = get_class($this).' [ uid='.$this->data['uid']['value'].' ]'.chr(10).'{'.chr(10);

        foreach($this->data as $key => $keyData){
        	if($key != 'history_last' && $key != 'history_past') { //do not display history fields, they would add a lot of garbage to the output

        		foreach ( $keyData as $index => $value ) {
			        if ( $index == 'value' ) {
				        //ignore dummy fields
				        if ( ( isset( $this->data[ $key ]['dataType'] ) and ! in_array( $this->data[ $key ]['dataType'], self::$dummyFieldTypes ) ) ) {
					        //ignore keys with noEcho==1
					        if ( isset( $this->data[ $key ]['noEcho'] ) && filterBoolean( $this->data[ $key ]['noEcho'] ) == true ) {
						        continue;
					        }
					        //echo other keys to the string
					        if ( is_array( $value ) ) {
						        $string .= chr( 9 ) . $key . ' => ' . print_r( $value, true );
					        } else {
						        $string .= chr( 9 ) . $key . ' => ' . $value . chr( 10 );
					        }
				        }
			        }
		        }
	        }
        }
        $string.= '}'.chr(10);
        return $string;
    }

    /**
     * Strips all attributes in record keys, retaining only the 'value' attribute
     * This is especially usefull for diplaying the content of a record
     * @return array the stripped Array
     */
    function toArray(){

        $strippedData = array();

        foreach($this->data as $key => $keyData){
            foreach($keyData as $index => $value) {
                if ($index == 'value') {
                    if ((isset($this->data[$key]['dataType']) and !in_array($this->data[$key]['dataType'], self::$dummyFieldTypes))) {
                        $strippedData[$key]['value'] = $value;
                    }
                }
            }
        }
        return $strippedData;
    }

    /**
     * Outputs the record as html
     * @return string
     */
    function toHTML(){
        $html = array();
        $html[] = "<div>".chr(10);
        foreach($this->data as $key => $item){
            if((isset($this->data[$key]['dataType']) and !in_array($this->data[$key]['dataType'],self::$dummyFieldTypes)))
                $html[] = $key.' = '.$item['value']."<br>".chr(10);
        }
        $html[] = "</div>".chr(10);
        return implode(chr(10),$html);
    }

    /**
     * Outputs an HTML description of the record featuring the keys specified in $fieldsList, eventually translated using the the values supplied by $keyProcessors
     * @param $keysList The list of keys to output in the desired order
     * @param $keyProcessors An array of processing functions indexed by key name (as found in $keysList) for pre-processing values before displaying these. processing function definition must be located in the record's own class file
     * @return string
     */
    /*function displayReadOnly($fieldsList=array(), $keyProcessors=array()){
        $html = array();
        if(is_array($fieldsList)){
            if(sizeof($fieldsList)==0){
                //display all fields by default
                $html[] = "<div class='display_".get_class($this)."'>".chr(10);
                foreach($this->data as $key => $item){
                if((isset($this->data[$key]['dataType']) and !in_array($this->data[$key]['dataType'],self::$dummyFieldTypes)))
                $html[] = $key.' = '.$item['value']."<br>".chr(10);
                }
                $html[] = "</div>".chr(10);
                return implode(chr(10),$html);
            }
            else{
                //display user defined list of fields
            }
        }
        return implode(chr(10),$html);
    }*/











    /**
     * Outputs an HTML table of all the records available in this class
     * @param $request array The list of keys fetched from SQL, which will make the columns of the table. format must be array('SELECT' => '', 'FROM' => '', 'JOIN' => '', 'WHERE' => '', 'ORDER BY' => '', 'ORDER_DIRECTION' => '');
     * @param $moduleName string The name of the module or plugin displaying this record
     * @param $submoduleName string The name of the submodule or plugin displaying this record
     * @param $mainKey string The key of the field best describing the name of this record, used for deletion dialog : "Are you sure you want to delete the record $mainKey ?"
     * @param $keyProcessors array An array of processing functions indexed by key name (as found in $keysList) for pre-processing values before displaying these in the table. processing function definition must be located in the record's own class file
     * @param $page int requests displaying the page number given from the results list
     * @param $availableActions array lists the action icons available in the actions column. Default value is array('view'=>0,'edit'=>1,'delete'=>1). Additionally, a custom function can be supplied as a string for the value of 'view','edit','delete','sortUp','sortDown'.
     * This URL can contain some of the following markers that will be automatically substituted : {subdir}, {record_class}, {record_uid}, {module_id}, {submodule_id}, {your_custom_column_name} (this latest value has to exist as a valid column in the record table).
     * Example value for $availableActions :
     * array(
     *  'create' => 1,
     *  'view' => 'myClass::makeHTMLActionLink("viewFaq","{subdir}","{record_uid}")',
     *   'edit' => 'myClass::makeHTMLActionLink("editFaq","{subdir}","{record_uid}")',
     *   'delete' => 1,
     *   'sortUp' => 'myClass::makeJSONActionLink("sortUp","{subdir}","{record_uid}")',
     *   'sortDown' => 'myClass::makeJSONActionLink("sortDown","{subdir}","{record_uid}")',
     *   'clone' => '1',
     *   'copyPaste' => '1',
     *    'myCustomAction1' => 'myClass::makeJSONActionLink("customAction1","{subdir}","{record_uid}")',
     * ),
     * @param $advancedOptions array allows specifying more options using the following format
     *  array(
     *      'callBackOnRecordClose' => '' //runs the specified javascript code on closing a record created or edited from this recordEditTable
     *      'ajaxActions' => 0, //boolean processes action links as ajax requests rather than as regular links
     *      'buttons' => '' //string allows inserting some custom HTML code on top of the record edit table, usualy a create new record button
     *      'hideColumns' => 'column1,column2', //string coma separated list of field names. removes from the recordEditTable display some columns specified in the SELECT of the request. This can be usefull if you need to have some field value retrevied for usage in a keyprocessor function, but you do not want it to be displayed
     *      'forceNumRowsPerPage' => '32', //int overrides the default number of rows per page
     *      'forceDistinct' => 'false', //forces the addition of the keyword distinct in very front of the request when required and a table alias is used
     *      //'showNumRowsPerPageSelector' => true, //boolean displays a selector allowing the user to set the number of rows per page
     *      'showResultsCount' => true, //boolean shows the total number of results
     *      'listRecordsInAllLanguages' => true, //force listing the records not only from current language, but from all languages
     *      'columnClasses' => array( //adds custom CSS classes to array columns. Useful for responsive design or other purpose.
     *          'columnIndex1' => 'columnClassName',
     *          'columnIndex2' => 'columnClassName',
     *      ),
     *      'debugSQL' => 0 //show SQL request
     *      'showPrint' => true, //displays print button
     *      'showRefresh' => true, //displays refresh button
     *      'buttonClass' => 'btn-xs', //adds thespecified class to the recordEditTable displayed buttons. Mainly for altering the aspect of buttons.
     *      'printMode' => true, //disables the display of buttons useless for printed version arround the record table
     *      'editRecordButtons' => array( //set available buttons in edit / create record forms opened in AJAX from the showRecordEditTable view
     *          'showSaveButton' => true,
     *          'showSaveCloseButton' => true,
     *          'showPreviewButton' => true,
     *          'showCloseButton' => true
     *      ),
     *      'formAttributes' = array(
     *          'method'=>'POST',
     *          //'onsubmit'=>'sendForm();',
     *          'role'=>'form',
     *          'class'=>'form-horizontal',
     *          'enctype'=>'x-www-form-urlencoded'
     *      ),
     *      'actionOnTitleClick' => 'edit', //selects edit or view action when clicking the link on a record's title in the showRecordEditTable
     *      'copyPasteBuffer' => '1,2,9', //holds a list of uids to put in the copyPasteBuffer. This value is often filled automatically when calling a re-display of the current recordEditTable
     *      //When pasting items to a recordEditTable shown as a field inside a record's form, we need to point the parent columns from the pasted items to the record.
     *      //Sometimes the developer may not want to use the regular 'parent_class' and 'parent_uid' columns, so he is able to customize the target columns with the settings below
     *      'copyPasteToParentClass' => 'parent_class', //sets the name of the parent class into the record
     *      'copyPasteToParentUid' => 'parent_uid',     //field the value of the indicated column with the parent's uid
     *  )
     * @param $recordEditTableID string forces the recordEditTableID in order to keep it the same when refreshing an existing recordEditTable
     * @return string
     */


     function showRecordEditTable($request, $moduleName, $subModuleName, $mainKey,   $keyProcessors = array(), $page = NULL, $availableActions = array('create'=>1,'view'=>0,'edit'=>1,'delete'=>1,'clone'=>0), $advancedOptions=array('ajaxActions' => 0, 'showPrint' => false, 'showResultsCount' => true), $recordEditTableID = null){

        global $mf,$l10n,$pdo,$config,$logger;

        $recordClass = get_class($this);

        //create an instance in order to access some static fields
        $record = new $recordClass();

        //make sure we have a recordEditTableID
        if(!isset($recordEditTableID))$recordEditTableID = mt_rand();

        // we need a truly unique ID for sessions as multiple instances of the same recordEditTableID may be shown in different tabs of the browser
	    // we will store in the session all of the unmodified parameters of the recordEditTable, and thus avoid to transmit them during GET requests for refreshing the recordEditAble display
	    $session_recordEditTableID = $recordEditTableID.'_'.mt_rand();

        $language = $mf->getDataEditingLanguage();

        $html = array();

        //buttons HTML displayed on top of the showRecordEditTable
        $buttons='';

        //Default values for request
        if(!isset($request['SELECT']) || trim($request['SELECT']) == '') $request['SELECT'] = '*';

        if(!isset($request['FROM'])  || trim($request['FROM']) == '') $request['FROM'] = static::getTableName();

        $fromDef = explode(' ',$request['FROM']);
        //$tableName = $fromDef[0];

        if(sizeof($fromDef)==2){
         $alias = $fromDef[1];
        }
        else $alias = $fromDef[0];

        if(!isset($request['JOIN'])) $request['JOIN'] = '';

        if(!isset($request['WHERE'])) $request['WHERE'] = '';

        if(!isset($request['ORDER DIRECTION'])) $request['ORDER DIRECTION'] = 'ASC';

        if(!isset($request['ORDER BY']) || $request['ORDER BY'] == '') $request['ORDER BY'] = $alias.'.uid';


        //values adaptation
        $customWhereSupplement = '';
        if(trim($request['WHERE']) != '') $customWhereSupplement = ' AND ';

        $customJoinSupplement = '';
        if(trim($request['JOIN']) != '') {
            $customJoinSupplement = ' ';
        }

        if(isset($advancedOptions['hideColumns']))$hideColumns = explode(',',$advancedOptions['hideColumns']);
        else $hideColumns = array();

        if(isset($advancedOptions['actionOnTitleClick']))$actionOnTitleClick = $advancedOptions['actionOnTitleClick'];
        else $actionOnTitleClick = 'view';

         if(isset($advancedOptions['forceDistinct']) && $advancedOptions['forceDistinct']==true) $distinct = "DISTINCT ";
         else $distinct = '';

        if(isset($advancedOptions['columnClasses']) && sizeof($advancedOptions['columnClasses'])>0)$useColumnClasses=true;
        else $useColumnClasses=false;


        if(!isset($advancedOptions['debugSQL']))$advancedOptions['debugSQL']=0;
        if($advancedOptions['debugSQL']==false)$advancedOptions['debugSQL']=0;
        if($advancedOptions['debugSQL']==true)$advancedOptions['debugSQL']=1;

        if(!isset($advancedOptions['ajaxActions']))$advancedOptions['ajaxActions']=0;
        if($advancedOptions['ajaxActions']==false)$advancedOptions['ajaxActions']=0;
        if($advancedOptions['ajaxActions']==true)$advancedOptions['ajaxActions']=1;

        if(!isset($advancedOptions['showPrint']))$advancedOptions['showPrint']=0;
        if($advancedOptions['showPrint']==false)$advancedOptions['showPrint']=0;
        if($advancedOptions['showPrint']==true)$advancedOptions['showPrint']=1;

        if(!isset($advancedOptions['printMode']))$advancedOptions['printMode']=0;
        if($advancedOptions['printMode']==false)$advancedOptions['printMode']=0;
        if($advancedOptions['printMode']==true)$advancedOptions['printMode']=1;

	     if(!isset($advancedOptions['showRefresh']))$advancedOptions['showRefresh']=0;
	     if($advancedOptions['showRefresh']==false)$advancedOptions['showRefresh']=0;
	     if($advancedOptions['showRefresh']==true)$advancedOptions['showRefresh']=1;

	     if(!isset($advancedOptions['buttonClass']))$advancedOptions['buttonClass']='';

	     if(!isset($advancedOptions['callBackOnRecordClose']))$advancedOptions['callBackOnRecordClose']='';

        if(isset($advancedOptions['listRecordsInAllLanguages']) && $advancedOptions['listRecordsInAllLanguages']==true){
            $customWhereSupplement = '';
            $language='';
        }
        else {
            $language = $alias.".language='".$language."'";
        }

        //init copy/paste buffer
	    //if advanced option setting not set, set empty value
	    if(!isset($advancedOptions['copyPasteBuffer']))$advancedOptions['copyPasteBuffer']='';

        // Sometimes the developer may not want to use the regular 'parent_class' and 'parent_uid' columns to indicate to which record the pasted record is linked to
	    // so he is able to customize the target columns
	    if(!isset($advancedOptions['copyPasteToParentClassColumn']))$advancedOptions['copyPasteToParentClassColumn']='parent_class';
	    if(!isset($advancedOptions['copyPasteToParentUidColumn']))$advancedOptions['copyPasteToParentUidColumn']='parent_uid';
	    if(!isset($advancedOptions['copyPasteToParentClass']))$advancedOptions['copyPasteToParentClass']='';
	    if(!isset($advancedOptions['copyPasteToParentUid']))$advancedOptions['copyPasteToParentUid']='';

	    if($advancedOptions['copyPasteBuffer'] != '' )
			//if value is not empty, extract it as array
		    $copyPasteBuffer = explode( ',', $advancedOptions['copyPasteBuffer'] );

	    else $copyPasteBuffer = array();

		//init ou session value which will allow to pass the value between recordEditTable displays when paging
        if(!isset($_SESSION['RET_copyPasteBuffer']))$_SESSION['RET_copyPasteBuffer']=array();
        if(!isset($_SESSION['RET_copyPasteBuffer'][$recordEditTableID]))$_SESSION['RET_copyPasteBuffer'][$recordEditTableID]=array();

        //if some new entries have been added to the copyPasteBuffer from updateRecordEditTable JSON request or function parameter, process them and add them to our session based selection
        if(count($copyPasteBuffer) > 0 ) $_SESSION['RET_copyPasteBuffer'][$recordEditTableID] = array_merge($_SESSION['RET_copyPasteBuffer'][$recordEditTableID],$copyPasteBuffer);
        else $_SESSION['RET_copyPasteBuffer'][$recordEditTableID] = array();

        //reset javascript copy/paste buffer when displaying a new recordEditTable
	    if($advancedOptions['copyPasteBuffer'] == '')



        //wether or not the buttons at the bottom of the record editing form should be displayed
	    // when opening an object's editing dialog from the recordEditTable
        if($advancedOptions['ajaxActions']==1) {
            if(!isset($advancedOptions['editRecordButtons'])){
                $advancedOptions['editRecordButtons'] = array(
                   'showSaveButton' => '1',
                   'showSaveCloseButton' => '1',
                   'showPreviewButton' => '0',
                   'showCloseButton' => '1',
                   'showCloneButton' => '1',
                   'showHistoryButton' => ($record->enableHistory)?'1':'0',
                );
            }
            else{
                if(!isset($advancedOptions['editRecordButtons']['showSaveButton']))$advancedOptions['editRecordButtons']['showSaveButton']='1';
                if(!isset($advancedOptions['editRecordButtons']['showSaveCloseButton']))$advancedOptions['editRecordButtons']['showSaveCloseButton']='1';
                if(!isset($advancedOptions['editRecordButtons']['showPreviewButton']))$advancedOptions['editRecordButtons']['showPreviewButton']='1';
                if(!isset($advancedOptions['editRecordButtons']['showCloseButton']))$advancedOptions['editRecordButtons']['showCloseButton']='1';
                if(!isset($advancedOptions['editRecordButtons']['showHistoryButton']))$advancedOptions['editRecordButtons']['showHistoryButton']='0';

	            //we need to make sure the values are string otherwise the form secure test will fail
	            if($advancedOptions['editRecordButtons']['showSaveButton']==1)
		            $advancedOptions['editRecordButtons']['showSaveButton']='1';
	            else
		            $advancedOptions['editRecordButtons']['showSaveButton']='0';


	            if($advancedOptions['editRecordButtons']['showSaveCloseButton']==1)
		            $advancedOptions['editRecordButtons']['showSaveCloseButton']='1';
	            else
		            $advancedOptions['editRecordButtons']['showSaveCloseButton']='0';


	            if($advancedOptions['editRecordButtons']['showPreviewButton']==1)
		            $advancedOptions['editRecordButtons']['showPreviewButton']='1';
	            else
		            $advancedOptions['editRecordButtons']['showPreviewButton']='0';


	            if($advancedOptions['editRecordButtons']['showCloseButton']==1)
		            $advancedOptions['editRecordButtons']['showCloseButton']='1';
	            else
		            $advancedOptions['editRecordButtons']['showCloseButton']='0';

	            if($advancedOptions['editRecordButtons']['showHistoryButton']==1)
		            $advancedOptions['editRecordButtons']['showHistoryButton']='1';
	            else
		            $advancedOptions['editRecordButtons']['showHistoryButton']='0';
            }
        }




		//composing action buttons displayed on top of the recordEditTable
	     if(isset($availableActions['create'])){
           if($availableActions['create'] == 1){

               $secKeys = array(
               	    'action' => 'createRecord',
                    'record_class' => $recordClass,
                    'mfSetLocale' => $mf->getLocale()
               );
	           $sec = getSec($secKeys);

               $buttons .= '<button class="btn-sm btn-primary mf-btn-new '.$advancedOptions['buttonClass'].'" type="button" onClick="openRecord(\'{subdir}/mf/core/ajax-core-html.php?'.$sec->parameters.'&recordEditTableID='.$recordEditTableID.'&header=0&footer=0&sec='.$sec->hash.'&fields='.$sec->fields.'&ajax=1&showSaveButton=1&showSaveCloseButton=1&showPreviewButton=0&showCloseButton=1&mode='.$mf->mode.'\',\'90%\',\''.$advancedOptions['callBackOnRecordClose'].'\',\''.str_replace("'","\\'",$l10n->getLabel($recordClass,'record_name')).'\');"><i class="glyphicon glyphicon-plus"></i> '.$l10n->getLabel($recordClass,'new_record').'</button> ';

           }
           else if($availableActions['create'])$buttons .= $availableActions['create'];
        }

	     if(isset($availableActions['copyPaste'])){
		     if($availableActions['copyPaste'] == 1){

			     $buttons .= '<button class="btn-sm mf-btn-new '.$advancedOptions['buttonClass'].'" type="button" onClick="copyRecords_'.$recordEditTableID.'()" title="'.$l10n->getLabel('backend','copy_entry').'"><i class="glyphicon glyphicon-copy"></i> </button> ';
			     $buttons .= '&nbsp;<button class="btn-sm mf-btn-new '.$advancedOptions['buttonClass'].'" type="button" onClick="pasteRecords_'.$recordEditTableID.'()" title="'.$l10n->getLabel('backend','paste_entry').'"><i class="glyphicon glyphicon-paste"></i> </button> ';
			     $buttons .= '&nbsp;<span id="fieldStatus_'.$recordEditTableID.'"></span>';

		     }
		     else if($availableActions['copyPaste'])$buttons .= $availableActions['copyPaste'];


		     //copy link
		     $secKeys = array(
			     'action' => 'copyRecordsFromRET',
			     'record_class' => $recordClass,
			     'recordEditTableID' => $recordEditTableID,
			     'mode' => $mf->mode,
			     'mfSetLocale' => $this->data['language']['value'],
		     );
		     $sec = getSec($secKeys);


		     $html[] = '<script id="script_a_'.$recordEditTableID.'" type="text/javascript">
		     
		                   
							function toggleCopy_'.$recordEditTableID.'(checkboxUid,recordUid){
							
							console.log("toggleCopy_'.$recordEditTableID.'("+checkboxUid+","+recordUid+")");
								
								isChecked = $("#"+checkboxUid).prop("checked");
								
							console.log("isChecked="+isChecked);
								
								if(isChecked){ 
								console.log("1");
									//add recordUid to javascript copyPaste buffer if not already present
									if(jQuery.inArray(recordUid,lastDisplay_' . $recordEditTableID . '["copyPaste"]) === -1){
								console.log("2");
												lastDisplay_' . $recordEditTableID . '["copyPaste"].push(recordUid);
									}	
								}	
								else {
								console.log("3");
									//remove item from javascript copyPaste buffer if present
								    if(jQuery.inArray(recordUid,lastDisplay_' . $recordEditTableID . '["copyPaste"]) !== -1){
								        console.log("4");
								        var index = lastDisplay_' . $recordEditTableID . '["copyPaste"].indexOf(recordUid);
								        
								        if (index > -1) {
										  lastDisplay_' . $recordEditTableID . '["copyPaste"].splice(index, 1);
										}
								    }
								}    
								console.log("recordEditTable_'.$recordEditTableID.' copy buffer = " + lastDisplay_'.$recordEditTableID.'["copyPaste"].join(","));
								console.log("");    
							}
                            
                            function copyRecords_'.$recordEditTableID.'(){
                                
                                //var checkedBoxes = $("#recordEditTable_'.$recordEditTableID.' input:checkbox:checked");
                                
                                selectedUids = lastDisplay_'.$recordEditTableID.'["copyPaste"].join(",");

                               // $("#fieldStatus_'.$recordEditTableID.'").html(jsonData.count+" '.$l10n->getLabel('backend','elements_copied').'").fadeIn( 100 ).delay( 3000 ).fadeOut( 3000 );

                                var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", {action: "copyRecordsFromRET",record_class: "'.$recordClass.'",recordEditTableID: "'.$recordEditTableID.'",copy_uids: selectedUids,mode: "'.$mf->mode.'", mfSetLocale: "'.$this->data['language']['value'].'",sec:"'.$sec->hash.'",fields:"'.$sec->fields.'"})
                                .success(function(jsonData) {
                                    //display count of records copied
                                    $("#fieldStatus_'.$recordEditTableID.'").html(jsonData.nbElements+" '.$l10n->getLabel('backend','elements_copied').'").fadeIn( 100 ).delay( 3000 ).fadeOut( 3000 );
                                    
                                    console.log("lastDisplay_' . $recordEditTableID . '[\'copyPaste\']="+lastDisplay_' . $recordEditTableID . '["copyPaste"]);
                                    
                                });

                            }';

		     $locale= ($this->data['language']['value'] != '')?$this->data['language']['value']:$mf->getLocale();
		     //paste link
		     $secKeys = array(
			     'action' => 'pasteRecordsToRET',
			     'record_class' => $recordClass,
			     'mode' => $mf->mode,
			     'session_recordEditTableID' => $session_recordEditTableID,
			     'mfSetLocale' => $locale,
		     );
		     $sec = getSec($secKeys);



		     $html[] = '
                            function pasteRecords_'.$recordEditTableID.'(){
                                
                                var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", {action: "pasteRecordsToRET",record_class: "'.$recordClass.'",recordEditTableID: "'.$recordEditTableID.'", copyPasteToParentClass:  "'.$advancedOptions["copyPasteToParentClass"].'", copyPasteToParentUid: "'.$advancedOptions["copyPasteToParentUid"].'", mode: "'.$mf->mode.'", session_recordEditTableID: "'.$session_recordEditTableID.'", mfSetLocale: "'.$locale.'", sec:"'.$sec->hash.'",fields:"'.$sec->fields.'"})
                                .success(function(jsonData) {
                                    if(jsonData.result=="success"){
                                    
                                        //add the new rows to our recordEditTable
                                        $("#recordEditTable_'.$recordEditTableID.' tbody").prepend( jsonData.pasted_html );
                                        
                                        //display count of records pasted
                                        $("#fieldStatus_'.$recordEditTableID.'").html(jsonData.nbElements+" '.$l10n->getLabel('backend','elements_pasted').'").fadeIn( 100 ).delay( 3000 ).fadeOut( 3000 );
                                        
                                        //empty copy buffer
	                                    lastDisplay_' . $recordEditTableID . '["copyPaste"] = new Array();
	                                    console.log("lastDisplay_' . $recordEditTableID . '[\'copyPaste\']="+lastDisplay_' . $recordEditTableID . '["copyPaste"]);
	                                    
	                                    //uncheck checkboxes
	                                    $("#recordEditTable_'.$recordEditTableID.' input:checkbox").prop("checked", false);
                                    }
                                    else{
                                        alert(jsonData.message);
                                    }

                                });
                            }
                            </script>';
	     }
	     else $availableActions['copyPaste'] = 0;

         //add user supplied buttons
         if(isset($advancedOptions['buttons'])) {
             $buttons .= $advancedOptions['buttons'];
         }

        //echo "class dbRecord.php init() showRecordEditTable() classname=".$recordClass." moduleName =".$moduleName." subModuleName =".$subModuleName." tableName =".static::getTableName()."<br />";

        $maxRecordsPerPage = (isset($advancedOptions['forceNumRowsPerPage']))?$advancedOptions['forceNumRowsPerPage']:((isset($config['max_records_per_page']))?$config['max_records_per_page']:30);

        $page = ( (isset($page))? $page : (isset($_REQUEST['page'])? intval($_REQUEST['page']) : 0 ) );
        //we need to know the total record number in the table for displaying the pager
        $sql = "SELECT COUNT(*) FROM ".$request['FROM'].$customJoinSupplement.$request['JOIN']." WHERE ".$request['WHERE'].$customWhereSupplement.$language;

        if($advancedOptions['debugSQL'])
            $html[] = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$sql.'</div>';

        try{
            $stmt = $pdo->query($sql);
        }
        catch(PDOException $e){
            $html[] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
            "<strong>Message =</strong> ".$e->getMessage()."<br/>".
            "<strong>SQL =</strong> ".$sql."<br/>".
            '</div>';
            return implode(chr(10),$html);
        }
        $totalRecordCount = $stmt->fetchColumn();




         //inserting div for creating/displaying dialogs if not available in the page.
        $html[] = '
                    <div id="dbr_dialogHolder"></div>
                    <script>
                    if($("#mf_dialogs").length == 0) {
                        //document.write("<div id=\"mf_dialogs\"></div>");
                        document.getElementById("dbr_dialogHolder").innerHTML="<div id=\"mf_dialogs\"></div>";
                    }
                    </script>';

         $html[] = '<div id="record_dialog"></div>';


         $sec = getSec(array(
             'action' => 'deleteRecord',
             'mode' => $mf->mode,
             'mfSetLocale' => $this->data['language']['value'],
         ));

         $html[] = '
            <script id="script_b_'.$recordEditTableID.'" >
                var deleteRecordUid = -1;
                var deleteURL = "";
                var useAjax = 0;
                var recordDialog;
                var recordOpenMethod="dialog";  //"dialog" for jQuery UI dialog, "window" for browser window
                
                function showError(jsonData){
                    var sliceIndex = jsonData.responseText.indexOf("{\"jsonrpc\"");
                    alert("'.$l10n->getLabel('backend','mf_error').'\n\n"+jsonData.responseText.slice(0,sliceIndex));
                }
                
                function showDeleteModal_'.$recordEditTableID.'(uid, title, deleteLink, isAjax) {
                
                    //deleteLink is the called URL for effectively erasing the record. The URL must return a JSON message, with a field "result" with value either "error" or "success". 
                    //this will generate the message displayed in the dialog
                    useAjax = isAjax;
                    deleteURL = deleteLink;
                     
                    dialogContentURL = "/mf/core/ajax-core-html.php?'.$sec->parameters.'&delete_url="+b64EncodeUnicode(deleteLink)+"&title="+title+"&is_ajax="+isAjax+"&header=0&footer=0&sec='.$sec->hash.'&fields='.$sec->fields.'";
                     
                    mf.dialogManager.openDialog("delete_modal", "deleteDialog_"+uid, mf.dialogManager.getLastDialog(), dialogContentURL, "50%", "deleteCallback_'.$recordEditTableID.'()", "'.$l10n->getLabel('main','delete_title').'");
                };
                
                function deleteCallback_'.$recordEditTableID.'(){
                
                    if(useAjax == 1){
                        if(!(typeof lastDisplay_'.$recordEditTableID.' === "undefined")){
                        console.log("calling updateRecordEditTable_'.$recordEditTableID.'()");
                            //update record list view to integrate the current record data changes to the list
                            updateRecordEditTable_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], lastDisplay_'.$recordEditTableID.'["page"]);
                            '.((trim($advancedOptions['callBackOnRecordClose']) != '')?$advancedOptions['callBackOnRecordClose']:'').'
                        }
                    }
                    else {
                        document.location = deleteURL;

                    }
                    mf.dialogManager.closeLastDialog();
                } 
                
               

                function restore_'.$recordEditTableID.'(restoreUrl) {
                    var jqxhr = $.post(getURLFromString(restoreUrl), getParametersFromString(restoreUrl) )
                    .success(function(jsonData) {
                        if(!(typeof lastDisplay_'.$recordEditTableID.' === "undefined")){
                            //update record list view to integrate the current record data changes to the list
                            updateRecordEditTable_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], lastDisplay_'.$recordEditTableID.'["page"]);
                            '.((trim($advancedOptions['callBackOnRecordClose']) != '')?$advancedOptions['callBackOnRecordClose']:'').'
                        }
                    })
                    .fail( function(xhr, textStatus, errorThrown) {
                        alert("dbRecord.php restore() : " + xhr.responseText);
                    })
                };

                function sort_'.$recordEditTableID.'(actionUrl) {

                    var jqxhr = $.post(getURLFromString(actionUrl), getParametersFromString(actionUrl) )
                    .success(function(jsonData) {
                        if(!(typeof lastDisplay_'.$recordEditTableID.' === "undefined")){
                            //update record list view to integrate the current record data changes to the list
                            updateRecordEditTable_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], lastDisplay_'.$recordEditTableID.'["page"]);
                        }
                    })
                    .fail( function(xhr, textStatus, errorThrown) {
                        alert("dbRecord.php sort_'.$recordEditTableID.'() : " + xhr.responseText);
                    })

                };

                function clone_'.$recordEditTableID.'(actionUrl) {

                    var jqxhr = $.post(getURLFromString(actionUrl), getParametersFromString(actionUrl) )
                    .success(function(jsonData) {
                        if(!(typeof lastDisplay_'.$recordEditTableID.' === "undefined")){
                            //update record list view to integrate the current record data changes to the list
                            updateRecordEditTable_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], 0);
                        }
                    })
                    .fail( function(xhr, textStatus, errorThrown) {
                        alert("dbRecord.php clone() : " + xhr.responseText);
                    })

                };

                function getURLFromString(theURL) {
                    //var query = theURL.substr(1);
                    var urlSplit = theURL.split("?");

                    return urlSplit[0];
                }

                function getParametersFromString(theURL) {

                    var urlSplit = theURL.split("?");

                    var result = {};
                    if(urlSplit.length > 1){
                         var parameters = urlSplit[1].split("&");
                        for(var i=0; i<parameters.length; i++) {
                            var item = parameters[i].split("=");
                            result[item[0]] = item[1];
                        }
                    }
                    return result;
                }

            </script>
            <!-- Modal -->
            <div id="windowTitleDialog_'.$recordEditTableID.'" class="modal fade" role="dialog" style="z-index:10000">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" aria-hidden="true" data-dismiss="modal" type="button">&times;</button>
                            <h4>'.$l10n->getLabel('backend','deletion').'</h4>
                        </div>
                        <div class="modal-body">
                            <p id="deleteMessage">

                            </p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" onclick="okClicked();" type="button">'.$l10n->getLabel('backend','ok').'</button>
                            <button class="btn btn-default" data-dismiss="modal" type="button">'.$l10n->getLabel('backend','cancel').'</button>
                        </div>
                    </div>
                </div>
            </div>';



         $action = "updateRecordEditTable";

         //form security variables
	     $sec = getSec(array(
             'action' => $action,
             'session_recordEditTableID' => $session_recordEditTableID,
             'mode' => $mf->mode,
             'mfSetLocale' => (($this->data['language']['value'] != '')?$this->data['language']['value']:$mf->getLocale()),
	     ));

         //AJAX REQUESTS
         $html[] = '<script id="script_c_'.$recordEditTableID.'" >
                if(typeof lastDisplay_'.$recordEditTableID.' === "undefined") {
                    lastDisplay_'.$recordEditTableID.' = new Array();
                    lastDisplay_'.$recordEditTableID.'["sortingField"] = "'.$request['ORDER BY'].'";
                    lastDisplay_'.$recordEditTableID.'["sortingDirection"] = "'.$request['ORDER DIRECTION'].'";
                    lastDisplay_'.$recordEditTableID.'["page"] = 0;
                    lastDisplay_'.$recordEditTableID.'["copyPaste"] = new Array();
                }
              
                //Refresh with last request on ajax save + windows closing
                function updateRecordEditTable_'.$recordEditTableID.'(sortingField, sortingDirection, page, copyPaste){
                
                    console.log("updateRecordEditTable_'.$recordEditTableID.'("+sortingField+", "+sortingDirection+", "+page+")");
                    
                    // workaround when quotes are needed in sorting field, such as when using such syntax : 
                    // "FIELD(status, \"pending\",\"payment_failure\",\"archived\")"
                    // in such case, call updateRecordEditTable() with an urlencoded version of your sorting field as first argument
                    sortingField = decodeURI(sortingField);

                    lastDisplay_'.$recordEditTableID.'["sortingField"] = sortingField;
                    lastDisplay_'.$recordEditTableID.'["sortingDirection"] = sortingDirection;
                    lastDisplay_'.$recordEditTableID.'["page"] = page;
                    
                    //saving actually displayed RET content
                    saveContent = $("#recordEditTable_'.$recordEditTableID.'").html();
                    $("#recordEditTable_'.$recordEditTableID.'").children().eq(1).html( \'<table class="recordEditTable table table-hover table-striped"><tbody><tr><td><div style="height:30px;"><i style="width:14px;height:14px;margin-top:10px;" class="center glyphicon glyphicon-refresh gly-spin"></i></div></td></tr></tbody></table>\' );
                    
                   //update the record table
                    var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", {
                        action: "'.$action.'",
                        sec:"'.$sec->hash.'",
                        fields:"'.$sec->fields.'",
                        mode:"'.$mf->mode.'",
                        mfSetLocale:"'.(($this->data['language']['value'] != '')?$this->data['language']['value']:$mf->getLocale()).'", 
                        sorting_field: sortingField,
                        sorting_direction: sortingDirection,
                        show_page_num: page,
                        copy_paste: lastDisplay_'.$recordEditTableID.'["copyPaste"].join(","),
                        session_recordEditTableID: "'.$session_recordEditTableID.'",
                       
                    
                    }).success(function(jsonData) {
                        
                        if(jsonData.result == "success"){
                            //deleted old scripts including this script (it should continue executing since launched in memory)
		                    $("#script_a_'.$recordEditTableID.'").remove();
		                    $("#script_b_'.$recordEditTableID.'").remove();
		                    $("#script_c_'.$recordEditTableID.'").remove();
                    
                            //updating RET with new content
                            $("#recordEditTable_' . $recordEditTableID . '").replaceWith( jsonData.html );
                        }
                        else showError(jsonData.message);

                    }).fail( function(xhr, textStatus, errorThrown) {
                        //displaying again previously displayed RET content that had been saved
                        $("#recordEditTable_'.$recordEditTableID.'").replaceWith( saveContent );
                        alert("dbRecord.php showRecordEditTable_'.$recordEditTableID.'() : " + xhr.responseText);
                    })
                    
                    

                }';

         $action = "printRecordEditTable";

         //form security variables
	     $sec = getSec(array(
             'action' => $action,
             'session_recordEditTableID' => $session_recordEditTableID,
		     'mfSetLocale' => $mf->getLocale()
         ));

         $html[] = '
                function printRecordEditTable_'.$recordEditTableID.'(){

                    if($("#printSelection_'.$recordEditTableID.'").val()=="this"){
                        //printing only current result page
                        launchPrint_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], lastDisplay_'.$recordEditTableID.'["page"]);
                    }
                    else{
                        //printing all results
                        launchPrint_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], "all");
                    }

                }

                function launchPrint_'.$recordEditTableID.'(sortingField, sortingDirection, page){
                   console.log("launchPrint_'.$recordEditTableID.'("+sortingField+", "+sortingDirection+", "+page+")");

                   //update the record table
                    var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-html.php", {
                        action: "'.$action.'",
                        mfSetLocale: "'.$mf->getLocale().'",
                        sec:"'.$sec->hash.'",
                        fields:"'.$sec->fields.'",
                         
                        sorting_field: sortingField,
                        sorting_direction: sortingDirection,
                        show_page_num: page,
                        session_recordEditTableID: "'.$session_recordEditTableID.'",
                        
                        header:1,
                        footer:1,
                        getHTMLAsAJAX:1,
                    
                    })
                    .success(function(jsonData) {
                        var win=null;
                        win = window.open("width=200,height=200");
                        self.focus();
                        win.document.open();
                        win.document.write(jsonData.html);
                        win.document.close();
                        win.print();
                        win.close();
                    })
                    .fail( function(xhr, textStatus, errorThrown) {
                        alert("dbRecord.php launchPrint_'.$recordEditTableID.'() : " + xhr.responseText);
                    })
                }
            </script>';





         $html[] = '<div id="recordEditTable_'.$recordEditTableID.'">';


         $html[] = '<div class="row">';

         //display the record pager
         $pager = Array();
         //echoValue('totalRecordCount',$totalRecordCount);
         //echoValue('maxRecordsPerPage',$maxRecordsPerPage);
         if($totalRecordCount > $maxRecordsPerPage){

             //get the current page URL and strip an eventual previous &page parameter
             //parse the current URL request
             $parsedUrl = j_parseUrl(selfURL());

             //convert and decode $parsedUrl['query'] as an array of parameters
             parse_str(urldecode($parsedUrl['query']), $parsedUrl['query']);

             //display pager if there are more records than $config['max_records_per_page'], that is $maxRecordsPerPage
             $pager[] = "<div class='pull-right'>";

             $pager[] = "<ul class='pagination pagination-sm'>";
             $numPages = ceil($totalRecordCount /  $maxRecordsPerPage);

             if($page > 0) {
                 $onclick = 'updateRecordEditTable_'.$recordEditTableID.'("'.urlencode($request['ORDER BY']).'", "'.$request['ORDER DIRECTION'].'", '.($page-1).')';
                 $pager[] = "<li><a onClick='".$onclick."'>«</a></li>";
             }
             for($pageIndex=0;$pageIndex < $numPages; $pageIndex++){
                 //limit pager display to 10 pages around the current page
                 if($pageIndex > ($page-5) && $pageIndex < ($page+5)){
                     $onclick = 'updateRecordEditTable_'.$recordEditTableID.'("'.urlencode($request['ORDER BY']).'", "'.$request['ORDER DIRECTION'].'", '.$pageIndex.')';
                     $pager[] = "<li><a onClick='".$onclick."'".(($pageIndex == $page)?" class='active'":"").">".($pageIndex+1)."</a></li>";
                 }
                 //show "..." if there are more pages available than currently displayed
                 else if($pageIndex == ($page-5) || $pageIndex == ($page+5)){
                     $onclick = 'updateRecordEditTable_'.$recordEditTableID.'("'.urlencode($request['ORDER BY']).'", "'.$request['ORDER DIRECTION'].'", '.$pageIndex.')';
                     $pager[] = "<li><a onClick='".$onclick."'".(($pageIndex == $page)?" class='active'":"").">"."..."."</a></li>";
                 }
             }
             if($page < $numPages-1) {
                 $onclick = 'updateRecordEditTable_'.$recordEditTableID.'("'.urlencode($request['ORDER BY']).'", "'.$request['ORDER DIRECTION'].'", '.($page+1).')';
                 $pager[] = "<li><a onClick='".$onclick."'>»</a></li>";
             }
             $pager[] = "</ul>";
             if(isset($advancedOptions['showResultsCount']) && $advancedOptions['showResultsCount']){
                 //computing displayed records bounds
                 $lowBound = $page*$maxRecordsPerPage+1;
                 if(($totalRecordCount - $page*$maxRecordsPerPage) >= $maxRecordsPerPage ) $highBound = $page*$maxRecordsPerPage + $maxRecordsPerPage;
                 else $highBound = $page*$maxRecordsPerPage + ($totalRecordCount - $page*$maxRecordsPerPage);

                 $pager[] = "<div class='resultsCount'>".$totalRecordCount.' '.$l10n->getLabel('dbRecord','results')." (".$lowBound."-".$highBound.")</div>";
             }
             $pager[] = "</div>";

         }
         else {
             //displaying only the results count
	         if(isset($advancedOptions['showResultsCount']) && $advancedOptions['showResultsCount']) {
		         $pager[] = "<div class='pull-right'>";
		         $pager[] = "<div class='resultsCount'>" . $totalRecordCount . ' ' . $l10n->getLabel( 'dbRecord', 'results' ) . "</div>";
		         $pager[] = "</div>";
	         }
             //force page = 0 to avoid showing a blank RET page when deleting a record right above LIMIT
	         $page=0;
         }
         //if(!$advancedOptions['printMode'])$html = array_merge($html,$pager);

         //display the buttons, except when the page is called in print mode
         if( ( (trim($buttons)!='') || $advancedOptions['showPrint'] || $advancedOptions['showRefresh'] || (isset($advancedOptions['buttons']) && $advancedOptions['buttons'] !='') ) && !$advancedOptions['printMode']){
             $html[] = "<div class='col-lg-12 '>";
             $html[] = "    <div class='buttons pull-left'>";
             $html[] = $buttons;

             if($advancedOptions['showPrint']){
                 $html[] = "
                                <form id=\"showRecordEditTablePrint\">
                                    <div class=\"form-group\">
                                        <button onclick=\"printRecordEditTable_".$recordEditTableID."();\" type=\"button\" class=\"float-left btn-sm ".$advancedOptions['buttonClass']."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-print\"></span> ".$l10n->getLabel('backend','print')."</button>
                                        <select id=\"printSelection_".$recordEditTableID."\" class=\"float-left form-control printSelect\">
                                            <option value=\"this\">".$l10n->getLabel('backend','printThis')."</option>
                                            <option value=\"all\">".$l10n->getLabel('backend','printAll')."</option>
                                        </select>
                                        </div>
                                </form>
                           ";
             }

	         if($advancedOptions['showRefresh']){
		         $html[] = "
                                <button onclick=\"updateRecordEditTable_".$recordEditTableID."();\" type=\"button\" class=\"float-left btn-sm ".$advancedOptions['buttonClass']."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-refresh\"></span> ".$l10n->getLabel('backend','refresh')."</button>
                                        
                           ";
	         }

             $html[] = "    </div>";
             $html = array_merge($html,$pager);
             $html[] = "</div>";
         }



         $html[] = '</div><!-- /.row -->';


	     // STORING the recordEditTable settings in the session
	     // currently displayed RET settings are required to be forwarded in queries to match current display
	     // but we can't always disclose them, especialy the request parameter, for security reasons, so we are storing it into the $_SESSION
	     // storing those settings in the session also makes the code MUCH simplier
	     if(!isset($_SESSION['mf']))$_SESSION['mf'] = array();
	     if(!isset($_SESSION['mf']['recordEditTables']))$_SESSION['mf']['recordEditTables'] = array();
	     $_SESSION['mf']['recordEditTables'][$session_recordEditTableID] = array(
		     'request' => $request,
		     'moduleName' => $moduleName,
		     'subModuleName' => $subModuleName,
		     'mainKey' => $mainKey,
		     'keyProcessors' => $keyProcessors,
		     'page' => $page,
		     'availableActions' => $availableActions,
		     'advancedOptions' => $advancedOptions,
		     'recordEditTableID' => $recordEditTableID,
		     'recordClass' => $recordClass,
		     'hideColumns' => $hideColumns,
		     'useColumnClasses' => $useColumnClasses,
		     'actionOnTitleClick' => $actionOnTitleClick,
		     'locale' => $this->data['language']['value'],
	     );

        if ($totalRecordCount > 0) {

            $sqlNoLimit = "SELECT ".$distinct.$alias.".*,".$request['SELECT']." FROM ".$request['FROM'].$customJoinSupplement.$request['JOIN']." WHERE ".$request['WHERE'].$customWhereSupplement.$language." ORDER BY ".$request['ORDER BY']." ".$request['ORDER DIRECTION'];

            //paginated SQL request
            if ($page > 0){
                $sql = $sqlNoLimit." LIMIT ".($page*$maxRecordsPerPage).",".$maxRecordsPerPage;
            }
            else{
                $sql = $sqlNoLimit." LIMIT ".$maxRecordsPerPage;
            }

            if($advancedOptions['debugSQL'])
                $html[] = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$sql.'</div>';


            try{
                $stmt = $pdo->query($sql);
            }
            catch(PDOException $e){
                /*echo "<strong>Message =</strong> ".$e->getMessage()."<br/>".chr(10);
                echo "<strong>SQL =</strong> ".$sql."<br/>".chr(10);
                return 0;*/
                $html[] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                    "<strong>Message =</strong> ".$e->getMessage()."<br/>".
                    "<strong>SQL =</strong> ".$sql."<br/>".
                    '</div>';
                return implode(chr(10),$html);
            }

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
//print_r($rows);

            //preparing values
            $selectArray = explode(',',$request['SELECT']);
            $selectArrayNoAlias = array();
            $selectArrayAliases = array();

            foreach($selectArray as $index => $keyName){
                $selectArray[$index] = trim(trim($keyName),'`'); //filter spaces
                //seek alias and remove it from key name
                if($aliasSeparatorPosition = strrpos($selectArray[$index], '.')){
                    //we have an alias
                    $selectArrayNoAlias[$index] = substr($selectArray[$index], $aliasSeparatorPosition+1);
                    $selectArrayAliases[$index] = substr($selectArray[$index], 0, $aliasSeparatorPosition);
                }
                else {
                    $selectArrayNoAlias[$index] = $selectArray[$index];
                    $selectArrayAliases[$index] = '';
                }
            }


	        $_SESSION['mf']['recordEditTables'][$session_recordEditTableID]['selectArrayNoAlias'] = $selectArrayNoAlias;
	        $_SESSION['mf']['recordEditTables'][$session_recordEditTableID]['selectArrayAliases'] = $selectArrayAliases;



			//DISPLAYING ROWS NOW


            $html[] = "<table class='recordEditTable table table-hover table-striped'>";
            $html[] = "<thead><tr class='recordEditTableHeaders'>";

            //add a column for prefixing each line with a select box if copy/paste is enabled
            if($availableActions['copyPaste']==1)$html[] = "<th><i class=\"glyphicon glyphicon-copy\"></i></th>";

            foreach($selectArrayNoAlias as $index => $key){
                if(!in_array($key,$hideColumns)){
                    $onclick = 'updateRecordEditTable_'.$recordEditTableID.'("'.$key.'", "ASC", 0)'; //glyphicon glyphicon-arrow-down

                    if($useColumnClasses && isset($advancedOptions['columnClasses'][$key]))$class=' class="'.$advancedOptions['columnClasses'][$key].'"';
                    else $class='';

                    $html[] = "<th".$class."><a onClick='".$onclick."' title='".$l10n->getLabel('backend','sort_by')." \"";

                    //echo colum name
                    if($selectArrayAliases[$index]=='' || $selectArrayAliases[$index] == static::getTableName()){
                        //get label from current record's class
                        $html[] =  $l10n->getLabel($recordClass,$key)."\"'>".$l10n->getLabel($recordClass,$key);
                    }
                    else{
                        //get label from class name supplied as alias

                        $html[] =  $l10n->getLabel($selectArrayAliases[$index],$key)."\"'>".$l10n->getLabel($selectArrayAliases[$index],$key);
                    }
                    $html[] = "</a>";

                    $onclickToggle = "updateRecordEditTable_".$recordEditTableID."('".$key."', ".(($request['ORDER DIRECTION'] == 'ASC')?"'DESC'":"'ASC'").", 0)"; //glyphicon glyphicon-arrow-down
                    $html[] = '<a onClick="'.$onclickToggle.'"  title="'.$l10n->getLabel('backend','sort_by').'"><img src="'.SUB_DIR.$mf->xEnd->template.'/img/toggle.png" width="16" height="16"></a>';

                    $html[] = "</th>";
                }
            }
            if(!in_array('actions',$hideColumns) and !$advancedOptions['printMode']){
                $html[] = '<th class="actions" colspan=20>'.$l10n->getLabel('backend','actions')."</th>";
            }
            $html[] = '</tr></thead><tbody>';

            $x_end = ($mf->mode == 'frontend')?'frontend':'backend';
            $locale = $mf->info[$x_end.'_locale'];

         /*   $options = array(
                'options' => array(
                    'default' => 0, // valeur à retourner si le filtre échoue
                ),
            );*/

            $firstIteration=true;

            foreach($rows as $row){
	            $html[] = $this->RETGetRow($row, $firstIteration, $session_recordEditTableID);
	            $firstIteration = false;
            }
            $html[] = "</tbody></table>";

            if(!$advancedOptions['printMode']) {
                $html[] = '<div class="row">
                                <div class="col-lg-12">';
                $html = array_merge($html, $pager);
                $html[] = '</div></div>';
            }




        } else{
            //display "no page in table" message
            $html[] = "<table class='recordEditTable table table-hover table-striped'>";
            $html[] = "<tbody><tr><td>".$l10n->getLabel('dbRecord','no_result')."</td></tr></tbody></table>";

        }
        $html[] = '</div> <!-- /#recordEditTable_'.$recordClass.'-->';

        return implode(chr(10),$html);
    }

	/**
	 * Generates the HTML code for displaying a row of the recordEditTable
	 *
	 * @param $row an associative array of the values to displau
	 * @param $firstIteration
	 * @param $session_recordEditTableID string this ID points to all the recordEditTable advanced settings in the PHP Session. It avoids passing plenty of recordEditTable parameters in the get requests when refreshing the recordEditTable view. Unfortunately it has to be truely unique and can't be the same as the recordEditTableID in case there would be several instances of a same recordEditTables displayed in different browser tabs
	 */

	function RETGetRow($row,$firstIteration, $session_recordEditTableID){

     	global $mf, $l10n, $logger;
     	$locale = $mf->getLocale();
     	$html = array();

		$RETSession =           $_SESSION['mf']['recordEditTables'][$session_recordEditTableID];
		$mainKey =              $RETSession['mainKey'];
		$moduleName =           $RETSession['moduleName'];
		$subModuleName =        $RETSession['subModuleName'];
		$recordEditTableID =    $RETSession['recordEditTableID'];
		$availableActions =     $RETSession['availableActions'];
		$advancedOptions =      $RETSession['advancedOptions'];
		$keyProcessors =        $RETSession['keyProcessors'];
		$recordClass =          $RETSession['recordClass'];
		$selectArrayNoAlias =   $RETSession['selectArrayNoAlias'];
		$hideColumns =          $RETSession['hideColumns'];
		$useColumnClasses =     $RETSession['useColumnClasses'];
		$actionOnTitleClick =   $RETSession['actionOnTitleClick'];
		$locale =               $mf->getLocale();

	    // $this->data[$key]['value'] = $rows[$key];
	    if($firstIteration){
		    if(!isset($row[$mainKey])){
			    $mf->addErrorMessage('dbRecord::showRecordEditTable(), '.$l10n->getLabel('backend','instance_of').' "'.$this->getClassName().'" : '.$l10n->getLabel('backend','mainkey').' "'.$mainKey.'" '.$l10n->getLabel('backend','does_not_exist').' !');
			    //print_r($row);
		    }

	    }
	    $html[] = "<tr>";

	    //prefix each line with a select box if copy/paste is enabled
	    if($availableActions['copyPaste']==1){
		    $inBuffer = in_array($row['uid'],$_SESSION['RET_copyPasteBuffer'][$recordEditTableID]);
		    $html[] = "<th><input type=\"checkbox\"  id=\"".$recordEditTableID."_copy_".$row['uid']."\" data-recordUid=\"".$row['uid']."\" onclick=\"toggleCopy_".$recordEditTableID."('".$recordEditTableID."_copy_".$row['uid']."',".$row['uid'].")\" ".(($inBuffer==1)?' checked="checked"':'')."/></th>";
	    }

	    //PREPARE ACTION LINKS
	    $viewLink = '';
	    $editLink = '';
	    $deleteLink = '';
	    $restoreLink = '';
	    $sortUpLink = '';
	    $sortDownLink = '';
	    $cloneLink = '';
	    $customRecordActionLinks = array();


	    foreach($availableActions as $recordActionKey => $recordAction){

		    //fix strings to integers because some tests below would fail otherwise
		    if(gettype($recordActionKey) == 'string'){
			    if($recordAction == '1')$recordAction = 1;
			    if($recordAction == '0')$recordAction = 0;
		    }

		    $formAttributes = array(
			    'method'=>'POST',
			    'onsubmit'=>'sendForm();',
			    'role'=>'form',
			    'class'=>'form-horizontal',
			    'enctype'=>'x-www-form-urlencoded',
			    'autocomplete'=>'off' //disable form autocompletion
		    );


		    switch($recordActionKey){
			    case 'create':
				    //ignore the create action, as it is processed once at the showRecordEditTable level (the create button is on top of the showRecordEditTable)
				    //but not at each record row
				    break;
			    //make view link
			    case 'view':

				    if(gettype($recordAction) == 'integer' || gettype($recordAction) == 'boolean'){
					    //process boolean value
					    if($recordAction == 1){

						    //default Mindflow link for viewing a record
						    if(isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions']){
							    //form security variables

							    $sec = getSec(array(
								    'action' => 'viewRecord',
								    'record_uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'header' => '0',
								    'footer' => '0',
								    'ajax' => '1',
									'recordEditTableID' => $recordEditTableID,
								    'record_class' => $recordClass,
								    'showSaveButton' => '0',
								    'showSaveCloseButton' => '0',
								    'showPreviewButton' => $advancedOptions['editRecordButtons']['showPreviewButton'],
								    'showCloseButton' => $advancedOptions['editRecordButtons']['showCloseButton'],
								    'showHistoryButton' => $advancedOptions['editRecordButtons']['showHistoryButton'],
								    'mode' => $mf->mode,
								    'mfSetLocale' => $locale,
							    ));

							    $viewLink = SUB_DIR.'/mf/core/ajax-core-html.php?'.$sec->parameters.'&sec='.$sec->hash.'&fields='.$sec->fields;
						    }

						    else {
							    //form security variables
							    $sec = getSec(array(
								    'action' => 'viewRecord',
								    'uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'header' => '0',
								    'footer' => '0',
								    'ajax' => '1',
								    'class' => $recordClass,
								    'mode' => $mf->mode,
								    'recordEditTableID' => $recordEditTableID,
								    'showSaveButton' => '0',
								    'showSaveCloseButton' => '0',
								    'showPreviewButton' => '0',
								    'showCloseButton' => '1',
								    'showHistoryButton' => '0',
								    'mfSetLocale' => $locale,
							    ));


							    $viewLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }
					    }
				    }
				    else {

					    //these tags can be specified in custom links and they will be substituted by MindFlow

					    $action = $recordAction;
					    $action = str_replace("{subdir}",SUB_DIR,$action);
					    $action = str_replace("{record_uid}",$row['uid'],$action);
					    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);

					    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
					    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
					    $action = str_replace("{mode}",$mf->mode,$action);
					    $action = str_replace("{record_class}",$recordClass,$action);
					    $action = str_replace("{module_id}",$moduleName,$action);
					    $action = str_replace("{submodule_id}",$subModuleName,$action);
					    $action = str_replace("{showSaveButton}",$advancedOptions['editRecordButtons']['showSaveButton'],$action);
					    $action = str_replace("{showSaveCloseButton}",$advancedOptions['editRecordButtons']['showSaveCloseButton'],$action);
					    $action = str_replace("{showPreviewButton}",$advancedOptions['editRecordButtons']['showPreviewButton'],$action);
					    $action = str_replace("{showCloseButton}",$advancedOptions['editRecordButtons']['showCloseButton'],$action);
					    $action = str_replace("{showHistoryButton}",$advancedOptions['editRecordButtons']['showHistoryButton'],$action);

					    //arbitrary column name marker. Automatically replace {markerName} with $row['markerName']
					    preg_match_all('/{(\w+)}/', $action, $matches);

					    foreach ($matches[0] as $index => $var_name) {
						    if (isset($row[$matches[1][$index]])) {
							    $action = str_replace($var_name, $row[$matches[1][$index]], $action);
						    }
					    }

					    eval( '$viewLink = '.$action.';');
				    }
				    break;


			    //make edit link
			    case 'edit':

				    if(gettype($recordAction) == 'integer' || gettype($recordAction) == 'boolean'){
					    //process boolean value
					    if($recordAction == 1){

						    if(isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions']){

							    $sec = getSec(array(
								    'action' => 'editRecord',
								    'record_uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'header' => '0',
								    'footer' => '0',
								    'ajax' => '1',

								    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
								    'recordEditTableID' => $recordEditTableID,
								    'record_class' => $recordClass,
								    'showSaveButton' => "'".$advancedOptions['editRecordButtons']['showSaveButton']."'",
								    'showSaveCloseButton' => "'".$advancedOptions['editRecordButtons']['showSaveCloseButton']."'",
								    'showPreviewButton' => "'".$advancedOptions['editRecordButtons']['showPreviewButton']."'",
								    'showCloseButton' => "'".$advancedOptions['editRecordButtons']['showCloseButton']."'",
								    'showHistoryButton' => "'".$advancedOptions['editRecordButtons']['showHistoryButton']."'",
								    'mode' => $mf->mode,
								    'mfSetLocale' => $locale,

							    ));

							    $editLink = SUB_DIR.'/mf/core/ajax-core-html.php?'.$sec->parameters.'&sec='.$sec->hash.'&fields='.$sec->fields;

						    }
						    else{

							    //form security variables
							    $sec = getSec(array(
								    'action' => 'editRecord',
								    'uid' => $row['uid'],

								    'class' => $recordClass,
								    'mode' => $mf->mode,
								    'mfSetLocale' => $locale,
							    ));

							    $editLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').'&'.$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }

					    }
				    }
				    else{

					    //these tags can be specified in custom links and they will be substituted by MindFlow

					    $action = $recordAction;
					    $action = str_replace("{subdir}",SUB_DIR,$action);
					    $action = str_replace("{record_uid}",$row['uid'],$action);
					    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);

					    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
					    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
					    $action = str_replace("{mode}",$mf->mode,$action);
					    $action = str_replace("{record_class}",$recordClass,$action);
					    $action = str_replace("{module_id}",$moduleName,$action);
					    $action = str_replace("{submodule_id}",$subModuleName,$action);
					    $action = str_replace("{showSaveButton}",$advancedOptions['editRecordButtons']['showSaveButton'],$action);
					    $action = str_replace("{showSaveCloseButton}",$advancedOptions['editRecordButtons']['showSaveCloseButton'],$action);
					    $action = str_replace("{showPreviewButton}",$advancedOptions['editRecordButtons']['showPreviewButton'],$action);
					    $action = str_replace("{showCloseButton}",$advancedOptions['editRecordButtons']['showCloseButton'],$action);
					    $action = str_replace("{showHistoryButton}",$advancedOptions['editRecordButtons']['showHistoryButton'],$action);


					    //arbitrary column name marker. Automatically replace {markerName} with $row['markerName']
					    preg_match_all('/{(\w+)}/', $action, $matches);

					    foreach ($matches[0] as $index => $var_name) {
						    if (isset($row[$matches[1][$index]])) {
							    $action = str_replace($var_name, $row[$matches[1][$index]], $action);
						    }
					    }


//debug_print_backtrace();
					    eval( '$editLink = '.$action.';');

				    }

				    break;

			    //make delete link
			    case 'delete':
			    case 'restore':

				    if(gettype($recordAction) == 'integer' || gettype($recordAction) == 'boolean'){
					    //process boolean value
					    if($recordAction == 1){

						    //default Mindflow link for deleting a record
						    if(isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions']){
							    if(isset($row['deleted'])){
								    if($row['deleted']==0){
									    //delete
									    //form security variables
									    $sec = getSec(array(
										    'action' => 'deleteRecord',
										    'record_uid' => $row['uid'],
										    'session_recordEditTableID' => $session_recordEditTableID,
										    'mode' => $mf->mode,
										    'record_class' => $recordClass,
										    'mfSetLocale' => $locale,
									    ));

									    $deleteLink = SUB_DIR.'/mf/core/ajax-core-json.php?'.$sec->parameters.'&sec='.$sec->hash.'&fields='.$sec->fields;


								    }
								    else if($row['deleted']==1){
									    //restore
									    //form security variables
									    $sec = getSec(array(
										    'action' => 'restoreRecord',
										    'record_uid' => $row['uid'],
										    'session_recordEditTableID' => $session_recordEditTableID,
										    'mode' => $mf->mode,
										    'record_class' => $recordClass,
										    'mfSetLocale' => $locale,
									    ));

									    $restoreLink = SUB_DIR.'/mf/core/ajax-core-json.php?'.$sec->parameters.'&sec='.$sec->hash.'&fields='.$sec->fields;

								    }
							    }
						    }
						    else {
							    if(isset($row['deleted'])){
								    if($row['deleted']==0){
									    //delete
									    //form security variables
									    $sec = getSec(array(
										    'action' => 'delete',
										    'uid' => $row['uid'],
										    'session_recordEditTableID' => $session_recordEditTableID,
										    'mode' => $mf->mode,
										    'class' => $recordClass,
										    'mfSetLocale' => $locale,
									    ));

									    $deleteLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
								    }
								    else if($row['deleted']==1){
									    //restore
									    //form security variables
									    $sec = getSec(array(
										    'action' => 'restore',
										    'uid' => $row['uid'],
										    'session_recordEditTableID' => $session_recordEditTableID,
										    'mode' => $mf->mode,
										    'class' => $recordClass,
										    'mfSetLocale' => $locale,
									    ));

									    $restoreLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
								    }
							    }
						    }

						    //"{subdir}/mf/index.php?module='.$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').'&action=delete&class='.$recordClass.'&uid=" + deleteRecordUid;
					    }
				    }
				    else {
					    if(isset($row['deleted'])){
						    if($row['deleted']==0){
							    //delete
							    $action = $availableActions['delete'];
							    $action = str_replace("{subdir}",SUB_DIR,$action);
							    $action = str_replace("{record_uid}",$row['uid'],$action);
							    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);

							    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
							    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
							    $action = str_replace("{mode}",$mf->mode,$action);
							    $action = str_replace("{record_class}",$recordClass,$action);
							    $action = str_replace("{module_id}",$moduleName,$action);
							    $action = str_replace("{submodule_id}",$subModuleName,$action);

							    eval( '$deleteLink = '.$action.';');
						    }
						    else if($row['deleted']==1){
							    $action = $availableActions['restore'];
							    $action = str_replace("{subdir}",SUB_DIR,$action);
							    $action = str_replace("{record_uid}",$row['uid'],$action);
							    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);

							    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
							    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
							    $action = str_replace("{mode}",$mf->mode,$action);
							    $action = str_replace("{record_class}",$recordClass,$action);
							    $action = str_replace("{module_id}",$moduleName,$action);
							    $action = str_replace("{submodule_id}",$subModuleName,$action);

							    //arbitrary column name marker. Automatically replace {markerName} with $row['markerName']
							    preg_match_all('/{(\w+)}/', $action, $matches);

							    foreach ($matches[0] as $index => $var_name) {
								    if (isset($row[$matches[1][$index]])) {
									    $action = str_replace($var_name, $row[$matches[1][$index]], $action);
								    }
							    }

							    eval( '$restoreLink = '.$action.';');
						    }
					    }

				    }

				    if(trim($deleteLink)!=''){
					    if(isset($row['deleted'])){
						    if($row['deleted']==0){

							    //delete modal
							    $deleteLink = 'javascript:showDeleteModal_'.$recordEditTableID.'('.$row['uid'].',"'.urlencode($row[$mainKey]).'","'.$deleteLink.'",'.$advancedOptions['ajaxActions'].');';
						    }
					    }
					    //do nothing for restore link
				    }
				    break;


			    case 'sortDown':

				    if(gettype($recordAction) == 'integer' || gettype($recordAction) == 'boolean'){
					    //process boolean value
					    if($recordAction == 1){
						    //default Mindflow link for sorting a record
						    if(isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions']){
							    //form security variables
							    $sec = getSec(array(
								    'action' => 'sort',
								    'direction' => 'down',
								    'record_uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'mode' => $mf->mode,
								    'record_class' => $recordClass,
								    'mfSetLocale' => $locale,
							    ));

							    $sortDownLink =SUB_DIR."/mf/core/ajax-core-json.php?".$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }
						    else {
							    //form security variables
							    $sec = getSec(array(
								    'action' => 'sort',
								    'direction' => 'down',
								    'record_uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'mode' => $mf->mode,
								    'record_class' => $recordClass,
								    'mfSetLocale' => $locale,
							    ));

							    $sortDownLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }
					    }
				    }
				    else{

					    $action = $recordAction;
					    $action = str_replace("{subdir}",SUB_DIR,$action);
					    $action = str_replace("{record_uid}",$row['uid'],$action);
					    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);

					    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
					    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
					    $action = str_replace("{mode}",$mf->mode,$action);
					    $action = str_replace("{record_class}",$recordClass,$action);
					    $action = str_replace("{module_id}",$moduleName,$action);
					    $action = str_replace("{submodule_id}",$subModuleName,$action);

					    //arbitrary column name marker. Automatically replace {markerName} with $row['markerName']
					    preg_match_all('/{(\w+)}/', $action, $matches);

					    foreach ($matches[0] as $index => $var_name) {
						    if (isset($row[$matches[1][$index]])) {
							    $action = str_replace($var_name, $row[$matches[1][$index]], $action);
						    }
					    }

					    eval( '$sortDownLink = '.$action.';');
				    }
				    break;


			    case 'sortUp':

				    if(gettype($recordAction) == 'integer' || gettype($recordAction) == 'boolean'){
					    //process boolean value
					    if($recordAction == 1){
						    //default Mindflow link for sorting a record
						    if(isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions']){
							    //form security variables
							    $sec = getSec(array(
								    'action' => 'sort',
								    'direction' => 'up',
								    'record_uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'mode' => $mf->mode,
								    'record_class' => $recordClass,
								    'mfSetLocale' => $locale,
							    ));

							    $sortUpLink = SUB_DIR."/mf/core/ajax-core-json.php?".$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }
						    else {
							    //form security variables
							    $sec = getSec(array(
								    'action' => 'sort',
								    'direction' => 'up',
								    'uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'mode' => $mf->mode,
								    'class' => $recordClass,
								    'mfSetLocale' => $locale,
							    ));

							    $sortUpLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }
					    }
				    }
				    else{

					    $action = $recordAction;
					    $action = str_replace("{subdir}",SUB_DIR,$action);
					    $action = str_replace("{record_uid}",$row['uid'],$action);
					    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);

					    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
					    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
					    $action = str_replace("{mode}",$mf->mode,$action);
					    $action = str_replace("{record_class}",$recordClass,$action);
					    $action = str_replace("{module_id}",$moduleName,$action);
					    $action = str_replace("{submodule_id}",$subModuleName,$action);

					    //arbitrary column name marker. Automatically replace {markerName} with $row['markerName']
					    preg_match_all('/{(\w+)}/', $action, $matches);

					    foreach ($matches[0] as $index => $var_name) {
						    if (isset($row[$matches[1][$index]])) {
							    $action = str_replace($var_name, $row[$matches[1][$index]], $action);
						    }
					    }

					    eval( '$sortUpLink = '.$action.';');
				    }
				    break;

			    case 'clone':

				    if(gettype($recordAction) == 'integer' || gettype($recordAction) == 'boolean'){
					    //process boolean value
					    if($recordAction == 1){
						    //default Mindflow link for cloning a record
						    if(isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions']){
							    //form security variables
							    $sec = getSec(array(
								    'action' => 'clone',
								    'record_uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'mode' => $mf->mode,
								    'record_class' => $recordClass,
								    'mfSetLocale' => $locale,
							    ));

							    $cloneLink =SUB_DIR."/mf/core/ajax-core-json.php?".$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }
						    else {
							    //form security variables
							    $sec = getSec(array(
								    'action' => 'clone',
								    'uid' => $row['uid'],
								    'session_recordEditTableID' => $session_recordEditTableID,
								    'mode' => $mf->mode,
								    'class' => $recordClass,
								    'mfSetLocale' => $locale,
							    ));

							    $cloneLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').$sec->parameters."&sec=".$sec->hash."&fields=".$sec->fields;
						    }
					    }
				    }
				    else{

					    $action = $recordAction;
					    $action = str_replace("{subdir}",SUB_DIR,$action);
					    $action = str_replace("{record_uid}",$row['uid'],$action);
					    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);


					    //TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
					    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
					    $action = str_replace("{mode}",$mf->mode,$action);
					    $action = str_replace("{record_class}",$recordClass,$action);
					    $action = str_replace("{module_id}",$moduleName,$action);
					    $action = str_replace("{submodule_id}",$subModuleName,$action);

					    //arbitrary column name marker. Automatically replace {markerName} with $row['markerName']
					    preg_match_all('/{(\w+)}/', $action, $matches);

					    foreach ($matches[0] as $index => $var_name) {
						    if (isset($row[$matches[1][$index]])) {
							    $action = str_replace($var_name, $row[$matches[1][$index]], $action);
						    }
					    }

					    eval( '$cloneLink = '.$action.';');
				    }
				    break;


			    case 'copyPaste':

				    break;

			    //make custom link and replace supplied markers with values extracted from the displayed record. See list of available markers below :
			    default:
				    // $recordActionKey." ";
				    if(is_array($recordAction)){

					    $action = $recordAction['link'];
					    $action = str_replace("{subdir}",SUB_DIR,$action);
					    $action = str_replace("{record_uid}",$row['uid'],$action);
					    $action = str_replace("{session_recordEditTableID}",$session_recordEditTableID,$action);

						//TODO : should be extracted from $session_recordEditTableID from NOW, to be removed after implementation has been done and checked
					    $action = str_replace("{recordEditTableID}",$recordEditTableID,$action);
					    $action = str_replace("{mode}",$mf->mode,$action);
					    $action = str_replace("{record_class}",$recordClass,$action);
					    $action = str_replace("{module_id}",$moduleName,$action);
					    $action = str_replace("{submodule_id}",$subModuleName,$action);

					    //arbitrary column name marker. Automatically replace {markerName} with $row['markerName']
					    preg_match_all('/{(\w+)}/', $action, $matches);

					    foreach ($matches[0] as $index => $var_name) {
						    if (isset($row[$matches[1][$index]])) {
							    $action = str_replace($var_name, $row[$matches[1][$index]], $action);
						    }
					    }


					    // IMPORTANT : EN CAS DE BUG, NE PLUS TOUCHER LES QUOTES A CE NIVEAU, MAIS CORRIGER LA VALEUR FOURNIE POUR $action
					    // elle doit être du style :
					    // 'link' => "javascript:window.opener.updateOption_" . $key . "( 'add',{record_uid},'{record_class}')",
					    eval( '$customRecordActionLinks[$recordActionKey] = '.$action.';');
				    }
				    else if($firstIteration)$mf->addWarningMessage("DBRecord->showRecordEditTable() ID=$recordEditTableID : custom record actions for recordActionKey \"$recordActionKey\" must be specified with an array");
				    break;

		    }//end switch($action);
	    }

	    $excludedActionsLinkTypes = array('sortUp','sortDown','clone','delete','restore');

	    //DISPLAY ROW KEYS
	    foreach($selectArrayNoAlias as $index => $key){
		    //echo "key=".$key." row['".$key."']=".$row[$key]."<br>\n";

		    //echo "<br />\n<br />\n";
		    if(!in_array($key,$hideColumns)){
			    $value = $row[$key];
			    //echo "row=".print_r($row, true)."<br />\n";

			    if($useColumnClasses && isset($advancedOptions['columnClasses'][$key]))$class=' class="'.$advancedOptions['columnClasses'][$key].'"';
			    else $class='';

			    if(isset($keyProcessors[$key])){
				    //process key for display with optional user supplied processing function.
				    eval('$value = '.$keyProcessors[$key].'($key,$value,$locale,$row);');

			    }
			    if($key == $mainKey  and !$advancedOptions['printMode']){
				    //Add open record link on title if mainKey

				    switch($actionOnTitleClick){
					    case 'view':
						    $link = "openRecord(\"".$viewLink."\",\"90%\",\"\", \"".$l10n->getLabel($recordClass,'record_name')." (".$row['uid'].")\");";
						    break;
					    case 'edit':
						    $link = "openRecord(\"".$editLink."\",\"90%\",\"".$advancedOptions['callBackOnRecordClose']."\", \"".$l10n->getLabel($recordClass,'record_name')." (".$row['uid'].")\");";
						    break;
					    default:
						    if(!in_array($recordActionKey,$excludedActionsLinkTypes)) $link = $customRecordActionLinks[$recordActionKey];
						    else $link = $viewLink;

				    }

				    $linkClass = (isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions'])?" class='mainKey'":"mainKey";


				    $html[] = "<td".$class."><a".$linkClass." class='mainKey' onclick='".$link."' title='".$l10n->getLabel('backend',$actionOnTitleClick)."'>".$value."</a></td>";
			    }
			    else $html[] = "<td".$class.">".$value."</td>";
		    }
	    }
	    if(!in_array('actions',$hideColumns) and !$advancedOptions['printMode']){
		    // DISPLAY ACTION ICONS
		    //$linkClass = (isset($advancedOptions['ajaxActions']) && $advancedOptions['ajaxActions'])?" class='open_ajax'":"";
		    $linkClass = '';

		    //view link
		    if($viewLink != '') $html[] = "<td class='recordAction viewAction'><a".$linkClass." onclick='openRecord(\"".$viewLink."\",\"90%\",\"\", \"".$l10n->getLabel($recordClass,'record_name')." (".$row['uid'].")\");' title='".$l10n->getLabel('backend','view')."'><i class='glyphicon glyphicon-eye-open'></i></a></td>";

		    //edit link
		    if($editLink != '') $html[] = "<td class='recordAction editAction'><a".$linkClass." onclick='openRecord(\"".$editLink."\",\"90%\",\"".$advancedOptions['callBackOnRecordClose']."\", \"".$l10n->getLabel($recordClass,'record_name')." (".$row['uid'].")\");' title='".$l10n->getLabel('backend','edit')."'><i class='glyphicon glyphicon-pencil'></i></a></td>";


		    //sort link
		    if($sortUpLink != '') $html[] = "<td class='recordAction sortUpAction'><a onclick='sort_'.$recordEditTableID.'(\"".$sortUpLink."\");' title='".$l10n->getLabel('backend','sortUp')."'><i class='glyphicon glyphicon-arrow-up'></i></a></td>";
		    if($sortDownLink != '') $html[] = "<td class='recordAction sortDownAction'><a onclick='sort_'.$recordEditTableID.'(\"".$sortDownLink."\");' title='".$l10n->getLabel('backend','sortDown')."'><i class='glyphicon glyphicon-arrow-down'></i></a></td>";

		    //clone link
		    if($cloneLink != '')
			    $html[] = <<<EOT
                        <td class='recordAction cloneAction'><a onclick='clone_$recordEditTableID("$cloneLink");' title='{$l10n->getLabel('backend','clone')}'><i class='glyphicon glyphicon-duplicate'></i></a></td>
EOT;

		    foreach($customRecordActionLinks as $recordActionKey => $link){

			    if(isset($availableActions[$recordActionKey]['class']) && $availableActions[$recordActionKey]['class'] != '')$linkClass = 'class="'.$availableActions[$recordActionKey]['class'].'"';
			    else $linkClass = "";

			    if(isset($availableActions[$recordActionKey]['labelDomain']))$domain = $availableActions[$recordActionKey]['labelDomain'];
			    else{
				    $domain = '';
				    if($firstIteration)$mf->addWarningMessage("DBRecord->showRecordEditTable() : You did not specify [".$recordActionKey."]['labelDomain']");
			    }

			    if(isset($availableActions[$recordActionKey]['linkText']))
				    $linkText = $availableActions[$recordActionKey]['linkText'];
			    else{
				    $linkText = '';
				    if($firstIteration)$mf->addWarningMessage("DBRecord->showRecordEditTable() : You did not specify [".$recordActionKey."]['linkText']");
			    }

			    if(isset($availableActions[$recordActionKey]['linkTarget']))
				    $linkTarget = " target=\"" .$availableActions[$recordActionKey]['linkTarget'] . "\"";
			    else{
				    $linkTarget = '';
			    }

			    //$html[] = "<td class='recordAction editAction'><a".$linkClass." href='".$link."' title='".$l10n->getLabel($domain,$recordActionKey)."'>".$linkText."</a></td>";
			    $html[] = <<<EOT
                        <td class="recordAction"><a $linkClass href="$link"$linkTarget title="{$l10n->getLabel($domain,$recordActionKey)}">$linkText</a></td>
EOT;

		    }


		    //restore icon

		    //print_r($row);
		    //delete link
		    if($deleteLink != '' ||  $restoreLink !=''){
			    if($row['deleted']==0){
				    //deleted icon
				    $html[] = "<td class='recordAction deleteAction'><a data-controls-modal='windowTitleDialog_'.$recordEditTableID.'' data-backdrop='true' data-keyboard='true' href='".$deleteLink."' title='".$l10n->getLabel('backend','delete')."'><i class='glyphicon glyphicon-trash'></i></a></td>";
			    }
			    else{
				    $html[] = "<td class='recordAction restoreAction'><a data-controls-modal='windowTitleDialog_'.$recordEditTableID.'' data-backdrop='true' data-keyboard='true' onclick='restore_".$recordEditTableID."(\"".$restoreLink."\");' title='".$l10n->getLabel('backend','restore')."'><i class='glyphicon glyphicon-share-alt'></i></a></td>";
			    }

		    }
	    }
	    $html[] = "</tr>";

	    return implode(chr(10),$html);
    }

    /*
     * Gets executed before a record has been loaded
     * overload this function in you class and place custom code in it
     */
    function preLoad(){}

    /*
     * Gets executed after a record has been loaded
     * overload this function in you class and place custom code in it
     */
    function postLoad(){}

    /*
     * Gets executed before a record is being created for the first time
     * overload this function in you class and place custom code in it
     */
    function preCreate(){}

    /*
     * Gets executed after a record is being created for the first time
     * overload this function in you class and place custom code in it
     */
    function postCreate(){}

    /*
     * Gets executed before a record is being stored in the database, except when the record is created for the first time : use method preCreate in this case
     * overload this function in you class and place custom code in it
     */
    function preStore(){}

    /*
     * Gets executed after a record is being stored in the database, except when the record is created for the first time : use method postCreate in this case
     * overload this function in you class and place custom code in it
     */
    function postStore(){}


    /*
     * Gets executed before a record is being deleted
     * overload this function in you class and place custom code in it
     */
    function preDelete(){}

    /*
     * Gets executed after a record has been deleted
     * overload this function in you class and place custom code in it
     */
    function postDelete(){}

    /*
     * Gets executed before a record is being restored
     * overload this function in you class and place custom code in it
     */
    function preRestore(){}

    /*
     * Gets executed after a record has been restored
     * overload this function in you class and place custom code in it
     */
    function postRestore(){}

	/*
	 * Gets executed prior processing the form when it has been submited
	 * inside the formsManager::processForm() function
	 * Allows pre-processing the data in order to alter the data structure before injecting the submited data insite it.
	 */
	function preProcessForm(){}

	/*
	 * Gets executed after processing the form when it has been submited
	 * inside the formsManager::processForm() function.
	 * Replaces function onSubmit() which has become obsolete.
	 */
	function postProcessForm(){}


    /**
     * Lists all the records available in the class as SQL result rows
     * @return mixed an array of rows as returned by a SQL request
     */
    function getActiveRecords($language=''){
        global $mf,$pdo;

        if($language=='') $language = $mf->getDataEditingLanguage();

        $records = array();

        //preparing request only once
        $sql = "SELECT * FROM ".static::getTableName()." WHERE deleted = '0' AND language='".$language."' ORDER BY uid DESC"; //uid = :uid AND

        try{
            $stmt = $pdo->query($sql);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        return $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    }


    /**
     * This function must be overloaded with your custom code.
     * Returns an URL where the record can be previewed from the backend.
     * @return string
     */
    function getPreviewUrl(){
        return 'getPreviewUrl()/not/yet/overloaded/for/class/'.get_class($this);
    }

    /**
     * This function must be overloaded with your custom code.
     * Returns the URL for viewing the record from a frontend page
     * This function may return different URL values according to the context the record is being viewed :
     * Say you have a website with 3 main sections : foot trips, car trips, bike trips. You want to display a news record which is generic to the 3 sections, but you want it to be seen in each section.
     * So according to which section the news item is called from, getUrl() may return : /foot-trips/news/my-news-item.html or /car-trips/news/my-news-item.html
     * @return string
     */
    function getUrl(){
        return 'getUrl()/not/yet/overloaded/for/class/'.get_class($this);
    }

    /**
     * This function must be overloaded with your custom code.
     * Returns the MAIN URL for viewing the record from a frontend page
     * If a record may have different display URLs (see getUrl() function doc about this), often you must only supply one main / unique / canonical URL to search engines so you avoid duplicate contents
     * This function will return the canonical URL of your object
     * @return string
     */
    function getCanonicalUrl(){
        return 'getCanonicalUrl()/not/yet/overloaded/for/class/'.get_class($this);
    }


    /**
     * Check if a table exists in the current database.
     *
     * @return bool TRUE if table exists, FALSE if no table found.
     */
    static function tableExists() {
        return tableExists(static::getTableName());
    }

    //standard key processing functions for displaying in showRecordEditTable

    /**
     * Formats a date for displaying it in the showRecordEditTable
     * @param string $key current key name
     * @param string $value value of the key
     * @param string $locale currently displayed locale
     * @param array $row whole row data for the current record. Usefull if it is required to check the value of another key
     * @return string the formated date
     */
    static function formatDate($key, $value, $locale, $row){
        global $l10n;

        if($value=='')return'';

        $date = new DateTime($value);

        if(isset($GLOBALS['site_locales'][$locale]['php_date_format']))
            return $date->format($GLOBALS['site_locales'][$locale]['php_date_format']);
        else echo $l10n->getLabel('main','variable')." \$GLOBALS['site_locales']['".$locale."']['php_date_format']".$l10n->getLabel('main','not_defined_config').' '.$l10n->getLabel('main','for_locale').' "'.$locale.'"'.chr(10);
    }

    /**
     * Formats a datetime featuring milliseconds for displaying it in the showRecordEditTable
     * @param string $key current key name
     * @param string $value value of the key
     * @param string $locale currently displayed locale
     * @param array $row whole row data for the current record. Usefull if it is required to check the value of another key
     * @return string the formated date
     */
    static function formatDateTimeMs($key, $value, $locale, $row){
        if($value=='')return'';
        global $l10n;
        $date = new DateTime($value);
        if(isset($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']))
            return $date->format($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']);
        else echo $l10n->getLabel('main','variable')." \$GLOBALS['site_locales']['".$locale."']['php_datetime_ms_format']".$l10n->getLabel('main','not_defined_config').' '.$l10n->getLabel('main','for_locale').' "'.$locale.'"'.chr(10);
    }



    /**
     * Formats a datetime for displaying it in the showRecordEditTable
     * @param string $key current key name
     * @param string $value value of the key
     * @param string $locale currently displayed locale
     * @param array $row whole row data for the current record. Usefull if it is required to check the value of another key
     * @return string the formated date
     */
    static function formatDateTime($key, $value, $locale, $row){
        if($value=='' || $value=='0000-00-00 00:00:00')return'';
        global $l10n;
        $date = new DateTime($value);
        if(isset($GLOBALS['site_locales'][$locale]['php_datetime_format']))
            return $date->format($GLOBALS['site_locales'][$locale]['php_datetime_format']);
        else echo $l10n->getLabel('main','variable')." \$GLOBALS['site_locales']['".$locale."']['php_datetime_format']".$l10n->getLabel('main','not_defined_config').' '.$l10n->getLabel('main','for_locale').' "'.$locale.'"'.chr(10);
    }

    /**
     * Formats a time for displaying it in the showRecordEditTable
     * @param string $key current key name
     * @param string $value value of the key
     * @param string $locale currently displayed locale
     * @param array $row whole row data for the current record. Usefull if it is required to check the value of another key
     * @return string the formated date
     */
    static function formatTime($key, $value, $locale, $row){
        if($value=='')return'';
        global $l10n;
        $date = new DateTime($value);
        if(isset($GLOBALS['site_locales'][$locale]['php_time_format']))
            return date($GLOBALS['site_locales'][$locale]['php_time_format'], $value);
        else echo $l10n->getLabel('main','variable')." \$GLOBALS['site_locales']['".$locale."']['php_time_format']".$l10n->getLabel('main','not_defined_config').' '.$l10n->getLabel('main','for_locale').' "'.$locale.'"'.chr(10);
    }


    /**
     * Formats a timestamp for displaying it in the showRecordEditTable
     * @param string $key current key name
     * @param string $value value of the key
     * @param string $locale currently displayed locale
     * @param array $row whole row data for the current record. Usefull if it is required to check the value of another key
     * @return string the formated date
     */
    static function formatTimestamp($value, $locale, $row){
        global $l10n;
        $date = new DateTime('@'.$value);
        if(isset($GLOBALS['site_locales'][$locale]['php_datetime_format']))
            return date($GLOBALS['site_locales'][$locale]['php_datetime_format'], $value);
        else echo $l10n->getLabel('main','variable')." \$GLOBALS['site_locales']['".$locale."']['php_datetime_format']".$l10n->getLabel('main','not_defined_config').' '.$l10n->getLabel('main','for_locale').' "'.$locale.'"'.chr(10);
    }


    static function getDeletedName($key,$value,$locale,$row){
        global $l10n;
        if($value == '1')return '<span class="mf_deleted">'.$l10n->getLabel('dbRecord','deleted').'</span>';
        else return '<span class="mf_active">'.$l10n->getLabel('dbRecord','active').'</span>';

    }

	static function listDownloads($key,$value,$locale,$row){
		global $mf;

    	$currentFiles = unserialize($value);
		$filesStr = '';

		if(isset($currentFiles) && is_array($currentFiles)) {
			foreach ($currentFiles as $file) {
				$fileName = $file['filename'];
				$filesStr .= '<a class="downloadLink" href="' . $mf->getUploadsDir() . $file['filepath'] . $file['filename'] . '" target="_blank"><i class="glyphicon glyphicon-download-alt"></i> ' . $fileName . '</a><br />';
			}
		}
		return $filesStr;
	}


//    function makeSlug($value, $increment=0) {
    function makeSlug($value) {
        global $pdo;

        return GenerateUrl($value).'-'.$this->data['uid']['value'];

//        if($increment > 0)$slugValue = $slugValue.'-'.$increment;
//
//        //if this record already has an uid (update record), the slug might already exist so exclude it from the search
//        if ($this->data['uid']['value'] != '' ) { $where = " AND uid != '".$this->data['uid']['value']."' "; } else { $where = ''; }
//
//        //search for already existing slug
//        $sql = "SELECT count(*) FROM ".static::getTableName()." WHERE slug = '".$slugValue."' ".$where;
//
//        $stmt = $pdo->query($sql);
//
//        //count existing slugs with the same value
//        $totalRecordCount = $stmt->fetchColumn();
//
//        if ($totalRecordCount > 0) {
//            //slug already exists, append increment to it to make it unique
//            $increment++;
//            return $this->makeSlug($value, $increment);
//        } else {
//            return $slugValue;
//        }
    }



    /**
     * Searches the given $searchValue into the given $tableName and tries to match it with the specified $fieldNames. This function is intended to be executed from the recordSelectField search function
     *
     * @param $searchValue string the value we are searching for
     * @param $formID string the id attribute associated with the form the recordSelectField search function is being executed from
     * @param $fulltextFields string coma separated list of fulltext fields/columns being searched in
     * @param $equalFields string coma separated list of varchar fields/columns being searched in
     * @param $orderByFields string coma separated list fields/columns to sort the result by
     * @param $titleFields string coma separated list of fields to display as title for the found items in the search result set
     * @param $tableName string the table name in the SQL database of the type of record being investigated
     * @param $recordClass string the class name of the type of record being investigated
     * @param $callingFieldName string the field name (or you may call it key) of the record from which the search was launched
     * @param $callingRecordClass string the class name of the record from which the search was launched
     * @return array
     */
    static function searchRecordTypeahead($searchValue,$formID,$fulltextFields,$varcharFields,$equalFields,$searchWhere,$orderByFields,$titleFields,$tableName,$recordClass,$callingFieldName,$callingRecordClass,$debug=false){
        global $mf,$l10n,$pdo,$logger;

        if(in_array($debug,array('false','',0,'0',false)))$debug=false;
        if(in_array($debug,array('true',1,'1',true)))$debug=true;
        //$query = $pdo->quote('%'.$searchValue.'%');

        $fulltextFieldsEnabled = (trim($fulltextFields) != '');
        $varcharFieldsEnabled = (trim($varcharFields) != '');
        $equalFieldsEnabled = (trim($equalFields) != '');


        //deal with fulltext search
        $fulltextFieldString = '';
        if($fulltextFieldsEnabled){
        	//a '-' character in the end raises the followin error : 1064 syntax error, unexpected $end, expecting FTS_TERM or FTS_NUMB or '*'
	        //strip the endin '-' if present
	        if(substr($searchValue, -1) == '-')$searchValue = substr($searchValue, 0, strlen ( $searchValue )-1);

            $fulltextFieldString = 'MATCH('.$fulltextFields.') AGAINST('.$pdo->quote($searchValue.'*').' IN BOOLEAN MODE)';
            $fulltextFieldString = rtrim($fulltextFieldString,' ');
            //if($fulltextFieldString != '') $fulltextFieldString = (($equalFieldString != '')?' OR ':'').$fulltextFieldString;
        }

        //deal with varchar search
        $varcharFields = explode(',',trim($varcharFields));
        $varcharFieldString = '';
        if($varcharFieldsEnabled){
            $isFirst = true;
            foreach($varcharFields as $fieldName){
                $varcharFieldString .= (($isFirst)?' ':' OR ').$fieldName.' LIKE '.$pdo->quote('%'.$searchValue.'%');
                $isFirst = false;
            }
            $varcharFieldString = rtrim($varcharFieldString,' ');
            if($varcharFieldString != '') $varcharFieldString = (($fulltextFieldString != '')?' OR ('.$varcharFieldString.')': '('.$varcharFieldString.')');
        }

        //deal with strict equality search
        $equalFields = explode(',',trim($equalFields));
        $equalFieldString = '';
        if($equalFieldsEnabled){
            $isFirst = true;
            foreach($equalFields as $fieldName){
                $equalFieldString .= (($isFirst)?'':' OR ').$fieldName.' = '.$pdo->quote($searchValue);
                $isFirst = false;
            }
            $equalFieldString = rtrim($equalFieldString,' ');
            if($equalFieldString != '') $equalFieldString = (($fulltextFieldString != '' || $varcharFieldString != '')?' OR ('.$equalFieldString.')':'('.$equalFieldString.')');
        }

        if(trim($orderByFields) != '')$orderBy = " ORDER BY ".$orderByFields;
        else $orderBy='';

        $whereString = '';
        if($searchWhere !=''){
            $whereString = ' AND '.$searchWhere;
        }

        try {
	        $sql = "SELECT DISTINCT uid" . ( ( trim( $titleFields ) != '' ) ? ',' . $titleFields : '' ) . " FROM " . $tableName . " WHERE " . $fulltextFieldString . $varcharFieldString . $equalFieldString . $whereString . " AND deleted=0" . $orderBy;
	        if ( $debug ) {
		        echo chr( 10 ) . chr( 10 ) . $sql . chr( 10 ) . chr( 10 );
	        }

	        $stmt = $pdo->query( $sql );
	        $rows = $stmt->fetchAll( PDO::FETCH_ASSOC );
        }
			catch(PDOException $e){
			$logger->critical("searchRecordTypeahead PDOException code=".$e->getCode()." message=".$e->getMessage());
			$logger->critical($sql);
		}
        $items = array();

        $titleFields = explode(',',$titleFields);


        if(count($rows) > 0){

            //we need to access the calling record data structure in order to look for the keyProcessors defined in the calling field. Let's create one, for this purpose
            $record = new $callingRecordClass();

            foreach($rows as $row){
                $title = '';
                foreach($titleFields as $fieldName){
                    $fieldName = trim($fieldName," \t\n\r\0\x0B`");
                    if(isset($record->data[$callingFieldName]['keyProcessors'][$fieldName])){
                        $funcName = $record->data[$callingFieldName]['keyProcessors'][$fieldName];
                        $funcArg = $row[$fieldName];

                        eval('$modTitle = '.$funcName.'($funcArg);');

                        $title .= $modTitle.' ';
                    }
                    else $title .= $row[$fieldName]." ";


                }
                $title = trim($title);

                $viewLink = makeHTMLActionLink('viewRecord', $recordClass,$row['uid'], false).'&formID='.$formID.'&key='.$fieldName;
                $editLink = makeHTMLActionLink('editRecord', $recordClass,$row['uid'], false).'&formID='.$formID.'&key='.$fieldName;
                $deleteLink = makeJSONActionLink('deleteRecord', $recordClass,$row['uid'], true);

                $items[$row['uid']] = array(
                    'title' => $title,
                    'viewLink' => $viewLink,
                    'editLink' => $editLink,
                    'deleteLink' => $deleteLink,
                );
            }
        }
        else{
            $items[1] = array(
                'title' => $l10n->getLabel('dbRecord','no_result'),
                'viewLink' => '#',
                'editLink' => '#',
                'deleteLink' => '#',
            );
        }
        return $items;
    }

/*
    static function getClassName(){
        return get_called_class();
    }*/

    /**
     * Clones a dbRecord, but does not store it. It is the responsability of the user to call ->store() on the new object.
     * Also if there are files linked to the record, the developper overloading this function should copy these, and adjust file paths to the new record directory.
     */
    function __clone(){
        // erase uid so a new database entry will be created on the next store()
        $this->data['uid']['value']='';
    }

    /*
     * Returns the files / upload directory for this record, relative to the uploads directory
     * Concatenate the returned value with constant (DOC_ROOT OR $_SERVER['DOCUMENT_ROOT']) AND $mf->getUploadsDir() to get the absolute path
     * IMPORTANT : the record needs to be already stored once so its uid is available
     * The directory is automatically created in the process if it doesn't exist yet.
     *
     * @return string the directory
     */
    function getFilesDir(){
        global $mf,$config;

        $thisClass = get_class($this);

        //we make 1 directory per record named like 'class_uid'
        $recordDir = $thisClass.DIRECTORY_SEPARATOR.$thisClass.'_'.$this->data['uid']['value'].DIRECTORY_SEPARATOR;

        if (!is_dir ($recordDir)) {
            @mkdir(DOC_ROOT.$mf->getUploadsDir().$recordDir, 0775,true);
        }
        return $recordDir;
    }


	/**
	 * Adds some code to be executed when a hook action is called on some class. This is especially usefull for third party plugins which may want to execute an action when a known record type is saved, for example a page record.
	 * The called function should take 1 argument, which will be feeded with the current record instance.
	 * Sample called function profile :
	 *
	 * static function onPageStore($thisPage, $myParameter1, $myParameter2){
	 *      //process the record data
	 *      $thisPage->data['title']['value'] = $myParameter1.$myParameter2;
	 * }
	 *
	 * Call the hook in your plugin's index.php file
	 * dbRecord::addHook('postStore', 'page', 'myClass::onPageStore', array($myParameter1Value, $myParameter2Value));
	 *
	 * @param string $triggerName can be any of 'preCreate','postCreate','preStore','postStore','preLoad','postLoad','preDelete','postDelete','preRestore','postRestore'
	 * @param string $recordClass the class name of the record which fires the hook
	 * @param string $callback name of the function to be invoked. The first parameter of the function will receive the current object activating the hook.
	 * @param array $parameters an array of parameters for the function. IMPORTANT : an instance of the current object will be automatically injected prior the first parameter.
	 * @param array $locales an array containing a list of locales ex : ['fr','en']. Use this if you wan to restrict triggering the hook to some locales, pass an empty array or null otherwise
	 */
    static function addHook($triggerName, $recordClass, $callback, $parameters, $locales = null){

    	if(!is_array(self::$hooks[$triggerName]))self::$hooks[$triggerName] = array();
	    if(!isset(self::$hooks[$triggerName][$recordClass]))self::$hooks[$triggerName][$recordClass] = array();

	    self::$hooks[$triggerName][$recordClass][] = array('callback' => $callback, 'parameters' => $parameters, 'locales' => $locales);
    }

	/**
	 * Removes a previously added triggerHook.
	 *
	 * @param string $triggerName can be any of 'preCreate','postCreate','preStore','postStore','preLoad','postLoad','preDelete','postDelete','preRestore','postRestore'
	 * @param string $recordClass the class name of the record which fires the hook
	 * @param string $callback name of the function to be removed
	 */
    static function removeHook($triggerName, $recordClass, $callback)
    {
    	if(isset(self::$hooks[$triggerName][$recordClass])){
	        foreach(self::$hooks[$triggerName][$recordClass] as $index => $funcDef){
	            if($funcDef['callback'] == $callback)
	                unset(self::$hooks[$triggerName][$recordClass][$index]);
		    }
    	}
    }

	/**
	 * Executes associated hooks for the given hook action name
	 * @param string $triggerName can be any of 'preCreate','postCreate','preStore','postStore','preLoad','postLoad','preDelete','postDelete','preRestore','postRestore'
	 */
    private function playHooks($triggerName)
    {
    	global $mf;

    	if(isset(self::$hooks[$triggerName][static::class])){
		    foreach(self::$hooks[$triggerName][static::class] as $index => $userFunc){

		    	if(isset($userFunc['locales'])){

		    		if(is_array($userFunc['locales'])) {

					    //if the array of locales is empty, call for any locale
					    if(count($userFunc['locales']) == 0) {
						    call_user_func_array( $userFunc['callback'], array_merge( array( $this ), $userFunc['parameters'] ) );
					    }

		    			//else call if the locale matches
					    else if(in_array($mf->getlocale(),$userFunc['locales'])){

							    call_user_func_array( $userFunc['callback'], array_merge( array( $this ), $userFunc['parameters'] ) );
					    }

				    }
			    }
			    //if no locales are specified, call for any locale
			    else call_user_func_array( $userFunc['callback'] , array_merge(array($this),$userFunc['parameters']));


		    }
	    }
    }

	/**
	 * Removes the fields specified in array $fieldNames from $tabName in order to prevent their display
	 * @param string $tabName name of the tab containing the field. If the field is displayed prior tabs, set $tabName to ''.
	 * @param array $fieldNames containing the fieldnames to be removed
	 */
    function showInEditFormRemoveField($tabName,$fieldNames){
	    $this->showInEditForm[$tabName] = array_diff($this->showInEditForm[$tabName], $fieldNames);
    }

	/**
	 * Inserts the specified fields after the $fieldAfter field in the specified tab. Set $fieldAfter to 0 if you need to perform insertion in the first position.
	 * @param string $tabName name of the tab containing the field. If the field is displayed prior tabs, set $tabName to ''.
	 * @param array $fieldNames containing the fieldnames to be added
	 * @param string $fieldAfter name of the field after which the insertion should be performed. Set $fieldAfter to 0 if you need to perform insertion in the first position.
	 */
	function showInEditFormInsertField($tabName,$fieldNames,$fieldAfter){

    	if($fieldAfter === 0) {
    		$offset = 0;
	    }
    	else{
		    //seek $fieldAfter's position
		    $offset = array_search($fieldAfter, $this->showInEditForm[$tabName]);
	    }

		if($offset){
			//insert the elements
    		$input = $this->showInEditForm[$tabName];
			$this->showInEditForm[$tabName] = array_splice($input, $offset+1, 0, $fieldNames);
			$this->showInEditForm[$tabName] = $input;
		}

	}

	/**
	 * Appends the specified fields at the end of the specified tab
	 * @param string $tabName name of the tab containing the field. If the field is displayed prior tabs, set $tabName to ''.
	 * @param array $fieldNames containing the fieldnames to be added
	 */
	function showInEditFormAppendField($tabName,$fieldNames){
		foreach($fieldNames as $fieldName) {
			$this->showInEditForm[ $tabName ][] = $fieldName;
		}
	}


	static function formatCreationDate($key,$value,$locale,$additionalValue=''){
		global $mf,$l10n,$logger;

		$locale = $mf->getLocale();

		if($value=='' || $value=='0000-00-00 00:00:00')return'';
		global $l10n;

		$date = DateTime::createFromFormat($GLOBALS['site_locales'][$locale]['php_datetime_ms_format'], $value);
		if(isset($GLOBALS['site_locales'][$locale]['php_datetime_format']))
			return $date->format($GLOBALS['site_locales'][$locale]['php_datetime_format']);
		else echo $l10n->getLabel('main','variable')." \$GLOBALS['site_locales']['".$locale."']['php_datetime_format']".$l10n->getLabel('main','not_defined_config').' '.$l10n->getLabel('main','for_locale').' "'.$locale.'"'.chr(10);
	}

	static function formatCreationDateKP($value) {
		global $mf;
		dbRecord::formatCreationDate( '', $value, $mf->getLocale(), '' );
	}

	/**
	 * Returns a list of formFields containing all the changes added since and including the specified timestamp
	 * @param $record the record instance to work on
	 * @param string $requestedFieldName if a fieldname is specified, the function will work on the history of this field only
	 * @param boolean $includeSimpleSaves Normaly, saves without affecting any fields are ignored. Setting this flag to true shows them in the list
	 *
	 * @return array
	 */
	static function listHistoryEntries($record,$requestedFieldName=null, $includeSimpleSaves=false){

		$history = $record->data['history_past']['value'];
		$historyEntriesList = array();

		$recordClass = get_class($record);
		$blankRecord = new $recordClass();

		//print_r($history);

		// when processing history for a single fieldName
		// select only $requestedFieldName, but also allow 'modificator_uid','modificator_class','creator_uid','creator_class' to go thru because we need this info too
		$allowedFieldNames = [
			$requestedFieldName,
			'modificator_uid',
			'modificator_class',
			'creator_uid',
			'creator_class'
		];

		//convert $record->showInEditForm values into a 1D array
		if(! isset($requestedFieldName)){
			$showInEditForm = array();

			foreach($record->showInEditForm as $tab=>$tabData){
				$showInEditForm = array_merge($showInEditForm,$tabData);
			}
		}

		//parsing history entries
		foreach ( $history as $timestamp => $fields ) {

			//parsing modified fields inside history entries
			foreach ( $fields as $fieldName => $fieldHistoryData ) {

				if(isset($requestedFieldName)) {
					// when processing history for a single fieldName, filter most other fields
					$processThisField = array_key_exists( $requestedFieldName, $fields ) && in_array( $fieldName, $allowedFieldNames );
				}
				else {
					// when processing history for a record, process all fields available in the record's form
					if(in_array( $fieldName, $showInEditForm ) || in_array( $fieldName, $allowedFieldNames ))$processThisField = true;
					else $processThisField = false;
				}

				if ($processThisField) {

					if ( isset( $fieldHistoryData['new'] ) ) {
						$index = 'new';
					} else if ( isset( $fieldHistoryData['upd'] ) ) {
						$index = 'upd';
					} else if ( isset( $fieldHistoryData['old'] ) ) {
						$index = 'old';
					}

					//init field data with record's data, so we are sure the field is fully defined (history only stores the differences)
					if ( ! isset( $historyEntriesList[ $timestamp ] ) ) {
						$historyEntriesList[ $timestamp ] = array();
					}

					if ( isset( $record->data[ $fieldName ] ) ) {
						$historyEntriesList[ $timestamp ][ $fieldName ] = $record->data[ $fieldName ];
						//apply default value
						if ( isset( $blankRecord->data[ $fieldName ] ) ) {
							$historyEntriesList[ $timestamp ][ $fieldName ]['value'] = $blankRecord->data[ $fieldName ]['value'];
						}
					}

					if ( $index == 'new' ) {
						//field creation
						foreach ( $fieldHistoryData['new'] as $fieldAttribute => $fieldValue ) {
							//						if(!is_array($fieldValue))echo "historyEntriesList[$timestamp][$fieldName][$fieldAttribute] = ".$fieldValue.chr(10);
							//						else echo "historyEntriesList[$timestamp][$fieldName][$fieldAttribute] = ".print_r($fieldValue,true).chr(10);
							$historyEntriesList[ $timestamp ][ $fieldName ][ $fieldAttribute ] = $fieldValue;
						}
					} else if ( $index == 'upd' ) {

						//field attribute update
						foreach ( $fieldHistoryData['upd'] as $fieldAttribute => $fieldValue ) {
							if ( $fieldAttribute == 'value' ) {
								if ( isset( $fieldValue['new'] ) ) {
									$historyEntriesList[ $timestamp ][ $fieldName ][ $fieldAttribute ] = $fieldValue['new'];
								} else if ( isset( $fieldValue['old'] ) ) {
									unset( $historyEntriesList[ $timestamp ][ $fieldName ][ $fieldAttribute ] );
								}
							}
						}
					} else if ( $index == 'old' ) {
						//field deletion
						unset( $historyEntriesList[ $timestamp ][ $fieldName ] );
					}
				}
			}

		}



//print_r($historyEntriesList);
		return $historyEntriesList;
	}

	/**
	 * Returns the list of formFields featuring their current state at the specified timestamp moment
	 * @param $getTimestamp the timestamp to work on
	 * @param $record the record instance to work on
	 * @param string $fieldName if a fieldname is specified, the function will work on the history of this field only
	 *
	 * @return array
	 */
	static function getHistoryVersion($getTimestamp,$record,$fieldName=null){
		global $mf,$l10n;

		if($fieldName=='')$fieldName=null;

		$history = self::listHistoryEntries($record,$fieldName);

		//print_r($history);

		if($fieldName) {
			if ( isset( $history[ $getTimestamp ] ) ) {

				$fieldData =  array($fieldName => $history[ $getTimestamp ][$fieldName]);

				//we do not want to show the history button towards another level and we do not want the field to be editable
				$fieldData[$fieldName]['showFieldHistory'] = false;
				$fieldData[$fieldName]['editable'] = false;

				return $fieldData;
			} else {
				return null;
			}
		}
		else{
			if ( isset( $history[ $getTimestamp ] ) ) {
				$fields =  $history[ $getTimestamp ];

				foreach($fields as $fieldName => $fieldData){
					//we do not want to show the history button towards another level and we do not want the field to be editable
					$fields[$fieldName]['showFieldHistory'] = false;
					$fields[$fieldName]['editable'] = false;
				}
				return $fields;

			} else {
				return null;
			}
		}

	}

}
