<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Defines a recordEditTable field, allowing to display a list of records from another table inside a form field. recordEditTable and recordSelectField allow to create a one to may relationship.
 * Unlike the recordSelect field, the recordEditTable field retains no value in the current record, it only displays a view on a list of records matching the SQL query defined for this field.
 * It specifically has the advantage of providing all of the showRecordEditable() function features, such as showing a finely configured table of records, with it values interpreted using keyprocessors.
 */
class recordEditTableField implements formField
{
    /**
     * Returns the HTML code for displaying the field, initialized with the data supplied in $fieldData
     *
     * Creates a select box populated with records from other tables. Records can be edited, deleted, copied, pasted in the same field or an other field displaying the same type of records. Records can be created too.
     * All these features can be enabled / disabled by configurable options.
     * VERY IMPORTANT : the added record (a dbRecord extended class) MUST feature a column/field named parent_uid of type int(11), which is required for the copy paste operations.
     * This column will/must contain the uid of the parent record where the current field is displayed as an option in a select field.
     *
     * @param $f fieldData : this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
     * $f->formID           // value of the id attribute of the <form> tag this field is included in
     * $f->mode             // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
     * $f->dataType         // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
     * $f->key              // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
     * $f->fieldData        // this is the data array of the current field
     * $f->recordUid        // the uid (Unique ID) of the record containing the current field
     * $f->recordClassName  // the class name of the record containing the current field
     * $f->record           // all the data for the record containing the current field
     * $f->microKey         // field name when processing a microTemplate field (only used when mode=="micro|")
     * $f->microIndex       // sorting index when processing a microTemplate field (only used when mode=="micro|")
     * @returns String a string containing the HTML for displaying the field
     */
    static public function displayField($f){


        //create an instance of our record so we are sure its l10n file is loaded (often it is not).
        $editedRecord = new $f->fieldData['editClass']();

        $html = array();
        //$dataType = $f->fieldData['dataType'];
        $fieldAttr = formsManager::getTagAttributes('field',$f->key, $f->fieldData);
        $divAttr = formsManager::getTagAttributes('div',$f->key, $f->fieldData);

        //display the label in front of the field or not
        $showLabel      = (isset($f->fieldData['showLabel']))?      $f->fieldData['showLabel']      :true;

        //display a <div> around the field or not
        $showFieldDiv   = (isset($f->fieldData['showFieldDiv']))?   $f->fieldData['showFieldDiv']   :true;

        //display custom HTML in front of the field
        $preFieldHTML   = (isset($f->fieldData['preFieldHTML']))?   $f->fieldData['preFieldHTML']   :'';

        //display custom HTML in after the field
        $postFieldHTML  = (isset($f->fieldData['postFieldHTML']))?  $f->fieldData['postFieldHTML']  :'';

        //display custom HTML in front of the field
        $preDivHTML   = (isset($f->fieldData['preDivHTML']))?   $f->fieldData['preDivHTML']   :'';

        //display custom HTML in after the field
        $postDivHTML  = (isset($f->fieldData['postDivHTML']))?  $f->fieldData['postDivHTML']  :'';

        if(isset($f->fieldData['fullWidth']))$fullWidth = filterBoolean($f->fieldData['fullWidth']);
        else $fullWidth = false;

        //input editing
        if(isset($f->fieldData['icon']))$icon = $f->fieldData['icon'];
        else $icon = '<b class="glyphicon glyphicon-list-alt"></b>';

        if($fullWidth) $html[] = '<div class="row">';
        if($showLabel)$html[] = formsManager::getFieldLabel($f->fieldData, $f->mode, $f->key, $f->recordClassName, !$fullWidth);

        if($fullWidth) $html[] = '</div>';

        if(!isset($f->fieldData['debugSQL']))$f->fieldData['debugSQL']=0;
        if(!isset($f->fieldData['keyProcessors']))$f->fieldData['keyProcessors']=array();

        if(!isset($f->fieldData['editable']))$f->fieldData['editable']=true;
	    $isEditable = filterBoolean($f->fieldData['editable']);

        if(!$isEditable) {
            $f->fieldData['selectActions']['create'] = 0;
            $f->fieldData['selectActions']['edit'] = 0;
            $f->fieldData['selectActions']['delete'] = 0;
            $f->fieldData['selectActions']['clone'] = 0;
        }

	    $f->fieldData['advancedOptions']['copyPasteToParentClass'] = $f->recordClassName;
	    $f->fieldData['advancedOptions']['copyPasteToParentUid'] = $f->recordUid;

        //$editedRecord = new $f->fieldData['editClass']();

        if($fullWidth) $html[] = '<div class="row">';
        $html[] = $preDivHTML;
        $html[] = ($showFieldDiv)?'<div class="input-group input-group-mf-padding '.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';

        $html[] = $editedRecord->showRecordEditTable(
            $f->fieldData['selectItemsSQL'],
            $f->fieldData['moduleName'],
            $f->fieldData['subModuleName'],
            $f->fieldData['mainKey'],
            $f->fieldData['keyProcessors'],
            $f->fieldData['page'],
            $f->fieldData['selectActions'],
            $f->fieldData['advancedOptions'],
            ((isset($f->fieldData['recordEditTableID']))?$f->fieldData['recordEditTableID']:null)
        );

        $html[] = ($showFieldDiv)?'</div>':'';
        $html[] = $postDivHTML;
        if($fullWidth) $html[] = '</div>';
        //$html[] = '<span class="errorMsg col-lg-offset-4 col-lg-8"></span>';

        return implode(chr(10),$html);
    }













    /**
     * Process the field value prior saving it to the database, whenever inner value adaptation is required, for SQL strict compliance for example.
     * This is executed after 'preRecord' processing functions.
     * @param $fieldData this is the data array of the current field
     * @param $value this is the submited value
     * @return mixed
     */
    static public function onSave($fieldData, $value){
        //convert '' to 0 for strict SQL compliance
        /*if(is_array($value)){
            if(count($value)==1 && $value[0]=='') $value=0;
            else return $value;
        }
        else if($value=='')return 0;
        else return $value;*/
        return $value;
    }

    static public function getCSS(){return "";}
    static public function getTopJS(){return "";}
    static public function getBottomJS(){return "";}







}
