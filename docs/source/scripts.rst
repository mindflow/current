.. |vspace| raw:: latex

   \vspace{5mm}

.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>
	
|embed_js|

***********
Les scripts
***********

Génèrer un autre type de code que du HTML
-----------------------------------------

On peut utiliser MindFlow pour générer du XML, du JSON, du SVG etc.

Dans ce cas, on utilisera le pipeline de rendu différement. Tout d'abord, ce n'est plus une page HTML qui sera appellée, mais un script PHP personnalisé. 

Voici par exemple comment le plugin ``pages`` génère un **sitemap.xml** du site. Le fichier ``/mf/plugins/pages/sitemap-pages.php`` est définit comme suit :

.. code-block:: php

	<?php

	require_once '../../../mf_config/config.php';
	require_once DOC_ROOT.SUB_DIR.'/mf/frontend/frontend.php';
	require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

	//on instancie MindFlow
	$mindFlowFrontend = new frontend();

	$mf = $mindFlowFrontend->mf;
	$l10n = $mf->l10n;
	$pdo =$mf->db->pdo;
	$host = getHTTPHost();

	//on génère les données en exécutant les plugins
	$mindFlowFrontend->prepareData();

	// en revanche, à partir d'ici, on n'appelle plus la fonction render() de Mindflow
	// on génère notre propre rendu au format XML
	
	//On crée les entêtes adaptés pour servir du  XML
	header("Content-type: text/xml");
	echo '<?xml version="1.0" encoding="UTF-8" ?>';

	$xml=array();

	$xml[] =  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

	// on consulte nos données dans la base (ceci n'a pas été fait dans prepareData() dans ce cas précis
	// mais on aurait pu ...
	$sql = "SELECT uid, modification_date,changefreq,priority,hide_lastmod FROM mf_pages where deleted=0 AND hidden=0 AND hide_from_sitemap=0";
	$stmt = $pdo->query($sql);
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// on génère nos lignes de résultats XML
	foreach($rows as $row){

		$xml[] = '  <url>';
		$xml[] = '      <loc>'.$host.$mf->modules->urlRewriter->getPageURL($row['uid']).'</loc>';
		if($row['hide_lastmod']==0)$xml[] = '<lastmod>'.strstr($row['modification_date'],' ',true).'</lastmod>';
		if(trim($row['changefreq'])!='')$xml[] = '<changefreq>'.$row['changefreq'].'</changefreq>';
		if(trim($row['priority'])!='')$xml[] = '<priority>'.$row['priority'].'</priority>';
		$xml[] = '  </url>';
	}

	$xml[] =  '</urlset>';

	// on gère l'affichage du résultat sans passer par MindFlow
	echo  implode(chr(10),$xml);

	?>
	
On pourra éventuellement ajouter un peu de réécriture d'URL au fichier .htaccess pour donner l'illusion qu'on appelle bien un fichier XML et non un script PHP :

.. code-block:: html

	RewriteEngine On
	RewriteRule ^sitemap\.xml/?$ /mf/plugins/pages/sitemap-pages.php [NC,L]
	
|
|
|
|
	
Créer des scripts CLI
---------------------

Il est souvent pratique d'exécuter des scripts CLI, que ce soit pour importer des données depuis une autre base de données, pour convertir / réinterprêter des données, pour exécuter des taches cron à intervalles régulier, établir des communications avec d'autres hôtes avec CURL, modifier et sauver les titres de toutes les pages présentes dans la base... les possibilités sont immenses.

Le fonctionnement d'un tel script est relativement trivial et se passe d'explication. L'exemple suivant va lire le contenu d'une table nommée ``static_countries`` listant les noms et propriétés de tous les pays du monde, puis créer et sauver un objet mfCountry dans MindFlow pour chaque entrée de la table :

.. code-block:: php
	
	<?php

	$execution_start = microtime(true);

	// Lors de l'exécution en mode CLI, PHP n'assigne pas d'hôte.
	// Il est nécessaire d'en indiquer un pour lui permettre d'aller chercher le bon fichier de configuration
	$_SERVER['HTTP_HOST'] = 'www.sample-website.com';

	require_once '../../mf_config/config.php';

	echo "DOC_ROOT=".DOC_ROOT.chr(10);
	echo "SUB_DIR=".SUB_DIR.chr(10);

	require_once DOC_ROOT.SUB_DIR.'/mf/core/dbRecord.php';
	require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_countryData/records/mfCountry.php';

	require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
	require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';


	$mindFlowBackend = new backend();
	$mindFlowBackend->prepareData();
	$mf = $mindFlowBackend->mf;
	$l10n = $mf->l10n;
	$pdo = $mf->db->pdo;

	//on fait un select sur la table static_countries
	$sql = "SELECT * FROM static_countries WHERE 1";
	$stmt = $pdo->query($sql);

	$countries = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$compteur = 0;

	//on crée nos objets MindFlow
	foreach($countries as $country){
	
		$newCountry = new mfCountry();
		$newCountry->loadByISO2($country['cn_iso_2']);

		$newCountry->data['cn_official_name_local']['value'] = $country['cn_official_name_local'];
		$newCountry->data['cn_official_name_en']['value'] = $country['cn_official_name_en'];
		$newCountry->data['cn_capital']['value'] = $country['cn_capital'];
		$newCountry->data['cn_short_local']['value'] = $country['cn_short_local'];
		$newCountry->data['cn_short_en']['value'] = $country['cn_short_en'];
		$newCountry->data['cn_short_fr']['value'] = $country['cn_short_fr'];
		$newCountry->store();

		$compteur++;
	}

	$execution_end = microtime(true);
	echo "Processed : ".$compteur." pays.".chr(10).chr(10);
	echo "Execution time : ".(($execution_end-$execution_start))." seconds.".chr(10).chr(10);

	?>

Dans l'exemple ci-dessus nous avions pré-importé la table static_countries dans la base de données MindFlow plutôt que de monter une base de données secondaire. Mais il est tout à fait possible d'ouvrir une seconde base de données comme source pour migrer nos données. Dans ce cas on procèdera comme suit :

.. code-block:: php

	$mindFlowBackend = new backend();
	$mindFlowBackend->prepareData();
	$mf = $mindFlowBackend->mf;
	$l10n = $mf->l10n;
	
	// A ce stade, MindFlow a déjà créé son propre dbManager $mf->db et chargé sa base de données accessible via la variable $mf->db->pdo
	// Créons un second dbManager avec une seconde base
	$this->dbHost = 'mysql.myHosting.com';
	$this->dbPort = '3306';
	$this->dbName = 'my_db_name';
	$this->dbUsername = 'my_db_username';
	$this->dbPassword = 'my_db_password';
	$this->forceUtf8 = true;
	
	$mf->dbSource = new dbManager($dbHost, $dbPort, $dbName, $dbUsername, $dbPassword, $forceUtf8);
	
	//on ouvre la base de données source
	$mf->dbSource->openDB();
	
	//on peut maintenant faire des requêtes pour lire nos données sur cette base :
	$sqlSource = "SELECT * FROM static_countries WHERE 1";
	$stmt = $mf->dbSource->pdo->query($sqlSource);
	$countries = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//puis on peut écrire dans la BDD de MindFlow :
	foreach($countries as $country){
		$sqlTarget = "UPDATE ... SET ... WHERE ....";
		$stmt =  $mf->db->pdo->query($sqlTarget);
	}
	

	
	

	