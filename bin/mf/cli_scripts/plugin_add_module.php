<?php

/**
 * This wizards assists you in creating a new module.
 * It is possible to specify in the command line a config file featuring the preconfigured used variables in order to avoid having to seize them at every run when configuring the wizard.
 * In this cas, call the script using the following syntax :
 *
 * php plugin_add_module.php config_file.php
 *
 * See file sample-configs/agPlugin_config.php for the available variables to configure.
 */

//http://php.net/manual/en/features.commandline.php

require_once "colors.php";
require_once "functions.php";

// Create new Colors class
$colors = new Colors();

if(isset($argv[1])){
    echo chr(10);
    if(is_file ( $argv[1] )) {
        echo $colors->getColoredString("config file specified, loading defaults from the file...", 'cyan', null);
        include $argv[1];
        echo $colors->getColoredString("done", 'cyan', null).chr(10).chr(10);
    }
    else echo $colors->getColoredString("Specified config.php file not found, running 100% interactive mode.", 'dark_gray', null).chr(10).chr(10);
}


echo "Natural name of the module you want to create ? [My module] : ";
$prefix = chr(10);
if(!isset($moduleNaturalName)){
    $moduleNaturalName = trim(fgets(STDIN));
    $prefix = '';
}
if($moduleNaturalName=='')$moduleNaturalName = 'My module';

echo $prefix.$colors->getColoredString("Module will be named \"".$moduleNaturalName."\"", 'cyan', null).chr(10).chr(10);

//suggest class name from moduleNaturalName
$nameChunks = explode(' ',$moduleNaturalName);
$isFirst=true;
$suggestedName = '';
foreach($nameChunks as $chunk){
    if($isFirst){
        $suggestedName = strtolower($chunk);
        $isFirst=false;
    }
    else $suggestedName .= ucfirst($chunk);
}
$suggestedName=trim($suggestedName);

echo "Class name of the module you want to create ? [".$suggestedName."] : ";
$prefix = chr(10);
if(!isset($moduleClassName)){
    $moduleClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($moduleClassName=='')$moduleClassName = $suggestedName;
echo $prefix.$colors->getColoredString("Module class will be named \"".$moduleClassName."\"", 'cyan', null).chr(10).chr(10);

echo "Short description ? [This module does ...] : ";
$prefix = chr(10);
if(!isset($moduleDesc)){
    $moduleDesc = trim(fgets(STDIN));
    $prefix = '';
}
if($moduleDesc=='')$moduleDesc = 'This module does ...';

echo $prefix.$colors->getColoredString("Plugin description will be \"".$moduleDesc."\"", 'cyan', null).chr(10).chr(10);

if(!isset($moduleTypeBackend)) {
    $answer = "";
    $acceptedValues = array('y', 'n', 'yes', 'no');
    while (!in_array($answer, $acceptedValues)) {
        echo "Will the module display in backend ? [yes/no] : ";
        $answer = trim(fgets(STDIN));
    }
    if ($answer == 'y' || $answer == 'yes') $moduleTypeBackend = 1;
    else $moduleTypeBackend = 0;
}

if(!isset($moduleTypeFrontend)) {
    $answer = "";
    $acceptedValues = array('y', 'n', 'yes', 'no');
    while (!in_array($answer, $acceptedValues)) {
        echo "Will the module display in frontend ? [yes/no] : ";
        $answer = trim(fgets(STDIN));
    }
    if ($answer == 'y' || $answer == 'yes') $moduleTypeFrontend = 1;
    else $moduleTypeFrontend = 0;
}

if(!isset($moduleTypeAuth)) {
    $answer = "";
    $acceptedValues = array('y', 'n', 'yes', 'no');
    while (!in_array($answer, $acceptedValues)) {
        echo "Will the module supply a new authentification medium for loging in to MindFlow ? [yes/no] : ";
        $answer = trim(fgets(STDIN));
    }
    if ($answer == 'y' || $answer == 'yes') $moduleTypeAuth = 1;
    else $moduleTypeAuth = 0;
}

echo "Base ISO2 locale for localized files ? [en] : ";
$prefix = chr(10);
if(!isset($defaultLocale)){
    $defaultLocale = trim(fgets(STDIN));
    $prefix = '';
}
if($defaultLocale=='')$defaultLocale = 'en';

echo $prefix.$colors->getColoredString("Plugin default locale will be \"".$defaultLocale."\"", 'cyan', null).chr(10).chr(10);

echo "Folder name of the website where the record should be created (in /mf_websites) [default] : ";
$prefix = chr(10);
if(!isset($websiteFolderName)){
    $websiteFolderName = trim(fgets(STDIN));
    $prefix = '';
}
if($websiteFolderName=='')$websiteFolderName = 'default';
echo $prefix.$colors->getColoredString("Record will be created in /mf_websites/".$websiteFolderName.'/plugins', 'cyan', null).chr(10).chr(10);

echo "Class name of the plugin holding the record ? [myPlugin] : ";
$prefix = chr(10);
if(!isset($pluginClassName)){
    $pluginClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginClassName=='')$pluginClassName = 'myPlugin';
echo $prefix.$colors->getColoredString("Plugin class will be named \"".$pluginClassName."\"", 'cyan', null).chr(10).chr(10);
$pluginKey = $pluginClassName;

echo "Folder name / plugin key of the plugin holding the record ? [$pluginClassName] : ";
$prefix = chr(10);
if(!isset($pluginDirName)){
    $pluginDirName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginDirName=='')$pluginDirName = $pluginClassName;
$pluginKey = $pluginDirName;

echo $prefix.$colors->getColoredString("Will create record in plugin folder /mf_websites/".$websiteFolderName."/plugins/".$pluginDirName, 'cyan', null).chr(10).chr(10);




//add form ?

$answer = "";
$acceptedValues = array('y','n','yes','no');
while (!in_array($answer, $acceptedValues)){
    echo "Will now create module files, ready to write ? [yes/no] : ";
    $answer = trim(fgets(STDIN));
}
if($answer=='y' || $answer=='yes'){
    echo chr(10).$colors->getColoredString("Now writing files...", 'green', null);

    //value for moving to mf root dir
    $chdir = "../.." ;

    //create modules folder
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName."/modules");

    //create module php file
    $moduleContent = file_get_contents ("ressources/plugin_template/modules/myModule.php");
    $moduleContent = str_replace("{moduleClassName}",$moduleClassName,$moduleContent);
    $moduleContent = str_replace("{moduleClassNameUcFirst}",ucfirst($moduleClassName),$moduleContent);
    $moduleContent = str_replace("{pluginDirName}",$pluginDirName,$moduleContent);
    $moduleContent = str_replace("{pluginKey}",$pluginKey,$moduleContent);
    //$moduleContent = str_replace("{recordTitleField}",$recordTitleField,$moduleContent);

    $moduleType='';
    if($moduleTypeFrontend)    $moduleType .= 'frontend,';
    if($moduleTypeBackend)     $moduleType .= 'backend,';
    if($moduleTypeAuth)        $moduleType .= 'authentification,';

    $moduleType = rtrim($moduleType,',');

    $moduleContent = str_replace("{moduleType}",$moduleType,$moduleContent);
    //writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php',$moduleContent);

    //filling prepareData() function
    $isFirst=true;

    if($moduleTypeFrontend) {
        $sectionContent = file_get_contents("ressources/plugin_template/sections/module_prepareData_frontend.php");
        $sectionContent = (($isFirst)?'':'else ').$sectionContent;
        $isFirst=false;
        $moduleContent = setSection("module_prepareData","prepareData_frontend",$sectionContent,$moduleContent, chr(9).chr(9));
    }
    if($moduleTypeBackend) {
        $sectionContent = file_get_contents("ressources/plugin_template/sections/module_prepareData_backend.php");
        $sectionContent = str_replace("{moduleClassName}",$moduleClassName,$sectionContent);
        $sectionContent = str_replace("{moduleClassNameUcFirst}",ucfirst($moduleClassName),$sectionContent);
        $sectionContent = str_replace("{pluginKey}",$pluginKey,$sectionContent);
        $sectionContent = (($isFirst)?'':'else ').$sectionContent;
        $isFirst=false;
        $moduleContent = setSection("module_prepareData","prepareData_backend",$sectionContent,$moduleContent, chr(9).chr(9));
    }
    if($moduleTypeAuth) {
        $sectionContent = file_get_contents("ressources/plugin_template/sections/module_prepareData_authentification.php");
        $sectionContent = (($isFirst)?'':'else ').$sectionContent;
        $isFirst=false;
        $moduleContent = setSection("module_prepareData","prepareData_authentification",$sectionContent,$moduleContent, chr(9).chr(9));
    }


    //filling render() function

    $isFirst=true;

    if($moduleTypeFrontend) {
        $sectionContent = file_get_contents("ressources/plugin_template/sections/module_render_frontend.php");
        $sectionContent = (($isFirst)?'':'else ').$sectionContent;
        $isFirst=false;
        $moduleContent = setSection("module_render","render_frontend",$sectionContent,$moduleContent, chr(9).chr(9));
    }
    if($moduleTypeBackend) {
       $sectionContent = file_get_contents("ressources/plugin_template/sections/module_render_backend.php");
        $sectionContent = str_replace("{moduleClassName}",$moduleClassName,$sectionContent);
        $sectionContent = str_replace("{pluginDirName}",$pluginDirName,$sectionContent);
        $sectionContent = (($isFirst)?'':'else ').$sectionContent;
        $isFirst=false;
        $moduleContent = setSection("module_render","render_backend",$sectionContent,$moduleContent, chr(9).chr(9));
    }
    if($moduleTypeAuth) {
        $sectionContent = file_get_contents("ressources/plugin_template/sections/module_render_authentification.php");
        $sectionContent = (($isFirst)?'':'else ').$sectionContent;
        $isFirst=false;
        $moduleContent = setSection("module_render","render_authentification",$sectionContent,$moduleContent, chr(9).chr(9));
    }


    //create module json file
    if(!is_file($chdir . "/mf_websites/" . $websiteFolderName . "/plugins/" . $pluginDirName . '/' . $pluginClassName . '-json-backend.php')){
        $jsonContent = file_get_contents("ressources/plugin_template/my-plugin-json-backend.php");
        writeFile($chdir . "/mf_websites/" . $websiteFolderName . "/plugins/" . $pluginDirName . '/' . $pluginClassName . '-json-backend.php', $jsonContent);
    }
    //create language folder
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName."/languages/modules");

    //create module language main file
    $l10nModuleContent = file_get_contents ("ressources/plugin_template/languages/modules/myModule_l10n.php");
    $l10nModuleContent = str_replace("{defaultLocale}",$defaultLocale,$l10nModuleContent);
    $l10nModuleContent = str_replace("{moduleNaturalName}",str_replace('"','\"',$moduleNaturalName),$l10nModuleContent);

    writeFile($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/modules/'.$moduleClassName.'_l10n.php',$l10nModuleContent);

    //create ressources folder
    createDir($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/ressources/templates/');

    //copy module html template
    $copySuccess = copy ( 'ressources/plugin_template/ressources/templates/module.html' , $chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/ressources/templates/'.$moduleClassName.'.html' );
    echo chr(10).$colors->getColoredString('Creating '.$chdir.'/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/ressources/templates/'.$moduleClassName.'.html ... ', 'green', null);
    if($copySuccess)echo $colors->getColoredString('success.', 'green', null);
    else echo $colors->getColoredString('failed.', 'red', null);

    //load module in index.php
    $indexContent = file_get_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/index.php');
    $loadModuleContent = file_get_contents ("ressources/plugin_template/sections/index_load_module.php");

    $loadModuleContent = str_replace("{moduleNaturalName}",$moduleNaturalName,$loadModuleContent);
    $loadModuleContent = str_replace("{moduleClassName}",$moduleClassName,$loadModuleContent);
    $loadModuleContent = str_replace("{pluginDirName}",$pluginDirName,$loadModuleContent);


    echo chr(10).$colors->getColoredString("Inserting load module directive in ".$pluginDirName.'/index.php', 'green', null);
    $indexContent = setSection("index_load_module","load_module_".$moduleClassName,$loadModuleContent,$indexContent, chr(9).chr(9));

    //add module access user right
    $userRightContent = file_get_contents ("ressources/plugin_template/sections/index_add_module_editing_right.php");
    $userRightContent = str_replace("{pluginKey}",$pluginKey,$userRightContent);
    $userRightContent = str_replace("{moduleClassNameUcFirst}",ucfirst($moduleClassName),$userRightContent);
    $userRightContent = str_replace("{pluginDirName}",$pluginDirName,$userRightContent);
    $indexContent = setSection("index_add_module_editing_right","user_right_".$moduleClassName,$userRightContent,$indexContent, chr(9).chr(9));

    //write plugin index file
    file_put_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/index.php', $indexContent);

    //create user right l10n file
    if(is_file($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/userRights_l10n.php')){
        echo chr(10).$colors->getColoredString("File exists, updating... : ".$chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/userRights_l10n.php', 'green', null);
        $l10nLangContent = file_get_contents($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/userRights_l10n.php');
    }
    else {
        echo chr(10).$colors->getColoredString("Creating file ".$chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/userRights_l10n.php', 'green', null);
        $l10nLangContent = file_get_contents("ressources/plugin_template/languages/userRights_l10n.php");
        $l10nLangContent = str_replace("{defaultLocale}", $defaultLocale, $l10nLangContent);
    }
    $l10nUserRightLang = file_get_contents ("ressources/plugin_template/sections/l10n_userright_lang.php");
    $l10nUserRightLang = str_replace("{moduleClassNameUcFirst}",str_replace('"','\"',ucfirst($moduleClassName)),$l10nUserRightLang);
    $l10nUserRightLang = str_replace("{moduleNaturalName}",str_replace('"','\"',$moduleNaturalName),$l10nUserRightLang);

    $l10nLangContent = setSection("add_userright_lang","l10n_userright_$moduleClassName",$l10nUserRightLang,$l10nLangContent, chr(9).chr(9));

    writeFile($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/userRights_l10n.php',$l10nLangContent);

    //finally, write module file
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php',$moduleContent);
    echo chr(10).$colors->getColoredString("Done.", 'cyan', null);

    //if($moduleWillListRecords)echo chr(10).$colors->getColoredString("Make sure to add the record listed to the plugin as well using module_add_record.php or you will likely experience \"Fatal error: Uncaught Error: Class '".$recordClassName."' not found\".", 'purple', null).chr(10).chr(10);


}
else echo chr(10).$colors->getColoredString("Aborting write. Done.", 'red', null).chr(10).chr(10);

echo chr(10);
