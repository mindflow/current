
if(mf === undefined) {
    var mf = {
        locale: "",
        l10n: [],
    }
}


/**
 * This function makes sure a javascript is loaded only once.
 * It does not seem to work properly with scripts which require to be present on documentReady (jQuery, Bootstrap...)
 * @param scriptURL This is the URL of the javascript you want to load, either specified globally or relative to the website root
 */
function loadScript(scriptURL) {
    var list = document.getElementsByTagName('script');
    var i = list.length, flag = false;
    while (i--) {
        if (list[i].src === 'filePathToJSScript') {
            flag = true;
            break;
        }
    }

    // if we didn't already find it on the page, add it
    if (!flag) {
        var script = document.createElement('script');
        script.src = scriptURL;
        console.log("Loading script "+scriptURL);
        document.getElementsByTagName('head')[0].appendChild(script);
    }
}

function b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
}

function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

/**
 * Strips a single parameter from the url
 *
 * @param key
 * @param sourceURL
 * @returns {*}
 */
function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}
