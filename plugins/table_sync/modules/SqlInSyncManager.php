<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
//{module_include_record}

//[module_include_form_InSyncFilter]
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/table_sync/forms/SqlInSyncFilter.php';
//[/module_include_form_InSyncFilter]
//{module_include_form}



class SqlInSyncManager implements module
{

    private $moduleKey = 'SqlInSyncManager';
    private $moduleType = 'backend';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function __construct()
    {

    }


    function prepareData()
    {
        global $mf,$l10n,$config;

        $this->pageOutput = '';

        //[prepareData_backend]
		if($mf->mode == 'backend')
        {

            //process backend display
            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('table_sync', 'allowEditSync') == 1)));

            if ($this->moduleAccess) {
                //add the plugin to the backend menus
                //$mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=SqlInSyncManager"><span class="glyphicon glyphicon-menu glyphicon-arrow-left" aria-hidden="true"></span>' . $l10n->getLabel('SqlInSyncManager', 'menu_title') . '</a>', 'datas', LOW_PRIORITY);

                if (isset($_REQUEST['submodule']) && ($this->moduleKey == $_REQUEST['submodule'])) {

                    //$this->localMenu = '<li rel="' . $this->moduleKey . '" class="active"><a href="{subdir}/mf/index.php?module=' . $this->moduleKey . '"><span class="glyphicon glyphicon-menu glyphicon-arrow-left" aria-hidden="true"></span> {menu-title}</a></li>';


                    //breadcrumb
	                $this->section = $l10n->getLabel('backend', 'administration');
	                $this->subModuleName = ' / '. $l10n->getLabel('SqlInSyncManager', 'menu_title'); //<span class="glyphicon glyphicon-menu glyphicon-arrow-left" aria-hidden="true"></span>

                    if (isset($_REQUEST['action'])) $this->action = $_REQUEST['action'];

                    //[module_list_backend]
					switch($this->action){

                        case "list":
                        default:

                            //[module_insert_form_SqlInSyncManager]
							//composition du filtre d'affichage
                            $recordsFilter = new SqlInSyncFilter();

                            $form = $mf->formsManager;

                            $this->pageOutput .= '<div id="formFilter">';
                            $this->pageOutput .= $form->editForm(SUB_DIR.$config['website_pluginsdir'].'/table_sync/tableSync-json-backend.php', 'SqlInSyncFilter', 'updateContent', $recordsFilter, $this->moduleKey, '', true, $l10n->getLabel('backend','search'));
                            $this->pageOutput .= '<script>
                                function updateContent(jsonData){
                                    $("#recordEditTable_InSync").html(jsonData.message);
                                }
                            </script></div>';
							//[/module_insert_form_SqlInSync]
							//{module_insert_form}


                            $this->pageOutput .= $this->listRecords('','');
                            break;
                    }
					//[/module_list_backend]
                    //{module_list_backend}


                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >' . $l10n->getLabel('backend', 'module_no_access') . '</div>';
                $this->localMenu = '';
            }
        }
		//[/prepareData_backend]
		//{module_prepareData}
    }


    function render(&$mainTemplate)
    {
        global $mf,$l10n,$config;

        //[render_backend]
		if($mf->mode == 'backend')
        {
            //process backend display
            if (isset($_REQUEST['submodule']) && ($this->moduleKey == $_REQUEST['submodule'])) {

                $mainTemplate = str_replace("{module-body}",$this->pageOutput,$mainTemplate);
                $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);
                $mainTemplate = str_replace("{submodule-id}",$this->moduleKey,$mainTemplate);

            }
        }
		//[/render_backend]
		//{module_render}
    }


    function getType()
    {
        return $this->moduleType;
    }

    //[module_list_function_InSync]
	function listRecords($sqlConditions = '', $dateConditions = '',  $deletedCondition=null)
{
        global $mf,$l10n;

        //using time to store session SQL in case of multiple open tabs in the browser.
        //$time will allow distincting the good tab because the session now knows when the form was generated
        $sec = md5(microtime());

        if($deletedCondition==null)$deletedCondition=' AND '.SqlInSync::getTableName().'.deleted=0';

        //requête pour l'export CSV stockée en $_SESSION
      /*  if(!isset($_SESSION['actions']))$_SESSION['actions']=array();
        $_SESSION['actions'][$sec]=array(
            'action' => 'exportCSV',
            'sql' => 'SELECT * FROM '.SqlInSync::getTableName().' WHERE deleted=0'.$sqlConditions,
            'sqlConditions' => $sqlConditions,
            'record_class' => 'SqlInSync',
            'skipKeys' => array('uid', 'deleted', 'hidden', 'sorting', 'creator_uid','creator_class','parent_uid','parent_class', 'edit_lock', 'start_time', 'end_time', 'language', 'alt_language', 'password', 'history_last', 'history_past'
            ),
            'print_column_names' => true,
            'keyProcessors' => array(
                //'title'=>'SqlInSync::colorTitle',
            ),
        );*/

        $record = new SqlInSync();

        $buttons = '<button type="button" class="btn-sm mf-btn-new" id="exportCSV" name="exportCSV" onclick="document.location=\''.getHTTPHost().SUB_DIR.'/mf/core/csv-exporter.php?sec='.$sec.'\';"><span class="glyphicon glyphicon-th"></span> '.$l10n->getLabel('backend','export_to_csv').'</button>';

		$js = "<script>
			function replay(uid,secHash){
			    
			    $('#state_'+uid).html('<span class=\"orange\">".$l10n->getLabel('SqlInSync','Receiving')."</span> &nbsp;<img src=\"".SUB_DIR."/mf/backend/templates/mf-default/img/small-loading.gif\" />');
			    $('#result_'+uid).html('');
			    
			    $.ajax({
		        url: '".SUB_DIR.$GLOBALS['config']['website_pluginsdir']."/table_sync/tableSync-json-backend.php',
		        type: 'POST',
		        data: {
		            action:\"replaySqlIn\",
		            uid: uid,
		            sec: secHash,
		            fields: 'action,uid'
		        },
		        dataType: 'json',
		        success: function(jsonData) {
		            if (jsonData.result == 'success') {
		
		                //update table row data
		                 $('#state_'+uid).replaceWith(jsonData.in_state);
		                 $('#result_'+uid).replaceWith(jsonData.in_result);
		                 
		            } else {
		                
		                 if (jsonData.message != '') {
		                     showError(jsonData.message);
		                     return false;
		                 }
		                
		                 //update table row data
		                 $('#state_'+uid).replaceWith(jsonData.in_state);
		                 $('#result_'+uid).replaceWith(jsonData.in_result);
		            }
		        },
		        error: function(jsonData) {
		            if (jsonData.message != '') {
	                     showError(jsonData.message);
	                     return false;
	                 }
	                 else showError(jsonData);
		            return false;
		        }
		    });
			}
			
			
			function resetState(uid,recordClass,secHash){
		    
			    $('#state_'+uid).html('');
			    $('#result_'+uid).html('');
			    
			    $.ajax({
		        url: '".SUB_DIR.$GLOBALS['config']['website_pluginsdir']."/table_sync/tableSync-json-backend.php',
		        type: 'POST',
		        data: {
		            action:\"resetState\",
		            uid: uid,
		            record_class: recordClass, 
		            sec: secHash,
		            fields: 'action,uid,record_class' 
		        },
		        dataType: 'json',
		        success: function(jsonData) {
		            if (jsonData.result == 'success') {
		
		                //update table row data
		                 $('#state_'+uid).replaceWith(jsonData.state);
		                 $('#result_'+uid).replaceWith(jsonData.result);
		                 
		            } else {
		                
		                 if (jsonData.message != '') {
		                     showError(jsonData.message);
		                     return false;
		                 }
		                
		                 //update table row data
		                 $('#state_'+uid).replaceWith(jsonData.state);
		                 $('#result_'+uid).replaceWith(jsonData.result);
		            }
		        },
		        error: function(jsonData) {
		            if (jsonData.message != '') {
	                     showError(jsonData.message);
	                     return false;
	                 }
	                 else showError(jsonData);
		            return false;
		        }
		    });
			}
		
		</script>
		
		";

	return $js.$record->showRecordEditTable(
			array(
				'SELECT' => 'creation_date,source,state,result',
				'FROM' => SqlInSync::getTableName(),
				'JOIN' => '',
				'WHERE' => '1=1'.$dateConditions.$sqlConditions.$deletedCondition,
				'ORDER BY' => 'creation_date',
				'ORDER DIRECTION' => 'DESC',
			),
			'SyncManager',//'table_sync',
			'SqlInSyncManager',
			'creation_date',
			$keyProcessors = array(
				'creation_date' => 'dbRecord::formatDateTime',
				'deleted' => 'dbRecord::getDeletedName',
				'state' => 'SqlInSync::getState',
				'result' => 'SqlInSync::getResult',
			),
            $page = NULL,

            array(
                'create' => 0,
                'view' => 1,
                'edit' => 0,
                'delete'=> 1
            ),
            array(
                'ajaxActions' => true,
                //'buttons' => $buttons,
                'columnClasses' => array(
                    'creation_date' => 'hidden-xs',
                    'email' => 'hidden-xs hidden-sm',
                    'deleted' => 'hidden-xs hidden-sm',
                ),
                'debugSQL' => 0,
                'showRefresh' => true,
                'editRecordButtons' => array(
                    'showSaveButton' => true,
                    'showSaveCloseButton' => true,
                    'showPreviewButton' => false,
                    'showCloseButton' => true
                ),

            ),
            'SqlInSync' //recordEditTableID
        );
    }
	//[/module_list_function_SqlInSync]
	//{module_list_function}


}

