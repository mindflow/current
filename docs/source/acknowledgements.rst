.. |vspace| raw:: latex

   \vspace{5mm}

.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>
	
|embed_js|

*************
Remerciements
*************

Sponsors
########

	Nous tenons à remercier chaleureusement les sociétés, organisations et personnes suivantes pour leur support et leur confiance dans le développement du framework MindFlow :
	
	* **Mind2Machine**, studio de développement Internet, qui a initié et porté le développement de MindFlow de 2012 à 2016. La société a cessé son activité fin 2016, mais le projet MindFlow continue, soutenu notamment par Vélorizons.
	
	|vspace|
	
	* **Vélorizons**, agence de voyages à vélo : `https://www.velorizons.com <https://www.velorizons.com>`_ 
		
	|vspace|
	
	* **Les Accorderies**, système d'échange local : `http://www.accorderie.fr <http://www.accorderie.fr>`_ 
		
	|vspace|
	
	* **Syndicat des Sophrologues professionnels** : `http://annuaire.syndicat-sophrologues.fr <http://annuaire.syndicat-sophrologues.fr>`_ 
	
	|vspace|
	
	* **L'Etat Français**, qui a accordé un Crédit Impôt Innovation au projet.
	
	|vspace|
	
	* **Pascal Gaudin**, **Joël Lebossé, Pascale Caron**, **Alain Giraud**,
	
	


Contributeurs
#############

	Ces nobles geeks ont apporté leur valheureuse contribution à MindFlow :
	
	* **Alban Cousinié** : créateur du framework
	
	* **Sanjay Rupchandani** : ajout des includes dans les templates
	
	* **Benjamin Vaudour** : configuration Nginx