<?php
/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once 'utils.php';

/**
 * This script handles the secure processing and writing on the server of the files uploaded using MindFlow's files field.
 */
$execution_start = time();

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();
global $mf,$l10n,$config;

//print_r($_REQUEST);

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec()){


    //we are serving json
    header('Content-type: application/json');
    // HTTP headers for no cache etc
    //header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    //header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    // Settings
    $cleanupTargetDir = true; // Remove old files

    // put the file in the temp directory first
    $targetDir = $_SERVER['DOCUMENT_ROOT'].$config['temp_directory'];

    if (!is_dir($targetDir)) {
        @mkdir($targetDir, 0775);
    }


    $uploadName = isset($_REQUEST["fileName"]) ? $_REQUEST["fileName"] : '';

    //convert non url compliant characters
    $uploadName = Slug($uploadName, '-', '.');

    $timestamp = time();

    //name part of the file
    $fileNamePart = strstr($uploadName, '.', true)."_".$timestamp;
    $fileExtPart = strrchr ($uploadName, '.');

    // Clean the fileName for security reasons
    $cleanedFileName = $fileName = $fileNamePart.$fileExtPart;


    $uploadsdir = $_SERVER['DOCUMENT_ROOT'].$mf->getUploadsDir();
    if (!is_dir ($uploadsdir)) {
        @mkdir($uploadsdir, 0775, true);
    }


    $recorddir = $uploadsdir.DIRECTORY_SEPARATOR.$_REQUEST['record_class'];

    if (!is_dir ($recorddir)) {
        @mkdir($recorddir, 0775);
    }

    if($_REQUEST['record_uid']==''){
        die('{"jsonrpc" : "2.0", "result" : "error", "message" : "'.$l10n->getLabel('main','must_save_record').'"}');
    }

    if($_REQUEST['record_uid'] != 'undefined') {
    	//we make 1 directory per record named like 'class_uid' if we are in a dbRecord subclass
	    $finalDir = $recorddir . DIRECTORY_SEPARATOR . $_REQUEST['record_class'] . '_' . $_REQUEST['record_uid'];
	    if (!is_dir ($finalDir)) {
		    @mkdir($finalDir, 0775);
	    }
	}
    //no subdirectory for dbForm subclasses
    else {
	    $finalDir = $recorddir;
	    $_REQUEST['record_uid'] = '';
    }



    $finalFile = $finalDir.DIRECTORY_SEPARATOR.$cleanedFileName;

    if (isset($_FILES['uploadedFile'])) {
        if (move_uploaded_file($_FILES['uploadedFile']['tmp_name'], $finalFile)) {

            //UPLOAD SUCCESS

            //handle resizing if required

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //if instance of dbRecord
            if(is_subclass_of ($record,'dbRecord')) {

	            //load its content
	            $record->load( intval( $_REQUEST['record_uid'] ) );

	            $micro_key = ( isset( $_REQUEST['micro_key'] ) ) ? $_REQUEST['micro_key'] : '';
	            $micro_index = ( isset( $_REQUEST['micro_index'] ) ) ? $_REQUEST['micro_index'] : '';
	            $field_name = ( isset( $_REQUEST['field_name'] ) ) ? $_REQUEST['field_name'] : '';

	            //extract existing value from record data
	            if ( $_REQUEST['record_mode'] == 'record|' ) {
		            $workingField = &$record->data[ $_REQUEST['field_name'] ];
		            $workingValue = &$workingField['value'];
	            } else if ( $_REQUEST['record_mode'] == 'template|' ) {
		            $workingField = &$record->data['template_content']['value'][ $_REQUEST['field_name'] ];
		            $workingValue = &$workingField['value'];
	            } else if ( $_REQUEST['record_mode'] == 'micro|' ) {
		            //the field is located inside a template data structure
		            //update the field inside the template_content field
		            if ( ! isset( $record->data['template_content']['value'][ $micro_key ]['value'][ $micro_index ] ) ) {
			            $record->data['template_content']['value'][ $micro_key ]['value'] = array(
				            $micro_index => $mf->microtemplates[ $micro_key ]
			            );
		            }
		            //print_r($record->data['template_content']['value'][$micro_key]);
		            $workingField = &$record->data['template_content']['value'][ $micro_key ]['value'][ $micro_index ]['microtemplate_data'][ $field_name ];
		            $workingValue = &$workingField['value'];
	            }

	            $filePath = $record->getFilesDir();
	            //$filePath = $_REQUEST['record_class'].DIRECTORY_SEPARATOR.$_REQUEST['record_class'].'_'.$_REQUEST['record_uid'].DIRECTORY_SEPARATOR;

	            /**************************************************
	             *   MindFlow < v1.4 data conversion
	             *************************************************/
	            //the field is not located inside a template data structure, but straight in the record
	            //simply update the requested field
	            if ( is_array( $workingValue ) ) {
		            $updatedValue = $workingValue;
	            } else {
		            //initialize void value
		            $updatedValue = array();

		            //convert MindFlow < v1.4 existing data to array
		            if ( $workingValue != '' ) {

			            $oldValues = explode( ',', $workingValue );
			            $i         = 0;
			            foreach ( $oldValues as $key => $value ) {
				            list( $width, $height, $type, $attr ) = getimagesize( $finalFile );

				            $updatedValue[ $i ] = array(
					            'timestamp' => $timestamp,
					            'filename'  => $value,
					            'filepath'  => $filePath,
					            'width'     => $width,
					            'height'    => $height
				            );
				            $i ++;
			            }
		            }
	            }
	            //END MindFlow < v1.4 data conversion

	            //append the new upload to the record array
	            list( $width, $height, $type, $attr ) = getimagesize( $finalFile );

	            $updatedValue[] = array(
		            'timestamp' => $timestamp,
		            'filename'  => $cleanedFileName,
		            'filepath'  => $filePath,
		            'width'     => $width,
		            'height'    => $height
	            );


	            //check file extension
	            if ( isset( $workingField['allowedExtensions'] ) ) {
		            $allowedExtensions = explode( ',', str_replace( ' ', '', $workingField['allowedExtensions'] ) );
	            } else {
		            $allowedExtensions = array(
			            'gif',
			            'jpg',
			            'jpeg',
			            'png',
			            'xls',
			            'xlsx',
			            'doc',
			            'docx',
			            'ppt',
			            'pptx',
			            'pdf',
			            'odb',
			            'odc',
			            'odf',
			            'odg',
			            'odp',
			            'ods',
			            'odt',
			            'oxt'
		            );
	            }

	            if ( ! in_array( ltrim( $fileExtPart, '.' ), $allowedExtensions ) ) {
		            unlink( $finalFile );
		            die( '{"jsonrpc" : "2.0", "result" : "error", "message" : "' . $_REQUEST["fileName"] . ' : ' . $l10n->getLabel( 'main', 'file_ext_refused' ) . implode( ',', $allowedExtensions ) . '"}' );
	            }

	            //check file extension
	            if ( isset( $workingField['maxItems'] ) ) {
		            if ( sizeof( $updatedValue ) > intval( $workingField['maxItems'] ) ) {
			            unlink( $finalFile );
			            die( '{"jsonrpc" : "2.0", "result" : "error", "message" : "' . $_REQUEST["fileName"] . ' : ' . $l10n->getLabel( 'main', 'upload_max1' ) . " " . $workingField['maxItems'] . " " . $l10n->getLabel( 'main', 'upload_max2' ) . '"}' );
		            }

	            }
	            //update the value into the record data
	            $workingField['value'] = $updatedValue;

	            //print_r($record->data);

	            //save the updated record
	            $record->store();
            }





            //if instance of dbForm
            else if(is_subclass_of ($record,'dbForm')) {

            	//rename the value, better logic
            	$form = $record;

	            if ( $_REQUEST['record_mode'] == 'record|' ) {
		            $workingField = &$form->data[ $_REQUEST['field_name'] ];
		            $workingValue = &$workingField['value'];
	            }

	            $filePath = $_REQUEST['record_class'].DIRECTORY_SEPARATOR;

	            if ( is_array( $workingValue ) ) {
		            $updatedValue = $workingValue;
	            }

	            //append the new upload to the record array
	            list( $width, $height, $type, $attr ) = getimagesize( $finalFile );

	            $updatedValue[] = array(
		            'timestamp' => $timestamp,
		            'filename'  => $cleanedFileName,
		            'filepath'  => $filePath,
		            'width'     => $width,
		            'height'    => $height
	            );


	            //check file extension
	            if ( isset( $workingField['allowedExtensions'] ) ) {
		            $allowedExtensions = explode( ',', str_replace( ' ', '', $workingField['allowedExtensions'] ) );
	            } else {
		            $allowedExtensions = array(
			            'gif',
			            'jpg',
			            'jpeg',
			            'png',
			            'xls',
			            'xlsx',
			            'doc',
			            'docx',
			            'ppt',
			            'pptx',
			            'pdf',
			            'odb',
			            'odc',
			            'odf',
			            'odg',
			            'odp',
			            'ods',
			            'odt',
			            'oxt'
		            );
	            }


	            if ( ! in_array( ltrim( $fileExtPart, '.' ), $allowedExtensions ) ) {
		            unlink( $finalFile );
		            die( '{"jsonrpc" : "2.0", "result" : "error", "message" : "' . $_REQUEST["fileName"] . ' : ' . $l10n->getLabel( 'main', 'file_ext_refused' ) . implode( ',', $allowedExtensions ) . '"}' );
	            }

	            //check file extension
	            if ( isset( $workingField['maxItems'] ) ) {
		            if ( sizeof( $updatedValue ) > intval( $workingField['maxItems'] ) ) {
			            unlink( $finalFile );
			            die( '{"jsonrpc" : "2.0", "result" : "error", "message" : "' . $_REQUEST["fileName"] . ' : ' . $l10n->getLabel( 'main', 'upload_max1' ) . " " . $workingField['maxItems'] . " " . $l10n->getLabel( 'main', 'upload_max2' ) . '"}' );
		            }

	            }
	            //update the value into the record data
	            $workingField['value'] = $updatedValue;

            }


            //update record with uploaded file
            //echo "success ".$_REQUEST['file'];
            $mf->db->closeDB();
            //die('{"jsonrpc" : "2.0", "result" : "new field value ='.$newFieldValue.'"}');



            //send back the filename + a fileID that can safely be used as an ID for an HTML element
            die('{"jsonrpc" : "2.0", "result" : "success", "fileID" : "'.Slug($fileName, '').'", "fileName" : "'.$cleanedFileName.'", "filePath" : "'.$filePath.'", "recordUid" : "'.$_REQUEST['record_uid'].'", "recordClass" : "'.$_REQUEST['record_class'].'"}');
        }
        else die('{"jsonrpc" : "2.0", "result" : "error", "message" : "'.$l10n->getLabel('main','failed_copy_temp').'"}');
    }
    else die('{"jsonrpc" : "2.0", "result" : ""error", "message" : "'.$l10n->getLabel('main','files_empty').'"}');

}else {
    $mf->db->closeDB();
    die('{"jsonrpc" : "2.0", "error" : "message" : "'.$l10n->getLabel('main','files_empty').'"}');
}
?>