<?php
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';

class tableSync implements plugin{

    function setup()
    {

    }

    function getName()
    {
        global $l10n;
        return $l10n->getLabel('tableSync','name');
    }

    function getDesc()
    {
        global $l10n;
        return $l10n->getLabel('tableSync','desc');
    }

    function getPath()
    {
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey()
    {
        return "table_sync";
    }


    function getVersion()
    {
        return "1.0";
    }

    function getDependencies()
    {
        $dependencies = array();
        return $dependencies;
    }

    function init()
    {
        global $mf;
        $l10n = $mf->l10n;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');


        //inform MindFlow of the new dbRecords supplied by this plugin
		$mf->pluginManager->addRecord('table_sync', 'FullSync', $relativePluginDir.'/records/FullSync.php');
		$mf->pluginManager->addRecord('table_sync', 'OutSync', $relativePluginDir.'/records/OutSync.php');
		$mf->pluginManager->addRecord('table_sync', 'InSync', $relativePluginDir.'/records/InSync.php');
	    $mf->pluginManager->addRecord('table_sync', 'SqlOutSync', $relativePluginDir . '/records/SqlOutSync.php');
	    $mf->pluginManager->addRecord('table_sync', 'SqlInSync', $relativePluginDir . '/records/SqlInSync.php');


        //load modules
        $mf->pluginManager->loadModule($relativePluginDir,"modules","SyncManager", true);
		$mf->pluginManager->loadModule($relativePluginDir,"modules","FullSyncManager", false);
		$mf->pluginManager->loadModule($relativePluginDir,"modules","OutSyncManager", false);
		$mf->pluginManager->loadModule($relativePluginDir,"modules","InSyncManager", false);
	    $mf->pluginManager->loadModule($relativePluginDir,"modules","SqlOutSyncManager", false);
	    $mf->pluginManager->loadModule($relativePluginDir,"modules","SqlInSyncManager", false);


        //add editing rights
        if(class_exists('mfUserGroup')) mfUserGroup::defineUserRight('table_sync', 'allowEditSync',array(
	            'value' => '0',
	            'dataType' => 'checkbox',
	            'valueType' => 'number',
	            'processor' => '',
	            'div_attributes' => array('class' => 'col-lg-2'),
	        ),
	        $relativePluginDir.'/languages/userRights_l10n.php'
	    );


	    //add cron function for executing FullSyncs
	    $mf->addCronFunc('FullSync::fireSync();');
	    $mf->addCronFunc('OutSync::fireSync();');
	    $mf->addCronFunc('SqlOutSync::fireSync();');

	    //set Hooks from config file
	    if(isset($GLOBALS['config']['plugins']['table_sync']['hooks'])){
			foreach ($GLOBALS['config']['plugins']['table_sync']['hooks'] as $hook){
				$triggers = explode(',',$hook['triggers']);
				foreach($triggers as $triggerName) {
					dbRecord::addHook( $triggerName, $hook['recordClass'], $hook['callback'], $hook['parameters'], $hook['locales'] );
				}
			}
	    }

    }
}

$mf->pluginManager->addPluginInstance(new tableSync());


