<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


/**
 * Displays the form out of a templateData field. Must be used in conjunction with a templateNameField. The templateData field will hold the template values, while the templateName will point to the name of the datastructure used as definition.
 */
class templateDataField implements formField
{
    /**
     * Returns the HTML code for displaying the field, initialized with the data supplied in $f->fieldData
     * @param $f fieldData : this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
     * $f->formID           // value of the id attribute of the <form> tag this field is included in
     * $f->mode             // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
     * $f->dataType         // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
     * $f->key              // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
     * $f->fieldData        // this is the data array of the current field
     * $f->fieldsData        // this is the data array of all fields
     * $f->recordUid        // the uid (Unique ID) of the record containing the current field
     * $f->recordClassName  // the class name of the record containing the current field
     * $f->record           // all the data for the record containing the current field
     * $f->microKey         // field name when processing a microTemplate field (only used when mode=="micro|")
     * $f->microIndex       // sorting index when processing a microTemplate field (only used when mode=="micro|")
     * @returns String a string containing the HTML for displaying the field
     */
    static public function displayField($f)
    {
        global $mf,$l10n;
        $html = array();

        //get the structure from template, in case it has evolved
        $templateKeys = array_keys($mf->templates);

        //create an instance of the object to display in order to retreive its default values
        $record_model = new $f->recordClassName();

        //make sure we don't raise an error if $fieldData['value'] is not properly initialized
        if(!is_array($f->fieldData['value'])){$f->fieldData['value']=array();}

        $html[] = formsManager::getFieldLabel($f->fieldData, $f->mode, $f->key, $f->recordClassName);


        if(count($f->fieldData['value'])==0){

            //if no template data is defined
            //inject fields definitions of the first template available ($templateKeys[0]) by default
            //$record_model->data['template_content']['value'] = $mf->templates[$templateKeys[0]]['template_data'];
            $record_model->data['template_content']['value'] = array(
                'undefined_message' => array(
                    'value' => '<div class="row"><div class="col-lg-8 col-lg-offset-4"><div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$l10n->getLabel('main','template_undefinded').'</div></div></div>',
                    'dataType' => 'html',
                    'valueType' => 'text',
                    'div_attributes' => array('class' => 'col-lg-6'),
                ));


            //get the list of fields to be displayed in the edit form
            $fields = str_replace (' ','','undefined_message'); //get rid of nasty spaces
            $fields_displaylist = explode(',',$fields);

            // when creating a new record, there is no value defined so just push the template's definition
            // including its default values
            $html[] ="<div class='clearfix'></div>";
            $html[] = $mf->formsManager->makeFieldset($html,$record_model->data['template_content']['value'], $f->recordClassName, $fields_displaylist, 'template|', $f->fieldsData,$f->formID, 0, '', '', $f->record);
        }
        else{
            // when displaying an existing record

            //get the list of fields to be displayed in the edit form for the current template
            $currentTemplateName = $f->fieldsData['template_name']['value'];

            $html[] ="<div class='clearfix'></div>";

            //merge with the latest known template definition (in case the definition has changed, it updates the fields)
            if(isset($mf->templates[$currentTemplateName])){
                $f->fieldData['value'] = array_merge_recursive_distinct($mf->templates[$currentTemplateName]['template_data'],$f->fieldData['value'] );
                $fields = str_replace (' ','',$mf->templates[$currentTemplateName]['showInEditForm']); //get rid of nasty spaces
                $fields_displaylist = explode(',',$fields);

                // get the data from the given record and display
                $html[] = $mf->formsManager->makeFieldset($html,$f->fieldData['value'], $f->recordClassName, $fields_displaylist, 'template|', $f->fieldsData,$f->formID, 0, '', '', $f->record);
            }
            else{
                $mf->addErrorMessage($l10n->getLabel('backend','template_not_defined1').' \''.$currentTemplateName.'\' '.$l10n->getLabel('backend','template_not_defined2'));

            }

        }
        return implode(chr(10),$html);



    }


    /**
     * Process the field value prior saving it to the database, whenever inner value adaptation is required, for SQL strict compliance for example.
     * This is executed after 'preRecord' processing functions.
     * @param $fieldData this is the data array of the current field
     * @param $value this is the submited value
     * @return mixed
     */
    static public function onSave($fieldData, $value){
        return $value;
    }

    static public function getCSS(){return "";}
    static public function getTopJS(){return "";}
    static public function getBottomJS(){return "";}







}
