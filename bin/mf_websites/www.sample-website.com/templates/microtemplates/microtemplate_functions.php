<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alban
 * Date: 26/02/13
 * Time: 09:31
 * To change this template use File | Settings | File Templates.
 */


function photoPersonne($page,$templateKey, $value){

    $mf = &$GLOBALS['mf'];
    $html = array();

    if($value!=''){
        //$imagesArray = explode(',', $value);
        //$image_path = $GLOBALS['config']['uploads_directory'].DIRECTORY_SEPARATOR.'page'.DIRECTORY_SEPARATOR.'page_'.$mf->info['currentPageUid'].DIRECTORY_SEPARATOR;
        if(isset($value[0])){
            $image = $value[0];

            list($width, $height, $type, $attr)= getimagesize(DOC_ROOT.SUB_DIR.$image['filepath'].$image['filename']);
            $html[] = '<img src="'.$image['filepath'].$image['filename'].'" width="'.$width.'" height="'.$height.'" alt="" />';
        }
    }
    return implode(chr(10),$html);

}

function socialFacebook($page,$templateKey, $value){
    if($value !='') return '<a href="'.$value.'" target="_blank" class="imgRollover social"><img src="/mf_websites/pure_moment/templates/images/facebook_off.png" width="26" height="26" alt="facebook"></a>';
    else return '';
}

function socialLinkedin($page,$templateKey, $value){
    if($value !='') return '<a href="'.$value.'" target="_blank" class="imgRollover social"><img src="/mf_websites/pure_moment/templates/images/linkedin_off.png" width="26" height="26" alt="Linked in"></a>';
    else return '';
}

function socialTwitter($page,$templateKey, $value){
    if($value !='') return '<a href="'.$value.'" target="_blank" class="imgRollover social"><img src="/mf_websites/pure_moment/templates/images/twitter_off.png" width="26" height="26" alt="Twitter"></a>';
    else return '';
}

function socialDribble($page,$templateKey, $value){
    if($value !='') return '<a href="'.$value.'" target="_blank" class="imgRollover social"><img src="/mf_websites/pure_moment/templates/images/dribble_off.png" width="26" height="26" alt="Dribble"></a>';
    else return '';
}

function socialGoogle($page,$templateKey, $value){
    if($value !='') return '<a href="'.$value.'" target="_blank" class="imgRollover social"><img src="/mf_websites/pure_moment/templates/images/google_off.png" width="26" height="26" alt="Google +"></a>';
    else return '';
}


function photoClient($page,$templateKey, $value){

    $mf = &$GLOBALS['mf'];
    $html = array();

    if($value!=''){
        $imagesArray = explode(',', $value);
        $image_path = $GLOBALS['config']['uploads_directory'].DIRECTORY_SEPARATOR.'page'.DIRECTORY_SEPARATOR.'page_'.$mf->info['currentPageUid'].DIRECTORY_SEPARATOR;

        $image = $imagesArray[0];

        if($image !=''){
            list($width, $height, $type, $attr)= getimagesize(DOC_ROOT.SUB_DIR.$image_path.$image);
            if(trim($image) != '')$html[] = '<img src="{image_path}'.$image.'" width="'.$width.'" height="'.$height.'" alt="" />';
        }
    }
    return implode(chr(10),$html);

}

