<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */



$execution_start = time();

require_once '../../../mf_config/config.php';
error_reporting(E_ERROR | E_PARSE);
require_once DOC_ROOT.SUB_DIR.'/mf/frontend/frontend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//we are serving XML
header("Content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" ?>';

//create a frontend for being able to use all the mindflow objects
$mindFlowFrontend = new frontend();
$mindFlowFrontend->prepareData();
$mf = $mindFlowFrontend->mf;
$l10n = $mf->l10n;
$pdo =$mf->db->pdo;
$host = getHTTPHost();
$xml=array();

$xml[] =  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

$sql = "SELECT uid, modification_date,changefreq,priority,hide_lastmod FROM mf_pages where deleted=0 AND hidden=0 AND hide_from_sitemap=0";
$stmt = $pdo->query($sql);

$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach($rows as $row){

    $xml[] = '  <url>';
    $xml[] = '      <loc>'.$host.$mf->modules->urlRewriter->getPageURL($row['uid']).'</loc>';
    if($row['hide_lastmod']==0)$xml[] = '      <lastmod>'.strstr($row['modification_date'],' ',true).'</lastmod>';
    if(trim($row['changefreq'])!='')$xml[] = '      <changefreq>'.$row['changefreq'].'</changefreq>';
    if(trim($row['priority'])!='')$xml[] = '      <priority>'.$row['priority'].'</priority>';
    $xml[] = '  </url>';
}

$xml[] =  '</urlset>';

echo  implode(chr(10),$xml);
