<?


//templates definition
$siteInfoTemplate = array(

    'siteInfo' => array(
        'template_name' => 'siteInfo',
        'template_file' => '',
        'template_data' => array(
            'titre-adresse1' => array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array(
                    'class' => 'col-lg-5'
                )
            ),
            'sstitre-adresse1' => array(
                'value' => '',
                'dataType' => 'textarea 5 3',
                'div_attributes' => array(
                    'class' => 'col-lg-5'
                )
            ),
            'adresse1' => array(
                'dataType' => 'textarea 5 3',
                'value' => '',
                'processor' => '',
                'div_attributes' => array(
                    'class' => 'col-lg-5'
                )
            ),
            'latitude-adresse1' => array(
                'dataType' => 'input text',
                'value' => '',
                'processor' => '',
                'div_attributes' => array(
                    'class' => 'col-lg-2'
                )
            ),
            'longitude-adresse1' => array(
                'dataType' => 'input text',
                'value' => '',
                'processor' => '',
                'div_attributes' => array(
                    'class' => 'col-lg-2'
                )
            ),

            'social-facebook' => array(
                'value' => '',
                'dataType' => 'input text',
                'processor' => 'socialFacebook',
                'div_attributes' => array(
                    'class' => 'col-lg-6'
                )
            ),
            'tel-work' => array(
                'value' => '',
                'dataType' => 'input text',
                'processor' => '',
                'div_attributes' => array(
                    'class' => 'col-lg-2'
                )
            ),
            'tel-mobile' => array(
                'value' => '',
                'dataType' => 'input text',
                'processor' => '',
                'div_attributes' => array(
                    'class' => 'col-lg-2'
                )
            ),

        ),
        'showInEditForm' =>'titre-adresse1,sstitre-adresse1,adresse1,latitude-adresse1,longitude-adresse1,social-facebook,tel-work,tel-mobile',
    ),
);
?>