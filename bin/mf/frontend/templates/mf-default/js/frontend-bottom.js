/**
 * Created with JetBrains PhpStorm.
 * User: Alban
 * Date: 19/02/13
 * Time: 16:26
 * To change this template use File | Settings | File Templates.
 */



jQuery(document).ready(function() {

    /*$(".microEditLink").fancybox({
        minWidth:'80%',
        minHeight:'80%',
        //fitToView	: true,
        autoSize	: true,
        closeClick	: false,
        //autoResize : true
        padding : '30px',
    });*/

    $(".microTemplateButton").fancybox({

        fitToView	: false,
        autoSize	: true,
        closeClick	: false,
        openEffect	: 'elastic',
        closeEffect	: 'elastic'
    });

    if($("#uiMessages").html().trim() == '')$("#uiMessages").hide();


    $('.imgRollover').hover(
        function()
        {
            img =  $(this).find('img');
            src = img.attr('src');
            img.attr('src', src.replace("_off","_on"));
        },
        function()
        {
            img =  $(this).find('img');
            src = img.attr('src');
            img.attr('src', src.replace("_on","_off"));
        }
    );

});




function printDiv(divID) {
    //print fix
    //cleaning up styles from previous printing sessions
    $('body *').removeClass('print-parent');
    $('body *').removeClass('print');
    $('body *').removeClass('noprint');

    //set styles for current printing session
    $('body *').addClass('noprint');
    $('#'+divID).removeClass('noprint');
    $('#'+divID).addClass('print-parent');

    $('#'+divID+' *').removeClass('noprint');
    $('#'+divID+' *').addClass('print');
    //alert('printing div '+divID);
    //launch print
    window.print();

    //cleaning up styles from previous printing sessions
    $('body *').removeClass('print-parent');
    $('body *').removeClass('print');
    $('body *').removeClass('noprint');
}

function printDivNewWindow(divID) {

    var win=null;
    var content = $('#'+divID);

    win = window.open("width=200,height=200");
    self.focus();
    win.document.open();
    win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
    win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
    win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
    win.document.write(content.html());
    win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
    win.document.close();
    win.print();
    win.close();
}


