<?php
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';

class {pluginClassName} implements plugin{

    function setup()
    {

    }

    function getName()
    {
        global $l10n;
        return $l10n->getLabel('{pluginClassName}','name');
    }

    function getDesc()
    {
        global $l10n;
        return $l10n->getLabel('{pluginClassName}','desc');
    }

    function getPath()
    {
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey()
    {
        return "{pluginDirName}";
    }


    function getVersion()
    {
        return "1.0";
    }

    function getDependencies()
    {
        $dependencies = array();
        return $dependencies;
    }

    function init()
    {
        global $mf;
        $l10n = $mf->l10n;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');


        //inform MindFlow of the new dbRecords supplied by this plugin
        //{index_load_record}


        //load modules
        //{index_load_module}


        //add editing rights
        //{index_add_module_editing_right}



    }
}
$mf->pluginManager->addPluginInstance(new {pluginClassName}());


