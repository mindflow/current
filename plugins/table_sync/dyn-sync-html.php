<?php

$execution_start = time();

require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//we are serving HTML
header('Content-type: text/html');

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();

global $mf,$l10n;

//print_r($_REQUEST);
//echo "test1=".isset($_REQUEST['sec'])." ".$_REQUEST['sec']." ";
//echo "test2=".isset($_REQUEST['fields']) ." ".$_REQUEST['fields']." ";
//echo "test3=".formsManager::checkSecValue($_REQUEST['sec'],explode(',',$_REQUEST['fields']))." ";

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec()){

    //put you AJAX HTML in this var, the base HTML template from templates/your_backend_theme_name/ajax.html will be wrapped around with header + footer
    $ajaxBody = '';

    if(isset($_REQUEST['action'])){

        global $mf,$l10n;

        if($_REQUEST['action'] == 'createFullSync'){

	        $fullSync = new FullSync();

	        $fullSync->showInEditForm = array(
		        'tab_main'=>array(
			        'name','enabled','local_table','out_rule',
			        'minute','hour','dom','dow','exec_asap',
		        )
	        );

	        $recordEditTableID = 'FullSync';

	        $form = $mf->formsManager;
	        $ajaxBody = '<div id="ajaxResult">'.$form->editRecord($fullSync, '', '', true, true,false,true, 1, null, null, $recordEditTableID).'</div><!-- /.ajaxResult -->';

        }


    }


    //Build the HTML
    $html = array();

    //process page header
    if(isset($_REQUEST['header']) && $_REQUEST['header']>0){
        $mf->header = $mindFlowBackend->getStandardHeader(' id="ajax-container"');

        //processing UI messages
        $warnings=array();
        foreach($mf->warningMessages as $message) $warnings[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{warning-messages}",implode(chr(10),$warnings),$mf->header);

        $errors=array();
        foreach($mf->errorMessages as $message) $errors[] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{error-messages}",implode(chr(10),$errors),$mf->header);

        $success=array();
        foreach($mf->successMessages as $message) $success[] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{success-messages}",implode(chr(10),$success),$mf->header);

        $infos=array();
        foreach($mf->infoMessages as $message) $infos[] = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{info-messages}",implode(chr(10),$infos),$mf->header);

        $html[]= $mf->header;
    }

    $mf->body = file_get_contents(DOC_ROOT.SUB_DIR.$mindFlowBackend->template.'/ajax.html');

    //set AJAX body
    $mf->body = str_replace("{ajax}",$ajaxBody,$mf->body);

    $mf->body = str_replace("{subdir}",SUB_DIR,$mf->body);
    if(isset($_REQUEST['submodule']))$mf->body = str_replace("{submodule-name}",$_REQUEST['submodule'],$mf->body);

    $html[]= $mf->body;

    if(isset($_REQUEST['footer']) && $_REQUEST['footer']>0){
        //process page footer
        $mf->footer = $mindFlowBackend->getStandardFooter();
        $html[]= $mf->footer;
    }

    //output HTML
    echo implode(chr(10),$html);

    $mf->db->closeDB();

}
else {
    $mf->db->closeDB();
    die('{"jsonrpc" : "2.0", "error" : "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}');
}