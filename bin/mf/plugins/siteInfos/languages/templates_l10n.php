<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'fr' => array(
        'titre-adresse1' => 'Titre adresse',
        'sstitre-adresse1' => 'Sous-titre adresse',
        'adresse1' => 'Texte adresse',
        'latitude-adresse1' => 'Latitude',
        'longitude-adresse1' => 'Longitude',
        'tel-work' => 'Tél profesionnel',
        'tel-mobile' => 'Tél mobile',

        /*mandatory records*/
        'no_record' => 'Il n\'existe pas d\'info genérale pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=siteInfos&action=create">créer une info générale</a>.',
        'social-facebook' => 'Lien page facebook',
        'social-linkedin' => 'Lien page linkedin',
        'social-twitter' => 'Lien page twitter',
        'social-dribble' => 'Lien page dribble',
        'social-google' => 'Lien page google+',
    ),

    'en' => array(
        'titre-adresse1' => 'Address title',
        'sstitre-adresse1' => 'Address title',
        'adresse1' => 'Address text',
        'latitude-adresse1' => 'Latitude',
        'longitude-adresse1' => 'Longitude',
        'tel-work' => 'Pro phone',
        'tel-mobile' => 'Global phone',

        /*mandatory records*/
        'no_record' => 'There currently exists no general site info. You can <a href="{subdir}/mf/index.php?module=siteInfos&action=create">create a general site info</a>.',
        'social-facebook' => 'Facebook page link',
        'social-linkedin' => 'Linkedin page link',
        'social-twitter' => 'Twitter page link',
        'social-dribble' => 'Dribble page link',
        'social-google' => 'Google+ page link',
    ),


);