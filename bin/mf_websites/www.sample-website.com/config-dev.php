<?php

// Define where the current website files are located in a local variable so it can be reused easily below
$websiteDir = '/mf_websites/www.sample-website.com';


// Website configuration is stored in an array for easy configuration manipulation and output
$GLOBALS['config'] = array(


    // Install tool password : change default value as soon as possible !
    'installTool_password' => '1234',

    // Salt for forms security checks. Update with random text at each new mindFlow installation !
    // Go to https://strongpasswordgenerator.com/ to make one. You can use whatever length you want. 12 to 128 characters should be more than enough.
    'salt' => 'Nv6abc4454819sA',

    //database configuration
    'db_host' => 'mysql.sample-website.com',
    'db_port' => '3306',
    'db_name' => 'my_database',
    'db_username' => 'my_user',
    'db_password' => 'my_password',
    'db_force_utf8' => true,
    'db_engine' => 'MyISAM',

    // Plugins configuration
    'load_plugins' => 'welcome,mf_users,urlRewriter,mf_authentification,pages,contacts,my_plugin',//events,mf_countryData,mf_searchEngine,portfolio,siteInfos,',
    'default_plugin' => 'welcome',

    // lists locales availables in frontend
    'frontend_locales' => array(
        'fr' => 'Français',
        'en' => 'English',
        'it' => 'Italian',
    ),

    // backend locales are used when you want to translate the backend in several languages,
    // such as when you have several webmasters not necessarily speaking the language of the mother company
    // in this case they can select their language in the properties of their backend user profile
    'backend_locales' => array(
        'fr' => 'Français',
        'en' => 'English',
    ),

    // Default user configuration. Indicates what language should be displayed by default for frontend and backend contexts.
    'default_frontend_locale' => 'fr',
    'default_backend_locale' => 'fr',

    // Name of the website
    'sitename' => 'My website',

    // Website charset & timezone
    'charset' => 'utf-8',
    'timezone' => 'Europe/Paris',

    // CSS style for the debug log <div>
    'debuglog_style' => 'clear:both;width:100%;margin:50px 0 0 0;overflow:auto;top:20px;background:rgba(0,0,0,0.75);padding:10px;color:white',

    // Do not touch these unless really needed
    'plugins' => array(),
    'website_dir' => $websiteDir,
    'website_pluginsdir' => $websiteDir.'/plugins',

    // Paths pointing to the website templates
    'backend_template' => '/mf/backend/templates/mf-default',
    // Enter either a file name or a directory name here.
    // Directory is used for responsive backgrounds. It should feature the files bg-full.jpg, bg-2100.jpg, bg-1280.jpg and bg-640.jpg with resized backgrounds featuring the specified width in the file name
    'backend_template_background' => '/mf/backend/templates/mf-default/img/bg1/',
    'backend_logo' => '/mf/backend/templates/mf-default/img/logo.png', //optimal size : 100x100 pixels
    'backend_favicon' => '',
    'templates_directory' => $websiteDir.'/templates',
    'temp_directory' => $websiteDir.'/temp',
    'uploads_directory' => $websiteDir.'/uploads',
    'kcfinder_uploads_directory' => $websiteDir.'/uploads',

    // Maximum record count to display in the showRecordEditTable arrays
    'max_records_per_page' => '20',

    //logger configuration
    'enable_logger' => 1,
    'monolog' => 'mf_websites/www.sample-website.com/monolog-config.php',

    // Configuration for emails sent from de the website (notifications and such things)
    'siteEmailName' => 'My website',
    'siteEmailSMTPServer' => 'smtp.sample-website.com',
    'siteEmailSMTPPort' => 25,
    'siteEmailSMTPEmail' => 'site@sample-website.com',
    'siteEmailSMTPUsername' => 'site@sample-website.com',
    'siteEmailSMTPPassword' => 'my_email_password',

    // Add here the classes that may be stored in the $_SESSION variable so they can be retreived using php autoloader
    'preload_classes' => array(
        0 => DOC_ROOT.SUB_DIR.'/mf/plugins/mf_users/records/mfUser.php',
        1 => DOC_ROOT.SUB_DIR.'/mf/plugins/mf_users/records/mfUserGroup.php',
        2 => DOC_ROOT.SUB_DIR.'/mf/plugins/urlRewriter/modules/urlRewriter.php',
    )
);

/*
 * Plugins configuration go below
 * Copy and paste below the content of the file defaultPluginConfig.php defined in the plugins you use
 * Then customize according to your needs
 */

$GLOBALS['config']['plugins']['pages'] = array(
    //executes this function prior rendering any page if defined
    //define the function in your templates.php file
    'prepareDataFunc' => 'pagePrepareData()',
);



$GLOBALS['config']['plugins']['siteInfos'] = array(
    'siteInfo-template-def' => '/mf_websites/www.sample-website.com/templates/plugins/siteInfos/templates/siteInfo-template.php',
    'siteInfo-class-language' => '/mf_websites/www.sample-website.com/templates/plugins/siteInfos/languages/siteInfo.php',
);


?>