<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'en' => array(
        'menu_title' => 'Manage the search index',
        'new_record' => 'Add an index entry',
        'edit_record' => 'Edit this index entry',
        'view_record' => 'View this index entry',
        'rebuild_index' => 'Rebuild all index',
        'entries_count_single' => 'entry set into the index',
        'entries_count_many' => 'entries set into the index'
    ),

    'fr' => array(
        'menu_title' => 'Gérer l\'index de recherche',
        'new_record' => 'Ajouter une entrée à l\'index',
        'edit_record' => 'Editer l\'entrée de l\'index',
        'view_record' => 'Voir l\'entrée de l\'index',
        'rebuild_index' => 'Reconstruire tout l\'index',
        'entries_count_single' => 'entrée présente dans l\'index',
        'entries_count_many' => 'entrées présentes dans l\'index'
    ),
);