

if( !(mf === undefined) && mf.dialogManager === undefined) {

    mf.dialogManager = {
        dialogsStack: [],
        lastDialog: null,

        //d_type possible values : selectField_dialog, record_dialog, page_dialog, microTemplate_dialog

        openDialog: function (d_type, d_id, d_parentId, d_url, d_width, d_closeAction, d_title) {

            if (d_id == "") d_id = "d" + mf.dialogManager.randId();
            if (d_width == "") d_width = "90%";
            console.log("*** openDialog id=" + d_id);
            $("#mf_dialogs").append('<div id="' + d_id + '"></div>');

            newDialog = $("#" + d_id).dialog({
                autoOpen: false,
            });


            // Create and display loading icon div + content div that will fit into our dialog.
            // The .content div is made slightly transparent so the user can see the content loading as it progresses,
            $("#" + d_id).html('<div class="loading" style=text-align:center;margin:20px auto 0px auto;"><img src="/mf/backend/templates/mf-default/img/30.gif" /></div><div class="content" style="opacity:.2;"></div>');

            // and click events are disabled on it in order to prevent clicking the form while it is not ready yet.
            //$("#" + d_id+" .content").css("pointer-events","none");

            //build our dialog
            newDialog.dialog({
                title: d_title,
                closeText: "hide",
                requestFailed: "Request failed. Please try again",
                width: d_width,
                position: {my: "center", at: "top+10%", of: window},
                modal: true
                //show: { effect: "fadeIn", duration: 500 }
            });

            //AJAX load of the server's HTML which we will display in the .content div
            $.ajax({
                url: d_url,
                success: function(response) {
                    //apply the html to .content div once we get the response
                    $("#" + d_id+" .content").html(response);

                    //force evaluating javascript received inside the HTML (always disabled after AJAX call for security reasons)
                    $("#" + d_id).find("script").each(function (i) {
                        eval($(this).text());
                    });
                },
                complete:function(response) {

                    //once loading is complete, add the dialog to our dialog manager stack
                    dialogStore = {
                        type: d_type,
                        id: d_id,
                        parentId: d_parentId,
                        url: d_url,
                        width: d_width,
                        closeAction: d_closeAction,
                        dialogWindow: newDialog
                    };

                    newDialog.on("dialogclose", function (event, ui) {
                        console.log("*** Event dialogclose d_id=" + d_id);
            //            console.log("event=");
            //            console.log(event);
                        mf.dialogManager.closeDialog(d_id);
                    });


                    mf.dialogManager.dialogsStack.push(dialogStore);
                    mf.dialogManager.lastDialog = mf.dialogManager.getLastDialog();

            //        console.log("*** END openDialog id=" + d_id);
            //       console.log("stack=");
            //        console.log(mf.dialogManager.dialogsStack);

                    //make the loaded HTML opaque
                    $("#" + d_id+" .content").fadeTo(250,1);

                    //hide loading icon
                    $("#" + d_id+" .loading").css("display","none");

                    //Restore clicks on the content layer now the content is fully loaded
                    $("#" + d_id+" .content").css("pointer-events","auto");

                },
                error: function(response) {
                    alert(response);
                }
            });

            newDialog.dialog("open");

        },

        closeDialog: function (d_id) {
            console.log("*** closeDialog id=" + d_id);
            //   console.log("stack before : ");
            //   console.log(mf.dialogManager.dialogsStack);


            //if we have a record, notify MindFlow the record is being closed in order to release the edit_lock flag
            closedRecordUid = $("#"+d_id+" #uid").val();
            closedRecordClass = $("#"+d_id+" #class").val();
            closedRecordSec = $("#"+d_id+" #closeSec").val();
            closedRecordFields = $("#"+d_id+" #closeFields").val();
            locale = $("#"+d_id+" #mfSetLocale").val();
            xEndMode = $("#"+d_id+" #mode").val();

            if(closedRecordUid != undefined){
                $.ajax({
                    url: SUB_DIR+"/mf/core/ajax-core-json.php",
                    type: 'POST',
                    data: { action:'close_record', record_uid: closedRecordUid, record_class: closedRecordClass, mode: xEndMode, sec: closedRecordSec, fields:closedRecordFields, mfSetLocale:locale},
                    dataType: 'json',
                    success: function(jsonData) {
                        console.log("Record "+closedRecordClass+"("+closedRecordUid+") closed successfuly");
                    }
                });
            }


            var closedDialog = null;    // the dialog being closed
            var dialogStackIndex = 0; //index of our dialog in the stack

            //seek dialog in the stack
            for (var i = 0, len = mf.dialogManager.dialogsStack.length; i < len; i++) {
                console.log("dialogs.js closeDialog iteration i=" + i + " id=" + mf.dialogManager.dialogsStack[i].id);
                if (mf.dialogManager.dialogsStack[i].id == d_id) {

                    closedDialog = mf.dialogManager.dialogsStack[i];
                    console.log("Found closedDialog id " + closedDialog.id+" at stack index "+i);
                    dialogStackIndex = i;
                }

            }
          //  console.log("closedDialog=");
          //  console.log(closedDialog);

            if (closedDialog != null) {

                //run closeAction function
                if (closedDialog.closeAction != "") {
                    if(typeof closedDialog.closeAction == "string")
                        eval(closedDialog.closeAction);
                    else closedDialog.closeAction();
                }

                //destroy dialog
                closedDialog.dialogWindow.dialog("destroy");
                $("#" + closedDialog.id).remove();

                //remove dialog from stack
                mf.dialogManager.dialogsStack.splice(dialogStackIndex, 1);

                //update lastDialog pointer
                if (mf.dialogManager.dialogsStack.length > 0)
                    mf.dialogManager.lastDialog = mf.dialogManager.getLastDialog();
                else mf.dialogManager.lastDialog = null;

            //    console.log("stack after : ");
            //    console.log(mf.dialogManager.dialogsStack);
            }
            else {
                console.log("Dialog id=" + d_id + " not found in stack !");
            }

        },
        getLastDialog: function () {
            return mf.dialogManager.dialogsStack[mf.dialogManager.dialogsStack.length - 1];
        },
        closeLastDialog: function () {
         //   console.log("*** closeLastDialog");
         //   console.log("stack=");
         //   console.log(mf.dialogManager.dialogsStack);
            if (mf.dialogManager.dialogsStack.length > 0) {
                mf.dialogManager.lastDialog = mf.dialogManager.getLastDialog();
         //       console.log("closeLastDialog id = " + mf.dialogManager.lastDialog.id);
                mf.dialogManager.lastDialog.dialogWindow.dialog("close");
            }
            else {
         //       console.log("Stack is empty, nothing to close !");
                mf.dialogManager.lastDialog = null;
            }

        },

        randId: function () {
            return Math.random().toString(36).substr(2, 10);
        }
    }
}

/**
 * Opens a record editing jquery dialog
 * @param editRecordLink string a link supplying the HTML code to be loaded in the dialog
 * @param dialogWidth string a width, preferently in percent of the screen size, like '70%'
 * @param evalCodeOnClose custom JS code to be evaluated when the record dialog gets closed.
 */
function openRecord(editRecordLink, dialogWidth, evalCodeOnClose, recordTitle){
    console.log('openRecord('+editRecordLink+', '+dialogWidth+', '+evalCodeOnClose+', '+recordTitle+')');
    mf.dialogManager.openDialog('record_dialog','','',editRecordLink,dialogWidth,evalCodeOnClose,recordTitle);
}

/**
 * Closes the last opened dialog
 */
function closeRecord(){
    console.log('closeRecord() [Closing the last opened dialog]');
    mf.dialogManager.closeLastDialog();
}