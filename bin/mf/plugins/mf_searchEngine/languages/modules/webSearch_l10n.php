<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'fr' => array(
        'no_result' => 'Aucun résultat ne correspond à votre recherche.',
        'type_keywords' => 'Saisissez les mots recherchés.<br /><br /> Préfixez un mot par le signe \'+\' pour rendre son apparition obligatoire, ou par le signe \'-\' si au contraire les résultats contenant ce mot ne doivent pas apparaitre, comme ceci : ouest +Népal -Canada',
        'searchForm-title' => 'Rechercher',
        'searchForm-placeholder' => 'Mot-clé, destination ...',
        'searchForm-submit' => 'Rechercher',
    ),

    'en' => array(
        'no_result' => 'No results were found to match your search.',
        'type_keywords' => 'Please type in your search terms. <br /><br /> You can prefix chosen words with the  \'+\' sign to make them mandatory, or with the  \'-\' sign  to avoid results containing this word from appearing, e.g. west +nepal -canada',
        'searchForm-title' => 'Search',
        'searchForm-placeholder' => 'Keyword, place ...',
        'searchForm-submit' => 'Search',
    ),

    'de' => array(
        'no_result' => 'Ihre Suche ergab leider keine Treffer.',
        'type_keywords' => 'Suchbegriffe eintragen.<br /><br /> Setzen Sie ein Pluszeichen \'+\' vor die Wörter, die Sie unbedingt ansehen wollen. Im Gegensatz, setzen Sie ein Minuszeichen \'-\' vor die Wörter, die Sie ausschließen wollen. Zum Beispiel : Provence +Rennrad -Nizza',
        'searchForm-title' => 'Suchen',
        'searchForm-placeholder' => 'Suchbegriff, Reiseziel ...',
        'searchForm-submit' => 'Suchen',
    ),
);