<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/pages/records/page.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class pages implements module
{
    private $moduleKey = 'pages';
    private $pluginVersion = '1.0';
    private $moduleType = 'backend,frontend';
    private $dependencies = 'urlRewriter';
    private $pageFrontendHooks = array();

    private $action='list';

    var $pageOutput;


    function prepareData(){
        global $mf,$l10n,$config;
        $this->pageOutput = '';


        if($mf->mode == 'frontend'){
            if(isset($config['plugins']['pages']['prepareDataFunc']) && $config['plugins']['pages']['prepareDataFunc'] != '') eval($config['plugins']['pages']['prepareDataFunc'].";");


        }
        else if($mf->mode == 'backend'){

            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('pages','allowEditPages')==1)));


            if($this->moduleAccess){
                $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=pages"><span class="glyphicon glyphicon-menu glyphicon-file" aria-hidden="true"></span>'.$l10n->getLabel('pages','menu_title').'</a>', 'content', HIGH_PRIORITY);

                //add nested sortable
                $mf->addBottomJs('<script src="'.SUB_DIR.'/mf_librairies/nestedSortable/jquery.mjs.nestedSortable.js"></script>',true,false);

                if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                    if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                    switch($this->action){
                        case "list":
                            $page = new page();
                            $this->pageOutput .= $this->listPages();

                            break;

                        case "create":
                            $this->pageOutput .= $this->createPage();
                            break;

                        case "edit":

                            //case edit requested by some module as a GET url parameter
                            if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                $this->pageOutput .= $this->editPage(intval($_GET['uid']));
                            }
                            break;

                        case "delete":
                            $this->pageOutput .= $this->deletePage($_REQUEST['uid']);
                            break;
                    }
                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
                $this->localMenu = '';
            }

        }

    }

    function render(&$mainTemplate){

        global $mf,$l10n,$config;

        if($mf->mode == 'backend'){
            if(!isset($_REQUEST['module']) || ($this->moduleKey == $_REQUEST['module'])){

                $pagesBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/pages/ressources/templates/pages.html');

                $pagesBody = str_replace("{page}",$this->pageOutput,$pagesBody);
                $pagesBody = str_replace("{page_managment}","<span class=\"glyphicon glyphicon-menu glyphicon-file\" aria-hidden=\"true\"></span>".$l10n->getLabel('pages','page_managment'),$pagesBody);
                $pagesBody = str_replace("{manage_pages}",$l10n->getLabel('pages','manage_pages'),$pagesBody);
                $pagesBody = str_replace("{editing}",$l10n->getLabel('pages','editing'),$pagesBody);

                $mainTemplate = str_replace("{current_module}",$pagesBody,$mainTemplate);
            }
        }
        else if($mf->mode == 'frontend'){

            //process functions registered by other plugins on page load. Example purpose : create a statistics record when a page is loaded
            //functions must be registered using the addFrontendHook() method
            foreach($this->pageFrontendHooks as $hookFuncCall){
                eval($hookFuncCall.';');
            }

            $html = array();

            if(isset($mf->info['currentPageUid'])){

                $page = new page();

                // Processing HTTP Status codes in case $mf->info['http_status'] has been altered
                $httpStatus = $mf->info['http_status'];

                if($httpStatus != 200){
                    require_once DOC_ROOT.SUB_DIR.'/mf/plugins/pages/httpStatusCodes.php';
                    header('HTTP/1.0 '.$httpStatus.' '.$httpStatusCodes[$httpStatus]);
                }

                if( $httpStatus != 200 && (isset($mf->templates[$httpStatus.'-page']) || is_file(DOC_ROOT.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR.$httpStatus.'.html'))) {
                    //Load custom HTTP Status page
                    $templateName = $httpStatus.'-page';
                    if(isset($mf->templates[$templateName])){
                        $templateFile = $mf->templates[$templateName]['template_file'];
                        $page->data['template_content']['value'] = $mf->templates[$templateName]['template_data'];
                    }
                    else $templateFile = $httpStatus.'.html';

                }
                else {
                    //Load the current page to be displayed
                    if($page->load($mf->info['currentPageUid'])){

                        //increase view count if the current user is not logged into the back office
                        if(!isset($_SESSION['current_be_user'])){
                            $page->data['view_count']['value'] += 1;
                            $page->store();
                        }

                        //extracting template
                        $templateName = $page->data['template_name']['value'];
                        $templateFile = $mf->templates[$templateName]['template_file'];

                    }
                    else{
                        if($mf->info['currentPageUid']==0)
                            echo $l10n->getLabel('pages','no_pages').'<br />'.chr(10);
                        else echo $l10n->getLabel('pages','page_noexist').'<br />'.chr(10);
                        die();
                    }
                }
                //print_r($page);
               /*echo "templateName=".$templateName."<br />".chr(10);
                echo "templateFile =".$templateFile ."<br />".chr(10);
                echo "<br />".chr(10);*/

                if(isset($templateFile) && is_file(DOC_ROOT.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR.$templateFile)){

                    $templateHTML = file_get_contents(DOC_ROOT.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR.$templateFile);

                    foreach($page->data as $key => $value){


                        if($key != 'template_content') {
                            //here we are in the record definition (recorded directly in the database)
                            if(isset($value['dataType']) &&  $value['dataType'] != 'function'){
                                //main case
                                if(!in_array($key, array('meta_description','meta_keywords','meta_robots')))
                                if(!is_array($value['value'])){
                                    $templateHTML = str_replace("{".$key."}",$value['value'],$templateHTML);
                                }
                            }
                            else if(isset($value['dataType']) &&  $value['dataType'] != 'microtemplate'){
                                //microtemplate case
                                $microtemplates = self::processMicrotemplates($value['value'], $page);
                                $templateHTML = str_replace("{".$key."}",$microtemplates,$templateHTML);
                            }
                            else{
                                //functions
                                //calling the function and inserting its output
                                $functionOutput = '';
                                $functionName = $value;
                                if($functionName != '')eval('$functionOutput ='.$functionName.'($page,$key,$value[\'value\']);');
                                $templateHTML = str_replace("{".$key."}",$functionOutput,$templateHTML);
                            }
                        }
                        else if($key != ''){
                            //here we are in the template definition (recorded as a serialized array in the database)

                            //echo 'templateKey='.$templateKey.'<br />'.chr(10);
                            //if($templateKey == 'titre-page') echo 'titre-page='.$templateValue['value'].'<br />'.chr(10);
                            //print_r($page->data);

                            $templateDataStructure = $mf->templates[$templateName]['template_data'];
                            $templateValues = $value['value'];
                            $templateHTML = self::processTemplate($templateDataStructure, $templateValues, $templateHTML, $page);

                        }
                    }


                    //substituting values

                    $templateHTML = str_replace("{template_path}",SUB_DIR.$config['templates_directory'],$templateHTML);
                    $templateHTML = str_replace("{image_path}",$mf->getUploadsDir().'page'.DIRECTORY_SEPARATOR.'page_'.$mf->info['currentPageUid'].'/',$templateHTML);

                    //re-process page header values in case they were included by a function
                    $templateHTML = str_replace("{page_title}",$page->data['title']['value'],$templateHTML);

                   if($page->data['meta_description']['value']!=''){
                        $templateHTML = str_replace("{meta_description}",'<meta name="description" content="'.htmlentities($page->data['meta_description']['value']).'" />',$templateHTML);
                        $templateHTML = str_replace("{page_description}",htmlentities($page->data['meta_description']['value']),$templateHTML);
                   }
                    else {
                        $templateHTML = str_replace("{meta_description}",'',$templateHTML);
                        $templateHTML = str_replace("{page_description}",'',$templateHTML);
                    }

                    if($page->data['meta_keywords']['value']!=''){
                        $templateHTML = str_replace("{meta_keywords}",'<meta name="keywords" content="'.$page->data['meta_keywords']['value'].'" />',$templateHTML);
                        $templateHTML = str_replace("{page_keywords}",$page->data['meta_keywords']['value'],$templateHTML);
                    }
                    else {
                        $templateHTML = str_replace("{meta_keywords}",'',$templateHTML);
                        $templateHTML = str_replace("{page_keywords}",'',$templateHTML);
                    }

                    if($page->data['meta_robots']['value']!=''){
                        $templateHTML = str_replace("{meta_robots}",'<meta name="robots" content="'.$page->data['meta_robots']['value'].'" />',$templateHTML);
                        $templateHTML = str_replace("{page_robots}",$page->data['meta_robots']['value'],$templateHTML);
                    }
                    else{
                        $templateHTML = str_replace("{meta_robots}",'',$templateHTML);
                        $templateHTML = str_replace("{page_robots}",'',$templateHTML);
                    }

                    if(count($mf->frontendMessages)>0){
                        $messages = '<style>
                        #mf_messages{position:absolute;left:0;top:0;padding:20px 10px 0 10px;z-index:10000;}
                        .mf_message{background:rgba(101,101,101,.5);color:white;padding:3px 8px 3px 8px;font-weight:bold;}
                        </style>';
                        $messages .= '<div id=mf_messages>'.implode(chr(10),$mf->frontendMessages).'</div>';
                        $templateHTML = str_replace("{mindflow-messages}",$messages,$templateHTML);
                    }
                    else $templateHTML = str_replace("{mindflow-messages}",'',$templateHTML);

                    //look for specific markers with arguments such as {link} {url} or {label}
                    $seekMarkers = getMarkers($templateHTML);

                    foreach($seekMarkers as $marker){
                        $split = explode(' ',$marker[1]);
                        switch($split[0]){
                            case 'link':
                                if(isset($mf->modules->urlRewriter)){
                                    $pageUid = $class = $id = '';
                                    if(isset($split[1]))$pageUid = $split[1];
                                    if(isset($split[2]))$class = $split[2];
                                    if(isset($split[3]))$id = $split[3];

                                    $link = $mf->getPageLink($pageUid, $class, $id);

                                    $templateHTML = str_replace($marker[0],$link,$templateHTML);
                                }
                                else $templateHTML = str_replace($marker[0],getLabel('frontend','urlRewriter_missing'),$templateHTML);
                                break;

                            case 'url':
                                if(isset($mf->modules->urlRewriter)){
                                    $pageUid = '';
                                    if(isset($split[1]))$pageUid = $split[1];

                                    $link = $mf->modules->urlRewriter->getPageUrl($pageUid);

                                    $templateHTML = str_replace($marker[0],$link,$templateHTML);
                                }
                                else $templateHTML = str_replace($marker[0],getLabel('frontend','urlRewriter_missing'),$templateHTML);
                                break;

                            case 'label':
                                $domain = $label = '';
                                if(isset($split[1])) $domain = $split[1];
                                if(isset($split[2])) $label = $split[2];
                                $text = $mf->l10n->getLabel($domain,$label);
                                $templateHTML = str_replace($marker[0],$text,$templateHTML);

                                break;
                        }
                    }

                    }
                    else{
	                    $templateHTML = '<!doctype html><html><head><meta charset="utf-8"></head><body>'.
                       $l10n->getLabel('main','error').$l10n->getLabel('main','template').DOC_ROOT.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR.$templateFile.$l10n->getLabel('main','not_found').
                       '</body></html>';

                    }
                    $html[] = $templateHTML;



            }
            else{
                //404 page not found
            }
            //$html[] = $_SERVER['REQUEST_URI'];
            $mainTemplate[] = implode(chr(10),$html);
        }

        //print_r($page->data['template_content']);

    }





    function createPage(){
        global $mf;

        $page = new page();
        $form = $mf->formsManager;
        $advancedSettings = array(
            //force page form to reloaded when saved as different view of the fields may occur after page properties change
            'reloadFormOnSave' => 'true',
            'JSCallbackFunctionAfterSubmit'=>'refreshPageEntry',
        );

        return $form->editRecord($page, $this->moduleKey, '', true,true,true,true,0,$advancedSettings);
    }

    function editPage($uid){

        global $mf,$l10n;

        $page = new page();

        if($page->load($uid)){
            $form = $mf->formsManager;
            $advancedSettings = array(
                //force page form to reloaded when saved as different view of the fields may occur after page properties change
                'reloadFormOnSave' => 'true',
                'JSCallbackFunctionAfterSubmit'=>'refreshPageEntry',
            );
            return $form->editRecord($page, $this->moduleKey, '', true,true,true,true,0,$advancedSettings);
        }
        else return '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$l10n->getLabel('pages','page_noexist').'</div>';
    }

    function deletePage($uid){
        $page = new page();
        $page->load($uid);
        $page->delete();
        return $this->listPages();
    }

    function listPages(){
        global $mf,$l10n;

        $html = array();

        $html[] = '<a onclick="createPage();return false;" class="btn btn-primary btn-sm" style="margin:0 0 10px 0;">'.$l10n->getLabel('pages','add_page').' <i class="glyphicon glyphicon-file glyphicon glyphicon-white" ></i></a>';

        //form security variables
        $secKeys = array(
            'action' => "createPage",
            'record_class' => "page",
            'mode' => $mf->mode,
        );
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));


        $html[] = '<script>
            function createPage(){
                var jqxhr = $.post("{subdir}/mf/plugins/pages/ajax-pages-json.php", { action:"createPage", record_class: \'page\', sec: \''.$secHash.'\', fields: \''.$secFields.'\', mode: \''.$mf->mode.'\'})
                .success(function(jsonData) {
                    //remove no page message in case it would be present.
                    $("#no_page").remove();
                    //delete the <li> entry of the page from the DOM on success
                    $("#pagesRoot").prepend(jsonData.html);
                })
                .fail( function(xhr, textStatus, errorThrown) {
                    alert("pages.php createPage() : "+ xhr.responseText);
                })
        }
        </script>';

        $this->parsePages($mf->info['pageTree'], $html, $l10n, array('title'), $this->moduleKey, '');

        //inserting div for creating/displaying dialogs if not available in the page.
        $html[] = '
                    <div id="pg_dialogHolder"></div>
                    <script>
                    if($("#mf_dialogs").length == 0) {
                        document.getElementById("pg_dialogHolder").innerHTML="<div id=\"mf_dialogs\"></div>";
                    }
                    </script>';

        //form security variables
        $secKeys = array(
            'action' => "sortPages",
            'mode' => $mf->mode,
        );
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));

        $mf->addBottomJs("<script>
        var recordDialog;
        var recordOpenMethod='dialog';//'dialog' for jQuery UI dialog, 'window' for browser window

        $(document).ready(function(){
            $('.nestedSortableRoot').nestedSortable({
                forcePlaceholderSize: true,
                handle: '.glyphicon-move',
                helper:	'clone',
                items: 'li',
                opacity: .6,
                placeholder: 'placeholder',
                revert: 250,
                tabSize: 25,
                tolerance: 'pointer',
                toleranceElement: '> div',
                maxLevels: 0,
                isTree: true,
                //protectRoot: true,
                expandOnHover: 700,
                update: function() {
                    console.log($(this).nestedSortable('toArray'));
                    var order = $(this).nestedSortable('serialize') + '&action=sortPages&sec=".$secHash."&fields=".$secFields."&mode=".$mf->mode."';
                    $.post('".SUB_DIR."/mf/plugins/pages/ajax-pages-json.php', order);
                }
            });

            $('.disclose').on('click', function() {
			    $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		    })

            recordDialog = $('#record_dialog' ).dialog({
               autoOpen: false,
            });

        });



        //var fancyboxProxy = $.fancybox;
        </script>",true,false);

        return implode(chr(10),$html);
    }

    function parsePages($page, &$html, $l10n, $keysList, $moduleName, $subModuleName){
        global $mf;

        if(is_array($page)){
            //these are root pages

            $html[] = "<div>";
            if(sizeof($page) >0){
                foreach($keysList as $key){
                   $html[] = "<span>".$l10n->getLabel(get_class($page[0]),$key)."</span>";
                }
                $html[] = "<span class=pull-right>".$l10n->getLabel('backend','actions')."</span>";
            }
            $html[] = "</div><ol id='pagesRoot' class='nestedSortableRoot nestedSortableItem'>";
            //display the table header
            if(sizeof($page) >0){
                foreach($page as $thePage){
                    $this->showPageRow($thePage, $html, $l10n, $keysList, $moduleName, $subModuleName);
                }
            }
            else{
                //display "no page in table" message
                 $html[] = '<div class="alert fade in" id="no_page">
                '.$l10n->getLabel(get_class($this),'no_record').'
              </div>';
                $html[] = "<ol id='pagesRoot' class='nestedSortableRoot nestedSortableItem'></ol>";

            }
            $html[] = "</ol>";

            $html[] = '
        <script>
        var deleteRecordUid = -1;
        
        function showError(jsonData){
            var sliceIndex = jsonData.responseText.indexOf("{\"jsonrpc\"");
            alert("'.$l10n->getLabel('backend','mf_error').'\n\n"+jsonData.responseText.slice(0,sliceIndex));
        }
        
        function getDeleteModal(uid, title) {
        console.log(" getDeleteModal titre="+title + " uid="+uid);
            deleteRecordUid = uid;
            $(\'#deleteMessage\').html( "'.$l10n->getLabel('backend','areyousure_delete').'"+ title +" ?'.'" )
            $(\'#windowTitleDialogPages\').modal(\'show\');
            };
        function closeDialog () {
            $(\'#windowTitleDialogPages\').modal(\'hide\');
            };';

            //form security variables
            $secKeys = array(
                'action' => "deleteRecord",
                'record_class' => "page",
                'mode' => $mf->mode,
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $html[] = '
        function okClicked () {
            var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action:"deleteRecord", record_uid: deleteRecordUid, record_class: \'page\', sec: "'.$secHash.'", fields: "'.$secFields.'", mode: "'.$mf->mode.'"})
            .success(function(jsonData) {
                 //delete the <li> entry of the page from the DOM on success
                 $("#page_"+deleteRecordUid).remove();
            })
            .fail( function(xhr, textStatus, errorThrown) {
                alert("pages.php okClicked() : "+ xhr.responseText);
            })

            closeDialog ();
        };';

            //form security variables
            $secKeys = array(
                'action' => "showHideRecord",
                'record_class' => "page",
                'mode' => $mf->mode,
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $html[] = '

        function showHideRecord(uid) {
            toggleRecordUid = uid;
            var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action:"showHideRecord", record_uid: toggleRecordUid, record_class: \'page\', sec: "'.$secHash.'", fields: "'.$secFields.'", mode: "'.$mf->mode.'"})
            .success(function(jsonData) {

                 if(jsonData.hidden == "1"){
                    $("#page_"+toggleRecordUid+" i.showHide").filter(":first").removeClass("glyphicon-eye-close").removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");
                 }
                 else $("#page_"+toggleRecordUid+" i.showHide:first").filter(":first").removeClass("glyphicon-eye-close").removeClass("glyphicon-eye-open").addClass("glyphicon-eye-open");

            })
            .fail( function(xhr, textStatus, errorThrown) {
                alert("pages.php showHideRecord() : "+ xhr.responseText);
            })

            return false;
        };';

            //form security variables
            $secKeys = array(
                'action' => "getPageTitle",
                'record_class' => "page",
                'mode' => $mf->mode,
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $html[] = '
        
        function refreshPageEntry(status,jsonData){
            //console.log(jsonData);
            
            recordUid = jsonData.uid;
            
            var jqxhr = $.post("'.SUB_DIR.'/mf/plugins/pages/ajax-pages-json.php", { action:"getPageTitle", record_uid: recordUid, record_class: \'page\', sec: "'.$secHash.'", fields: "'.$secFields.'", mode: "'.$mf->mode.'"})
            .success(function(jsonData) {

                 if(jsonData.result == "success"){
                    $("#page_"+recordUid+" .pageTitle").html(jsonData.title);
                    
                 }
                 else showError(jsonData.message);

            })
            .fail( function(xhr, textStatus, errorThrown) {
                alert("pages.php refreshPageEntry() : "+ xhr.responseText);
            })
        }


        </script>
        <!-- Modal -->
        <div id="windowTitleDialogPages" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Suppression</h4>
                            </div>
                        <div class="modal-body">
                            <p id="deleteMessage"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="okClicked();">OK</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        ';

        }
        else{
            //these are sub-root (child) pages
             $this->showPageRow($page, $html, $l10n, $keysList, $moduleName, $subModuleName);
        }
    }

    /**
     * Processes display of a single page in the nestedSortable based page list of module pages
     * @param page $page the page data
     * @param array $html the html currently being edited
     * @param l10nManager $l10n the l10nManager object used for accessing translations
     * @param array $keysList record field keys for which value should be display in the page title area
     * @param string $moduleName current module key
     * @param string $subModuleName current submodule key
     */
    function showPageRow($page, &$html, $l10n, $keysList, $moduleName, $subModuleName){

        global $mf;

        $html[] = "<li id=page_".$page->data['uid']['value']." class=pageItem>";

        $html[] = "<div><span class='disclose'><span></span></span>";

        //Select page navigation title if available
        $pageTitle = ( isset($page->data['nav_title']['value']) && (trim($page->data['nav_title']['value']) != '') )?$page->data['nav_title']['value']:$page->data['title']['value'];

        foreach($keysList as $key){
            //special case check for page navigation title
            $value = ($key == 'title')?$pageTitle:$page->data[$key]['value'];

            if ($page->data['type']['value'] == 1)
                $html[] = "<i class='glyphicon glyphicon-folder-open' title='".$page->data['uid']['value']."' class='page".$key."'></i> <span class='pageTitle'>".$value."</span>";
            else
                $html[] = "<i class='glyphicon glyphicon-file' title='".$page->data['uid']['value']."'  class='page".$key."'></i> <span class='pageTitle'>".$value."</span>";
        }
        //$html[] = "<i class='glyphicon glyphicon-move'></i>";
        $html[] = "<span class=pageActions>";
        $html[] = "<span class=recordAction><a><i class='glyphicon glyphicon-move'></i></a></span>";

        // DISPLAY ACTION ICONS


        //$useAJAX = (isset($config['plugins']['pages']['useAJAX']))?$config['plugins']['pages']['useAJAX']:true;
        //$linkClass = ($useAJAX)?" class='open_ajax'":"";
        $linkClass = "";

        //if($useAJAX){

            //form security variables
            $secKeys = array(
                'action' => 'editRecord',
                'record_class' => 'page',
                'record_uid' => $page->data['uid']['value'],
                'mode' => $mf->mode,
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $advancedSettings = array(
                'reloadFormOnSave' => 'true',
                'JSCallbackFunctionAfterSubmit'=>'refreshPageEntry',
            );

            $buttonParams = '&showSaveButton=true&showSaveCloseButton=true&showPreviewButton=true&showCloseButton=true&advancedSettings='.urlencode(base64_encode(http_build_query($advancedSettings)));

            //default Mindflow link for editing a record
            $editLink = SUB_DIR."/mf/core/ajax-core-html.php?action=editRecord&record_class=page&record_uid=".$page->data['uid']['value']."&ajax=1".$buttonParams."&header=0&footer=0&sec=".$secHash."&fields=".$secFields."&mode=".$mf->mode;
        /*}
        else{

            //form security variables
            $secKeys = array(
                'action' => 'editRecord',
                'class' => 'page',
                'uid' => $page->data['uid']['value']
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $editLink = SUB_DIR."/mf/index.php?module=".$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'')."&action=edit&class=page&uid=".$page->data['uid']['value']."&sec=".$secHash."&fields=".$secFields;
        }*/
        //edit page action
        //$html[] = "<span class=recordAction><a".$linkClass." href='".$editLink."' title='".$l10n->getLabel('backend','edit')."'><i class='glyphicon glyphicon-pencil'></i></a></span>";
        $html[] = '<span class=recordAction><a'.$linkClass.' onclick="mf.dialogManager.openDialog(\'page_dialog\',\'\',\'\',\''.$editLink.'\',\'90%\', \'\', \'' . $l10n->getLabel('page', 'record_name') . '\');" title="'.$l10n->getLabel('backend','edit').'"><i class="glyphicon glyphicon-pencil"></i></a></span>';

        //if($useAJAX)
        //else $html[] = "<span class=recordAction><a href='{subdir}/mf/index.php?module=".$moduleName."&action=edit&uid=".$page->data['uid']['value']."' title='".$l10n->getLabel('backend','edit')."'><i class='glyphicon glyphicon-pencil'></i></a></span>";



        //show hide page action
        $visibilityIcon = ($page->data['hidden']['value']=='1')?'glyphicon glyphicon-eye-close':'glyphicon glyphicon-eye-open';
        $html[] = "<span class=recordAction><a onclick='javascript:showHideRecord(\"".$page->data['uid']['value']."\");' title='".$l10n->getLabel('pages','showhide')."'><i class='".$visibilityIcon." showHide'></i></a></span>";
        //delete page action
        if($page->data['no_delete']['value'] != 1) $html[] = "<span class=recordAction><a data-controls-modal='windowTitleDialogPages' data-backdrop='true' data-keyboard='true' href='javascript:getDeleteModal(".$page->data['uid']['value'].", \"".htmlentities($pageTitle, ENT_QUOTES, "UTF-8")."\");' title='".$l10n->getLabel('backend','delete')."'><i class='glyphicon glyphicon-trash'></i></a></span>";
        else $html[] = "<span class=recordAction><span class=voidIcon></span></span>";
        $html[] = "</span>";
        $html[] = "</div>";

        if(isset($page->children) && sizeof($page->children)>0){
            $html[] = "<ol class='nestedSortableItem'>";
            foreach($page->children as $child){
                $this->parsePages($child, $html, $l10n, $keysList, $moduleName, $subModuleName, '');
            }
            $html[] = "</ol>";
        }

        $html[] = "</li>";

    }


    static function processMicrotemplates($microtemplates, $page){
        $html = array();

        foreach($microtemplates as $microtemplate){
            $html[] = self::processMicrotemplate($microtemplate, $page);
        }
        return implode(chr(10),$html);
    }


    static function processMicrotemplate($microtemplate, $page){
        global $mf,$config;

        $html = array();

        $microtemplateKey = $microtemplate['microtemplate_key'];
        $microtemplateFile = $mf->microtemplates[$microtemplateKey]['microtemplate_file'];
        $microtemplateHTML = file_get_contents(DOC_ROOT.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR."microtemplates".DIRECTORY_SEPARATOR.$microtemplateFile);
        $templateDataStructure = $mf->microtemplates[$microtemplateKey]['microtemplate_data'];

        return self::processTemplate($templateDataStructure, $microtemplate['microtemplate_data'], $microtemplateHTML, $page);
    }

    /*
     * Processes a template a returns the HTML with the substituted values
     *
     */
    static function processTemplate($templateDataStructure, $templateValues, $templateHTML, $page){

        global $mf,$config;

        foreach($templateDataStructure as $templateKey => $templateValue){
            //overriding template value with page record value
            // we are iteration through the template definition only to have it up to date
            if(isset($templateValues[$templateKey]))
                $templateValue = $templateValues[$templateKey];

            //test to skip residual values from former template after switch on the page
            if(isset($templateDataStructure[$templateKey])){

                if(isset($templateDataStructure[$templateKey]['processor']))$functionName = $templateDataStructure[$templateKey]['processor'];
                else $functionName = '';

                if( $functionName == ''){
//echo "\$templateKey=".$templateKey." datatype=".substr($templateValue['dataType'],0,5)."<br />\n";
                    if(isset($templateValue['dataType']) &&  $templateValue['dataType'] == 'microtemplate'){
                        //microtemplate case
                        $microtemplates = self::processMicrotemplates($templateValue['value'], $page);
                        $templateHTML = str_replace("{".$templateKey."}",$microtemplates,$templateHTML);
                    }
                    else if(isset($templateValue['dataType']) &&  $templateValue['dataType'] == 'include'){
                        //include case

                        ob_start();

                        include DOC_ROOT.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR.$templateValue['value'];

                        $IncHTML = ob_get_clean();

                        $templateHTML = str_replace("{".$templateKey."}",$IncHTML,$templateHTML);
                    }
                    else if(isset($templateValue['dataType']) &&  substr($templateValue['dataType'],0,5) == 'files'){
                        $value = $templateValue['value'];
                        if(is_array($value) && isset($value[0])) $file = $mf->getUploadsDir().$value[0]['filepath'].$value[0]['filename'];
                        else $file = '';
                        $templateHTML = str_replace("{".$templateKey."}",$file,$templateHTML);
                    }
                    else{
                        //main case
                        $templateHTML = str_replace("{".$templateKey."}",$templateValue['value'],$templateHTML);
                    }
                }
                else{
                    //function
                    //calling the function and inserting its output
                    $functionOutput = '';
                    //retreive the function name directly from the template in case its definition gets updated
                    if(isset($templateDataStructure[$templateKey])){

                        //call the function with the current page object and current fieldName as an argument
                        if(!isset($templateValue['value']))$templateValue['value']='';
                        //echo '$functionOutput ='.$functionName.'($page, $templateKey, $templateValue[\'value\']);'.'<br/>'.chr(10);
                        eval('$functionOutput ='.$functionName.'($page, $templateKey, $templateValue[\'value\']);');
                        $templateHTML = str_replace("{".$templateKey."}",$functionOutput,$templateHTML);

                    }
                }
            }
        }

        return $templateHTML;
    }

    /**
     * Adds an external function to be called on page load, such as statItem::createStatEntry()
     * @param $hookValue
     */
    function addFrontendHook($hookValue){
        $this->pageFrontendHooks[]=$hookValue;
    }

    function getType(){
        return $this->moduleType;
    }

}
