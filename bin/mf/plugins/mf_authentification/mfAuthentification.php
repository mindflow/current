<?php
/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';

class mfAuthentification implements module
{

    private $moduleType = 'authentification,backend';

    private $getPassword = false;
    private $sentPassword = false;
    private $givenEmail = '';

    private $errorMessage='';



    function redirectLogin($forceHTTPS=false){

        //if there was a module called prior auth, redirect to that module
        if(isset($_SESSION['loginCalledUrl']) && $_SESSION['loginCalledUrl']!=''){
            $setUrl =  $_SESSION['loginCalledUrl'];
            unset($_SESSION['loginCalledUrl']);
            //redirect to called url prior login only if this url is in the mf backend (includes '/mf/' in its path)
            if(strpos( $setUrl , '/mf/') !== false)header("Location: ".$setUrl);
            else header("Location: ".(($forceHTTPS)?'https://':((!empty($_SERVER['HTTPS'])) ? "https://" : "http://")).$_SERVER['SERVER_NAME']."/mf/index.php");
        }
        //else redirect to login page
        else header("Location: ".(($forceHTTPS)?'https://':((!empty($_SERVER['HTTPS'])) ? "https://" : "http://")).$_SERVER['SERVER_NAME']."/mf/index.php");
    }

    function prepareData(){
        global $mf,$l10n,$config,$logger;

        switch($mf->mode){

            case "authentification":

                //process identification
                if(isset($_POST['advancedAuthLogin'])){

                    if(!$this->identify($_POST['advancedAuthLogin'], $_POST['advancedAuthPassword'])){
                        //if auth fails, prepare error message for display
                        $this->errorMessage = '<div class="alert alert-danger"><strong>'.$l10n->getLabel('mfAuthentification','invalid_identification').'</strong></div>';

                        //log failed auth with username
                        $logErrorMessage = $l10n->getLabel('mfAuthentification','log_invalid_identification');
                        $logger->notice($_POST['advancedAuthLogin'].' : '.$logErrorMessage);
                    }
                    else{
                        //success
                        if(isset($config['plugins']['mf_authentification'])){
                            if(isset($config['plugins']['mf_authentification']['forceSSL'])){

                                if($config['plugins']['mf_authentification']['forceSSL']){
                                    $this->redirectLogin(true);
                                }
                                else $this->redirectLogin(false);
                            }
                            else $this->redirectLogin(false);
                        }
                        else $this->redirectLogin(false);
                        exit;
                    }
                }
                else if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'getPassword'){

                    $this->getPassword = true;

                    if(isset($_REQUEST['userEmail'])){
                        if($_REQUEST['userEmail']!=''){
                            $this->givenEmail = $_REQUEST['userEmail'];

                            $status = mfUser::sendPassword($this->givenEmail);
                            if($status){
                                $this->errorMessage = '<div class="alert alert-success"><strong>'.$l10n->getLabel('mfAuthentification','email_sent').'</strong></div>';
                                $this->sentPassword = true;
                            }
                            else $this->errorMessage = '<div class="alert alert-danger"><strong>'.$l10n->getLabel('mfAuthentification','email_notfound').'</strong></div>';
                        }
                        else $this->errorMessage = '<div class="alert alert-warning"><strong>'.$l10n->getLabel('mfAuthentification','void_email').'</strong></div>';
                    }
                }
                else{
                    //when there is no specific action. This is the login landing page.
                    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "on"){
                        //force SSL if required
                        if(isset($config['plugins']['mf_authentification']['forceSSL'])){
                            if($config['plugins']['mf_authentification']['forceSSL'])header("Location: https://".$_SERVER['SERVER_NAME']."/mf/index.php");
                        }
                    }

                    //If there were URL parameters in the URL, save them so they can be restored after login
                    $parsedUrl = j_parseUrl(selfURL());
                    if(strlen($parsedUrl['query'])>0){

                        if (strpos($parsedUrl['query'],'authLogout') !== false) {
                            //do not save logout URL as this would create a dead en loop
                            unset($_SESSION['loginCalledUrl']);
                        }
                        else if(strpos($parsedUrl['path'],'/mf/install') !== false ){
                            //do not save install tool URL as it would redirect on it when loging in the MindFlow backend if the install tool has been opened first
                            unset($_SESSION['loginCalledUrl']);
                        }
                        else {
                            //save otherwise
                            $_SESSION['loginCalledUrl'] = selfURL();

                        }
                    }
                }
                break;

            case "backend":

                //process logout
                if(isset($_GET['authLogout'])){
                    unset($_SESSION['current_be_user']);
                    unset($mf->currentUser);
                }
                break;
        }

        //add custom CSS to main CSS
        $css = '<link rel="stylesheet" href="'.SUB_DIR.'/mf/plugins/mf_authentification/ressources/mfAuthentification.css" type="text/css" media="screen">';
        //prevent including twice due to dual context authentification + backend
        if(!in_array($css,$mf->getCssList()))$mf->addCss($css, true, false);


    }

    function render(&$mainTemplate){

        global $mf,$l10n;

        if($this->sentPassword)
            $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/mf_authentification/ressources/templates/sentPassword.html');
        else if($this->getPassword)
            $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/mf_authentification/ressources/templates/getPassword.html');
        else
            $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/mf_authentification/ressources/templates/mfAuthentification.html');

        $moduleBody = str_replace("{login}",(isset($_POST['advancedAuthLogin']))?$_POST['advancedAuthLogin']:'',$moduleBody);
        $moduleBody = str_replace("{please_identify}",$l10n->getLabel('mfAuthentification','please_identify'),$moduleBody);
        $moduleBody = str_replace("{errorMessage}",$this->errorMessage,$moduleBody);
        $moduleBody = str_replace("{username}",$l10n->getLabel('mfAuthentification','username'),$moduleBody);
        $moduleBody = str_replace("{your_username}",$l10n->getLabel('mfAuthentification','your_username'),$moduleBody);
        $moduleBody = str_replace("{connect}",$l10n->getLabel('mfAuthentification','connect'),$moduleBody);
        $moduleBody = str_replace("{forgotten_password}",$l10n->getLabel('mfAuthentification','forgotten_password'),$moduleBody);
        $moduleBody = str_replace("{password_recovery}",$l10n->getLabel('mfAuthentification','password_recovery'),$moduleBody);

        $moduleBody = str_replace("{user_email}",$l10n->getLabel('mfAuthentification','user_email'),$moduleBody);
        $moduleBody = str_replace("{your_email}",$l10n->getLabel('mfAuthentification','your_email'),$moduleBody);
        $moduleBody = str_replace("{receive_email}",$l10n->getLabel('mfAuthentification','receive_email'),$moduleBody);
        $moduleBody = str_replace("{given_email}",$this->givenEmail,$moduleBody);
        $moduleBody = str_replace("{back_to_login}",$l10n->getLabel('mfAuthentification','back_to_login'),$moduleBody);

        $moduleBody = str_replace("{password}",$l10n->getLabel('mfAuthentification','password'),$moduleBody);
        $moduleBody = str_replace("{your_password}",$l10n->getLabel('mfAuthentification','your_password'),$moduleBody);
        //$moduleBody = str_replace("{connect}",$l10n->getLabel('mfAuthentification','connect'),$moduleBody);
        $moduleBody = str_replace("{subdir}",SUB_DIR,$moduleBody);

        //$mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);
        $mainTemplate = str_replace("{sa_loginform}",$moduleBody, $mainTemplate);


    }



    function getType(){
        return $this->moduleType;
    }


    function identify($user_name, $password){

        if($user_name !=''){
            global $mf,$pdo;

            //checking username and password against database
            $sql = "SELECT * FROM mf_user WHERE username = :user_name AND backend_access = 1 AND deleted = '0' AND hidden = '0' ";
            $stmt = $pdo->prepare($sql);
            // AND MotDePasse = :password

            try{
                //echo "pass=".crypt ( $password, $config['salt'] );
                $stmt->execute(array(':user_name' => $user_name));
                // ':password' => crypt ( $password, $config['salt'] )
            }
            catch(PDOException $e){
                echo $e->getMessage();
            }

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            //echo "rowcount=".$stmt->rowCount();

            if($stmt->rowCount() > 0){
                $hash  = str_replace ( '\$' , '$' , $row['password'] );
                //security check against the user altering the SQL request
                //echo 'password_verify('.$password.', '.$hash.')='.password_verify($password, $hash);
                if (password_verify($password, $hash)) {
                    // Valid
                    $current_user = new mfUser();
                    $current_user->load($row['uid']);
                    if(WEB_MODE){
                        $mf->currentUser = $current_user;
                        return $_SESSION['current_be_user'] = $current_user;
                    }
                    else return $current_user;
                } else {
                    // Invalid
                    return 0;
                }

            }
            else return 0;
        }
        else return 0;

    }


}
