<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';

class mfUsersPlugin implements plugin{

    function setup(){
    }

    function getPath(){
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getName(){
        global $l10n;
        return $l10n->getLabel('mfUsersPlugin','name');
    }

    function getDesc(){
        global $l10n;
        return $l10n->getLabel('mfUsersPlugin','desc');
    }

    function getKey(){
        return "mf_users";
    }


    function getVersion(){
        return "1.0";
    }

    function getDependencies(){
        $dependencies = array();
        return $dependencies;
    }

    function init(){
        global $mf,$l10n;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');

        //inform MindFlow of the new dbRecords supplied by this plugin
        $mf->pluginManager->addRecord('mf_users', 'mfUser', $relativePluginDir.'/records/mfUserGroup.php');
        $mf->pluginManager->addRecord('mf_users', 'mfUserGroup', $relativePluginDir.'/records/mfUserGroup.php');

        //load template localisation
        l10nManager::loadL10nFile('templates',$relativePluginDir.'/languages/userGroupsTemplate_l10n.php');

        //load default plugin
        $mf->pluginManager->loadModule($relativePluginDir,"modules","users", true);
        $mf->pluginManager->loadModule($relativePluginDir,"modules","userGroups", true);

        //define available plugin user rights
        if(class_exists('mfUserGroup')) mfUserGroup::defineUserRight('mf_users', 'allowEditUsers',array(
            'value' => '0',
            'dataType' => 'checkbox',
            'valueType' => 'number',
            'processor' => '',
            'div_attributes' => array('class' => 'col-lg-2'),
        ),
            $relativePluginDir.'/languages/userRights_l10n.php'
        );

        if(class_exists('mfUserGroup')) mfUserGroup::defineUserRight('mf_users', 'allowEditUserGroups',array(
            'value' => '0',
            'dataType' => 'checkbox',
            'valueType' => 'number',
            'processor' => '',
            'div_attributes' => array('class' => 'col-lg-2'),
        ),
            $relativePluginDir.'/languages/userRights_l10n.php'
        );

    }
}

$mf->pluginManager->addPluginInstance(new mfUsersPlugin());


