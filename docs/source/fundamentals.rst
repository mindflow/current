.. |vspace| raw:: latex

   \vspace{5mm}
   
.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>
	
|embed_js|


************
Fondamentaux
************



===================
Frontend et Backend
===================

MindFlow distingue les notions de **frontend** et de **backend**. Il est important de comprendre la signification de ces termes car il sont repris dans tous les documents concernant MindFlow :

.. _frontend:

Frontend
--------

*	**C'est la partie "site internet"**, c'est à dire la partie publique vue par les internautes qui consultent le site. Elle permet de consulter et d'afficher des gabarits HTML, des données et des formulaires sur le site Internet.

.. _backend:


Backend
-------

*	**C'est est la partie "back office"**, le module d'administration privé à partir duquel le webmestre gère les contenus du site Internet. En plus de ce que peut faire le Frontend, le Backend fournit en standard des services d'interface graphique et de gestion des droits d'accès utilisateur.

Connexion au backend
--------------------

Pour se connecter à la partie Frontend d'un site web sous MindFlow, il suffit évidement de taper l'adresse du site web :

http://www.mydomain.tld

Pour se connecter à la partie Backend d'un site web sous MindFlow, il faut taper l'adresse du site web, suffixée du dossier "/mf", "mf" correspondant aux initiales de MindFlow :

http://www.mydomain.tld/mf

L'écran de connexion apparaît alors :

.. image:: images/login.jpg
    :alt: Ecran de connexion à MindFlow

.. note::
	L'image de fond, peut être très simplement personnalisée depuis le fichier de configuration du site web, normalement situé dans le dossier ``/mf_websites`` et souvent nommé ``config-dev.php`` ou ``config-prod.php``. 
	
	Pour ce faire, il faudra modifier la variable ``$config['backend_template_background']``. Vous pourrez pointer celle-ci soit vers une image, soit vers un dossier pour les backgrounds responsives.

	S'il s'agit d'un dossier, celui-ci doit contenir 4 fichiers nommés respectivement : 
	
	* bg-640x1024.jpg 
	* bg-1280x800.jpg 
	* bg-2100x1312.jpg
	* bg-3840x2400.jpg 
	
	Ceux-ci s'afficheront en fonction de la résolution d'écran de l'utilisateur. Vous pouvez créer votre propre dossier, qui devra comporter chacun de ces fichiers ajustés aux dimensions indiquées dans son nom. MindFlow propose 2 fonds par défaut : 
	
	* /mf/backend/templates/mf-default/img/bg1
	* /mf/backend/templates/mf-default/img/bg2

Outre l'image de fond, le gabarit peut-être entièrement personnalisé pour le client :

.. image:: images/login-accorderies.jpg
    :alt: Ecran de connexion personnalisé

Dans le cas de modifications élaborées comme le deuxième exemple ci-dessus, le gabarit HTML d'affichage de l'écran d'authentification est géré par un Plugin d'authentification adapté sur mesure, comme mentionné dans la section suivante Arborescence des dossiers et fichiers dans MindFlow.

===================================================
Arborescence des dossiers et fichiers dans MindFlow
===================================================

Voici un tour rapide du système de fichiers de MindFlow. Il vous aidera à comprendre son fonctionnement.
Une application MindFlow contient toujours les dossiers suivants :

.. image:: images/dossiers-mindflow.png
    :alt: Les principaux dossiers de MindFlow

``/mf :`` contient la totalité des classes qui "motorisent" MindFlow. Il s'agit du coeur du programme.

``/mf_config :`` contient les paramètres de configuration généraux de MindFlow

``/mf_librairies :`` contient les librairies (généralement javascript) tierces parties sur lesquelles repose MindFlow, par exemple jQuery-ui

``/mf_websites :`` contient dans chaque sous dossier un site web ou application, avec ses paramètres de configuration spécifiques, ses gabarits et ses fichiers (y compris les fichiers uploadés par les utilisateurs)

``.htaccess :`` le fichier habituel de directives pour le serveur Apache, incluant les instructions de réécriture d'URL nécessaires au bon fonctionnement de l'application

``index.php :`` c'est le fichier PHP principal appelé par les navigateurs quand un site web est demandé. C'est à partir de lui que démarre l'exécution de MindFlow pour un site web public. Pour l'accès au module d'administration, l'exécution démarre depuis le fichier /mf/index.php



Rentrons un peu plus en détail dans le contenu de ces dossiers :

**Contenu du dossier /mf**
--------------------------

``/backend :`` contient la classe backend.php qui gère l'exécution de MindFlow en mode back office / module d'administration, ainsi que les gabarits HTML de celui-ci.

``/core :`` contient l'ensemble des classes qui constituent le noyau de MindFlow. Ce dossier est le plus important de tous.

``/frontend :`` contient la classe frontend.php qui gère l'exécution de MindFlow en mode Frontend / site web

``/languages :`` contient les traductions des textes affichés par les classes de MindFlow

``/plugins :`` contient les plugins système de MindFlow. Chaque Plugin apporte ses  propres fonctionnalités. Les plugins les plus importants sont :

* **mf_authentification** (ou simpleAuthentification dans les anciennes versions) : gère l'authentification pour se connecter au Backend.

|vspace|

* **pages :** gère l'affichage des pages dans le cadre d'une utilisation site Internet ainsi que leur édition en Backend. 

|vspace|

* **UrlRewriter :** assure la gestion de la réécriture d'URL sur la partie Frontend / site internet.

|vspace|

* **Welcome :** affiche une page de bienvenue lors de la connexion au Backend de MindFlow



``/index.php :`` c'est le fichier qui déclenche l'affichage du Backend MindFlow (quand vous appelez http://www.mydomain.tld/mf , c'est lui qui s'exécute)

``/mf.php :`` la classe "mf" est l'objet de le plus important de Mindflow. Elle est facilement accessible en tout point de votre site ou application, et vous donne facilement accès à toutes les informations et plugins dont vous avez besoin.

**Contenu du dossier /mf_config**
---------------------------------

.. image:: images/dossier-config.png
    :alt: Le dossier mf_config/
	
Celui-ci ne contient qu'un seul fichier, le fichier ``config.php``. Ce fichier, assez court, permet de définir :

* le dossier de travail de MindFlow, utile si par exemple vous souhaitez placer votre site dans un sous-dossier de l'hébergement. Dans ce cas vous pouvez renseigner la constante ``SUB_DIR`` avec le nom du sous-dossier.

|vspace|

* Le format des dates employés dans MindFlow

|vspace|

* **Et surtout, quel fichier de configuration spécifique doit être chargé en fonction du nom de domaine appelé pour ouvrir le site :**

::

	switch($_SERVER['HTTP_HOST']){
		
		case 'dev.mydomain.tld':
			require_once DOC_ROOT.SUB_DIR.'/mf_websites/mydomain/config-dev.php'; 
			break;
        
		case 'www.mydomain.tld':
			require_once DOC_ROOT.SUB_DIR.'/mf_websites/mydomain/config-prod.php';
			break;
            
        default:
            echo "<p>Warning : ".$_SERVER['HTTP_HOST']. " : this hostname could not be found in mf_config/config.php</p>";
            die();
	}


**Contenu du dossier /mf_librairies**
-------------------------------------

.. image:: images/dossiers-mflib.png
    :alt: Le dossier mf_librairies/

On y trouve les bibliothèques tierces parties employées par MindFlow. Il s'agit principalement de bibliothèques javascript, mais d'autres types de bibliothèques pourraient y trouver leur place.

L'idée est de disposer d'un endroit centralisé ou l'on pourra aller piocher dans les bibliothèques déjà présentes : inutile par exemple d'intégrer une copie de jQuery-ui dans chaque Plugin : on pourra se reposer sur la version déjà installée si celle-ci convient.

**Contenu du dossier /mf_websites**
-----------------------------------

.. image:: images/dossiers-mfwebsite.png
    :alt: Le dossier mf_websites/

Le dossier **/mf_websites** doit contenir un dossier par site web. L'idée étant que chaque dossier contienne l'ensemble des données du site web et des plugins spécifiques qu'il emploie. Si vous vouliez sauvegarder votre site web ou le déplacer sur une autre installation MindFlow, vous n'auriez que ce dossier à déplacer, pour des opérations plus souples et plus rapides.

**Il est conseillé de nommer le dossier de votre site web par son nom de domaine**, ceci permettant de le repérer immédiatement dans le cas d'installation de multiples sites web : **/mf_websites/mydomain** ou **/mf_websites/mydomain.tld**

En dessous de ce dossier principal, vous devriez idéalement mettre en place les dossiers suivants :

``/plugins :`` permet de stocker les dossier propres à votre site, notamment les plugins développés sur mesure ou alors ceux dont le code a pu être modifié par rapport à la version standard, et qui n'ont donc plus leur place dans le dossier /mf/plugins,

``/temp :``` dossier temporaire pour l'upload des fichiers. Ce dossier doit impérativement être présent et le serveur web doit y disposer des droits d'écriture. La variable ``$config['temp_directory']`` du fichier de configuration spécifique au site doit pointer dessus.

``/uploads :`` dossier pour le stockage permanent des documents uploadés par les utilisateurs. Ce dossier doit impérativement être présent et le serveur web doit y disposer des droits d'écriture. La variable ``$config['uploads_directory']`` du fichier de configuration spécifique au site doit pointer dessus.

``/templates :`` c'est le dossier le plus important de votre site Internet : vous y placerez les gabarits HTML, les scripts CSS et Javascript, ainsi que les fonctions PHP requises pour rendre dynamique l'affichage de vos pages. Notez que ce dossier concerne essentiellement les sites Internet. Une application exploitant uniquement le backend de MindFlow n'aura dans l'absolu pas besoin d'exploiter le dossier "templates". La variable ``$config['templates_directory']`` du fichier de configuration spécifique au site doit pointer sur ce dossier.

``config-dev.php`` et ``config-prod.php`` :  ce sont les fichiers de configuration spécifique à votre site Internet, qui indiquent le paramétrage de MindFlow  que vous souhaitez employer. Dans l'absolu, vous pouvez si vous le voulez n'avoir qu'un seul de ces fichiers, ou en avoir plus de 2, et vous pouvez leur donner le nom que vous voulez (celui-ci doit juste être reflété dans le fichier de configuration générale ``/mf_config/config.php``).
En appelant l'un ou l'autre de ces fichiers en fonction d'un nom de domaine précisé dans ``/mf_config/config.php``, vous pouvez par exemple spécifier des paramètres de base de données différents entre vos sites. C'est pratique lorsque vous jonglez entre un site de développement et un site de production.
Vous pourriez aussi avoir 2 sites qui exploitent le même dossier de gabarits (/templates), mais qui stockent leurs données (/uploads) dans 2 dossiers différents et 2 bases de données différentes : cela sera utile si vous souhaitez créer plusieurs sites différents exploitant la même mise en page.

**Zoom sur le dossier /uploads**
--------------------------------

Le dossier ``/uploads`` contient les documents qui ont été transférés dans les objets hérités de la classe dbRecord utilisés dans votre application ou votre site web.

Prenons par exemple l'objet newsItem du Plugin news de MindFlow. Cet objet est destiné à stocker une actualité ou un billet de blog dans la base de données. Pour ce faire, parmi d'autres champs ('titre', 'date', 'texte'...), il contient un champ 'images' qui va stocker les images  associées à cette actualités et uploadées par le webmaster du site via le formulaire d'édition de notre objet newsItem.

Ces images ne sont pas stockées dans la base de données : bien que ce soit tout à fait possible, cela alourdirait considérablement celle-ci et rendrait les imports exports de données longs et fastidieux, voir impossible sur certains serveurs. Pour ces raisons, stocker des images en base de données est rarement souhaitable. Le système de fichier du disque du serveur est conçu pour stocker du fichier, et c'est donc là que MindFlow les écrit, dans le dossier  ``/mf_websites/votre_site/uploads/`` . Les seules informations que MindFlow écrira dans la base de données sont le nom du fichier, son chemin, et éventuellement quelques meta-informations supplémentaires comme la largeur ou la hauteur pour une image.

Si l'on écrivait tous les fichiers transférés directement sous le dossier ``/uploads/``, après plusieurs années d'exploitation de votre site, le résultat pourrait se révéler assez indigeste, avec plusieurs milliers de fichiers divers et variés mélangés sous la racine de ce même dossier.

Nous avons donc imaginé une structure de dossiers qui permettrait de retrouver facilement les fichiers associés à un enregistrement :

Admettons que vous avez créé un objet newsItem qui a pour UID la valeur 38 (**UID = Unique ID = clef primaire dans la table de données de l'enregistrement concerné**). Celui-ci sera stocké dans le dossier :

::

	/uploads
		/newsItem
			/newsitem_38
			
Ainsi le sous-dossier **/newsItem_38** contient tous les fichiers uploadés dans l'objet newsItem ayant pour **UID 38**. Ceci permet d'accéder facilement à ces données, et se révèle extrêmement pratique lorsque vous devez opérer des migrations de données entre une version de développement et une version de production de votre site/application par exemple.

Pour information, le champ UID est spécifié en AUTO INCREMENT dans la base de données, ce qui implique que sa valeur augmente constamment, chaque fois qu'on ajoute un nouvel enregistrement. On pourra donc récupérer facilement tous les objets créés après celui ayant pour UID 25 par exemple : il suffira de récupérer les dossiers mentionnant un UID supérieur à 25.

Si par contre on veut récupérer les dossiers de tous les objets modifiés depuis 1 mois, on pourra obtenir la liste des UID concernés en accédant au champ standard 'modification_date' que contiennent tout objet hérité de la classe dbRecord, avec une requête SQL du genre :

.. code-block:: mysql

	SELECT uid FROM mf_news WHERE modification_date > DATE_ADD(NOW(),INTERVAL -1 MONTH)

On pourra ensuite copier les dossiers manuellement ou au moyen d'un script, selon la quantité de dossiers à traiter.

