<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(



    'fr' => array(
        'insee_com' => 'Code INSEE',
        'postal_code' => 'Code Postal',
        'nom_comm' => 'Commune',
        'nom_dept' => 'Département',
        'nom_region' => 'Région',
        'statut' => 'Statut',
        'z_moyen' => 'Altitude Moyenne',
        'superficie' => 'Superficie',
        'population' => 'Population',
        'geo_point_2d' => 'geo_point_2d',
        'geo_shape' => 'geo_shape',
        'id_geofla' => 'ID Geogla',
        'code_comm' => 'Code Commune',
        'code_cant' => 'Code Canton',
        'code_arr' => 'Code Arrondissement',
        'code_dept' => 'Code Département',
        'code_reg' => 'Code Région',

        /*mandatory records*/
        //'no_record' => 'Aucun résultat.',
        'record_name' => 'France',
        'new_record' => 'Ajout commune'
    ),

);

?>