# MindFlow 1.9
MindFlow est un CMS PHP5 / MYSQL développé selon des objectifs de performance et de modularité. 
MindFlow est conçu pour fournir une solution souple et efficace pour des programmeurs. Si vous n'avez pas de connaissance du langage PHP et recherchez une interface configurable en WYSIWYG nous vous suggérons de rechercher un autre outil. 

Principales fonctionnalités :

* Génération de code HTML5 / UTF-8
* Module d'administration Bootstrap 3.0 responsive
* Supporte les sites et modules d'administration multilingues
* Pas de langage de script à la noix : uniquement du PHP brut a tous les niveaux = performance + flexibilité
* Fonctionne également entant que framework, par exemple pour générer des fichiers JSON ou des scripts CLI
* Génération et traitement automatique de formulaires Bootstrap 3.0
* Securité renforcée des formulaires avec prévention des injections de données

MindFlow est distribué sous licence Apache version 2

# Téléchargement

Dernière version :
* http://www.mindflow-framework.org/binaries/1.9/mindflow-v1.9.3.zip

Versions antérieures :
* http://www.mindflow-framework.org/binaries/1.9/mindflow-v1.9.1.zip
* http://www.mindflow-framework.org/binaries/1.9/mindflow-v1.9.zip
* http://www.mindflow-framework.org/binaries/1.8/mindflow-v1.8.2.zip
* http://www.mindflow-framework.org/binaries/1.7/mindflow-v1.7.zip
* http://www.mindflow-framework.org/binaries/1.6/mindflow-v1.6.zip
* http://www.mindflow-framework.org/binaries/1.4/mindflow-v1.4.zip


# Documentation

La documentation développeur est publiée sur le site suivant :

* http://www.mindflow-framework.org/docs/1.9/sphinx/
* http://www.mindflow-framework.org/docs/1.9/phpdoc/

Documentation antérieure :
MindFlow 1.8 :
* http://www.mindflow-framework.org/docs/1.8/sphinx/
* http://www.mindflow-framework.org/docs/1.8/phpdoc/

Pour le développement de site Internet, vous pouvez consulter le dossier /bin/mf_websites/www.sample-website.com et vous faire une idée du fonctionnement de la chose.

Une version inaboutie de documentation phpdocumentor est présente dans le dossier /phpdocs


|

-----------------------------------------------------------------------------------

|

# MindFlow 1.9
A PHP5 / MySQL CMS developed with high performance and modularity goals.
MindFlow is designed to be and efficient tool for programmers. If you have no experience with the PHP programming language and are looking for a WYSIWYG interface, we suggest you try another tool.

Features :

* HTML5 / UTF-8 Generation
* Bootstrap 3.0 responsive backend
* Supports multi-lingual websites and backend
* No scripting bullshit : just plain PHP everywhere = performance + flexibility
* Works as a framework as well, say for generating json files or cli scripts
* Automatic Bootstrap 3.0 forms generation and submission processing
* Form security with data injection prevention+

MindFlow is licenced under the Apache Licence version 2

# Download

Actual version :
* http://www.mindflow-framework.org/binaries/1.9/mindflow-v1.9.3.zip

Previous versions :
* http://www.mindflow-framework.org/binaries/1.9/mindflow-v1.9.1.zip
* http://www.mindflow-framework.org/binaries/1.9/mindflow-v1.9.0.zip
* http://www.mindflow-framework.org/binaries/1.8/mindflow-v1.8.2.zip
* http://www.mindflow-framework.org/binaries/1.7/mindflow-v1.7.zip
* http://www.mindflow-framework.org/binaries/1.6/mindflow-v1.6.zip
* http://www.mindflow-framework.org/binaries/1.4/mindflow-v1.4.zip

# Documentation

See the documentation for installation and usage as a developper:

* http://www.mindflow-framework.org/docs/1.9/sphinx/  (French only)
* http://www.mindflow-framework.org/docs/1.9/phpdoc/

Older documentation :
MindFlow 1.8 :
* http://www.mindflow-framework.org/docs/1.8/sphinx/ (French only)
* http://www.mindflow-framework.org/docs/1.8/phpdoc/

For website development, you may look at folder /bin/mf_websites/www.sample-website.com and figure how it works

An early phpdocumentor documentation is available in the /phpdocs folder



