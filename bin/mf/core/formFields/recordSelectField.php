<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Defines a recordSelect field, allowing to display a list of records from another table inside a form field. recordEditTable and recordSelectField allow to create a one to may relationship.
 * The recordSelect field retains in its 'value' parameter the list of uids of the records associated with the current record.
 * The field can be run in 2 modes according to the developer's needs: 'initItemsSQL' or 'addItemsSQL'.
 * 'initItemsSQL' pre-populates the field with the result of the configured SQL request, allowing the user to pick among those items those which he wishes to select.
 * 'addItemsSQL' starts with an empty field and allows the user to add items among the set defined by the SQL request.
 */
class recordSelectField implements formField
{
    /**
     * Returns the HTML code for displaying the field, initialized with the data supplied in $fieldData
     *
     * Creates a select box populated with records from other tables. Records can be edited, deleted, copied, pasted in the same field or an other field displaying the same type of records. Records can be created too.
     * All these features can be enabled / disabled by configurable options.
     * VERY IMPORTANT : the added record (a dbRecord extended class) MUST feature a column/field named parent_uid of type int(11), which is required for the copy paste operations.
     * This column will/must contain the uid of the parent record where the current field is displayed as an option in a select field.
     *
     * @param $f fieldData : this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
     * $f->formID           // value of the id attribute of the <form> tag this field is included in
     * $f->mode             // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
     * $f->dataType         // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
     * $f->key              // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
     * $f->fieldData        // this is the data array of the current field
     * $f->recordUid        // the uid (Unique ID) of the record containing the current field
     * $f->recordClassName  // the class name of the record containing the current field
     * $f->record           // all the data for the record containing the current field
     * $f->microKey         // field name when processing a microTemplate field (only used when mode=="micro|")
     * $f->microIndex       // sorting index when processing a microTemplate field (only used when mode=="micro|")
     * @returns String a string containing the HTML for displaying the field
     */
    static public function displayField($f){
        global $mf,$formsManager;

        $formsManager->addTopJs('typeahead','<script type="text/javascript" src="'.SUB_DIR.'/mf_librairies/Bootstrap-3-Typeahead/bootstrap3-typeahead.min.js"></script>');
        $formsManager->addTopJs('recordSelectField','<script type="text/javascript" >
            //list of multiselect instances with option saveAll enabled
            if (typeof multiSelectSaveAllIDs === "undefined") {
                var multiSelectSaveAllIDs = new Array();
                var multiSelectSaveAllSelections = new Array();
            }

        </script>');


        $l10n = $mf->l10n;
        //$pdo = $mf->db->pdo;

        //create an instance of our record so we are sure its l10n file is loaded (for dbForms, it might not).
        $editedRecord = new $f->fieldData['editClass']();

        $mfSetLocale = (isset($f->record->data['language']['value']) && $f->record->data['language']['value'] != '')?$f->record->data['language']['value']:$mf->getLocale();

        $html = array();
        //$dataType = $f->fieldData['dataType'];
        $fieldAttr = formsManager::getTagAttributes('field',$f->key, $f->fieldData);
        $divAttr = formsManager::getTagAttributes('div',$f->key, $f->fieldData);

        //display the label in front of the field or not
        $showLabel      = (isset($f->fieldData['showLabel']))?      $f->fieldData['showLabel']      :true;

        //display a <div> around the field or not
        $showFieldDiv   = (isset($f->fieldData['showFieldDiv']))?   $f->fieldData['showFieldDiv']   :true;

        //display custom HTML in front of the field
        $preFieldHTML   = (isset($f->fieldData['preFieldHTML']))?   $f->fieldData['preFieldHTML']   :'';

        //display custom HTML in after the field
        $postFieldHTML  = (isset($f->fieldData['postFieldHTML']))?  $f->fieldData['postFieldHTML']  :'';

        //display custom HTML in front of the field
        $preDivHTML   = (isset($f->fieldData['preDivHTML']))?   $f->fieldData['preDivHTML']   :'';

        //display custom HTML in after the field
        $postDivHTML  = (isset($f->fieldData['postDivHTML']))?  $f->fieldData['postDivHTML']  :'';


        //Add icon before label
        if(isset($f->fieldData['icon']))$icon = $f->fieldData['icon'];
        else $icon = '<b class="glyphicon glyphicon-list-alt"></b>';

        $f->fieldData['innerPreLabelHTML'] = (isset($f->fieldData['innerPreLabelHTML']))? $f->fieldData['innerPreLabelHTML'].$icon.'&nbsp;' : $icon.'&nbsp;';


        if($showLabel)$html[] = formsManager::getFieldLabel($f->fieldData, $f->mode, $f->key, $f->recordClassName);

        if(!isset($f->fieldData['debugSQL']))$f->fieldData['debugSQL']=0;
        if(!isset($f->fieldData['keyProcessors']))$f->fieldData['keyProcessors']=array();

        if(!isset($f->fieldData['editable']))$f->fieldData['editable']=true;
	    $isEditable = filterBoolean($f->fieldData['editable']);

        $selectID = $f->mode.$f->key;
        $dialogSelectID = str_replace ( '|' ,'_' ,$selectID);

        $viewEnabled        = isset($f->fieldData['selectActions']['view'])     && $f->fieldData['selectActions']['view'] != '0';
        $editEnabled        = isset($f->fieldData['selectActions']['edit'])     && $f->fieldData['selectActions']['edit'] != '0';
        $createEnabled      = isset($f->fieldData['selectActions']['create'])   &&  $f->fieldData['selectActions']['create'] != '0';
        $addEnabled         = isset($f->fieldData['selectActions']['add'])      &&  $f->fieldData['selectActions']['add'] != '0';
        $deleteEnabled      = isset($f->fieldData['selectActions']['delete'])   && $f->fieldData['selectActions']['delete'] != '0';
        $removeEnabled      = isset($f->fieldData['selectActions']['remove'])   && $f->fieldData['selectActions']['remove'] != '0';
        $sortEnabled        = isset($f->fieldData['selectActions']['sort'])     && $f->fieldData['selectActions']['sort'] != '0';
        $clipboardEnabled   = isset($f->fieldData['selectActions']['clipboard']) && $f->fieldData['selectActions']['clipboard'] != '0';
        $searchEnabled      = isset($f->fieldData['selectActions']['search'])   && $f->fieldData['selectActions']['search'] != '0';

        //If the records are allowed to be edited by someone else than their creator, $allowCrossEditing = true
        $allowCrossEditing = (isset($f->fieldData['allow_cross_editing']))? $f->fieldData['allow_cross_editing'] : true ;

        $selectTypeTag = ($f->fieldData['editType'] == 'multiple')?' multiple="multiple"':'';
        $html[] = $preDivHTML;
        $html[] = ($showFieldDiv)?'<div class="input-group input-group-mf-padding '.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
        $html[] = '<table>
                    <tr>'.
                    //'.(($icon != '')?'<td class="multiselect-col"><span class="input-group-addon">'.$icon.'</span></td>':'').'
                    '<td class="multiselect-col">';


        // SELECT Field generation
        if($isEditable){
            $html[] = $preFieldHTML;
            $html[] = '<select name="'.$selectID.'[]" id="'.$selectID.'" class="form-control '.$f->fieldData['editType']." ".$fieldAttr['classes'].'"'.$fieldAttr['attributes'].$selectTypeTag.((!$isEditable)?' disabled':'').'>';

        }
        else {
            $html[]='<input type="hidden" value="'.$f->fieldData['value'].'" name="'.$selectID.'" id="'.$selectID.'"/>';
            $selectID = $selectID.'_disabled';
            $html[] = $preFieldHTML;
            $html[]='<select name="'.$selectID.'[]" id="'.$selectID.'" class="form-control '.$f->fieldData['editType']." ".$fieldAttr['classes'].'"'.$fieldAttr['attributes'].$selectTypeTag.'>';

        }

        //Now get the list + data of the selected records displayed in this field
        $records = self::getSelectItems($f);

        //convert those records to <option> tags
        $options = self::getOptions($records,$f);
        $html = array_merge($html,$options);

        $html[] = '</select>';
        $html[] = $postFieldHTML;
        $html[] = '</td><td class="multiselect-col">';

        //component ID selector generation
        $jquerySelectID = '#'.str_replace('|','\\\\|',$selectID);


        //enable saveAll mode if in addItems mode
        if(isset($f->fieldData['addItemsSQL']) && ($f->fieldData['editType'] == 'multiple')){
            $html[] = '
                <script type="text/javascript">

                if(multiSelectSaveAllSelections["'.$jquerySelectID.'"]==null){
                    // restore value from record data if no value has been saved in javascript (first form display case).
                    multiSelectSaveAllSelections["'.$jquerySelectID.'"]="'.rtrim($f->fieldData['value'],',').'";
                }

                // stores the current select ID so it can be processed so it has all its items saved on form submission
                // this is for savemode = "saveAll" which is the default behavior
                // do not store again value on form refresh (inArray test to check if value already exists)
                if($.inArray("'.$jquerySelectID.'", multiSelectSaveAllIDs) == -1){
                    multiSelectSaveAllIDs[multiSelectSaveAllIDs.length] = "'.$jquerySelectID.'";
                }

                </script>';
        }


        if(!$allowCrossEditing) {
            $html[] = '
            <script type="text/javascript">
                //grey in / grey out editing icons according to the editing status of the option
                $("' . $jquerySelectID . '").change(function(){
    
                    editLink = $(this).find(":selected").attr("data-edit");
    
                    if(typeof editLink != "undefined"){
                        $("' . $jquerySelectID . '_editIcon").attr("style", "opacity: 1;");
                        $("' . $jquerySelectID . '_deleteIcon").attr("style", "opacity: 1;");
                    }
                    else{
                        $("' . $jquerySelectID . '_editIcon").attr("style", "opacity: .4;");
                        $("' . $jquerySelectID . '_deleteIcon").attr("style", "opacity: .4;");
                    }
                });
            </script>';
        }

        if(isset($f->fieldData['icons-wrap']) && $f->fieldData['icons-wrap']==1 || !isset($f->fieldData['icons-wrap'])){
            $wrapBefore='<span class="input-group-addon">';
            $wrapAfter='</span>';
        }
        else{
            $wrapBefore='';
            $wrapAfter='';
        }

        if(isset($f->fieldData['selectActions'])){



            if($viewEnabled){
                $iconView = (isset($f->fieldData['icon-view']))?$f->fieldData['icon-view']:'<b class="glyphicon glyphicon-eye-open"></b>';
                $html[] = $wrapBefore.'<a onclick="viewSelectedRecord_'.$f->key.'()" title="'.$l10n->getLabel('backend','view_entry').'">'.$iconView.'</a>'.$wrapAfter;

               /* $callbackFunc = "";
                if(isset($f->fieldData['actionsJsCallBacks']['view'])){
                    $callbackFunc = $f->fieldData['actionsJsCallBacks']['view'];
                }*/

                $html[] = '<script type="text/javascript">
                            function viewSelectedRecord_'.$f->key.'(){
                                viewLink = $("'.$jquerySelectID.'").find(":selected").attr("data-view");
                                viewedIndex = $("'.$jquerySelectID.'").find(":selected").attr("value");
                                
                                console.log("viewing option "+viewedIndex);
                                
                                if($("'.$jquerySelectID.'").val()>0){
                                    
                                    openRecord(viewLink,"70%","","'.$l10n->getLabel($f->fieldData['editClass'],'record_name').' ('.$f->record->data['uid']['value'].')"); 
                                }
                                else alert("'.$l10n->getLabel('backend','one_entry_selected').'");
                            }
                           </script>';
            }


            if($isEditable && $createEnabled){
                $iconCreate = (isset($f->fieldData['icon-create']))?$f->fieldData['icon-create']:'<b class="glyphicon glyphicon-plus"></b>';
                if($f->fieldData['selectActions']['create']==1){
                    $actionLink = makeHTMLActionLink('createRecord', $f->fieldData['editClass'],'', false, $f->recordClassName, $f->recordUid).'&formID='.$f->formID.'&key='.$f->key;
                }
                else {
                    $actionLink = $f->fieldData['selectActions']['create'].'&formID='.$f->formID.'&key='.$f->key;
                }

                $html[] = $wrapBefore.'<a onclick="createRecord_'.$f->key.'();" title="'.$l10n->getLabel('backend','create_entry').'">'.$iconCreate.'</a>'.$wrapAfter;


                $html[] = '<script type="text/javascript">
                            
                            function createRecord_'.$f->key.'(){
                                editedIndex = -1; //reset editedIndex state to none
                            
                                //check if the record has an uid, if not ask to save it, then create the linked record
                                checkCanEdit("'.$f->formID.'",function() {
                                    var parentUid = $("#'.$f->formID.' #uid").val();
                                    
                                    if(parentUid !== undefined) parentSuffix = "&parent_uid="+parentUid;
                                    else parentSuffix = "";
                                    
                                    var createLink_'.$f->key.' = "'.$actionLink.'"+parentSuffix;
                                       console.log("editedIndex="+editedIndex);                                 
                                    openRecord(createLink_'.$f->key.',"68%","updateOption_'.$f->key.'(\"create\",editedIndex,\"'.$f->fieldData['editClass'].'\")", "'.$l10n->getLabel($f->fieldData['editClass'],'record_name').' ('.$f->record->data['uid']['value'].')");
                                    $("#ignoreMandatoryFields").val("0");
                                },"'.$f->key.'");
                            }
                            </script>';
            }





            $iconEdit = (isset($f->fieldData['icon-edit']))?$f->fieldData['icon-edit']:'<b class="glyphicon glyphicon-pencil"></b>';
            if($isEditable && $editEnabled){


                $html[] = $wrapBefore.'<a id="'.$selectID.'_editIcon" onclick="editSelectedRecord_'.$f->key.'()" title="'.$l10n->getLabel('backend','edit_entry').'">'.$iconEdit.'</a>'.$wrapAfter;
                $html[] = '<script type="text/javascript">
                            var editedIndex = -1;

                            function editSelectedRecord_'.$f->key.'(){

                                //check if the record has an uid, if not ask to save it, then create the linked record
                                checkCanEdit("'.$f->formID.'",function() {
                                    editLink_'.$f->key.' = $("'.$jquerySelectID.'").find(":selected").attr("data-edit");
                                    editedIndex = $("'.$jquerySelectID.'").find(":selected").attr("value");

                                    console.log("editing option "+editedIndex);

                                    if($("'.$jquerySelectID.'").val()>0){
                                    
                                        if(typeof editLink_'.$f->key.' != "undefined"){
                                           //open the edit record dialog
                                           //callback updateOption_xyz() function when closing the record editing window
                                            openRecord(editLink_'.$f->key.',"68%", "updateOption_'.$f->key.'(\"edit\",editedIndex,\"'.$f->fieldData['editClass'].'\")","'.$l10n->getLabel($f->fieldData['editClass'],'record_name').' ('.$f->record->data['uid']['value'].')");
                                        }
                                        else alert("'.$l10n->getLabel('backend','record_edit_no_access').'");
                                    }
                                    else alert("'.$l10n->getLabel('backend','one_entry_selected').'");
                                },"'.$f->key.'")
                            }
                            
                            </script>';

            }


            if($isEditable && ($editEnabled || $createEnabled || $addEnabled )){
                $secKeys = array(
                    'action' => "getOption",
                    'record_uid' => $f->record->data['uid']['value'],
                    'record_key' => $f->key,
                    'record_class' => $f->recordClassName,
                    'option_class' => $f->fieldData['editClass'],
                    'mode' => $mf->mode,
                    'mfSetLocale' => $mfSetLocale,
                );
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                //build secured JSON function arguments to give to the JS client in case it requires a detailed access to detailed data of the recordSelect options defined
                $secKeysListAction = array(
                    'action' => "listRecordSelectItemsData",
                    'record_uid' => $f->record->data['uid']['value'],
                    'record_key' => $f->key,
                    'record_class' => $f->recordClassName,
                    'options_class' => $f->fieldData['editClass'],
                    'mode' => $mf->mode,
                    'mfSetLocale' => $mfSetLocale,
                );
                $secHashListAction = formsManager::makeSecValue($secKeysListAction);
                $secFieldsListAction = implode(',',array_keys($secKeysListAction));
                $secKeysListAction['sec'] = $secHashListAction;
                $secKeysListAction['fields'] = $secFieldsListAction;

                $html[] = '<script type="text/javascript">
                            //when an option has been created or edited, updates its content into the select box.
                            function updateOption_'.$f->key.'(actionName,uid,recordClass){
                            
                                

console.log(\'updateOption_'.$f->key.'(\'+actionName+\',uid=\'+uid+\',\'+recordClass+\')\');

                                    if(uid!="" && uid !="-1"){                                        

                                        var jqxhr = $.post("' . SUB_DIR . '/mf/core/ajax-core-json.php", { action:"getOption", record_uid: "' . $f->record->data['uid']['value'] . '", record_class: "' . $f->recordClassName . '", record_key: "' . $f->key . '", option_uid: uid, option_class: "' . $f->fieldData['editClass'] . '", form_id: "' . $f->formID . '", mfSetLocale: "'.$mfSetLocale.'" ,sec: "' . $secHash . '",fields: "' . $secFields . '",mode: "' . $mf->mode . '"})
                                        .success(function(jsonData) {
                                        
                                            
                                            console.log("adding option "+jsonData.text+" retuned uid="+jsonData.uid+" call uid="+uid);
        
                                            var option = $("<option></option>").attr("value", uid).text(jsonData.text).attr("data-edit", jsonData.dataEdit).attr("data-delete", jsonData.dataDelete).attr("data-view", jsonData.dataView);
                                            selected = $("' . $jquerySelectID . ' option[value="+uid+"]");
        
                                            if(selected != undefined && selected.attr("value") == uid){
                                                //update existing option in the select box
                                                selected.replaceWith(option);
                                            }
                                            else{
                                                //create new option in the select box
                                                $("' . $jquerySelectID . '").append(option);
                                                $("' . $jquerySelectID . '").val(uid).trigger("change");
                                            }
                                            //we need to save this keys value in the parent record first so the options list is accurate if the client calls listRecordSelectItemsData
                                            parentFormID = "' . $f->formID . '"; //$("#record\\\\|' . $f->key . '").closest("form").attr("id");
                                            console.log("recordSelect parentFormID="+parentFormID);
                                            
                                            saveForm(parentFormID, "save", true, function() {
                                                    ' . ((isset($f->fieldData['onRefresh'])) ? 'eval(\'' . $f->fieldData['onRefresh'] . '(uid,recordClass,actionName,' . json_encode($secKeysListAction) . ')' . '\');' : '') . '
                                            }, "' . $f->key . '",0);
                                        })
                                        .fail( function(xhr, textStatus, errorThrown) {
                                            alert("recordSelectField.php updateOption_'.$f->key.'() : " + xhr.responseText);
                                        })
                                        
                                    }
                                
                                
                            }
                            
                            

                           </script>';
            }







            //record deletion

            if($isEditable && $deleteEnabled){
                $iconDelete = (isset($f->fieldData['icon-delete']))?$f->fieldData['icon-delete']:'<b class="glyphicon glyphicon-trash"></b>';

                $html[] = $wrapBefore.'<a  id="'.$selectID.'_deleteIcon" onclick="deleteSelectedRecords_'.$f->key.'()" title="'.$l10n->getLabel('backend','delete_entry').'">'.$iconDelete.'</a>'.$wrapAfter; //.'<div class="deleteStatus"><span id="deleteStatus_'.$f->key.'"></span></div>'

                //build delete request secure arguments
                $secKeys = array(
                    'action' => "deleteRecords",
                    'record_uid' => $f->record->data['uid']['value'],
                    'record_key' => $f->key,
                    'record_class' => $f->recordClassName,
                    'option_class' => $f->fieldData['editClass'],
                    'mode' => $mf->mode,
                    'mfSetLocale' => $mfSetLocale,
                );
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                //build secured JSON function arguments to give to the JS client in case it requires a detailed access to detailed data of the recordSelect options defined
                $secKeysListAction = array(
                    'action' => "listRecordSelectItemsData",
                    'record_uid' => $f->record->data['uid']['value'],
                    'record_key' => $f->key,
                    'record_class' => $f->recordClassName,
                    'options_class' => $f->fieldData['editClass'],
                    'form_id' => $f->formID,
                    'mode' => $mf->mode,
                    'mfSetLocale' => $mfSetLocale,
                );
                $secHashListAction = formsManager::makeSecValue($secKeysListAction);
                $secFieldsListAction = implode(',',array_keys($secKeysListAction));
                $secKeysListAction['sec'] = $secHashListAction;
                $secKeysListAction['fields'] = $secFieldsListAction;

                $html[] = '<script type="text/javascript">


                        function deleteSelectedRecords_'.$f->key.'(){
                        
                            console.log("deleteSelectedRecords_'.$f->key.'()");
                        
                            editLink_'.$f->key.' = $("'.$jquerySelectID.'").find(":selected").attr("data-edit");
                            
                            console.log("delete test="+(typeof editLink_'.$f->key.' != "undefined"));
                        
                            if(typeof editLink_'.$f->key.' != "undefined"){
                               
                                selectedUids = "";
                                first=true;
                                counter = 0;
                                $("'.$jquerySelectID.' option:selected").each(function() {
                                    if(first){
                                        selectedUids = this.value;
                                        first=false;
                                    }
                                    else {
                                        selectedUids = selectedUids + "," + this.value;
                                    }
                                    counter++;
                                });
    
                                if(counter > 0){
                                    if(confirm("'.$l10n->getLabel('backend','areyousure_delete2').'")){
    
                                        var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action: "deleteRecords", record_key: "'.$f->key.'", record_class: "'.$f->recordClassName.'", record_uid: "'.$f->record->data['uid']['value'].'", option_class: "'.$f->fieldData['editClass'].'", option_uids: selectedUids, form_id:"'.$f->formID.'", mfSetLocale: "'.$mfSetLocale.'" , sec:"'.$secHash.'", fields:"'.$secFields.'",mode: "'.$mf->mode.'"})
                                        .success(function(jsonData) {
                                            //cut records from display
                                            $("'.$jquerySelectID.'").find(":selected").remove();
                                            
                                            '.((isset($f->fieldData['onRefresh']))?'eval(\''.$f->fieldData['onRefresh'].'('.$f->record->data['uid']['value'].',"'.$f->recordClassName.'","delete",'.json_encode($secKeysListAction).')'.'\');':'').'
    
                                            //display count of records deleted
                                            $("#fieldStatus_'.$f->key.'").html(counter+" '.$l10n->getLabel('backend','elements_deleted').'").fadeIn( 100 ).delay( 3000 ).fadeOut( 3000 );
    
                                        })
                                    }
                                }
                                else alert("'.$l10n->getLabel('backend','no_entry_selected').'");
                            
                            }
                            else alert("'.$l10n->getLabel('backend','record_edit_no_access').'");
                            
                            
                        }
                        </script>

                        ';
            }



            // AJAX request for searching in the record table some items to be added to the recordSelectField
            if($isEditable && $searchEnabled){
                $iconSearch = (isset($f->fieldData['icon-search']))?$f->fieldData['icon-search']:'<b class="glyphicon glyphicon-search"></b>';
                if($f->fieldData['selectActions']['search']==1)$actionLink = makeHTMLActionLink('searchRecord', $f->fieldData['editClass'],'', false, $f->recordClassName, $f->recordUid).'&formID='.$f->formID.'&key='.$f->key;
                else $actionLink = $f->fieldData['selectActions']['search'].'&formID='.$f->formID.'&key='.$f->key;
                $html[] = $wrapBefore.'<a id="'.$f->key.'_searchIcon" title="'.$l10n->getLabel('backend','search_entry').'">'.$iconSearch.'</a><input id="'.$f->key.'_searchBox" type="text" onValidate="searchRecord_'.$f->key.'()">'.$wrapAfter;

                //look for the tablewe are working on
                $tableName = ((isset($f->fieldData['addItemsSQL']))? $f->fieldData['addItemsSQL']['FROM'] : ((isset($f->fieldData['initItemsSQL']))? $f->fieldData['initItemsSQL']['FROM'] : '' ));

                //form security variables
                $secKeys = array(
                    'action' => 'recordSearch',
                    'search_table_name' => $tableName,
                    'search_class_name' => ((isset($f->fieldData['editClass']))?''.$f->fieldData['editClass']:''),
                    'search_fulltext_fields' => ((isset($f->fieldData['searchFulltextFields']))?''.$f->fieldData['searchFulltextFields']:''),
                    'search_varchar_fields' => ((isset($f->fieldData['searchVarcharFields']))?''.$f->fieldData['searchVarcharFields']:''),
                    'search_equal_fields' => ((isset($f->fieldData['searchEqualFields']))?''.$f->fieldData['searchEqualFields']:''),
                    'search_title_fields' => ((isset($f->fieldData['searchTitleFields']))?''.$f->fieldData['searchTitleFields']:''),
                    'search_orderby_fields' => ((isset($f->fieldData['searchOrderByFields']))?''.$f->fieldData['searchOrderByFields']:''),
                    'calling_field_name' => $f->key,
                    'calling_record_class' => $f->recordClassName,
                    'mfSetLocale' => $mfSetLocale,
                    'form_ID' => $f->formID,

                );
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                $html[] = '<script type="text/javascript">

                            var searchIcon_'.$f->key.' = $("#'.$f->key.'_searchIcon");
                            var searchBox_'.$f->key.' = $("#'.$f->key.'_searchBox");
                            var searchTargetField_'.$f->key.' = $("'.$jquerySelectID.'");

                            $(document).ready(function(){

                                searchBox_'.$f->key.'.typeahead({
                                        source: function(query, process) {
                                            //see http://tatiyants.com/how-to-use-json-objects-with-twitter-bootstrap-typeahead/
                                            formID = "'.$f->formID.'";
                                
                                            if(query.length > 2){
                                                listedItems = new Array();
                                                titlesMap = {};
                                                viewLinkMap = {};
                                                editLinkMap = {};
                                                deleteLinkMap = {};

                                                searchValue = searchBox_'.$f->key.'.val();


                                                var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { 
                                                    action: "recordSearch", 
                                                    value: searchValue, 
                                                    form_ID:formID, 
                                                    search_table_name:"'.$tableName.'",
                                                    search_class_name:"'.((isset($f->fieldData['editClass']))?''.$f->fieldData['editClass']:'').'",
                                                    search_fulltext_fields:"'.((isset($f->fieldData['searchFulltextFields']))?''.$f->fieldData['searchFulltextFields']:'').'",
                                                    search_varchar_fields:"'.((isset($f->fieldData['searchVarcharFields']))?''.$f->fieldData['searchVarcharFields']:'').'", 
                                                    search_equal_fields:"'.((isset($f->fieldData['searchEqualFields']))?''.$f->fieldData['searchEqualFields']:'').'", 
                                                    search_title_fields:"'.((isset($f->fieldData['searchTitleFields']))?''.$f->fieldData['searchTitleFields']:'').'", 
                                                    search_where:"'.((isset($f->fieldData['addItemsSQL']['WHERE']))?''.$f->fieldData['addItemsSQL']['WHERE']:((isset($f->fieldData['initItemsSQL']['WHERE']))?''.$f->fieldData['initItemsSQL']['WHERE']:'')).'", 
                                                    search_orderby_fields:"'.((isset($f->fieldData['searchOrderByFields']))?''.$f->fieldData['searchOrderByFields']:'').'", 
                                                    search_debug:"'.((isset($f->fieldData['searchDebug']))?''.$f->fieldData['searchDebug']:'false').'", 
                                                    calling_field_name:"'.$f->key.'",
                                                    calling_record_class:"'.$f->recordClassName.'",
                                                    mfSetLocale: "'.$mfSetLocale.'", 
                                                    sec:"'.$secHash.'", 
                                                    fields:"'.$secFields.'" })
                                                .success(function(jsonData) {

                                                    if(jsonData.result=="success"){
                                                        console.log(jsonData.items);
                                                        
                                                        //reset entries at each feedback in order to avoid duplicates
                                                        listedItems = new Array();
                                                        titlesMap = {};
                                                        viewLinkMap = {};
                                                        editLinkMap = {};
                                                        deleteLinkMap = {};
    
                                                        $.each(jsonData.items, function (i, retreivedItems) {
                                                            console.log(i+","+retreivedItems.title);
                                                            titlesMap[retreivedItems.title] = i;
                                                            viewLinkMap[i] = retreivedItems.viewLink;
                                                            editLinkMap[i] = retreivedItems.editLink;
                                                            deleteLinkMap[i] = retreivedItems.deleteLink;
                                                            listedItems.push(retreivedItems.title);
                                                        });
                                                        process(listedItems.reverse());
                                                    }
                                                    else if(jsonData.result=="error"){
                                                        alert(jsonData.message);
                                                    }

                                                })
                                                .fail( function(xhr, textStatus, errorThrown) {
                                                    alert("recordSelectField.php searchBox_'.$f->key.'.typeahead() : " + xhr.responseText);
                                                })
                                                
                                            }

                                        },
                                        matcher: function (item) {
                                            if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                                                return true;
                                            }
                                        },
                                        highlighter: function (item) {
                                            var regex = new RegExp( "(" + this.query + ")", "gi" );
                                            return item.replace( regex, "<strong>$1</strong>" );
                                        },
                                        updater: function (item) {
                                            foundRecordUid = titlesMap[item];
                                            //console.log("selected="+foundRecordUid+","+item);
                                            optionsAsString = "<option value=\'" + foundRecordUid + "\' data-view=\'" + viewLinkMap[foundRecordUid] + "\' data-edit=\'" + editLinkMap[foundRecordUid] + "\' data-delete=\'" + deleteLinkMap[foundRecordUid] + "\'>" + item + "</option>";

                                           '.(($f->fieldData['editType']=="multiple")?'searchTargetField_'.$f->key.'.find("option").end().append($(optionsAsString));':'searchTargetField_'.$f->key.'.find("option").remove().end().append($(optionsAsString));').'
                                            searchTargetField_'.$f->key.'.val(foundRecordUid).trigger("change");
                                            return "";
                                        },
                                        items: "all",
                                });
                            });



                            $(function() {
                                //open searchbox on click
                                searchIcon_'.$f->key.'.click(function() {
                                    if (searchBox_'.$f->key.'.is(":hidden")) {
                                        
                                        searchBox_'.$f->key.'.animate({
                                            "width": searchBox_'.$f->key.'.width() == 150 ? "0px" : "150px"
                                            }, "fast", function() {
                                                if (searchBox_'.$f->key.'.width() == 0) {
                                                    searchBox_'.$f->key.'.hide();
                                                }
                                        });
                                        searchBox_'.$f->key.'.show().focus();
                                    }

                                });
                                
                                //close searchbox on focus lost
                                searchBox_'.$f->key.'.blur(function() {
                                    searchBox_'.$f->key.'.animate({
                                            "width": "0px"
                                            }, "fast", function() {
                                                searchBox_'.$f->key.'.hide();
                                        });

                                });
                            });

                            var searchLink_'.$f->key.' = "'.$actionLink.'";
                            function searchRecord_'.$f->key.'(){
                                 alert("searchRecord_'.$f->key.'()");
                            }
                            </script>
                            <style>
                            #'.$f->key.'_searchBox {
                                display:none;
                                width: 0px;
                                margin-left:5px;
                            }
                            </style>
                            ';
            }


            if($f->fieldData['editType']=="multiple") $html[] = '<br /><div style="width:80px;">';

            if($isEditable && isset($f->fieldData['selectActions']['add'])  && $f->fieldData['selectActions']['add'] != '0'){
                $iconAdd = (isset($f->fieldData['icon-add']))?$f->fieldData['icon-add']:'<b class="glyphicon glyphicon-plus-sign"></b>';

                if($f->fieldData['selectActions']['add']==1)$actionLink = makeHTMLActionLink('select_add_records_popup', $f->recordClassName,$f->record->data['uid']['value'], true).'&formID='.$f->formID.'&key='.$f->key;
                else $actionLink = $f->fieldData['selectActions']['add'].'&formID='.$f->formID.'&key='.$f->key;
                //$html[] = $wrapBefore.'<a onclick="window.open(\''.$actionLink.'\', \'addActionWindow_'.$f->key.'\',\'height=640,width=640,location=no,menubar=no,resizable=yes,status=no,titlebar=no,toolbar=no,scrollbars=yes\')";" title="'.$l10n->getLabel('backend','add_entry').'">'.$iconAdd.'</a>'.$wrapAfter;
                $html[] = $wrapBefore.'<a onclick="addRecord_'.$f->key.'();" title="'.$l10n->getLabel('backend','add_entry').'">'.$iconAdd.'</a>'.$wrapAfter;

                $html[] = '<script type="text/javascript">
                            
                            function addRecord_'.$f->key.'(){
                                editedIndex = -1; //reset editedIndex state to none
                                //check if the record has an uid, if not ask to save it, then create the linked record
                                checkCanEdit("'.$f->formID.'",function() {
                                    var parentUid = $("'.$jquerySelectID.'").closest("form").find("#uid").val();
                                    
                                    if(parentUid !== undefined) parentSuffix = "&parent_uid="+parentUid;
                                    else parentSuffix = "";
                                                                        
                                    var addLink_'.$f->key.' = "'.$actionLink.'"+parentSuffix;
                                    window.open(addLink_'.$f->key.', \'addActionWindow_'.$f->key.'\',\'height=640,width=640,location=no,menubar=no,resizable=yes,status=no,titlebar=no,toolbar=no,scrollbars=yes\');
                                    $("#ignoreMandatoryFields").val("0");
                                },"'.$f->key.'")
                            }
                            </script>';
            }

            //record removal
            if($isEditable && isset($f->fieldData['selectActions']['remove']) && $f->fieldData['selectActions']['remove']==1){
                $iconRemove = (isset($f->fieldData['icon-remove']))?$f->fieldData['icon-remove']:'<b class="glyphicon glyphicon-minus-sign"></b>';
                $html[] = $wrapBefore.'<a onclick="removeRecord_'.$f->key.'();" title="'.$l10n->getLabel('backend','remove_entry').'">'.$iconRemove.'</a>'.$wrapAfter;

                $html[] = '<script type="text/javascript">
                            function removeRecord_'.$f->key.'(){
                                selected = $("'.$jquerySelectID.'").find(":selected").val();
                                $("'.$jquerySelectID.' option[value=\'"+selected+"\']").remove();
                            }
                           </script>';
            }


            if($isEditable && $clipboardEnabled){
                $iconCopy = (isset($f->fieldData['icon-copy']))?$f->fieldData['icon-copy']:'<b class="glyphicon glyphicon-copy"></b>';
                $iconPaste = (isset($f->fieldData['icon-paste']))?$f->fieldData['icon-copy']:'<b class="glyphicon glyphicon-paste"></b>';

                $html[] = $wrapBefore.'<a onclick="copyRecords_'.$f->key.'();" title="'.$l10n->getLabel('backend','copy_entry').'">'.$iconCopy.'</a>'.$wrapAfter;
                $html[] = $wrapBefore.'<a onclick="pasteRecords_'.$f->key.'();" title="'.$l10n->getLabel('backend','paste_entry').'">'.$iconPaste.'</a>'.$wrapAfter;

                $secKeys = array(
                    'action' => "copyRecordsFromSelect",
                    'option_class' => $f->fieldData['editClass'],
                    'mode' => $mf->mode,
                    'mfSetLocale' => $mfSetLocale,
                );

	            $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                $html[] = '<script type="text/javascript">
                            
                            function copyRecords_'.$f->key.'(){

                                selectedUids = "";
                                first=true;
                                counter = 0;
                                $("'.$jquerySelectID.' option:selected").each(function() {
                                    if(first){
                                        selectedUids = this.value;
                                        first=false;
                                    }
                                    else {
                                        selectedUids = selectedUids + "," + this.value;
                                    }
                                    counter++;
                                });


                                $("#fieldStatus_'.$f->key.'").html(counter+" '.$l10n->getLabel('backend','elements_copied').'").fadeIn( 100 ).delay( 3000 ).fadeOut( 3000 );

                                var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action: "copyRecordsFromSelect", option_class: "'.$f->fieldData['editClass'].'", option_uids: selectedUids, form_id:"'.$f->formID.'", mfSetLocale: "'.$mfSetLocale.'", sec:"'.$secHash.'", fields:"'.$secFields.'",mode: "'.$mf->mode.'"}) 
                                .success(function(jsonData) {
                                    //display count of records copied
                                    $("#fieldStatus_'.$f->key.'").html(counter+" '.$l10n->getLabel('backend','elements_copied').'").fadeIn( 100 ).delay( 3000 ).fadeOut( 3000 );
                                })

                            }';

                $secKeys = array(
                    'action' => "pasteRecordsToSelect",
                    'record_uid' => $f->record->data['uid']['value'],
                    'record_key' => $f->key,
                    'record_class' => $f->recordClassName,
                    'options_class' => $f->fieldData['editClass'],
                    'form_id' => $f->formID,
                    'mode' => $mf->mode,
                    'mfSetLocale' => $mfSetLocale,
                );
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                //build secured JSON function arguments to give to the JS client in case it requires a detailed access to detailed data of the recordSelect options defined
                $secKeysListAction = array(
                    'action' => "listRecordSelectItemsData",
                    'record_uid' => $f->record->data['uid']['value'],
                    'record_key' => $f->key,
                    'record_class' => $f->recordClassName,
                    'options_class' => $f->fieldData['editClass'],
                    'form_id' => $f->formID,
                    'mode' => $mf->mode,
                    'mfSetLocale' => $mfSetLocale,
                );
                $secHashListAction = formsManager::makeSecValue($secKeysListAction);
                $secFieldsListAction = implode(',',array_keys($secKeysListAction));
                $secKeysListAction['sec'] = $secHashListAction;
                $secKeysListAction['fields'] = $secFieldsListAction;

                $html[] = '
                            function pasteRecords_'.$f->key.'(){
                            
                                var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action: "pasteRecordsToSelect", record_key: "'.$f->key.'", record_class: "'.$f->recordClassName.'", record_uid: "'.$f->record->data['uid']['value'].'", options_class: "'.$f->fieldData['editClass'].'", form_id:"'.$f->formID.'", mfSetLocale: "'.$mfSetLocale.'", sec:"'.$secHash.'", fields:"'.$secFields.'",mode: "'.$mf->mode.'"})
                                .success(function(jsonData) {
                                    if(jsonData.result=="success"){

                                        pastedUids = jsonData.pasted_uids;
                                        pastedTexts = jsonData.pasted_texts;
                                        dataEdit = jsonData.data_edit;
                                        dataDelete = jsonData.data_delete;
                                        dataView = jsonData.data_view;
                                        counter=0;

                                        //build and add the new options to the recordSelect
                                        for (var i = 0; i < pastedUids.length; i++) {
                                            var option = $("<option></option>").attr("value", pastedUids[i]).text(pastedTexts[i]).attr("data-edit", dataEdit[i]).attr("data-view", dataView[i]).attr("data-delete", dataDelete[i]).attr("selected","selected");

                                            //create new option in the select box
                                            $("'.$jquerySelectID.'").append(option);
                                            counter++;
                                        }
                                        
                                        //notify record select JS callback function if registered in "onRefresh" key of the recordSelect definition
                                        '.((isset($f->fieldData['onRefresh']))?'eval(\''.$f->fieldData['onRefresh'].'(pastedUids,"'.$f->fieldData['editClass'].'","paste",'.json_encode($secKeysListAction).')\');':'').'

                                        //display count of records pasted
                                        $("#fieldStatus_'.$f->key.'").html(counter+" '.$l10n->getLabel('backend','elements_pasted').'").fadeIn( 100 ).delay( 3000 ).fadeOut( 3000 );
                                    }
                                    else{
                                        alert(jsonData.message);
                                    }

                                })
                            }
                            </script>';
            }

            if($f->fieldData['editType']=="multiple") $html[] = '</div>';

            //record sorting
            if($isEditable && ($f->fieldData['editType'] == 'multiple') && $sortEnabled){
                $iconUp = (isset($f->fieldData['icon-up']))?$f->fieldData['icon-up']:'<b class="glyphicon glyphicon-circle-arrow-up"></b>';
                $iconDown = (isset($f->fieldData['icon-down']))?$f->fieldData['icon-down']:'<b class="glyphicon glyphicon-circle-arrow-down"></b>';

                if($f->fieldData['editType']=="multiple") $html[] = '<br /><div style="width:40px">';
                $html[] = $wrapBefore.'<a onclick="moveUp_'.$f->key.'();" title="'.$l10n->getLabel('backend','entry_up').'">'.$iconUp.'</a>'.$wrapAfter;
                $html[] = '<script type="text/javascript">
                            function moveUp_'.$f->key.'(){
                                count = $("'.$jquerySelectID.' option").size();
                                if(count > 1){
                                    selected = $("'.$jquerySelectID.'").find(":selected");
                                    selectedText = selected.text();
                                    selectedVal = selected.val();
                                    selectedDataEdit = selected.attr("data-edit");
                                    selectedDataView = selected.attr("data-view");
                                    selectedDataDelete = selected.attr("data-delete");

                                    var option = $("<option></option>").attr("value", selectedVal).text(selectedText).attr("data-edit", selectedDataEdit).attr("data-view", selectedDataView).attr("data-delete", selectedDataDelete);

                                    selected.prev().before(option);
                                    selected.remove();

                                    $("#record\\\\|'.$f->key.'").val(selectedVal);
                                    
                                    '.((isset($f->fieldData['onRefresh']))?'eval(\''.$f->fieldData['onRefresh'].'(selectedVal,"'.$f->fieldData['editClass'].'","sortUp","")'.'\');':'').'
                                
                                }
                            }

                            function moveDown_'.$f->key.'(){
                                count = $("'.$jquerySelectID.' option").size();
                                if(count > 1){
                                    selected = $("'.$jquerySelectID.'").find(":selected");
                                    selectedText = selected.text();
                                    selectedVal = selected.val();
                                    selectedData = selected.attr("data-edit");
                                    selectedDataEdit = selected.attr("data-edit");
                                    selectedDataView = selected.attr("data-view");
                                    selectedDataDelete = selected.attr("data-delete");

                                    var option = $("<option></option>").attr("value", selectedVal).text(selectedText).attr("data-edit", selectedDataEdit).attr("data-view", selectedDataView).attr("data-delete", selectedDataDelete);

                                    selected.next().after(option);
                                    selected.remove();

                                    $("#record\\\\|'.$f->key.'").val(selectedVal);
                                    
                                    '.((isset($f->fieldData['onRefresh']))?'eval(\''.$f->fieldData['onRefresh'].'(selectedVal,"'.$f->fieldData['editClass'].'","sortDown","")'.'\');':'').'
                                
                                }
                            }
                      </script>';
                if($f->fieldData['editType']=="multiple") $html[] = '</div><div style="width:40px">';
                $html[] = $wrapBefore.'<a onclick="moveDown_'.$f->key.'();" title="'.$l10n->getLabel('backend','entry_down').'">'.$iconDown.'</a>'.$wrapAfter;
                if($f->fieldData['editType']=="multiple") $html[] = '</div>';
            }

            $html[] = '<div class="fieldStatus"><span id="fieldStatus_'.$f->key.'"></span></div>';

            $html[] = '</td></tr></table>';



            //error message display
            $html[] = ($showFieldDiv)?'</div>':'';
            $html[] = $postDivHTML;
            $html[] = '<span class="errorMsg col-lg-offset-4 col-lg-8"></span>';

        }
        if(isset($f->fieldData['noClearFix']) and $f->fieldData['noClearFix'] == '1');
        else $html[] = "<div class='clearfix'></div>";



        if(isset($f->fieldData['addItemsSQL']) && ($f->fieldData['editType'] == 'multiple')) {
            $formsManager->addPriorSendFormJs('recordSelectField', '
                        
                //save selections, then select all options so they are all saved
                for ( i = 0 ; i < multiSelectSaveAllIDs.length; i++){
                    
                    //save the component selection
                    multiSelectSaveAllSelections[multiSelectSaveAllIDs[i]] = $(multiSelectSaveAllIDs[i]).val();
                    if(multiSelectSaveAllSelections[multiSelectSaveAllIDs[i]] != null)console.log("saving recordSelect value "+multiSelectSaveAllSelections[multiSelectSaveAllIDs[i]]+" for "+multiSelectSaveAllIDs[i]);
    
                    //then select every option so they are all saved in the field value
                    //selection will then be restored after the form submission
                    $(multiSelectSaveAllIDs[i]+" option").prop("selected",true);
    
                }');

            $formsManager->addPostSendFormJs('recordSelectField', '
            
                // multiSelect with saveAll enabled : restore selections after form refresh
                for ( i = 0 ; i < multiSelectSaveAllIDs.length; i++){
                    $(multiSelectSaveAllIDs[i]+" option:selected").attr("selected",false);
                    // restore the component selection
                    $(multiSelectSaveAllIDs[i]).val(multiSelectSaveAllSelections[multiSelectSaveAllIDs[i]]);
                }
            ');
        }

        // Jquery serialize won't POST the select variable if the field is empty.
        // We need the field to be sent for controlling the min_entries setting of validation if the field is empty
        // This is a fix for jQuery behavior
        $formsManager->addBottomJs('serializeOverride','<script type="text/javascript" >
            $.fn.extend({

                serialize: function() {
                    return jQuery.param( this.serializeArray() );
                },
                serializeArray: function() {
                    console.log("serializeArray");
                    return this.map(function() {
                        // Can add propHook for "elements" to filter or add form elements
                        var elements = jQuery.prop( this, "elements" );
                        return elements ? jQuery.makeArray( elements ) : this;
                    })
                        .filter(function() {
                            var type = this.type;
                            // Use .is(":disabled") so that fieldset[disabled] works
                            return this.name && !jQuery( this ).is( ":disabled" ) &&
                                rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
                                ( this.checked || !rcheckableType.test( type ) );
                        })
                        .map(function( i, elem ) {
                            var val = jQuery( this ).val();
            
                            return val == null ?
                            { name: elem.name, value: "" } :
                                jQuery.isArray( val ) ?
                                    jQuery.map( val, function( val ) {
                                        return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
                                    }) :
                                { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
                        }).get();
                }
            
            });

        </script>');


        return implode(chr(10),$html);
    }









    /**
     * returns an array of uid => title for options displayed in recordSelect
     * @param $f an stdClass containing the required parameters. Must be defined like this :
     *              $f = new stdClass();
                    $f->key = $recordKey;
                    $f->recordClassName = $recordClass;
                    $f->fieldData = $record->data[$recordKey];
                    $f->record = $record;
     * @param bool $returnFullData instead of feeding the return array with the option title, the function will return all the option record data
     * @return array
     */
    static function getSelectItems($f, $returnFullData=false){
        global $mf,$l10n,$pdo,$logger;

        //debug_print_backtrace();
        $fieldData = $f->fieldData;
        $key = $f->key;
        $recordClassName = $f->recordClassName;
        $debugSQL = (isset($f->fieldData['debugSQL']))?$f->fieldData['debugSQL']:0;

        $items=array();

        $alias = '';

        //if 'initItemsSQL' is defined, populate items list to be displayed on load
        if(isset($fieldData['initItemsSQL'])){

            $processSQL=true;
            if(isset($fieldData['initItemsSQL']['SELECT']))$titleFields = $fieldData['initItemsSQL']['SELECT'];
            else{
                $mf->addWarningMessage("You must specify a title field for record ".$recordClassName.'->data[\''.$key.'\'][\'initItemsSQL\']');
                $processSQL = false;
            }
            if(isset($fieldData['initItemsSQL']['FROM'])){
                $fromDef = explode(' ',$fieldData['initItemsSQL']['FROM']);
                $tableName = $fromDef[0];

                if(sizeof($fromDef)==2){
                    $alias = $fromDef[1];
                }
                else $alias = $fromDef[0];
            }
            else{
                $mf->addWarningMessage("You must specify a table name for record ".$recordClassName.'->data[\''.$key.'\'][\'initItemsSQL\']');
                $processSQL = false;
            }
            if(isset($fieldData['initItemsSQL']['WHERE']) && trim($fieldData['initItemsSQL']['WHERE'])!='')$WHERE = " AND ".$fieldData['initItemsSQL']['WHERE'];
            else $WHERE='';

            if(isset($fieldData['initItemsSQL']['JOIN']) && trim($fieldData['initItemsSQL']['JOIN'])!='')$JOIN = " ".$fieldData['initItemsSQL']['JOIN'];
            else $JOIN='';

            if(isset($fieldData['initItemsSQL']['ORDER BY']) && trim($fieldData['initItemsSQL']['ORDER BY'])!='')$ORDER_BY = " ORDER BY ".$fieldData['initItemsSQL']['ORDER BY'];
            else $ORDER_BY ='';

	        if(isset($fieldData['initItemsSQL']['GROUP BY']) && trim($fieldData['initItemsSQL']['GROUP BY'])!='')$GROUP_BY = " GROUP BY ".$fieldData['initItemsSQL']['GROUP BY'];
	        else $GROUP_BY ='';

            if($processSQL){
                //preparing request only once
                $sql = "SELECT ".$alias.".uid,".$alias.".language,".$titleFields." FROM ".$fieldData['initItemsSQL']['FROM'].$JOIN." WHERE ".$alias.".deleted = '0' ".$WHERE.$GROUP_BY.$ORDER_BY;

                if($debugSQL){
                    $mf->addInfoMessage("debugSQL key=".$key." SQL=".$sql);
                    //echo "debugSQL key=".$key." SQL=".$sql;
                }

                $stmt = $pdo->prepare($sql);

                try{
                    $stmt->execute(array());
                }
                catch(PDOException  $e)
                {
                    echo '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>';

                    echo $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
                    echo $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
                    echo "<p>".nl2br(generateCallTrace())."</p>";
                    echo "</body></html>";

                    $logger->error($l10n->getLabel('main','error').$e->getMessage());
                    $logger->error($l10n->getLabel('main','number').$e->getCode());
                    $logger->error(nl2br(generateCallTrace()));

                    die();
                }

                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                //$titles = explode(',',$titleFields);

                foreach($rows as $row){
                    //$fullTitle='';

                    $linkedRecordClassName = $fieldData['editClass'];

                    //we need to use the load() method of the records, otherwise some data may not be properly loaded, such as dates not converted from SQL etc..
                    $record = new $linkedRecordClassName();
                    $record->load($row['uid'],$row['language']);

                    $items[$row['uid']] = $record->data;

                }
            }
        }
        //if 'addItemsSQL' is defined but not 'initItemsSQL', populate items list with only the selected records
        else if(isset($fieldData['addItemsSQL'])){

            $processSQL=true;
            if(isset($fieldData['addItemsSQL']['SELECT']))$titleFields = $fieldData['addItemsSQL']['SELECT'];
            else{
                $mf->addWarningMessage("You must specify a title field for record ".$recordClassName.'->data[\''.$key.'\'][\'addItemsSQL\']');
                $processSQL = false;
            }
            if(isset($fieldData['addItemsSQL']['FROM'])){
                $fromDef = explode(' ',$fieldData['addItemsSQL']['FROM']);
                $tableName = $fromDef[0];

                if(sizeof($fromDef)==2){
                    $alias = $fromDef[1];
                }
                else $alias = $fromDef[0];
            }
            else{
                $mf->addWarningMessage("You must specify a table name for record ".$recordClassName.'->data[\''.$key.'\'][\'addItemsSQL\']');
                $processSQL = false;
            }
            if(isset($fieldData['addItemsSQL']['WHERE']) && trim($fieldData['addItemsSQL']['WHERE'])!='')$WHERE = " AND ".$fieldData['addItemsSQL']['WHERE'];
            else $WHERE='';

            if(isset($fieldData['addItemsSQL']['JOIN']) && trim($fieldData['addItemsSQL']['JOIN'])!='')$JOIN = " ".$fieldData['addItemsSQL']['JOIN'];
            else $JOIN='';

            /*
            if(isset($fieldData['addItemsSQL']['ORDER BY']) && trim($fieldData['addItemsSQL']['ORDER BY'])!='')$ORDER_BY = " ORDER BY ".$fieldData['addItemsSQL']['ORDER BY'];
            else $ORDER_BY ='';

            if(isset($fieldData['addItemsSQL']['ORDER DIRECTION']) && trim($fieldData['addItemsSQL']['ORDER DIRECTION'])!='')$ORDER_DIRECTION = " ".$fieldData['addItemsSQL']['ORDER DIRECTION'];
            else $ORDER_DIRECTION ='';
            */

	        if(isset($fieldData['addItemsSQL']['GROUP BY']) && trim($fieldData['addItemsSQL']['GROUP BY'])!='')$GROUP_BY = " GROUP BY ".$fieldData['addItemsSQL']['GROUP BY'];
	        else $GROUP_BY ='';



            //cleanup rogue records which have previously been created and remain in the database, but were not connected to the main record when it has not been saved
            /*if(trim($fieldData['value'] != '') && isset($fieldData['addItemsSQL']['WHERE']) && trim($fieldData['addItemsSQL']['WHERE']) != ''){
                $sql = "DELETE FROM ".$tableName." WHERE ".$tableName.".uid NOT IN(".$fieldData['value'].") AND parent_uid=".$f->record->data['uid']['value']." AND ".$fieldData['addItemsSQL']['WHERE'];
                $pdo->query($sql);
                if($debugSQL)$mf->addInfoMessage("debugSQL key=".$key." SQL=".$sql);
            }*/

            if($processSQL){
                if($fieldData['value'] != '' && $fieldData['value']!='0'){
                    if(isset($fieldData['addItemsSQL']['ORDER BY']) && trim($fieldData['addItemsSQL']['ORDER BY']) != ''){
                        $IN_CONDITION = " AND ".$alias.".uid IN(".trim($fieldData['value'],',').")";
                        $ORDER_BY = " ORDER BY ".$fieldData['addItemsSQL']['ORDER BY'].((isset($fieldData['addItemsSQL']['ORDER DIRECTION']))?" ".$fieldData['addItemsSQL']['ORDER DIRECTION']:'');
                    }
                    else{
                        $IN_CONDITION = " AND ".$alias.".uid IN(".trim($fieldData['value'],',').")";
                        $ORDER_BY = " ORDER BY FIELD(".$alias.".uid, ".$fieldData['value'].") ASC";
                    }

                    $sql = "SELECT ".$alias.".uid,".$alias.".language,".$titleFields." FROM ".$fieldData['addItemsSQL']['FROM'].$JOIN." WHERE ".$alias.".deleted = '0'".$WHERE.$IN_CONDITION.$GROUP_BY.$ORDER_BY;

                    if($debugSQL){
                        $mf->addInfoMessage("debugSQL key=".$key." SQL=".$sql);
                        //echo "debugSQL key=".$key." SQL=".$sql;
                    }

                    try
                    {

                        $stmt = $pdo->query($sql);
                    }
                    catch(PDOException  $e)
                    {
                        echo '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>';

                        echo $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
                        echo $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
                        echo "<p>".nl2br(generateCallTrace())."</p>";
                        echo "</body></html>";

                        $logger->error($l10n->getLabel('main','error').$e->getMessage());
                        $logger->error($l10n->getLabel('main','number').$e->getCode());
                        $logger->error(nl2br(generateCallTrace()));

                        die();
                    }

                    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);


                    foreach($rows as $row){

                        $linkedRecordClassName = $fieldData['editClass'];
                        //we need to use the load() method of the records, otherwise some data may not be properly loaded, such as dates not converted from SQL etc..
                        $record = new $linkedRecordClassName();

                        $record->load($row['uid'],$row['language']);
                        $items[$row['uid']] = $record->data;

                    }
                }
            }
        }

        return $items;
    }

    /**
     * Converts a list of records to a list of select options, given the specification of the supplied field data array
     * @param $records list of records to be converted to html <option> tags
     * @param stdClass $f fieldData specification of the current recordSelect field
     */
    static function getOptions($records,$f){

    	global $mf;

        $viewEnabled        = isset($f->fieldData['selectActions']['view'])     && $f->fieldData['selectActions']['view'] != '0';
        $editEnabled        = isset($f->fieldData['selectActions']['edit'])     && $f->fieldData['selectActions']['edit'] != '0';
        $deleteEnabled      = isset($f->fieldData['selectActions']['delete'])   && $f->fieldData['selectActions']['delete'] != '0';

        if(!isset($f->fieldData['editable']))$f->fieldData['editable']=true;
	    $isEditable = filterBoolean($f->fieldData['editable']);


        $html = array();

        //and process them for display
        $titleFields = '';
        $fieldData = $f->fieldData;
        $items=array();

        if(isset($fieldData['initItemsSQL'])){
            if(isset($fieldData['initItemsSQL']['SELECT']))$titleFields = $fieldData['initItemsSQL']['SELECT'];

        }
        else if(isset($fieldData['addItemsSQL'])){
            if(isset($fieldData['addItemsSQL']['SELECT']))$titleFields = $fieldData['addItemsSQL']['SELECT'];
        }

        $titles = explode(',',$titleFields);


//print_r($fieldData);

        //extract and process the content of each <option> using optional keyProcessors
        foreach($records as $recordUid => $recordData) {

            $fullTitle = '';
            $columnSeparator = ((isset($fieldData['columnSeparator']))?$fieldData['columnSeparator']:' ');

            foreach ($titles as $title)
            {
                $title = trim(trim($title, '`'));

                if (isset($fieldData['keyProcessors'][$title]))
                {
                    eval('$modTitle = ' . $fieldData['keyProcessors'][$title] . '($recordData[$title][\'value\']);');
                    $fullTitle .= $modTitle . $columnSeparator;
                }
                else $fullTitle .= $recordData[$title]['value'] . $columnSeparator;
            }
            if(isset($fieldData['columnSeparator'])) $fullTitle = rtrim($fullTitle, $columnSeparator);

            $items[$recordUid] = rtrim($fullTitle, ' ');
        }

        //If the records are allowed to be edited by someone else than their creator, $allowCrossEditing = true
        $allowCrossEditing = (isset($f->fieldData['allow_cross_editing']))? $f->fieldData['allow_cross_editing'] : true ;

        //build the <options> tag attributes
        if( is_array($items) && sizeof($items)>0){

            //get the current logged in user
            $xEndUser = $mf->getCurrentUser();

            //prepend empty option (empty choice by default) if requested
            if(isset($fieldData['initItemsSQL']) && isset($fieldData['prependEmptyEntry'])){

                if(filterBoolean($f->fieldData['prependEmptyEntry']) == 1)
                    $html[] = "<option value=''></option>";
            }


            //adding records from the specified SQL, previously gathered with self::getSelectItems()
            foreach($items as $recordUid => $value){

                $recordCreatorUid = $records[$recordUid]['creator_uid']['value'];
                $recordCreatorClass = $records[$recordUid]['creator_class']['value'];

                //allow editing if cross editing is enabled or if the current user is the creator of the record
                $allowItemEditing = $allowCrossEditing || ($recordCreatorClass == get_class($xEndUser) && $recordCreatorUid == $xEndUser->data['uid']['value']);

                if($viewEnabled){
                    if($f->fieldData['selectActions']['view']==1)$viewLink = makeHTMLActionLink('viewRecord', $f->fieldData['editClass'],$recordUid, false, $f->recordClassName, $f->recordUid).'&formID='.$f->formID.'&key='.$f->key;
                    else {
                        $viewLink = $f->fieldData['selectActions']['view'];
                        $viewLink = str_replace('{record_uid}',$recordUid,$viewLink);
                    }
                    $viewLink = ' data-view="'.$viewLink.'"';

                }
                else $viewLink='';

                if($isEditable && $editEnabled && $allowItemEditing){
                    if($f->fieldData['selectActions']['edit']==1)$editLink = makeHTMLActionLink('editRecord', $f->fieldData['editClass'],$recordUid, false, $f->recordClassName, $f->recordUid).'&formID='.$f->formID.'&key='.$f->key;
                    else {
                        $editLink = $f->fieldData['selectActions']['edit'];
                        $editLink = str_replace('{record_uid}',$recordUid,$editLink);
                    }
                    $editLink = ' data-edit="'.$editLink.'"';

                }
                else $editLink='';



                if($isEditable &&  $deleteEnabled && $allowItemEditing){
                    if($f->fieldData['selectActions']['delete']==1)$deleteLink = makeHTMLActionLink('deleteRecord', $f->fieldData['editClass'],$recordUid, false, $f->recordClassName, $f->recordUid);
                    else {
                        $deleteLink = $f->fieldData['selectActions']['delete'];
                        $deleteLink = str_replace('{record_uid}',$recordUid,$deleteLink);
                    }
                    $deleteLink = ' data-delete="'.$deleteLink.'"';

                }
                else $deleteLink='';

                $selection = explode(',',rtrim($f->fieldData['value'],','));

                $html[] = "<option value='".$recordUid."'".((in_array($recordUid,$selection))?" selected=\"selected\"":"").$viewLink.$editLink.$deleteLink.">".$value."</option>";
            }
        }

        return $html;
    }

    /**
     * Process the field value prior saving it to the database, whenever inner value adaptation is required, for SQL strict compliance for example.
     * This is executed after 'preRecord' processing functions.
     * @param $fieldData this is the data array of the current field
     * @param $value this is the submited value
     * @return mixed
     */
    static public function onSave($fieldData, $value){
        //convert '' to 0 for strict SQL compliance
        if(is_array($value)){
            if(count($value)==1 && $value[0]=='') $value=0;
            else return $value;
        }
        else if($value=='')return 0;
        else return $value;
    }

    static public function getCSS(){return "";}
    static public function getTopJS(){return "";}
    static public function getBottomJS(){return "";}







}
