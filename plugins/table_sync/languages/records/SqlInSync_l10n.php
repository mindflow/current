<?php
$l10nText = array(

    'fr' => array(
        'title' => "Title",
        'tab_main' => "Main",
        'sql_request' => "Requête SQL",
        'sql_result' => "Résultat SQL",
        'in_rule' => "Règle appliquée à la réception",
        'source' => "Source",
        'state' => "Etat",
        'result' => "Résultat",
        'local_log' => "Log local",
        'remote_log' => "Log distant",
        'editor_class' => "Classe éditeur",
        'editor_username' => "Nom d'utilisateur éditeur",
        'editor_uid' => "Uid éditeur",
        'Receiving' => "Réception",
        'Inactive' => "Inactive",
        'Success' => "Succès",
        'Failure' => "Echec",
        'replay' => "Rejouer la réception",


        /*mandatory records*/
        'record_name' => "Synchro entrante",
        'new_record' => "Add Synchro entrante",
    ),

);

?>