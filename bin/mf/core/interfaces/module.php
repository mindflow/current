<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Defines the base functions required from a plugin module. A plugin module is a panel for editing plugin data in the backend, or for managing and displaying data in the frontend.
 */
interface module
{

    /**
     * Launches the data processing step of all modules, executing their own prepareData() function
     * @return mixed
     */
    function prepareData();

    /**
     * Launches the rendering step of all modules, executing their own render() function
     * @param $mainTemplate
     * @return mixed
     */
    function render(&$mainTemplate);

    /**
     * must return one or several plugin types separated by comas
     * current possible values are "backend, frontend, authentification"
     * @return string coma separated list of current module execution context types
     */
    function getType();

}