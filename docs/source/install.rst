.. |vspace| raw:: latex

   \vspace{5mm}
   
.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>
	
|embed_js|

.. _install:

Installation de MindFlow
************************

**Copie des fichiers :**

Transférez les dossiers et fichiers suivants sous la racine de votre hébergement web :

.. image:: images/dossiers-mindflow.png
    :alt: Les principaux dossiers de MindFlow

**Configuration :**

**1.** Allez dans le dossier ``/mf_websites/`` et renommez le sous-dossier intitulé ``/www.sample-website.com/``, de préférence avec le nom de domaine de votre site web, mais ce n'est pas obligatoire.

|
|

**2.** Créez une base de données MySQL et notez les informations suivantes :

* nom de l'hôte (serveur) de la BDD
* nom de la BDD
* nom d'utilisateur
* mot de passe

|
|

**3.** Éditez le fichier ``/mf_websites/www.votresite.com/config-dev.php`` et saisissez les informations de votre base de données dans les champs suivants :

.. code-block:: php

	'db_host' => 'mysql.myserver.com',
	'db_port' => '3306',
	'db_name' => 'cx_mydb',
	'db_username' => 'cx_mydbuser',
	'db_password' => '1234abcd',

|
|

**4.** Personnalisez les valeurs des champs suivants :

.. code-block:: php

	// salt pour le cryptage des mots de passe
	// IMPORTANT : ne laissez pas la valeur par défaut
	'salt' => 'Nv6abc4454819sA', 
	
	// mot de passe de l'utilitaire d'installation
	// saisissez un mot de passe personnalisé
	'installTool_password' => 'unmotdepasse', 

|
|

	
**5.** Dans ce même fichier toujours, recherchez et remplacez la valeur ``'www.sample-website.com'`` par le nom du dossier de votre site web sous le dossier ``/mf_websites`` : par exemple ``www.votresite.com``

|
|

**6.** Éditez le fichier ``/mf_config/config.php`` et ajoutez à l'instruction switch / case une entrée ayant pour clé le nom de domaine de votre site web et pointant vers le fichier de configuration que vous avez édité à l'étape 3. :

.. code-block:: php

	switch($_SERVER['HTTP_HOST']){
		case 'www.votresite.com':
			require_once DOC_ROOT.SUB_DIR.'/mf_websites/www.votresite.com/config-dev.php'; 
			break;
		default:
			echo "<p>Warning : ".$_SERVER['HTTP_HOST']. " : this hostname could not be found in mf_config/config.php</p>";
			die();
	}

		
|
|

**7.** Rendez-vous dans l'utilitaire d'installation pour créer les tables. Pour ce faire, connectez-vous à l'adresse :

http://www.votresite.com/mf/install

Tapez le mot de passe que vous avez défini dans le champ 'installTool_password' pour vous connecter. 

.. image:: images/install-tool-1.png
    :alt: Mot de passe install tool

Puis cliquez sur 'Créer les tables SQL' : 

.. image:: images/install-tool-2.png

   
Pour chaque Plugin listé comportant des tables, cliquez sur le lien **'Installer le plugin'** pour créer toutes ces tables dans la base de données et importer les données de base :

.. image:: images/install-tool-3.png

.. important::

	Il y a 2 manières de créer les tables pour un plugin : 
	
	* le lien **'Installer le plugin'** crée toutes les tables et y insère les données de base en une seule opération. celui-ci est destiné à l'installation.
	
	|vspace|
	
	* le lien **Créer cette table** ne crée que la table de la ligne qui le contient, et n'importe pas les données de base. Il faut ensuite cliquer sur le lien **import données** qui apparait si des données de bases sont disponibles pour la table courante. La présence du lien **Créer cette table** est avant tout destinée aux développeurs qui font de nombreux tests lors de la création de classes d'enregistrements en PHP, pour leur permettre de créer et supprimer facilement des tables lors de la mise au point de leur code.
	
|
|

**8.** Assurez-vous que les dossiers suivants ont les droits en écriture depuis votre serveur HTTP (généralement chmod 775 et owner www-data, mais ça dépend des systèmes d'exploitation) :

::

	/mf_websites/www.votresite.com/temp/
	/mf_websites/www.votresite.com/uploads/
	
|
|

**9.** Vous pouvez vous connecter au module d'administration avec les identifiants par défaut :

::

	URL : http://www.votresite.com/mf/
	user : "admin"
	password : "1234"

.. important:: N'oubliez pas de **modifier le mot de passe du compte administrateur**. Il est également conseillé de renommer le compte administrateur en autre chose que 'admin', ce qui rendra bien plus difficiles les attaques en force brute.