<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

/**
 * Base class for database records, without form managment nor load/store functions
 *
 * Supplies table creation tools, but does not automatically aggregate dbRecord's basic fields (like uid, creation_date, etc.)
 * The user is responsible for the full definition of his table.
 *
 */
abstract class simpleRecord{

    static $tableName;                      //name of the SQL table
    static $oneTablePerLocale = false;      //if set to true, mindflow will create and manage one table per locale. Each table will feature a suffix for the locale, such as _fr, _en or _de
    static $createTableSQL;                 //SQL for the inherited class
    static $createTableKeysSQL;             //SQL keys for the inherited class

    static $enableImportInitializationData = 0;  //Enables initializing the table with data imported from function importInitializationData()


    //flags for features
    var $enableHistory=false;       //storing record history impacts performance, so it is disabled by default
    var $reloadFormOnSave=false;    //forces re-evaluating and displaying the record's editing form throught AJAX feedback once the record is saved. Useful if some value affects the form aspect. Disabled by default for performance optimization.

    //SQL for the dbRecord class : all mindflow records must have these fields available
    static $baseTableSQL= "";

    //ALTER TABLE `vz_orders_CB_fr` ADD `history_last` TEXT NOT NULL DEFAULT '' AFTER `alt_language`, ADD `history_past` TEXT NOT NULL DEFAULT '' AFTER `history_last`;

    //SQL for the keys of the dbRecord class : all mindflow records must have these fields available
    static $baseTableKeysSQL = "";



    static function getTableName(){
        global $mf;
        return static::$tableName.((static::$oneTablePerLocale)?'_'.$mf->getLocale():'');
    }



    /**
     * Performs the SQL requests required to define basic data for the current table / record
     */
    static function importInitializationData(&$html=array()){}


    /**
     * Creates the SQL table for this record in the database.
     * If you have set $oneTablePerLocale to true, the function will only create the table for the currently defined locale. If you want to create different tables for different locales,
     * you will have to alter the locale value in $mf->info['data_editing_locale'] prior creating a table for the desired locale.
     * @param int $showSQL generate HTML showing the result of the SQL request and feed the $html array passed by reference with it
     * @returns 1 on success, 0 when failing when $showSQL is set to false, returns creation sequence including genrated SQL request as HTML text when $showSQL is set to true.
     * @param bool $importInitializationData Fill the table with initialization data after creation if this data is available
     * @return int returns 1 if successful creation, 0 otherwise.
     */
    static function createSQLTable($showSQL=0,&$html=array(),$importInitializationData=false)
    {
        global $mf,$l10n,$pdo,$config;

        $status = 0;

        $tableSQL = static::$baseTableSQL.static::$createTableSQL;
        $keysSQL = rtrim(trim(static::$baseTableKeysSQL.static::$createTableKeysSQL),',');

        $sql = "CREATE TABLE ".static::getTableName()." (".
            $tableSQL.
            $keysSQL."
        ) ENGINE=".$config['db_engine']." DEFAULT CHARSET=utf8;";

        if($showSQL)$html[] = '<p><small>'.nl2br($sql).'</small></p>';


        $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );

        if ($pdo->exec($sql) !== false) {
            $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
            if($showSQL){
                $html[] = '<div class="alert alert-success"><strong>PDO::errorInfo():</strong><br />';
                $html[] = "<p style=\"font-weight:bold\">".$l10n->getLabel('dbRecord','table')." \"".static::getTableName()."\" ".$l10n->getLabel('dbRecord','creation_success')."</p>";
                $html[] = '</div>';
                $status = 1;
            }
            else $status = 1;

            if(static::$enableImportInitializationData && $importInitializationData){

                static::importInitializationData($html);

            }
            return $status;
        }
        else{
            if($showSQL){
                $html[] = '<div class="alert alert-danger"><strong>PDO::errorInfo():</strong><br />';
                $error = $pdo->errorInfo();
                $html[] = 'SQLSTATE='.$error[0].' <br />';
                $html[] = 'DRIVER ERROR CODE='.$error[1].' <br />';
                $html[] = 'ERROR MESSAGE='.$error[2].' <br />';
                $html[] = "<p style=\"font-weight:bold\">".$l10n->getLabel('dbRecord','creation_failure')." \"".static::getTableName()."\".</p>";
                $html[] = '</div>';

            }
            $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
            return 0;
        }

        //if($pdo->exec($sql) !== false) { return 1; }
        //else return 0;
    }

    /**
     * Import a given SQL dump
     * @param $filepath absolute file path
     */
    static function importSQLDump($filepath,$showSQL=0,&$html=array()){
        global $mf,$l10n,$pdo;

        $lineCounter = 0;
        $errorCounter = 0;

        // Temporary variable, used to store current query
        $templine = '';
        // Read in entire file
        $lines = file($filepath);

        $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );

        // Loop through each line
        foreach ($lines as $line)
        {
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || substr($line, 0, 2) == '#' || $line == '')
                continue;

            // Add this line to the current segment
            $templine .= $line;
            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';')
            {
                // Perform the query

                if ($pdo->exec($templine) !== false) {
                    $lineCounter++;
                }
                else{
                    $lineCounter++;
                    $errorCounter++;
                    if($showSQL){
                        $html[] = '<div class="alert alert-danger"><strong>PDO::errorInfo():</strong><br />';
                        $error = $pdo->errorInfo();
                        $html[] = 'SQLSTATE='.$error[0].' <br />';
                        $html[] = 'DRIVER ERROR CODE='.$error[1].' <br />';
                        $html[] = 'ERROR MESSAGE='.$error[2].' <br />';
                        $html[] = 'ERROR AT LINE='.$lineCounter.' <br />';
                        $html[] = '</div>';

                    }
                }

                // Reset temp variable to empty
                $templine = '';
            }
        }
        $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $html[] = '<div class="alert alert-success">';
        $html[] = "<p style=\"font-weight:bold\">".($lineCounter-$errorCounter)." ".$l10n->getLabel('dbRecord','import_success')."</p>";
        $html[] = '</div>';
        return 1;
    }




    static function getClassName(){
        return get_called_class();
    }


}
