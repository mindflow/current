<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "My plugin demo",
        'desc' => "<p>The purpose of this plugin is to demonstrate the basic architecture / requirements of a MindFlow plugin.</p>"


    ),


    'fr' => array(
        'name' => "Démo mon plugin",
        'desc' => "<p>L'objet de ce plugin est de présenter l'architecture de base / les éléments requis pour créer un plugin MindFlow</p>"


    ),
);

?>