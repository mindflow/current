<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class urlRewriter implements module
{
    private $moduleKey = 'urlRewriter';
    private $pluginVersion = '1.0';
    private $moduleType = 'frontend,backend';
    private $dependencies = '';
    public $triggerWords;

    static $parsedUrl;

    function __construct(){
        self::$parsedUrl = array();
        $this->triggerWords = array();

    }


    function prepareData(){

        global $mf,$l10n,$config;

        if($mf->mode == 'frontend'){

            //parse the current URL request
            self::$parsedUrl = j_parseUrl(selfURL());

            //convert and decode $parsedUrl['query'] as an array of parameters
            parse_str(urldecode(self::$parsedUrl['query']), self::$parsedUrl['query']);

            //create a copy of the page path,but in array version for convenience
            self::$parsedUrl['pathArray'] = explode('/',trim(self::$parsedUrl['path'],'/'));

            //retreive URL path length
            $pathLength = $this->getPathLength();

            //echo "pathSize=". $pathLength."<br />".chr(10);
            //print_r( self::$parsedUrl['pathArray']);
            //echo "<br />".chr(10);

            //trim the extension of the last path segment, in case we have a .html or .htm extension
            if( $pathLength > 0){
                $lastSegment = self::$parsedUrl['pathArray'][ $pathLength-1];
                //replace the path segment with its value without extension
                self::$parsedUrl['pathArray'][ $pathLength-1] = preg_replace('/\.[^.]+$/','',$lastSegment);
            }

            $languages = array_keys($config['frontend_locales']);

            //check language from the url if in frontend mode. Setting www.domain.com/fr/ or /en/ will switch the language
            //if no language ISO2 code is supplied, default language will apply
            if(isset(self::$parsedUrl['pathArray'][0]) && in_array(self::$parsedUrl['pathArray'][0],$languages)){
                //set the language & strip the ISO2 code from the url for not disturbing other functions
                $mf->info['frontend_locale'] = array_shift(self::$parsedUrl['pathArray']);
            }

            //add the parsed URL to the mfInfo object so it can be re-used by plugins :
            $mf->info['parsed_url'] = self::$parsedUrl;

        }

        //set the page tree in all cases
        $this->setPageTree();

        if($mf->mode == 'frontend'){
            $currentPage = $this->getCurrentPage();

            if($currentPage == false) {
                $currentPage = 0;
                $mf->info['http_status'] = 404;
            }
            else{
                //also show 404 error if the page has been hidden & the admin is not logged into the backend
                if(isset($mf->info['pagesByUid'][$currentPage]) && $mf->info['pagesByUid'][$currentPage]->data['hidden']['value'] == '1'){
                    if(isset($mf->currentUser)) $mf->frontendMessages[] = "<div class='mf_message warning'>".$l10n->getLabel('main','page_hidden')."</div>";
                    else {
                        $currentPage = 0;
                        $mf->info['http_status'] = 404;
                    }
                }
                else if(!is_numeric($currentPage)){
                	//if we don't have a boolean nor a page uid stored in $currentPage, we have an API call. Execute it.
	                eval($currentPage);
                }
            }


            $mf->info['currentPageUid'] = $currentPage;
        }




    }



    function render(&$mainTemplate){

    }

    /**
     * retreive URL path length and correct it (table has 1 element even if the path is void)
     * @return int
     */
    function getPathLength(){
        $pathLength = sizeof(self::$parsedUrl['pathArray']);
        if( $pathLength==1 && self::$parsedUrl['pathArray'][0]=='') {
            self::$parsedUrl['pathArray']=array();
            $pathLength=0;
        }
        return $pathLength;
    }

    /**
     * Returns the uid of the current page
     * false if no page is found
     * @return int
     */
    function getCurrentPage(){
        global $mf;

        foreach($mf->info['pageTree'] as $page){

            //process from site root pages. Pages at root level non marked as site root are ignored.
            if($page->data['is_siteroot']['value'] == 1){

                //retreive URL path length
                $pathLength = $this->getPathLength();

                //if there are no path segments, return the root page
                if($pathLength < 1){
                    //echo "path1 ";
                    return $page->data['uid']['value'];
                }
                else{
                    //echo "path2 ";
                    if(isset($page->children)){

                        foreach($page->children as $child){

                            $candidateUid = $this->seekPage($child, 0);
                            if($candidateUid == false)continue;

                            else{
                            	return $candidateUid;
                            }
                        }
                    }
                    else return $page->data['uid']['value'];


                }
            }

        }
        //if there is no page defined
        return false;
    }

    /**
     * recursively process the current page and its children looking for the segment with index $urlPathSegmentIndex
     * Say you have the following path : /fr/products/testPage.html
     * 'fr' will have an $urlPathSegmentIndex of 0,
     * 'products' will have an $urlPathSegmentIndex of 1,
     * 'testPage' will have an $urlPathSegmentIndex of 2,
     *
     * The function will return the page uid having its $page->data['url_path_segment']['value'] matching the given $urlPathSegmentIndex, so if a page with uid = 31 has $page->data['url_path_segment']['value']='products', the function will return  value 31
     * If no page is found, false is returned
     *
     * @param $page page object
     * @param $urlPathSegmentIndex the index representing the position of the seeked segment in the current path
     * @return int the page uid if found a match, else false
     */
    function seekPage($page, $urlPathSegmentIndex){

        //if there is a match in the path segment
        global $mf;

        $currentURLSegment = self::$parsedUrl['pathArray'][$urlPathSegmentIndex];

        //trigger words processing is prioritary
        foreach($this->triggerWords as $index => $triggerWordData){
            if($triggerWordData['type']=='segment'){
                if($currentURLSegment == $triggerWordData['word'])return $triggerWordData['page'];
            }
            else if($triggerWordData['type']=='prefix'){
                if(substr($currentURLSegment, 0, strlen($triggerWordData['word'])) == $triggerWordData['word']){
                    return $triggerWordData['page'];
                }
            }
            else if($triggerWordData['type']=='api'){

	            if($currentURLSegment == $triggerWordData['word']){
		            return $triggerWordData['page'];
	            }
            }
        }

        //then process regular url path segments
        if($page->data['url_path_segment']['value'] == $currentURLSegment){
            if(++$urlPathSegmentIndex == $this->getPathLength()){
                //if this is the last segment in the path, return the page
                //echo "return ".$page->data['uid']['value']."<br>".chr(10);
                return $page->data['uid']['value'];
            }
            else{
                //continue with child pages
                if($page->children){
                    foreach($page->children as $child){
                        $candidateUid = $this->seekPage($child, $urlPathSegmentIndex);
                        if($candidateUid == false) continue;
                        else {
                            //echo "return ".$candidateUid."<br>".chr(10);
                            return $candidateUid;
                        }
                    }
                }
            }
        }

        else {
            return false;
        }
    }


    /**
     * Trigger words can be specified to the urlRewriter using this function. When a trigger word is met, the associated page Uid is called immediately.
     * Thus it is recommended that trigger words are met only for the required purpose in your site and not for pages unrelated to that purpose, as the trigger word would interfere with their own url rewriting.
     * @param string $triggerWord the trigger word to look for (ex : 'products')
     * @param string $triggerType can be "segment", "prefix" or "api". "segment" will attempt to match the triggerWord with the whole segment (ex:'/products/') while prefix will attempt to match a segment prefix (ex: 'products-for-your-home'), "api" wil behave like segment, but wil eval() the code contained in ;
     * @param int $pageUid this page uid will be returned when the trigger word is met inside the current page path. If the $triggerType is "api" enter here a code to be evaluated, such as "global $myAPI = new myAPI();"
     */
    function addTrigger($triggerWord,$triggerType='segment',$pageUid){
    	//global $logger;
    	//echo "addTrigger($triggerWord,$triggerType,$pageUid)<br />".chr(10);

        //check triggerType conformity
        $triggerTypes = array('segment','prefix','api');
        if(!in_array($triggerType,$triggerTypes)){
            $triggerType = 'segment';
        }

        //ad to the list of triggerWords
        $this->triggerWords[]=array(
            'word' => $triggerWord,
            'type' => $triggerType,
            'page' => $pageUid
        );
    }




    /**
     * Loads all the pages of the website and store a tree representation of the website in the $mf->info['pageTree'] and $mf->info['pagesByUid'] values
     * This will allow quick and easy access to pages from all plugins and prevent useless SQL requests
     */
    function setPageTree(){

        global $mf,$pdo;

        $results = $pdo->query("SHOW TABLES LIKE 'mf_pages'");
        //if table mf_pages is available
        if($results && $results->rowCount()>0) {
            //get all the existing pages that have not been deleted
            $sql = "SELECT * FROM mf_pages WHERE deleted = 0 ORDER BY sorting";
            $stmt = $pdo->prepare($sql);

            try{
                $stmt->execute(array());
            }
            catch(PDOException $e){
                echo $e->getMessage();
            }

            $pageRows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //convert the rows to an array of page objects where the key is the page uid
            foreach($pageRows as $pageRow){
                $mf->info['pagesByUid'][$pageRow['uid']] = new page();
                $mf->info['pagesByUid'][$pageRow['uid']]->loadPageFromRow($pageRow);
            }

            $mf->info['pageTree'] = $this->buildTree($mf);

            //print_r($mf->info['pageTree']);
            //var_dump($mf->info['pageTree']);

            return $stmt->rowCount();
        }
        else {
            $mf->info['pageTree']=array();
            return 0;
        }

    }

    /**
     * Generates a tree of page objects. The first page is the root of the website
     * @param $mf
     * @param null $currentPage
     * @return mixed
     */
    private function buildTree(&$mf,$currentPage = NULL, $language=''){
        if($language == '')$language = $mf->getDataEditingLanguage();

        if($currentPage == NULL){
            //first seek the root page(s)
            foreach($mf->info['pagesByUid'] as $page){
                //echo "parsing pagesByUid page=".$page->data['uid']['value'].chr(10);
                //echo "test=(".$page->data['language']['value']." == ".$language.");<br />";
                if($page->data['language']['value'] == $language){
                    //echo "test=(page->data['parent_uid']['value']=".$page->data['parent_uid']['value'].");<br />";
                    if($page->data['parent_uid']['value'] <= 1){
                        //echo "adding page=".$page->data['uid']['value']." to root".chr(10);
                        $mf->info['pageTree'][] = $page;
                        $this->buildTree($mf,$page);
                    }
                }
            }
            return $mf->info['pageTree'];
        }
        else{
            foreach($mf->info['pagesByUid'] as &$page){
                //echo "parsing pagesByUid page=".$page->data['uid']['value'].chr(10);
                if($page->data['language']['value'] == $language && $page->data['parent_uid']['value'] > 0){
                    if($page->data['parent_uid']['value'] == $currentPage->data['uid']['value']){
                        //echo "adding page=".$page->data['uid']['value']." as child of ".$currentPage->data['uid']['value']."".chr(10);
                        $currentPage->children[] = &$page;
                        $this->buildTree($mf,$page);
                    }
                }
            }
        }
    }

    /*
     * Returns the absolute URL below the homepage for the uid supplied
     * @param $pageUid int uid of the page for which the URL is requested
     * @param $appendHtmlSuffix boolean appends '.html' at the end of the url if true
     * @return string the url
     */
    function getPageURL($pageUid, $appendHtmlSuffix=true){
        //removed because of double call. setPageTree() is already used during prepareData(). It doesn't seem usefull to call it again
        //$this->setPageTree();

        global $mf,$l10n,$config;

        if(isset($mf->info['pagesByUid'][$pageUid])){
            $languagePrefix='';

            if(sizeof($config['frontend_locales'])>1){
                //if there are more than 1 language in use, append the language ISO2 code in the URL
                if($mf->mode=='frontend')$languagePrefix = '/'.$mf->info['pagesByUid'][$pageUid]->data['language']['value'];
                else $languagePrefix = '/'.$mf->info['data_editing_locale'];
            }

            $segment = $this->processPageURL($pageUid);
            if($segment != '') return $languagePrefix.$segment.(($appendHtmlSuffix)?'.html':'');
            else return $languagePrefix.'/';
        }
        else return $l10n->getLabel('backend','page_with_uid').$pageUid.$l10n->getLabel('backend','not_found');
    }

    /**
     * Recursive function retreiving all segments in the path in order to build the path
     * @param int $pageUid the uid of the page to look for at the end of the path
     * @return string the path found
     */
    private function processPageURL($pageUid){
        global $mf;

        //if the child page exists
        if(isset($mf->info['pagesByUid'][$pageUid])){
            //process and return the segment
            $currentPage = $mf->info['pagesByUid'][$pageUid];
            $pathSegment = $currentPage->data['url_path_segment']['value'];
            $parentUid = $currentPage->data['parent_uid']['value'];
            //if the parent is not the root page, append the segment to the path
            if($parentUid > 1){
                return $this->processPageURL($parentUid).'/'.$pathSegment;
            }
            else return '';
        }
        else return '/';
    }

    /*
     * Returns the absolute URL below the homepage for the uid supplied
     * @param $pageUid uid of the page for which the URL is requested
     * @param $class string the html class to apply to the link
     * @param $id string the html id to apply to the link
     * @return string the url
     */
    function getPageLink($pageUid, $class='', $id=''){
        global $mf,$l10n;

        if($class != '') $class = ' class="'.$class.'"';
        if($id != '') $id = ' id="'.$id.'"';
        if(isset($mf->info['pagesByUid'][$pageUid]))return '<a href="'.$this->getPageURL($pageUid).'"'.$class.$id.' >'.$mf->info['pagesByUid'][$pageUid]->data['title']['value'].'</a>';
        else return '<a href="#"'.$class.$id.' >'.$l10n->getLabel('urlRewriter','error_uid_not_found1').$pageUid.$l10n->getLabel('urlRewriter','error_uid_not_found2').'</a>';
    }





    function getType(){
        return $this->moduleType;
    }


}
