<?php

//require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

/**
 * This function is called on EVERY page load. You may want
 */
function pagePrepareData(){
/*
    $mf = &$GLOBALS['mf'];
    $locale = $mf->getLocale();

    if(isset($_REQUEST['my_var']) ){
        $_SESSION['my_var'] = $_REQUEST['my_var'];
    }
*/
}

function getGoogleAnalytics(){

    $googleID = 'UA-123456-1';

    if($googleID !=''){
            //Nouveau code Universal analytics
           return "
            <script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
              ga('create', '".$googleID."', 'auto');
              ga('send', 'pageview');
            </script>";
    }
    else return '';
}

function getLang($page, $key, $value){
    $mf = &$GLOBALS['mf'];
    return $mf->getFrontendLanguage();
}

function getLogo($page, $key, $value) {
    $mf = &$GLOBALS['mf'];
    $l10n = $mf->l10n;
    switch($mf->getFrontendLanguage()){
        case 'fr':
            return '<a href="/"><img src="{template_path}/img/logo-fr.png" alt="'.$l10n->getLabel('sample_website','logo').'" title="'.$l10n->getLabel('sample_website','logo').'" /></a>';
        case 'en':
            return '<a href="/"><img src="{template_path}/img/logo-en.png" alt="'.$l10n->getLabel('sample_website','logo').'" title="'.$l10n->getLabel('sample_website','logo').'" /></a>';

    }
}

function getSiteName(){
    return $GLOBALS['config']['sitename'];
}

function getNavigation(){
    return '<nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>';
}

?>