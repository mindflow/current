<?php

//default configuration for the plugin
//add and edit these values in your config file
$config['plugins']['contacts'] = array(
    'display-form' => 'true',
    'listRecordsInAllLanguages' => true,

    'email-template' => '/mf/plugins/contacts/ressources/templates/email.html',
    //'email-template' => $config['website_pluginsdir'].'/contacts/templates/email.html',
    'contact-class' => '/mf/plugins/contacts/records/contact.php',
    //'contact-class' => $config['website_pluginsdir'].'/contacts/records/contact.php',
    'contact-class-l10n' => '/mf/plugins/contacts/languages/contact.php',
    //'contact-class-l10n' => $config['website_pluginsdir'].'/contacts/l10n/contact.php',

    'email-subject' => 'Message reçu sur le site Internet',
    'email-intro' => '<p>Bonjour,</p><p>Vous avez reçu le message suivant sur le site Internet :</p>',
    'email-from' => array('site@sample-website.com' => 'Sample website'),
    'email-image' => '/mf_websites/www.sample-website.com/templates/img/logo-fr.png',
    'email-confirm' => array(
        'fr'=>'Votre message nous a bien été transmis. Merci pour votre intérêt.',
        'en' => 'Your message has been successfully transmitted. Thank you for your interest.',
        'de' => 'Wir haben Ihre Nachricht erhalten. Wir werden uns schnellstmöglich bei Ihnen melden. Danke für Ihre Anfrage und Ihr Interesse an unseren Reisen!'
    ),
    'email-destination' => 'your_recipient_address@sample-website.com',
    'email-destination-name' => 'Recipient Name',

);