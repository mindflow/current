<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow coutry data tables",
        'desc' => "<p>Supplies tables and datas for : <ul><li>Countries list</li><li>French departments list</li><li>French municipalities list</li><li>French Colissimo parcel sending fees</li></ul></p>"


    ),


    'fr' => array(
        'name' => "Tables données des pays MindFlow",
        'desc' => "<p>Fournit des tables de données pour : <ul><li>Liste des pays</li><li>Liste des départements Français</li><li>Liste des municipalités Françaises</li><li>Tarifs d'envoi Colissimo en France</li></ul></p>"


    ),
);

?>