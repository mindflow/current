<?php
/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/formField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/inputField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/selectField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/recordSelectField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/recordEditTableField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/checkboxField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/radioField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/dateField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/dateTimeField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/timeField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/colorField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/hiddenField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/urlInputField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/altLangField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/richTextField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/textareaField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/filesField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/microtemplateField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/templateNameField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/templateDataField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/htmlField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/dynHtmlField.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formFields/fieldset.php';



/**
 * Handles record forms and simple forms display and processing
 *
 * The formsManager class is responsible of the generation and processing of forms for the dbRecord and dbForm inherited objects.
 * It parses the data array of the object and builds the form fields, ordering them by pannels.
 * Then when the form is submited, it processes the fields, detects erroneous fields if any, then generates an object of the dbRecord or dbForm class with its values filled from the submited values.
 *
 */
class formsManager{

    private $cssList = array();         //List of css files to link in the page
    private $topJsList = array();       //Javascript files called in the page header
    private $bottomJsList = array();    //Javascript files called at the end of the page to prevent page loading slowdowns
    //private $microJsList = array();    //Javascript files called when loading microtemplates
    private $priorSendFormJS = array();    //Javascript code called before saving forms. MUST NOT be be wrapped between <script></script> tags
    private $postSendFormJS = array();    //Javascript code called after saving forms. MUST NOT be be wrapped between <script></script> tags
    var $tagsToBeClosed = array();      //some tags such as <fieldset> may be spanned accross multiple tags. This array indicates after which field the tag should be closed and the value of the closing field

    private $fieldTypes = array(); //list of available field types to be displayed in forms

    function __construct(){


        //adding Mindflow standard field types
        //additional custom field types can be supplied by plugins using the mf->formsManager->addFieldType() function.
        $this->addFieldType('input','inputField', false);
        $this->addFieldType('dummy-input','inputField', true);

        $this->addFieldType('select','selectField', false);
        $this->addFieldType('dummy-select','selectField', true);

        $this->addFieldType('record','recordSelectField', false);

        $this->addFieldType('recordEditTable','recordEditTableField', true);

        $this->addFieldType('checkbox','checkboxField', false);
        $this->addFieldType('dummy-checkbox','checkboxField', true);

        $this->addFieldType('radio','radioField', false);
        $this->addFieldType('dummy-radio','radioField', true);

        $this->addFieldType('date','dateField', false);
        $this->addFieldType('dummy-date','dateField', true);

        $this->addFieldType('datetime','dateTimeField', false);
        $this->addFieldType('dummy-dateTime','dateTimeField', true);

	    $this->addFieldType('datetime.ms','inputField', false);
	    $this->addFieldType('dummy-dateTime.ms','inputField', true);

        $this->addFieldType('time','timeField', false);
        $this->addFieldType('dummy-time','timeField', true);

        $this->addFieldType('color','colorField', false);
        $this->addFieldType('dummy-time','colorField', true);

        $this->addFieldType('hidden','hiddenField', false);
        $this->addFieldType('dummy-hidden','hiddenField', true);

        $this->addFieldType('urlinput','urlInputField', false);
        $this->addFieldType('dummy-urlinput','urlInputField', true);

        $this->addFieldType('altLang','altLangField', false);
        $this->addFieldType('dummy-altlang','altLangField', true);

        $this->addFieldType('rich_text','richTextField', false);
        $this->addFieldType('dummy-rich_text','richTextField', true);

        $this->addFieldType('textarea','textareaField', false);
        $this->addFieldType('dummy-textarea','textareaField', true);

        $this->addFieldType('files','filesField', false);
        $this->addFieldType('microtemplate','microtemplateField', false);
        $this->addFieldType('template_name','templateNameField', false);
        $this->addFieldType('template_data','templateDataField', false);
        $this->addFieldType('html','htmlField', true);
        $this->addFieldType('dyn_html','dynHtmlField', false);
        $this->addFieldType('fieldset','fieldset', true);
    }

    /**
     * Adds a new field type that can be displayed and processed by the forms manager. Must be invoked prior prepareData() sequence, such as in a plugin's index.php file.
     * @param $fieldIdentifier The name of the field to specify in $myForm->data['myFieldName']['dataType'] => 'myFieldIdentifier' in order to trigger this fieldClass
     * @param $fieldClass the class which takes car of displaying the field (must implement mf/core/interfaces/formField.php interface). The name of the class is also the field identifier to specify in $myForm->data['myFieldName']['dataType'] => 'myFieldTypeClassName'
     * @param $isDummy set to true if the field is not due to be stored in the database
     */
    function addFieldType($fieldIdentifier,$fieldClass,$isDummy){
        $this->fieldTypes[$fieldIdentifier] = $fieldClass;
        if($isDummy){
            dbRecord::$dummyFieldTypes[] = $fieldIdentifier;
        }

    }

    function prepareData(){


        //add custom field types CSS and JS
        /*foreach($this->fieldTypes as $fieldClass){
            eval("\$mf->addCss(".$fieldClass."::getCSS(),true,true);");
            eval("\$mf->addTopJs(".$fieldClass."::getTopJS(),true,true);");
            eval("\$mf->addBottomJs(".$fieldClass."::getBottomJS(),true,true);");
        }*/
    }







    /**
    /**
     * Outputs the HTML form code for editing a dbForm subclassed object in the backend
     * @param $actionURL string URL where the form should be submited. Default value is SUB_DIR.'/mf/core/ajax-core-json.php'
     * @param $actionName string value for the hidden field "action". Default value is 'processForm'
     * @param $callBackJSFuncName string name of the JS function called on the client side which will deal with the AJAX answered data
     * @param $formInstance form the form instance to display
     * @param $moduleName string the calling module / plugin. Set value to '' for frontend forms.
     * @param $subModuleName string the calling submodule / plugin.. Set value to '' for frontend forms.
     * @param $showSubmitButton boolean whether to show or not a submitButton at the end of the form
     * @param $submitButtonLabel string the text to display on the submit button
     * @param $submitButtonClass string the classes to apply to the submit button
     * @param $useAJAX boolean set to true to submit the form using AJAX. Default is true.
     * @param $advancedSettings array specifies some additional form settings. These are the default advancedSettings :
     *        $advancedSettings = array(
     *           'JSCallbackFunctionAfterSubmit'=>'',  //invokes the supplied JS function after saving the record. The function is also invoked if saving the record fails. The supplied function must have the following profile : myCallbackFunction(status (="success" or = "error"),jsonData);
     *           'PHPCallbackFunctionAfterSubmit'=>'',       //invokes the supplied phpFunction after submiting the form. The function is also invoked if saving the submission fails. The supplied function must have the following profile : static function myCallbackFunction($status (="success" or = "error"),$formObject); If a value is returned by the function, it will be supplied as a jsondata.phpResult value in the JSON response of the submission. It can then be handled using your own JSCallbackFunctionAfterSubmit javascript function
     *           'submitButtonOnClick' => "sendForm();",     //customize the submit button onclick action
     *           'showCancelButton' => false,           //set to true if you want to show a cancel button
     *           'cancelButtonLabel' => '',                  //customize the save close button label with the given text
     *           'cancelButtonOnClick' => 'history.go(-1);', //customize the cancel button onclick action usual values would be 'history.go(-1);' for non ajax forms and 'mf.dialogManager.closeLastDialog();' when the form has been opened using mf.dialogManager.openDialog() method.
     *        );
     * @param $formAttributes array sets attributes for the "form" tag. When modifying, be carefull not to alter the default values you do not need to modify. default values are defined as is : array('method'=>'POST', 'onsubmit'=>'sendForm();', 'role'=>'form', 'class'=>'form-horizontal','enctype'=>'x-www-form-urlencoded')
     * @param $recordEditTableID string specify a unique recordEditTableID you want to refresh after form submission here. 
     * @return string the form html code
     */
    function editForm(
        $actionURL=null,
        $actionName='processForm',
        $callBackJSFuncName='',
        $formInstance,
        $moduleName,
        $subModuleName,
        $showSubmitButton=true,
        $submitButtonLabel='',
        $submitButtonClass='btn btn-primary col-lg-offset-2',
        $useAJAX = true,
        $advancedSettings = null,
        $formAttributes = null,
        $recordEditTableID = null
    ){
        global $mf,$l10n;

        if(!isset($actionURL)) $actionURL= SUB_DIR.'/mf/core/ajax-core-json.php';

        if ($advancedSettings === null) {
            $advancedSettings = array(
                'PHPCallbackFunctionAfterSubmit'=>'',
                'JSCallbackFunctionAfterSubmit'=>'',
                'submitButtonOnClick' => "sendForm();",
                'showCancelButton' => false,
                'cancelButtonLabel' => $l10n->getLabel('main','cancel'),
                'cancelButtonOnClick' => 'history.go(-1);',
            );
        }

        if(!isset($advancedSettings['JSCallbackFunctionAfterSubmit']))$advancedSettings['JSCallbackFunctionAfterSubmit']='';
        //if(isset($callBackJSFuncName) && $callBackJSFuncName !='')$advancedSettings['JSCallbackFunctionAfterSubmit']= $callBackJSFuncName;

	    if(!isset($formAttributes['id'])){
		    $formID = 'form_'.str_replace('.','_',rand(0,9999999));
	    }
	    else{
		    $formID = $formAttributes['id'];
	    }


	    if(!isset($advancedSettings['submitButtonOnClick'])) $advancedSettings['submitButtonOnClick'] = "saveForm_".$formID."();";

        if ( $formAttributes === null) {
            $formAttributes = array( //adds/overloads all the attributes specified into this array to the <form> tag
                'method'=>'POST',
                'onsubmit'=>'saveForm_'.$formID.'();return false;',
                'role'=>'form',
                'class'=>'form-horizontal',
                'enctype'=>'x-www-form-urlencoded'
                /*'method'=>'POST',

                'action'=> '/mf/core/ajax-core-json.php', // you could set 'action'=>'/my-custom-url.php',
                //'onsubmit'=>'myCustomJSFunction();',
                'role'=>'form',
                'class'=>'form-horizontal',
                'enctype'=>'x-www-form-urlencoded'*/
            );
        }

        if($recordEditTableID=='') $recordEditTableID=null;

        if(isset($advancedSettings['PHPCallbackFunctionAfterSubmit']) && $advancedSettings['PHPCallbackFunctionAfterSubmit'] != '')$PHPCallback = true;
        else $PHPCallback = false;


        //process additional form attributes
        $formAttr = '';
        if(isset($formAttributes['action'])){
            $actionURL = $formAttributes['action'];
            unset($formAttributes['action']);
        }
        if(trim($actionURL) == '')$actionURL = $_SERVER['PHP_SELF'];


	    $formAttr .= ' id="'.$formID.'"';

        foreach($formAttributes as $name => $value){
            $formAttr .= ' '.$name.'="'.$value.'"';
        }

        if($submitButtonClass == null or trim($submitButtonClass) == '')$submitButtonClass = 'btn btn-primary col-lg-offset-2';


        $html = array();


        //form security variables
        $secKeys = array(
            'action' => $actionName,
            'class' => get_class ($formInstance),
            'module' => $moduleName,
            'submodule' => $subModuleName,
            'mode' => $mf->mode,
	        'mfSetLocale' => $mf->getLocale(),
        );
        if($PHPCallback)$secKeys['callbackFunction'] = $advancedSettings['PHPCallbackFunctionAfterSubmit'];
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));


        //inserting div for creating/displaying dialogs if not available in the page.
        $html[] = '
                    <div id="edf_dialogHolder"></div>
                    <script>
                    if($("#mf_dialogs").length == 0) {
                        //document.write("<div id=\"mf_dialogs\"></div>");
                        document.getElementById("edf_dialogHolder").innerHTML="<div id=\"mf_dialogs\"></div>";
                    }
                    
                    function showError(jsonData){
                    var sliceIndex = jsonData.responseText.indexOf("{\"jsonrpc\"");
	                    alert("'.$l10n->getLabel('backend','mf_error').'\n\n"+jsonData.responseText.slice(0,sliceIndex));
	                }
                    </script>';

        $html[] = '<form action="'.$actionURL.'"'.$formAttr.'>';

	    $html[] = '<input type="hidden" name="formID" id="formID" value="'.$formID.'" />';
        $html[] = '<input type="hidden" name="module" id="module" value="'.$moduleName.'" />';
        $html[] = '<input type="hidden" name="submodule" id="submodule" value="'.$subModuleName.'" />';

       /* $html[] = '<input type="hidden" name="showSubmitButton" id="showSubmitButton" value="'.$showSubmitButton.'" />';
        $html[] = '<input type="hidden" name="showCancelButton" id="showCancelButton" value="'.$advancedSettings['showCancelButton'].'" />';
        $html[] = '<input type="hidden" name="useAJAX" id="useAJAX" value="'.$useAJAX.'" />';
        $html[] = '<input type="hidden" name="advancedSettings" id="advancedSettings" value="'.urlencode(base64_encode(http_build_query($advancedSettings))).'" />';
        $html[] = '<input type="hidden" name="formAttributes" id="formAttributes" value="'.urlencode(base64_encode(http_build_query($formAttributes))).'" />';
*/
        if($PHPCallback)$html[] = '<input type="hidden" name="callbackFunction" id="callbackFunction" value="'.$advancedSettings['PHPCallbackFunctionAfterSubmit'].'" />';

        $html[] = '<input type="hidden" name="action" id="action" value="'.$actionName.'" />';
        $html[] = '<input type="hidden" name="mode" id="mode" value="'.$mf->mode.'" />';

        $html[] = '<input type="hidden" name="fm_action" id="fm_action" value="submit" />';
        $html[] = '<input type="hidden" name="class" id="class" value="'.get_class ($formInstance).'" />';
        $html[] = '<input type="hidden" name="mfSetLocale" id="mfSetLocale" value="'.$mf->getLocale().'" />';
        $html[] = '<input type="hidden" name="sec" id="sec" value="'.$secHash.'" />';
        $html[] = '<input type="hidden" name="fields" id="fields" value="'.$secFields.'" />';
        $html[] = '<input type="hidden" name="ignoreMandatoryFields" id="ignoreMandatoryFields" value="0" />';
        $html[] = '<input type="hidden" name="saveThoseFields" id="saveThoseFields" value="" />';

        $html[] =  $this->makePrefixPanel($formInstance,$formID);

        $i=0;

        if(count($formInstance->showInEditForm) > 1){
            //show tabs if we have more than 1 panel
            $html[] = '<ul id="recordEditor" class="nav nav-tabs">';

            foreach($formInstance->showInEditForm as $tab_id => $datas){
                $html[] = '<li id="t_'.$tab_id.'_'.$formID.'" '.(($i++ == 0)?'class="active"':'').'><a href="#'.$tab_id.'_'.$formID.'" data-toggle="tab">'.$l10n->getLabel(get_class ($formInstance),$tab_id).'</a></li>';
            }

            $html[] = '</ul>';
        }

        $html[] = '<div class="tab-content">';

        $html[] =  $this->makePanels($formInstance,$formID);

        $html[] = '
        <div id="fm_notification">
            <div id="fm_notificationMessage_'.$formID.'"></div>
        </div>';

        //$html[] = '<div class="mfSaveButtons">';

        //cast $showSubmitButton to boolean in case it would have been given as string
        $showSubmitButton = filterBoolean($showSubmitButton);

        $html[] = '<div class="mfSaveButtons"><div class="row"><div class="col-lg-12"><div class="formButtons">';

        $paddNextButton = " col-lg-offset-4";
        if($showSubmitButton) {

            if($useAJAX){
                $html[] = '<button  id="saveBtn" type="button" onclick="'.$advancedSettings['submitButtonOnClick'].'" class="'.$submitButtonClass.'">'.(($submitButtonLabel!='')?$submitButtonLabel:$l10n->getLabel('backend','save_form')).'</button>';
            }
            else $html[] = '<button  id="saveBtn" type="submit" class="'.$submitButtonClass.'">'.(($submitButtonLabel!='')?$submitButtonLabel:$l10n->getLabel('backend','save_form')).'</button>';
            $paddNextButton = "";
        }
        if($advancedSettings['showCancelButton']){
            $cancelButtonLabel = (isset($advancedSettings['cancelButtonLabel']) && $advancedSettings['cancelButtonLabel'] != '')?$advancedSettings['cancelButtonLabel']:$l10n->getLabel('main','cancel');
            $html[] = '<button  id="closeBtn" type="button" class="btn closeButton'.$paddNextButton.'" onclick="'.$advancedSettings['cancelButtonOnClick'].'" >'.$cancelButtonLabel.'</button>';
        }


        $html[] = '</div></div></div></div><!-- /.mfSaveButtons -->';

        $html[] = '</div><!-- /.tab-content -->';
        $html[] = '</form>';

        $html[] = '<script>

		//we keep this function for compatibility, but in case of multiple forms in the page, saveForm_'.$formID.'() should be preferred
        function sendForm(closeDialogOnSave=false){
            saveForm_'.$formID.'(closeDialogOnSave);
        }
        
        
        function saveForm_'.$formID.'(closeDialogOnSave=false){
            console.log("saveForm_'.$formID.'("+closeDialogOnSave+");");
        ';

        $html[] = implode(chr(10),$this->priorSendFormJS);

        $html[] = '
            

            var imgCode = "<div class=\"savingNotification\"><img src=\"'.SUB_DIR.$mf->xEnd->template.'/img/360.gif\" width=\"14\" height=\"14\" alt=\"\" />'.$l10n->getLabel('backend','processing').'</div>";

            $("#fm_notificationMessage_'.$formID.'").html(imgCode);';




    if($useAJAX) $html[] = '$.ajax({
                url: "'.$actionURL.'",
                type: $("#'.$formID.'").attr("method"),
                data: $("#'.$formID.'").serialize(),
                dataType: "json",
                success: function(jsonData) {
                    
                    console.log("result="+jsonData.result );
                    
                    JSCallbackFunctionAfterSubmit = "'.$advancedSettings['JSCallbackFunctionAfterSubmit'].'";

                    if(jsonData.result == "success"){
                    
                        //reset error fields
                        $("#' . $formID . ' .has-error").removeClass("has-error");
                        $("#' . $formID . ' .errorMsg").html("");
                        $("#' . $formID . ' .tabErrorCount").remove();
                                
                        $("#fm_notificationMessage_'.$formID.'").html("");
                        
                        cbJSFuncName = "'.$callBackJSFuncName.'";
                        
                        if( cbJSFuncName != ""){
                            if(typeof cbJSFuncName === "function") cbJSFuncName();
                            else eval(cbJSFuncName+"(jsonData);");
                        }
                        // Process callback functions
                        // JSCallbackFunctionAfterSubmit is set when calling editRecord and occurs at every save of the form
                        // while callback is set on calling saveform() and can be used only on a specific save operation
                        // if both have the same value, the callback function will be called only once
                        // otherwise, all defined functions will be called
                        
                        if((JSCallbackFunctionAfterSubmit != "" && JSCallbackFunctionAfterSubmit != undefined) ){
                                eval(JSCallbackFunctionAfterSubmit+"(\"success\",jsonData,\"'.$formID.'\");");
                        }
                        
                        '.((isset($recordEditTableID))?'//update record list view to integrate the current record data changes to the list
                        if(!(typeof lastDisplay_'.$recordEditTableID.' === "undefined")){
                        
                            updateRecordEditTable_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], lastDisplay_'.$recordEditTableID.'["page"]);
                        }
                        ':'').'
                        if(closeDialogOnSave)mf.dialogManager.closeLastDialog();
                    }
                    else if(jsonData.result == "error"){
                     
                        //submit failure, erroneous fields
                
                        console.log("Adding errors");
                        var errorFields = jsonData.failed_fields;
                        var errorCountByTab = jsonData.error_count_by_tab;
                        //reset error fields
                        $("#' . $formID . ' .has-error").removeClass("has-error");
                        $("#' . $formID . ' .errorMsg").html("");
                        $("#' . $formID . ' .tabErrorCount").remove();
                
                        for(key in errorFields){
                            $("#' . $formID . ' #div_"+key).addClass("has-error");
                            $("#' . $formID . ' #div_"+key+" .errorMsg").html(errorFields[key]);
                        }
                
                        for(tab in errorCountByTab){
                            if(errorCountByTab[tab] == 1) $("#t_"+tab+"_' . $formID . ' a").append( " <span class=\"tabErrorCount\">("+errorCountByTab[tab]+"' . $l10n->getLabel('main', 'error2') . '"+")</span>" );
                            else if(errorCountByTab[tab] > 1) $("#t_"+tab+"_' . $formID . ' a").append( " <span class=\"tabErrorCount\">("+errorCountByTab[tab]+"' . $l10n->getLabel('main', 'errors') . '"+")</span>" );
                        }
                
                        $("#fm_notificationMessage_' . $formID . '").html(jsonData.message);
                        
                        if(cbJSFuncName !=""){
                            if(typeof cbJSFuncName === "function")cbJSFuncName();
                            else eval(cbJSFuncName+"(jsonData);");
                        }
                        
                        if((JSCallbackFunctionAfterSubmit != "" && typeof JSCallbackFunctionAfterSubmit !== undefined)){
                            if(typeof JSCallbackFunctionAfterSubmit === "function")JSCallbackFunctionAfterSubmit();
                            else eval(JSCallbackFunctionAfterSubmit+"(\"error\",jsonData,\"'.$formID.'\");");
                        }
        
                    }
                    else if(jsonData.result == "error"){
                        $("#fm_notificationMessage_' . $formID . '").html(jsonData.message);
                        return false;
                    }
                    
           
                },
                error: function(jsonData) {
                    showError(jsonData);
                    return false;
                }
            });';

    if($useAJAX)$html[] = 'return false;
        }

        </script>';


        $cssList        = implode(chr(10),$this->cssList);
        $topJsList      = implode(chr(10),$this->topJsList);
        $bottomJsList   = implode(chr(10),$this->bottomJsList);

        return $cssList.$topJsList.implode(chr(10),$html).$bottomJsList;

    }











    /**
     * Outputs the HTML form code for editing a dbRecord subclassed object in the backend
     * @param $record class the record to display an editing form for
     * @param $moduleName string the plugin id for calling it from the url with &module=
     * @param $subModuleName string optional : the plugin id for calling it from the url with &submodule= (requires non void $moduleName value)
     * @param $showSaveButton boolean Enables / disables the display of the "Save" button at the end of the form
     * @param $showSaveCloseButton boolean Enables / disables the display of the "Save and close" button at the end of the form
     * @param $showPreviewButton boolean Enables / disables the display of the "Save and preview" button at the end of the form
     * @param $showCloseButton boolean Enables / disables the display of the "Close" button at the end of the form
     * @param $ajaxEditing record the form output as AJAX
     * @param $advancedSettings array allows customization of some aspects of the form : set a javascript function to be called after form submission, set a php function to be called after form submission, alter the text of the save button, save close button, preview button or close button : array('JSCallbackFunctionAfterSubmit'=>'', 'PHPCallbackFunctionAfterSubmit'=>'', 'saveButtonLabel' => '', 'saveCloseButtonLabel' => '', 'previewButtonLabel' => '', 'closeButtonLabel' => '',)
     * @param $formAttributes array sets attributes for the "form" tag. When modifying, be carefull not to alter the default values you do not need to modify. default values are defined as is : array('method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal','enctype'=>'x-www-form-urlencoded')
     * @param $recordEditTableID string if the editRecord() function is called from a recordEditTable, specify the unique recordEditTableID here. This will allow refreshing the view of the source recordEditTable once the editRecord form is closed. Specify a null or void string '' if you have to set the parameter, but there is no matching recordEditTable in some case.
     * @param $editHistoryMode boolean shows up history editing features on form fields. Normally you don't want to use this feature, which is only required for MindFlow's record history editing.
     * @return string the form html code
     */

    //* @param $phpPageOnClose under certain circumstances, such as when calling ediBackend() from an AJAX script, we may loose the value of the orginal PHP script for the module, which is called when closing the form for returning to the module main page.

    function editRecord(
            $record,
            $moduleName,
            $subModuleName = '',
            $showSaveButton=true,
            $showSaveCloseButton=true,
            $showPreviewButton=true,
            $showCloseButton=true,
            $ajaxEditing = 0,
            $advancedSettings = null,
            $formAttributes = null,
            $recordEditTableID = null,
			$editHistoryMode = false
    ){

        if ($advancedSettings === null) {
            $advancedSettings = array(
                'JSCallbackFunctionAfterSubmit'=>'',  //invokes the supplied JS function after saving the record. The function is also invoked if saving the record fails. The supplied function must have the following profile : myCallbackFunction(status (="success" or = "error"),jsonData);
                'PHPCallbackFunctionAfterSubmit'=>'', //invokes the supplied phpFunction after saving the record. The function is also invoked if saving the record fails. The supplied function must have the following profile : myCallbackFunction($status (="success" or = "error"),$recordClassName,$recordUid);
	                                                  // If a value is returned by the function, it will be supplied as a jsondata.phpResult value in the JSON response of the submission. It can then be handled using your own JSCallbackFunctionAfterSubmit javascript function
                'saveButtonLabel' => '',    //customize the save button label with the given text
                'saveCloseButtonLabel' => '',    //customize the save close button label with the given text
                'previewButtonLabel' => '',    //customize the preview button label with the given text
                'closeButtonLabel' => '',    //customize the close button label with the given text
                'disableSave' => 'false', //not only the save buttons will not be shown, but the form saving javascript code won't be output, making it impossible to trick the form saving process
                'noEdit' => 'false', //disable editing for all fields of the record with this simple flag
                'reloadFormOnSave' => 'false', //forces re-evaluating and displaying the form throught AJAX feedback once the record is saved. Useful if some value affects the form aspect.
                'showHistoryButton' => ($record->enableHistory)?'true':'false', //adds a form button for requesting display of the record history
            );
        }



        if ( $formAttributes === null) {
            $formAttributes = array( //adds/overloads all the attributes specified into this array to the <form> tag
                'method'=>'POST',

                'action'=> '/mf/core/ajax-core-json.php', // you could set 'action'=>'/my-custom-url.php',
                //'onsubmit'=>'myCustomJSFunction();',
                'role'=>'form',
                'class'=>'form-horizontal',
                'enctype'=>'x-www-form-urlencoded'
            );
        }

        if($recordEditTableID=='') $recordEditTableID=null;

        global $mf,$l10n,$pdo;

     	$html = array();
        $showSaveButton         = filterBoolean($showSaveButton);
        $showSaveCloseButton    = filterBoolean($showSaveCloseButton);
        $showPreviewButton      = filterBoolean($showPreviewButton);
        $showCloseButton        = filterBoolean($showCloseButton);
        if(isset($advancedSettings['showHistoryButton']))$showHistoryButton        = filterBoolean($advancedSettings['showHistoryButton']);
        else $showHistoryButton = $record->enableHistory;

        if(isset($advancedSettings['PHPCallbackFunctionAfterSubmit']) && $advancedSettings['PHPCallbackFunctionAfterSubmit'] != '')$PHPCallback = true;
        else $PHPCallback = false;

        if(isset($advancedSettings['disableSave']))$disableSave = filterBoolean($advancedSettings['disableSave']);
        else $disableSave = false;

        if(isset($advancedSettings['noEdit']))$noEdit = filterBoolean($advancedSettings['noEdit']);
        else $noEdit = false;



        if(isset($advancedSettings['reloadFormOnSave']))$reloadFormOnSave = filterBoolean($advancedSettings['reloadFormOnSave']);
        else $reloadFormOnSave = false;
        //The flag set inside the record takes priority if set to true
        if($record->reloadFormOnSave == true)$reloadFormOnSave = $advancedSettings['reloadFormOnSave'] = true;


        if($noEdit) {
            //disable data editing for every record is noEdit is true
            foreach ($record->data as $key => $value) {
                $record->data[$key]['editable'] = 0;
            }
        }

        //process additional form attributes
        $formAttr = '';
        if(!isset($formAttributes['action']))$formAttributes['action'] = SUB_DIR . '/mf/core/ajax-core-json.php';
        if($formAttributes['action'] == '/mf/core/ajax-core-json.php'){
            //fix value. concatenation cannot be done in function defaut parameter setup
            $formAttributes['action'] = SUB_DIR . '/mf/core/ajax-core-json.php';
        }

        //set autocomplete = off by default in backend because it interferes with typeahead popups and is hard to circumvent
        //also autcomplete is rarely required in backends
        if($mf->mode == "backend" && !isset($formAttributes['autocomplete']))$formAttributes['autocomplete'] = 'off';


        if(!isset($formAttributes['id'])){
            $formID = 'form_'.str_replace('.','_',rand(0,9999999));
            $formAttr .= ' id="'.$formID.'"';
            if($noEdit) $formAttr .= ' class="formNoEdit"';
        }
        else{
            $formID = $formAttributes['id'];
        }
        $classSet = false;
        foreach($formAttributes as $name => $value){
            if(strcasecmp($name, "class") == 0)$value .= " formNoEdit";
            $formAttr .= ' '.$name.'="'.$value.'"';
        }
        if(!$classSet && $noEdit)$formAttr .= ' class="formNoEdit"';

		$action = 'saveRecord';
		$class = get_class ($record);
		if(isset($record->data['uid']['value']))$uid = $record->data['uid']['value'];
        else $mf->addErrorMessage($l10n->getLabel('backend','uid_not_defined1').' '.$class.' '.$l10n->getLabel('backend','uid_not_defined2'));



	    //Managment for warning message when multiple users are editing the same record
	    if($disableSave==false && $noEdit == false){
		    if($record->data['uid']['value'] != '') {
			    $isLocked = ($mf->mode == 'backend' && $record->data['edit_lock']['value'] == '1' && $record->data['editor_uid']['value'] != '0' && $record->data['editor_uid']['value'] != $mf->currentUser->data['uid']['value']);

			    if($isLocked && $record->data['edit_start_time']['value'] != '' && $record->data['edit_start_time']['value']!='NULL'){
			    	$editDate = DateTime::createFromFormat( $GLOBALS['site_locales'][ $mf->getLocale() ]['php_datetime_format'], $record->data['edit_start_time']['value'] );
				    $dateNow = new DateTime('NOW');
				    $interval = $editDate->diff($dateNow);
				    //records edited since more than 2 days are no longer considered as locked.
				    if($interval->format('%a') > 2)$isLocked = false;
			    }


			    //if the record is locked, show warning message + unlock button
			    if($isLocked){
				    $editorClass = $record->data['editor_class'][ 'value' ];

				    //check if the editorClass exists on this system (the record may have been imported from another system)
				    if(class_exists($editorClass)) {
					    $editor = new $editorClass();
					    $editor->load( $record->data['editor_uid']['value'] );


					    //function for unlocking a record which is being edited by another user
					    $html[] = '
				        <script>
				            //disable save record buttons when a record is locked
				            $("#' . $formID . ' #saveBtn").prop("disabled", true);
					        $("#' . $formID . ' #saveCloseBtn").prop("disabled", true);
				        
				            function unlockRecord(recordUid, recordClass, xEndMode, unlockHash, unlockFields){
				                if(recordUid != undefined){
					                $.ajax({
					                    url: SUB_DIR+"/mf/core/ajax-core-json.php",
					                    type: "POST",
					                    data: { action:"unlock_record", record_uid: recordUid, record_class: recordClass, mode: xEndMode, sec: unlockHash, fields:unlockFields, mfSetLocale:"' . $mf->getLocale() . '"},
					                    dataType: "json",
					                    success: function(jsonData) {
					                        console.log("Record "+recordClass+"("+recordUid+") unlocked successfuly");
					                        
					                        $("#' . $formID . ' #saveBtn").prop("disabled", false);
					                        $("#' . $formID . ' #saveCloseBtn").prop("disabled", false);
					                        
					                        $("#lockMessage").replaceWithPush("<div class=\"alert alert-info alert-dismissable\">' . $l10n->getLabel( 'main', 'can_now_edit' ) . ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button></div>").delay(4000).fadeOut(1000);                   
					                    }
					                });
					            }
				            }
						</script>
				        ';

					    $unlockSec = getSec( array(
						    'action'       => 'unlock_record',
						    'record_uid'   => $record->data['uid']['value'],
						    'record_class' => get_class( $record ),
						    'mfSetLocale'  => $mf->getLocale(),
						    'mode'         => $mf->mode,
					    ) );


					    if ( $record->data['editor_uid']['value'] != '0' ) {
						    $editorName = $editor->data['first_name']['value'] . ' ' . $editor->data['last_name']['value'];
					    } else {
						    $editorName = $record->data['editor_class']['value'];
					    }

					    //display warning message
					    $mf->addCustomMessage( '<div id="lockMessage"><div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' .
					                           $l10n->getLabel( 'main', 'warn_edit1' ) . $editorName . $l10n->getLabel( 'main', 'warn_edit2' ) . $editDate->format( 'd/m/Y' ) . $l10n->getLabel( 'main', 'warn_edit3' ) . $editDate->format( 'H:m' ) . $l10n->getLabel( 'main', 'warn_edit4' ) .
					                           ' <button class="mf-btn-new btn-xs smallBtnText" onclick="unlockRecord(' . $record->data['uid']['value'] . ',\'' . get_class( $record ) . '\',\'' . $mf->mode . '\',\'' . $unlockSec->hash . '\',\'' . $unlockSec->fields . '\');">' . $l10n->getLabel( 'main', 'warn_edit_force' ) . '</button>' .
					                           '</div></div> ' );


				    }
				    else{
				    	//if the editorClass does not exist, reset the editor to the current user
					    if ( isset( $mf->currentUser ) ) {
						    //set the creator user
						    $editor_uid   = $mf->currentUser->data['uid']['value'];
						    $editor_class = get_class( $mf->currentUser );
					    } else if ( $mf->mode == 'frontend' ) {

						    $editor_uid   = 0;
						    $editor_class = 'unidentified (frontend)';
					    } else {

						    $editor_uid   = 0;
						    $editor_class = 'unidentified (backend)';
					    }


					    $recordClass = get_class( $record );
					    $sql         = "UPDATE " . $recordClass::getTableName() . " SET edit_lock=1, editor_uid=" . $editor_uid . ", editor_class=" . $pdo->quote( $editor_class ) . " , edit_start_time=NOW() WHERE uid=" . $record->data['uid']['value'];
					    $stmt        = $pdo->query( $sql );
				    }
			    }
			    else {
				    //if the record is editable, we will activate the edit_lock flag during its editing
				    if ( isset( $mf->currentUser ) ) {
					    //set the creator user
					    $editor_uid   = $mf->currentUser->data['uid']['value'];
					    $editor_class = get_class( $mf->currentUser );
				    } else if ( $mf->mode == 'frontend' ) {

					    $editor_uid   = 0;
					    $editor_class = 'unidentified (frontend)';
				    } else {

					    $editor_uid   = 0;
					    $editor_class = 'unidentified (backend)';
				    }


				    $recordClass = get_class( $record );
				    $sql         = "UPDATE " . $recordClass::getTableName() . " SET edit_lock=1, editor_uid=" . $editor_uid . ", editor_class=" . $pdo->quote( $editor_class ) . " , edit_start_time=NOW() WHERE uid=" . $record->data['uid']['value'];
				    $stmt        = $pdo->query( $sql );
			    }
		    }
	    }



		if($uid != ''){
			$mfSetLocale = $record->data['language']['value'];

			//form security variables
			$secKeys = array(
				'action' => $action,
				'class' => $class,
				'uid' => $uid,
				'mode' => $mf->mode,
				'mfSetLocale' => $mfSetLocale,
			);
			if($PHPCallback)$secKeys['callbackFunction'] = $advancedSettings['PHPCallbackFunctionAfterSubmit'];
		}
		else{
			$mfSetLocale = $mf->getLocale();

			//form security variables
			$secKeys = array(
				'action' => $action,
				'class' => $class,
                'mode' => $mf->mode,
				'mfSetLocale' => $mfSetLocale,
			);
			if($PHPCallback)$secKeys['callbackFunction'] = $advancedSettings['PHPCallbackFunctionAfterSubmit'];
		}

		//DEBUG SEC
		//print_r($secKeys);
		$formSec = getSec($secKeys);

        //inserting div for creating/displaying dialogs if not available in the page.
        $html[] = '
                    <div id="edr_dialogHolder"></div>
                    <script>
                    if($("#mf_dialogs").length == 0) {
                        //document.write("<div id=\"mf_dialogs\"></div>");
                        document.getElementById("edr_dialogHolder").innerHTML="<div id=\"mf_dialogs\"></div>";
                    }
                    //var lastEditedUid="";
                    console.log("Displaying formID '.$formID.'");
                    </script>';



        $advancedSettingsStr = urlencode(base64_encode(http_build_query($advancedSettings)));
        $formAttributesStr = urlencode(base64_encode(http_build_query($formAttributes)));

        if($PHPCallback)$PHPCallbackStr = '<input type="hidden" name="callbackFunction" id="callbackFunction" value="'.$advancedSettings['PHPCallbackFunctionAfterSubmit'].'" />';
        else $PHPCallbackStr = '';

	    $closeSec = getSec(array(
		    'mfSetLocale' => $mfSetLocale,
		    'action' => 'close_record',
		    'record_uid' => $uid,
		    'record_class' => $class,
		    'mode' => $mf->mode,
	    ));

        $html[] = <<<EOT
        <form$formAttr>
            <input type="hidden" name="formID" id="formID" value="$formID" />
            <input type="hidden" name="module" id="module" value="$moduleName" />
            <input type="hidden" name="submodule" id="submodule" value="$subModuleName" />
            <input type="hidden" name="showSaveButton" id="showSaveButton" value="$showSaveButton" />
            <input type="hidden" name="showSaveCloseButton" id="showSaveCloseButton" value="$showSaveCloseButton" />
            <input type="hidden" name="showPreviewButton" id="showPreviewButton" value="$showPreviewButton" />
            <input type="hidden" name="showCloseButton" id="showCloseButton" value="$showCloseButton" />
            <input type="hidden" name="ajaxEditing" id="ajaxEditing" value="$ajaxEditing" />
            <input type="hidden" name="advancedSettings" id="advancedSettings" value="$advancedSettingsStr" />
            <input type="hidden" name="formAttributes" id="formAttributes" value="$formAttributesStr" />
            <input type="hidden" name="uid" id="uid" value="$uid" />
            $PHPCallbackStr
            <input type="hidden" name="action" id="action" value="$action" />
            <input type="hidden" name="mode" id="mode" value="$mf->mode" />
            <input type="hidden" name="fm_action" id="fm_action" value="submit" />
            <input type="hidden" name="class" id="class" value="$class" />
            <input type="hidden" name="mfSetLocale" id="mfSetLocale" value="$mfSetLocale" />
            <input type="hidden" name="sec" id="sec" value="{$formSec->hash}" />
            <input type="hidden" name="fields" id="fields" value="{$formSec->fields}" />
            <input type="hidden" name="ignoreMandatoryFields" id="ignoreMandatoryFields" value="0" />
            <input type="hidden" name="saveThoseFields" id="saveThoseFields" value="" />
            <input type="hidden" name="recordEditTableID" id="recordEditTableID" value="$recordEditTableID" />
            <input type="hidden" name="closeSec" id="closeSec" value="{$closeSec->hash}" />
            <input type="hidden" name="closeFields" id="closeFields" value="{$closeSec->fields}" />
EOT;


        $html[] =  $this->makePrefixPanel($record,$formID);

        $i=0;

		if(count($record->showInEditForm) > 1){
			//show tabs if we have more than 1 panel
			$html[] = '<ul id="recordEditor" class="nav nav-tabs">';

			foreach($record->showInEditForm as $tab_id => $datas){
				if($tab_id != '') //do not show tab if $tab_id==''.  This is intended for displaying some data more freely prior tabbed data.
				    $html[] = '<li id="t_'.$tab_id.'_'.$formID.'" '.(($i++ == 0)?'class="active"':'').'><a href="#'.$tab_id.'_'.$formID.'" data-toggle="tab">'.$l10n->getLabel(get_class ($record),$tab_id).'</a></li>';
			}

			$html[] = '</ul>';

		}

		$html[] = '<div class="tab-content">';

		$html[] =  $this->makePanels($record,$formID,$editHistoryMode);

		$html[] = '
		<div id="fm_notification">
			<div id="fm_notificationMessage_'.$formID.'"></div>
		</div>';

		$html[] = '<div class="mfSaveButtons"><div class="row"><div class="col-lg-12"><div class="formButtons">';


        $paddNextButton = " col-lg-offset-4";
        if($showSaveButton){
			$saveButtonLabel = (isset($advancedSettings['saveButtonLabel']) && $advancedSettings['saveButtonLabel'] != '')?$advancedSettings['saveButtonLabel']:$l10n->getLabel('backend','save_changes');
			$html[] = '<button  id="saveBtn" type="button" onclick="saveAction_'.$formID.'(\'save\');" class="btn saveButton'.$paddNextButton.'">'.$saveButtonLabel.'</button>';
            $paddNextButton = "";
		}

		if($showSaveCloseButton){
			$saveCloseButtonLabel = (isset($advancedSettings['saveCloseButtonLabel']) && $advancedSettings['saveCloseButtonLabel'] != '')?$advancedSettings['saveCloseButtonLabel']:$l10n->getLabel('backend','save_close');
			$html[] = '<button id="saveCloseBtn" type="button" onclick="saveAction_'.$formID.'(\'save_close\');" class="btn btn-primary saveCloseButton'.$paddNextButton.'">'.$saveCloseButtonLabel.'</button>';
            $paddNextButton = "";
		}

		if($showPreviewButton){
			$previewButtonLabel = (isset($advancedSettings['previewButtonLabel']) && $advancedSettings['previewButtonLabel'] != '')?$advancedSettings['previewButtonLabel']:$l10n->getLabel('backend','preview');
			$html[] = '<button id="previewBtn" type="button" onclick="saveAction_'.$formID.'(\'save_preview\');" class="btn previewButton'.$paddNextButton.'">'.$previewButtonLabel.'</button>';
            $paddNextButton = "";
		}

		if($showCloseButton){
			$closeButtonLabel = (isset($advancedSettings['closeButtonLabel']) && $advancedSettings['closeButtonLabel'] != '')?$advancedSettings['closeButtonLabel']:$l10n->getLabel('backend','close');
			$html[] = '<button id="closeBtn" type="button" class="btn closeButton'.$paddNextButton.'" onclick="saveAction_'.$formID.'(\'close\');" >'.$closeButtonLabel.'</button>';
			$paddNextButton = "";
		}


		// display the record's global history button
	    if($showHistoryButton){

		    $sec = getSec(array(
			    'mfSetLocale' => $mfSetLocale,
			    'action' => 'editHistory',
			    'record_uid' => $uid,
			    'record_class' => $class,
			    'mode' => $mf->mode,
			    'formID' => $formID,
		    ));

		    $historyLink = SUB_DIR . "/mf/core/ajax-core-html.php?{$sec->parameters}&sec={$sec->hash}&fields={$sec->fields}";
		    $html[] = '<button id="historyBtn" type="button" class="btn btn-default'.$paddNextButton.'" onclick="showHistory(\''.$historyLink.'\');" ><span class="glyphicons glyphicons-history pointer" style="margin-top:3px;"></span> '.$l10n->getLabel('main','history').'</button>';
	    }

		$html[] = '</div></div></div></div><!-- /.mfSaveButtons -->';

		$html[] = '</div><!-- /.tab-content -->';
		$html[] = '</form>';

		//url for previewing this class of record
		//$previewUrl = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$record->getPreviewUrl() : "http://".$_SERVER['SERVER_NAME'].$record->getPreviewUrl();
//print_r($record->data['uid']);
		$html[] = '<script>
		var editedIndex = (typeof editedIndex === "undefined") ? 0 : editedIndex;';

	    if($showHistoryButton){
		    $html[] = '
		    
		    function showHistory(historyLink){
		        mf.dialogManager.openDialog("record_dialog","","",historyLink,"95%","","'.$l10n->getLabel('main','history').' '.$l10n->getLabel($class,'record_name').' '.$uid.'");
		    }
		    
		    ';
	    }

        $html[] = '
        
            function restoreHistoryStep2(jsonData){
                console.log("restoreHistoryStep2 result=");
                console.log(jsonData.result);
            }
            
            
            /**
            * Displays an error message contained in an AJAX response from the server and displays it in the browser window
            */
            function showError(jsonData){
                var sliceIndex = jsonData.responseText.indexOf("{\"jsonrpc\"");
                alert("'.$l10n->getLabel('backend','mf_message').'\n\n"+jsonData.responseText.slice(0,sliceIndex));
            }
    
            /**
			 * Saves a record matching the current formID. See mfForms.js saveForm() for more info.
			 */
            function saveForm_'.$formID.'(saveMode, ajaxEditing, callback, saveThoseFields, ignoreMandatoryFields){
            
                console.log("saveForm_'.$formID.'("+saveMode+", "+ajaxEditing+", "+callback+", "+saveThoseFields+") ");
                
                //if saving only some fields, put those field names in the form
                if (typeof saveThoseFields != "undefined"){

                    $("#'.$formID.' #saveThoseFields").val(saveThoseFields);
                }
    
                    
            ';

            $html[] = implode(chr(10),$this->priorSendFormJS);

            $html[] = '
                var imgCode = "<div class=\"savingNotification\"><img src=\"'.SUB_DIR.$mf->xEnd->template.'/img/360.gif\" width=\"24\" height=\"24\" alt=\"\" />&nbsp;'.$l10n->getLabel('backend','saving').'</div>";

                if(ignoreMandatoryFields != ""){
                    $("<input>").attr({
                        type: "hidden",
                        id: "ignoreMandatoryFields",
                        name: "ignoreMandatoryFields",
                        value: "1",
                    }).appendTo("#'.$formID.'");
                }
                
                $("#fm_notificationMessage_'.$formID.'").html(imgCode);';


        if(!$disableSave) {
            $html[] = '
            
                $.ajax({
                    url: $("#' . $formID . '").attr("action") ,
                    type: $("#' . $formID . '").attr("method"),
                    data: $("#' . $formID . '").serialize(),
                    dataType: "json",
                    success: function(jsonData) {
                    
                        JSCallbackFunctionAfterSubmit = "'.$advancedSettings['JSCallbackFunctionAfterSubmit'].'";
                        
                        if(jsonData.action_status == "ok"){
                        
                            if((saveMode == "save_close" && ajaxEditing) || saveMode != "save_close"){

                                //reset error fields if they had been set previously
                                $("#' . $formID . ' .has-error").removeClass("has-error");
                                $("#' . $formID . ' .errorMsg").html("");
                                $("#' . $formID . ' .tabErrorCount").remove();
                                ';

                    if($reloadFormOnSave){
                        //forces the update of the form in the page using form data sent back through AJAX.
                        //Applies only if $advancedSettings['reloadFormOnSave'] == true

                        $html[] = '
                                //update the form with the AJAX received data 
                                newFormID = substituteForm("'.$formID.'",jsonData.form,jsonData.uid); 
                             
                                ';}
                    else{
                        //regular case
	                    $html[] = '         
                                 newFormID = "'.$formID.'";
                                 //update the record uid in the form. This prevents duplicates when a new record is saved twice
                                 $("#' . $formID . ' #uid").val(jsonData.uid);
                                 
                                 //set edited option index, needed if editing a recordSelectField
                                 editedIndex = jsonData.uid;
                                 ';
	                }

                    $html[] = '
                                //show form update status message
                                $("#fm_notificationMessage_"+newFormID).html(jsonData.message).delay(4000).fadeOut(2000, function() {
                                    $(this).html("");
                                    $(this).show();
                                });
                                
                                
                                // Process callback functions
                                // JSCallbackFunctionAfterSubmit is set when calling editRecord and occurs at every save of the form
                                // while callback is set on calling saveform() and can be used only on a specific save operation
                                // if both have the same value, the callback function will be called only once
                                // otherwise, all defined functions will be called
                                
                                if((JSCallbackFunctionAfterSubmit != "" && typeof JSCallbackFunctionAfterSubmit !== "undefined") || (callback != "" && typeof callback !== "undefined")){
                                    if(JSCallbackFunctionAfterSubmit == callback){
                                        if(typeof JSCallbackFunctionAfterSubmit === "function")JSCallbackFunctionAfterSubmit()
                                            else eval(JSCallbackFunctionAfterSubmit+"(\"success\",jsonData,newFormID);");
                                    }
                                    else{
                                        if(JSCallbackFunctionAfterSubmit != "" && typeof JSCallbackFunctionAfterSubmit !== "undefined"){
                                            if(typeof JSCallbackFunctionAfterSubmit === "function")JSCallbackFunctionAfterSubmit()
                                            else eval(JSCallbackFunctionAfterSubmit+"(\"success\",jsonData);");
                                        }
                                        if(callback != "" && typeof callback !== "undefined"){
                                            if(typeof callback === "function")callback();
                                            else eval(callback+"(\"success\",jsonData,newFormID);");
                                        }
                                    }
                                }
                                 
                                if(saveMode=="save_close"){
                                     '.((isset($recordEditTableID))?'//update record list view to integrate the current record data changes to the list
                                    if(!(typeof lastDisplay_'.$recordEditTableID.' === "undefined")){
                                    
                                        updateRecordEditTable_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], lastDisplay_'.$recordEditTableID.'["page"]);
                                    }
                                    ':'').'
                                    
                                    console.log("closing dialog");
                                    mf.dialogManager.closeLastDialog();

                                }
                                else if(saveMode=="save_preview"){

                                    //show form update status message
                                    $("#fm_notificationMessage_"+newFormID).html(jsonData.message).fadeOut(2000, function() {
                                        $(this).html("");
                                        $(this).show();
                                     });
                                     ';
                    //form security variables
                    $secKeys = array(
                        'action' => "getPreviewUrl",
                        'record_class' => $class,
                        'mode' => $mf->mode,
                    );
                    $secHash = formsManager::makeSecValue($secKeys);
                    $secFields = implode(',', array_keys($secKeys));

                    $html[] = '
                                    var uid = $("#"+newFormID+" #uid").val();
                                    
                                    var previewUrl = "";
                                     var jqxhr = $.post("' . SUB_DIR . '/mf/core/ajax-core-json.php", { action:"getPreviewUrl", record_uid: uid, record_class: "' . $class . '", sec: "' . $secHash . '",fields: "' . $secFields . '",mode: "' . $mf->mode . '"})
                                    .success(function(jsonData) {
                                        //display the edit form
                                        window.open(jsonData.result,"mf_preview","width=1024,height=768,width=1024,location=yes,scrollbars=yes,status=yes,menubar=yes,resizable=yes,titlebar=yes,toolbar=yes");
                                    })
                                    .fail( function(xhr, textStatus, errorThrown) {
                                        alert("formsManager.php saveForm_'.$formID.'() : " + xhr.responseText);
                                    })
                                }
                             }' . (($mf->mode == 'backend') ? '
                             else if(saveMode=="save_close" && !ajaxEditing){

                                $("#fm_notificationMessage_' . $formID . '").queue(function() {
                                  document.location = "' . SUB_DIR . '/mf/index.php?module=' . $moduleName . (($subModuleName != '') ? '&submodule=' . $subModuleName : '') . '&action=list&class=' . get_class($record) . '";
                                });
                             }' : '
                             else {window.close();}
                             ') . '

                             
                             
                        }
                        else{ //submit failure, erroneous fields
    
                    console.log("Adding errors");
                            var errorFields = jsonData.failed_fields;
                            var errorCountByTab = jsonData.error_count_by_tab;
                            //reset error fields
                            $("#' . $formID . ' .has-error").removeClass("has-error");
                            $("#' . $formID . ' .errorMsg").html("");
                            $("#' . $formID . ' .tabErrorCount").remove();

                            for(key in errorFields){
                                $("#' . $formID . ' #div_"+key).addClass("has-error");
                                $("#' . $formID . ' #div_"+key+" .errorMsg").html(errorFields[key]);
                            }
                            
                            for(tab in errorCountByTab){
                               if(errorCountByTab[tab] == 1) $("#t_"+tab+"_' . $formID . ' a").append( " <span class=\"tabErrorCount\">("+errorCountByTab[tab]+"' . $l10n->getLabel('main', 'error2') . '"+")</span>" );
                               else if(errorCountByTab[tab] > 1) $("#t_"+tab+"_' . $formID . ' a").append( " <span class=\"tabErrorCount\">("+errorCountByTab[tab]+"' . $l10n->getLabel('main', 'errors') . '"+")</span>" );
                            }
                            
                            $("#fm_notificationMessage_' . $formID . '").html(jsonData.message);';


                        $html[] = '
                                if((JSCallbackFunctionAfterSubmit != "" && JSCallbackFunctionAfterSubmit != undefined) || (callback != "" && callback != undefined)){
                                    if(JSCallbackFunctionAfterSubmit == callback){
                                        eval(JSCallbackFunctionAfterSubmit+"(\"error\",jsonData);");
                                    }
                                    else{
                                        if(JSCallbackFunctionAfterSubmit != "" && JSCallbackFunctionAfterSubmit != undefined){
                                            eval(JSCallbackFunctionAfterSubmit+"(\"error\",jsonData,newFormID);");
                                        }
                                        if(callback != "" && callback != undefined){
                                            eval(callback+"(\"error\",jsonData);");
                                        }
                                    }
                                }
                                
                                if((JSCallbackFunctionAfterSubmit != "" && typeof JSCallbackFunctionAfterSubmit !== "undefined") || (callback != "" && typeof callback !== "undefined")){
                                    if(JSCallbackFunctionAfterSubmit == callback){
                                        if(typeof JSCallbackFunctionAfterSubmit === "function")JSCallbackFunctionAfterSubmit()
                                            else eval(JSCallbackFunctionAfterSubmit+"(\"error\",jsonData);");
                                    }
                                    else{
                                        if(JSCallbackFunctionAfterSubmit != "" && typeof JSCallbackFunctionAfterSubmit !== "undefined"){
                                            if(typeof JSCallbackFunctionAfterSubmit === "function")JSCallbackFunctionAfterSubmit()
                                            else eval(JSCallbackFunctionAfterSubmit+"(\"error\",jsonData,newFormID);");
                                        }
                                        if(callback != "" && typeof callback !== "undefined"){
                                            if(typeof callback === "function")callback();
                                            else eval(callback+"(\"error\",jsonData,newFormID);");
                                        }
                                    }
                                }
                        }
                    },
                    error: function(jsonData) {
                        showError(jsonData);
                    }
                
                });
                
                //if saving only some fields, empty field saveThoseFields after save
                if (typeof saveThoseFields != "undefined") $("#'.$formID.' #saveThoseFields").val("");
                
            ';
        }
        else{
	        $html[] = '
	        return false;';
        }
        $html[] = '
            } // end saveForm_'.$formID;

        $html[] = implode(chr(10),$this->postSendFormJS);

        //form security variables
        $secKeys = array(
            'action' => "closeRecord",
            'record_class' => $class,
            'mode' => $mf->mode,
        );
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',', array_keys($secKeys));

        $html[] = '
            /**
             * Saves a record matching the supplied formID. This function relies on saveForm(), but adds the ability to close the record.
			 * It is intended to be trigerred from buttons within the form. See mfForms.js / saveAction() for more info.
             */
            function saveAction_'.$formID.'(saveMode, callback, saveThoseFields){
            
            
                var ajaxEditing = '.(($ajaxEditing)?1:0).';
 
                if(saveMode == "save" || saveMode == "save_close" || saveMode == "save_preview"){
                    saveForm_'.$formID.'(saveMode, ajaxEditing, callback, saveThoseFields, false);
                }
                else if(saveMode == "close"){
                    if(ajaxEditing){
                        formID = "'.$formID.'";
                        uid = $("#'.$formID.' #uid").val();
                    
                        //request to cleanup the current record if it is in temporary state (never saved by direct user action, only saved for technical coherency)
                        var jqxhr = $.post("' . SUB_DIR . '/mf/core/ajax-core-json.php", { action:"closeRecord", record_uid: uid, record_class: "' . $class . '", sec: "' . $secHash . '",fields: "' . $secFields . '",mode: "' . $mf->mode . '"})
                        .success(function(jsonData) {
                        })
                        .fail( function(xhr, textStatus, errorThrown) {
                            alert("formsManager.php saveForm_'.$formID.'() : " + xhr.responseText);
                        })
       
                        '.((isset($recordEditTableID))?'//update record list view to integrate the current record data changes to the list
                        if(!(typeof lastDisplay_'.$recordEditTableID.' === "undefined")){
                            updateRecordEditTable_'.$recordEditTableID.'(lastDisplay_'.$recordEditTableID.'["sortingField"], lastDisplay_'.$recordEditTableID.'["sortingDirection"], lastDisplay_'.$recordEditTableID.'["page"]);
                        }
                        
                        ':'').'                       
                        console.log("closing dialog");
                        mf.dialogManager.closeLastDialog();
    
                    }'.
                    (($mf->mode=='backend')?'
                    else {
                        $("#fm_notificationMessage_'.$formID.'").queue(function() {
                          document.location = "'.SUB_DIR.'/mf/index.php?module='.$moduleName.(($subModuleName!='')?'&submodule='.$subModuleName:'').'&action=list&class='.get_class ($record).'";
                        });
                    }':'
                    else {window.close();}
                    ').'
                }
                return false;
            }';



        $html[] = '
        </script>';


		$cssList        = implode(chr(10),$this->cssList);
		$topJsList      = implode(chr(10),$this->topJsList);
		$bottomJsList   = implode(chr(10),$this->bottomJsList);

		return $cssList.$topJsList.implode(chr(10),$html).$bottomJsList;
        
    }







    function getMicroTemplateForm($recordUid, $recordClass, $microtemplateKey, $microtemplateIndex){
        global $mf,$l10n;
        $html = array();

        //load the microtemplate
        $record = new $recordClass();
        $record->load($recordUid);

        if(isset($record->data['template_content']['value'][$microtemplateKey]['value'][$microtemplateIndex])){
            //load microtemplate data from existing record if one is already set
            //merge it with the the microtemplate datastructure definition loaded from file in case the definition has evolved so we can reflect changes
            $microtemplate = array_merge_recursive_distinct($mf->microtemplates[$microtemplateKey],$record->data['template_content']['value'][$microtemplateKey]['value'][$microtemplateIndex]);
        }
        else {
            //initialize the microtemplate data from the microtemplate datastructure definition loaded from file
            $microtemplate = $mf->microtemplates[$microtemplateKey];
            $microtemplateIndex = 0;
        }


        //build the form
        $formID = 'form_'.str_replace('.','_',rand(0,9999999));
        $componentJSID = str_replace('-','_',$microtemplateKey);

        //form security variables
        if($recordUid !=''){
            $secKeys = array(
                'action' => "saveMicrotemplate",
                'record_class' => $recordClass,
                'record_uid' => $recordUid,
                'micro_key' => $microtemplateKey,
            );
        }
        else{
            $secKeys = array(
                'action' => "saveMicrotemplate",
                'record_class' => $recordClass,
                'micro_key' => $microtemplateKey,
            );
        }
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));

        $html[] = '<div class="container-fluid">';
        $html[] = '<div class="row">';
        $html[] = '<form action="'.$_SERVER['REQUEST_URI'].'" id="'.$formID.'" method="POST" class="form-horizontal" role="form">';

        $html[] = '<input type="hidden" name="action" id="action" value="saveMicrotemplate" />';

        $html[] = '<input type="hidden" name="record_class" value="'.$recordClass.'" />';
        $html[] = '<input type="hidden" name="record_uid" value="'.$recordUid.'" />';
        $html[] = '<input type="hidden" name="micro_key" value="'.$microtemplateKey.'" />';
        $html[] = '<input type="hidden" name="micro_index" value="'.$microtemplateIndex.'" />';
        $html[] = '<input type="hidden" name="sec" id="sec" value="'.$secHash.'" />';
        $html[] = '<input type="hidden" name="fields" id="fields" value="'.$secFields.'" />';


        if(count($microtemplate['showInEditForm']) > 1){

            //show tabs if we have more than 1 panel
            $html[] = '<ul id="recordEditor" class="nav nav-tabs">';
            $i=0;
            foreach($microtemplate['showInEditForm'] as $tab_id => $datas){
                $html[] = '<li id="t_'.$tab_id.'_'.$formID.'" '.(($i++ == 0)?'class="active"':'').'><a href="#'.$tab_id.'_'.$formID.'" data-toggle="tab">'.$l10n->getLabel($recordClass,$tab_id).'</a></li>';
            }
            $html[] = '</ul>';
        }




        $html[] = '<div id="myTabContent" class="tab-content">';

        $keysOfDatasInTab = explode(',',$microtemplate['showInEditForm']);
        $this->makeFieldset($html, $microtemplate['microtemplate_data'], $recordClass, $keysOfDatasInTab , 'micro|', NULL, $formID, $recordUid,$microtemplateKey, $microtemplateIndex, $record);


        $html[] = '<button id="mtSaveCloseBtn" type="button" class="btn btn-primary saveButton col-lg-offset-4" onclick="'.$componentJSID.'_saveMicroTemplate();closeMicroEditor();">'.$l10n->getLabel('backend','save_close').'</button>';
        $html[] = '<button id="mtCloseBtn" type="button" class="btn closeButton" onclick="closeMicroEditor();">'.$l10n->getLabel('backend','close').'</button>';

        $html[] = '</div><!-- /#myTabContent -->
        </form>
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->';


        $x_end = ($mf->mode == 'frontend')?'frontend':'backend';

        $html[] = '<script>
            microEditorJSID = "'.$componentJSID.'";

            function '.$componentJSID.'_saveMicroTemplate(){

                //transfer CKEditor content back to the textarea tag before transmitting the form
                if (typeof CKEDITOR != "undefined"){
                    for (var i in CKEDITOR.instances) {
                        (function(i){

                            var textareaID = "#"+CKEDITOR.instances[i].name.replace("|","\\\|");
                            $(textareaID).val(CKEDITOR.instances[i].getData());
                        })(i);
                    }
                }

                $.ajax({
                    url: "'.SUB_DIR.'/mf/core/ajax-core-json.php",
                    type: $("#'.$formID.'").attr("method"),
                    data: $("#'.$formID.'").serialize(),
                    dataType: "json",
                    success: function(jsonData) {
                        $("#microeditor").html(jsonData.html);
                    },
                    error: function(jsonData) {
                        showError(jsonData);
                        return false;
                    }
                });

            }

            // Delete CKEditor instances from the microtemplate form to avoid bugs. It will be recreated on the form refresh if necessary.
            // This avoid bugs when different microtemplates feature same key name for fields.
            function '.$componentJSID.'_purgeCKeditor(){
                console.log("'.$componentJSID.'_purgeCKeditor() running. $(\"#'.$formID.'\").find(textareaID).exists()");

                if (typeof CKEDITOR != "undefined"){
                    for (var i in CKEDITOR.instances) {
                        (function(i){
                            var textareaID = "#"+CKEDITOR.instances[i].name.replace("|","\\\|");


                            if($("#'.$formID.'").find(textareaID).exists()){
                                console.log("Destroying CKEditor instance "+CKEDITOR.instances[i].name);
                                CKEDITOR.instances[i].destroy();
                            }
                        })(i);
                    }
                }
            }
            </script>';

        $cssList        = implode(chr(10),$this->cssList);
        $topJsList      = implode(chr(10),$this->topJsList);
        $bottomJsList   = implode(chr(10),$this->bottomJsList);

        return $cssList.$topJsList.implode(chr(10),$html).$bottomJsList;
    }

    /**
     * If there exists a tab with id='', it will be displayed before all the other tabs. This is intended to provide a possibility to show up some form fields outside the tabbed pannels set.
     * Those fields will thus remain displayed whatever is the tab shown below
     * @param $record object record to extract the data from
     * @param $formID sets the unique ID of the form
     * @return string the HTML of the fieldset
     */
    function makePrefixPanel($record,$formID){
        global $mf,$l10n;
        $html = array();

        if(count($record->showInEditForm) > 1){
            //show tabs if we have more than 1 panel
            foreach($record->showInEditForm as $tab_id => $keysOfDatasInTab){

                if($tab_id==''){
                    //if $tab_id == '', display content without tab. This is intended for displaying some data more freely prior tabbed data
                    $this->makeFieldset($html, $record->data, get_class($record), $keysOfDatasInTab, 'record|', NULL, $formID, 0, '', '', $record);
                }
            }

        }
        return implode(chr(10),$html);
    }


    /**
     * Builds the content of the tabbed panels featuring form fields
     * @param $record object record to extract the data from
     * @param $formID sets the unique ID of the form
     * @param $editHistoryMode boolean shows up history editing features on form fields. Normally you don't want to use this feature, which is only required for MindFlow's record history editing.
     * @return string the HTML of the fieldset
     */
    function makePanels($record,$formID,$editHistoryMode = false){

        global $mf,$l10n;
        $html = array();


        $i=0;

        if(count($record->showInEditForm) > 1){
            //show tabs if we have more than 1 panel
            foreach($record->showInEditForm as $tab_id => $keysOfDatasInTab){

                if($tab_id!=''){
                    //creating tabbed panel
                    $html[] = '<div class="tab-pane fade' . (($i++ == 0) ? ' active in' : '') . '" id="' . $tab_id . '_' . $formID . '">';
                    //now build fields in each panel
                    $this->makeFieldset($html, $record->data, get_class($record), $keysOfDatasInTab, 'record|', NULL, $formID, 0, '', '', $record, $editHistoryMode);
                    $html[] = '</div>';
                }
            }

            if(isset($_REQUEST['openTab'])) {
	            $html[] = '<script>
		        	openTab("'.$formID.'", "'.$_REQUEST['openTab'].'");
            	</script>';
            }

        }
        else{
            $tabs = array_keys($record->showInEditForm);

            $this->makeFieldset($html, $record->data, get_class($record), $record->showInEditForm[$tabs[0]], 'record|', NULL, $formID, 0, '', '', $record, $editHistoryMode);
        }

        //add custom postFormHTML() code from object class
        eval('$html[] = implode(chr(10),'.get_class($record).'::$postFormHTML);');

        return implode(chr(10),$html);
    }




    /**
     * Builds the html for the form fields that displays in a record editing form. This is the heart of automatic form generation for record editing
     *
     * @param array $docHtml The html array to append html output code to
     * @param array $fieldsData The record data array to display the form for
     * @param string $recordClassName The class name of the object currently displayed
     * @param array $keysOfDatasInTab An array of the fields name to extract from the fieldsData and display in the $html (often not all fields are displayed)
     * @param string $recordMode Allows alternating label names between 'record|' and 'template|' so the forms manager can figure when collecting the data if he has to store the data in a record column or in a more complicate structure inside a template_data type field
     * @param array $mainRecordData When displaying $fieldsData from a template column, this function is called recursively with the content of the template_data supplied in the $fieldsData parameter. As a result, essential values from the root of the record's data structure such as $fieldsData['uid']['value'] (the current record uid) are no longer accessible.
     * @param $editHistoryMode boolean shows up history editing features on form fields. Normally you don't want to use this feature, which is only required for MindFlow's record history editing.
     * Passing the $fieldsData from the first function call in the $mainRecordData parameter ensures recursive function calls will still have access to the root of the record's data structure. This parameter is not required from the first function call.
     */

    function makeFieldset(&$docHtml, $fieldsData, $recordClassName, $keysOfDatasInTab, $recordMode, $mainRecordData = NULL, $formID, $recordUid=0, $microKey='', $microIndex='', $record, $editHistoryMode=false){

        global $mf,$l10n;

        if($mainRecordData == NULL)$mainRecordData = $fieldsData;

        if($recordMode != "micro|"){
            $recordUid = (isset($fieldsData['uid']['value']))? $fieldsData['uid']['value'] : $mainRecordData['uid']['value'];
        }

        //store temporarily the HTML of each field in an array indexed by the field key
        //this array will then be sorted in the order specified by $keysOfDatasInTab (that is the value $this->showInEditForm['tabName'] in our object definition)
        //this array will then be added to $html
        $allFieldsHtml = array();

        if(is_array($keysOfDatasInTab)){

            //processing each data entry for display
            foreach($keysOfDatasInTab as $index => $key){
                if(trim($key)!='') {
                    if (isset($fieldsData[$key])) {
                        $fieldData = $fieldsData[$key];

                        //get the current field's HTML
	                    $allFieldsHtml[$key] = $this->getFieldHTML($record, $fieldData, $key, $formID, $recordMode, $microKey, $microIndex, $editHistoryMode);

//                        if (isset($fieldData['dataType'])) {
//
//                            $dataType = explode(' ', trim($fieldData['dataType']));
//                            $fieldType = $dataType[0];
//
//
//                            $showWrapDiv = (isset($fieldData['showWrapDiv'])) ? $fieldData['showWrapDiv'] : true;
//
//                            $abstractFields = array('fieldset', 'html', 'template_data', 'hidden');
//
//                            if (!in_array($fieldType, $abstractFields)) { //dbRecord::$dummyFieldTypes
//                                if ($showWrapDiv) $html[] = '<div id="div_' . $key . '" class="form-group ' . $rowAttr['classes'] . '"' . $rowAttr['attributes'] . '>';
//                            }
//
//
//                            //NEW STYLE form field types
//                            if (array_key_exists($fieldType, $this->fieldTypes)) {
//                                // Defining the function arguments into an object avoids redefining all existing functions when an argument is added.
//                                // It is also easier to use when invoking the displayField function using eval().
//                                $f = new stdClass();        //this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
//                                $f->formID = $formID;       // value of the id attribute of the <form> tag this field is included in
//                                $f->mode = $recordMode;     // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
//                                $f->dataType = $dataType;   // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
//                                $f->key = $key;             // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
//                                $f->recordClassName = $recordClassName; // the class name of the record containing the current field
//                                $f->recordUid = $recordUid;     // the uid (Unique ID) of the record containing the current field
//                                $f->fieldData = $fieldData;     // this is the data array of the current field
//                                $f->fieldsData = $fieldsData;   // this is the data array of all fields
//                                $f->record = $record;           // all the data for the record containing the current field
//                                $f->microKey = $microKey;       // field name when processing a microTemplate field (only used when mode=="micro|")
//                                $f->microIndex = $microIndex;   // sorting index when processing a microTemplate field (only used when mode=="micro|")
//
//
//                                //display the field
//                                eval("\$html[] = " . $this->fieldTypes[$fieldType] . "::displayField(\$f);");
//                            }
//
//                            //close field div tag
//                            if (!in_array($fieldType, $abstractFields)) {
//                                if ($showWrapDiv) $html[] = '</div><!-- /.form-group -->';
//                            }
//                        }


//                        $allFieldsHtml[$key] = implode(chr(10), $html);


                    } else {
                        $mf->addErrorMessage($l10n->getLabel('main', 'field') . ' \'' . $key . '\' ' . $l10n->getLabel('main', 'field_not_found') . ' ' . $recordClassName . " (uid=" . $recordUid . "). " . $l10n->getLabel('main', 'is_it_defined') . "<br />" .  nl2br(generateCallTrace()));
                        //$mf->addErrorMessage(nl2br(trim(preg_replace('/[ ]{2,}|[\t]/', '&nbsp;&nbsp;&nbsp;&nbsp;',print_r($f->fieldsData,true)))));

                    }
                }
            }

        }
        //now add our fields HTML code in the order defined in $keysOfDatasInTab
        //print_r($allFieldsHtml);
        foreach ($keysOfDatasInTab as $key => $value){
            //$docHtml[] = $key." ".$value."<br />";
            if(isset($allFieldsHtml[$value]))$docHtml[] = $allFieldsHtml[$value];
            if(isset($this->tagsToBeClosed[$value]))$docHtml[] = $this->tagsToBeClosed[$value];
        }


    }

	/**
	 * Produces the HTML code for displaying a field
	 * @param dbRecord $record and instance of the record being worked on
	 * @param array $fieldData the field data to be displayed (array value). Its content may be different from what is found in the record, for example when displaying a version from the record's history
	 * @param string $fieldName the name of the field being worked on
	 * @param string $formID the id of the form in which this content is going to be displayed
	 * @param string $mode the display mode ('frontend' or 'backend')
	 * @param string $microKey field name when processing a microTemplate field (only used when mode=="micro|")
	 * @param $microIndex sorting index when processing a microTemplate field (only used when mode=="micro|")
	 * @param $editHistoryMode boolean shows up history editing features on form fields. Normally you don't want to use this feature, which is only required for MindFlow's record history editing.
	 *
	 * @return string
	 */
    function getFieldHTML($record, $fieldData, $fieldName, $formID, $mode, $microKey, $microIndex, $editHistoryMode=false){
    	global $l10n;

	    //HTML code for current field
	    $html = array();

	    $recordClass = get_class($record);

	    if (isset($fieldData['dataType'])) {

		    $dataType = explode(' ', trim($fieldData['dataType']));
		    $fieldType = $dataType[0];

		    //retreive row class and attributes customization for field
		    $rowAttr = $this->getRowAttributes($fieldName, $fieldData);

		    $showWrapDiv = (isset($fieldData['showWrapDiv'])) ? $fieldData['showWrapDiv'] : true;

		    $abstractFields = array('fieldset', 'html', 'template_data', 'hidden');

		    if (!in_array($fieldType, $abstractFields)) { //dbRecord::$dummyFieldTypes
			    if ($showWrapDiv) $html[] = '<div id="div_' . $fieldName . '" class="form-group ' . $rowAttr['classes'] . '"' . $rowAttr['attributes'] . '>';
		    }


		    //NEW STYLE form field types
		    if (array_key_exists($fieldType, $this->fieldTypes)) {
			    // Defining the function arguments into an object avoids redefining all existing functions when an argument is added.
			    // It is also easier to use when invoking the displayField function using eval().
			    $f = new stdClass();        //this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
			    $f->formID = $formID;       // value of the id attribute of the <form> tag this field is included in
			    $f->mode = $mode;     // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
			    $f->dataType = $dataType;   // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
			    $f->key = $fieldName;             // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
			    $f->recordClassName = $recordClass; // the class name of the record containing the current field
			    $f->recordUid = $record->data['uid']['value'];     // the uid (Unique ID) of the record containing the current field
			    $f->fieldData = $fieldData;     // this is the data array of the current field
			    $f->fieldsData = $record->data;   // this is the data array of all fields
			    $f->record = $record;           // all the data for the record containing the current field
			    $f->microKey = $microKey;       // field name when processing a microTemplate field (only used when mode=="micro|")
			    $f->microIndex = $microIndex;   // sorting index when processing a microTemplate field (only used when mode=="micro|")
			    $f->editHistoryMode = $editHistoryMode;   // sorting index when processing a microTemplate field (only used when mode=="micro|")


			    //display the field
			    eval("\$html[] = " . $this->fieldTypes[$fieldType] . "::displayField(\$f);");
		    }

		    //close field div tag
		    if (!in_array($fieldType, $abstractFields)) {
			    if ($showWrapDiv) $html[] = '</div><!-- /.form-group -->';
		    }
	    }
	    else{
	    	$html[] = $l10n->getLabel('main','error').' '.$l10n->getLabel('backend','missing_datatype1').' '.$fieldName.' '.$l10n->getLabel('backend','missing_datatype2').' '.$recordClass ;
	    }
	    return implode(chr(10), $html);
    }

    /**
     * Returns the <label> to be shown in front of the field
     * @param $fieldData field specification array
     * @param string $recordMode Allows alternating label names between 'record|' and 'template|' so the forms manager can figure when collecting the data if he has to store the data in a record column or in a more complicate structure inside a template_data type field
     * @param $key unique key name for current field, like 'country'
     * @param $recordClassName class of the record holding the form / currently being edited
     * @param bool $addBootStrapClasses set to false if you need to get rid of class="col-lg-4 control-label" which is being inserted automatically in the <label> tag. Default is true.
     * @return string
     */
    static function getFieldLabel($fieldData, $recordMode, $key, $recordClassName, $addBootStrapClasses=true){
        global $l10n;
        $html = array();

        $dataType = explode(' ',trim($fieldData['dataType']));

        $mandatory = "";

        if(!isset($fieldData['editable']) || $fieldData['editable']==1){
            if(isset($fieldData['validation']['mandatory']) && $fieldData['validation']['mandatory'] == '1')$mandatory .= " <span class='mandatory_label'>*</span>";
        }
        if($dataType[0] != 'hidden'){


            $labelAttr = formsManager::getTagAttributes('label',$key, $fieldData);

            if($labelAttr['classes'] != ''){
                $class = (($addBootStrapClasses)?'class="col-lg-4 control-label'.$labelAttr['classes'].'"':'class="'.$labelAttr['classes'].'"');
            }
            else $class= (($addBootStrapClasses)?'class="col-lg-4 control-label"':'');

            $preLabelHTML = (isset($fieldData['preLabelHTML']))?$fieldData['preLabelHTML']:'';
            $postLabelHTML = (isset($fieldData['postLabelHTML']))?$fieldData['postLabelHTML']:'';
            $innerPreLabelHTML = (isset($fieldData['innerPreLabelHTML']))?$fieldData['innerPreLabelHTML']:'';
            $innerPostLabelHTML = (isset($fieldData['innerPostLabelHTML']))?$fieldData['innerPostLabelHTML']:'';

            //displaying label for data's field
            if($recordMode == 'template|' || $recordMode == 'micro|'){
                $html[] = $preLabelHTML.'<label '.$class.'for="'.$recordMode.$key.'"'.$labelAttr['attributes'].'>'.$innerPreLabelHTML.$l10n->getLabel('templates',$key).$mandatory.$innerPostLabelHTML.'</label>'.$postLabelHTML;
            }
            else{
                $html[] = $preLabelHTML.'<label '.$class.' for="'.$recordMode.$key.'"'.$labelAttr['attributes'].'>'.$innerPreLabelHTML.$l10n->getLabel($recordClassName,$key).$mandatory.$innerPostLabelHTML.'</label>'.$postLabelHTML;
            }
        }
        return implode(chr(10),$html);
    }



    /*
     * Allows modifying the value of a field to be displayed on the fly before showing it.
     * This does only work with the following field types : input, date, datetime, time, color, urlinput, checkbox, rich_text, textarera, files (in uneditable mod only for this last one)
     * IMPORTANT : this function must be called statically (i.e: you must specify it like 'formsManager::preDisplay')
     * @param string $key the key for the field in the form
     * @param string $value the value to be altered (notice altered value will be saved with the form. It can be altered back to its database value using function preRecord() )
     * @param string $fieldData the specification table for the key as retreived from the record class
     * @param string $additionalValue can be an additional value supplied by the the type of field, such as the file path for a file object
     * @return string the value
     */
    static function preDisplay($key,$value,$fieldData,$additionalValue=''){
        if(isset($fieldData['preDisplay']) && $fieldData['preDisplay'] != ''){
            global $mf;
            $x_end = ($mf->mode == 'frontend')?'frontend':'backend';
            $locale = $mf->info[$x_end.'_locale'];

            eval('$value = '.$fieldData['preDisplay'].'($key,$value,$locale,$additionalValue);');

            return $value;
        }
        else return $value;
    }

    /**
     * Checks the validity off all supplied fields and returns a populated record and a list of erroneous fields
     * The returned record is not stored in the database. It is your responsability to store the record after having executed this function.
     * This will not be required for simple website forms (dbForms), but it will be for database object's editing forms (dbRecords).
     * @param array $saveThoseFields specifies a list of fields to be checked and saved. Those no present in the table will be ignored. This is usefull when you need to do a partial saving of a record's form content.
     * @return array returns an array with the following structure :
     * array(
     *  record => an instance of the processed record, or 0 if the record failed to be processed
     *  errorFields => an array of the erroneous record keys names, all erroneous fields in the form ( 'key_name' => 'text_of_error_message' )
     * )
     *
     */

    function checkAndProcessForm($saveThoseFields = array()){

        global $mf,$l10n,$logger, $loggerA;

        //stores unvalid fields
        $erroneousFields = array();

        if(isset($_REQUEST['fm_action'])){
            $fmAction = $_REQUEST['fm_action'];

            //create an instance of the record class
            $record = new $_REQUEST['class']();
            $existingRecord = new $_REQUEST['class']();

            //load the record's existing data in it if it is an existing record
            if(isset($_REQUEST['uid']) && $_REQUEST['uid']!=''){
                $record->load(intval($_REQUEST['uid']));
                //the clone function can't be used safely here because values may be altered by a magic __clone() function inside the object, so it is much safer to load it twice
	            $existingRecord->load(intval($_REQUEST['uid']));
            }

            //check the selected template name and initialize the data structure inside 'template_content' with the data structure of this template
            if(isset($_REQUEST['record|template_name'])){
                // && ($record->data['template_name']['value'] != $_REQUEST['record|template_name'])
                //Always update the data structure in order to make sure it is up to date (in case the template definition has been modified manually)
                //save the existing 'template_content' data of the current record so we don't overwrite it while setting the new data structure
                $existingRecordContent = $record->data['template_content']['value'];
                //check the selected template name and initialize the data structure inside 'template_content' with the data structure of this template
                $currentTemplateName = $_REQUEST['record|template_name'];
                //then merge back the saved existing data in the updated data structure
                $record->data['template_content']['value'] = $this->mergeDatas($mf->templates[$currentTemplateName]['template_data'], $existingRecordContent);
            }

            $ignoreFieldValidation = (isset($_REQUEST['ignoreMandatoryFields']))?intval($_REQUEST['ignoreMandatoryFields']):0;

            //store all the submited values into a record that is not saved for now
            $submitedRecord = $this->processForm($record);

            //validate the submited values
            foreach($_REQUEST as $key => $value){
                $keyComp = explode('|',$key);

                if(count($keyComp)>1){
                    //if we have a composite parameter name, such as record_something or template_something
                    switch($keyComp[0]){
                        case 'record':
                            //echo $keyComp[1].chr(10);
                            $debug = (isset($record->data[$keyComp[1]]['validation']['debug']) && $record->data[$keyComp[1]]['validation']['debug']==true)?1:0;

                            $debugMsg = '';

                            if(isset($record->data[$keyComp[1]]['validation']) and !$ignoreFieldValidation){
                                if($debug)$debugMsg = 'Field='.$keyComp[1].chr(10).((($record->data[$keyComp[1]]['validation']['mandatory']))?' mandatory='.$record->data[$keyComp[1]]['validation']['mandatory']:'').chr(10);
                                //check field validity
                                if(isset($record->data[$keyComp[1]]['validation']['mandatory']) && $record->data[$keyComp[1]]['validation']['mandatory'] == '1'){
                                    $valid = true;

                                    // check value using supplied filter function
                                    if($valid && isset($record->data[$keyComp[1]]['validation']['filterFunc'])){

                                        $function = explode('->',rtrim($record->data[$keyComp[1]]['validation']['filterFunc'],'()'));
                                        $filterfunc = '';

                                        if($function[0]=='$this'){
                                            if(gettype($value)=="array") $value = implode(',',$value);

                                            $filterfunc = '$valid = $record->'.$function[1].'("'.$value.'", $submitedRecord, $existingRecord);';
                                            eval($filterfunc);
                                        }
                                        //pass value thrue checking function
                                        else {
                                            $filterfunc = '$valid = '.$record->data[$keyComp[1]]['validation']['filterFunc'].'($value, $submitedRecord, $existingRecord);';
                                            eval($filterfunc);
                                        }


                                        if($debug) {
                                            if (is_array($value)) {
                                                $debugMsg .= " value=" . print_r($value, true) . " filterFunc=\"" . $filterfunc . "\" ".chr(10);
                                            } else $debugMsg .= " value=" . $value . " filterFunc=\"" . $filterfunc . "\" ".chr(10);
                                        }

                                        //echo "filterfunc valid=".$valid;

                                        //if returned value is simply 'false'
                                        if($valid == false){
                                            //reject value and apply standard error message from the l10n file
                                            $erroneousFields[$keyComp[1]] = $l10n->getLabel($_REQUEST['class'], $keyComp[1].'_fail');
                                        }
                                        //in case a string is returned instead of 'true' or 'false', we have a custom error message
                                        else if(gettype($valid)=="string"){
                                            $erroneousFields[$keyComp[1]] = $valid;
                                        }
                                    }

                                    // check value using supplied list of forbidden values
                                    else if(isset($record->data[$keyComp[1]]['validation']['fail_values']) && gettype($record->data[$keyComp[1]]['validation']['fail_values']) == 'array'){

                                        if(in_array($value, $record->data[$keyComp[1]]['validation']['fail_values'],true)){
                                            //reject value
                                            $erroneousFields[$keyComp[1]] = $l10n->getLabel($_REQUEST['class'], $keyComp[1].'_fail');
                                            $valid = false;
                                        }
                                        if($debug){
                                            if(is_array($value)) {
                                                $debugMsg .= " value=".print_r($value,true)." pool=";

                                                $failValues = '';
                                                foreach($record->data[$keyComp[1]]['validation']['fail_values'] as $failValue){
                                                    if(is_array($failValue)) {
                                                        $failValues .= print_r($failValue,true).',';
                                                    }
                                                    else $failValues .= $failValue.',';
                                                }
                                                $failValues = rtrim($failValues,',');
                                                $debugMsg .= $failValues.chr(10);
                                            }
                                            else $debugMsg .= " value=".$value." pool=".implode(' , ',$record->data[$keyComp[1]]['validation']['fail_values']).chr(10);
                                        }
                                    }

                                    //check value against minimum entry count
                                    if(isset($record->data[$keyComp[1]]['validation']['min_entries']) && is_numeric($record->data[$keyComp[1]]['validation']['min_entries'])) {
                                        if(isset($value[0]) && count($value)==1  && trim($value[0]) == '')$entryCount=0;
                                        else $entryCount=count($value);

                                        if($debug) $debugMsg .= ' min_entries='.$record->data[$keyComp[1]]['validation']['min_entries'].' entry_count='.$entryCount.chr(10);
                                        if(is_array($value)){
                                            $valid = false;
                                            if($entryCount >= $record->data[$keyComp[1]]['validation']['min_entries'] && trim($value[0]) != ''){
                                                $valid = true;
                                            }
                                            else {
                                                $erroneousFields[$keyComp[1]] = $l10n->getLabel($_REQUEST['class'], $keyComp[1].'_fail');
                                                $valid = false;
                                            }
                                        }

                                    }

                                    //check value against maximum entry count
                                    if(isset($record->data[$keyComp[1]]['validation']['max_entries']) && is_numeric($record->data[$keyComp[1]]['validation']['max_entries'])) {
                                        if(count($value)==1  && trim($value[0]) == '')$entryCount=0;
                                        else $entryCount=count($value);

                                        if($debug) $debugMsg .= ' max_entries='.$record->data[$keyComp[1]]['validation']['max_entries'].' entry_count='.$entryCount.chr(10);
                                        if(is_array($value)){
                                            $valid = false;
                                            if($entryCount <= $record->data[$keyComp[1]]['validation']['max_entries'])$valid = true;
                                            else {
                                                $erroneousFields[$keyComp[1]] = $l10n->getLabel($_REQUEST['class'], $keyComp[1].'_fail');
                                                $valid = false;
                                            }
                                        }

                                    }
                                }
                                if($debug)echo "Debug : ".$debugMsg." Validation result = ".(($valid===true)?'success':'fail').chr(10).chr(10);
                            }
                            break;

                        case 'files':
                            if(isset($record->data[$keyComp[1]]['validation']) and !$ignoreFieldValidation){
                                //check field validity
                                if(isset($record->data[$keyComp[1]]['validation']['mandatory']) && $record->data[$keyComp[1]]['validation']['mandatory'] == 1){
                                    $valid = true;
                                    $entries = explode(',',$value);

                                    if(sizeof($entries)==0 || trim($entries[0])=='' || isset($record->data[$keyComp[1]]['validation']['min-entries']) && sizeof($entries) < intval($record->data[$keyComp[1]]['validation']['min-entries'])){
                                        //reject value
                                        $erroneousFields[$keyComp[1]] = $l10n->getLabel($_REQUEST['class'], $keyComp[1].'_fail');
                                        $valid = false;
                                    }

                                }
                            }
                            break;

                        //TODO : Check if fields are correctly verified in templates ?
                        case 'template':
                            //print_r($record->data['template_content']['value']);

                            //$record->data['template_content']['value'] = array_merge_recursive_distinct($mf->templates[$currentTemplateName]['template_data'],record->data['template_content']['value']);

                            if(isset($record->data[$keyComp[1]]['preRecord'])){
                                //process the value with the specified record processing function from the class
                                $function = explode('->',rtrim($record->data['template_content']['value'][$keyComp[1]]['preRecord'],'()'));
                                if($function[0]=='$this')
                                    eval('$processedValue =  $record->'.$function[1].'("'.$value.'","'.$record->data['template_content']['value'][$keyComp[1]]['value'].'");');
                                else eval('$processedValue = '.$record->data['template_content']['value'][$keyComp[1]]['preRecord'].'("'.$value.'","'.$record->data['template_content']['value'][$keyComp[1]]['value'].'");');

                                $record->data['template_content']['value'][$keyComp[1]]['value'] = $processedValue;

                                if ($record->data['template_content']['value'][$keyComp[1]]['dataType'] == 'rich_text') {
                                    $record->data['template_content']['value'][$keyComp[1]]['value'] = stripslashes($value);
                                } else {
                                    $record->data['template_content']['value'][$keyComp[1]]['value'] = $value;
                                }
                            }
                            //common case
                            else

                                //Fix
                                if (isset($record->data['template_content']['value'][$keyComp[1]]['dataType']) && $record->data['template_content']['value'][$keyComp[1]]['dataType'] == 'rich_text') {
                                    $record->data['template_content']['value'][$keyComp[1]]['value'] = stripslashes($value);
                                } else {
                                    $record->data['template_content']['value'][$keyComp[1]]['value'] = $value;
                                }
                            break;
                    }
                }
            }


        }
        //make a list of erroneous keys by tab
        $errorCountByTab = array();

        foreach($record->showInEditForm as $tabID => $tabKeys){
            $errorCountByTab[$tabID] = 0;
        }


        //for each erroneous field
        foreach($erroneousFields as $keyName => $errorMessage){
            //check within each tab against the list of keys displayed
            foreach($record->showInEditForm as $tabID => $tabKeys){
                if(count($saveThoseFields) > 0 ){
                    //when saving only certain fields, do not show errors for other fields aside tab names
                    if(in_array($keyName,$saveThoseFields) && in_array($keyName,$tabKeys))$errorCountByTab[$tabID] += 1;
                }
                else {
                    if(in_array($keyName,$tabKeys))$errorCountByTab[$tabID] += 1;
                }
            }
        }



        $result = array(
            'record' => $submitedRecord,
            'errorFields' => $erroneousFields,
            'errorCountByTab' => $errorCountByTab
        );

        return $result;
    }







    /**
     * Processes a form submission (requires the form to have been created using the formsManager or to comply with MindFlow form requirements)
     * The returned record is not stored in the database. It is your responsability to store the record after having executed this function.
     * This will not be required for simple website forms (dbForms), but it will be for database object's editing forms (dbRecords).
     * @param dbRecord $record speed optimization : if the record has already been loaded by the calling function (such as in checkAndProcessForm()), pass it as a parameter to prevent loading it again
     * @return the record loaded and filled with values from the form submited, 0 if failure.
     */

    function processForm($record = null){
        global $mf;

        if(isset($_REQUEST['fm_action'])){
            $fmAction = $_REQUEST['fm_action'];

            //create an instance of the record class
            if($record==null){
                $record = new $_REQUEST['class']();
                //load the record's existing data in it if it is an existing record
                if(isset($_REQUEST['uid']) && $_REQUEST['uid']!=''){
                    $record->load(intval($_REQUEST['uid']));
                }

                // requires AUTOLOAD of all classes
                //retreive the record / form object being processed from the session
                /*$formID = $_REQUEST['formID'];
                $record = $_SESSION['forms'][$formID]['record'];
                unset($_SESSION['forms'][$formID]);*/

            }

            //allows pre-processing the data in order to alter the data structure before injecting the submited data insite it.
	        $record->preProcessForm();

            //check the selected template name and initialize the data structure inside 'template_content' with the data structure of this template
            if(isset($_REQUEST['record|template_name'])){
                // && ($record->data['template_name']['value'] != $_REQUEST['record|template_name'])
                //Always update the data structure in order to make sure it is up to date (in case the template definition has been modified manually)

                //save the existing 'template_content' data of the current record so we don't overwrite it while setting the new data structure
                $existingRecordContent = $record->data['template_content']['value'];

                //check the selected template name and initialize the data structure inside 'template_content' with the data structure of this template
                $currentTemplateName = $_REQUEST['record|template_name'];

                //then merge back the saved existing data in the updated data structure
                $record->data['template_content']['value'] = $this->mergeDatas($mf->templates[$currentTemplateName]['template_data'], $existingRecordContent);
            }


            //update record's data
            foreach($_REQUEST as $key => $value){

                $keyComp = explode('|',$key);
                if(count($keyComp)>1){

                    //if we have a composite parameter name, such as record_something or template_something
                    switch($keyComp[0]){
                        case 'record':
                            /*echo "keyComp[1]=".$keyComp[1]."<br />
                            ";*/
                            if(isset($record->data[$keyComp[1]])){ //security fix : ignore and do not process keys submited but not defined in the record data array
                                $fieldClass = (isset($this->fieldTypes[$record->data[$keyComp[1]]['dataType']]))? $this->fieldTypes[$record->data[$keyComp[1]]['dataType']]:'';



                                if(isset($record->data[$keyComp[1]]['preRecord'])){
                                    //process the value with the specified record processing function from the class
                                    $function = explode('->',rtrim($record->data[$keyComp[1]]['preRecord'],'()'));

                                    if($function[0]=='$this'){

                                        $value1 = str_replace ( '$' , '\$' , $value);
                                        $value2 = str_replace ( '$' , '\$' , $record->data[$keyComp[1]]['value'] );
                                        eval('$processedValue =  $record->'.$function[1].'("'.$value1.'","'.$value2.'");');

                                    }
                                    else {

	                                    $value1 = str_replace ( '$' , '\$' , $value);
                                        $value2 = str_replace ( '$' , '\$' , $record->data[$keyComp[1]]['value'] );
	                                    eval('$processedValue =  '.$record->data[$keyComp[1]]['preRecord'].'("'.$value1.'","'.$value2.'");');

                                    }
                                    if(is_array($processedValue))$record->data[$keyComp[1]]['value'] = implode(',',$processedValue);
                                    else $record->data[$keyComp[1]]['value'] = $processedValue;
                                }
                                //common case
                                else {
                                    if(is_array($value))$record->data[$keyComp[1]]['value'] = implode(',',$value);
                                    else $record->data[$keyComp[1]]['value'] = $value;
                                }

                                //run formFields onSave() methods
                                if( is_subclass_of($fieldClass,'formField')) {
                                    $formField = new $fieldClass;
                                    $record->data[$keyComp[1]]['value'] = $formField->onSave($record->data[$keyComp[1]],$record->data[$keyComp[1]]['value']);
                                }
                            }
                            break;
                        case 'template':
                            //echo "processing key = ".$keyComp[1]."  \n\r";
                            if(isset($record->data['template_content']['value'][$keyComp[1]])){ //security fix : ignore and do not process keys submited but not defined in the record data array
                                if(isset($record->data['template_content']['value'][$keyComp[1]]['preRecord'])){
                                    //process the value with the specified record processing function from the class
                                    $function = explode('->',rtrim($record->data['template_content']['value'][$keyComp[1]]['preRecord'],'()'));
                                    if($function[0]=='$this')
                                        eval('$processedValue =  $record->'.$function[1].'("'.$value.'","'.$record->data['template_content']['value'][$keyComp[1]]['value'].'");');
                                    else eval('$processedValue =  '.$record->data['template_content']['value'][$keyComp[1]]['preRecord'].'("'.$value.'","'.$record->data['template_content']['value'][$keyComp[1]]['value'].'");');
                                    $record->data['template_content']['value'][$keyComp[1]]['value'] = $processedValue;
                                }
                                //common case

                                else {

                                    $record->data['template_content']['value'][$keyComp[1]]['value'] = $value;
                                }
                            }
                            break;
                    }

                }
            }


            //verify unchecked checkboxes or radio buttons which are not transmitted with the form
            /*foreach($record->data as $key => $value){
                //check for key presence in the form
                foreach($record->showInEditForm as $tab => $keys){
                    if(in_array($key,$keys)){
                        //key is displayed
                        if(isset($record->data[$key]['dataType']) && ($record->data[$key]['dataType']=='checkbox' || $record->data[$key]['dataType']=='radio')){
                            if(!isset($_REQUEST['record|'.$key])){
                                //if the field name was not found in the request, then the checkbox or radio is not checked
                                $record->data[$key]['value']=0;
                            }
                        }
                    }
                }
            }*/

            //verify unchecked checkboxes or radio buttons which are not transmitted with the form for template data
            /*if(isset($record->data['template_content']['value'])){
                $tplShowInEditForm = explode(',',$mf->templates[$currentTemplateName]['showInEditForm']);

                //echo "t1=".isset($record->data['template_content']['value']).'\n';
                foreach($record->data['template_content']['value'] as $key => $value){

                    //check for key presence in the form
                    if(in_array($key,$tplShowInEditForm)){
                        //key is displayed
                        if(isset($record->data['template_content']['value'][$key]['dataType']) && ($record->data['template_content']['value'][$key]['dataType']=='checkbox' || $record->data['template_content']['value'][$key]['dataType']=='radio')){
                            //echo "isset(\$_REQUEST['template|'.$key])=".isset($_REQUEST['template|'.$key]);
                            if(!isset($_REQUEST['template|'.$key])){
                                //if the field name was not found in the request, then the checkbox or radio is not checked
                                $record->data['template_content']['value'][$key]['value']=0;
                            }
                        }
                    }

                }
            }*/

            $record->postProcessForm();
//print_r($record);
            return $record;

        }
        else return 0;
    }

















    /**
     * Retreives the the various attributes assigned to a given tag and separates the class value from the other attributes into an array so they are ready to insert into a form tag
     * @param $tag the tag identifier, such as 'div', 'label', or 'field' (this latest is for inputs)
     * @param $key
     * @param $fieldData
     * @return array $result['classes'] contains the class value (ex : 'col-lg-3') specified and $result['attributes'] contains the concatenated tag attributes arguments (ex: 'onclick="alert(\'hello\'); rel="external" '
     */
    static function getTagAttributes($tag, $key, $fieldData){
        $attr = array();
        if($tag=='div') $attr['classes'] = $key;
        else $attr['classes'] = '';
        $attr['attributes'] = '';

        //attributes for input field
        if(isset($fieldData[$tag.'_attributes'])){
            foreach($fieldData[$tag.'_attributes'] as $attrName => $attrValue){
                if($attrName == 'class') $attr['classes'] .= ' '.$attrValue;
                else if($attrValue!='')$attr['attributes'] .= ' '.$attrName.'="'.$attrValue.'"';
                else $attr['attributes'] .= ' '.$attrName;
            }
        }
        return $attr;
    }


    private function getRowAttributes($key, $fieldData){
        $attr = array();
        $attr['classes'] = ''; //$key;
        $attr['attributes'] = '';

        //attributes for input field
        if(isset($fieldData['row_attributes'])){
            foreach($fieldData['row_attributes'] as $attrName => $attrValue){
                if($attrName == 'class') $attr['classes'] .= ' '.$attrValue;
                else if($attrValue!='')$attr['attributes'] .= ' '.$attrName.'="'.$attrValue.'"';
                else $attr['attributes'] .= ' '.$attrName;
            }
        }
        return $attr;
    }



        //utility function to recursively merge two arrays with overwrites.
    private function mergeDatas($templateArray, $recordArray)
    {
        foreach($recordArray as $key => $Value)
        {
            if(array_key_exists($key, $templateArray) && is_array($Value))
                //only merge the data value so the datatype from the template is preserved and modifications of the datatype can be reflected
                $templateArray[$key]['value'] = $recordArray[$key]['value'];

            else
                $templateArray[$key] = $Value;

        }

        return $templateArray;

    }

    /**
     * Security function. Verifies the 'sec'/'fields' combo of a request in order to make sure the form hasn't been altered
     * Will return false if the value of the 'fields' value is empty. It is recommended to always supply an 'action' field so the value of the sec/fields combo can not be reused for attacking other available actions.
     * @return bool true if validation succeeds, false if the form was altered
     */
    static function isRequestSecured(){
        //return (checkSec());
        if (isset($_REQUEST['sec']) && isset($_REQUEST['fields']) ){
            if($_REQUEST['fields'] != '') return formsManager::checkSecValue($_REQUEST['sec'],explode(',',$_REQUEST['fields']));
            else return false;
        }
    }


    /*
     * Security function. Verifies a set of fields in a request in order to make sure the form hasn't been altered
     * @param $secHash string The security hash that has been retreived with the form, usualy contained in the field named 'sec'
     * @param $secKeys array The list of fields for which their values be concatenated in the encrypted secKey, with the key of the field given as index.
     * IMPORTANT : These values must not change between the form generation and the submission validation
     * The total length of the concatenated values should not exceed 62 characters. Longer values will work, but security is not guaranted, as characters beside the 72th will be ignored during the hash match.
     * @return bool true if validation succeeds, false if the form was altered
     */
    static function checkSecValue($secHash, $secKeys, $debug=false){
        global $config;

	    // Warning : never echo $secString in a public web page, as it will reveal your salt
	    // if you do so, you'd better add a display condition matching your remote IP adress in front if($_SERVER['REMOTE_ADDR']=='xx.xxx.xxx.xx')
	    $secString = $config['salt'];

        if($debug)$debugArray=array();

        foreach($secKeys as $key){
            if(isset($_REQUEST[$key])){
            	$secString .= $_REQUEST[$key];
	            if($debug) $debugArray[$key] = $_REQUEST[$key];
            }
			else throw new Exception('formsManager.php checkSecValue() : field '.$key.' is missing in the request. checkSecValue() fails.');
        }

        if($debug) {
		    echo "<br>" . chr( 10 )."formsManager.php checkSecValue() debug :"."<br>" . chr( 10 ).print_r( $debugArray , true)."<br>" . chr( 10 );

		    echo "expected sec=" . self::makeSecValue( $debugArray ) . "<br>" . chr( 10 );
		    echo "given sec hash=" . $secHash . "<br>" . chr( 10 );
	    }
        return (hash('sha256',$secString) == $secHash);
    }

    /*
     * Security function. Generates the value to put 'sec' field of a form in order to make sure the form hasn't been altered
     * @param $lockedKeys array The list of fields for which their values be checked in the encrypted secKey, with the key of the field given as index.
     * IMPORTANT : These values must not change between the form generation and the submission validation
     * The total length of the concatenated values should not exceed 62 characters. Longer values will work, but security is not guaranted, as characters beside the 72th will be ignored during the hash match.
     * Exemple value : array('action' => 'edit', 'uid' => 13);
     * @return string the security hash to include in your form, conventionally in a field named 'sec'
     */
    static function makeSecValue($lockedKeys, $debug=false){
        global $config;

	    $secString = $config['salt'];

	    foreach($lockedKeys as $key => $value){
            $secString .= $value;
        }

		if($debug) {
			echo " make=" . print_r( $lockedKeys, true ) . '<br />' . chr( 10 );
			// Warning : never echo $secString in a public web page, as it will reveal your salt
			// if you do so, you'd better add a display condition matching your remote IP adress in front
			//if($_SERVER['REMOTE_ADDR']=='xx.xxx.xxx.xx') echo " make=" . $secString . '<br />' . chr( 10 );
			echo " make returns =" . hash( 'sha256', $secString ) . '<br />' . chr( 10 );
		}
        return hash('sha256',$secString);
    }


    /**
     *  Adds a CSS HTML tag to the current form
     * @param $uniqueKey string a unique identifier for the tag. If the addCss() method is called twice with the same $uniqueKey, the tag will be output once. But if the $uniqueKey is changed, incremented for exemple, both tags will be output
     * @param $cssTag string the full css tag, such as '<link rel="stylesheet" href="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />'
     */
    function addCss($uniqueKey, $cssTag){
        $this->cssList[$uniqueKey]= $cssTag;
    }

    /**
     *  Adds a script tag to the top of the rendered pages
     * @param $uniqueKey string a unique identifier for the tag. If the addTopJs() method is called twice with the same $uniqueKey, the tag will be output once. But if the $uniqueKey is changed, incremented for exemple, both tags will be output
     * @param $jsTag string the full js tag, such as '<script src="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>'
     */
    function addTopJs($uniqueKey,$jsTag){
        $this->topJsList[$uniqueKey]= $jsTag;
    }

    /**
     *  Adds a script tag to the bottom of the rendered pages
     * @param $uniqueKey string a unique identifier for the tag. If the addBottomJs() method is called twice with the same $uniqueKey, the tag will be output once. But if the $uniqueKey is changed, incremented for exemple, both tags will be output
     * @param $jsTag string the full js tag, such as '<script src="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>'
     */
    function addBottomJs($uniqueKey,$jsTag){
        $this->bottomJsList[$uniqueKey]= $jsTag;
    }


    /**
     *  Adds a piece of javascript (DO NOT surround it with <script> tags) to execute before a form is sent. This is especially usefull for for formFields wich can run their custom JS code before form submission
     * @param $uniqueKey string a unique identifier for the tag. If the addPriorSendFormJs() method is called twice with the same $uniqueKey, the tag will be output once. But if the $uniqueKey is changed, incremented for exemple, both tags will be output
     * @param $jsCode string custom field javascript processing code to add to the sendForm() method of forms
     */
    function addPriorSendFormJs($uniqueKey,$jsCode){
        $this->priorSendFormJS[$uniqueKey]= $jsCode;
    }


    /**
     *  Adds a piece of javascript (DO NOT surround it with <script> tags) to execute after a form has been sent. This is especially usefull for for formFields wich can run their custom JS code after form submission
     * @param $uniqueKey string a unique identifier for the tag. If the addPriorSendFormJs() method is called twice with the same $uniqueKey, the tag will be output once. But if the $uniqueKey is changed, incremented for exemple, both tags will be output
     * @param $jsCode string custom field javascript processing code to add to the sendForm() method of forms
     */
    function addPostSendFormJs($uniqueKey,$jsCode){
        $this->postSendFormJS[$uniqueKey]= $jsCode;
    }

}
