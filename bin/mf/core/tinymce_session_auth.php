<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
//require_once DOC_ROOT.SUB_DIR.'/mf/core/records/mfUser.php';



//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();
$mf = $mindFlowBackend->mf;
//$l10n = $mf->l10n;

//echo("locale=".$_SESSION['current_be_user']->data['backend_locale']."<br />");
//echoValue('locale',$mf->getLocale());
//print_r($_SESSION);

if(isset($_SESSION['current_be_user'])){
    $_SESSION['isLoggedIn'] = true;
    $_SESSION['mc_path'] = '../../../..'.$mf->getUploadsDir().DIRECTORY_SEPARATOR.'fichiers';
    $_SESSION['mc_rootpath'] = '../../../..'.$mf->getUploadsDir().DIRECTORY_SEPARATOR.'fichiers';

    //make sure the files dir exists and create it if not available
    $filesDir = $_SERVER['DOCUMENT_ROOT'].$mf->getUploadsDir().DIRECTORY_SEPARATOR.'fichiers';
    if (!is_dir ($filesDir)) {
        mkdir($filesDir, 0775);
    }

    $_SESSION['imagemanager.general.language'] = $mf->getLocale(); //$_SESSION['current_be_user']->data['backend_locale'];
    $_SESSION['filemanager.general.language'] = $mf->getLocale(); //$_SESSION['current_be_user']->data['backend_locale'];

    // Redirect
    $url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_REQUEST['return_url'] : "http://".$_SERVER['SERVER_NAME'].$_REQUEST['return_url'];

    header("location: " . $url);
}
else {
    $_SESSION['isLoggedIn'] = false;
    $_SESSION['mc_path'] = 'files';
    $_SESSION['mc_rootpath'] = 'files';
    die('TinyMCE Auth Error : user not identified.');
}

