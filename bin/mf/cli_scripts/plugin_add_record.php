<?php

/**
 * This wizards assists you in creating a new record.
 * It is possible to specify in the command line a config file featuring the preconfigured used variables in order to avoid having to seize them at every run when configuring the wizard.
 * In this cas, call the script using the following syntax :
 *
 * php plugin_add_record.php config_file.php
 *
 * See file sample-configs/agPlugin_config.php for the available variables to configure.
 */

//http://php.net/manual/en/features.commandline.php

require_once "colors.php";
require_once "functions.php";

// Create new Colors class
$colors = new Colors();

if(isset($argv[1])){
    echo chr(10);
    if(is_file ( $argv[1] )) {
        echo $colors->getColoredString("config file specified, loading defaults from the file...", 'cyan', null);
        include $argv[1];
        echo $colors->getColoredString("done", 'cyan', null).chr(10).chr(10);
    }
    else echo $colors->getColoredString("Specified config.php file not found, running 100% interactive mode.", 'dark_gray', null).chr(10).chr(10);
}



echo "Natural name of the record you want to create ? [My record] : ";
$prefix = chr(10);
if(!isset($recordNaturalName)){
    $recordNaturalName = trim(fgets(STDIN));
    $prefix = '';
}
if($recordNaturalName=='')$recordNaturalName = 'My record';

echo $prefix.$colors->getColoredString("Record will be named \"".$recordNaturalName."\"", 'cyan', null).chr(10).chr(10);

//suggest class name from moduleNaturalName
$nameChunks = explode(' ',$recordNaturalName);
$isFirst=true;
$suggestedName = '';
foreach($nameChunks as $chunk){
    if($isFirst){
        $suggestedName = strtolower($chunk);
        $isFirst=false;
    }
    else $suggestedName .= ucfirst($chunk);
}
$suggestedName=trim($suggestedName);

echo "Class name of the record you want to create ? [".$suggestedName."] : ";
$prefix = chr(10);
if(!isset($recordClassName)){
    $recordClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($recordClassName=='')$recordClassName = $suggestedName;
echo $prefix.$colors->getColoredString("Record class will be named \"".$recordClassName."\"", 'cyan', null).chr(10).chr(10);


echo "SQL table name where the records will be stored ? [my_record] : ";
$prefix = chr(10);
if(!isset($SQLTableName)){
    $SQLTableName = trim(fgets(STDIN));
    $prefix = '';
}
if($SQLTableName=='')$SQLTableName = $suggestedName;
echo $prefix.$colors->getColoredString("Record table name will be \"".$SQLTableName."\"", 'cyan', null).chr(10).chr(10);



echo "In the record definition, what will be the column name of the record's title field ? [title] : ";
$prefix = chr(10);
if (!isset($recordTitleField)) {
    $recordTitleField = trim(fgets(STDIN));
    $prefix = '';
}
if ($recordTitleField == '') $recordTitleField = 'title';

echo $prefix . $colors->getColoredString("Record's title field will be \"" . $recordTitleField . "\"", 'cyan', null) . chr(10) . chr(10);





echo "Base ISO2 locale for localized files ? [en] : ";
$prefix = chr(10);
if(!isset($defaultLocale)){
    $defaultLocale = trim(fgets(STDIN));
    $prefix = '';
}
if($defaultLocale=='')$defaultLocale = 'en';

echo $prefix.$colors->getColoredString("Plugin default locale will be \"".$defaultLocale."\"", 'cyan', null).chr(10).chr(10);

echo "Folder name of the website where the record should be created (in /mf_websites) [default] : ";
$prefix = chr(10);
if(!isset($websiteFolderName)){
    $websiteFolderName = trim(fgets(STDIN));
    $prefix = '';
}
if($websiteFolderName=='')$websiteFolderName = 'default';
echo $prefix.$colors->getColoredString("Record will be created in /mf_websites/".$websiteFolderName.'/plugins', 'cyan', null).chr(10).chr(10);

echo "Class name of the plugin holding the record ? [myPlugin] : ";
$prefix = chr(10);
if(!isset($pluginClassName)){
    $pluginClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginClassName=='')$pluginClassName = 'myPlugin';
echo $prefix.$colors->getColoredString("Plugin class will be named \"".$pluginClassName."\"", 'cyan', null).chr(10).chr(10);


echo "Folder name / plugin key of the plugin holding the record ? [$pluginClassName] : ";
$prefix = chr(10);
if(!isset($pluginDirName)){
    $pluginDirName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginDirName=='')$pluginDirName = $pluginClassName;
$pluginKey = $pluginDirName;

echo $prefix.$colors->getColoredString("Will create record in plugin folder /mf_websites/".$websiteFolderName."/plugins/".$pluginDirName, 'cyan', null).chr(10).chr(10);





//does this module lists any record ?
if(!isset($moduleWillListRecords)) {
    $answer = "";
    $acceptedValues = array('y', 'n', 'yes', 'no');
    $moduleWillListRecords = false;
    while (!in_array($answer, $acceptedValues)) {
        echo "Add this record for listing / edition in an existing module ? [yes/no] : ";
        $answer = trim(fgets(STDIN));
    }
    if ($answer == 'y' || $answer == 'yes') {
        $moduleWillListRecords = true;
    }
}




if($moduleWillListRecords) {

    echo "Class name of the module where the record should be displayed ? [myModule] : ";
    $prefix = chr(10);
    if(!isset($moduleClassName)){
        $moduleClassName = trim(fgets(STDIN));
        $prefix = '';
    }
    if($moduleClassName=='')$moduleClassName = 'myModule';
    echo $prefix.$colors->getColoredString("Module class will be \"".$moduleClassName."\"", 'cyan', null).chr(10).chr(10);



    echo "Enter a create record action label [New My record] : ";
    $prefix = chr(10);
    if (!isset($createRecordLabel)) {
        $createRecordLabel = trim(fgets(STDIN));
        $prefix = '';
    }
    if ($createRecordLabel == '') $createRecordLabel = 'New My record';

    echo $prefix . $colors->getColoredString("Create record action label will be \"" . $createRecordLabel . "\"", 'cyan', null) . chr(10) . chr(10);



    echo "Enter an edit record action label [Edit My record] : ";
    $prefix = chr(10);
    if (!isset($editRecordLabel)) {
        $editRecordLabel = trim(fgets(STDIN));
        $prefix = '';
    }
    if ($editRecordLabel == '') $editRecordLabel = 'Edit My record';

    echo $prefix . $colors->getColoredString("Edit record action label will be \"" . $editRecordLabel . "\"", 'cyan', null) . chr(10) . chr(10);
}

$answer = "";
$acceptedValues = array('y','n','yes','no');
while (!in_array($answer, $acceptedValues)){
    echo "Will now create record files, ready to write ? [yes/no] : ";
    $answer = trim(fgets(STDIN));
}
if($answer=='y' || $answer=='yes'){
    echo chr(10).$colors->getColoredString("Now writing files...", 'green', null);

    //value for moving to mf root dir
    $chdir = "../.." ;

    //create records folder
    echo chr(10).$colors->getColoredString("Creating record directory ... ", 'green', null);
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName."/records");
    echo $colors->getColoredString("Done.", 'green', null);

    //create record php file
    echo chr(10).$colors->getColoredString("Creating record class file ... ", 'green', null);
    $pluginContent = file_get_contents ("ressources/plugin_template/records/myRecord.php");
    $pluginContent = str_replace("{recordClassName}",$recordClassName,$pluginContent);
    $pluginContent = str_replace("{pluginDirName}",$pluginDirName,$pluginContent);
    $pluginContent = str_replace("{pluginKey}",$pluginKey,$pluginContent);
    $pluginContent = str_replace("{recordTitleField}",$recordTitleField,$pluginContent);
    $pluginContent = str_replace("{SQLTableName}",$SQLTableName,$pluginContent);

    //finally, write plugin file
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/records/'.$recordClassName.'.php',$pluginContent);
    echo chr(10).$colors->getColoredString("Done.", 'green', null);

    //create language folder
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName."/languages/records");

    //create the record's l10n file
    $l10nRecordContent = file_get_contents ("ressources/plugin_template/languages/records/myRecord_l10n.php");
    $l10nRecordContent = str_replace("{defaultLocale}",$defaultLocale,$l10nRecordContent);
    if(isset($moduleNaturalName))$l10nRecordContent = str_replace("{moduleNaturalName}",str_replace('"','\"',$moduleNaturalName),$l10nRecordContent);
    $l10nRecordContent = str_replace("{recordTitleField}",str_replace('"','\"',$recordTitleField),$l10nRecordContent);
    $l10nRecordContent = str_replace("{recordNaturalName}",str_replace('"','\"',$recordNaturalName),$l10nRecordContent);

    writeFile($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/records/'.$recordClassName.'_l10n.php',$l10nRecordContent);


    //declare/load record into plugin's index.php
    $indexContent = file_get_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/index.php');
    $loadRecordContent = file_get_contents ("ressources/plugin_template/sections/index_load_record.php");

    $loadRecordContent = str_replace("{pluginKey}",$pluginKey,$loadRecordContent);
    $loadRecordContent = str_replace("{recordClassName}",$recordClassName,$loadRecordContent);
    $loadRecordContent = str_replace("{pluginDirName}",$pluginDirName,$loadRecordContent);

    echo chr(10).$colors->getColoredString("Inserting load record directive in ".$pluginDirName.'/index.php', 'green', null);
    $indexContent = setSection("index_load_record","load_record_".$recordClassName,$loadRecordContent,$indexContent, chr(9).chr(9));

    //write plugin index file
    file_put_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/index.php', $indexContent);

    //add record listing
    if($moduleWillListRecords) {
        echo chr(10).$colors->getColoredString("The module will list records", 'green', null);
        echo chr(10).$colors->getColoredString("Inserting list content directives in ".$moduleClassName.'.php', 'green', null);
        echo chr(10).$colors->getColoredString("Updating module ".$chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php', 'green', null);

        $moduleContent = file_get_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php');

        //update //[/module_list_backend] section
        //add record list switch statement in function prepareData
        $moduleListBackend = file_get_contents ("ressources/plugin_template/sections/module_list_backend.php");
        //update //[/module_list_backend] section
        $moduleContent = setSection("module_list_backend","module_list_backend",$moduleListBackend,$moduleContent, chr(9).chr(9).chr(9).chr(9).chr(9));

        //add record listing function to the module's php file
        $moduleListFunction = file_get_contents ("ressources/plugin_template/sections/module_list_function.php");
        $moduleListFunction = str_replace("{recordClassName}",$recordClassName,$moduleListFunction);
        $moduleListFunction = str_replace("{recordNaturalName}",$recordNaturalName,$moduleListFunction);
        $moduleListFunction = str_replace("{moduleClassName}",$moduleClassName,$moduleListFunction);
        $moduleListFunction = str_replace("{recordTitleField}",$recordTitleField,$moduleListFunction);
        $moduleListFunction = str_replace("{pluginKey}",$pluginKey,$moduleListFunction);
        $moduleListFunction = str_replace("{SQLTableName}",$SQLTableName,$moduleListFunction);

        $moduleContent = setSection('module_list_function',"module_list_function_".$recordClassName,$moduleListFunction,$moduleContent, chr(9));

        echo chr(10).$colors->getColoredString("Record listing added.", 'green', null).chr(10).chr(10);

        //write the module file
        writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php',$moduleContent);
        echo chr(10).$colors->getColoredString("Done.", 'cyan', null);


        //update the module's l10n file
        echo chr(10).$colors->getColoredString("Updating l10n file : ".$chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/languages/modules/'.$moduleClassName.'_l10n.php'.' ... ', 'green', null);

        $l10nFileContent = file_get_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/languages/modules/'.$moduleClassName.'_l10n.php');

        $l10nModuleContent = file_get_contents ("ressources/plugin_template/sections/l10n_module_main.php");
        $l10nModuleContent = str_replace("{createRecordLabel}",str_replace('"','\"',$createRecordLabel),$l10nModuleContent);
        $l10nModuleContent = str_replace("{editRecordLabel}",str_replace('"','\"',$editRecordLabel),$l10nModuleContent);
        $l10nFileContent = setSection('l10n_module_main',"l10n_module_main_".$recordClassName,$l10nModuleContent,$l10nFileContent, chr(9).chr(9));
        writeFile($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/modules/'.$moduleClassName.'_l10n.php',$l10nFileContent);
        echo chr(10).$colors->getColoredString("Done.", 'green', null);



    }


}
else echo chr(10).$colors->getColoredString("Aborting write. Done.", 'red', null).chr(10).chr(10);

echo chr(10);