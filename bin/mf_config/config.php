<?php

/*
   Copyright 2017 Alban Cousini�

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

    //ini_set("memory_limit",'512M');

    error_reporting(E_ALL | E_STRICT);

    //figure if MindFlow is being run in CLI mode or not.
    define("CLI_MODE",(php_sapi_name() === 'cli'));
    define("WEB_MODE",!CLI_MODE);

    //only super frequently used values are defined as constants
    if(WEB_MODE) define("DOC_ROOT",     $_SERVER['DOCUMENT_ROOT']);//server document root path in web mode
    else {
        //TODO : it would be more reliable to extract the website dir form dirname( __FILE__ )
        //execution document root path in CLI mode
        $mfDirectory = strstr(getcwd(), '/mf/', true);
        if($mfDirectory=='')$mfDirectory = strstr(getcwd(), '/mf_websites/', true);
        define("DOC_ROOT", $mfDirectory);

    }

    //adjust subdir value if the CMS is not located immediately below the document root
    define("SUB_DIR", '');

    //Mindflow version
    $GLOBALS['mf_version'] = 'v1.9.4';

    //site locales
    $GLOBALS['site_locales'] = array(
        'fr' => array(
            'complete' => 'fr_FR',              //complete locale string
            'php_date_format' => 'd/m/Y',
            'picker_date_format' => 'DD/MM/YYYY', //according to moments.js dateformats (http://momentjs.com/docs/)

            'php_datetime_format' => 'd/m/Y H:i:s',
            'php_datetime_ms_format' => 'd/m/Y H:i:s.u',
            'picker_datetime_format' => 'DD/MM/YYYY HH:mm:ss', //according to moments.js dateformats (http://momentjs.com/docs/)

            'php_time_format' => 'H:i:s',
            'picker_time_format' => 'HH:mm:ss', //according to moments.js dateformats (http://momentjs.com/docs/)
        ),
        'en' => array(
            'complete' => 'en_US',
            'php_date_format' => 'd/m/Y',
            'picker_date_format' => 'DD/MM/YYYY',

            'php_datetime_format' => 'd/m/Y H:i:s',
            'php_datetime_ms_format' => 'd/m/Y H:i:s.u',
            'picker_datetime_format' => 'DD/MM/YYYY HH:mm:ss',

            'php_time_format' => 'H:i:s',
            'picker_time_format' => 'HH:mm:ss',
        ),
        'de' => array(
            'complete' => 'de_DE',
            'php_date_format' => 'd/m/Y',
            'picker_date_format' => 'DD/MM/YYYY',

            'php_datetime_format' => 'd/m/Y H:i:s',
            'php_datetime_ms_format' => 'd/m/Y H:i:s.u',
            'picker_datetime_format' => 'DD/MM/YYYY HH:mm:ss',
            /*'php_date_format' => 'd.m.Y',
            'picker_date_format' => 'DD.MM.YYYY',

            'php_datetime_format' => 'd.m.Y H:i:s',
            'php_datetime_ms_format' => 'd.m.Y H:i:s.u',
            'picker_datetime_format' => 'DD.MM.YYYY HH:mm:ss',*/

            'php_time_format' => 'H:i:s',
            'picker_time_format' => 'HH:mm:ss',
        ),
		'it' => array(
            'complete' => 'it_IT',
            'php_date_format' => 'd/m/Y',
            'picker_date_format' => 'DD/MM/YYYY',

            'php_datetime_format' => 'd/m/Y H:i:s',
            'php_datetime_ms_format' => 'd/m/Y H:i:s.u',
            'picker_datetime_format' => 'DD/MM/YYYY HH:mm:ss',

            'php_time_format' => 'H:i:s',
            'picker_time_format' => 'HH:mm:ss',
        ),
    );


    if(isset($_SERVER['HTTP_HOST'])){ //condition for CLI scripts
        switch($_SERVER['HTTP_HOST']){

            case 'www.yourwebsite.com':
                require_once DOC_ROOT.SUB_DIR.'/mf_websites/www.yourwebsite.com/config-dev.php';
                break;

            case 'mftest.cousinie.net':
                require_once DOC_ROOT.SUB_DIR.'/mf_websites/mftest.cousinie.net/config-dev.php';
                break;

            default:
                require_once DOC_ROOT.SUB_DIR.'/mf_websites/www.sample-website.com/config-dev.php';
        }
    }


?>
