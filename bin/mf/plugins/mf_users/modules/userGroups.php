<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_users/records/mfUser.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_users/records/mfUserGroup.php';
//require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_users/forms/usersForm.php';

class userGroups implements module
{
    private $moduleKey = 'userGroups';
    private $moduleType = 'backend';
    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function __construct(){

    }


    function prepareData(){
        global $mf,$l10n;
        $this->pageOutput = '';

        if($mf->mode == 'frontend'){

            //$currentPageUid = $mf->info['currentPageUid'];

        }
        else if($mf->mode == 'backend'){

            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('mf_users','allowEditUserGroups')==1)));


            if($this->moduleAccess){
                    if(!$mf->pluginManager->backendMenuExists('administration'))$mf->pluginManager->addBackendMenu('administration', LOOSE_PRIORITY);
                    //add the plugin to the backend menus
                    $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=userGroups"><span class="glyphicon glyphicon-menu glyphicon-user" aria-hidden="true"></span>'.$l10n->getLabel('userGroups','menu_title').'</a>','administration', LOW_PRIORITY);

                    if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                        $this->localMenu ='<li rel="'.$this->moduleKey.'" class="active"><a href="{subdir}/mf/index.php?module='.$this->moduleKey.'">{menu-title}</a></li>';


                        //breadcrumb
                        $this->section = '';//$l10n->getLabel('backend','administration');
                        $this->moduleName = "<span class=\"glyphicon glyphicon-menu glyphicon-user\" aria-hidden=\"true\"></span>".$l10n->getLabel('userGroups','menu_title');
                        $this->subModuleName = "";

                        if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                        switch($this->action){
/*

                            case "create":
                                $this->pageOutput .= '<h3>'.$l10n->getLabel('userGroups','new_record').'</h3>';
                                $this->pageOutput .= $this->createUserGroup();
                                break;

                            case "edit":
                                $this->pageOutput .= '<h3>'.$l10n->getLabel('userGroups','edit_record').'</h3>';
                                //case edit requested by some module as a GET url parameter
                                if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                    $this->pageOutput .= $this->editUserGroup(intval($_GET['uid']));
                                }
                                //case we just re-edit the last saved record
                                else {
                                    $this->pageOutput .= $this->editUserGroup($currentRecordUid);
                                }
                                break;


                            case "delete":
                                $this->pageOutput .= $this->deleteUserGroup($_REQUEST['uid']);
                                break;
*/
                            case "list":
                            default:

                                $this->pageOutput .= $this->listRecords('','');
                                break;
                        }
                    }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
                $this->localMenu = '';
            }

        }
    }

    function render(&$mainTemplate){
        global $mf,$l10n;

        if($mf->mode == 'backend'){

            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/mf_users/ressources/templates/module.html');
                $moduleBody = str_replace("{module-body}",$this->pageOutput,$moduleBody);

                $moduleBody = str_replace("{local-menu}",$this->localMenu,$moduleBody);
                $moduleBody = str_replace("{menu-title}",$l10n->getLabel('userGroups','menu_title'),$moduleBody);

                $mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);
                //breadcrumb
                $mainTemplate = str_replace("{section}",$this->section,$mainTemplate);
                $mainTemplate = str_replace("{module-name}",$this->moduleName,$mainTemplate);
                $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);

            }

        }
        else if($mf->mode == 'frontend'){}
    }


    function getType(){
        return $this->moduleType;
    }



    function listRecords($sqlConditions = '', $dateConditions = '',  $deletedCondition=' AND deleted=0'){

        global $mf,$l10n;

        $GroupeDeSecurite = $mf->currentUser->data['user_group']['value'];
        $user = new mfUserGroup();

        $buttons = '';

       /* $secKeys = array('action' => 'createRecord', 'record_class' => 'mfUserGroup');
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));
        //only admin users can create userGroups in this module
        //if($mf->currentUser->data['user_group']['value'] > ADMINISTRATEUR)
            $buttons .= '<button class="btn-sm btn-primary mf-btn-new" type="button" onClick="openRecord(\'{subdir}/mf/core/ajax-core-html.php?action=createRecord&record_class=mfUserGroup&recordEditTableID=userGroups&header=0&footer=0&sec='.$secHash.'&fields='.$secFields.'&ajax=1&showSaveButton=1&showSaveCloseButton=1&showPreviewButton=&showCloseButton=1&mode='.$mf->mode.'\',\'70%\',\'\',\''.$l10n->getLabel('userGroups','record_name').'\');">'.$l10n->getLabel('userGroups','new_record').'</button>';
*/

        //default date conditions
        //if($dateConditions=='')$dateConditions = ' AND DateAffichage <= CURDATE()'; //AND DateFin >= CURDATE()



        return $user->showRecordEditTable(
            array(
                'SELECT' => 'group_name',
                'FROM' => '',
                'JOIN' => '',
                'WHERE' => '1=1'.$dateConditions.$sqlConditions.$deletedCondition,
                'ORDER BY' => 'group_name',
                'ORDER DIRECTION' => 'ASC',
            ),
            'mf_users',
            '',
            'group_name',
            $keyProcessors = array(
                'deleted' => 'dbRecord::getDeletedName'
            ),
            $page = NULL,

            array(
                'create' => 1,
                'view' => 1,
                'edit' => 1,
                'delete'=> 1
            ),
            array(
                'ajaxActions' => true,
                'buttons' => $buttons,
                'listRecordsInAllLanguages' => true
            ),
            'userGroups'
        );


    }
/*
    function createUserGroup(){
        $mf = &$GLOBALS['mf'];

        $userGroup = new mfUserGroup();

        //remove useless keys
        $userGroup->showInEditForm['tab_main'] = array_diff($userGroup->showInEditForm['tab_main'], array('creation_date', 'modification_date'));


        $form = $mf->formsManager;
        return $form->editRecord($userGroup, $this->moduleKey, '', true, true,false,true);
    }

    function editUserGroup($uid){
        $mf = &$GLOBALS['mf'];

        $userGroup = new mfUserGroup();
        $userGroup->load($uid);

        print_r($userGroup->data);

        $form = $mf->formsManager;
        return $form->editRecord($userGroup, $this->moduleKey, '', true, true,false,true, true);

    }



    function viewUserGroup($uid){
        $mf = &$GLOBALS['mf'];

        $userGroup = new user();
        $userGroup->load($uid);

        //setup AJAX output
        foreach($userGroup->data as $key => $data){
            //disable fields editing
            $userGroup->data[$key]['editable']=false;
        }

        //create and publish the record form
        $form = $mf->formsManager;
        return '<div id="ajaxResult">'.$form->editRecord($userGroup, '', '', false,false,false,false,1).'</div><!-- /.ajaxResult -->';

    }

    function deleteUserGroup($uid){
        $userGroup= new mfUserGroup();
        $userGroup->load($uid);
        $userGroup->delete();
        return $userGroup->listRecords();
    }*/




}

