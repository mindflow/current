<?php
$l10nText = array(

    'fr' => array(
        'tab_main' => "Main",
        'local_table' => "Table source",
        'local_uid' => "Uid objet source",
        'local_data' => "Données objet source",
        'out_rule' => "Règle d'envoi",
        'destination' => "Destination",
        'state' => "Etat",
        'result' => "Résultat",
        'local_log' => "Log local",
        'remote_log' => "Log distant",
        'editor_class' => "Classe éditeur",
        'editor_username' => "Nom d'utilisateur éditeur",
        'editor_uid' => "Uid éditeur",
        'Sending' => "Envoi",
        'Scheduled' => "Envoi programmé",
        'Inactive' => "Inactive",
        'Success' => "Succès",
        'Failure' => "Echec",
        'replay' => "Rejouer l'envoi",



        /*mandatory records*/
        'record_name' => "Synchro sortante",
        'new_record' => "Ajout Synchro sortante",
    ),

);

?>