<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

global $mf;

$l10nText = array(


    'fr' => array(
        'tab_main' => 'Principal',

        'parent_uid'=> 'UID enregistrement parent',
        'version'=> 'Version du système de paiement',
        'tpe'=> 'Numéro de TPE Virtuel',
        'date'=> 'Date et heure de la demande de capture',
        'date_commande'=> 'Date de la commande',
        'devise'=> 'Devise',
        'montant'=> 'Montant TTC de la commande initiale',
        'montant_a_capturer'=> 'Montant TTC de la demande de capture',
        'montant_deja_capture'=> 'Montant TTC déjà capturé',
        'montant_restant'=> 'Montant TTC restant après capture',
        'reference'=> 'Référence de la commande',
        'texte-libre'=> 'Texte libre',
        'lge'=> 'Code langue',
        'societe'=> 'Société',
        'MAC'=> 'Sceau MAC',
        'type_paiement'=> 'Type de payement',
        'stoprecurrence'=> 'Forcer la fin de la récurrence',
        'phonie'=> 'Valeur appel phonie',
        'type_paiement'=> 'Type de payement',

        /*mandatory records*/
        //'no_record' => 'Aucun résultat.',
        'record_name' => 'Capture',
        'new_record' => 'Ajout capture',
    ),


);

?>