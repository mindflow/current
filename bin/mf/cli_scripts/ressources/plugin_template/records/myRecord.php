<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class {recordClassName} extends dbRecord
{
    //table name in the SQL database
    static $tableName = '{SQLTableName}';

    static $createTableSQL= "
        `{recordTitleField}` varchar(255) DEFAULT '',
    ";

    static $createTableKeysSQL="
          KEY `k_{recordTitleField}` (`{recordTitleField}`(12)),
          FULLTEXT KEY `fk_{recordTitleField}` (`{recordTitleField}`)
        ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init()
    {
        parent::init();

        global $mf,$l10n;

        $this->data = array_merge($this->data, array(

            '{recordTitleField}' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

        ));


        $this->showInEditForm = array(

            'tab_main'=>array(
                '{recordTitleField}',
            )
        );


        static::$postFormHTML[] = "<script></script>";

    }

}















