<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_searchEngine/records/mfSearchIndex.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_searchEngine/forms/searchIndexFilter.php';

class manageSearchIndex implements module
{
    private $moduleKey = 'manageSearchIndex';
    private $moduleType = 'backend';
    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function prepareData(){
        global $mf,$l10n;
        $this->pageOutput = '';

        if($mf->mode == 'backend'){

            if(isset($mf->currentUser)){

                //check access rights
                $this->userGroup = $mf->currentUser->getUserGroup();
                $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('mf_searchEngine','allowEditSearchIndex')==1)));

                if($this->moduleAccess){

                    //$mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=manageSearchIndex"><span class="glyphicon glyphicon-menu glyphicon-search" aria-hidden="true"></span>'.$l10n->getLabel('manageSearchIndex','menu_title').'</a>','datas').'</a>';


                    if(isset($_REQUEST['submodule']) && ($_REQUEST['submodule'] == $this->moduleKey)){
                        //breadcrumb
                        //$this->subModuleName = "<span class=\"glyphicon glyphicon-menu glyphicon-search\" aria-hidden=\"true\"></span>".$l10n->getLabel('manageSearchIndex','menu_title');

                        if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                        switch($this->action){

/*
                            case "create":
                                $this->pageOutput .= '<h3>'.$l10n->getLabel('manageSearchIndex','new_record').'</h3>';
                                $this->pageOutput .= $this->create();
                                break;

                            case "edit":
                                $this->pageOutput .= '<h3>'.$l10n->getLabel('manageSearchIndex','edit_record').'</h3>';
                                //case edit requested by some module as a GET url parameter
                                if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                    $this->pageOutput .= $this->edit(intval($_GET['uid']));
                                }
                                break;

                            case "view":
                                $this->pageOutput .= '<h3>'.$l10n->getLabel('manageSearchIndex','view_record').'</h3>';
                                //case edit requested by some module as a GET url parameter
                                if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                    $this->pageOutput .= $this->edit(intval($_GET['uid']));
                                }
                                break;


                            case "delete":
                                $this->pageOutput .= $this->delete($_REQUEST['uid']);
                                break;*/

                            case "rebuildIndex":
                                $mf->pluginManager->getModuleInstance('searchIndexer')->rebuildIndex();

                            case "list":
                            default:

                                //composition du filtre d'affichage
                                $searchIndexFilter = new searchIndexFilter();

                                $form = $mf->formsManager;

                                $this->pageOutput .= '<div id="formFilter">';
                                $this->pageOutput .= $form->editForm(SUB_DIR.'/mf/plugins/mf_searchEngine/ajax-searchEngine-json.php', 'searchIndexFilter', 'updateContent', $searchIndexFilter, $this->moduleKey, '',  true, $l10n->getLabel('backend','search'),null);
                                $this->pageOutput .= '<script>
                                    function updateContent(jsonData){
                                        $("#recordEditTable_mfSearchIndex").html(jsonData.message);
                                    }
                                </script></div>';


                                $this->pageOutput .= '<div id="section-to-print">'.$this->listRecords('').'</div>';
                                break;
                        }
                    }
                }
                else $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
            }
        }

    }

    function render(&$mainTemplate){
        global $mf;

        if($mf->mode == 'backend'){

            $mainTemplate = str_replace("{submodule-body}",$this->pageOutput,$mainTemplate);
            $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);
            $mainTemplate = str_replace("{submodule-id}",$this->moduleKey,$mainTemplate);

        }
    }



    function getType(){
        return $this->moduleType;
    }


    function listRecords($sqlConditions = ''){

        global $mf,$l10n,$pdo,$logger;

        $userGroup = $mf->currentUser->data['user_group']['value'];
        $searchIndex = new mfSearchIndex();

        try
        {
            //count entries in the index
            $sql = "SELECT COUNT(*) FROM mf_search_index WHERE 1";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException  $e)
        {
            echo '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>';
            if($e->getCode() == 1045){
                echo $l10n->getLabel('database','connexion_error').'<br />'.chr(10);
                $logger->emergency($l10n->getLabel('database','connexion_error'));
            }
            else{
                //invalid db host address exception
                if($e->getCode()=='2002') echo '<strong>'.$l10n->getLabel('database','host_error').'</strong><br />'.chr(10);

                echo $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
                echo $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
                $logger->emergency($l10n->getLabel('main','error').$e->getMessage());
                $logger->emergency($l10n->getLabel('main','error').$e->getMessage());
            }
            echo "</body></html>";
            die();
        }
        $indexCount = $stmt->fetchColumn();
        if($indexCount == false)$indexCount = 0;

        $searchIndex = new mfSearchIndex();

//{subdir}/mf/index.php?module=searchIndexer&submodule=manageSearchIndex&action=create'
        //$buttons = '<button class="btn-sm btn-primary mf-btn-new" type="button" onClick="openRecord(\'{subdir}/mf/core/ajax-core-html.php?action=createRecord&record_class=searchIndex&header=0&footer=0&sec='.$secHash.'&fields='.$secFields.'&ajax=1&showSaveButton=1&showSaveCloseButton=1&showPreviewButton=&showCloseButton=1&mode='.$mf->mode.'\',\'70%\',\'\',\''.$l10n->getLabel('mfSearchIndex','record_name').'\');">'.$l10n->getLabel('manageSearchIndex','new_record').'</button>';
        $buttons = ' <button class="btn-sm mf-btn-new" type="button" onClick="rebuildIndex();">'.$l10n->getLabel('manageSearchIndex','rebuild_index').'</button>';
        $buttons .= ' '.$indexCount.' '.(($indexCount > 1)?$l10n->getLabel('manageSearchIndex','entries_count_many'):$l10n->getLabel('manageSearchIndex','entries_count_single'));
        $buttons .= '<script>function rebuildIndex(){
            document.location = "'.selfURL().'&action=rebuildIndex";
        };</script>';

        return $searchIndex->showRecordEditTable(
            array(
                'SELECT' => 'class_name,object_uid,title,ressource_url',
                'FROM' => 'mf_search_index',
                'JOIN' => '',
                'WHERE' => 'deleted=0'.$sqlConditions,
                'ORDER BY' => 'sorting',
                'ORDER DIRECTION' => 'DESC',
            ),
            'searchIndexer',
            'manageSearchIndex',
            'ressource_url',

            $keyProcessors = array(

            ),
            $page = NULL,

            array(
                'create' => 1,
                'view' => 1,
                'edit'=> 1,
                'delete'=> 1,
            ),
            array(
                'ajaxActions' => true,
                'buttons' => $buttons,
                'hideColumns' => '',
                'debugSQL' => 0
            ),
            'mfSearchIndex'
        );


    }
/*
    function create(){
        $mf = &$GLOBALS['mf'];

        $searchIndex = new mfSearchIndex();

        $form = $mf->formsManager;
        return $form->editRecord($searchIndex, 'searchIndexer', $this->moduleKey, true,true,false,true);

    }


    function edit($uid){
        $mf = &$GLOBALS['mf'];

        $searchIndex = new mfSearchIndex();
        $searchIndex->load($uid);

        $form = $mf->formsManager;
        return $form->editRecord($searchIndex,  'searchIndexer', $this->moduleKey, true,true,false,true);



    }

    function delete($uid){
        $searchIndex = new mfSearchIndex();
        $searchIndex->load($uid);
        $searchIndex->delete_forever();
        return $this->listRecords();
    }*/



}

