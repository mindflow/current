<?php
function listRecords($sqlConditions = '', $dateConditions = '',  $deletedCondition=' AND {SQLTableName}.deleted=0'){

        global $mf,$l10n;


        //using time to store session SQL in case of multiple open tabs in the browser.
        //$time will allow distincting the good tab because the session now knows when the form was generated
        $sec = md5(microtime());

        //requête pour l'export CSV stockée en $_SESSION
        if(!isset($_SESSION['actions']))$_SESSION['actions']=array();
        $_SESSION['actions'][$sec]=array(
            'action' => 'exportCSV',
            'sql' => 'SELECT * FROM {SQLTableName} WHERE deleted=0'.$sqlConditions,
            'sqlConditions' => $sqlConditions,
            'record_class' => '{recordClassName}',
            'skipKeys' => array('uid', 'deleted', 'hidden', 'sorting', 'creator_uid','creator_class','parent_uid','parent_class', 'change_log', 'edit_lock', 'start_time', 'end_time', 'language', 'alt_language', 'password',
            ),
            'print_column_names' => true,
            'keyProcessors' => array(
                //'title'=>'{recordClassName}::colorTitle',
            ),
        );

        $record = new {recordClassName}();

        $buttons = '<button type="button" class="btn-sm mf-btn-new" id="exportCSV" name="exportCSV" onclick="document.location=\''.getHTTPHost().SUB_DIR.'/mf/core/csv-exporter.php?sec='.$sec.'\';"><span class="glyphicon glyphicon-th"></span> '.$l10n->getLabel('backend','export_to_csv').'</button>';

        return $record->showRecordEditTable(
            array(
                'SELECT' => 'creation_date,{recordTitleField},deleted',
                'FROM' => '',
                'JOIN' => '',
                'WHERE' => '1=1'.$dateConditions.$sqlConditions.$deletedCondition,
                'ORDER BY' => '{recordTitleField}',
                'ORDER DIRECTION' => 'ASC',
            ),
            '{moduleClassName}',//'{pluginKey}',
            '',
            '{recordTitleField}',
            $keyProcessors = array(
                'creation_date' => 'dbRecord::formatDate',
                'deleted' => 'dbRecord::getDeletedName',
            ),
            $page = NULL,

            array(
                'create' => 1,
                'view' => 1,
                'edit' => 1,
                'delete'=> 1
            ),
            array(
                'ajaxActions' => true,
                'buttons' => $buttons,
                'columnClasses' => array(
                    'creation_date' => 'hidden-xs',
                    'email' => 'hidden-xs hidden-sm',
                    'deleted' => 'hidden-xs hidden-sm',
                ),
                'debugSQL' => 0,
                'editRecordButtons' => array(
                    'showSaveButton' => true,
                    'showSaveCloseButton' => true,
                    'showPreviewButton' => false,
                    'showCloseButton' => true
                ),

            ),
            '{recordClassName}' //recordEditTableID
        );
    }