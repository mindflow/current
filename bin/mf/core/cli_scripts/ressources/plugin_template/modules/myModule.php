<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
//{module_include_record}

//{module_include_form}



class {moduleClassName} implements module{

    private $moduleKey = '{moduleClassName}';
    private $moduleType = '{moduleType}';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function __construct(){

    }


    function prepareData(){
        global $mf,$l10n,$config;

        $this->pageOutput = '';

        //{module_prepareData}
    }


    function render(&$mainTemplate){
        global $mf,$l10n,$config;

        //{module_render}
    }


    function getType(){
        return $this->moduleType;
    }

    //{module_list_function}


}

