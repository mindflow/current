<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

$execution_start = time();

require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//we are serving json
header('Content-type: application/json');

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();
$mf = $mindFlowBackend->mf;
$pdo = $mf->db->pdo;
$l10n = $mf->l10n;


//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec()){

    $currentUser = $mf->currentUser;


    if(isset($_REQUEST['action'])){

        if($_REQUEST['action'] == 'paymentsForm'){

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm()){

                if(isset($mf->currentUser)){
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('my_velorizons','allowEditMoneticoPayments')==1)));

                    if($moduleAccess){
                        $sqlConditions = '';
                        $dateConditions = '';
                        $deletedCondition = '';
                        $matchCondition ='';
                        $oneCond=false; //pour voir si une condition a déjà été activée ou pas

                        //Processing form data
                        if(trim($form->data['keyword_filter']['value']) != ''){
                            $sqlConditions .= ' AND MATCH(`reference`,`texte-libre`) AGAINST('.$pdo->quote($form->data['keyword_filter']['value'].'*').' IN BOOLEAN MODE)
                            OR montant LIKE'.$pdo->quote('%'.$form->data['keyword_filter']['value'].'%').'
                            OR MAC = '.$pdo->quote($form->data['keyword_filter']['value']);
                        }

                        if(isset($form->data['deleted_filter']) && $form->data['deleted_filter']['value'] == '1')$deletedCondition = ' AND deleted=1';
                        else $deletedCondition = ' AND deleted=0';

                        if($form->data['DateDebut_filter']['value'] != '')
                            $dateConditions .= 'creation_date >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], 'date', $mf->getLocale()).' 00:00:00');

                        if($form->data['DateFin_filter']['value'] != ''){
                            $boolean = '';
                            if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
                            $dateConditions .= $boolean.' creation_date <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], 'date', $mf->getLocale()).' 23:59:59.999');
                        }
                        if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';






                        $module = new editPayments();
                        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition.$matchCondition);


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}');
                    }
                    else{
                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','module_no_access')).'}');
                    }
                }
                else{
                    $mf->db->closeDB();
                    die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','session_expired')).'}');
                }
            }
            else{
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','form-process-error')).'}');
            }
        }
        else if($_REQUEST['action'] == 'capturesForm'){

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm()){

                if(isset($mf->currentUser)){
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('my_velorizons','allowEditMoneticoCaptures')==1)));

                    if($moduleAccess){
                        $sqlConditions = '';
                        $dateConditions = '';
                        $deletedCondition = '';
                        $matchCondition ='';
                        $oneCond=false; //pour voir si une condition a déjà été activée ou pas

                        //Processing form data
                        if(trim($form->data['keyword_filter']['value']) != ''){
                            $sqlConditions .= ' AND MATCH(`reference`,`texte-libre`) AGAINST('.$pdo->quote($form->data['keyword_filter']['value'].'*').' IN BOOLEAN MODE)
                            OR montant LIKE '.$pdo->quote('%'.$form->data['keyword_filter']['value'].'%').'
                            OR montant_a_capturer LIKE '.$pdo->quote('%'.$form->data['keyword_filter']['value'].'%').'
                            OR MAC = '.$pdo->quote($form->data['keyword_filter']['value']);
                        }

                        if(isset($form->data['deleted_filter']) && $form->data['deleted_filter']['value'] == '1')$deletedCondition = ' AND deleted=1';
                        else $deletedCondition = ' AND deleted=0';

                        if($form->data['DateDebut_filter']['value'] != '')
                            $dateConditions .= 'creation_date >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], 'date', $mf->getLocale()).' 00:00:00');

                        if($form->data['DateFin_filter']['value'] != ''){
                            $boolean = '';
                            if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
                            $dateConditions .= $boolean.' creation_date <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], 'date', $mf->getLocale()).' 23:59:59.999');
                        }
                        if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';






                        $module = new editCaptures();
                        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition.$matchCondition);


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}');
                    }
                    else{
                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','module_no_access')).'}');
                    }
                }
                else{
                    $mf->db->closeDB();
                    die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','session_expired')).'}');
                }
            }
            else{
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','form-process-error')).'}');
            }
        }
        else if($_REQUEST['action'] == 'bankFeedbacksForm'){

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm()){

                if(isset($mf->currentUser)){
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('my_velorizons','allowEditBankFeedbacks')==1)));

                    if($moduleAccess){
                        $sqlConditions = '';
                        $dateConditions = '';
                        $deletedCondition = '';
                        $matchCondition ='';
                        $oneCond=false; //pour voir si une condition a déjà été activée ou pas

                        //Processing form data
                        if(trim($form->data['keyword_filter']['value']) != ''){
                            $sqlConditions .= ' AND MATCH(`reference`,`texte-libre`) AGAINST('.$pdo->quote($form->data['keyword_filter']['value'].'*').' IN BOOLEAN MODE)
                            OR montant LIKE '.$pdo->quote('%'.$form->data['keyword_filter']['value'].'%').'
                            OR MAC = '.$pdo->quote($form->data['keyword_filter']['value']);
                        }

                        if(isset($form->data['deleted_filter']) && $form->data['deleted_filter']['value'] == '1')$deletedCondition = ' AND deleted=1';
                        else $deletedCondition = ' AND deleted=0';

                        if($form->data['DateDebut_filter']['value'] != '')
                            $dateConditions .= 'creation_date >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], 'date', $mf->getLocale()).' 00:00:00');

                        if($form->data['DateFin_filter']['value'] != ''){
                            $boolean = '';
                            if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
                            $dateConditions .= $boolean.' creation_date <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], 'date', $mf->getLocale()).' 23:59:59.999');
                        }
                        if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';





                        $module = new editBankFeedbacks();
                        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition.$matchCondition);


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}');
                    }
                    else{
                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}');
                    }
                }
                else{
                    $mf->db->closeDB();
                    die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','session_expired')).'}');
                }
            }
            else{
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','form-process-error')).'}');
            }
        }


    }
    else {
        $mf->db->closeDB();
        die('{"jsonrpc" : "2.0", "error" : "No action specified."}');
    }
    //echo "Error : No action specified.";
}
else {
    $mf->db->closeDB();
    die('{"jsonrpc" : "2.0", "result" : "error", "message" : "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}');
}

$mf->db->closeDB();




