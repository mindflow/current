<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';

class myPlugin implements plugin{

    function setup(){

    }

    function getName(){
        $l10n = $GLOBALS['mf']->l10n;
        return $l10n->getLabel('myPlugin','name');
    }

    function getDesc(){
        $l10n = $GLOBALS['mf']->l10n;
        return $l10n->getLabel('myPlugin','desc');
    }

    function getPath(){
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey(){
        return "my_plugin";
    }


    function getVersion(){
        return "1.0";
    }

    function getDependencies(){
        $dependencies = array();
        return $dependencies;
    }

    function init(){
        global $mf,$l10n;

        $relativePluginDir = $this->getPath();

        //load the backend menu translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');


        //inform MindFlow of the new dbRecords supplied by this plugin
        $mf->pluginManager->addRecord('my_plugin', 'myContact', $relativePluginDir.'/records/myContact.php');

        //load plugins
        $mf->pluginManager->loadModule($relativePluginDir,"modules","editMyContacts", true);

        //add plugin data editing rights to user groups
        if(class_exists('mfUserGroup')) mfUserGroup::defineUserRight('my_plugin', 'allowEditMyContacts',array(
            'value' => '0',
            'dataType' => 'checkbox',
            'valueType' => 'number',
            'processor' => '',
            'div_attributes' => array('class' => 'col-lg-2'),
        ),
            $relativePluginDir.'/languages/userRights_l10n.php'
        );

        //unset($_SESSION['loginCalledUrl']);
    }
}
$mf->pluginManager->addPluginInstance(new myPlugin());
