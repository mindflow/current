<?php
$l10nText = array(

    'fr' => array(
        'title' => "Title",
        'tab_main' => "Main",
        'local_table' => "Table source",
        'local_data' => "Données objet source",
        'out_rule' => "Règle d'envoi",
        //'destination' => "Destination",
        'state' => "Etat",
        'result' => "Résultat",
        'local_log' => "Log local",
        'remote_log' => "Log distant",
        'editor_class' => "Classe éditeur",
        'editor_username' => "Nom d'utilisateur éditeur",
        'editor_uid' => "Uid éditeur",
        'Scheduled' => "Envoi programmé",
        'Sending' => "Envoi",
        'Inactive' => "Inactive",
        'Success' => "Succès",
        'Failure' => "Echec",
        'replay' => "Rejouer l'envoi",
        'reset' => "Réinitialiser l'état",

	    'name' => "Nom",
	    'enabled' => "Active",
	    'minute' => "Minute",
	    'minute_fail' => "Veuillez indiquer une valeur comprise entre 0 et 59",
	    'hour' => "Heure",
	    'hour_fail' => "Veuillez indiquer une valeur comprise entre 0 et 23",
	    'dom' => "Jour du mois",
	    'dom_fail' => "Veuillez indiquer une valeur comprise entre 1 et 31",
	    'dow' => "Jour de la semaine",
	    'dow_fail' => "Veuillez indiquer une valeur comprise entre 1 et 7",
        'not_numeric' => "La valeur saisie doit être une valeur numérique ou *",
	    'exec_asap' => "Executer dès que possible",
	    'time_last_fired' => "Dernière exécution",
        'destination' => "Destination",

        /*mandatory records*/
        'record_name' => "synchro intégrale de tables",
        'new_record' => "Ajout synchro intégrale de tables",
    ),

);

?>