<?php
if($mf->mode == 'backend') {

            //process backend display
            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('{pluginKey}', 'allowEdit{moduleClassNameUcFirst}') == 1)));

            if ($this->moduleAccess) {
                //add the plugin to the backend menus
                $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module={moduleClassName}">' . $l10n->getLabel('{moduleClassName}', 'menu_title') . '</a>', 'datas', LOW_PRIORITY);

                if (isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])) {

                    $this->localMenu = '<li rel="' . $this->moduleKey . '" class="active"><a href="{subdir}/mf/index.php?module=' . $this->moduleKey . '">{menu-title}</a></li>';


                    //breadcrumb
                    $this->section = $l10n->getLabel('backend', 'datas');
                    $this->moduleName = " / " . $l10n->getLabel('{moduleClassName}', 'menu_title');
                    $this->subModuleName = "";

                    if (isset($_REQUEST['action'])) $this->action = $_REQUEST['action'];

                    //[module_list_backend]
                    $this->pageOutput .= "Hello world.<br />";
                    $this->pageOutput .= "action = " . $this->action;
                    //[/module_list_backend]
                    //{module_list_backend}


                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >' . $l10n->getLabel('backend', 'module_no_access') . '</div>';
                $this->localMenu = '';
            }
        }