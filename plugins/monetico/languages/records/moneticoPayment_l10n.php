<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

global $mf;

$l10nText = array(


    'fr' => array(
        'tab_main' => 'Principal',

        'parent_uid'=> 'UID enregistrement parent',
        'version'=> 'Version du système de paiement',
        'tpe'=> 'Numéro de TPE Virtuel',
        'date'=> 'Date et heure',
        'devise'=> 'Devise',
        'montant'=> 'Montant TTC de la commande',
        'reference'=> 'Référence de la commande',
        'texte-libre'=> 'Texte libre',
        'mail'=> 'Email',
        'lge'=> 'Code langue',
        'societe'=> 'Société',
        'MAC'=> 'Sceau MAC',
        'type_paiement'=> 'Type de payement',
        'aliascb'=> 'Alias carte bancaire client',
        'forcesaisiecb'=> 'Forcer la saisie de la carte bancaire',
        '3dsdebrayable'=> 'Débrayage 3DSecure',
        'protocole'=> 'Paiement via le partenaire',

        'nbrech'=> 'Nombre d’échéances pour cette commande',
        'dateech1'=> 'Date 1ère échéance',
        'montantech1'=> 'Montant TTC 1ère échéance',
        'dateech2'=> 'Date 2ème échéance',
        'montantech2'=> 'Montant TTC 2ème échéance',
        'dateech3'=> 'Date 3ème échéance',
        'montantech3'=> 'Montant TTC 3ème échéance',
        'dateech4'=> 'Date 4ème échéance',
        'montantech4'=> 'Montant TTC 4ème échéance',



        /*mandatory records*/
        //'no_record' => 'Aucun résultat.',
        'record_name' => 'Paiement',
        'new_record' => 'Ajout paiement',
    ),


);

?>