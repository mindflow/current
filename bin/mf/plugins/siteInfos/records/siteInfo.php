<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

class siteInfo extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_siteInfos';

    static $createTableSQL= "
        `template_name` varchar(255) NOT NULL,
        `template_content` longblob,
    ";

    static $createTableKeysSQL="";

    static $template;

    function __construct(){
        $this->init();
    }

    function init(){

        global $config;

        //load template translations
        $this->loadTemplateL10n('/mf/plugins/siteInfos');

        parent::init();

        //load the site info template definition
        if(isset($config['plugins']['siteInfos']['siteInfo-template-def']) && $config['plugins']['siteInfos']['siteInfo-template-def'] != '') {
            include DOC_ROOT . SUB_DIR . $config['plugins']['siteInfos']['siteInfo-template-def'];
        }
        else include DOC_ROOT.SUB_DIR.'/mf/plugins/siteInfos/templates/siteInfo-template.php';
        self::$template = $siteInfoTemplate['siteInfo']['template_data'];

        $this->data = array_merge($this->data, array(
            'template_name'   =>  array(
                'value' => 'siteInfo',
                'dataType' => 'template_name',
                'processor' => '',
            ),
            'template_content'   =>  array(
                'value' => $siteInfoTemplate['siteInfo']['template_data'],
                'dataType' => 'template_data',
                'processor' => '',
            ),


        ));



        $this->showInEditForm = array(
            'tab_siteInfo'=> array('template_content')
        );


    }

    static function getSiteInfo(){
        //first get the current site Info object uid
        global $pdo;

        $siteInfoUid = 0;

        $sql = "SELECT * FROM ".self::$tableName." WHERE 1";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array());
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }


        if($stmt->rowCount() > 0){

            //a siteInfo object exists. Load it and return it.
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $siteInfoUid = $row['uid'];

            $siteInfo = new siteInfo();
            $siteInfo->load($siteInfoUid);
            return $siteInfo;
        }
        else {
            //there is no siteInfo object defined in the database. Create one, store it and return it.
            $siteInfo = new siteInfo();
            $siteInfo->store();
            return $siteInfo;
        };
    }

    function load($uid, $forceLocale = null){
        $loadstatus = parent::load($uid, $forceLocale);
        return $loadstatus;
    }

    function store($updateDates = true, $forceCreate = false, $debug = false, $forceLocale = null){
        return parent::store($updateDates, $forceCreate, $debug, $forceLocale);
    }



    /*
    * Loads the language file for the current class
    * @param $moduleKey the name / folder name of the plugin this class belongs to
    */
    function loadTemplateL10n($moduleKey, $subDir=''){
        global $config;
        //make sure base class dbRecord l10n text is loaded for any record type
        if(!isset($GLOBALS['l10n_text']['dbRecord'])){
            include DOC_ROOT.SUB_DIR.'/mf/languages/dbRecord_l10n.php';
            $GLOBALS['l10n_text']['dbRecord'] =  $l10nText;
        }

        //load l10n text for inherited record types
        if(get_class($this) != 'dbRecord' && !isset($GLOBALS['l10n_text'][get_class($this)])){

            if(isset($config['plugins']['siteInfos']['siteInfo-class-language']))
                include_once DOC_ROOT.SUB_DIR.$config['plugins']['siteInfos']['siteInfo-class-language'];
            else include_once DOC_ROOT.SUB_DIR.'/mf/plugins/'.$moduleKey.'/languages/'.$subDir.'/'.get_class($this).'_l10n.php';
            //include_once DOC_ROOT.SUB_DIR.'/mf/plugins/siteInfos/languages/siteInfo_l10n.php';

            //merging base class dbRecord existing Labels
            $l10nWithParent = array_merge_recursive($GLOBALS['l10n_text']['dbRecord'],$l10nText);
            $GLOBALS['l10n_text'][get_class($this)] =  $l10nWithParent;
            l10nManager::loadL10nArray('templates',$l10nWithParent);

        }
    }

}


