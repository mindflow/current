<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Defines the base functions required for a plugin. A plugin is a container holding one or several modules and records required to accomplish a specific task or adding a specific set of features to MindFlow.
 */
interface plugin {

    /**
     * Must create all plugin related tables and insert required default data
     */
    function setup();

    /**
    * Must return the name of the plugin
    * @return string the plugin name
    */
    function getName();

    /**
     * Must return an explanation of the purpose of the plugin
     * @return string the description
     */
    function getDesc();

    /**
     * Must return the plugin path relative to Mindflow root (such as "/mf/plugins/my_plugin")
     * @return string the plugin path
     */
    function getPath();

    /**
    * Must return the plugin key, which is also the plugin directory name.
    * We recommend avoiding spaces and substitute underscores.
    * @return string the plugin key
    */
    function getKey();

    /**
     * Must return the version number of the plugin
     * @return string the version number
     */
    function getVersion();

    /**
    * Must return the list of plugins keys this plugin relies on
    * @return array of strings featuring valid plugin keys
    */
    function getDependencies();

    /**
     * Must initialize all the objects required for the plugin to run.
     * The function will likely make use of the following helpers :
     * $mf->pluginManager->addRecord() add a database record object
     * $mf->pluginManager->loadModule() add frontend / backend / authentification module
     * mfUserGroup::defineUserRight() add a user right restriction check
     */
    function init();


}
