<?php

require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//we are serving json
header('Content-type: application/json');

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();

global $mf,$pdo,$l10n;

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec())
{

    $currentUser = $mf->currentUser;

    if(isset($_REQUEST['action']))
    {

        //{add_json_action}


    }
    else
    {
        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "No action specified."}';
    }
}
else
{
    $response = '{"jsonrpc" : "2.0", "result" : "error", "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}';
}

echo $response;
$mf->db->closeDB();




