<?php
/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

//ALTER TABLE `mf_contacts` CHANGE `start_time` `start_time` DATETIME NULL;

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

$execution_start = time();

global $pdo,$config;

//remove uploadsDir value in filePath slot from files fields
$replaceValue = $config['uploads_directory'];

/**
 * Seeks for records featuring files fields and updates their filepath for MindFlow 1.9, removing the uploadsDir from the filePath
 */
function makeTheUpgrade($instance,$className) {

	global $mf, $pdo, $config, $l10n, $html, $replaceValue;

	//create only one instance first
	$tableName = $instance::getTableName();

	$html[] = "Analyzing record " . $className . " table name=" . $tableName . "<br />" . chr( 10 );

	//look for files fields
	$recordRequiresUpdate = false;

	//check all its fields
	foreach ( $instance->data as $fieldName => $fieldDef ) {

		//if one of those fiels is either of type 'files', 'microtemplate' or 'template_data', we will have to iterate thru its database entries
		//otherwise we can skip to the next class

		$dataType      = explode( ' ', $fieldDef['dataType'] );
		$dataTypeShort = $dataType[0];

		switch ( $dataTypeShort ) {
			case 'files':
			case 'microtemplate':
			case 'template_data':
				$recordRequiresUpdate = true;
				break;


		}

	}

	if($recordRequiresUpdate){

		$html[] = "\t record ".$className." requires update"."<br />".chr(10);
		$sql = "SELECT uid FROM ".$tableName." WHERE 1";
		$stmt = $pdo->query($sql);

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$record = new $className();
			$record->load($row['uid']);

			processRecord($record);

		}
	}

}



function processRecord(&$record){
	global $html;

	foreach($record->data as $fieldKey => $fieldData){

		if (isset($fieldData['dataType'])) {
			$dataType = explode(' ', $fieldData['dataType']);
			$dataTypeShort = $dataType[0];

			if($dataTypeShort == 'files') {
				//here we have a file and it requires update
				$html[] = "\t\tUpdating ".get_class($record)." uid=" . $record->data['uid']['value'] . "<br />" . chr( 10 );
				processFilesField($record->data[$fieldKey]['value']);
			}

			else if ($dataTypeShort == 'microtemplate') {

				$html[] = "\t\tUpdating ".get_class($record)." uid=" . $record->data['uid']['value'] . "<br />" . chr( 10 );
				processMicroTemplate( $record->data[$fieldKey]['value'] );
			}

			else if ($dataTypeShort == 'template_data') {

				$html[] = "\t\tUpdating ".get_class($record)." uid=" . $record->data['uid']['value'] . "<br />" . chr( 10 );
				processTemplate($record->data[$fieldKey]['value']);
			}
		}


	}

	$record->store();
}



function processFilesField(&$value){
	global $replaceValue,$html;

	if(is_array($value)) {

		foreach ($value as $item => $fileContent) {

			$separator = '';
			//make sure the replaced value contains a trailing '/'
			if( strlen($replaceValue)>0 && substr($replaceValue, -1) != DIRECTORY_SEPARATOR ) $separator = DIRECTORY_SEPARATOR;

			//sanity check, replace '//' with '/' in paths
			$replaceValue = str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $replaceValue);
			$fileContent['filepath'] = str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $fileContent['filepath']);

			//do the replacement
			$value[$item]['filepath'] = str_replace($replaceValue.$separator, '', $fileContent['filepath']);
			$value[$item]['filepath'] = str_replace('/mf_websites/velorizons/uploads/', '', $value[$item]['filepath']);
			$html[] = "\t\tOld path = ".$fileContent['filepath']." \t\t New path = ".$value[$item]['filepath']." \$replaceValue=".$replaceValue.$separator."<br />".chr(10);

		}
	}
}

function processTemplate(&$value) {
	global $html;

	foreach($value as $fieldKey => $fieldData){
		if(isset($fieldData['dataType'])) {
			$dataType      = explode( ' ', $fieldData['dataType'] );
			$dataTypeShort = $dataType[0];

			if ( $dataTypeShort == 'files' ) {

				processFilesField( $value[ $fieldKey ]['value'] );
			}
			else if ( $dataTypeShort == 'microtemplate' ) {

				processMicroTemplate( $value[ $fieldKey ]['value'] );
			}
		}
	}
}

function processMicroTemplate(&$microtemplate){

	foreach($microtemplate as $mtIndex => $mtEntry){

		processTemplate($microtemplate[$mtIndex]['microtemplate_data']);

	}
}



foreach($mf->pluginManager->records as $pluginKey => $record){
	foreach($record as $className => $classData){

		$instance = new $className();

		if($instance::$oneTablePerLocale){
			//save current locale
			$currentLocale = $mf->info['data_editing_locale'] ;

			//alter table for each locale
			foreach ($config['frontend_locales'] as $locale => $localeName){
				//switch the locale
				$mf->setDataEditingLanguage($locale);

				makeTheUpgrade($instance,$className);
			}

			//restore current locale
			$mf->setDataEditingLanguage($currentLocale);
		}
		else {
			makeTheUpgrade($instance,$className);
		}

	}
}
$html[] = "Done.<br />".chr(10);


?>