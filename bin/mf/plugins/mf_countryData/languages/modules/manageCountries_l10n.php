<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'en' => array(
        'menu_title' => 'Countries',
        'new_record' => 'Add a country',
        'edit_record' => 'Edit country',
        'view_record' => 'View country',
        'entries_count_single' => 'entry',
        'entries_count_many' => 'entries',
    ),

    'fr' => array(
        'menu_title' => 'Pays',
        'new_record' => 'Ajouter un pays',
        'edit_record' => 'Editer le pays',
        'view_record' => 'Voir le pays',
        'entries_count_single' => 'entrée',
        'entries_count_many' => 'entrées',

    ),
);