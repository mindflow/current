<?php

$l10nText = array(

    'fr' => array(
	    'titre-adresse1' => 'Company address title',
	    'adresse1' => 'Company address text',
	    'latitude-adresse1' => 'Latitude',
	    'longitude-adresse1' => 'Longitude',

	    'social-facebook' => 'Facebook page link',
	    'social-linkedin' => 'Linkedin page link',
	    'social-twitter' => 'Twitter page link',
	    'social-dribble' => 'Dribble page link',
	    'social-google' => 'Google+ page link',

        /*mandatory records*/
        'record_name' => 'Sitewide informations',
	    'template_content' => '',

    ),

    'en' => array(
	    'titre-adresse1' => 'Titre adresse société',
	    'adresse1' => 'Texte adresse société',
	    'latitude-adresse1' => 'Latitude',
	    'longitude-adresse1' => 'Longitude',

	    'social-facebook' => 'Lien page facebook',
	    'social-linkedin' => 'Lien page linkedin',
	    'social-twitter' => 'Lien page twitter',
	    'social-dribble' => 'Lien page dribble',
	    'social-google' => 'Lien page google+',

	    /*mandatory records*/
        'record_name' => 'Informations du site',
	    'template_content' => '',

    ),
);

?>