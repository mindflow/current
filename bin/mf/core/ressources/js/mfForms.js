/**
 * Created by Alban on 12/01/2017.
 */



jQuery(document).ready(function() {

    $(".microTemplateButton").fancybox({

        fitToView	: false,
        autoSize	: true,
        closeClick	: false,
        openEffect	: 'elastic',
        closeEffect	: 'elastic'
    });

    if($("#uiMessages") && $("#uiMessages").html() && $("#uiMessages").html().trim() == '')$("#uiMessages").hide();

    $('.imgRollover').hover(
        function()
        {
            img =  $(this).find('img');
            src = img.attr('src');
            img.attr('src', src.replace("_off","_on"));
        },
        function()
        {
            img =  $(this).find('img');
            src = img.attr('src');
            img.attr('src', src.replace("_on","_off"));
        }
    );

});


/**
 * Prints the content of a given <div> in the page
 * @param divID a <div> id value to print
 */
function printDiv(divID) {
    //print fix
    //cleaning up styles from previous printing sessions
    $('body *').removeClass('print-parent');
    $('body *').removeClass('print');
    $('body *').removeClass('noprint');

    //set styles for current printing session
    $('body *').addClass('noprint');
    $('#'+divID).removeClass('noprint');
    $('#'+divID).addClass('print-parent');

    $('#'+divID+' *').removeClass('noprint');
    $('#'+divID+' *').addClass('print');
    //alert('printing div '+divID);
    //launch print
    window.print();

    //cleaning up styles from previous printing sessions
    $('body *').removeClass('print-parent');
    $('body *').removeClass('print');
    $('body *').removeClass('noprint');
}


/**
 * Opens a window and prints in it the content of the given <div> id.
 * Usefull for triggering a paper print of the content of the <div>, while ignoring the rest of the content in the page.
 *
 * @param string divID a <div> id value to print
 */
function printDivNewWindow(divID) {

    var win=null;
    var content = $('#'+divID);

    win = window.open("width=200,height=200");
    self.focus();
    win.document.open();
    win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
    win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
    win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
    win.document.write(content.html());
    win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
    win.document.close();
    win.print();
    win.close();
}


/**
 * Pops up a JS window at the center of the screen
 * @param url
 * @param title
 * @param features
 * @param w
 * @param h
 * @constructor
 */
function PopupCenter(url, title, features, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, features + 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}


var r20 = /%20/g,
    rbracket = /\[\]$/,
    rCRLF = /\r?\n/g,
    rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
    rsubmittable = /^(?:input|select|textarea|keygen)/i;
var rcheckableType = (/^(?:checkbox|radio)$/i);

/**
 * Saves a record matching the supplied formID. This function relies on saveForm(), but adds the ability to close the record.
 * It is intended to be trigerred from buttons within the form.
 *
 * @param string formID the formID is the value of the 'id' field of the <form> tag you want to save. As they often contain a random id in MindFlow, they can be tricky to figure.
 *              An easy method for retreiving the formID is to get the id attribute by climbing up from a known field belonging to the form.
 *              Like this : formID = $('#record\\\\|client').closest('form').attr('id');
 * @param string saveMode can be 'save', 'save_close', 'save_preview' or 'close'. The first only saves, the second saves and closes the form, the third saves and pops up a preview window, the 4th closes the current record without saving it.
 * @param callback callback specify a callback function to be executed after the saving. example doFinalize() (without quotes). The callback function profile should be callback(status,jsonData), where status can be "success" or "error" and jsonData is an object carying feedback information (do a console.log(jsonData) and check the console for more details on the returned information.)
 * @param string saveThoseFields only saves the coma separated list of field names specified here and ignores the other fields
 * @param boolean ignoreMandatoryFields  do not raise an error if a mandatory field is not filled correctly
 */
function saveAction(formID, saveMode, callback, saveThoseFields){
    eval('saveAction_'+formID+'(saveMode, callback, saveThoseFields);');
}

/**
 * Saves a record matching the supplied formID, with the possibility to do a selective save on some fields and the possibility to bypass mandatory fields in case a save is required
 * even if not all the record information is filled correctly, such as when displaying a tabbed form and saving the entries between tab switches.
 *
 * @param string formID the formID is the value of the 'id' field of the <form> tag you want to save. As they often contain a random id in MindFlow, they can be tricky to figure.
 *              An easy method for retreiving the formID is to get the id attribute by climbing up from a known field belonging to the form.
 *              Like this : formID = $('#record\\\\|client').closest('form').attr('id');
 * @param string saveMode can be 'save', 'save_close', 'save_preview'. The first only saves, the second saves and closes the form, the third saves and pops up a preview window
 * @param boolean ajaxEditing  Set to 0 or false if not using AJAX. Non AJAX mode hasn't been tested for a while. Keep it to 1.
 * @param callback callback specify a callback function to be executed after the saving. example doFinalize() (without quotes). The callback function profile should be callback(status,jsonData), where status can be "success" or "error" and jsonData is an object carying feedback information (do a console.log(jsonData) and check the console for more details on the returned information.)
 * @param string saveThoseFields only saves the coma separated list of field names specified here and ignores the other fields
 * @param boolean ignoreMandatoryFields  do not raise an error if a mandatory field is not filled correctly
 */
function saveForm(formID, saveMode, ajaxEditing, callback, saveThoseFields, ignoreMandatoryFields){
    eval('saveForm_'+formID+'(saveMode, ajaxEditing, callback, saveThoseFields, ignoreMandatoryFields);');
}



/**
 * Checks if the current record has an uid, and if not, forces saving the form,
 * so an uid can be obtained for the current record and so options added can refer to it.
 * @param formID string the unique ID of the current form
 * @param callback function name to be called on save success
 * @param fieldName the name of the field to save
 */
function checkCanEdit(formID,callback,fieldName){
    if(formID === undefined){
        alert("Error : parameter formID value undefined for call to 'checkCanEdit("+formID+","+callback+","+fieldName+")'");
        return false;
    }

    var thisRecordUid =  $("#"+formID+" #uid").val();

    if(thisRecordUid == ""){
        saveForm(formID,"save", 1, function setCallback() {
            if (typeof callback === "function")callback();
        },fieldName,true);
    }
    else{
        if (typeof callback === "function")callback();
    }
}

/**
 * Returns the called URL and strips the request parameters so there is only the full URL to the current page/script
 *
 * @param string theURL A full request URL including request parameters
 * @returns {*}
 */
function getURLFromString(theURL) {
    //var query = theURL.substr(1);
    var urlSplit = theURL.split("?");

    return urlSplit[0];
}

/**
 * Extracts parameters from an URL and returns them as an associative array
 *
 * @param string theURL the supplied URL
 * @returns {{}}
 */
function getParametersFromString(theURL) {

    var urlSplit = theURL.split("?");

    var result = {};
    if(urlSplit.length > 1){
        var parameters = urlSplit[1].split("&");
        for(var i=0; i<parameters.length; i++) {
            var item = parameters[i].split("=");
            result[item[0]] = item[1];
        }
    }
    return result;
}

//fix for jQuery replaceWith() method returning the originalObject instead of the new object for chained calls
$.fn.replaceWithPush = function(a) {
    var $a = $(a);
    this.replaceWith($a);
    return $a;
};

//fix for jQuery exists()
jQuery.fn.exists = function(){
    return this.length>0;
}


/**
 * After a record save and refresh, substitutes the new HTML to the old HTML of the record editing form.
 *
 * @param string oldFormID formID for the form that has just been saved
 * @param string newFormHTML HTML for the refreshed form
 * @param int editedIndexUid Uid of the curretly edited record
 * @returns string the new formID
 */
function substituteForm(oldFormID,newFormHTML,editedIndexUid) {
    //save open tab id to restore the opened tab after updating the form
    tabID = $("#"+oldFormID+" ul#recordEditor li.active").attr("id");

    //hide content while reloading
    contentPane = $("#"+oldFormID).parent().parent();
    contentPane.fadeTo(0,.1);

    console.log("substituteForm oldFormID="+oldFormID);

    //remove scripts that came along with the form
    $("#"+oldFormID).siblings("script").remove();
    //substitute the form data and update the formID with the one of the updated form
    newFormID = $("#"+oldFormID).replaceWithPush(newFormHTML).find("#formID").val();

    //restore the opened tab after updating the form, if there are tabs displayed
    if(tabID != undefined){
        //we need to adapt the tab id to new form uid. It is not so simple to manage multiple forms in a single window !
        var formUidPos = tabID.indexOf("form_");
        tabID = tabID.slice(0,formUidPos);
        tabID = tabID + newFormID;

        $("#"+newFormID+" #"+tabID).addClass("active").siblings().removeClass("active");

        tabPaneID = tabID.slice(2);

        $("#"+newFormID+" #"+tabPaneID).addClass("active").addClass("in").siblings().removeClass("active").removeClass("in");
    }
    //update the record uid in the form. This prevents duplicates when a new record is saved twice
    $("#"+newFormID+" #uid").val(editedIndexUid);

    //set edited option index, needed if editing a recordSelectField
    editedIndex = editedIndexUid;

    //force evaluating javascript (always disabled after AJAX call for security reasons)
    $("#"+newFormID).find("script").each(function(i) {
        eval($(this).text());
    });

    //show content after reloading
    contentPane.fadeTo(250,1);

    return newFormID;
}

/**
 * Refreshes the recordeditTable matching the supplied recordEditTableId
 *
 * @param string recordEditTableId Id of the recordEditTable to be refreshed
 */
function updateRecordEditTable(recordEditTableId) {
    eval('updateRecordEditTable_'+recordEditTableId+'(lastDisplay_'+recordEditTableId+'["sortingField"], lastDisplay_'+recordEditTableId+'["sortingDirection"], lastDisplay_'+recordEditTableId+'["page"]);');
}


currentActiveTab = new Array();
/**
 * Switches to the tab defined by targetTabName in the form formID
 * @param formID the unique ID of the adressed form
 * @param targetTabName the nname of the tab to be opened
 */
function openTab(formID, targetTabName){

    console.log("openTab targetTabName="+"#"+targetTabName+"_"+formID);
    $(".nav-tabs a[href=#"+targetTabName+"_"+formID+"]").tab("show");
    var offset = $("#"+targetTabName+"_"+formID).offset().top-30;
    $("html, body").animate({scrollTop: offset}, "slow");
    currentActiveTab[formID] = targetTabName;
}