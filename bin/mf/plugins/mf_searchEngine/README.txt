The supplied stopwords list supplies stopwords for French, English and German

To override the default stopword list, set the ft_stopword_file system variable of your MySQL server.
Stopwords file must be included under [mysqld] section in my.ini file like this :
ft_stopword_file='path/to/stopword_file.txt'
The variable value should be the path name of the file containing the stopword list, or the empty string to disable stopword filtering.
The server looks for the file in the data directory unless an absolute path name is given to specify a different directory.
After changing the value of this variable or the contents of the stopword file, restart the server and rebuild your FULLTEXT indexes using command : repair table tablename