<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


/* Quick and dirty debugging output functions */
function echoValue($name, $value){
	echo $name."=".$value."<br/>";
}

function echoTable($table){
	echo nl2br(preg_replace('/[ ]{2,}|[\t]/', '&nbsp;&nbsp;&nbsp;&nbsp;',print_r(print_r($table,true))))."<br/><br/><br/>";

}


function array2Html($array, $table = true){

	//$tableHeader = '';
	$out = '';
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			/*if (!isset($tableHeader)) {
				$tableHeader =
					'<th>' .
						implode('</th><th>', $key) .
						'</th>'; //array_keys($value)
			}
			array_keys($value);
			$out .= '<tr>';
			$out .= array2Html($value, false);
			$out .= '</tr>';*/
			$out .= "<tr><td>$key</td><td>".array2Html($value, false)."</td></tr>";
		} else {
			$out .= "<tr><td>$key</td><td>$value</td></tr>";
		}
	}

	if ($table) {
		return '<table>' . $out . '</table>';
	} else {
		return $out;
	}
}

/*
 function array2Html($array, $table = true){

    $tableHeader = '';
    $out = '';
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            if (!isset($tableHeader)) {
                $tableHeader =
                    '<th>' .
                        implode('</th><th>', array_keys($value)) .
                        '</th>';
            }
            array_keys($value);
            $out .= '<tr>';
            $out .= array2Html($value, false);
            $out .= '</tr>';
        } else {
            $out .= "<td>$value</td>";
        }
    }

    if ($table) {
        return '<table>' . $tableHeader . $out . '</table>';
    } else {
        return $out;
    }
}

 */

/***
 * Convert bytes value in KB or MB to get human readable value
 * @param $size size in bytes
 * @return string
 *
 */
function convert($size)
{
	$unit=array('b','kb','mb','gb','tb','pb');
	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

/* Usage :
$filename = '/etc/hosts/@Álix Ãxel likes - beer?!.jpg';
$filename = Slug($filename, '_', '.'); // etc_hosts_alix_axel_likes_beer.jpg
*/

function Slug($string, $slug = '-', $extra = null)
{
	return strtolower(trim(preg_replace('~[^0-9a-z' . preg_quote($extra, '~') . ']+~i', $slug, unaccent_html($string)), $slug));
}

function unaccent_html($string) // normalizes (romanization) accented chars
{
	if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false)
	{
		$string = html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string), ENT_QUOTES, 'UTF-8');
	}

	return $string;
}

function unaccent_keywords($dataString){
	$keyword = str_replace(" ","", $dataString);
	$keyword = str_replace(",","-", $keyword);
	return unaccent_string($keyword);
}

function unaccent_string($dataString){
	$map = [
		// single letters
		'à' => 'a',
		'á' => 'a',
		'â' => 'a',
		'ã' => 'a',
		'ä' => 'a',
		'ą' => 'a',
		'å' => 'a',
		'ā' => 'a',
		'ă' => 'a',
		'ǎ' => 'a',
		'ǻ' => 'a',
		'À' => 'A',
		'Á' => 'A',
		'Â' => 'A',
		'Ã' => 'A',
		'Ä' => 'A',
		'Ą' => 'A',
		'Å' => 'A',
		'Ā' => 'A',
		'Ă' => 'A',
		'Ǎ' => 'A',
		'Ǻ' => 'A',


		'ç' => 'c',
		'ć' => 'c',
		'ĉ' => 'c',
		'ċ' => 'c',
		'č' => 'c',
		'Ç' => 'C',
		'Ć' => 'C',
		'Ĉ' => 'C',
		'Ċ' => 'C',
		'Č' => 'C',

		'ď' => 'd',
		'đ' => 'd',
		'Ð' => 'D',
		'Ď' => 'D',
		'Đ' => 'D',


		'è' => 'e',
		'é' => 'e',
		'ê' => 'e',
		'ë' => 'e',
		'ę' => 'e',
		'ē' => 'e',
		'ĕ' => 'e',
		'ė' => 'e',
		'ě' => 'e',
		'È' => 'E',
		'É' => 'E',
		'Ê' => 'E',
		'Ë' => 'E',
		'Ę' => 'E',
		'Ē' => 'E',
		'Ĕ' => 'E',
		'Ė' => 'E',
		'Ě' => 'E',

		'ƒ' => 'f',


		'ĝ' => 'g',
		'ğ' => 'g',
		'ġ' => 'g',
		'ģ' => 'g',
		'Ĝ' => 'G',
		'Ğ' => 'G',
		'Ġ' => 'G',
		'Ģ' => 'G',


		'ĥ' => 'h',
		'ħ' => 'h',
		'Ĥ' => 'H',
		'Ħ' => 'H',

		'ì' => 'i',
		'í' => 'i',
		'î' => 'i',
		'ï' => 'i',
		'ĩ' => 'i',
		'ī' => 'i',
		'ĭ' => 'i',
		'į' => 'i',
		'ſ' => 'i',
		'ǐ' => 'i',
		'Ì' => 'I',
		'Í' => 'I',
		'Î' => 'I',
		'Ï' => 'I',
		'Ĩ' => 'I',
		'Ī' => 'I',
		'Ĭ' => 'I',
		'Į' => 'I',
		'İ' => 'I',
		'Ǐ' => 'I',

		'ĵ' => 'j',
		'Ĵ' => 'J',

		'ķ' => 'k',
		'Ķ' => 'K',


		'ł' => 'l',
		'ĺ' => 'l',
		'ļ' => 'l',
		'ľ' => 'l',
		'ŀ' => 'l',
		'Ł' => 'L',
		'Ĺ' => 'L',
		'Ļ' => 'L',
		'Ľ' => 'L',
		'Ŀ' => 'L',


		'ñ' => 'n',
		'ń' => 'n',
		'ņ' => 'n',
		'ň' => 'n',
		'ŉ' => 'n',
		'Ñ' => 'N',
		'Ń' => 'N',
		'Ņ' => 'N',
		'Ň' => 'N',

		'ò' => 'o',
		'ó' => 'o',
		'ô' => 'o',
		'õ' => 'o',
		'ö' => 'o',
		'ð' => 'o',
		'ø' => 'o',
		'ō' => 'o',
		'ŏ' => 'o',
		'ő' => 'o',
		'ơ' => 'o',
		'ǒ' => 'o',
		'ǿ' => 'o',
		'Ò' => 'O',
		'Ó' => 'O',
		'Ô' => 'O',
		'Õ' => 'O',
		'Ö' => 'O',
		'Ø' => 'O',
		'Ō' => 'O',
		'Ŏ' => 'O',
		'Ő' => 'O',
		'Ơ' => 'O',
		'Ǒ' => 'O',
		'Ǿ' => 'O',


		'ŕ' => 'r',
		'ŗ' => 'r',
		'ř' => 'r',
		'Ŕ' => 'R',
		'Ŗ' => 'R',
		'Ř' => 'R',


		'ś' => 's',
		'š' => 's',
		'ŝ' => 's',
		'ş' => 's',
		'Ś' => 'S',
		'Š' => 'S',
		'Ŝ' => 'S',
		'Ş' => 'S',

		'ţ' => 't',
		'ť' => 't',
		'ŧ' => 't',
		'Ţ' => 'T',
		'Ť' => 'T',
		'Ŧ' => 'T',


		'ù' => 'u',
		'ú' => 'u',
		'û' => 'u',
		'ü' => 'u',
		'ũ' => 'u',
		'ū' => 'u',
		'ŭ' => 'u',
		'ů' => 'u',
		'ű' => 'u',
		'ų' => 'u',
		'ư' => 'u',
		'ǔ' => 'u',
		'ǖ' => 'u',
		'ǘ' => 'u',
		'ǚ' => 'u',
		'ǜ' => 'u',
		'Ù' => 'U',
		'Ú' => 'U',
		'Û' => 'U',
		'Ü' => 'U',
		'Ũ' => 'U',
		'Ū' => 'U',
		'Ŭ' => 'U',
		'Ů' => 'U',
		'Ű' => 'U',
		'Ų' => 'U',
		'Ư' => 'U',
		'Ǔ' => 'U',
		'Ǖ' => 'U',
		'Ǘ' => 'U',
		'Ǚ' => 'U',
		'Ǜ' => 'U',


		'ŵ' => 'w',
		'Ŵ' => 'W',

		'ý' => 'y',
		'ÿ' => 'y',
		'ŷ' => 'y',
		'Ý' => 'Y',
		'Ÿ' => 'Y',
		'Ŷ' => 'Y',

		'ż' => 'z',
		'ź' => 'z',
		'ž' => 'z',
		'Ż' => 'Z',
		'Ź' => 'Z',
		'Ž' => 'Z',


		// accentuated ligatures
		'Ǽ' => 'A',
		'ǽ' => 'a',
	];
	return strtr($dataString,$map);
}

function GenerateUrl ($text) {

	//Convert accented characters, and remove parentheses and apostrophes
	$from = explode (',', "ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,(,),[,],'");
	$to = explode (',', 'c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,,,,,,');

	//Do the replacements, and convert all other non-alphanumeric characters to spaces
	$text = preg_replace ('~[^\w\d]+~', '-', str_replace ($from, $to, strtolower(trim($text))));

	//Remove a - at the beginning or end and make lowercase
	return preg_replace ('/^-/', '', preg_replace ('/-$/', '', $text));
}


/**
 * Identical functionnality to parse_url(), but supports IPv6 hostnames as well
 * @param $url
 * @return array
 */
function j_parseUrl($url) {
//echo "url=".$url.chr(10);
	$r  = "(?:([a-z0-9+-._]+)://)?";
	$r .= "(?:";
	$r .=   "(?:((?:[a-z0-9-._~!$&'()*+,;=:]|%[0-9a-f]{2})*)@)?";
	$r .=   "(?:\[((?:[a-z0-9:])*)\])?";
	$r .=   "((?:[a-z0-9-._~!$&'()*+,;=]|%[0-9a-f]{2})*)";
	$r .=   "(?::(\d*))?";
	$r .=   "(/(?:[a-z0-9-._~!$&'()*+,;=:@/]|%[0-9a-f]{2})*)?";
	$r .=   "|";
	$r .=   "(/?";
	$r .=     "(?:[a-z0-9-._~!$&'()*+,;=:@]|%[0-9a-f]{2})+";
	$r .=     "(?:[a-z0-9-._~!$&'()*+,;=:@\/]|%[0-9a-f]{2})*";
	$r .=    ")?";
	$r .= ")";
	$r .= "(?:\?((?:[a-z0-9-._~!$&'()*+,;=:\/?@]|%[0-9a-f]{2})*))?";
	$r .= "(?:#((?:[a-z0-9-._~!$&'()*+,;=:\/?@]|%[0-9a-f]{2})*))?";
	preg_match("`$r`i", $url, $match);

//echo "r=".$r.chr(10);
	$parts = array(
		"scheme"=>'',
		"userinfo"=>'',
		"authority"=>'',
		"host"=> '',
		"port"=>'',
		"path"=>'',
		"query"=>'',
		"fragment"=>'');
//echo "count(\$match)=".count ($match).chr(10);

	switch (count ($match)) {
		case 10: $parts['fragment'] = $match[9];
		case 9: $parts['query'] = $match[8];
		case 8: $parts['path'] =  $match[7];
		case 7: $parts['path'] =  $match[6] . $parts['path'];
		case 6: $parts['port'] =  $match[5];
		case 5: $parts['host'] =  $match[3]?"[".$match[3]."]":$match[4];
		case 4: $parts['userinfo'] =  $match[2];
		case 3: $parts['scheme'] =  $match[1];
	}

//print_r($parts);

	$parts['authority'] = ($parts['userinfo']?$parts['userinfo']."@":"").
	                      $parts['host'].
	                      ($parts['port']?":".$parts['port']:"");
	return $parts;
}



/**
 * Returns http:// or https:// , matching the protocol used to load the current page
 */
function getHTTPScheme(){
	return ((!empty($_SERVER['HTTPS'])) ? "https://" : "http://");
}


/**
 * Returns the hostname prepended with http:// or https://
 */
function getHTTPHost(){
	return getHTTPScheme().$_SERVER['SERVER_NAME'];
}

/**
 * Returns the complete URL of the current page
 * @param $includeQueryString boolean set to false if you do not want to get the parameters after the ? in the url returned
 * @return String
 */
function selfURL($includeQueryString=true){
	if(CLI_MODE){
		return getHTTPHost();
	}
	else {
		if ($includeQueryString)
			$url = getHTTPHost() . $_SERVER['REQUEST_URI'];
		else
			if(strpos ( $_SERVER['REQUEST_URI'], '?'))$url = getHTTPHost() . strstr($_SERVER['REQUEST_URI'], '?', true);
			else $url = getHTTPHost() . $_SERVER['REQUEST_URI'];
		return $url;
	}
}

/**
 * Recognizes ftp://, ftps://, http:// and https:// in a case insensitive way and adds http:// prefix to the URL if missing
 *
 * @param $url
 * @return string
 */
function addHttp($url) {
	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		$firstChar = substr ( $url , 0 , 1 );
		if($firstChar !='/')$url = '/'.$url;
		$url = "http://" . $url;
	}
	return $url;
}

/**
 * Recognizes ftp://, ftps://, http:// and https:// in a case insensitive way and adds 'http://'.$_SERVER['SERVER_NAME'] prefix to the URL if missing
 *
 * @param $url
 * @return string
 */
function addHttpHost($url) {
	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		$firstChar = substr ( $url , 0 , 1 );
		if($firstChar !='/')$url = '/'.$url;

		$url = "http://" .$_SERVER['SERVER_NAME'].$url;
	}
	return $url;
}

/**
 * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
 * keys to arrays rather than overwriting the value in the first array with the duplicate
 * value in the second array, as array_merge does. I.e., with array_merge_recursive,
 * this happens (documented behavior):
 *
 * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
 *     => array('key' => array('org value', 'new value'));
 *
 * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
 * Matching keys' values in the second array overwrite those in the first array, as is the
 * case with array_merge, i.e.:
 *
 * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
 *     => array('key' => array('new value'));
 *
 * Parameters are passed by reference, though only for performance reasons. They're not
 * altered by this function.
 *
 * @param array $array1
 * @param array $array2
 * @return array
 * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
 * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
 */
function array_merge_recursive_distinct ( array &$array1, array &$array2 )
{
	$merged = $array1;

	foreach ( $array2 as $key => &$value )
	{
		if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
		{
			$merged [$key] = array_merge_recursive_distinct ( $merged [$key], $value );
		}
		else
		{
			$merged [$key] = $value;
		}
	}

	return $merged;
}

/*
 * Deletes an element from an array given its value
 */
function array_delete(&$array, $element) {

	$array = array_diff($array, array($element));
}


/* Make a complete deep copy of an array replacing
references with deep copies until a certain depth is reached
($maxdepth) whereupon references are copied as-is...  */

function array_deep_copy (&$array, &$copy, $maxdepth=50, $depth=0) {
	if(is_array($array)){
		if($depth > $maxdepth) { $copy = $array; return; }
		if(!is_array($copy)) $copy = array();

		foreach($array as $k => &$v) {

			if(is_array($v)) {        array_deep_copy($v,$copy[$k],$maxdepth,++$depth);
			} else {
				$copy[$k] = $v;
			}
		}
	}
	else return array();
}

/*
 * Will resize an image to the maximum width or maximum height supplied and will return the filename of the resized image in $imageArray['resizedFilename'], while keeping the original image ratio.
 * If the resized image already exists, it will not be processed again and the filename of the existing resized image will be returned
 * If the given dimensions exceeds those of the original image, existing dimensions of the image will be kept as is : the image will not be upscaled to a bigger image with a poor definition.
 * A new entry 'resizedFilename' is added to the image array returned by the function, giving the path of the resized image, while the entry 'filename' giving the original image path remains untouched.
 * @param $imageArray array the image definition array as obtained from a dbRecord file entry. If the 'filePath' slot in the array begins with a slash, it is considered as an absolute path from the server DOCUMENT_ROOT and will be used as is. Otherwise, it will be considered as a relative path and the uploads directory will be prepended to it.
 * @param $maxWidth int the maximum width of the resized image
 * @param $maxHeight int the maximum height of the resized image
 * @param $forceMatchDimensions boolean if true, will crop the image if the original image ratio does not match the ratio of the target dimensions, and will return an image fitting both given width and height dimension, with a correct subject aspect ratio
 * @param $forceCreateImage boolean if true, will regenerate the image each time the function is called, without checking if the resized image already exists on the filesystem. Use with caution : low performance !
 * @return array the user supplied $imageArray value with a new field $imageArray['resizedFilename'] added.
 */
function mfResizeImage($imageArray, $maxWidth, $maxHeight, $forceMatchDimensions=false, $forceCreateImage = false){

	/*echo nl2br(generateCallTrace());
	echo "<br>";
	echoTable($imageArray);
	echo "substr(\$imageArray['filepath'] , 0, 1 )=".substr($imageArray['filepath'] , 0, 1 )."<br>";
	echo "<br>";*/
	//$GLOBALS['func_start'] = microtime(true);
	global $mf;

	//absolute path from the DOCUMENT ROOT
	if(substr($imageArray['filepath'] , 0, 1 ) == DIRECTORY_SEPARATOR)$uploadsDir = '';
	//relative path from the uploads directory
	else $uploadsDir = $mf->getUploadsDir();

	$resized_filename = strstr($imageArray['filename'], '.', true)."_".$maxWidth."x".$maxHeight."_resized.jpg";
	$originalFilename = $imageArray['filename'];
	if($originalFilename == 'louise2_1532528861.jpg')print_r($imageArray);
	if(isset($imageArray['width'])){
		//retreive already computed original image dimensions from the file array if available
		$originalWidth = $imageArray['width'];
		$originalHeight = $imageArray['height'];
	}
	else{
		//else compute original image dimensions
		list($originalWidth, $originalHeight, $type, $attr)= getimagesize(DOC_ROOT.$uploadsDir.$imageArray['filepath'].$originalFilename);
	}

	//if the resized image already exists in the filesystem
	if(!$forceCreateImage && file_exists(DOC_ROOT.$uploadsDir.$imageArray['filepath'].$resized_filename)){

		// if the original image dimensions are available
		// compute the resized image from existing dimension instead of using function getimagesize() which is a performance killer
		// and return the resized info without processing the image
		$imageArray['resizedFilename'] = $resized_filename;
		if($forceMatchDimensions){
			$dims = array();
			$dims['width'] = $maxWidth;
			$dims['height'] = $maxHeight;
		}
		else $dims = getImageResizedDimensions($originalWidth, $originalHeight, $maxWidth, $maxHeight);

		$resizedWidth = $dims['width'];
		$resizedHeight = $dims['height'];
	}
	//Resized image does not exist in the filesystem
	else   {
		if($originalWidth <= $maxWidth or $originalHeight <= $maxHeight){
			//if original image dimensions are already in bounds regarding the resized maximums, do nothing
			$imageArray['resizedFilename'] = $imageArray['filename'];
			$resizedWidth = $originalWidth;
			$resizedHeight = $originalHeight;

		}
		else{

			//generate a resized image
			$imageArray['resizedFilename'] = resize(DOC_ROOT.$uploadsDir.$imageArray['filepath'], $imageArray['filename'], $maxWidth, $maxHeight, $originalWidth, $originalHeight,$forceMatchDimensions);
			if($forceMatchDimensions){
				$dims = array();
				$dims['width'] = $maxWidth;
				$dims['height'] = $maxHeight;
			}
			else $dims = getImageResizedDimensions($originalWidth, $originalHeight, $maxWidth, $maxHeight);

			$resizedWidth = $dims['width'];
			$resizedHeight = $dims['height'];
		}
	}
	$imageArray['width'] = $resizedWidth;
	$imageArray['height'] = $resizedHeight;

	//$execTime = round (microtime(true) - $GLOBALS['func_start'],4);
	//if(!isset($GLOBALS['exec_total']))$GLOBALS['exec_total']=0;
	//$GLOBALS['exec_total'] += $execTime;
	if($originalFilename == 'louise2_1532528861.jpg')print_r($imageArray);
	return $imageArray;
}

function resize($cur_dir, $cur_file, $max_width, $max_height, $originalWidth, $originalHeight, $preserveAspect=true, $timestamp=''){

	//compute resizing ratios
	$horizontalResizeRatio = $originalWidth / $max_width;
	$verticalResizeRatio = $originalHeight / $max_height;

	if($preserveAspect && $horizontalResizeRatio != $verticalResizeRatio){
		//ratios are different, we need to crop to preserve image aspect
		if($horizontalResizeRatio < $verticalResizeRatio){
			//crop vertically
			$correctHeight = $max_height*$horizontalResizeRatio;

			//find topMost and bottomMost pixels of cropped image
			$removedHeight = $originalHeight-$correctHeight;
			$topLeftY = round($removedHeight/2,0);
			$bottomRightY = $topLeftY+$correctHeight;
			$topLeftX = 0;
			$bottomRightX = $originalWidth;

			$image = getImgRessourceFromFilename($cur_dir.$cur_file);
			$croppedImage = crop($image, $topLeftX, $topLeftY, $bottomRightX, $bottomRightY,$originalWidth,$originalHeight);

			$resized = imagecreatetruecolor($max_width, $max_height);
			imagecopyresampled($resized, $croppedImage, 0, 0, 0, 0, $max_width, $max_height, $originalWidth, $correctHeight);



		}
		else{
			//crop horizontally
			$correctWidth = $max_width*$verticalResizeRatio;

			//find topMost and bottomMost pixels of cropped image
			$removedWidth = $originalWidth-$correctWidth;
			$topLeftY = 0;
			$bottomRightY = $originalHeight;
			$topLeftX = round($removedWidth/2,0);
			$bottomRightX = $topLeftX+$correctWidth;

			$image = getImgRessourceFromFilename($cur_dir.$cur_file);
			$croppedImage = crop($image, $topLeftX, $topLeftY, $bottomRightX, $bottomRightY,$originalWidth,$originalHeight);

			$resized = imagecreatetruecolor($max_width, $max_height);
			imagecopyresampled($resized, $croppedImage, 0, 0, 0, 0, $max_width, $max_height, $correctWidth, $originalHeight);
		}
	}
	else {
		$resized = resizeImage($cur_dir.$cur_file, $max_width, $max_height);
	}


	$timestampAddon = ($timestamp!='')?'_'.$timestamp:'';
	$resized_filename = strstr($cur_file, '.', true).$timestampAddon."_".$max_width."x".$max_height."_resized.jpg";
	$resized_path= $cur_dir.$resized_filename;

	@imagejpeg($resized, $resized_path,90);
	return $resized_filename;
}

/**
 * Crop image
 *
 * @param int|array $x1 Top left x-coordinate of crop box or array of coordinates
 * @param int       $y1 Top left y-coordinate of crop box
 * @param int       $x2 Bottom right x-coordinate of crop box
 * @param int       $y2 Bottom right y-coordinate of crop box
 * @return ImageManipulator for a fluent interface
 * @throws RuntimeException
 */
function crop($image, $x1, $y1 = 0, $x2 = 0, $y2 = 0, $originalWidth, $originalHeight)
{
	if (!is_resource($image)) {
		//throw new RuntimeException('No image set');
		trigger_error('utils.php function crop() : invalid image ressource set', E_USER_WARNING);
	}
	if (is_array($x1) && 4 == count($x1)) {
		list($x1, $y1, $x2, $y2) = $x1;
	}

	$x1 = max($x1, 0);
	$y1 = max($y1, 0);

	$x2 = min($x2, $originalWidth);
	$y2 = min($y2, $originalHeight);

	$width = $x2 - $x1;
	$height = $y2 - $y1;

	$cropped = imagecreatetruecolor($width, $height);
	imagecopy($cropped, $image, 0, 0, $x1, $y1, $width, $height);

	return $cropped;
}

/**
 * Resize an image and keep the proportions
 * @param $filename the file to be processed
 * @param $max_width maximum width after resizing : if the image is wider than higher this limit will be used.
 * @param $max_height maximum height after resizing : if the image is higher than wider this limit will be used.
 * @return image or false if the file is missing
 */
function resizeImage($filename, $max_width, $max_height)
{
	if(is_file($filename)){
		list($orig_width, $orig_height) = getimagesize($filename);
		$dims = getImageResizedDimensions($orig_width, $orig_height, $max_width, $max_height);
		$image_p = imagecreatetruecolor($dims['width'], $dims['height']);
		$image = getImgRessourceFromFilename($filename);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $dims['width'], $dims['height'], $orig_width, $orig_height);

		return $image_p;
	}
	else return false;
}

/**
 * loads filename and converts it into an image object in memory
 * @param $filename string the file to be loaded
 * @return image
 */
function getImgRessourceFromFilename($filename){

	if(preg_match("/.jpg/i", $filename))
	{
		$format = 'image/jpeg';
	}
	else if(preg_match("/.jpeg/i", $filename))
	{
		$format = 'image/jpeg';
	}
	else if (preg_match("/.gif/i", $filename))
	{
		$format = 'image/gif';
	}
	else if(preg_match("/.png/i", $filename))
	{
		$format = 'image/png';
	}
	if($format!='')
	{
		switch($format)
		{
			case 'image/jpeg':
				$image = imagecreatefromjpeg($filename);
				break;
			case 'image/gif';
				$image = imagecreatefromgif($filename);
				break;
			case 'image/png':
				$image = imagecreatefrompng($filename);
				break;
		}
	}
	return $image;
}



/**
 * Computes the dimension of a resized image without actually processing the image
 * @param $filename the file to be processed
 * @param $max_width maximum width after resizing : if the image is wider than higher this limit will be used.
 * @param $max_height maximum height after resizing : if the image is higher than wider this limit will be used.
 * @return array width computed width as 'width' and computed height as 'height' indices
 */
function getImageResizedDimensions($orig_width, $orig_height, $max_width, $max_height){
	$resizedDimensions = array();

	$resizedDimensions['width'] = $orig_width;
	$resizedDimensions['height'] = $orig_height;

	# image taller than wide
	if ($orig_height > $orig_width) {
		if(round(($max_height / $orig_height) * $orig_width) > $max_width){
			//using maxHeight limit, if the computed width remains wider than max_width, limit image size by the width
			$resizedDimensions['width'] = $max_width;
			$resizedDimensions['height'] = round(($max_width / $orig_width) * $orig_height);
		}
		else{
			$resizedDimensions['height'] = $max_height;
			$resizedDimensions['width'] = round(($max_height / $orig_height) * $orig_width);
		}
	}

	# image wider than tall
	else if ($orig_width > $orig_height) {
		if(round(($max_width / $orig_width) * $orig_height) > $max_height){
			//using maxWidth limit, if the computed height remains taller than max_height, limit image size by the height
			$resizedDimensions['height'] = $max_height;
			$resizedDimensions['width'] = round(($max_height / $orig_height) * $orig_width);
		}
		else{
			$resizedDimensions['width'] = $max_width;
			$resizedDimensions['height'] = round(($max_width / $orig_width) * $orig_height);
		}
	}
	return $resizedDimensions;
}


function addWrapper(&$item1, $key, $wrapString){
	$item1 = $wrapString.$item1.$wrapString;
}

function formatTel($tel){
	$tel = str_replace (' ', '', $tel);
	return chunk_split($tel, 2, ' ');
}



function dateToSQL($date, $dataType, $locale){
	global $l10n;

	if($date == '' ) return '';

	if(strpos($date,'-') != false) {
		//date is already in SQL format
		return $date;
		//trigger_error("utils.php dateToSQL(".$date.") : ".$l10n->getLabel('main','date_already_sql'), E_USER_WARNING);
	}

	if ( $dataType == 'date' ) {
		if ( $date != '' and $date != date( $GLOBALS['site_locales'][ $locale ]['php_datetime_format'], strtotime( '0000-00-00' ) ) and $date != 'NULL' ) { //'30/11/-0001'
			//$date = str_replace( '.', '/', $date);
			$date = DateTime::createFromFormat( $GLOBALS['site_locales'][ $locale ]['php_date_format'], $date );
			if ( $date ) {
				return $date->format( 'Y-m-d' );
			} //else return '0000-00-00';
			else {
				return null;
			}
		} //else return '0000-00-00';
		else {
			return null;
		}
	} else if ( $dataType == 'datetime' ) {
		if ( $date != '' and $date != date( $GLOBALS['site_locales'][ $locale ]['php_datetime_format'], strtotime( '0000-00-00 00:00:00' ) ) and $date != 'NULL' ) { //'30/11/-0001 00:00:00'
			$date = DateTime::createFromFormat( $GLOBALS['site_locales'][ $locale ]['php_datetime_format'], $date );
			if ( $date ) {
				return $date->format( 'Y-m-d H:i:s' );
			} //else return '0000-00-00 00:00:00';
			else {
				return null;
			}
		} else {
			return null;
		}
	} else if ( $dataType == 'datetime.ms' ) {
		if ( $date != '' and $date != date( $GLOBALS['site_locales'][ $locale ]['php_datetime_ms_format'], strtotime( '0000-00-00 00:00:00.000000' ) ) and $date != 'NULL' ) { //'30/11/-0001 00:00:00'
			$date = DateTime::createFromFormat( $GLOBALS['site_locales'][ $locale ]['php_datetime_ms_format'], $date );
			if ( $date ) {
				return $date->format( 'Y-m-d H:i:s.u' );
			} //else return '0000-00-00 00:00:00.000000';
			else {
				return null;
			}
		} //else return '0000-00-00 00:00:00.000000';
		else {
			return null;
		}
	} else {
		return $date;
	}

}

function dateFromSQL($date, $dataType, $locale){
	if($dataType=='date'){
		$date = new DateTime($date);
		return $date->format($GLOBALS['site_locales'][$locale]['php_date_format']);
	}
	else if($dataType=='datetime'){
		$date = new DateTime($date);
		return $date->format($GLOBALS['site_locales'][$locale]['php_datetime_format']);
	}
	else if($dataType=='datetime.ms'){
		$date = new DateTime($date);
		return $date->format($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']);
	}
	else return $date;
}

function dateTimeMsNow($locale){
	$date= new DateTime('NOW');
	return $date->format($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']);
}

function dateTimeNow($locale){
	$date= new DateTime('NOW');
	return $date->format($GLOBALS['site_locales'][$locale]['php_datetime_format']);
}

function dateNow($locale){
	$date= new DateTime('NOW');
	return $date->format($GLOBALS['site_locales'][$locale]['php_date_format']);
}




// FORM VALIDATION UTILS
/**
 * Validates a given date string formated as specified in $GLOBALS['site_locales'][$mf->getLocale()]['php_date_format'],
 * returning true or false. Makes use of the standard PHP function checkdate()
 * @param $date a date formated according to the current display locale
 * @return bool validation result
 */
function validateDate($date){
	global $mf,$l10n;

	$dateStripped = date_parse_from_format ( $GLOBALS['site_locales'][$mf->getLocale()]['php_date_format'] , $date );

	if($date){
		if(checkdate($dateStripped['month'],$dateStripped['day'],$dateStripped['year']))return true;
		else return $l10n->getLabel('main','invalid_date');
	}
	else return false;
}

/**
 * Validates the format of an email address. Makes use of the standard PHP function filter_var ( $email, FILTER_VALIDATE_EMAIL);
 * @param $email
 * @return bool validation result
 */
function validateEmail($email){
	if (!filter_var(trim($email), FILTER_VALIDATE_EMAIL) === false) return true;
	else return false;
}





/**
 * Check if a table exists in the current database.
 *
 * @param string $tableName Table to search for.
 * @return bool TRUE if table exists, FALSE if no table found.
 */
function tableExists($tableName) {
	global $pdo;
	// Try a select statement against the table
	// Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
	try {
		$result = @$pdo->query("SELECT 1 FROM ".$tableName." LIMIT 1");
	} catch (Exception $e) {
		// We got an exception == table not found
		return FALSE;
	}

	// Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
	return $result !== FALSE;
}


/*
 * Same as str_replace but features a limit to replace only the n first results
 */
function str_replace_limit($search, $replace, $string, $limit = 1) {
	if (is_bool($pos = (strpos($string, $search))))
		return $string;

	$search_len = strlen($search);

	for ($i = 0; $i < $limit; $i++) {
		$string = substr_replace($string, $replace, $pos, $search_len);

		if (is_bool($pos = (strpos($string, $search))))
			break;
	}
	return $string;
}





/**
 * Recrusive print variables and limit by level.
 *
 * @param   mixed  $data   The variable you want to dump.
 * @param   int    $level  The level number to limit recrusive loop.
 *
 * @return  string  Dumped data.
 *
 * @author  Simon Asika (asika32764[at]gmail com)
 * @date    2013-11-06
 */
function print_r_level($data, $level = 5)
{
	static $innerLevel = 1;

	static $tabLevel = 1;

	static $cache = array();

	$self = __FUNCTION__;

	$type       = gettype($data);
	$tabs       = str_repeat('    ', $tabLevel);
	$quoteTabes = str_repeat('    ', $tabLevel - 1);

	$recrusiveType = array('object', 'array');

	// Recrusive
	if (in_array($type, $recrusiveType))
	{
		// If type is object, try to get properties by Reflection.
		if ($type == 'object')
		{
			if (in_array($data, $cache))
			{
				return "\n{$quoteTabes}*RECURSION*\n";
			}

			// Cache the data
			$cache[] = $data;

			$output     = get_class($data) . ' ' . ucfirst($type);
			$ref        = new \ReflectionObject($data);
			$properties = $ref->getProperties();

			$elements = array();

			foreach ($properties as $property)
			{
				$property->setAccessible(true);

				$pType = $property->getName();

				if ($property->isProtected())
				{
					$pType .= ":protected";
				}
				elseif ($property->isPrivate())
				{
					$pType .= ":" . $property->class . ":private";
				}

				if ($property->isStatic())
				{
					$pType .= ":static";
				}

				$elements[$pType] = $property->getValue($data);
			}
		}
		// If type is array, just retun it's value.
		elseif ($type == 'array')
		{
			$output = ucfirst($type);
			$elements = $data;
		}

		// Start dumping datas
		if ($level == 0 || $innerLevel < $level)
		{
			// Start recrusive print
			$output .= "\n{$quoteTabes}(";

			foreach ($elements as $key => $element)
			{
				$output .= "\n{$tabs}[{$key}] => ";

				// Increment level
				$tabLevel = $tabLevel + 2;
				$innerLevel++;

				$output  .= in_array(gettype($element), $recrusiveType) ? $self($element, $level) : $element;

				// Decrement level
				$tabLevel = $tabLevel - 2;
				$innerLevel--;
			}

			$output .= "\n{$quoteTabes})\n";
		}
		else
		{
			$output .= "\n{$quoteTabes}*MAX LEVEL*\n";
		}
	}

	// Clean cache
	if($innerLevel == 1)
	{
		$cache = array();
	}

	return $output;
}// End function




// Generates a strong password of N length containing at least one lower case letter,
// one uppercase letter, one digit, and one special character. The remaining characters
// in the password are chosen at random from those four sets.
//
// The available characters in each set are user friendly - there are no ambiguous
// characters such as i, l, 1, o, 0, etc. This, coupled with the $add_dashes option,
// makes it much easier for users to manually type or speak their passwords.
//
// Note: the $add_dashes option will increase the length of the password by
// floor(sqrt(N)) characters.

function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
{
	$sets = array();
	if(strpos($available_sets, 'l') !== false)
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
	if(strpos($available_sets, 'u') !== false)
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
	if(strpos($available_sets, 'd') !== false)
		$sets[] = '23456789';
	if(strpos($available_sets, 's') !== false)
		$sets[] = '!@#$%&*?';

	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}

	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];

	$password = str_shuffle($password);

	if(!$add_dashes)
		return $password;

	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
}

/**
 * truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 *
 * @return string Trimmed string.
 */
function truncateHtml($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
	if ($considerHtml) {
		// if the plain text is shorter than the maximum length, return the whole text
		if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
			return $text;
		}
		// splits all html-tags to scanable lines
		preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
		$total_length = strlen($ending);
		$open_tags = array();
		$truncate = '';
		foreach ($lines as $line_matchings) {
			// if there is any html-tag in this line, handle it and add it (uncounted) to the output
			if (!empty($line_matchings[1])) {
				// if it's an "empty element" with or without xhtml-conform closing slash
				if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
					// do nothing
					// if tag is a closing tag
				} else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
					// delete tag from $open_tags list
					$pos = array_search($tag_matchings[1], $open_tags);
					if ($pos !== false) {
						unset($open_tags[$pos]);
					}
					// if tag is an opening tag
				} else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
					// add tag to the beginning of $open_tags list
					array_unshift($open_tags, strtolower($tag_matchings[1]));
				}
				// add html-tag to $truncate'd text
				$truncate .= $line_matchings[1];
			}
			// calculate the length of the plain text part of the line; handle entities as one character
			$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
			if ($total_length+$content_length> $length) {
				// the number of characters which are left
				$left = $length - $total_length;
				$entities_length = 0;
				// search for html entities
				if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
					// calculate the real length of all entities in the legal range
					foreach ($entities[0] as $entity) {
						if ($entity[1]+1-$entities_length <= $left) {
							$left--;
							$entities_length += strlen($entity[0]);
						} else {
							// no more characters left
							break;
						}
					}
				}
				$truncate .= substr($line_matchings[2], 0, $left+$entities_length);
				// maximum lenght is reached, so get off the loop
				break;
			} else {
				$truncate .= $line_matchings[2];
				$total_length += $content_length;
			}
			// if the maximum length is reached, get off the loop
			if($total_length>= $length) {
				break;
			}
		}
	} else {
		if (strlen($text) <= $length) {
			return $text;
		} else {
			$truncate = substr($text, 0, $length - strlen($ending));
		}
	}
	// if the words shouldn't be cut in the middle...
	if (!$exact) {
		// ...search the last occurance of a space...
		$spacepos = strrpos($truncate, ' ');
		if (isset($spacepos)) {
			// ...and cut the text in this position
			$truncate = substr($truncate, 0, $spacepos);
		}
	}
	// add the defined ending to the text
	$truncate .= $ending;
	if($considerHtml) {
		// close all unclosed html-tags
		foreach ($open_tags as $tag) {
			$truncate .= '</' . $tag . '>';
		}
	}
	return $truncate;
}


/**
 * Creates a link to ajax-core-html.php with secured form parameters
 *  @param string $action an action available from ajax-core-html.php, such as 'editRecord' or 'viewRecord'
 *  @param string $recordClass the class of the record to process
 *  @param int|string $recordUid the uid of the record to be edited.
 *  @param boolean $includeCssAndJs tell mindflow to include all required CSS and JS code. This is usefull if the given HTML is displayed in an open window instead of an AJAX Window
 *  @param string $parentClass when you create a record from another, you may want to indicate the class of the record it is created from
 *  @param int $parentUid when you create a record from another, you may want to indicate the uid of the record it is created from
 *  @return string the link to get the content action executed
 */
function makeHTMLActionLink($action, $recordClass='', $recordUid=0, $includeCssAndJs=false, $parentClass='', $parentUid=0){
	global $mf;
	//form security variables
	$secKeys = array(
		'action' => $action,
		'mode' => $mf->mode,
		'mfSetLocale' => $mf->getLocale(),
	);


	// The following fields are only being checked if passed as arguments in this function call.
	// They may be altered using javascript by the developper (obviously in this case, those fields are no longer secured)
	if($recordClass != '' && $recordClass != null) $secKeys['record_class'] = $recordClass;
	if($recordUid != '' && $recordUid != null) $secKeys['record_uid'] = $recordUid;
	if($parentClass != '' && $parentClass != null) $secKeys['parent_class'] = $parentClass;
	if($parentUid != 0 && $parentUid != '0' && $parentUid != null) $secKeys['parent_uid'] = $parentUid;

	$sec = getSec($secKeys);

	if($includeCssAndJs)$showHeaderAndFooter=1;
	else $showHeaderAndFooter=0;

	return SUB_DIR.'/mf/core/ajax-core-html.php?'.$sec->parameters.'&header='.$showHeaderAndFooter.'&footer='.$showHeaderAndFooter.'&sec='.$sec->hash.'&fields='.$sec->fields;
}

/**
 * Creates a link to ajax-core-json.php with secured form parameters
 *  @param string $action an action available from ajax-core-html.php, such as 'editRecord' or 'viewRecord'
 *  @param string $recordClass the class of the record to process
 *  @param int|string $recordUid the uid of the record to be edited.
 *  @return string the link to get the content action executed
 */
function makeJSONActionLink($action, $recordClass, $recordUid){
	global $mf;
	//form security variables
	$secKeys = array(
		'action' => $action,
		'record_class' => $recordClass,
		'record_uid' => $recordUid,
		'mode' => $mf->mode,
		'mfSetLocale' => $mf->getLocale(),
		);
	$sec = getSec($secKeys);

	//$secHash = formsManager::makeSecValue($secKeys);
	//$secFields = implode(',',array_keys($secKeys));

	return SUB_DIR.'/mf/core/ajax-core-json.php?'.$sec->parameters.'&sec='.$sec->hash.'&fields='.$sec->fields;
}


/**
 * Looks for all markers in $subject and returns an array populated with these
 * @param $subject string the template feature {markers} to be analyzed
 * @return array a list of all the markers found ordered as {marker} => marker
 */
function getMarkers($subject){
	$pattern = "/{(.*?)}/";
	$matches = array();
	preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);
	return $matches;
}

/**
 * Empties a folder recursively from all the files and folders it contains
 * @param $dir absolute path to the folder to be emptied
 * @param bool $delete if true, delete the root folder as well
 */
function clear_dir($dir, $delete = false) {
	$folder = $dir;
	$dir = opendir($folder);
	while($file = readdir($dir)) {
		if(!in_array($file, array(".", ".."))){
			if(is_dir("$folder/$file")) {
				clear_dir("$folder/$file", true);
			} else {
				unlink("$folder/$file");
			}
		}
	}
	closedir($dir);

	if($delete == true) {
		rmdir("$folder/$file");
	}
}


/**
 * Logs sent email information into the logfile configured into $config[$mf->getLocale()]['enableEmailLog']
 * @param $from email address of the sender
 * @param $to email address of the recipient
 * @param $subject  subject of the email
 * @param $textToLog text to be put in the log
 */
function logMails($from, $to, $subject, $textToLog){
	global $mf,$config;
	$logMails = (isset($config[$mf->getLocale()]['enableEmailLog']) && $config[$mf->getLocale()]['enableEmailLog']== true);


	if($logMails){
		$logFile = (isset($config[$mf->getLocale()]['emailLogFile']))?$config[$mf->getLocale()]['emailLogFile']:'';
		if($logFile != ''){
			if (!(substr($logFile, 0, 1) === '/')){
				$logFile = DOC_ROOT.SUB_DIR.'/'.$logFile;
			}

			$date = new DateTime('NOW');
			$logEntry = '['.$date->format($GLOBALS['site_locales'][$mf->getLocale()]['php_datetime_format']).']'.chr(10);
			if(is_array($from))$logEntry .= 'FROM = '.implode(';',$from).chr(10);
			else $logEntry .= 'FROM = '.$from.chr(10);
			if(is_array($from))$logEntry .= 'TO = '.implode(';',$to).chr(10);
			else $logEntry .= 'TO = '.$to.chr(10);
			$logEntry .= 'SUBJECT = '.$subject.chr(10);
			$logEntry .= 'LOG = '.$textToLog.chr(10).chr(10);

			if(file_exists ( $logFile ))file_put_contents($logFile, $logEntry, FILE_APPEND);
			else file_put_contents($logFile, $logEntry);
		}
	}
}


/**
 * Copy a file, or recursively copy a folder and its contents
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @return      bool     Returns TRUE on success, FALSE on failure
 */
function copyr($source, $dest)
{
	// Check for symlinks
	if (is_link($source)) {
		return symlink(readlink($source), $dest);
	}

	// Simple copy for a file
	if (is_file($source)) {
		return copy($source, $dest);
	}

	// Make sure source directory exists
	if (!is_dir($source)) {
		mkdir($source,0775,true);
	}

	// Make destination directory
	if (!is_dir($dest)) {
		mkdir($dest,0775,true);
	}

	// Loop through the folder
	$dir = dir($source);
	while (false !== $entry = $dir->read()) {
		// Skip pointers
		if ($entry == '.' || $entry == '..') {
			continue;
		}

		// Deep copy directories
		copyr("$source/$entry", "$dest/$entry");
	}

	// Clean up
	$dir->close();
	return true;
}

/**
 * Deletes recursively a directory and its files
 * @param $dir absolute path to the directory
 * @return bool true on succes, false on failure
 */
function delTree($dir) {
	if(is_dir($dir)) {
		$files = array_diff(scandir($dir), array('.', '..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}
	else return true;
}


/**
 * Displays a backtrace of the calling function, without displaying the function call arguments, which often turns the standard PHP function debug_print_backtrace() unreadable.
 * @return string the backtrace text. Wrap it with nl2br() if you want to display it as HTML.
 */
function generateCallTrace()
{
	global $l10n,$loggerD;

	$e = new Exception();
	$trace = explode("\n", $e->getTraceAsString());

	// reverse array to make steps line up chronologically
	//$trace = array_reverse($trace);
	array_shift($trace); // remove {main}
	array_pop($trace); // remove call to this method

	$length = count($trace);
	$result = array();
	$result[] = '<strong>'.$l10n->getLabel('main','trace').'</strong>';

	for ($i = 0; $i < $length; $i++)
	{
		$result[] = ($i + 1)  . ')' . htmlentities (substr($trace[$i], strpos($trace[$i], ' '))); // replace '#someNum' with '$i)', set the right ordering
	}
	return chr(10)."\t" . implode("\n\t", $result);
}

/**
 * Generates the values required to secure a form. The user inputs an associative array of the values of the form which should never be altered during the request.
 *
 * @param $securedURLParameters an associative array of the URL parameters and their values in input, such as array('action' => 'saveRecord', 'record_class' => 'myContact', 'record_uid' => '29');
 * @return array an associative array featuring the useful values to include in your request :
 *          'parameters' will return action=saveRecord&record_class=myContact&record_uid=29
 *          'hash' will return a sha256 salted hash of the supplied values
 *          'fields' will return the coma separated list of fields to check
 */
function getSec($securedURLParameters){
	$sec = new stdClass;

	$sec->parameters = http_build_query($securedURLParameters);
	$sec->hash = formsManager::makeSecValue($securedURLParameters);
	$sec->fields = implode(',',array_keys($securedURLParameters));

	return $sec;
}

/**
 * checks the sec and fields parameters from $_REQUEST have not been altered regarding secured fields.
 * If the value of one of the fields listed in $_REQUEST['fields'] has been altered, the hash will mismatch and the function will return a false status
 *
 *  @param bool $debug echoes debug information
 *  @return boolean the check success. False if fields verification fails.
 */
function checkSec($debug=false){

	if(isset($_REQUEST['sec'])){
		if(isset($_REQUEST['fields'])){
			return formsManager::checkSecValue($_REQUEST['sec'],explode(',',$_REQUEST['fields']),$debug);
		}
		else {
			if($debug) echo 'utils.php checkSec() : \$_REQUEST[\'fields\'] is not defined. Can not execute checkSecValue().';
			return false;
		}
	}
	else {
		if($debug) echo 'utils.php checkSec() : \$_REQUEST[\'sec\'] is not defined. Can not execute checkSecValue().';
		return false;
	}

	//return (isset($_REQUEST['sec']) && isset($_REQUEST['fields']) )&& formsManager::checkSecValue($_REQUEST['sec'],explode(',',$_REQUEST['fields']));
}

/**
 * filter_var($bool, FILTER_VALIDATE_BOOLEAN) fails to handle strings, so we need our own function for proper filtering of booleans
 * @param $bool
 */
function filterBoolean($bool){
	$type = gettype($bool);

	switch($type) {

		case 'boolean':
			return $bool;

		case 'integer':
			switch ( $bool ) {
				case 0:
					return false;

				case 1:
					return true;

				default:
					return false;
			}

		case 'string':
			switch ( $bool ) {
				case 'false':
				case '0':
				case "'0'":
					return false;

				case 'true':
				case '1':
				case "'1'":
					return true;

				default:
					return false;
			}

		default:
			return false;
	}
}

/**
 * Convertit les dates vides en dates nulles pour la conformité SQL Strict
 * @param $key
 * @param $value
 * @param $locale
 * @param $row
 *
 * @return null
 */
function checkNullDate($key,$value,$locale,$row){
	global $pdo;

	if($value == '' || $value == 'NULL' || $value == null || $value == 'null') return 'NULL';
	else return $pdo->quote($value);
}