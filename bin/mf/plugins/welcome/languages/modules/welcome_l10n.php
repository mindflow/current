<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

$l10nText = array(


    'en' => array(
        'welcome_title' => 'Welcome to MindFlow',
        'welcome_intro' => 'MindFlow is the administration tool for your website. It enables you to create and edit pages, informations in the database, and to publish pictures and documents available for download.',
        'welcome_start_btn' => 'Start editing',
    ),

    'fr' => array(
        'welcome_title' => 'Bienvenue sur MindFlow',
        'welcome_intro' => 'MindFlow est l\'outil d\'administration de votre site web. Il vous permettra de créer et éditer des pages, des informations dans la base de données, de publier des images et des documents à télécharger.',
        'welcome_start_btn' => 'Commencer l\'édition',
    ),
);