<?php

/**
 * Migration's plugin configuration
 *
 * Long Description
 */

$GLOBALS['config']['plugins']['migration'] = array(

    'databases' => array(
    	//remote database configuration
        'www' => array(
	        'db_host' => 'mydb.dbnet.org',
	        'db_port' => '3306',
	        'db_name' => 'mydb',
	        'db_username' => 'mydb',
	        'db_password' => '1234',
	        'db_force_utf8' => true,
        ),
    ),
    'targets' => array(
    	'mf_contacts' => array(
	        'source_db' => 'www',
	        'source_table' => 'mf_contacts',
	        'source_primary_key' => 'uid',
	        'source_query' => array(
		    	'SELECT' => '*',
			    'WHERE' => 'deleted=0',
		    ),

	        'target_table' => 'mf_contacts',
	        'target_primary_key' => 'uid',

		    'import_function' => 'MigrationManager::importRow',

		    //optionnel : si certains champs doivent changer de nom, indiquer les correspondance sous la forme 'nom_champ_remote' => 'nom_champ_local'
		    //sinon le nom du champ est conservé
		    //généralement on essayera plutôt d'avoir un champ correct dès l'envoi en utilisant le 'remap_fields' des send_rules
		    //attention, si le champ est remapé, les keyProcessor s'appliquent alors sur le nom du champ cible
	        'remap_fields' => array(
	        	'creator' => 'creator_uid',
	        ),

		    //optionnel : les champs suivants ne seront pas envoyés
	        'filter_fields' => array('change_log'),

		    //optionnel : indiquer les fonctions de pré-traitement qui s'appliquent pour mettre au format un champ avant de l'enregistrer dans l'objet local
		    //préciser sous la forme 'nom_champ_local' => 'nom_objet::nom_fonction'
	        'key_processors' => array(
		        'start_time' => 'checkNullDate',
		        'end_time' => 'checkNullDate',
	        ),
	    ),


	    'mf_news' => array(
		    'source_db' => 'www',
		    'source_table' => 'mf_news',
		    'source_primary_key' => 'uid',
		    'source_query' => array(
			    'SELECT' => '*',
			    'WHERE' => 'deleted=0',
		    ),

		    'target_table' => 'mf_news',
		    'target_primary_key' => 'uid',

		    'import_function' => 'MigrationManager::importRow',

		    'remap_fields' => array(
			    'creator' => 'creator_uid',
		    ),

		    'filter_fields' => array('change_log'),
		    'key_processors' => array(
			    'start_time' => 'checkNullDate',
			    'end_time' => 'checkNullDate',
		    ),
	    ),

	    'mf_news_categories' => array(
		    'source_db' => 'www',
		    'source_table' => 'mf_news_categories',
		    'source_primary_key' => 'uid',
		    'source_query' => array(
			    'SELECT' => '*',
			    'WHERE' => 'deleted=0',
		    ),

		    'target_table' => 'mf_news_categories',
		    'target_primary_key' => 'uid',

		    'import_function' => 'MigrationManager::importRow',

		    'remap_fields' => array(
			    'creator' => 'creator_uid',
		    ),

		    'filter_fields' => array('change_log'),
		    'key_processors' => array(
			    'start_time' => 'checkNullDate',
			    'end_time' => 'checkNullDate',
		    ),
	    ),

	    'mf_pages' => array(
		    'source_db' => 'www',
		    'source_table' => 'mf_pages',
		    'source_primary_key' => 'uid',
		    'source_query' => array(
			    'SELECT' => '*',
			    'WHERE' => 'deleted=0',
		    ),

		    'target_table' => 'mf_pages',
		    'target_primary_key' => 'uid',

		    'import_function' => 'MigrationManager::importRow',

		    'remap_fields' => array(
			    'creator' => 'creator_uid',
		    ),

		    'filter_fields' => array('change_log'),
		    'key_processors' => array(
			    'start_time' => 'checkNullDate',
			    'end_time' => 'checkNullDate',
		    ),
	    ),

	    'mf_search_index' => array(
		    'source_db' => 'www',
		    'source_table' => 'mf_search_index',
		    'source_primary_key' => 'uid',
		    'source_query' => array(
			    'SELECT' => '*',
			    'WHERE' => 'deleted=0',
		    ),

		    'target_table' => 'mf_search_index',
		    'target_primary_key' => 'uid',

		    'import_function' => 'MigrationManager::importRow',

		    'remap_fields' => array(
			    'creator' => 'creator_uid',
		    ),

		    'filter_fields' => array('change_log'),
		    'key_processors' => array(
			    'start_time' => 'checkNullDate',
			    'end_time' => 'checkNullDate',
		    ),
	    ),



	    'mf_siteInfos' => array(
		    'source_db' => 'www',
		    'source_table' => 'mf_siteInfos',
		    'source_primary_key' => 'uid',
		    'source_query' => array(
			    'SELECT' => '*',
			    'WHERE' => 'deleted=0',
		    ),

		    'target_table' => 'mf_siteInfos',
		    'target_primary_key' => 'uid',

		    'import_function' => 'MigrationManager::importRow',

		    'remap_fields' => array(
		    ),

		    'filter_fields' => array('change_log','creator'),
		    'key_processors' => array(
			    'start_time' => 'checkNullDate',
			    'end_time' => 'checkNullDate',
		    ),
	    ),




    ),

);