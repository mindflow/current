<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'uid' => 'Unique record ID',
        'parent_uid' => 'Unique ID of parent record',
        'parent_class' => 'Class of parent record',
        'deleted' => 'Deleted',
        'active' => 'Active',
        'sorting' => 'Sorting order',
        'creation_date' => 'Creation date',
        'modification_date' => 'Last modification date',
        'creator_uid' => 'Creator user',
        'creator_class' => 'Creator class',
        'edit_lock' => 'Lock editing',
        'start_time' => 'Display start date & time',
        'end_time' => 'Display end date & time',
        'language' => 'Record language',
        'hidden' => 'Hide this record',
        'table' => 'Table',
        'creation_success' => 'created sucessfully !',
        'creation_failure' => 'An error occured while creating table',
        'results' => 'results',
        'key' => 'Key',
        'no_attr' => 'has no mandatory attribute',
        'no_result' => 'No result found',
        'import_success' => 'lines imported successfuly',
        "Modificator_uid" =>"Modified by",
	    'modificator_class' => "Modifier class",
		'editor_uid' =>"Editor",
        'editor_class' => "Editor class",
        'edit_start_time' => "Edit start time",
        "Alt_language" => "Page in alternative language",
        'history_last' => "Latest version history",
        'history_past' => "History of all versions",
    ),

    'fr' => array(
        'uid' => 'ID unique de l\'enregistrement',
        'parent_uid' => 'ID unique de l\'enregistrement parent',
        'parent_class' => 'Classe de l\'enregistrement parent',
        'deleted' => 'Effacé',
        'hidden' => 'Masquer cet enregistrement',
        'active' => 'Actif',
        'sorting' => 'Ordre de classement',
        'creation_date' => 'Date de création',
        'modification_date' => 'Date de dernière modification',
        'creator_uid' => 'Créé par',
        'creator_class' => 'Classe du créateur',
        'edit_lock' => 'Vérou d\'édition',
        'start_time' => 'Date & heure de début d\'affichage',
        'end_time' => 'Date & heure de fin d\'affichage',
        'language' => 'Langue de l\'enregistrement',
        'table' => 'Table',
        'creation_success' => 'créée avec succès !',
        'creation_failure' => 'Une erreur est survenue durant la création de la table',
        'results' => 'résultats',
        'key' => 'La clé',
        'no_attr' => 'n\'a pas l\'attribut obligatoire',
        'no_result' => 'Aucun résultat',
        'import_success' => 'lignes importées avec succès',
        'modificator_uid' => 'Modifié par',
        'modificator_class' => 'Classe du modificateur',
		'editor_uid' => 'Editeur',
        'editor_class' => "Classe de l'éditeur",
        'edit_start_time' => "Horaire de début d'édition",
        'alt_language' => "Page en langue alternative",
        'history_last' => "Historique denière version",
        'history_past' => "Historique toutes versions",

    ),

    'de' => array(
        'uid' => 'ID unique de l\'enregistrement',
        'parent_uid' => 'ID unique de l\'enregistrement parent',
        'parent_class' => 'Classe de l\'enregistrement parent',
        'deleted' => 'Effacé',
        'active' => 'Actif',
        'sorting' => 'Ordre de classement',
        'creation_date' => 'Date de création',
        'modification_date' => 'Date de dernière modification',
        'creator_uid' => 'Utilisateur créateur',
        'creator_class' => 'Class du créateur',
        'edit_lock' => 'Vérou d\'édition',
        'start_time' => 'Date & heure de début d\'affichage',
        'end_time' => 'Date & heure de fin d\'affichage',
        'language' => 'Langue de l\'enregistrement',
        'hidden' => 'Masquer cet enregistrement',
        'table' => 'Table',
        'creation_success' => 'créée avec succès !',
        'creation_failure' => 'Une erreur est survenue durant la création de la table',
        'results' => 'résultats',
        'key' => 'La clé',
        'no_attr' => 'n\'a pas l\'attribut obligatoire',
        'no_result' => 'Keine Resultat.',
        'import_success' => 'lines imported successfuly',
        'Modificator_uid' =>"Modifiziert von",
	    'modificator_class' => "Modifierklasse",
		'editor_uid' =>"Editor",
        'editor_class' => "Editor's class",
        'edit_start_time' => "Startzeit bearbeiten",
        'Alt_language' => "Seite in alternativer Sprache",
        'history_last' => "Latest version history",
        'history_past' => "Alle Versionen historisieren",
    ),


);

?>