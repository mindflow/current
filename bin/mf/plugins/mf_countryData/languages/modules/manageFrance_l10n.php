<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'en' => array(
        'menu_title' => 'French cities',
        'new_record' => 'Add a city',
        'edit_record' => 'Edit city',
        'view_record' => 'View city',
        'entries_count_single' => 'entry',
        'entries_count_many' => 'entries'
    ),

    'fr' => array(
        'menu_title' => 'Communes française',
        'new_record' => 'Ajouter une commune',
        'edit_record' => 'Editer la commune',
        'view_record' => 'Voir la commune',
        'entries_count_single' => 'entrée',
        'entries_count_many' => 'entrées'
    ),
);