<?php

//templates definition
$siteInfoTemplate = array(

	//A template with name 'default' is mandatory
	//newly created pages will use this template by default
	'siteInfo' => array(
		'template_name' => 'siteInfo',
		'template_file' => '',
		'template_data' => array(
			'titre-adresse1' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-6'),
			),
			'adresse1' => array(
				'dataType' => 'rich_text',
				'value' => '',
				'processor' => '',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-8'),
			),
			'latitude-adresse1' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-3'),
			),
			'longitude-adresse1' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-3'),
			),




			'social-facebook' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-5'),
			),
			'social-linkedin' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-5'),
			),
			'social-twitter' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-5'),
			),
			'social-google' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-5'),
			),
			'social-dribble' => array(
				'value' => '',
				'dataType' => 'input',
				'valueType' => 'text',
				'field_attributes' => array(
					'class' => 'input-lg',
				),
				'div_attributes' => array('class' => 'col-lg-5'),
			),


		),
		'showInEditForm' =>'titre-adresse1,adresse1,latitude-adresse1,longitude-adresse1,social-facebook,social-linkedin,social-twitter,social-google,social-dribble',

	),


);

?>