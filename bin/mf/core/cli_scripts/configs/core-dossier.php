<?php
/**************************
 * Add Plugin settings
 * Fill this section in all cases
 **************************/

//Natural name of the plugin
$pluginName = "Dynamo Core";

//Short description
$pluginDesc = "Coeur de fonctionnalités Dynamo";

//Base ISO2 locale for localized files
$defaultLocale = "fr";

//Class name of the plugin
$pluginClassName = "dynamoCore";

//Folder name of the website where the plugin should be created
$websiteFolderName = "dynamo";

//Folder name / plugin key of the plugin
$pluginDirName = "dyn_core";


/**************************
 * Add module settings
 * Fill this section when generating a module
 **************************/

//Natural name of the module
$moduleNaturalName = "Dossiers";

//Short description
$moduleDesc = "Gestion des dossiers";

//Class name of the module
$moduleClassName = "gestionDossiers";

//module type. Value should be either : frontend / backend or frontend,backend
$moduleTypeFrontend = 0;
$moduleTypeBackend = 1;
$moduleTypeAuth = 0;



/**************************
 * Add record settings
 * Fill this section if adding a record to an existing module
 **************************/

//Inserted record class name
$recordClassName = 'dossier';

//SQL table name where the records will be stored
$SQLTableName = 'dyn_dossiers';

//Inserted record natural name
$recordNaturalName = 'Dossier';

//Should the record be listed/edited into some module
$moduleWillListRecords = false;

//Column name of the listed record's title field
$recordTitleField = 'titre';

//create record Label
$createRecordLabel = 'Créer un dossier';

//edit record label
$editRecordLabel = 'Editer le dossier';


/**************************
 * Add form settings
 * Fill this section if adding a filter form to an existing module
 **************************/

//Displayed record class name
$formClassName = 'filtreDossiers';
