<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Defines a file upload field, allowing simultaneous upload of multiple files and allowing control of the file extensions allowed.
 * By default only the following file extensions are allowed : 'gif','jpg','jpeg','png','xls','xlsx','doc','docx','ppt','pptx','pdf','odb','odc','odf','odg','odp','ods','odt','oxt'
 */
class filesField implements formField
{
    /**
     * Returns the HTML code for displaying the field, initialized with the data supplied in $fieldData
     * @param $f fieldData : this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
     * $f->formID           // value of the id attribute of the <form> tag this field is included in
     * $f->mode             // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
     * $f->dataType         // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
     * $f->key              // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
     * $f->fieldData        // this is the data array of the current field
     * $f->recordUid        // the uid (Unique ID) of the record containing the current field
     * $f->recordClassName  // the class name of the record containing the current field
     * $f->record           // all the data for the record containing the current field
     * $f->microKey         // field name when processing a microTemplate field (only used when mode=="micro|")
     * $f->microIndex       // sorting index when processing a microTemplate field (only used when mode=="micro|")
     * @returns String a string containing the HTML for displaying the field
     */
    static public function displayField($f)
    {
        global $mf,$l10n,$formsManager,$config;

        $html = array();
        $fieldAttr = formsManager::getTagAttributes('field',$f->key, $f->fieldData);
        $divAttr = formsManager::getTagAttributes('div',$f->key, $f->fieldData);

        if(!isset($f->dataType[1])) $f->dataType[1]='text';

        //display the label in front of the field or not
        $showLabel      = (isset($f->fieldData['showLabel']))?      $f->fieldData['showLabel']      :true;

        //display a <div> around the field or not
        $showFieldDiv   = (isset($f->fieldData['showFieldDiv']))?   $f->fieldData['showFieldDiv']   :true;

        //display custom HTML in front of the field
        $preFieldHTML   = (isset($f->fieldData['preFieldHTML']))?   $f->fieldData['preFieldHTML']   :'';

        //display custom HTML in after the field
        $postFieldHTML  = (isset($f->fieldData['postFieldHTML']))?  $f->fieldData['postFieldHTML']  :'';

        //display custom HTML in front of the field
        $preDivHTML   = (isset($f->fieldData['preDivHTML']))?   $f->fieldData['preDivHTML']   :'';

        //display custom HTML in after the field
        $postDivHTML  = (isset($f->fieldData['postDivHTML']))?  $f->fieldData['postDivHTML']  :'';


        if($mf->mode == 'frontend'){
            if(isset($config['backend_template']))$xEndTemplatePath = $config['backend_template'];
            else $xEndTemplatePath = '/mf/backend/templates/main';
        }else{
            $xEndTemplatePath = $mf->xEnd->template;
        }

        if($showLabel)$html[] = formsManager::getFieldLabel($f->fieldData, $f->mode, $f->key, $f->recordClassName);

        /**************************************************
         *   MindFlow < v1.4 data conversion
         *************************************************/

        /*if(!is_array($f->fieldData['value'])){
            //initialize void value
            $updatedValue = array();

            //convert MindFlow < v1.4 existing data to array
            if(trim(ltrim($f->fieldData['value'],',')) != ''){
                $oldValues = explode(',',ltrim($f->fieldData['value'],','));
                $i=0;
                foreach($oldValues as $index => $value){
                    $updatedValue[$i]=array(
                        'timestamp' => time(),
                        'filename' => $value,
                        'filepath' => DIRECTORY_SEPARATOR.$f->recordClassName.DIRECTORY_SEPARATOR.$f->recordClassName.'_'.$f->recordUid.DIRECTORY_SEPARATOR
                        //'filepath' => $config['uploads_directory'].(($localizeUploadsDir)?DIRECTORY_SEPARATOR.$mf->getLocale():'').DIRECTORY_SEPARATOR.$f->recordClassName.DIRECTORY_SEPARATOR.$f->recordClassName.'_'.$f->recordUid.DIRECTORY_SEPARATOR
                    );
                    $i++;
                }
            }

            $f->fieldData['value'] = $updatedValue;

        }*/
        //END MindFlow < v1.4 data conversion

        if(!isset($f->fieldData['editable']))$f->fieldData['editable']=true;
	    $isEditable = filterBoolean($f->fieldData['editable']);

        if(!$isEditable){
            //read only record display, no input editing
            $currentFiles = $f->fieldData['value'];
            $html[] = $preDivHTML;
            $html[] = ($showFieldDiv)?'<div class="'.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
            $html[] = $preFieldHTML;
            $html[] = '<span id="'.$f->mode.$f->key.'" class="noedit '.$fieldAttr['classes'].'" '.$fieldAttr['attributes'].'>';

            $str = '';

            if(isset($currentFiles) && is_array($currentFiles)) {
                foreach ($currentFiles as $file) {
                    $fileName = formsManager::preDisplay($f->key, $file['filename'], $f->fieldData, $mf->getUploadsDir() . $file['filepath']);
                    $str .= '<a href="' . $mf->getUploadsDir() . $file['filepath'] . $file['filename'] . '" target="_blank"><i class="glyphicon glyphicon-download-alt"></i> ' . $fileName . '</a><br />';
                }
            }
            $html[] = $str;

            $html[] = '</span>';
            $html[] = $postFieldHTML;
            $html[] = ($showFieldDiv)?'</div>':'';
            $html[] = $postDivHTML;

        }
        else {
            $componentJSID = str_replace('-','_',$f->key);
            $modalID = 'uploaderModal_'.$componentJSID;

            $filesList = array();
            if(is_array($f->fieldData['value'])){
                foreach($f->fieldData['value'] as $index => $fileEntry){
                    if(is_array($fileEntry)){
                        if($fileEntry['filename'] != '')$filesList[] = $fileEntry['filename'];
                    }
                }
            }



            //input editing
            $fileUploaderHTML = '';
            $fileUploaderHTML .= $preDivHTML;
            $fileUploaderHTML .= ($showFieldDiv)?'<div class="'.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
            $fileUploaderHTML .= $preFieldHTML;
            $fileUploaderHTML .= '<a id="addFilesButton" onclick="'.$componentJSID.'_getFilesUploadDialog();return false;" class="btn btn-primary">
                                      '.$l10n->getLabel('backend','add-files').'
                                    </a>';
            $fileUploaderHTML .= $postFieldHTML;
            $fileUploaderHTML .= '<input type="hidden" value="'.implode (',' ,$filesList).'" name="'.'files|'.$f->key.'" id="'.'files|'.$f->key.'"/>
                                  <div class="clearfix">';
            $fileUploaderHTML .= ($showFieldDiv)?'</div>':'';
            $fileUploaderHTML .= $postDivHTML;
            $html[] = '</span>';


            //display files already present in the record
            $currentFiles = $f->fieldData['value'];
            $fileUploaderHTML .= '<ul id="sortable_'.$f->key.'" class="sortable col-lg-offset-3 col-lg-9">';
            if(is_array($currentFiles) && sizeof($currentFiles)>0){
                foreach($currentFiles as $file){
                    //using Slug() to make a clean ID with no '_' in order to avoid interference with the key separator using jQuery
                    $fileID = $f->key.'_'.Slug($file['filename'], '');

                    //form security variables
                    if($f->recordUid != ''){
                        $secKeys = array(
                            'record_class' => $f->recordClassName,
                            'record_uid' => $f->recordUid,
                            'field_name' => $f->key,
                            'micro_key' => $f->microKey
                        );
                    }else{
                        $secKeys = array(
                            'record_class' => $f->recordClassName,
                            'field_name' => $f->key,
                            'micro_key' => $f->microKey
                        );
                    }
                    $secHash = formsManager::makeSecValue($secKeys);
                    $secFields = rtrim(implode(',',array_keys($secKeys)),',');

                    //display a file icon instead of image preview if the file is not an image
                    $imgExtensions = array('gif','jpg','jpeg','png');
                    $fileExtPart = ltrim(strstr($file['filename'], '.', false),'.');
                    if(in_array($fileExtPart,$imgExtensions))$filePreview = '<img src="'.$mf->getUploadsDir().$file['filepath'].$file['filename'].'" />';
                    else $filePreview = '<i class="glyphicon glyphicon-file"></i>';

                    $fileUploaderHTML .= '
                                            <li class="fileSort imagebar" id="'.$fileID.'" data-name="'.$file['filename'].'"><a href="'.$mf->getUploadsDir().$file['filepath'].$file['filename'].'" target="blank">'.$filePreview.$file['filename'].'</a>
                                                <span class="btn-group btn-sort">
                                                    <span class="recordAction"><a><i class="glyphicon glyphicon-move"></i></a></span>
                                                    <span class="recordAction"><a href="'.$mf->getUploadsDir().$file['filepath'].$file['filename'].'" target="blank"><i class="glyphicon glyphicon-download-alt"></i></a></span>
                                                    <span class="recordAction"><a onclick="'.$componentJSID.'_removeFile(\''.$fileID.'\',\''.$f->recordClassName.'\',\''.$f->recordUid.'\',\''.$file['filename'].'\',\''.$f->key.'\',\''.$f->mode.'\',\''.$f->microKey.'\',\''.$f->microIndex.'\',\''.$secHash.'\',\''.$secFields.'\',\''.((isset($f->fieldData['jsCallbackFunc']))?$f->fieldData['jsCallbackFunc']:'').'\');return false;"><i class="glyphicon glyphicon-trash"></i></a></span>
                                                   </span>
                                            </li>
                                        ';
                    // <button class="btn btn-sm"><i class="glyphicon glyphicon-fullscreen"></i></button>
                }
            }

            $fileUploaderHTML .= '</ul><span class="errorMsg col-lg-offset-4 col-lg-8"></div>';

            $fileUploaderHTML .= '<div id="filesUploadDialog_'.$componentJSID.'" style="display:none;">

                                    </div>';
            /*
                //in case the file field is located inside a template data structure
                $templateName = '';
                if($f->mode=='template|'){
                    if(isset($mainRecordData['template_name']) && isset($mainRecordData['template_name']['value']))
                        $templateName = $mainRecordData['template_name']['value'];
                }
             */


            $formsManager->addBottomJs('filesField_'.$componentJSID,'<script type="text/javascript">
                function '.$componentJSID.'_removeFile(DOM_ID, recordClass,recordUid,fileName,fieldName,recordMode,microKey,microIndex, secHash, fieldList, jsCallbackFunc){
                    var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action:"removeFile", file: fileName, record_uid: recordUid, record_class: recordClass, sec: secHash, fields: fieldList, record_mode: recordMode, field_name: fieldName, micro_key: microKey, micro_index: microIndex, mode: "'.$mf->mode.'"})
                    .success(function(jsonData) {
                        
                         //delete the file <li> from the DOM on success
                         $("#"+DOM_ID).remove();

                         //delete the file name from the hidden input used for file count checking
                         var filesList =  $("#files\\\\|"+fieldName).val().split(",");
                         var index = filesList.indexOf(fileName);
                         if (index > -1) {
                            filesList.splice(index, 1);
                         }
                         $("#files\\\\|"+fieldName).val(filesList.join());

                         if(jsCallbackFunc)eval(jsCallbackFunc+"(jsonData)");
                        
                    })
                    .fail( function(xhr, textStatus, errorThrown) {
                        alert("filesField.php removeFile() : " + xhr.responseText);
                    })
                }
            </script>');

            //byte symbol translated
            $b = $l10n->getLabel('backend','b');

            //form security variables
            $secKeys = array(
                'action' => "sortRecordsFile",
                'record_class' => $f->recordClassName,
                'mode' => $mf->mode,
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = rtrim(implode(',',array_keys($secKeys)),',');


            $filesField2Script = array();

            $filesField2Script[] = '
                <script>
                        var '.$componentJSID.'_filesUploadDialog;

                        $(document).ready(function() {
                            $(function() {
                                $( "#sortable_'.$f->key.'" ).sortable({
                                    opacity: 0.6,
                                    cursor: "move",
                                    handle: ".glyphicon-move",
                                    width: "450px",
                                    update: function() {
                                        //console.log($(this).sortable("toArray"));
                                        var order = $(this).sortable("serialize") + "&record_class='.$f->recordClassName.'&record_uid='.$f->recordUid.'&record_mode='.$f->mode.'&action=sortRecordsFile&sec='.$secHash.'&fields='.$secFields.'&mode='.$mf->mode.'";
                                        $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", order);
                                    }
                                });
                                $( "#sortable_'.$f->key.'" ).disableSelection();
                            });
                        });
            ';

                        //form security variables
                        $secKeys = array(
                            'action' => "removeFile",
                            'record_class' => $f->recordClassName,
                            'field_name' => $f->key,
                        );
                        $secHash = formsManager::makeSecValue($secKeys);
                        $secFields = implode(',',array_keys($secKeys));


                        $record = new $f->recordClassName();

                        if(is_subclass_of ($record,'dbRecord')) {
	                        $filePath = SUB_DIR . $mf->getUploadsDir() . $f->recordClassName . DIRECTORY_SEPARATOR . $f->recordClassName . '_"+recordUid+"' . DIRECTORY_SEPARATOR;
	                        $fileMove = '<span class=\"recordAction\"><a><i class=\"glyphicon glyphicon-move\"></i></a></span>';
                        }
                        else if(is_subclass_of ($record,'dbForm')) {
	                        $filePath = SUB_DIR . $mf->getUploadsDir() . $f->recordClassName . DIRECTORY_SEPARATOR;
	                        $fileMove = '';
                        }

                        $filesField2Script[] = '

                                function '.$componentJSID.'_addFile(filename,recordUid,fileID){
                                    fileID = "'.$f->key.'_"+fileID;
                                    filePath = "'.$filePath.'"+filename;

                                    extension = filename.split('.').pop();

                                    //display a file icon instead of image preview if the file is not an image
                                    if (!extension.match(/\.(jpg|jpeg|png|gif)$/)){
                                        $("#sortable_'.$f->key.'").append(
                                        "<li class=\"fileSort imagebar\"  id=\""+fileID+"\" data-name=\""+filename+"\"><span class=\"glyphicon glyphicon-file\" aria-hidden=\"true\"></span>"+filename+"<span class=\"btn-group btn-sort\">'.$fileMove.'<span class=\"recordAction\"><a href=\""+filePath+"\" target=\"blank\"><i class=\"glyphicon glyphicon-download-alt\"></i></a></span><span class=\"recordAction\"><a onclick=\"'.$componentJSID.'_removeFile( \'"+fileID+"\', \''.$f->recordClassName.'\',\'"+recordUid+"\',\'"+filename+"\',\''.$f->key.'\',\''.$f->mode.'\',\''.$f->microKey.'\',\''.$f->microIndex.'\',\''.$secHash.'\',\''.$secFields.'\',\''.((isset($f->fieldData['jsCallbackFunc']))?$f->fieldData['jsCallbackFunc']:'').'\');return false;\"><i class=\"glyphicon glyphicon-trash\"></i></a></span></li>"
                                        );
                                    }
                                    else {
                                        $("#sortable_'.$f->key.'").append(
                                          "<li class=\"fileSort imagebar\"  id=\""+fileID+"\" data-name=\""+filename+"\"><img src=\""+filePath+"\" />"+filename+"<span class=\"btn-group btn-sort\">'.$fileMove.'<span class=\"recordAction\"><a href=\""+filePath+"\" target=\"blank\"><i class=\"glyphicon glyphicon-download-alt\"></i></a></span><span class=\"recordAction\"><a onclick=\"'.$componentJSID.'_removeFile( \'"+fileID+"\', \''.$f->recordClassName.'\',\'"+recordUid+"\',\'"+filename+"\',\''.$f->key.'\',\''.$f->mode.'\',\''.$f->microKey.'\',\''.$f->microIndex.'\',\''.$secHash.'\',\''.$secFields.'\',\''.((isset($f->fieldData['jsCallbackFunc']))?$f->fieldData['jsCallbackFunc']:'').'\');return false;\"><i class=\"glyphicon glyphicon-trash\"></i></a></span></li>"
                                        );
                                    }
                                    if($("#files\\\\|'.$f->key.'").val().trim()=="")
                                        $("#files\\\\|'.$f->key.'").val($("#files\\\\|'.$f->key.'").val() + filename);
                                    else
                                        $("#files\\\\|'.$f->key.'").val($("#files\\\\|'.$f->key.'").val() +","+ filename);
                                 }

                            ';



      $filesField2Script[] = '

                  function '.$componentJSID.'_popupLocalFileDialog(){
                      document.getElementById("'.$componentJSID.'_fileElem").click();
                  }

                  function '.$componentJSID.'_closeFilesUpload(){
                      '.$componentJSID.'_filesUploadDialog.dialog( "close" );
                  }

                  //actions to be performed when the window is closed
                  function '.$componentJSID.'_execFilesUploadCloseActions(){
                      '.$componentJSID.'_filesUploadDialog.dialog("destroy").remove();
                      $("#'.$modalID.'").remove();
                      $("#filesUploadDialog_'.$componentJSID.'").empty();
                  }

                  function '.$componentJSID.'_getFilesUploadDialog() {
                      $("#filesUploadDialog_'.$componentJSID.'").html("<div id=\"'.$modalID.'\" >\
                                      <form name=\"uploadForm\" id=\"uploadForm\">\
                                          <h4 class=\"modal-title\" id=\"uploaderModalLabel\">'.$l10n->getLabel('backend','add-files').'</h4>\
                                          <div class=\"modal-body\">\
                                              <input type=\"file\" id=\"'.$componentJSID.'_fileElem\" name=\"fileElem\" multiple style=\"display:none\" onchange=\"'.$componentJSID.'_updateSize();'.$componentJSID.'_handleFiles(this.files)\">\
                                              <button type=\"button\" class=\"btn btn-primary '.$componentJSID.'_fileSelect\" id=\"'.$componentJSID.'_fileSelect\" onclick=\"'.$componentJSID.'_popupLocalFileDialog()\">'.$l10n->getLabel('backend','select_files').'</button>\
                                              <div id=\"'.$componentJSID.'_fileList\">\
                                                  <p>'.$l10n->getLabel('backend','no_files_selected').'</p>\
                                              </div>\
                                              <p class=\"help-block\">'.$l10n->getLabel('backend','selected_files').' : <span id=\"'.$componentJSID.'_fileNum\">0</span>; '.$l10n->getLabel('backend','total_size').' : <span id=\"'.$componentJSID.'_fileSize\">0</span></p>\
                                          </div>\
                                          <!-- moved html -->\
                                          <div class=\"modal-footer\">\
                                              <button type=\"button\" class=\"btn btn-primary\" onclick=\"'.$componentJSID.'_sendFiles();\">'.$l10n->getLabel('backend','send_files').'</button>\
                                              <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" onclick=\"'.$componentJSID.'_closeFilesUpload();return false;\">'.$l10n->getLabel('backend','close').'</button>\
                                          \
                                          </div>\
                                      </form>\
                                  </div>");

                      '.$componentJSID.'_filesUploadDialog = $("#'.$modalID.'").dialog({
                         autoOpen: false,

                      });
                      if (typeof(recordDialog) != "undefined")parent=recordDialog;
                      else parent=window;

                      '.$componentJSID.'_filesUploadDialog.dialog( {
                          closeText: "hide",
                          requestFailed: "Request failed. Please try again",
                          width: "40%",
                          //position: { my: "center", at: "top+30%", of: parent }, //bug when displaying relative to parent
                          position: { my: "center", at: "top+30%", of: window },
                          modal: true,
                          width: 550,
                          minWidth:300,
                          //show: { effect: "fadeIn", duration: 500 }
                      } );
                      '.$componentJSID.'_filesUploadDialog.on( "dialogclose", function( event, ui ) {'.$componentJSID.'_execFilesUploadCloseActions();} );
                      '.$componentJSID.'_filesUploadDialog.dialog( "open");


                  };

              function '.$componentJSID.'_updateSize() {
                    var nBytes = 0,
                        oFiles = document.getElementById("'.$componentJSID.'_fileElem").files,
                        nFiles = oFiles.length;
                    for (var nFileId = 0; nFileId < nFiles; nFileId++) {
                      nBytes += oFiles[nFileId].size;
                    }
                    var sOutput = "<span class=uploadDesc >"+ nBytes + " '.$l10n->getLabel('backend','bytes').'" + "</span>";
                    // optional code for multiples approximation
                    for (var aMultiples = ["K'.$b.'", "M'.$b.'", "G'.$b.'", "T'.$b.'", "P'.$b.'", "E'.$b.'", "Z'.$b.'", "Y'.$b.'"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
                      sOutput = Math.floor(nApprox.toFixed(3)) + " " + aMultiples[nMultiple];
                    }
                    // end of optional code
                    document.getElementById("'.$componentJSID.'_fileNum").innerHTML = nFiles;
                    document.getElementById("'.$componentJSID.'_fileSize").innerHTML = sOutput;
              }


              window.URL = window.URL || window.webkitURL;

              //stat for counting finished uploads and close the modal once all uploads are done
              var filesUploaded;


              function '.$componentJSID.'_handleFiles(files) {
                    if (!files.length) {
                      $("#'.$componentJSID.'_fileList").html("<p>'.$l10n->getLabel('backend','no_files_selected').'</p>");

                    }
                    else {

                          $("#'.$componentJSID.'_fileList").html("<ul id=\"'.$componentJSID.'_uploadsList\"></ul>");

                          filesUploaded = new Array(files.length);

                          for (var i = 0; i < files.length; i++) {

                              var li = $("<li id=\"'.$componentJSID.'_uploadItem_"+i+"\"></li>");
                              li.appendTo("#'.$componentJSID.'_uploadsList");

                              var imageType = /image.*/;

                                    var file = $("<span><i class=\"glyphicon glyphicon-file\"></i> <strong>"+files[i].name+"</strong> "+files[i].type+"</span> ");
                                    file.appendTo("#'.$componentJSID.'_uploadItem_"+i);

                                    var info = $("<span></span>");
                                    info.html( " ( " + Math.floor(files[i].size/1024) + " K'.$l10n->getLabel('backend','b').' ) ");
                                    info.appendTo("#'.$componentJSID.'_uploadItem_"+i);

                                    var progress = $("<div class=\"progress\"><div id=\"uploadProgress_"+i+"\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 0%;\">0%</div></div>");
                                    progress.appendTo("#'.$componentJSID.'_uploadItem_"+i);

                                }

                          }
                    }
                    
                    
                    



                    function '.$componentJSID.'_sendFiles() {

                        var recordUid = "'.$f->recordUid.'";

                        //create the object record if it doesn\'t exit yet
                         if(recordUid ==""){
                            console.log("recordUid is null, obtaining new recordUid");
                            //save our record first because if it is a new record, there will be no database record to save the image information to
                            //we need to get a record uid in this case
                            var imgCode = "<div class=\"savingNotification\"><img src=\"'.SUB_DIR.$xEndTemplatePath.'/img/360.gif\" width=\"24\" height=\"24\" alt=\"\" />&nbsp;'.$l10n->getLabel('backend','saving').'</div>";

                            //transfer CKEditor content back to the textarea tag before transmitting the form
                            if (typeof CKEDITOR != "undefined"){
                                for (var i in CKEDITOR.instances) {
                                    (function(i){
                                        var textareaID = "#"+CKEDITOR.instances[i].name.replace("|","\\\|");
                                        $(textareaID).val(CKEDITOR.instances[i].getData());
                                    })(i);
                                }
                            }

                            //disable field validation so the recording of the image doesn\'t fail in case of erroneous fields
                            $("#'.$f->formID.' #ignoreMandatoryFields").val(1);
                            
                            $("#'.$f->formID.' #fm_notificationMessage").html(imgCode);
                            $.ajax({
                                url: "'.SUB_DIR.'/mf/core/ajax-core-json.php",
                                type: $("#'.$f->formID.'").attr("method"),
                                data: $("#'.$f->formID.'").serialize(),
                                dataType: "json",
                                success: function(jsonData) {
                                    if(jsonData.message.error){
                                        alert("ajax-core-json.php error 1.1 : " + jsonData.message.error);
                                    }
                                    else{
                                        //update the record uid in the form. This prevents duplicates when a new record is saved twice
                                        recordUid = jsonData.uid;
                                        $("#'.$f->formID.' #uid").val(recordUid);
                                        console.log("Obtained new recordUid : "+jsonData.uid);

                                        $("#'.$f->formID.' #ignoreMandatoryFields").val(0);

                                        //upload files only after the recordUid has been retreived
                                        var myFileList = document.getElementById("'.$componentJSID.'_fileElem").files;
                                        for (var i = 0; i < myFileList.length; i++) {
                                            //console.log("FileUpload "+i);
                                            new '.$componentJSID.'_FileUpload("uploadProgress_"+i, myFileList[i], i);
                                            filesUploaded[i]=0;
                                        }
                                    }
                                },
                                error: function(jsonData) {
                                    showError(jsonData);
                                    return false;
                                }
                            });
                            $("html").delay(5000);
                         }
                         else{

                            //upload files
                            var myFileList = document.getElementById("'.$componentJSID.'_fileElem").files;

                            for (var i = 0; i < myFileList.length; i++) {
                                new '.$componentJSID.'_FileUpload("uploadProgress_"+i, myFileList[i], i);
                                filesUploaded[i]=0;
                            }
                         }
                    }';

            $formsManager->addBottomJs('filesField2_'.$componentJSID, implode(chr(10), $filesField2Script));

            //form security variables
            $secKeys = array(
                'action' => "uploadFile",
                'record_class' => $f->recordClassName,
                'field_name' => $f->key,
                //'allowedFileExtensions' => $allowedFileExtensions
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $mf->formsManager->addBottomJs('filesField3_'.$componentJSID,'
                    function '.$componentJSID.'_FileUpload(progressID, file, fileIndex) {

                      //create the formData object
                      var formData = new FormData();

                      // Append our file to the formData object
                      formData.append("action", "uploadFile");
                      formData.append("uploadedFile", file);
                      formData.append("record_class", "'.$f->recordClassName.'");
                      formData.append("field_name", "'.$f->key.'");
                      formData.append("record_uid", $("#'.$f->formID.' #uid").val());
                      formData.append("record_mode", "'.$f->mode.'");
                      formData.append("fileName", file.name);
                      formData.append("sec", "'.$secHash.'");
                      formData.append("fields", "'.$secFields.'");');

            if($f->mode=='micro|'){
                $mf->formsManager->addBottomJs('filesField4_'.$componentJSID,'
                      //microtemplate specific fields
                      formData.append("micro_key", "'.$f->microKey.'");
                      formData.append("micro_index", "'.$f->microIndex.'");
                ');
            }


            $mf->formsManager->addBottomJs('filesField5_'.$componentJSID,'

                      var xhr = new XMLHttpRequest();
                      this.xhr = xhr;

                      var self = this;
                      this.xhr.upload.addEventListener("progress", function(e) {
                            if (e.lengthComputable) {
                              var percentage = Math.round((e.loaded * 100) / e.total);
                              $("#"+progressID).attr("aria-valuenow",percentage);
                              $("#"+progressID).attr("style","width: "+percentage+"%;");
                              $("#"+progressID).html(percentage+"%");
                            }
                      }, false);

                      xhr.upload.addEventListener("load", function(e){
                          $("#"+progressID).attr("aria-valuenow","100");
                          $("#"+progressID).attr("style","width: 100%;");
                          $("#"+progressID).html("100%");

                          filesUploaded[fileIndex]=1;

                      }, false);

                      // Open our connection using the POST method
                      xhr.open("POST", "'.SUB_DIR.'/mf/core/fileUploader.php");
                      ');



            $mf->formsManager->addBottomJs('filesField6_'.$componentJSID,'
                    
                    
                    
                      //handling response
                      xhr.onload  = function() {

                            var json = JSON.parse(xhr.responseText);

                            if(json.result=="error"){
                                alert(json.message);
                                return;
                            }
                            //retreive the sanitized filename as written on the server disk
                            filename = json.fileName;
                            recordUid = json.recordUid;
                            fileID = json.fileID;

                            '.$componentJSID.'_addFile(filename, recordUid, fileID);
                            
                            

                            //iterate over filesUploaded array and do logical AND on the uploadsDone value.
                            //If one upload is not finished, uploadsDone will be 0 and the modal window should not be hidden yet. Hide it if uploadsDone value is 1 at the end of the loop
                            var uploadsDone = 1;
                            for(i=0;i<filesUploaded.length;i++){
                                uploadsDone = filesUploaded[i] && uploadsDone;
                            }
                            if(uploadsDone == 1){
                                '.$componentJSID.'_closeFilesUpload();
                                //execute user defined callback function if specified
                                '.((isset($f->fieldData['jsCallbackFunc']))?$f->fieldData['jsCallbackFunc'].'(json);':'').'
                            }
                      };

                      // Send the file
                      xhr.send(formData);
                    }
                    
                    
                    </script>
                  ');

            $html[] = $fileUploaderHTML;
        }

        if(isset($f->fieldData['noClearFix']) and $f->fieldData['noClearFix'] == '1');
        else $html[] = "<div class='clearfix'></div>";

        return implode(chr(10),$html);
    }



    /**
     * Process the field value prior saving it to the database, whenever inner value adaptation is required, for SQL strict compliance for example.
     * This is executed after 'preRecord' processing functions.
     * @param $fieldData this is the data array of the current field
     * @param $value this is the submited value
     * @return mixed
     */
    static public function onSave($fieldData, $value){
        return $value;
    }

    static public function getCSS(){return "";}
    static public function getTopJS(){return "";}
    static public function getBottomJS(){return "";}


	/**
	 * Adds to the array of the specified fieldName a file data
	 * The file must be already present in the record's directory.
	 * It is you responsability to call store() on your object after executing the function in order to persist the added data in the database.
	 * This way you can cull the function multiple times to add multiple files and then store the object only once in the end.
	 *
	 * @param $dbRecord a dbRecord instance to update it data array
	 * @param $fieldName the name of the file field in the data array
	 * @param $fileName the file name included extension, without its path (we alrady know it : this is the record's directory)
	 */
	static public function addFileNoCopy(&$dbRecord, $fieldName, $fileName){
		global $mf,$l10n;

		//name part of the file
		$fileExtPart = strrchr ($fileName, '.');

		//check file extension
		if(isset($dbRecord->data[$fieldName]['allowedExtensions'])){
			$allowedExtensions = explode(',',str_replace(' ','',$dbRecord->data[$fieldName]['allowedExtensions']));
		}
		else{
			$allowedExtensions = array('gif','jpg','jpeg','png','xls','xlsx','doc','docx','ppt','pptx','pdf','odb','odc','odf','odg','odp','ods','odt','oxt');
		}

		if(!in_array(ltrim($fileExtPart,'.'),$allowedExtensions)){
			throw new Exception($l10n->getLabel('main','file_ext_not_allowed'));
		}

		if(!is_array($dbRecord->data[$fieldName]['value']))$dbRecord->data[$fieldName]['value'] = array();

		list($width, $height, $type, $attr)= getimagesize(DOC_ROOT.$mf->getUploadsDir().$dbRecord->getFilesDir().$fileName);

		$dbRecord->data[$fieldName]['value'][] = array(
			'timestamp' => time(),
			'filename' => $fileName,
			'filepath' => $dbRecord->getFilesDir(),
			'width' => $width,
			'height' => $height
		);

	}



}
