<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'en' => array(
        'group_name' => 'Group name',
        'user_rights' => 'User rights',
        'template_content' =>'',
        'template_name' =>'',

        /*mandatory records*/
        'record_name' => 'Backend usergroup',
        'new_record' => 'Add backend usergroup',
    ),

    'fr' => array(
        'group_name' => 'Nom du groupe',
        'user_rights' => 'Droits des utilisateurs',
        'template_content' =>'',
        'template_name' =>'',

        /*mandatory records*/
        'record_name' => 'Groupe utilisateurs backend',
        'new_record' => 'Ajout groupe d\'utilisateurs',
    ),
);

?>