<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
//require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/vz_satisfaction/records/vzSatisfaction.php';

class monetico implements module
{
    private $pluginName = 'monetico';
    private $pluginVersion = '1.0';
    private $pluginType = 'backend';
    private $dependencies = '';

    private $errorMessage='';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function __construct(){

    }


    function prepareData(){
        global $mf,$l10n;
        $locale = $mf->getLocale();
        $this->pageOutput = '';
        $this->localMenu = '';


        if($mf->mode == 'backend'){

            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('monetico','allowEditMonetico')==1)));

            if($this->moduleAccess){
                //add the plugin to the backend menus
                if($GLOBALS['config']['plugins']['monetico']['showInMenus'])$mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=monetico">'.$l10n->getLabel('monetico','menu_title').'</a>','datas', LOW_PRIORITY);

                if(isset($_REQUEST['module']) && ($this->pluginName == $_REQUEST['module'])){

                    if(!isset($_REQUEST['submodule'])|| $_REQUEST['submodule']=='')$_REQUEST['submodule']='editPayments';


                    //breadcrumb
                    $this->moduleName = $l10n->getLabel('monetico','module_title');
                    $this->subModuleName = $l10n->getLabel($_REQUEST['submodule'],'menu_title');

                    $enableCaptureModule = $GLOBALS['config']['plugins']['monetico']['enableCaptureModule'];
                    $enablePaymentModule = $GLOBALS['config']['plugins']['monetico']['enablePaymentModule'];
                    $enableBankFeedbackModule = $GLOBALS['config']['plugins']['monetico']['enableBankFeedbackModule'];

                    if($enablePaymentModule && ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('monetico','allowEditMoneticoPayments')==1))))
                        $this->localMenu .='<li rel="editPayments"'.(($_REQUEST['submodule']=='editPayments')?' class="active"':'').'><a href="{subdir}/mf/index.php?module=monetico&submodule=editPayments">'.$l10n->getLabel('editPayments','menu_title').'</a></li>';

                    if($enableCaptureModule && ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('monetico','allowEditMoneticoCaptures')==1))))
                        $this->localMenu .='<li rel="editCaptures"'.(($_REQUEST['submodule']=='editCaptures')?' class="active"':'').'><a href="{subdir}/mf/index.php?module=monetico&submodule=editCaptures">'.$l10n->getLabel('editCaptures','menu_title').'</a></li>';

                    if($enableBankFeedbackModule && ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('monetico','allowEditMoneticoBankFeedbacks')==1))))
                        $this->localMenu .='<li rel="editBankFeedbacks"'.(($_REQUEST['submodule']=='editBankFeedbacks')?' class="active"':'').'><a href="{subdir}/mf/index.php?module=monetico&submodule=editBankFeedbacks">'.$l10n->getLabel('editBankFeedbacks','menu_title').'</a></li>';


                    if(isset($_REQUEST['submodule'])){

                        if($_REQUEST['submodule']=='editPayments'){

                            $this->editPayments = new editPayments();
                            $this->editPayments->prepareData();
                        }
                        else if($_REQUEST['submodule']=='editCaptures'){

                            $this->editCaptures = new editCaptures();
                            $this->editCaptures->prepareData();
                        }
                        else if($_REQUEST['submodule']=='editBankFeedbacks'){

                            $this->editBankFeedbacks = new editBankFeedbacks();
                            $this->editBankFeedbacks->prepareData();
                        }
                    }
                }

            } else {
                $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
                $this->localMenu = '';
            }

        }
    }

    function render(&$mainTemplate){
        global $mf,$l10n;
        $locale = $mf->getLocale();

        if($mf->mode == 'backend'){
            if(isset($_REQUEST['module']) && ($this->pluginName == $_REQUEST['module'])){

                $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/ressources/templates/module.html');
                $mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);
                $mainTemplate = str_replace("{section}",$this->section,$mainTemplate);
                $mainTemplate = str_replace("{module-name}",$this->moduleName,$mainTemplate);
                $mainTemplate = str_replace("{module-id}",$this->pluginName,$mainTemplate);
                $mainTemplate = str_replace("{local-menu}",$this->localMenu,$mainTemplate);

                if(isset($_REQUEST['submodule'])){
                    if($_REQUEST['submodule']=='editPayments'){
                        $this->editPayments->render($mainTemplate);
                    }
                    else if($_REQUEST['submodule']=='editCaptures'){
                        $this->editCaptures->render($mainTemplate);
                    }
                    else if($_REQUEST['submodule']=='editBankFeedbacks'){
                        $this->editBankFeedbacks->render($mainTemplate);
                    }
                }
            }

        }
    }

    function setup(){

    }


    function getType(){
        return $this->pluginType;
    }

    function getName(){
        return $this->pluginName;
    }

    function getVersion(){
        return $this->pluginVersion;
    }

    function getDependencies(){
        return $this->dependencies;
    }







}

