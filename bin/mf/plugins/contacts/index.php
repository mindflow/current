<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';


class contactsPlugin implements plugin{

    function setup(){
        /*$contact = new contact();
        $contact->createSQLTable();

        return "Table \"".contact::$tableName."\" created sucessfully !";*/
    }

    function getName(){
        global $l10n;
        return $l10n->getLabel('contactsPlugin','name');
    }

    function getDesc(){
        global $l10n;
        return $l10n->getLabel('contactsPlugin','desc');
    }

    function getPath(){
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey(){
        return "contacts";
    }


    function getVersion(){
        return "1.1";
    }

    function getDependencies(){
        $dependencies = array();
        return $dependencies;
    }

    function init(){
        global $mf,$l10n,$config;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');


        //include custom contact class or base contact class by default
        if(isset($config['plugins']['contacts']['contact-class']))
            $mf->pluginManager->addRecord('contacts', 'contact', $config['plugins']['contacts']['contact-class']);
        else $mf->pluginManager->addRecord('contacts', 'contact', '/mf/plugins/contacts/records/contact.php');


        //"Datas" MENU
        $mf->pluginManager->loadModule("/mf/plugins/contacts","modules","contacts");

        if(class_exists('mfUserGroup') && $mf->mode=='backend') {
            mfUserGroup::defineUserRight('contacts', 'allowEditContacts',array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
                '/mf/plugins/contacts/languages/userRights_l10n.php'
            );
        }
    }
}

$mf->pluginManager->addPluginInstance(new contactsPlugin());




