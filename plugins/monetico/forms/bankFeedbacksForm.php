<?php
/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/dbForm.php';

class bankFeedbacksForm extends dbForm{

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)

    function init(){

        $this->loadL10n($GLOBALS['config']['website_pluginsdir'].'/monetico','forms');
        parent::init();

        //columns descriptions, including autogenerated form settings
        $this->data['htmlTableOpen_filter']             = array(
            'value' => '<div class="row"><div class="col-lg-6">',
            'dataType' => 'html',
            'valueType' => 'dummy',
        );

        $this->data['htmlTableHalf_filter']             = array(
            'value' => '</div><div class="col-lg-6">',
            'dataType' => 'html',
            'valueType' => 'dummy',
        );

        $this->data['htmlTableClose_filter']             = array(
            'value' => '</div></div>',
            'dataType' => 'html',
            'valueType' => 'dummy',
        );


        $this->data['keyword_filter'] = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'number',
            'div_attributes' => array('class' => 'col-lg-8'),
            'field_attributes' => array('onchange' => 'sendForm();'),
        );



        $this->data['deleted_filter']               = array(
            'value' => '',
            'dataType' => 'checkbox',
            'valueType' => 'boolean',
            'swap'=>1
        );

        $this->data['DateDebut_filter']         = array(
            'value' => '',
            'dataType' => 'date',
            'valueType' => 'date',
            'div_attributes' => array(
                'class' => 'col-lg-6'
            )
        );
        $this->data['DateFin_filter']               = array(
            'value' => '',
            'dataType' => 'date',
            'valueType' => 'date',
            'div_attributes' => array(
                'class' => 'col-lg-6'
            )
        );




        $this->showInEditForm = array(
            'tab_main'=>array('htmlTableOpen_filter', 'keyword_filter','deleted_filter','htmlTableHalf_filter', 'DateDebut_filter','DateFin_filter','htmlTableClose_filter' )
        );


        //static::$postFormHTML[] = "<script>";


        //static::$postFormHTML[] = "</script>";

    }




}
