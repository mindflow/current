<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/mf.php';


/**
 * Handles processing and display of the MindFlow frontend. The frontend displays a website or an application's data.
 *
 * The frontend class creates a $mf object and configures it for frontend display. It also loads the user configured template and substitutes the required markers for display the website.
 *
 */
class frontend{

    var $template = '';

    //initializes the CMS objects first so they can be exploited by all data processing functions
    function __construct(){

        global $mf,$l10n,$config,$logger;

        //speed measurement stat
        $GLOBALS['execution_start'] = microtime(true);


        //the $mf object holds the data and provides the standard cms function set to all modules, plugins and services
        $mf = new mf('frontend');

        //store a reference to this object the $mf object for further reference from all frontend modules
        $mf->xEnd = &$this;

        //load backend localization file for from editing stuff
        include DOC_ROOT.SUB_DIR.'/mf/languages/frontend_l10n.php';
        $GLOBALS['l10n_text']['frontend'] = $l10nText;

        include DOC_ROOT.SUB_DIR.'/mf/languages/frontend_l10n.php';
        $GLOBALS['l10n_text']['frontend'] = $l10nText;

        //log the frontend initialization
        //$logger->info($l10n->getLabel('main','init_frontend'));

        if(isset($config['frontend_template']))$this->template = $config['frontend_template'];
        else $this->template = SUB_DIR.'/mf/frontend/templates/mf-default';
    }


    //process all the functions that have to be executed for the data / page processing
    function prepareData(){
        global $mf,$config;

	    //Allows switching the locale of mindflow by specifying an ISO locale in the URL
	    if(isset($_REQUEST['mfSetLocale'])) {
		    $locales = array_keys($config['frontend_locales']);
		    if(in_array($_REQUEST['mfSetLocale'],$locales)){
			    $mf->info['data_editing_locale'] = $_REQUEST['mfSetLocale'];
		    }
	    }

        //load the templates
        require_once DOC_ROOT.SUB_DIR.$config['templates_directory'].'/templates.php';

        if(isset($_SESSION['current_fe_user'])){

            //reload current user in case his profile has been modified during a frontend operation (user profile self editing)
            $_SESSION['current_fe_user']->reload();
            $mf->currentUser = $_SESSION['current_fe_user'];
        }

        //then prepare backend data
        $mf->mode = 'frontend';

        $mf->formsManager->prepareData();
        $mf->pluginManager->prepareData();
    }

    //executes the render() function of every object and outputs the page
    function renderPage(){
        global $mf,$logger;

        $html = &$mf->html;

        //processing templates
        $mf->pluginManager->render($html);

        //adding custom CSS
        $additionnalCss = '';
        $cssList = $mf->getCssList();
        foreach($cssList as $css){
            $additionnalCss .= $css.chr(10);
        }
        $html = str_replace("{additional-css}",$additionnalCss,$html);

        //adding custom JS for the top of the page
        $additionalJS = '';
        $topJsList = $mf->getTopJsList();
        foreach($topJsList as $js){
            $additionalJS .= $js.chr(10);;
        }
        $html = str_replace("{additional-js-top}",$additionalJS,$html);

        //adding custom JS for the bottom of the page
        $additionalJS = '';
        $bottomJsList = $mf->getBottomJsList();
        foreach($bottomJsList as $js){
            $additionalJS .= $js.chr(10);
        }
        $html = str_replace("{additional-js-bottom}",$additionalJS,$html);

        //processing UI messages
	    $customMsgs=array();
	    foreach($mf->customMessages as $message) $customMsgs[] = $message.'</div>';
	    $mf->header = str_replace("{custom-messages}",implode(chr(10),$customMsgs),$mf->header);

	    $warnings=array();
        foreach($mf->warningMessages as $message) $warnings[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $html = str_replace("{warning-messages}",implode(chr(10),$warnings),$html);

        $errors=array();
        foreach($mf->errorMessages as $message) $errors[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $html = str_replace("{error-messages}",implode(chr(10),$errors),$html);

        $success=array();
        foreach($mf->successMessages as $message) $success[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $html = str_replace("{success-messages}",implode(chr(10),$success),$html);

        $infos=array();
        foreach($mf->infoMessages as $message) $infos[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $html = str_replace("{info-messages}",implode(chr(10),$infos),$html);

        $execDurationText = $mf->l10n->getLabel('main','execution_duration');
        $execTime = round (microtime(true) - $GLOBALS['execution_start'],4);

        $memUsageText = $mf->l10n->getLabel('main','memory_usage');
        $memory = convert(memory_get_usage(true)).$mf->l10n->getLabel('main','bytes');

        $html[] = "<!-- ".$execDurationText." ".$execTime. "ms / ".$memUsageText." ".$memory. "-->";
        $logger->notice($execDurationText." ".$execTime. "ms / ".$memUsageText." ".$memory);

        //$html[]= $mf->footer;
        echo implode(chr(10),$html);
    }

    // $bodyParams : attributes to be added to the page bodytag
    function getStandardHeader($bodyParams){
        global $mf,$l10n,$config;

        $header = file_get_contents(DOC_ROOT.SUB_DIR.$this->template.'/header.html');
        $header = str_replace("{charset}",$config['charset'],$header);
        $header = str_replace("{subdir}",SUB_DIR,$header);

        if(isset($mf->info['pageTitle']))$header = str_replace("{page-title}",strtoupper($mf->getLocale())." : ".$mf->info['pageTitle'],$header);
        else $header = str_replace("{page-title}",strtoupper($mf->getLocale())." : ".$l10n->getLabel('backend','backend_title'),$header);
        $header = str_replace("{body-params}",$bodyParams,$header);

        //adding custom CSS
        $additionnalCss = '';
        $cssList = $mf->getCssList();
        foreach($cssList as $css){
            $additionnalCss .= $css.chr(10);
        }
        $header = str_replace("{additional-css}",$additionnalCss,$header);

        //adding custom JS for the top of the page
        $additionalJS = '';
        $topJsList = $mf->getTopJsList();
        foreach($topJsList as $js){
            $additionalJS .= $js.chr(10);;
        }
        $header = str_replace("{additional-js-top}",$additionalJS,$header);

       // if(isset($config['backend_favicon']) && $config['backend_favicon'] != '')$favicon = $config['backend_favicon'];
       // else $favicon = '/mf/backend/templates/mf-default/ico/favicon.ico';
        $favicon = '';

        $header = str_replace("{backend_favicon}",$favicon,$header);

        return $header;
    }

    function getStandardFooter(){
        global $mf;

        $footer = file_get_contents(DOC_ROOT.SUB_DIR.$this->template.'/footer.html');
        $footer = str_replace("{subdir}",SUB_DIR,$footer);

        //adding custom JS for the bottom of the page
        $additionalJS = '';
        $bottomJsList = $mf->getBottomJsList();
        foreach($bottomJsList as $js){
            $additionalJS .= $js.chr(10);
        }
        $footer = str_replace("{additional-js-bottom}",$additionalJS,$footer);

        return $footer;
    }

    //clean up all ressources and free memory after MindFlow execution
    function freeRessources(){
        global $db;

        $db->closeDB();

    }
}