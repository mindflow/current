<?php
/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class page extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_pages';

    /*
     * allows storing a child page object for page hierarchies
     * this record is not populated when using the load() or loadPageFromRow() functions
     * look at the $mf->info['pageTree'] variable which stores the whole page tree for the current website and is initiated in class pages.php
     */
    var $children;



    function init(){
        global $mf,$l10n;

        parent::init();

        $children = array();

        static::$createTableSQL = "
          `hide_in_nav` int(11) NOT NULL DEFAULT 0,
          `url_path_segment` varchar(255) NOT NULL,
          `title` varchar(255) DEFAULT NULL,
          `nav_title` varchar(255) NOT NULL,
          `meta_description` varchar(255) DEFAULT NULL,
          `meta_keywords` varchar(255) DEFAULT NULL,
          `meta_robots` varchar(255) DEFAULT NULL,
          `type` varchar(255) NOT NULL,
          `is_siteroot` int(11) NOT NULL,
          `no_delete` int(11) NOT NULL,
          `shortcut` int(11) NOT NULL,
          `shortcut_mode` varchar(255) NOT NULL,
          `shortcut_to` varchar(255) NOT NULL,
          `template_name` varchar(255) NOT NULL,
          `template_content` longblob,
          `hide_from_sitemap` tinyint(4) NOT NULL DEFAULT 0,
          `hide_lastmod` tinyint(4) NOT NULL DEFAULT 0,
          `changefreq` varchar(10),
          `priority` DECIMAL(2,1) DEFAULT 0.5,
          `view_count` int(11) NOT NULL DEFAULT '0',
        ";



        static::$createTableKeysSQL = "";

        $dataPage = array(

            'hide_in_nav'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'url_path_segment'   =>  array(
                'value' => '',
                'dataType' => 'urlinput text',
                'valueType' => 'text',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-8'),
            ),
            'title'   =>  array(
                'value' => $l10n->getLabel('page','new_page'),
                'dataType' => 'input text',
                'valueType' => 'text',
                'processor' => '',
                'index' => 'title',
                'field_attributes' => array('class' => 'input-lg'),
                'div_attributes' => array('class' => 'col-lg-8'),
            ),
            'nav_title'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'valueType' => 'text',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-8'),
            ),
            'meta_description'   =>  array(
                'value' => '',
                'dataType' => 'textarea 8 2',
                'valueType' => 'text',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'meta_keywords'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'valueType' => 'text',
                'index' => 'tags',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'meta_robots'   =>  array(
                'value' => '',
                'dataType' => 'select',
                'valueType' => 'text',
                'possibleValues' => array(
                    "" => "",
                    "index,follow" => "index,follow",
                    "noindex,follow" => "noindex,follow",
                    "index, nofollow" => "index, nofollow",
                    "noindex, nofollow" => "noindex, nofollow",
                    "nosnippet" => "nosnippet",
                    "noodp" => "noodp",
                    "noarchive" => "noarchive",
                    "notranslate" => "notranslate",
                    "noimageindex" => "noimageindex",
                    "noydir" => "noydir",
                ),
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),

            'type'   =>  array(
                'value' => '0',
                'dataType' => 'select',
                'valueType' => 'number',
                'possibleValues' => array( 0 => $l10n->getLabel('page','file'), 1 => $l10n->getLabel('page','folder')),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'is_siteroot'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'no_delete'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'shortcut'   =>  array(
                'value' => '0',
                'dataType' => 'select',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'shortcut_mode'   =>  array(
                'value' => '0',
                'dataType' => 'select',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'shortcut_to'   =>  array(
                'value' => '0',
                'dataType' => 'select',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'template_name'   =>  array(
                'value' => 'default',
                'dataType' => 'template_name',
                'valueType' => 'text',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'template_content'   =>  array(
                'value' => array(),
                'dataType' => 'template_data',
                //'processor' => '',
                'valueType' => 'template',
                'div_attributes' => array(
                    'class' => 'templateContentBlock col-lg-8'
                ),
            ),
            'parent_uid'   =>  array(
                'value' => '1',
                'dataType' => 'record',
                'editType' => 'single',
                'editClass' => 'page',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-3'),

                'initItemsSQL' => array(
                    'SELECT' => 'title',
                    'FROM' => 'mf_pages',
                    'WHERE' => "deleted = '0' AND language='".$mf->getDataEditingLanguage()."' OR language='ALL'", //ALL required for root page
                    'ORDER BY' => 'title',
                    'ORDER DIRECTION' => 'DESC',
                ),
                'selectActions' => array(
                    'view' => 0,
                    'edit' => 1,
                    'create' => 0,
                    'delete' => 0,
                    'sort' => 0,
                    'clipboard' => 0
                ),
                'icon' => '<b class="glyphicon glyphicon-file"></b>',
                'debugSQL' => 0,

            ),
            'sitemap_title' => array(
                'value' => '<div class="row"><div class="col-lg-4 col-lg-offset-4"><h2>'.$l10n->getLabel('page','sitemap_title').'</h2></div></div>',
                'dataType' => 'html',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'hide_from_sitemap'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'hide_lastmod'   =>  array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'changefreq'   =>  array(
                'value' => '',
                'dataType' => 'select',
                'valueType' => 'text',
                'possibleValues' => array(
                    "" => "",
                    "always" => $l10n->getLabel('page','always'),
                    "hourly" => $l10n->getLabel('page','hourly'),
                    "daily" => $l10n->getLabel('page','daily'),
                    "weekly" => $l10n->getLabel('page','weekly'),
                    "monthly" => $l10n->getLabel('page','monthly'),
                    "yearly" => $l10n->getLabel('page','yearly'),
                    "never" => $l10n->getLabel('page','never'),
                ),
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'priority'   =>  array(
                'value' => '0.5',
                'dataType' => 'input text',
                'valueType' => 'text',
                'index' => 'tags',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'view_count'   =>  array(
                'value' => 0,
                'editable' => false,
                'dataType' => 'input text',
                'div_attributes' => array(
                    'class' => 'col-lg-2'
                )
            ),
        );

        //merge with dbRecord $this->data using array_merge_recursive_distinct() to owerwrite parent_uid definition
        $this->data = array_merge_recursive_distinct($this->data,$dataPage);

        $this->showInEditForm = array(
            'tab_content'=>array('title','nav_title','template_content'),
            'tab_meta'=> array('uid','hide_in_nav', 'parent_uid', 'type', 'alt_language', 'template_name',  'is_siteroot', 'no_delete'),
            'tab_stats'=> array('view_count'),
            'tab_seo'=> array('url_path_segment','meta_description', 'meta_keywords', 'meta_robots','sitemap_title','hide_from_sitemap', 'hide_lastmod', 'changefreq', 'priority'),
        );


    }

    static $enableImportInitializationData = 1;

    static function importInitializationData(&$html=array()){
        global $pdo;

        //create ROOT page
        $sql = "INSERT INTO `mf_pages` (`uid`, `deleted`, `hidden`, `sorting`, `creation_date`, `modification_date`, `creator_uid`, `edit_lock`, `start_time`, `end_time`, `language`, `alt_language`, `hide_in_nav`, `parent_uid`, `url_path_segment`, `title`, `nav_title`, `meta_description`, `meta_keywords`, `meta_robots`, `type`, `is_siteroot`, `no_delete`, `shortcut`, `shortcut_mode`, `shortcut_to`, `template_name`, `template_content`, `hide_from_sitemap`, `changefreq`, `priority`, `view_count`, `hide_lastmod`) VALUES
(1, 0, 0, 0, '1970-01-01 00:00:00', '1970-01-01 00:00:00', 0, 0, '1970-01-01 00:00:00', '9999-01-01 00:00:00', 'ALL', NULL, 0, 0, '', 'ROOT', '', NULL, NULL, NULL, '', 0, 0, 0, '', '', '', NULL, 0, '', '0.5', 0, 0);";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $html[] = "<strong>Root page created sucessfully !</strong>";

    }


    function preCreate(){
        //Make sure the parent_uid is never lower thant root page's uid
        if($this->data['parent_uid']['value'] < 1)$this->data['parent_uid']['value'] = 1;
    }

    function postCreate(){
        global $mf;

        //current page should not be listed in parent_uid list (would create infinite loop)
        $this->data['parent_uid']['initItemsSQL']['WHERE'] = "deleted = '0' AND uid != ".$this->data['uid']['value']." AND language='".$mf->getDataEditingLanguage()."' OR language='ALL'";
    }


    function postLoad(){
        global $mf;

        //current page should not be listed in parent_uid list (would create infinite loop)
        $this->data['parent_uid']['initItemsSQL']['WHERE'] = "deleted = '0' AND uid != ".$this->data['uid']['value']." AND language='".$mf->getDataEditingLanguage()."' OR language='ALL'";

    }

    function preStore(){
        $this->index();
    }


    function store($updateDates = true, $forceCreate = false, $debug = false, $forceLocale = null){
        global $mf,$l10n;

        if($this->data['url_path_segment']['value']=='')$this->data['url_path_segment']['value'] = GenerateUrl($this->data['title']['value']);
        else{
            // if url_path_segment == "new_page" && title != "new page", update the path segment with the real title
            $defaultPathSegment = GenerateUrl($l10n->getLabel('page','new_page'));
            if( $this->data['url_path_segment']['value'] == $defaultPathSegment && GenerateUrl($this->data['title']['value']) != $defaultPathSegment){
                $this->data['url_path_segment']['value'] = GenerateUrl($this->data['title']['value']);
            }
            //always clean up the segment to make sure the user has not added some bad characters
            $this->data['url_path_segment']['value'] = GenerateUrl($this->data['url_path_segment']['value']);
        }

        //Fix for infinite loops in case this page parent_uid = this page uid
        if($this->data['parent_uid']['value'] == $this->data['uid']['value']){
            //attach the page to the root page so it still can be seen, and prevent memory exhaustion
            $this->data['parent_uid']['value'] = 1;
        }

        //Make sure the parent_uid is never lower thant root page's uid
        if($this->data['parent_uid']['value'] < 1)$this->data['parent_uid']['value'] = 1;

        $status = parent::store($updateDates, $forceCreate, $debug, $forceLocale);
        return $status;
    }


    //desactivates a record in the database. The record can be recovered by switching its deleted field back to 0.
    function delete(){
        global $mf;

        //we will not allow deleting a site root page
        if($this->data['is_siteroot']['value'] != '1'){
            $this->data['deleted']['value'] = '1';
            $this->store();
        }
        else {
            $mf->db->closeDB();
            die('{"jsonrpc" : "2.0", "error" : "error", "message" : "'.$mf->l10n->getLabel('page','delete_prohibited').'"}');

        }
    }

    function postDelete(){
        //remove the page from the search engine index
        if(class_exists('searchIndexer'))searchIndexer::unindexItem($this);
    }







    /**
     * Creates a page from already extracted row information
     * Saves some SQL requests...
     * @param $row the existing result of an SQL request
     */
    function loadPageFromRow($row){
        foreach($this->data as $key => $value){
            //$this->data[$key]['value'] = $row[$key];
            $this->loadKey($row, $key,$value);
        }
        //echo "*** loadPageFromRow ***";
        //print_r(unserialize($this->data['template_content']['value']));
        //echo "<br /><br /><br /><br />";
        //echo "titre=".$this->data['title']['value']." ".$this->data['uid']['value']."<br /><br />";
        //$this->data['template_content']['value'] = unserialize($this->data['template_content']['value']);
    }







    function getPreviewUrl(){
        return $this->getCanonicalUrl();
    }

    function getCanonicalUrl(){
        global $mf;

        return $mf->modules->urlRewriter->getPageURL($this->data['uid']['value']);
    }



    /*
     * Adds this item to the search index
     */
    function index(){
        global $mf;

        //build indexable values
        $title = '';
        $digest = '';
        $tags = '';

        foreach($this->data as $key => $dataEntry){

            if(isset($dataEntry['dataType'])) {
                $dataType = explode(' ',$dataEntry['dataType']);
                $dataType = $dataType[0];

                if ($dataType == 'template_data') {
                    //echo "* TRIGGER templateData<br /><br />".chr(10).chr(10);
                    $this->indexTemplateData($digest, $tags, $dataEntry['value']);
                }
                else if ($dataType == 'microtemplate') {
                    //echo "* TRIGGER microtemplate<br /><br />".chr(10).chr(10);
                    $this->indexMicrotemplate($digest, $tags, unserialize($dataEntry['value']));
                }
                //fields are indexed only if they have an 'index' property
                else if(isset($dataEntry['index'])){

                    if ($dataType == 'function') {
                        //functions
                        //calling the function and inserting its output
                        $functionOutput = '';
                        $functionName = $dataEntry['processor'];

                        if ($functionName != '') eval('$functionOutput =' . $functionName . '($page,$key,$dataEntry[\'value\']);');
                        //echo "functionOutput=" . $functionOutput . chr(10);
                        if ($dataEntry['index'] == 'title') $title .= ' ' . trim(html_entity_decode(strip_tags($functionOutput)));
                        if ($dataEntry['index'] == 'digest') $digest .= ' ' .trim(html_entity_decode(strip_tags($functionOutput)));
                        if ($dataEntry['index'] == 'tags') $tags .= ' ' . $functionOutput;
                    }
                    else {
                        if ($dataEntry['index'] == 'title') $title .= ' ' .trim(html_entity_decode(strip_tags($dataEntry['value'])));
                        if ($dataEntry['index'] == 'digest') $digest .= ' ' .trim(html_entity_decode(strip_tags($dataEntry['value'])));
                        if ($dataEntry['index'] == 'tags') $tags .= ' ' . $dataEntry['value'];

                    }
                }
            }
        }

        //remove html tags from rich text
        $digest = strip_tags($digest);
        //make sure _all_ html entities are converted to the plain ascii equivalents - it appears
        //in some MS headers, some html entities are encoded and some aren't
        $digest = html_entity_decode($digest, ENT_QUOTES, 'UTF-8');
        $tags = $this->data['meta_keywords']['value'];

        //build ressource url
        $ressourceUrl = $mf->getPageURL($this->data['uid']['value']);

        //index the item
        if(class_exists('searchIndexer'))searchIndexer::indexItem($this,$title,$digest,$tags,$ressourceUrl);

    }

    function indexTemplateData(&$digest,&$tags,$value){

        foreach($value as $key => $dataEntry){

            if(isset($dataEntry['dataType'])) {
                $dataType = explode(' ', $dataEntry['dataType']);
                $dataType = $dataType[0];

                if ($dataType == 'microtemplate')
                    $this->indexMicrotemplate($digest, $tags, $dataEntry['value']);

                //fields are indexed only if they have an 'index' property
                else if(isset($dataEntry['index'])){

                    if ($dataType == 'function') {
                        //functions
                        //calling the function and inserting its output
                        $functionOutput = '';
                        $functionName = $dataEntry['processor'];
                        if ($functionName != '') eval('$functionOutput =' . $functionName . '($this,$key,$dataEntry[\'value\']);');

                        if ($dataEntry['index'] == 'digest') $digest .= ' ' . trim(strip_tags($functionOutput));
                        if ($dataEntry['index'] == 'tags') $tags .= ' ' . $functionOutput;
                    }
                    else {
                        if(!is_array($dataEntry['value'])){ //we are not indexing array values, such as files entries yet
                            if ($dataEntry['index'] == 'digest') $digest .= ' ' . trim(strip_tags($dataEntry['value']));
                            if ($dataEntry['index'] == 'tags') $tags .= ' ' . $dataEntry['value'];
                        }
                    }
                }
            }
        }
    }

    function indexMicrotemplate(&$digest,&$tags,$value){

        foreach($value as $mtKey => $mtData){
            if(isset($mtData['microtemplate_data'])){

                $this->indexTemplateData($digest, $tags, $mtData['microtemplate_data']);

            }
        }
    }

    /*
     * adds all instances of this class to the search index
     */
    function indexAll(){
        global $pdo;

        //retreive list of active records with one single request (best performance)
        $sql = "SELECT * FROM ".static::getTableName()." WHERE deleted=0";
        //hidden=0 AND (start_time < NOW() OR start_time == '0000-00-00 00:00:00') AND (end_time > NOW() OR end_time == '0000-00-00 00:00:00')
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array());

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($stmt->rowCount() > 0){

            //update
            foreach($rows as $row){

                //load the data of the current row in the current class instance
                foreach($this->data as $key => $value){
                    $this->loadKey($row, $key,$value);
                }
                //don't forget to apply custom load() function overload below, such as unserialization of values and such stuff
                //print_r($this->data);
                $this->index();
            }
        }
    }


}
