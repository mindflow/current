<?
/**

 * Created by JetBrains PhpStorm.

 * User: Alban

 * Date: 05/10/12

 * Time: 15:57

 * To change this template use File | Settings | File Templates.

 */

$l10nText = array(

    'en' => array(
        'tab_contact' => 'Properties',
        'civility' => 'Civility',
        'first_name' => 'Name',
        'last_name'=> '',
        'organization'=> 'Organization / Company',
        'email'=> 'Email',
        'phone'=> 'Phone',
        'recipient'=> 'Recipient',
        'subject'=> 'Sujet',
        'message'=> 'Message',
        'mr' => 'Mr',
        'mrs' => 'Mrs',

        /*mandatory records*/
        'no_record' => 'There currently exists no contact request. You can <a href="{subdir}/mf/index.php?module=contacts&action=create">create a contact request</a>.',
    ),

    'fr' => array(
        'tab_contact' => 'Propriétés',
        'civility' => 'Civilité',
        'first_name' => 'Prénom',
        'last_name'=> 'Nom',
        'organization'=> 'Organisme / Société',
        'email'=> 'Email',
        'phone'=> 'Téléphone',
        'recipient'=> 'Destinataire',
        'subject'=> 'Sujet',
        'message'=> 'Message',
        'mr' => 'Mr',
        'mrs' => 'Mme',
        'visit' => 'Je souhaite visiter votre exploitation le ',
        'infos_produits' => 'Je souhaite recevoir des informations sur vos produits et votre entreprise',

        /*mandatory records*/
        'no_record' => 'Il n\'existe pas de demande de contact pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=contacts&action=create">créer une demande de contact</a>.',
    ),

);



?>