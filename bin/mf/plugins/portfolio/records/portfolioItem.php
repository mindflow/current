<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once 'portfolioCategory.php';

class portfolioItem extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_portfolio';

    static $createTableSQL= "
          `category` varchar(255) DEFAULT NULL,
          `title` varchar(255) DEFAULT NULL,
          `portfolio_date` date DEFAULT NULL,
          `summup` text NOT NULL,
          `text` text NOT NULL,
          `images` text NOT NULL,
          `slug` varchar(255) NOT NULL,
    ";

    static $createTableKeysSQL="
        UNIQUE KEY `URL` (`slug`)
    ";

    var $categories;

    function init(){

        //load translations
        //$this->loadL10n('/mf/plugins/portfolio','records');

        parent::init();

        $category = new portfolioCategory();

        $this->data = array_merge($this->data, array(

            'title'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array('class' => 'input-lg'),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'portfolio_date'   =>  array(
                'value' => '',
                'dataType' => 'date',
                'valueType' => 'date',
                'preDisplay' => 'portfolioItem::formFormatDate',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('0000-00-00','00/00/0000',''),
                ),
                'div_attributes' => array(
                    'class' => 'col-lg-2'
                )
            ),
            'summup'   =>  array(
                'value' => '',
                'dataType' => 'textarea 50 3',
                'div_attributes' => array(
                    'class' => 'col-lg-6'
                )
            ),
            'text'   =>  array(
                'value' => '',
                'dataType' => 'rich_text',
                'div_attributes' => array(
                    'class' => 'col-lg-8'
                )
            ),
            'images' => array(
                'dataType' => 'files jpg,jpeg,gif,png 620 0',
                'value' => '',
                'processor' => 'imagesPortfolio',
                'div_attributes' => array(
                    'class' => 'col-lg-8'
                ),

            ),
            'category'   =>  array(
                'value' => '',
                'possibleValues' => $category->listCategoriesByUid(),
                'dataType' => 'select',
                'div_attributes' => array(
                    'class' => 'col-lg-4'
                )
            ),
            'slug'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array('class' => 'input-lg'),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

        ));



        $this->showInEditForm = array(
            'tab_portfolioItem'=> array('category','hidden','title','portfolio_date','summup', 'text','images')
        );

    }


    function store($updateDates = true, $forceCreate = false, $debug=false){

        $this->data['slug']['value'] = GenerateUrl($this->data['title']['value']);
        return parent::store($updateDates,$forceCreate, $debug);
    }

    function getCategoryName($key,$uid,$locale,$row){
        if(!$this->categories){ //performance tweak, categories are loaded only once if repetitive calls
            $category = new portfolioCategory();
            $this->categories = $category->listCategoriesByUid();
        }
        if(isset($this->categories[$uid]))return $this->categories[$uid];
        else return '';
    }

    function getPreviewUrl(){
        global $mf,$config;

        if(isset($config['plugins']['portfolio']['single-item-page-uid'])){
            return $mf->getPageURL($config['plugins']['portfolio']['single-item-page-uid'][$mf->getDataEditingLanguage()]).'?news='.$this->data['slug']['value'];

        }
        return 'GLOBALS[config][plugin-portfolio][single-item-page-uid] is not defined';
    }

    static function formFormatDate($key,$value,$locale,$additionalValue=''){
        if($value=='30/11/-0001')return '00/00/0000';
        else return $value;
    }


}


