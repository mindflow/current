<?php
$l10nText = array(

    'fr' => array(
        'tab_main' => "Main",
        'sql_request' => "Requête SQL",
        'sql_result' => "Résultat SQL",
        'sql_rule' => "Règle SQL",
        'destination' => "Destination",
        'state' => "Etat",
        'result' => "Résultat",
        'local_log' => "Log local",
        'remote_log' => "Log distant",
        'editor_class' => "Classe éditeur",
        'editor_username' => "Nom d'utilisateur éditeur",
        'editor_uid' => "Uid éditeur",
        'Sending' => "Envoi",
        'Scheduled' => "Envoi programmé",
        'Inactive' => "Inactive",
        'Success' => "Succès",
        'Failure' => "Echec",
        'replay' => "Rejouer la requête",



        /*mandatory records*/
        'record_name' => "Requête SQL sortante",
        'new_record' => "Ajout requête SQL sortante",
    ),

);

?>