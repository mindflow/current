<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'menu_title' => 'Pages',
        'page_recorded' => 'The page has been sucessfully recorded',
        'add_page' => 'Add a page',
        'page_managment' => 'Page managment',
        'manage_pages' => 'Manage pages',
        'editing' => 'Editing',
        'showhide' => 'Activate / disable page',
        /*mandatory records*/
        'no_record' => 'There currently exists no page. You can <a href="{subdir}/mf/index.php?module=pages&action=create">create a page</a>.',
        'add_microtemplate' => 'Add element',
        'select_microtemplate'  => 'Select element type',
        'page_noexist'  => 'Sorry the requested page does not exist !',
        'no_pages'  => 'No root page seems to be defined in the database. Create a page first and check its "Website Root" property.',

    ),

    'fr' => array(
        'menu_title' => 'Pages',
        'page_recorded' => 'La page a été enregistrée avec succès',
        'add_page' => 'Ajouter une page',
        'page_managment' => 'Gestion des pages',
        'manage_pages' => 'Gerer les pages',
        'editing' => 'Edition',
        'showhide' => 'Activer / désactiver la page',
        /*mandatory records*/
        'no_record' => 'Il n\'existe pas de pages pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=pages&action=create">créer une page</a>.',
        'add_microtemplate' => 'Ajouter un élément',
        'select_microtemplate'  => 'Sélectionnez le type d\'élément',
        'page_noexist'  => 'Désolé, la page demandée n\'existe pas !',
        'no_pages'  => "Aucune page racine n'a été trouvée dans la base de données. Créez d'abord une page et cochez lui la propriété \"Racine du site\".",
    ),
);