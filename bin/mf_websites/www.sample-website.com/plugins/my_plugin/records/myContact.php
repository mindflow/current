<?php

/*
 * Classe pour enregistrer et afficher les profils sophrologue en backend
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class myContact extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'my_contacts';

    static $createTableSQL= "
        `civility` tinyint(4) DEFAULT 0,
        `name` varchar(255) DEFAULT '',
        `password` varchar(255) DEFAULT '',
        `email` varchar(255) DEFAULT '',

    ";

    static $createTableKeysSQL="
          KEY `fk_vz_user_email` (`email`(12)),
          KEY `fk_vz_user_email_pwd` (`email`(12),`password`(12)),
          FULLTEXT KEY `vz_user_search` (`name`,`email`)
        ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)
    static $civility=array();



    var $memberGroup;

    function init(){
        parent::init();

        $mf = &$GLOBALS['mf'];
        $l10n = $mf->l10n;
        if(!isset($GLOBALS['l10n_text']['myContact']))$this->loadL10n($GLOBALS['config']['website_pluginsdir'].'/my_plugin','records');

        if(sizeof(self::$civility)==0)
            self::$civility = array(
                0 => '',
                1 => $l10n->getLabel('myContact','mr'),
                2 => $l10n->getLabel('myContact','mrs'),
                3 => $l10n->getLabel('myContact','miss'),
            );

        $this->data['civility']                  = array(
            'value' => '',
            'dataType' => 'select',
            'valueType' => 'number',
            'possibleValues' => self::$civility,
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('',0)
            ),
            'field_attributes' => array('class' => 'input-md'),
            'div_attributes' => array('class' => 'col-lg-2'),
        );

        $this->data['name']                  = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('')
            ),
            'field_attributes' => array(
                'class' => 'input-lg',
                'onChange' => 'nameChanged()',
            ),
            'div_attributes' => array('class' => 'col-lg-4'),
        );


        $this->data['email']             = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'filterFunc' => '$this->validateEmail'
            ),
            'div_attributes' => array('class' => 'col-lg-4'),
        );


        $this->data['password']            = array(
            'value' => '',
            'dataType' => 'input password',
            'valueType' => 'text',
            'preRecord' => '$this->cryptPassword',
            'preDisplay' => 'myContact::showPasswordDummy',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('')
            ),
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        $this->data['password_confirmation']            = array(
            'value' => '',
            'dataType' => 'dummy-input password',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'filterFunc' => '$this->confirmPassword'
            ),
            'div_attributes' => array('class' => 'col-lg-4'),
        );



        //disable editing of some fields
        $this->data['creation_date']['field_attributes']['disabled'] = '';
        $this->data['modification_date']['field_attributes']['disabled'] = '';
        $this->data['creation_date']['editable']=0;
        $this->data['modification_date']['editable']=0;




        $this->showInEditForm = array(

            'tab_main'=>array(
                'creation_date','modification_date', 'civility', 'name',  'password', 'password_confirmation',
                'email',
            )
        );


        static::$postFormHTML[] = "<script>

            function nameChanged(){
                alert('name changed !');
            }

            </script>";

    }



    /**
     * Vérifie si un utilisateur existe déjà sur la base de son adresse email
     * @param $email string l'adresse email de l'utilisateur
     * @return bool true si l'utilisateur existe, sinon false
     */
    static function checkUserExists($email){
        $mf=$GLOBALS['mf'];
        $pdo = $mf->db->pdo;

        $sql = "SELECT uid FROM ".static::$tableName." WHERE deleted=0 AND email=".$pdo->quote($email);
        $stmt = $pdo->query($sql);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if($stmt->rowCount()>0){

            return true;
        }
        else return false;
    }


    function validateEmail($email){
        if(!filter_var ( $email, FILTER_VALIDATE_EMAIL))return false;
        else return true;
    }

    static function showPasswordDummy($key,$value,$locale,$additionalValue=''){
        return 'password';
    }

    static function getCivility($key,$value,$locale,$row){
        $mf = &$GLOBALS['mf'];
        $l10n = $mf->l10n;

        if(sizeof(self::$civility)==0)
            self::$civility = array(
                0 => '',
                1 => $l10n->getLabel('myContact','mrs'),
                2 => $l10n->getLabel('myContact','mr'),
            );
        return self::$civility[$value];
    }

    function cryptPassword($newValue, $previousValue){
        if($newValue != 'password'){
            //on stocke le mot de passe dans la session pour pouvoir le glisser dans l'email de confirmation avant qu'il soit définitivement crypté
            $_SESSION['user_password'] = $_REQUEST['record|password'];
            //if the password has changed, crypt the new password
            return password_hash($newValue, PASSWORD_BCRYPT, array("cost" => 10));
        }
        //else do not alter the password, as it is already encrypted
        else {
            //echo "Password is the same, cryptPassword returns ".$previousValue."<br />";
            return $previousValue;
        }
    }

    function confirmPassword($newValue){

        if($_REQUEST['record|password'] != 'password' or $newValue != ''){
            //on stocke le mot de passe dans la session pour pouvoir le glisser dans l'email de confirmation avant qu'il soit définitivement crypté
            $_SESSION['user_password'] = $_REQUEST['record|password'];
            $hash =  password_hash($_REQUEST['record|password'], PASSWORD_BCRYPT, array("cost" => 10));

            if($_REQUEST['record|password'] == $this->data['password']['value'] or $hash == $this->data['password']['value']){
                //echo "password has not changed confirmPassword returns 1<br />".chr(10);
                return 1;

            }
            //else validate
            else {
                //if the password has changed, test against confirmation
                //echo "password has changed, test against confirmation confirmPassword returns ".($_REQUEST['record|password_confirmation'] == $_REQUEST['record|password'])."<br />.chr(10)";
                return ($_REQUEST['record|password_confirmation'] == $_REQUEST['record|password']);
            }
        }
        else return 1;
    }






}















