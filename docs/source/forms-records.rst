.. |vspace| raw:: latex

   \vspace{5mm}

.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>
	
|embed_js|

*************************************************
Formulaires et enregistrements en base de données
*************************************************

============================================================
Les enregistrements de base de données et la classe dbRecord
============================================================

Que ce soit dans une application ou dans un site Internet, un développeur est très souvent appelé à extraire et écrire des données depuis une base de données. Néanmoins la syntaxe est complexe, et il est souvent fastidieux d'aller lire ou écrire ces données.
Dans MindFlow il est possible (et recommandé) de créer un objet hérité de la classe dbRecord pour simplifier l'accès à la base de données.

Ainsi, si on veut lire l'email du membre ayant pour uid 113, plutôt que d'écrire :

.. code-block:: php

	$sql = "SELECT email FROM members WHERE uid = ".$pdo->quote(113);
	$stmt = $pdo->query($sql);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	echo $row['email']

A partir du moment ou notre classe est définie, on peut écrire avec MindFlow:

.. code-block:: php

	$member = new member();
	$member->load(113);
	echo $member->data['email']['value'];

Et si on veut y modifier l'email d'un membre, plutôt que d'écrire :

.. code-block:: php

	$sql = "UPDATE members set email = ".$pdo->quote("john@doe.com")." WHERE uid = ".$pdo->quote(113);
	$stmt = $pdo->query($sql);

On peut écrire :

.. code-block:: php

	$member->data['email']['value'] = "john@doe.com";
	$member->store();
	
Dans les 2 cas, vous noterez que l'on lit ou qu'on écrit dans le tableau nommé 'data' contenu dans notre objet :

.. code-block:: php

	//lecture :
	echo $member->data['email']['value'];

	//écriture
	$member->data['email']['value'] = "john@doe.com";

Le tableau 'data' contient donc toutes les données de notre objet. MindFlow se charge ensuite de lire ou d'écrire son contenu en base de données.

Ce service de lecture / écriture est fourni de base par la classe dbRecord, et vous n'avez pas à écrire de code SQL pour le faire fonctionner. Il vous faut simplement respecter le format du  tableau data en respectant quelques règles élémentaires :

Un tableau data contient une clé pour chaque champ stocké en base de données. Chaque clé stocke un tableau contenant la définition du champ concerné. Si dans votre table vous avez une clé nommée 'name' alors vous la définirez comme suit :

.. code-block:: php

	$this->data['name'] = array(
		'value' => '', //valeur du champ
		'dataType' => 'input', //type de champ pour l'édition formulaire
		'valueType' => 'text', //type de la valeur du champ
	);

* ``value`` contient la valeur du champ, qui sera écrite ou lue dans la base de données.

|vspace|

* ``dataType`` indique le type de champ d'édition lorsqu'on affiche l'objet sous forme de formulaire (rappelez-vous, MindFlow génère les formulaires quasi automatiquement). Voir les valeurs possibles dans la section Les clés génériques du tableau 'data'.  

|vspace|

* ``valueType :`` pas vraiment obligatoire, mais permet de déterminer le type de valeur contenue dans le champ, indépendamment du type de champ d'édition. Voir les valeurs possibles dans la section Les clés génériques du tableau 'data'. Cette information est utile pour traiter la valeur de manière appropriée lors d'un export au format Excel CSV par exemple. Il est conseillé de remplir ce champ lors de la définition.

.. important::

	Lorsque vous accédez à la valeur d'un champ, attention à ne pas oublier de spécifier le sous-tableau ``['value']`` :

	.. code-block:: php
	
		echo $member->data['email']['value'];
		// BON : l'email est renvoyé
		
		echo $member->data['email'];
		// MAUVAIS : lève une erreur Notice: Array to string conversion
	
	

Pour pouvoir utiliser les services de dbRecord, il vous faut préalablement créer un objet hérité de cette classe. **Cette définition inclus la spécification SQL des colonnes de la table associée à l'objet.** Créons la classe member :

.. code-block:: php

	class member extends dbRecord { //on étend la classe dbRecord

		// nom de la table dans la BDD sur laquelle va travailler cet objet
		static $tableName = 'member';

		// définition de la table dans la BDD.
		// Celle-ci est utilisée pour pouvoir créer la table depuis l'installeur MindFlow
		
		static $createTableSQL= "
			`name` varchar(255) DEFAULT NULL,
			`email` varchar(255) DEFAULT ''
		";

		// on peut spécifier des clés d'indexation pour augmenter les performances 
		// ou pour effectuer des recherches fulltext
		
		static $createTableKeysSQL="
		  FULLTEXT KEY `member_search` (`name`,`email`)
		";

		// on initialise le tableau 'data' contenant les données de l'objet en précisant
		// les valeurs par défaut dans les champ 'value' et le type de champ d'édition
		// pour leur formulaire dans le champ 'dataType' :
		
		function init(){
		
			parent::init(); //Important ! Inclus les champs standard de la classe dbRecord (creation_date, hidden_deleted...) dans notre nouveau type d'enregistrement.

			// on charge le fichier de localisation qui contient les noms d'affichage
			// des champs suivants pour les être humains. Par exemple 'name' sera
			// traduit en 'Votre nom'
			$this->loadL10n('/mf/plugins/mf_users','objects');
		
			$this->data['name'] = array(
					'value' => '',
					'dataType' => 'input',
					'valueType' => 'text',
			);

			$this->data['email'] = array(
					'value' => '@',
					'dataType' => 'input',
					'valueType' => 'text',
			);

		}
	}

Après avoir défini ainsi votre objet, celui-ci devient utilisable.

Peut-être aurez-vous remarqué que le champ 'uid' qui apparaissait dans exemples précédent n'est pas cité dans la classe ? En fait, celui, ainsi qu'un certain nombre d'autres champs sont directement hérités de la classe dbRecord, qui fourni systématiquement les champs suivants :

.. code-block:: mysql

	`uid` int(11) NOT NULL AUTO_INCREMENT, # Identifiant unique de l'enregistrement
	`parent_uid` int(11) unsigned NOT NULL DEFAULT '0', # si l'enregistrement a été créé par un autre enregistrement et est en lien avec celui-ci, on peut stocker ici l'uid de l'enregistrement parent
    `parent_class` varchar(50) NOT NULL DEFAULT '', # si l'enregistrement a été créé par un autre enregistrement et est en lien avec celui-ci, on peut stocker ici la classe de l'enregistrement parent
	`deleted` tinyint(4) NOT NULL DEFAULT '0', # valeur à 1 quand l'enregistrement est effacé
	`hidden` tinyint(1) NOT NULL DEFAULT '0', # valeur à 1 quand l'enregistrement est masqué
	`sorting` int(11) NOT NULL, # index de tri pour ordonner plusieurs enregistrements
	`creation_date` datetime NOT NULL, # date de création de l'enregistrement
	`modification_date` datetime NOT NULL, # date de dernière modification
	`creator_uid` int(11) NOT NULL, # uid de l'utilisateur backend ou frontend à l'origine de la création de l'enregistrement
	`parent_class` varchar(50) NOT NULL DEFAULT '', # classe de l'utilisateur backend ou frontend à l'origine de la création de l'enregistrement
	`change_log` text NOT NULL, # log des modifications de l'enregistrement (encore inutilisé)
	`edit_lock` int(11) NOT NULL, # pour verrouiller un enregistrement (encore inutilisé)
	`start_time` datetime NOT NULL, # date d'activation d'un enregistrement (encore inutilisé)
	`end_time` datetime NOT NULL, # date de désactivation d'un enregistrement (encore inutilisé)
	`language` varchar(5) NOT NULL, # Code ISO langue de l'enregistrement
	`alt_language` tinytext, # uid des enregistrements similaires dans des langues différentes

**Tous ces champs sont créés et maintenus automatiquement par MindFlow. Vous n'avez rien à faire.** Lors de la création de la table de votre objet, lors de son instanciation ou de son enregistrement, ceux-ci sont gérés par la classe dbRecord dont est hérité votre objet. 

Vous avez néanmoins la possibilité de les lire ou de les modifier selon vos souhaits. Si par exemple vous souhaitez altérer la valeur du champ 'creation_date', il vous suffira d'y accéder via le tableau data :

.. code-block:: php

	$monObjet->data['creation_date']['value'] = '2016-03-02';
	
	//notez au passage l'usage ici de l'argument optionel 'false' de la fonction store, 
	//qui permet ici de forcer la date de création (sinon elle serait gérée automatiquement par dbRecord). 
	$monObjet->store(false);

|

.. note::
    **Enregistrements et localisation**

    Par défaut, MindFlow va créer une table nommée selon la variable ``$tableName`` que vous avez saisie dans la défintion de la classe.
    Lorsque vous utilisez plusieurs langues dans votre site ou votre application, par défaut, chaque enregistrement verra sa colonne ``language`` renseignée avec la locale courante lorsqu'il a été créé.
    Ainsi cette colonne prendra par exemple pour valeur 'fr' lorsque vous aurez enregistré une demande de contact sur votre site en français et 'en' lorsque cette demande de contact aura été créée sur le site anglais.

    Par defaut, la fonction showRecordEditTable() qui liste les enregistrements d'une table donnée n'affiche que les enregistrements dont la valeur du champ ``language`` correspond à la locale courante du backend
    (la locale du backend s'ajuste en sélectionnant la langue désirée dans la liste déroulante située en haut à droite du module d'administration).

    Il existe néanmoins des cas de figure rares, mais réels, pour lesquels un développeur préfèrera avoir une table séparée par locale, plutôt que tous les enregistrements de toutes les langues dans une même table.
    C'est le cas par exemple si vous aggrégez dans une même application Mindflow des données venues de plusieurs sites MindFlows indépendants et localisés chacun dans une langue différente, par exemple si vous centralisez dans un même outil d'administration les commandes venues de ces différents sites.
    Dans ce cas de figure, les UIDs (identifiants uniques des enregistrements) des différentes tables vont rentrer en collision. Il vous faudra alors une table différente par locale. Ceci se fait en ajustant la variable ``$oneTablePerLocale`` dans votre définition de classe :

    .. code-block:: php

		// nom de la table dans la BDD sur laquelle va travailler cet objet
		static $tableName = 'order';

        //il est possible de créer une table d'enregistrement par locale
        static $oneTablePerLocale = true;

    Si la variable ``static::$oneTablePerLocale`` est à ``true``, alors MindFlow va créer et gérer les tables ``order_fr`` et ``order_en`` dans cet exemple.

|
|
|
	
======================================	
Format des données : Array vs stdClass
======================================

A ce stade, vous vous demandez peut-être pourquoi nous avons fait le choix d'utiliser un objet de type array pour stocker les données. En effet, la syntaxe serait un poil plus agréable si on écrivait :

.. code-block:: php

	$monObjet->creationDate->value = '2016-03-02';

Il y a une première raison à cela : la performance. En effet les tableaux sont des objets lourdement optimisés dans tous les langages de programmation, et il offrent des performances supérieures lorsqu'il s'agit de lire et écrire des données.

**Voici le temps d'exécution d'un test de création de 1 million d'objets de type stdClass et d'1 million d'objets de type Array :**

.. code-block:: php

	stdClass=0.443578004837 sec
	
	Array=0.210082054138 sec
	
**Les objets Array sont 2 fois plus rapides à créer que les stdClass.**

La seconde raison, et la plus importante, est que les Arrays peuvent être facilement multidimensionnels et surtout disposent de toute **une foule de fonctions** d'itération, de fusion, de scission, de tri, d'empilement etc qui en font **la structure le plus approprié qui soit pour stocker des données et les manipuler.**

Leur plus gros défaut, en définitive, c'est leur syntaxe puisqu'il faut taper des crochets et des guillemets, soit 2 caractères de plus à chaque accès ([''] vs ->).  Espérons que les gains de performance et de possibilités obtenus vous motiveront à supporter cet affront !



========================================
Créer un formulaire ou un enregistrement
========================================

Spécification du formulaire
---------------------------

Avec MindFlow, il ne vous est plus nécessaire de rédiger le HTML pour créer un formulaire, ni d'écrire le code collectant les valeurs lors de sa soumission. Il vous faut juste spécifier les champs souhaités, avec la possibilité de personnaliser le formulaire dans une certaine mesure, qui tend à offrir de plus en plus de possibilités avec les nouvelles versions de MindFlow.

L'affichage des formulaires a recours à la bibliothèque Twitter Bootstrap qui permet de générer des formulaires responsive et mobile friendly.

Pour créer un formulaire, **vous devez créer une classe héritant soit de la classe** ``dbRecord`` **, soit de la classe** ``dbForm`` **:**

* **dbRecord :** crée un objet PHP qui pourra être affiché/édité sous forme de formulaire et qui pourra être sauvegardé en base de données. La création d'un objet dbRecord requiert qu'il existe dans la base de données une table comportant les champs listés dans le tableau 'data' de la classe définie.

|vspace|

* **dbForm :** crée un objet PHP qui pourra être affiché/édité sous forme de formulaire, mais qui n'a pas d'existence en base de données. Il vous appartient ensuite de traiter les valeurs obtenues selon vos besoins, en les glissant dans un mail par exemple.

Dans les 2 cas, **la génération de formulaire repose sur la définition saisie dans le tableau 'data' contenu dans la classe**, qui est 100% compatible entre les 2 classes. Ainsi il est possible de copier / coller la spécification du tableau data d'une classe héritée de dbRecord vers une classe héritée de dbForm, et vis-et-versa.

**Le tableau 'data' doit toujours être initialisé dans la fonction** ``init()`` **de l'objet** ``dbRecord`` **ou** ``dbForm`` **.**

Voici un exemple de formulaire avec 1 seul champ de type 'select' (select box) :

.. code-block:: php

	class myContact extends dbRecord{

		function init(){

		   //chargement du fichier de localisation situé
		   //dans le sous dossier 'objects' du plugin 'myContact'
		   $this->loadL10n('myContact','objects');

		   //creation d'un champ 'contact'
		   $this->data['contact'] = array(

			'value' => '1', //

			'dataType' => 'select',

			'possibleValues' => array(
					1 => 'Par téléphone et email',
					2 => 'Par téléphone',
					3 => 'Par email'
			),
			'valueType' => 'number',

			'field_attributes' => array(
				'class' => 'MyFieldClass MyOtherFieldClass', // Classe perso ou Bootstrap
				//appel d'une fonction JS lors d'un changement sur le champ
					'onChange' => 'updateOtherfield();'
				),

			
				'div_attributes' => array(
					'class' => 'col-lg-3' // Classe Bootstrap grid
				)
			);
		}

	}

Dans l'exemple précédent, on notera les clés suivantes :

* ``value :`` la valeur par défaut du champ, tel qu'elle sera affichée si le formulaire est affiché vide. Cette valeur par défaut sera substituée par la valeur saisie par l'utilisateur une fois le formulaire rempli, ou par la valeur chargée depuis la base de données si l'objet y est lu.
* ``dataType :`` le type de champ de formulaire qui sera affiché.
* ``valueType :`` type de la valeur pour export CSV
* ``possibleValues :`` cette clé n'existe que pour les champs de type select. On y défini un tableau associatif ' valeur' => 'texte' qui définira les champs <option> à afficher dans le input select.
* ``field_attributes :`` liste des attributs à ajouter au champ input. Vous pouvez spécifier ici n'importe quel attribut conforme à la norme HTML dans un tableau associant 'nomDelAttribut' => 'valeur'
* ``div_attributes :`` liste des attributs à ajouter au container <div> qui entoure le champ input. Vous pouvez spécifier ici n'importe quel attribut conforme à la norme HTML dans un tableau associant 'nomDelAttribut' => 'valeur'

|
|	
	
.. important::

    * **Toutes ces clés sont sensibles à la casse.** Si une clé saisie ne produit pas l'effet escompté, pensez à vérifier la casse et la syntaxe de la clé ajoutée.

    * **MindFlow va générer automatiquement pour chaque champ un ID** de la forme ``record|clé_du_champ`` ou ``template|clé_du_champ`` si le champ est enregistré dans un template (plus rarement).

    * **En javascript/jQuery, le caractère '|' est un caractère spécial** et il doit être préfixé des caractères '\\' pour être pris en compte. Par exemple :

    .. code-block:: javascript

        alert($("#record\\|nom_client").val());

    * **Quand vous générez du javascript depuis PHP, il est EN PLUS nécessaire de doubler les caractères spéciaux** quand vous voulez référencer votre champ :

    .. code-block:: javascript

        $html = 'alert($("#record\\\\|nom_client").val());';

    * **Attention à ne pas utiliser les mêmes noms de clés dans 2 objets dbRecord ou dbForm différents si vous êtes susceptible de les afficher à l'écran en même temps** ( par exemple sous forme de boites de dialogues superposées : vous ouvrez un fiche client à partir de l'enregistrement d'une commande qu'il a réalisé sur le site, et dans les 2 vous affichez un champ intitulé ``nom_client``). Dans ce cas, vous auriez 2 champs avec le même ID dans le DOM. MindFlow saura s'en débrouiller, mais si vous référencez $("#record\\\\|nom_client") depuis une fonction javascript pour accéder à votre champ, vous aurez 2 résultats possibles et donc un résultat inprédictible.


|
|

Sur la base de cette spécification, MindFlow génère le code HTML suivant:

.. code-block:: html

	<div class="contact col-lg-3" >
		<select name="record|contact" id="record|contact" class="form-control MyFieldClass MyOtherFieldClass" onChange="updateOtherfield();">
			<option value='1'selected>Par téléphone et email</option>
			<option value='2'>Par téléphone</option>
			<option value='3'>Par email</option>
		</select>
		<span class="errorMsg"></span>
	</div>

Ce qui produit cet affichage :

.. image:: images/champ-select.png
    :alt: Un champ select


On voit que le champ ``field_attributes`` appelle une fonction Javascript ``updateOtherfield()``. Comment ajouter cette fonction à notre formulaire ? Au moyen de la variable ``static::$postFormHTML`` qui est en fait un tableau, auquel on peut ajouter des blocs de HTML qu'on voudrait voir affichés à la suite du formulaire. On ajoutera le code suivant, toujours au sein de la fonction ``init()`` :

.. code-block:: html

	static::$postFormHTML[] = "
		<script>
			function updateOtherfield(){
			
				var contact = $('#record\\\\|contact').val();

				if(contact == 1 || contact == 3)
					$('#div_email').hide('slow');
				else $('#div_email').show('slow');
		   }
		</script>";

On pourrait aussi bien lier un fichier JavaScript ou une feuille de style de la même manière :

.. code-block:: php

	static::$postFormHTML[] = "<link rel='stylesheet' href='".SUB_DIR."/mf/plugins/my_plugin/ressources/templates/my_plugin.css' type='text/css' media='screen'>";
	
|
|

.. important::
	**Par défaut, notre champ ne s'affiche pas dans le formulaire. Il faut explicitement spécifier qu'il s'affiche et sa position**, via la variable ``$this->showInEditForm`` qui est encore une fois un Array :

	.. code-block:: php

		$this->showInEditForm = array(
			'tab_main'=>array( 'creation_date', 'modification_date', 'contact'),
			'tab_state'=>array( 'deleted','hidden')
		);

	La clé de la première dimension ('tab_main', 'tab_state') défini l'onglet du formulaire dans lequel seront affichés nos champs. Il y a une entrée par onglet : ``'tab_main'`` définit un onglet et ``tab_state`` en définit un autre.

    A chaque clé d'onglet correspond une valeur dans le fichier de localisation _l10n.php associé à la classe. **Cette valeur définit le titre de notre  onglet** tel qu'il apparaitra dans l'interface utilisateur :

    .. code-block:: php

		'fr' => array(
			'tab_main' => 'Infos générales',
			'tab_state' => 'Etat',
		)

    **A noter :**

    * Si vous ne définissez qu'une seule clé pour la première dimension du tableau ``$this->showInEditForm``, alors l'onglet n'apparaitra pas (mais son contenu oui), car celà n'a pas de sens d'afficher une seule languette d'onglet.

    |vspace|

    * Si vous définissez plusieurs clés, mais que l'indice de la première clé est une chaine vide, alors les champs définis dans cette première clé seront affichés en amont de la liste des onglets. Par exemple, ci-dessous on voulait pouvoir afficher un tableau HTML récapitulatif en amont de notre formulaire d'édition. On a donc défini un champ de type "HTML" intitulé "summup" contenant notre tableau récapitulatif :

            .. code-block:: php

                $this->showInEditForm = array(
                    '' => array('summup'),
                    'tab_inscriptions'=> array('options','inscrits' ),
                    'tab_main'=> array('circuit','vendeur_depart'),
                    //... autres onglets ...
                );

            Ce qui donne :

            .. image:: images/void-tab.jpg



	La seconde dimension du tableau ``$this->showInEditForm`` définit les champs qui doivent être affichés et l'ordre dans lequel ils doivent être affichés. Dans l'exemple ci-dessus, si vous déplacez ``'contact'`` entre ``'creation_date'`` et ``'modification_date'``, le champ sera affiché entre les deux dans le formulaire HTML.

	En effet, on peut avoir des champs que l'on veut avoir présents dans la base de données, mais qu'on ne veut pas afficher dans notre formulaire. Ou alors on pourra avoir un champ qu'on voudra afficher en backend, mais pas en Frontend. Dans ce cas, il suffit de l'ommetre de la liste avant d'afficher le formulaire par exemple. 
	
	Si les affichages de formulaire en backend et en frontend sont trop différents pour être conciliables dans une même classe, on pourra envisager de créer 2 classes différentes interragissant avec une même table dans la base de données. Il est également possible de créer une classe héritée d'un enregistrement hérité de dbRecord, pour lui ajouter des champs supplémentaires par exemple.

	Dans le code d'exemple ci-dessus, en plus du champ ``'contact'``, on affiche aussi les champs ``'creation_date', 'modification_date','deleted','hidden'`` qui ne sont pas spécifiés dans notre tableau data. C'est parce qu'ils sont hérités de la classe dbRecord et qu'ils sont déjà présents dans notre tableau 'data' que nous avons la possibilité de les reprendre et de les afficher

	.. image:: images/onglets.png
		:alt: Un champ select
	



|
|
|
	
	
	
Localisation des champs
-----------------------

Vous avez peut-être noté que dans notre formulaire d'exemple, lors de l'affichage, le champ ``'contact'`` est précédé d'un label 'comment vous contacter' qui n'est pas mentionné dans la spécification du tableau 'data'. De même les onglets 'tab_main' et 'tab_state' n'ont pas un nom très convivial et ne sont pas affiché en tant que tel en titre de nos onglets. Pourquoi ? C'est parce que leurs textes sont extraits depuis un fichier de localisation (en abrégé l10n), qui va rapatrier la valeur du label en fonction de la langue couramment affichée en Frontend ou en backend.

Un fichier de localisation est un fichier PHP nommé avec le nom de la classe à laquelle il correspond, **et terminé par ``_l10n.php``**. Pour la classe ``myContact``, on aura donc ``myContact_l10n.php``.

A l'intérieur, un fichier de localisation se présente comme suit :

.. code-block:: php

	<?php

	$l10nText = array(
		'fr' => array(
			'contact' => 'Comment vous contacter',
			'tab_main' => 'Infos générales',
			'tab_state' => 'Etat',
		),
		'en' => array(
			'contact' => 'How do we contact you',
			'tab_main' => 'Global info',
			'tab_state' => 'Status',
		),
	);

	?>
	
La variable ``$l10nText`` se voit affectée un tableau à 2 dimensions. **La clé de la première dimension est le code ISO2 de la langue traduite ('fr', 'en', 'de'...),** spécifiée en base de casse ('fr' et pas 'FR'). La seconde dimension est un tableau associant à la clé de localisation la traduction correspondante. Il faut évidement spécifier les mêmes clés dans chaque langue. Si une clé n'est pas trouvée, MindFlow retournera une chaîne vide.

Dans les objets hérités de la classe dbForm, le fichier de localisation est chargé en tête de notre fonction init() via la commande :
 
.. code-block:: php
  
	$this->loadL10n('myContact','objects');

**Dans les objets hérités de dbRecord, le fichier de localisation est recherché automatiquement** par le pluginManager et il n'est pas nécessaire de saisir cette commande dans votre fonction init(). Dans les objets dbForm, l'opération reste manuelle pour le moment. Si elle n'est pas réalisée, les textes localisés n'apparaîtront pas.

La commande ``$this->loadL10n('myContact','objects');`` charge dans **le domaine de localisation 'myContact'** le fichier de localisation nommé ``myContact_l10n.php`` présent dans le sous-dossier ``objects`` du dossier ``/monPlugin/languages/``

**Le domaine de localisation est une clé qui permet d'identifier un jeu de traductions.** Quand on veut afficher une traduction dans MindFlow, on appelle la fonction :

.. code-block:: php

	$mf->l10n->getLabel('nom_du_domaine','clé_de_localisation');

MindFlow se charge de rapatrier la clé traduite dans la langue d'affichage courante, sans qu'il soit nécessaire de préciser cette dernière.
Dans le cadre de notre formulaire d'exemple, si on veut rapatrier la valeur de la clé 'contact', on écrira donc :

.. code-block:: php

	echo $mf->l10n->getLabel('myContact','contact');
	//affiche : "Comment vous contacter"

Si pour une raison inconnue votre fichier de localisation ne semble pas être chargé, ou que vous souhaitez l'exploiter en dehors d'une classe héritée de dbRecord ou dbForm, ou que vous voulez placer votre fichier de localisation à un emplacement arbitraire, vous pouvez forcer son chargement à l'aide de la fonction :

.. code-block:: php

	l10nManager::loadL10nFile('nom_du_domaine','chemin_relatif_a_la_racine_du_site');
	
	
Afficher l'objet sous forme de formulaire
-----------------------------------------

Pour afficher notre objet, on appelle l'une des fonctions suivantes à partir de l'instance ``$mf->formsManager`` :

* ``editForm() :`` affiche un formulaire hérité de la classe dbForm
* ``editRecord() :`` affiche un formulaire hérité de la classe dbRecord

Exemple d'utilisation :

.. code-block:: php

	$objetContact = new myContact();

	$html = $mf->formsManager->editRecord($objetContact, $this->pluginName);
	
	echo $html;

Ces fonctions proposent des paramètres assez nombreux. Nous vous invitons à consulter la classe **formsManager** dans la documentation phpDoc ou son code source. Ces paramètres y sont documentés.

Récupérer les valeurs du formulaire
-----------------------------------

Pour récupérer un objet contenant les valeurs d'un formulaire, on emploiera la fonction ``processForm()`` de la classe formsManager. Qu'il s'agisse d'un formulaire hérité de dbForm, ou d'un objet hérité de dbRecord, la methode renverra directement une instance de l'objet concerné :

.. code-block:: php

    global $formsManager;

	$myObject = $formsManager->processForm();
	//ou
	$myObject = $formsManager->processForm($record = $myObject);

.. important::

    La fonction ``processForm()`` ne sauve pas l'objet renvoyé, d'une part parce que l'enregistrement dans la base de données ne s'applique pas aux objets hérités de dbForm, d'autre part parce que vous ne souhaitez pas nécessairemnt le faire pour un objet hérité de dbRecord.
    Il vous incombe donc d'executer un ``store()`` sur l'objet renvoyé si nécessaire.

.. note::

    L'argument optionnel ``$record`` de la fonction processForm() permet une optimisation de performance, notamment lorsque l'objet a déjà été créé et passé à travers la fonction checkAndProcessForm(), ce qui permet de le réutiliser au lieu de le recréer. Vous ne l'utiliserez pas dans la plupart des cas.

|vspace|

* Si l'argument storeRecord est à false, alors la fonction retourne l'instance de l'objet récupéré, avec toutes les valeurs du formulaire intégrées dans les champs 'value' du tableau 'data'. Cette instance n'est alors pas sauvée dans la base de données, et il vous incombera de lui appliquer un ``->store()`` pour ce faire.

|
|
|

=====================================================
Création d'objet vs enregistrement en base de données
=====================================================

Poursuivons avec notre exemple de classe myContact héritée de dbRecord. Pour créer une instance de myContact, comme on l'a déjà vu, on écrit :

.. code-block:: php

	$contact = new myContact();

A partir de ce moment là, notre objet existe en mémoire avec son tableau data initialisé par la fonction ``init()`` (celle-ci est appelée automatiquement lors de la création de l'objet), mais les données de notre objet n'existent pas encore en base de données. Ce n'est que lorsqu'on aura appelé la fonction ``$contact->store()`` que les données seront écrites dans la base :

.. code-block:: php

	$contactUid = $contact->store();


.. important::

	Que l'on réalise une création d'objet (``CREATE`` en SQL) ou une mise à jour (``UPDATE``), **on appelle toujours la fonction** ``store()`` **. C'est MindFlow qui se préoccupe de savoir s'il doit exécuter un** ``CREATE`` **ou un** ``UPDATE`` **.**

	La règle étant que **si le champ** ``$contact->data['uid']['value']`` **correspondant à la clé primaire de l'enregistrement est vide (valeur = '' ou valeur = '0') ou contient une valeur uid qui n'existe pas dans la table, alors MindFlow exécute un** ``CREATE`` **. Sinon il met à jour l'enregistrement avec l'uid correspondant au moyen d'un** ``UPDATE`` **.**



**La fonction** ``$contact->store()`` **retourne l'uid de l'objet en cas de succès, ou la valeur 0 en cas d'échec.** Ainsi il est possible de réaliser facilement une action conditionnelle en cas de succès ou d'échec :

.. code-block:: php

	if($contactUid = $contact->store()){
		
		echo "le contact ayant pour uid ".$contactUid." a été créé avec succès";
	}
	else{
		echo "La création du contact a échoué";
	}

	
========	
Triggers
========

Il est pratique de spécifier dans un objet dbRecord des actions qui accompagnent les enregistrements en base de données. Par exemple, on pourrait vouloir envoyer un mail automatiquement avec le login et le mot de passe à un utilisateur chaque fois qu'un profil myContact est créé. Dans ce cas, on pourra surcharger le code de la fonction ``postCreate()`` de dbRecord et rédiger le code d'envoi d'email. Voici les différents triggers de la même trempe.

.. code-block:: php

    function postLoad()

La fonction ``postLoad()`` est exécutée après qu'un enregistrement a été chargé depuis MySQL, via la fonction ``load()``. 

.. code-block:: php

    function preCreate()

La fonction ``preCreate()`` est exécutée avant qu'un enregistrement soit créé dans MySQL, via la fonction ``store()``. 

.. code-block:: php

    function postCreate()

La fonction ``postCreate()`` est exécutée après qu'un enregistrement soit créé dans MySQL, via la fonction ``store()``.

.. code-block:: php

    function preStore()

La fonction ``preStore()`` est exécutée avant qu'un enregistrement soit modifié dans MySQL, via la fonction ``store()``.
 
.. code-block:: php
   
    function postStore()

La fonction ``postStore()`` est exécutée après qu'un enregistrement soit modifié dans MySQL, via la fonction ``store()``.

.. code-block:: php

    function onSubmit()

La fonction ``onSubmit()`` est exécutée durant le traitement d'un formulaire par la fonction ``formsManager::processForm()`` (MindFlow > 1.4) ou ``formsManager::processSubmition()`` (MindFlow < 1.4). 
Cette fonction est destinée à être employée avec les objets hérités de dbForm, qui n'ont pas d'existence en base de données et pour lesquels les triggers de type postCreate() etc... ne s'appliquent pas.

.. code-block:: php

    function preDelete()

La fonction ``preDelete()`` est exécutée juste avant l'effacement d'un enregistrement, via la fonction ``delete()``. On pourra l'utiliser pour effacer un enregistrement associé dans une autre table par exemple. Cette fonction est destinée à être employée avec les objets hérités de dbRecord uniquement.

.. code-block:: php

    function postDelete()
	
La fonction ``postDelete()`` est exécutée juste après l'effacement d'un enregistrement, via la fonction ``delete()``.

.. code-block:: php

    function preRestore()

La fonction ``preRestore()`` est exécutée juste avant l'effacement d'un enregistrement, via la fonction ``delete()``. On pourra l'utiliser pour restaurer un enregistrement associé dans une autre table par exemple. Cette fonction est destinée à être employée avec les objets hérités de dbRecord uniquement.

.. code-block:: php

    function postRestore()

La fonction ``postRestore()`` est exécutée juste après l'effacement d'un enregistrement, via la fonction ``delete()``.

.. note:: Il est possible de désactiver temporairement les triggers. Par exemple, si vous importez un fichier CSV de 1000 contacts via un script, et que vous créez en boucle des objets ``myContact``, vous ne voudrez sans doute pas qu'a chaque création de contact, l'utilisateur reçoive un mail avec son login et son mot de passe comme défini dans le trigger ``postCreate()`` de notre classe ``myContact.``

Pour ce faire, avant de sauver notre contact, on écrira  :

.. code-block:: php

	$contact->enablePostCreate = false;
	$contact->store();

La valeur du flag ``$enablePostCreate`` n'est valable que durant la durée de vie de l'objet PHP courant. Elle n'est pas enregistrée en base de données.

Voici la liste des variables disponibles dans la classe dbRecord pour neutraliser les triggers :

.. code-block:: php

    var $enablePreCreate=true;
    var $enablePostCreate=true;
    var $enablePreStore=true;
    var $enablePostStore=true;
    var $enablePreLoad=true;
    var $enablePostLoad=true;
    var $enablePreDelete=true;
    var $enablePostDelete=true;
    var $enablePreRestore=true;
    var $enablePostRestore=true;
	
|
|
|

========
Flags
========

Il existe différentes variables drapeaux (flags) qui peuvent être ajustés dans la classe dbRecord, et par conséquent pour tous les enregistrements hérités de celle-ci :

* ``$enableHistory`` **lorsque ce drapeau est positionné à true, il active le stockage de l'historique de l'enregistrement de 2 manières.** D'une part le champ ``history_last`` stocke la dernière copie enregistrée du tableau data de l'objet, ce qui servira a établir une comparaison avec la nouvelle version à chaque appel de store(). D'autre part le champ ``history_past`` stocke les différences entre la version actuelle et les versions précédentes de l'enregistrement (le fait de ne stocker que les différences permet de réduire l'espace occupé dans la base de données et d'impacter le moins possible les performances en lecture/écriture). La sauvegarde de l'historique d'un enregistrement diminue les performances, aussi ce drapeau est-il positionné à false par défaut.

|vspace|

* ``$reloadFormOnSave`` **lorsque ce drapeau est positionné à true,  il force le re-téléchargement en AJAX et le ré-affichage du formulaire d'édition de l'enregistrement après sa sauvegarde.** C'est utile si certaines modifications ou calculs sont effectués au niveau serveur dans la procédure onStore() et qu'on veut les refléter immédiatement dans l'affichage du navigateur. Ce drapeau est positionné à false par défaut de manière à minimiser le trafic de données entre le serveur et le navigateur, la régénération du formulaire n'étant pas nécessaire dans la plupart des cas.


|
|
|

.. _keys:
	
==========================	
Les clés du tableau 'data'
==========================

Clés génériques 
---------------
**Selon les types de champs (définis et différenciés par la valeur du champ** ``'datatype'`` **), certaines clés supplémentaires peuvent être spécifiées. D'autres en revanche sont communes à tous types les champs. Voici leur documentation complète :**

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>value</strong></td>
				<td>valeur par défaut lorsque l'enregistrement est créé, ou valeur courante de l'enregistrement
				lorsque celui-ci a été extrait de la base
				de données via la fonction load() ou lorsque la valeur a été modifiée par l'utilisateur.<br /><br />

                <strong>Tous les types de champ ont un attribut 'value', à l'exception notable du champ recordEditTable</strong> qui ne stocke par d'information dans la table associée à la classe, mais qui donne une vue spécifiée par une requête SQL sur une table tierce.
				</td>
			</tr>
			<tr class="row-even"><td><strong>dataType</strong></td>
			<td>
			définit le type de champ de formulaire qui devra être affiché, parmi les valeurs suivantes :
				<ul>
					<li>input</li>
					<li>date</li>
					<li>datetime</li>
					<li>time</li>
					<li>color</li>
					<li>hidden</li>
					<li>urlinput</li>
					<li>checkbox</li>
					<li>select</li>
					<li>record</li>
					<li>rich_text</li>
					<li>textarea</li>
					<li>files</li>
					<li>template_name</li>
					<li>microtemplate</li>
					<li>template_data</li>
					<li>fieldset</li>
					<li>html</li>
				</ul>
					<br />
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>'dataType' => 'hidden'</pre>
					</div>
					</p>
					<p>
					Les rôles et les clés spécifiques de ces types de champs sont détaillés dans la section suivante.
					</p>

					<p>
					<strong>Additionnellement, il est possible de préfixer le datatype de la plupart des champs avec la valeur 'dummy-'.</strong> Ainsi 'input' devient 'dummy-input'. En anglais, un dummy est une maquette. <strong>On pourrait parler aussi ici de pseudo-champ : il s'agit de champs qui apparaissent dans le formulaire, mais qui ne seront pas traités pour l'enregistrement en base de données.</strong>
					</p>
					<p>
					Par exemple si on enregistre en base de données un champ 'password' de type 'input', on pourra créer a coté un champ 'password-confirmation' de type 'dummy-input' qui ne sera pas enregistrée en base de données, mais traité dans la fonction trigger preCreate() pour contrôler que les champs 'password' et 'password-confirmation' correspondent bien.
					</p>
				
			</td>
			</tr>
			<tr class="row-odd">
				<td><strong>valueType</strong></td>
				<td>
				<p>définit le type de valeurs pour l'export CSV ou le réutilisation des données, parmi les types suivants :
				<ul>
					<li>number,[optionnel nombre de chiffres après la virgule]. Exemple : number,2</li>
					<li>money,[optionnel nombre de chiffres après la virgule],[optionnel symbole monétaire]. Exemple : money,2,&euro;</li>
					<li>date</li>
					<li>time</li>
					<li>percent</li>
					<li>fraction</li>
					<li>text</li>
				</ul>
				</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'valueType' => 'time'</pre>
				</div>
				</p>

				</td>
			</tr>
			<tr class="row-even">
				<td><strong>editable</strong></td>
				<td>
				<p>Valeur par défaut à 1. Passer la valeur à 0 pour rendre le champ non éditable dans le formulaire.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'editable' => '0'</pre>
				</div>
				</p>

				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>label_attributes</strong></td>
				<td>
				<p>liste des attributs à ajouter au tag <label> associé au champ input. Vous pouvez spécifier ici n'importe quel attribut conforme à la norme HTML dans un tableau associant 'nomDelAttribut' => 'valeur'.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'label_attributes' => array(
			'class' => 'myClass',
		)
		</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>field_attributes</strong></td>
				<td>
				<p>liste des attributs à ajouter au container &lt;div&gt; qui entoure le champ input. Vous pouvez spécifier ici n'importe quel attribut conforme à la norme HTML dans un tableau associant 'nomDelAttribut' => 'valeur'.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'div_attributes' => array(
		'class' => 'myClass',
		'width'=>'256'
	)
	</pre>
				</div>
				</p>

				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>div_attributes</strong></td>
				<td>
				<p>liste des attributs à ajouter au container <div> qui entoure le champ input. Vous pouvez spécifier ici n'importe quel attribut conforme à la norme HTML dans un tableau associant 'nomDelAttribut' => 'valeur'.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'div_attributes' => array(
		'class' => 'myClass',
		'width'=>'256'
	)
	</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>validation</strong></td>
				<td>
				<p>Spécifie si un champ est obligatoire ou pas, ce qui provoquera l'affichage d'un message d'erreur dans le cas ou :
				<ul>
				<li>soit la valeur du champ est listée dans le tableau 'fail_values'</li>
				<li>soit la fonction indiquée dans le champ 'filter_func' retourne la valeur false plutôt que true.</li>
				</ul>
				Pour que la vérification soit active, il faut que la clé 'mandatory' soit fixée à la valeur 1.
				</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'validation' => array(
    'mandatory' => '1',
		'fail_values' => array('','0'),
		
		// OU si besoin de validation plus élaborée	
		'filterFunc' => 'myContact::validateMember',
		
		//affiche des infos durant le test de validation
		'debug' => false,
		
		//nombre d'entrées minimum requis 
		//(champs à sélection multiples uniquement : 
		//select, recordSelect, files, checkbox)
		'min_entries' => 1,
		
		//nombre d'entrées maximum requis
		//(champs à sélection multiples uniquement : 
		//select, recordSelect, files, checkbox)
		'max_entries' => 3,
	),
	</pre>
				</div>
				</p>
				<p>En cas d'échec, le message d'erreur renvoyé sera lu dans le fichier de localisation _l10n.php associé à l'objet PHP et sera celui défini dans la clé de localisation du champ suffixée de la valeur '_fail'. Par exemple :
				<div class="highlight-php5">
						<pre>
	'password' => 'Mot de passe',
	'password_fail' => 'Le mot de passe spécifié est vide',
	</pre>
				</div>
				</p>
				<p>
				Exemple de fonction de validation employée en conjonction avec filterfunc. On vérifie ici si le username existe déjà dans la BDD :
	Exemple de fonction :
				<div class="highlight-php5">
						<pre>static function validateMember($fieldValue, $fullSubmitedRecord, $fullExistingRecord){
        //vérification de la valeur du champ courant	
        if($fieldValue != '' && $fieldValue > 0) return true;
        else {
	//vérification d'un autre champ de l'enregistrement
       //et décision
	if ($fieldValue == 0 && $fullSubmitedRecord->data['otherfield']['value'] == 1)
		return true;
	else return false;
        }	
    }
	</pre>
				</div>
				<strong>Note : si la fonction retourne une chaîne de caractères au lieu d'un booleen, le résultat de la validation est toujours considéré comme false (échec) et la chaine de caractère retournée est affichée comme message d'erreur. Ceci permet d'afficher des messages d'erreur variés pour gérer des cas de figures plus complexes qu'un simple true/false. Par exemple : "La longueur du mot de passe doit être d'au moins 8 caractères."</strong>
                Le champ $fullExistingRecord fournit l'état de l'enregistrement existant dans la base de données s'il existe déjà et est modifié. Ainsi on peut comparer la valeur modifiée avec la valeur existante.
                Si l'enregistrement est créé sur le moment, alors $fullExistingRecord a pour valeur null.
				</p>

				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>preRecord</strong></td>
				<td>
				<p>Appelle une fonction pour traiter le champ concerné avant de l'enregistrer dans la base de données :</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'preRecord' => '$this->cryptPassword',</pre>
				</div>
				</p>
				<p>
				Note : la clé 'preRecord' est exécutée après la clé 'validation'.
				Voici le profil de la fonction cryptPassword. Notez les 2 paramètres : $newValue est la valeur tapée par l'utilisateur dans le formulaire, tandis que $previousValue est la valeur qui était présente avant la saisie de l'utilisateur.
				Exemple de fonction :
				<div class="highlight-php5">
						<pre>
	function cryptPassword($newValue, $previousValue){
		if($newValue != $previousValue)
			 return crypt($newValue, $config['salt']);
		else return $newValue;
	}
	</pre>
				</div>
				</p>

				</td>
			</tr>
			<tr class="row-even">
				<td><strong>preDisplay</strong></td>
				<td>
				<p>Traite la valeur avant son affichage dans le formulaire. Ceci n'altère pas la valeur contenue dans le champ, uniquement la valeur affichée.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'preDisplay' => 'myClass::formFormatDate',</pre>
				</div>
				</p>
				<p>
				Exemple de fonction :
				<div class="highlight-php5">
						<pre>
	static function formatDate($key,$value,$locale,$additionalValue=''){
		$dateFormated=parent::formatDate($key, $value, $locale);
		if($dateFormated=='30/11/-0001')return '00/00/0000';
		else return $dateFormated;
	}
	</pre>
				</div>
				</p>

				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>noClearFix</strong></td>
				<td>
				<p>Quand on indique la valeur '1', prévient l'affichage du code &lt;div class="clearfix"&gt;&lt;/div&gt; après le champ. La classe 'clearfix' à pour effet d'annuler les float avec l'instruction CSS 'clear:both;'. En effet, vous voudrez parfois désactivez cette instruction pour un meilleur contrôle sur l'affichage du formulaire, quand par exemple vous voudrez afficher plusieurs champs en inline.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'noClearfix' => '1'</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>preLabelHTML</strong></td>
				<td>
				<p>Spécifie du code HTML à placer immédiatement avant le tag &lt;label&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'preLabelHTML' => '&lt;span class="blue"&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>postLabelHTML</strong></td>
				<td>
				<p>Spécifie du code HTML à placer immédiatement après le tag &lt;label&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'postLabelHTML' => '&lt;/span&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>innerPreLabelHTML</strong></td>
				<td>
				<p>Spécifie du code HTML à placer avant le texte, à l'intérieur du tag &lt;label&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;label&gt;.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'innerPreLabelHTML' => '&lt;span class="blue"&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>innerPostLabelHTML</strong></td>
				<td>
				<p>Spécifie du code HTML à placer immédiatement après le texte, à l'intérieur du tag &lt;label&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;label&gt;.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'innerPostLabelHTML' => '&lt;/span&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>preFieldHTML</strong></td>
				<td>
				<p>Spécifie du code HTML à placer immédiatement avant le tag &lt;input&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'preFieldHTML' => '<div style="display:inline;">',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>postFieldHTML</strong></td>
				<td>
				<p>Spécifie du code HTML à placer immédiatement après le tag &lt;input&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'postFieldHTML' => '&lt;/div&gt;&lt;button&gt;Valider&lt;/button&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>preDivHTML</strong></td>
				<td>
				<p>Spécifie du code HTML à placer immédiatement avant le tag &lt;div&gt; du champ. C'est utile pour ajouter du code personnalisé à l'extérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'preDivHTML' => '&lt;div style="display:inline;"&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>postDivHTML</strong></td>
				<td>
				<p>Spécifie du code HTML a placer immédiatement après le tag &lt;div&gt; du champ. C'est utile pour ajouter du code personnalisé à l'extérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'postDivHTML' => '&lt;/div&gt;&lt;button&gt;Valider&lt;/button&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>showLabel</strong></td>
				<td>
				<p>Permet de désactiver l'ajout du tag &lt;label&gt; devant le champ si spécifié à false. La valeur par défaut est fixée à true.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'showLabel' => false,</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>showFieldDiv</strong></td>
				<td>
				<p>Permet de désactiver l'ajout du tag &lt;div&gt; autour du champ si spécifié à false. La valeur par défaut est fixée à true.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'showFieldDiv' => false,</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-even">
				<td><strong>showWrapDiv</strong></td>
				<td>
				<p>Permet de désactiver l'ajout du tag &lt;div&gt; englobant le bloc label + champ si spécifié à false. La valeur par défaut est fixée à true.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'showWrapDiv' => false,</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>noEcho</strong></td>
				<td>
				<p>Permet de désactiver l'affichage du champ courant lorsqu'on fait un echo d'un objet dbRecord. C'est utile par exemple si le champ contient un gros tableau de données et qu'on publie l'objet dans le log, mais qu'on ne veut pas voir apparaître le contenu de ce champ qui polluerai le log</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'noEcho' => true,</pre>
				</div>
				</p>
				</td>
			</tr>
		</tbody>
	</table>

	
Clés spécifiques 
----------------

Champ "input"
^^^^^^^^^^^^^^^^^^^^^
Ajoute un champ input.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>dataType</strong></td>
				<td>Vous pouvez spécifier en second argument du champ dataType une variante du champ texte. Par défaut, le datatype 'input' équivaut à 'input text'. Mais vous pouvez aussi spécifier d'autres variantes prévues par la norme HTML pour l'attribut 'type' du tag 'input', par exemple :
				<ul>
					<li>password</li>
					<li>date</li>
					<li>button</li>
					<li>number</li>
					<li>color</li>
					<li>range</li>
					<li>month</li>
					<li>week</li>
					<li>time</li>
					<li>datetime</li>
					<li>datetime-local</li>
					<li>email</li>
					<li>search</li>
					<li>tel</li>
					<li>url</li>
				</ul>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>'dataType' => 'input password',</pre>
					</div>
					</p>
					<p>
					L'affichage de ces types de champs dépend du niveau de support du navigateur de l'utilisateur. En effet certains types de champs HTML5 ne sont pas correctement supportés, par exemple les types 'date' ou 'color' qui nous ont amené à mettre en place une implémentation spécifique à base de plugins pour Bootstrap.
					</p>

					<p>
					Pour les types 'checkbox' ou 'radio' également, voir notre implémentation spécifique car ceux-ci requièrent un traitement particulier de l'attribut checked. Note : le type 'radio' n'est disponible qu'à partir de MindFlow 1.6. On pourra en attendant contourner le problème par l'emploi d'un champ de type 'select' qui fournit la même fonctionnalité, avec néanmoins une ergonomie différente.
					</p>
					
				
			</td>
			</tr>
		</tbody>
	</table>

|
|
|

Champ "date"
^^^^^^^^^^^^^^^^^^^^^
Ajoute un champ date.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>Settings</strong></td>
				<td><p>Permet de customiser le champ Bootstrap datetimepicker selon les options dans la doc <a href="http://eonasdan.github.io/bootstrap-datetimepicker/" target="_blank">http://eonasdan.github.io/bootstrap-datetimepicker/</a></p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'settings'=> array(
		'minDate' => '"01/01/1900"',
		'showToday' => 'true',
		'defaultDate' => '"01/01/1950"',
	)
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
			<tr class="row-even">
				<td><strong>Settings</strong></td>
				<td><p>Permet d'ajouter des appels chainés à la commande de création du dateTimePicker.</p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'jQueryChainedCalls' => 'on("dp.change",function(e){console.log(datetimepickers["au"]);})'
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
Champ "datetime"
^^^^^^^^^^^^^^^^^^^^^^^^
Permet d'insérer un champ date et heure / minutes / secondes.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>Settings</strong></td>
				<td><p>Permet de customiser le champ Bootstrap datetimepicker selon les options dans la doc <a href="http://eonasdan.github.io/bootstrap-datetimepicker/" target="_blank">http://eonasdan.github.io/bootstrap-datetimepicker/</a></p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'settings'=> array(
		'useCurrent' => 'true',
        'sideBySide' => 'true',
        'showClose' => 'true',
	)
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
			<tr class="row-even">
				<td><strong>Settings</strong></td>
				<td><p>Permet d'ajouter des appels chainés à la commande de création du dateTimePicker.</p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'jQueryChainedCalls' => 'on("dp.change",function(e){console.log(datetimepickers["au"]);})'
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|


Champ "time"
^^^^^^^^^^^^^^^^^^^^^
Permet d'insérer un champ date et heure / minutes / secondes.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>Settings</strong></td>
				<td><p>Permet de customiser le champ Bootstrap datetimepicker selon les options dans la doc <a href="http://eonasdan.github.io/bootstrap-datetimepicker/" target="_blank">http://eonasdan.github.io/bootstrap-datetimepicker/</a></p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'settings'=> array(
		'minDate' => '"01/01/1900"',
		'showToday' => 'true',
		'defaultDate' => '"01/01/1950"',
	)
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
			<tr class="row-even">
				<td><strong>Settings</strong></td>
				<td><p>Permet d'ajouter des appels chainés à la commande de création du dateTimePicker.</p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'jQueryChainedCalls' => 'on("dp.change",function(e){console.log(datetimepickers["au"]);})'
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
	
Champ "color"
^^^^^^^^^^^^^^^^^^^^^
Permet d'insérer un champ de type color picker. On pourra personnaliser le champ Bootstrap colorpicker selon les options dans la doc http://www.eyecon.ro/bootstrap-colorpicker/

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>dataType</strong></td>
				<td><p>On doit ajouter en second argument le format de couleur parmi l'une des valeurs suivantes :<br />
				<strong>hex, rgb, rgba, hsl, hsla</strong></p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'dataType'=> 'color rgb'
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
Champ "hidden"
^^^^^^^^^^^^^^^^^^^^^^^
Permet d'insérer un champ invisible dans le formulaire.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td></td>
				<td>Aucun champ spécifique
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
Champ "urlinput"
^^^^^^^^^^^^^^^^^^^^^^^^^
Champ initialement prévu pour le Plugin pages uniquement. Permet de saisir un segment d'URL tout en affichant l'URL complète + le segment en dessous du champ.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td></td>
				<td>Aucun champ spécifique
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|


Champ "checkbox"
^^^^^^^^^^^^^^^^^^^^^^^^^
Affiche une case à cocher.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>value</strong></td>
				<td><p>1 pour cocher la case, sinon 0</p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'value'=> '1'
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
			<tr class="row-even">
				<td><strong>swap</strong></td>
				<td><p>Permet de permuter l'ordre du label et de la case à cocher.
	1 pour afficher le label après la case, sinon 0
	</p>
					<p>
					Exemple d'usage :
					<div class="highlight-php5">
						<pre>
	'swap'=> '1'
	</pre>
					</div>
					</p>					
				
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
Champ "radio"
^^^^^^^^^^^^^^^^^^^^^^^^^
Affiche un ou plusieurs boutons radio.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>possibleValues</strong></td>
				<td><p>tableau associatif 'clé' => Array qui définira les différents input radio à afficher</p>					
				
			</td>
			</tr>
			
		</tbody>
	</table>
	
	Paramètres autorisés dans le tableau associatif 'possiblevalues' du champ radio :
	
	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>editable</strong></td>
				<td>
				<p>Valeur par défaut à 1. Passer la valeur à 0 pour rendre le bouton radio non éditable dans le formulaire. Il est aussi possible de placer une valeur 'editable' dans la définition de chaque option de manière à en activer certaines et d'autres pas. Si le champ 'editable' est défini au niveau du champ ET au niveau des options, c'est la valeur au niveau du champ qui prend le pas sur tous les 'editable' définis en option. Aussi il est recommandé de définir 'editable' soit au niveau du champ, soit au niveau des options, mais pas les 2 simultanément.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'editable' => '0'</pre>
				</div>
				</p>

				</td>
			</tr>
			
			
			<tr class="row-even">
				<td><strong>preFieldHTML</strong></td>
				<td>
				<p>Spécifie du code HTML a placer immédiatement avant le tag &lt;input&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'preFieldHTML' => '<div style="display:inline;">',</pre>
				</div>
				</p>
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>postFieldHTML</strong></td>
				<td>
				<p>Spécifie du code HTML a placer immédiatement après le tag &lt;input&gt; du champ. C'est utile pour ajouter du code personnalisé à l'intérieur du conteneur &lt;div&gt; qui entoure le champ.</p>
				<p>Exemple d'usage :
				<div class="highlight-php5">
						<pre>'postFieldHTML' => '&lt;/div&gt;&lt;button&gt;Valider&lt;/button&gt;',</pre>
				</div>
				</p>
				</td>
			</tr>
			
		</tbody>
	</table>
	
Exemple de définition d'un champ radio :
	
.. code-block:: php

	'accueil' => array(
		'value' => '',
		'dataType' => 'radio',
		'valueType' => 'text',
		'possibleValues' => array(
			'TS' => array(
				'value' => 5,
				'field_attributes' => array(
					'class' => 'test',
					'onChange' => 'doSomething();',
				),
				'div_attributes' => array(
					'class' => 'col-lg-2'
				),
				//'preFieldHTML' => 'this is ',
				//'postFieldHTML' => ' a test',
				'editable'=> 0,
			),
			'S' => array(
				'value' => 4,
				'div_attributes' => array(
					'class' => 'col-lg-2'
				),
			),
			'MS' => array(
				'value' => 3,
				'div_attributes' => array(
					'class' => 'col-lg-2'
				),
			),
			'PS' => array(
				'value' => 2,
				'div_attributes' => array(
					'class' => 'col-lg-2'
				),
			),
			'PSD' => array(
				'value' => 1,
				'div_attributes' => array(
					'class' => 'col-lg-2'
				),
			),
			'SA' => array(
				'value' => 0,
				'div_attributes' => array(
					'class' => 'col-lg-2'
				),
			),
		),
		//pas de field_attributes !
		'div_attributes' => array(
			'class' => 'row'
		),
	),
	
Dans le fichier de localisation associé à ce formulaire, on aura les entrées suivantes:

.. code-block:: php

	'accueil' => 'Accueil général (téléphone et/ou mail)',
	'accueil_fail' => 'Ce champ n\'à pas été rempli',
	'accueil_TS' => 'Très satisfait',
	'accueil_S' => 'satisfait',
	'accueil_MS' => 'moyennement satisfait',
	'accueil_PS' => 'pas satisfait',
	'accueil_PSD' => 'pas satisfait du tout',
	'accueil_SA' => 'sans avis',

Cette définition, couplée avec les données du fichier de localisation, nous affiche :

.. image:: images/radio.png
    :alt: Input radio
	

|
|
|

	
Champ "select"
^^^^^^^^^^^^^^^^^^^^^^^^^
Affiche une liste déroulante.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>possibleValues</strong></td>
				<td><p>Liste des valeurs affichées dans la liste de sélection.
				
				On y défini un tableau associatif ' valeur' => 'texte' qui définira les champs <option> à afficher dans le input select.
				
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'possibleValues' => array(
		1 => 'Par téléphone et email',
		2 => 'Par téléphone',
		3 => 'Par email'
	),
	</pre>
				</div>
				</p>					
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>option_attributes</strong></td>
				<td><p>Permet de spécifier des attributs à ajouter aux options sous forme de tableau associatif</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'option_attributes' => array(
		'class' => 'myOption',
	),
	</pre>
				</div>				
				</td>
			</tr>
			
		</tbody>
	</table>
	
**Astuce :** pour créer un champ select a sélection multiple, spécifiez le tableau field_attributes comme suit :

.. code-block:: php

	'field_attributes' => array(
		'multiple' => 'multiple',
	),

	
|
|
|


Champ "recordSelect"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Affiche un champ de type select simple rempli avec une liste d'objets PHP dont l'un d'entre eux pourra être sélectionné, édité (via une fenêtre popup), supprimé. Il sera également possible de créer un enregistrement et de l'ajouter à la liste, de modifier l'ordre des enregistrements, et de copier / coller des entrées vers un autre champ affichant la même classe d'enregistrements.

.. image:: images/record-field.png
    :alt: Un champ recordSelect
	
.. warning:: 

	**Important :**  Le type de colonne pour ce champ en base de données recommandé est 'TEXT' (recommandé) ou l'une de ses variantes ('LONGTEXT', 'BLOB' etc.). 
	Le champ peut être défini comme INT uniquement dans le cas d'un champ de type simple, car le type INT ne permet pas de stocker plusieurs valeurs uid dans le cas d'un champ select multiple. 
	Attention dans ce cas si vous basculez le champ en multiple ultérieurement et que vous ne penser pas à changer la définition de la colonne SQL en TEXT, vous rencontrerez des dysfonctionnements.

.. warning:: 

	Pour des raisons techniques, les champs recordSelect ne sont pas compatibles avec les objets dbForm. Dans ce cas de figure, il faudra utiliser un champ select simple et le gérer vous même.
	
La liste des enregistrements pourra être établie au moyen d'une requête SQL spécifiée directement dans la configuration du champ selon 2 modes : ``addItemsSQL`` ou ``initItemsSQL``

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>value</strong></td>
				<td><p>Liste des uids sélectionnés dans la select box, séparés par des virgules
				</p>					
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>editType</strong></td>
				<td><p>Type de select box : 'single' ou 'multiple'</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'editType' => 'multiple'
	</pre>
				</div>				
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>editClass</strong></td>
				<td><p>Nom de la classe PHP a éditer. Celle-ci devra être hérité de dbRecord
				</p>	
				<div class="highlight-php5">
					<pre>
	'editClass' => 'myContact'
				</pre>
				</div>					
				</td>
			</tr>
			
			
			<tr class="row-even">
				<td><strong>addItemsSQL</strong></td>
				<td><p>On spécifiera soit addItemsSQL, soit initItemsSQL, mais pas les deux. En mode addItemsSQL, la liste est vide par défaut et on y ajoute des éléments au fur et à mesure. Les champs listés en tant que titre  de chaque option dans la select box sont ceux listés dans la clé SELECT. Le champ ORDER_BY défini par ailleurs l'ordre dans lequel ces éléments sont affichés.</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'addItemsSQL' => array(
		'SELECT' => 'date, i_prix',
		'FROM' => 'vz_departs',
		'WHERE' => '',
		'ORDER BY' => 'date',
		'ORDER DIRECTION' => 'ASC',
	),
	</pre>
				</div>				
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>addItemsSQL</strong></td>
				<td><p>On spécifiera soit initItemsSQL, soit initItemsSQL, mais pas les deux. En mode initItemsSQL, la liste est remplie par défaut avec les éléments spécifiés dans la requête. On y sélectionne les éléments que l'on souhaite retenir. Les champs listés en tant que titre  de chaque option dans la select box sont ceux listés dans la clé SELECT. Le champ ORDER_BY défini par ailleurs l'ordre dans lequel ces éléments sont affichés.
				</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'initItemsSQL' => array(
		'SELECT' => 'date, i_prix',
		'FROM' => 'vz_departs',
		'WHERE' => '',
		'ORDER BY' => 'date',
		'ORDER DIRECTION' => 'ASC',
	),
	</pre>
				</div>				
				</td>
			</tr>

    <tr class="row-even">
				<td><strong>prependEmptyEntry</strong></td>
				<td><p>Ajoute une entrée vide dans le select en tête des options. Utile pour forcer l'utilisateur a sélectionner une valeur sans présélectionner une valeur par défaut. S'utilise en conjonction avec initItemsSQL en affichant le recordSelect en mode Single
				</p>
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'prependEmptyEntry' => true,
	</pre>
				</div>
				</td>
			</tr>


    <tr class="row-odd">
				<td><strong>columnSeparator</strong></td>
				<td><p>Par défaut les valeurs des colonnes de votre SELECT sont présentées entre elles séparées par de simples espaces. Vous pouvez spécifier une chaine de caractère à insérer entre chaque valeur de colonne présentée dans l'option, faisant ainsi office de séparateur.
                Pour un tritement plus élaboré, par exemple si vous souhaitez entourer votre valeur de parenthèses, il vous faudra plutôt utiliser un keyProcessor.
				</p>
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'columnSeparator' => ' -> ',
	</pre>
				</div>
				</td>
			</tr>

			
			<tr class="row-even">
				<td><strong>selectActions</strong></td>
				<td><p>Tableau associatif définissant les icônes d'action qui seront affichées, ou pas. La valeur '1' active l'icône, la valeur '0' la désactive. Par défaut, toutes les icônes sont actives, sauf la recherche (search). Attention, quand le champ search est à 1, il est impératif de définir searchTitleFields, et au moins l'un des champs suivants : searchFulltextFields, searchVarcharFields, searchEqualFields.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'selectActions' => array(
		'create' => 1,
		'view' => 0,
		'edit' => 1,
		'delete' => 1,
		'sort' => 1,
		'clipboard' => 1,
		'search' => 0,
	),
	</pre>
	</div>	

	<p><strong>Il est également possible de remplacer la valeur 1 par un lien personnalisé. Dans ce cas, c'est ce lien qui sera appellé pour ouvrir la fenêtre popup.</strong></p>
	<p>Par exemple qui permet d'appeler un popup fait maison plutôt que le popup standard de MindFlow :</p>
	<div class="highlight-php5">
	<pre>
	'selectActions' => array(
		'create' => demandUtils::createDemand({record_uid}),
		'view' => 0,
		'edit' => 1,
		'delete' => 1,
		'sort' => 0,
	),
	</pre>
	</div>
				<p>Avec notre fonction qui crée le lien définie ainsi :</p>
	<div class="highlight-php5">
	<pre>
	static function createDemand($recordUid){
    
		//On crée les variables de sécurité pour prévenir les formulaires forgés

        $sec = getSec(array(
            'action' => 'createDemand',
			'record_class' => 'demand',
			'parent_uid' => $recordUid
        ));

		// on retourne le lien en y injectant les paramètres de sécurité
        // l'usage de $sec->parameters retourne les paramètres dans l'URL et évite de les saisir une seconde fois
        // avec la source d'erreur que cela représente
        // car si les paramètres dans l'URL et le Hash ne correspondent pas, la requête sera rejetée !
		return $config['website_pluginsdir'].'/plugin_demands/
		ajax-demand-json.php?'.$sec->parameters.'&sec='.$sec->hash.'&fields='.$sec->fields;
	}

	</pre>
	</div>	
		
	<p>Et dans notre fichier ajax-demand-json.php, on traitera la requête de la manière suivante :</p>
	<div class="highlight-php5">
	<pre>
	//vérification que les champs sécurisés n'ont pas été altérés
	if(checkSec()){

		if($_REQUEST['action'] == 'createDemand'){

			$myDemand = new demand();
			$myDemand->data['parent_uid']['value'] = intval($_REQUEST['parent_uid']);

			$form = $mf->formsManager;
			$htmlBody = '<div id="ajaxResult">'.$form->editRecord($myDemand, '', '', false, true,false,true,1).'</div>;
		}
	}


	</pre>
	</div>			
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>icon</strong></td>
				<td><p>Permet de personnaliser l'icône affichée en haut à gauche de la select box avec le code HTML souhaité.
				</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'icon' => '<b class="glyphicon glyphicon-list-alt"></b>',
	</pre>
				</div>				
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>debugSQL</strong></td>
				<td><p>Passer à true pour voir la requête SQL générée pour la sélection du contenu du champ et ainsi voir ce qui cloche en cas de problème.
				</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'debugSQL' => true,
	</pre>
				</div>				
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>keyProcessors</strong></td>
				<td><p>Permet de personnaliser l'affichage d'une valeur remontée de la base de données à la volée, au moyen d'une fonction. Par exemple si i_prix==1, afficher 'A'.
				</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'keyProcessors' => array(
		'date'=>'depart::formatDateDepart',
		'i_prix'=>'depart::formatIPrix'
	),
	</pre>
				</div>				
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>onRefresh</strong></td>
				<td><p>Spécifie une fonction retour (callback) Javascript qui sera notifiée des modifications apportées par l'utilisateur à la liste des options du recordSelect
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'onRefresh' => 'participantChange',
	</pre>
				</div>	
	<p>La fonction participantChange est définie comme suit :
	<div class="highlight-php5">
	<pre>
	function participantChange(optionIndex,optionClass,actionName,
	listOptionsFuncArguments){
		console.log("participantChange("+optionIndex+","+optionClass+",
		"+actionName+")");
		var jqxhr = $.post("/mf/core/ajax-core-json.php", 
		listOptionsFuncArguments)
			.success(function(jsonData) {
				if(jsonData.result=="success"){
				  console.log("listOptionsFuncArguments success");
				  for(optionItem in jsonData.optionsList){
					 first_name = jsonData.optionsList[optionItem]
					 ['first_name']['value'];
					 last_name = jsonData.optionsList[optionItem]
					 ['last_name']['value'];
					 console.log("user names="+first_name+" "
					 +last_name);
				  }
				}
				else{
					console.log("listOptionsFuncArguments error");
					alert(jsonData.message);
				}
			})
	}

	</pre>
				</p>
	<p><strong>Notez les arguments de rappel de notre fonction de callback / onRefresh :</strong></p>
	<p><strong>optionIndex :</strong> indice de l'option qui a été modifiée par l'utilisateur. Il s'agit également de l'uid de l'enregistrement affecté.</p>
	<p><strong>optionClass :</strong> classe de l'enregistrement modifié par l'utilisateur.</p>
	<p><strong>actionName :</strong> action réalisée par l'utilisateur. Peut être l'une des valeurs suivantes : view, create, edit, delete, sortUp, sortDown, paste</p>
	<p><strong>listOptionsFuncArguments :</strong> retourne une série d'arguments sécurisée (inaltérable) qui peut être utilisée pour obtenir les données détaillées des enregistrements contenus dans la liste des options. Pour les obtenir, il suffit d'appeler ajax-core-json.php en lui passant ces arguments, comme suit : <pre>var jqxhr = $.post("/mf/core/ajax-core-json.php", listOptionsFuncArguments);</pre></p>
	<p>Voyez l'exemple ci-dessus pour un usage plus détaillé des valeurs de retour de la fonction.</p>
	</p>
				
				</td>
			</tr>
			
			
			<tr class="row-odd">
				<td><strong>icons-wrap</strong></td>
				<td><p>Si à false, désactive le code <span class="input-group-addon"></span> qui génère un cadre gris autour des icônes d'action.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icons-wrap' => false,
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>icon-view</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'voir'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-view' => '&lt;b class="glyphicon glyphicon-eye-open"&gt;&lt;/b&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>icon-edit</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'éditer'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-edit' => '&lt;span class="btnMultiSelect"&gt;&lt;b class="glyphicon 
	glyphicon-pencil"&gt;&lt;/b&gt; Éditer la formation sélectionnée&lt;/span&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>icon-create</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'créer'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-create' => '&lt;span class="btnMultiSelect"&gt;&lt;b class="glyphicon 
	glyphicon-plus"&gt;&lt;/b&gt; Ajouter une formation&lt;/span&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>icon-delete</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'effacer'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-delete' => '&lt;span class="btnMultiSelect"&gt;&lt;b class="glyphicon 
	glyphicon-trash"&gt;&lt;/b&gt; Supprimer la formation sélectionnée&lt;/span&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>icon-add</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'ajouter'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-add' => '&lt;b class="glyphicon glyphicon-plus-sign"&gt;&lt;/b&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>icon-remove</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'ôter'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-remove' => '&lt;b class="glyphicon glyphicon-minus-sign"&gt;&lt;/b&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>icon-up</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'déplacer vers le haut'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-up' => '&lt;b class="glyphicon glyphicon-circle-arrow-up"&gt;&lt;/b&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>icon-down</strong></td>
				<td><p>Permet de personnaliser l'icône de l'action 'déplacer vers le bas'
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'icon-down' => '&lt;b class="glyphicon glyphicon-circle-arrow-down"&gt;&lt;/b&gt;',
	</pre></div>
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>allow_cross_editing</strong></td>
				<td><p>Par défaut ce champ est fixé à true. Si spécifié à false, ce champ permet de désactiver l'édition des entrées du recordSelect par des utilisateurs autres que le créateur de l'enregistrement. Les autres utilisateurs peuvent encore visualiser ou copier/coller l'enregistrement, mais ne peuvent pas l'éditer ou le supprimer.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'allow_cross_editing' => false,
	</pre></div>
				</td>
			</tr>
			
			
			
		</tbody>
	</table>
	
	<p><strong>Options associées à la recherche : </strong></p>
	
	<p>Ces options ne sont effectives que lorsque le champ selectActions['search'] est fixé à 1 pour que la recherche soit activée.</p>
	<p>Vous devez impérativement spécifier le champ 'searchTitleFields' ainsi que l'un des 3 champs suivants pour que la recherche fonctionne : 'searchFulltextFields', 'searchVarcharFields' ou 'searchEqualFields'</p>

	
	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
	<tr class="row-odd">
				<td><strong>searchTitleFields</strong></td>
				<td><p>Liste séparée par des virgules de nom de champs / colonnes qui seront affichés en tant que résultat de recherche. Ceci influence le segment SELECT de la  requête de recherche :
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
				<pre>'searchTitleFields' => 'ref,titre',</pre></div>
				Produit la requête :
				<div class="highlight-php5">
						<pre>
	SELECT uid,ref,titre FROM vz_circuits WHERE MATCH(ref,titre) 
	AGAINST('Timbr*' IN BOOLEAN MODE) AND deleted=0
	</pre></div>
	
								
				</td>
			</tr>
			
			
			<tr class="row-even">
				<td><strong>searchFulltextFields</strong></td>
				<td><p>Liste séparée par des virgules de nom de champs / colonnes présentes dans l'index fulltext de la table et pouvant être recherchées.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'searchFulltextFields' => '`ref`,`titre`',
	</pre>
	</div>
	<p>Ceci produit une recherche du type :</p>
	<div class="highlight-php5">
	<pre>
	SELECT uid,ref,titre FROM vz_circuits WHERE MATCH(`ref`,`titre`) 
	AGAINST('Timbr*' IN BOOLEAN MODE) AND deleted=0
	</pre>
	</div>
	<p>Note : le champ selectActions['search'] doit être fixé à 1 pour que la recherche soit activée.</p>	
				</td>
			</tr>
			
			
			<tr class="row-odd">
				<td><strong>searchVarcharFields</strong></td>
				<td><p>Liste séparée par des virgules de nom de champs / colonnes de type varchar et pouvant être recherchés.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'searchVarcharFields' => 'ref,titre',
	</pre></div>
	<p>Ceci produit une recherche du type :</p>
	<div class="highlight-php5">
	<pre>
	SELECT uid,ref,titre FROM vz_circuits 
	WHERE ( ref LIKE '%Timbr%' OR titre LIKE '%Timbr%') 
	AND deleted=0
	</pre></div>
	<p>Note : le champ selectActions['search'] doit être fixé à 1 pour que la recherche soit activée.</p>	
				</td>
			</tr>
			
			
			<tr class="row-even">
				<td><strong>searchEqualFields</strong></td>
				<td><p>Liste séparée par des virgules de nom de champs / colonnes de seront recherchés avec un critère d’égalité stricte.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'searchEqualFields' => 'ref,titre',
	</pre></div>
	<p>Ceci produit une recherche du type :</p>
	<div class="highlight-php5">
	<pre>
	SELECT uid,ref,titre FROM vz_circuits 
	WHERE (ref = 'Timbre' OR titre = 'Timbre') AND deleted=0
	</pre></div>
	<p>Note : le champ selectActions['search'] doit être fixé à 1 pour que la recherche soit activée.</p>	
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>searchOrderByFields</strong></td>
				<td><p>Rajoute une clause ORDER BY à la requête de recherche.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'searchOrderByFields' => 'titre',
	</pre></div>
	<p>Ceci produit une recherche du type :</p>
	<div class="highlight-php5">
	<pre>
	SELECT uid,ref,titre FROM vz_circuits WHERE (ref = 'Timbre') 
	AND deleted=0 ORDER BY titre
	</pre></div>
	<p>Note : le champ selectActions['search'] doit être fixé à 1 pour que la recherche soit activée.</p>	
				</td>
	</tr>
	
	
	<tr class="row-odd">
		<td>searchDebug</td>
		<td>Permet de visualiser la requête SQL générée pour la recherche dans la réponse AJAX, et d'analyser ainsi d'éventuels problèmes.
		Exemple d'usage:
		<div class="highlight-php5">
		<pre>
		'searchDebug' => true,
		</pre></div>
		</td>
	</tr>
	</tbody>
	</table>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
|
|
|


Champ "recordEditTable"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Affiche un recordEditTable au sein d'un enregistrement dbRecord. L'objet est simplement de permettre d'offrir une "vue" vers d'autres enregistrements : le contenu du recordEditTable n'est pas sauvé dans l'enregistrement courant car il est est juste défini par une requête SQL vers d'autres tables.
Les éléments affichés sont ceux qui satisfont les critères de cette requête. Pour l'essentiel, le champ recordEditTable reprend les arguments de la fonction showRecordEditTable de la classe dbRecord

.. image:: images/recordEditTableField.jpg
    :alt: Un champ recordEditTable
	
.. code-block:: php

	//Voici un exemple de définition de champ recordEditTable
	'options'   =>  array(
		'dataType' => 'recordEditTable',
		'editClass' => 'vzDossier',
		'selectItemsSQL' => array(
			'SELECT' => 'vzUser.first_name,vzUser.last_name,statut_vente,interet,titre',
			'FROM' => '',
			'JOIN' => 'JOIN vz_users vzUser on vzUser.uid=client',
			'WHERE' => '0', // la condition est ajustée dans les fonctions postCreate() et postLoad()
			'ORDER BY' => 'titre',
			'ORDER DIRECTION' => 'ASC',
		),
		'moduleName' => 'gestionDeparts',
		'subModuleName' => '',
		'mainKey' => 'titre',
		'keyProcessors' => array(),
		'page' => null,
		'selectActions' => array(
			'view' => 1,
			'edit' => 1,
			'delete' => 1,
		),
		'advancedOptions'=>  array(
			'ajaxActions' => true,
			'buttons' => $buttonCreateDossier,
			'debugSQL' => 1,
			'editRecordButtons' => array(
				'showSaveButton' => true,
				'showSaveCloseButton' => true,
				'showPreviewButton' => false,
				'showCloseButton' => true
			),
			'showPrint' => false,
			'actionOnTitleClick' => 'edit',
			'forceNumRowsPerPage' => '6',
		),
		'recordEditTableID' => 'depart_options',
		'innerPreLabelHTML' => '<b class="glyphicon glyphicon-folder-open"></b>',
		'div_attributes' => array('class' => 'col-lg-12','style' => 'padding-right:20px;'),
		'label_attributes' => array('class' => 'col-lg-12'),
		'fullWidth' => true,
	),

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			
			
			<tr class="row-even">
				<td><strong>dataTypeType</strong></td>
				<td>recordEditTable			
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>editClass</strong></td>
				<td><p>Nom de la classe PHP a éditer. Celle-ci devra être hérité de dbRecord
				</p>	
				<div class="highlight-php5">
					<pre>
	'editClass' => 'myContact'
				</pre>
				</div>					
				</td>
			</tr>
			
			
			<tr class="row-even">
				<td><strong>selectItemsSQL</strong></td>
				<td><p>On spécifiera ici la requête spécifiant les éléments à afficher</p>
				
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'selectItemsSQL' => array(
		'SELECT' => 'first_name,last_name,statut_vente,interet',
		'FROM' => '',
		'WHERE' => "statut_vente='option'", 
		'ORDER BY' => 'last_name',
		'ORDER DIRECTION' => 'ASC',
	),
	</pre>
	<br/>
	<p>Notez que si vous devez faire référence à l'uid de l'enregistrement courant dans votre clause WHERE, il vous faudra ajuster le WHERE dans les fonctions postCreate() et postLoad(). En effet, au moment de l'exécution de la fonction init(), il n'y a qu'une simple déclaration de la structure du tableau data et tous les champs value() sont vides : vous ne pouvez donc pas vous y référer à ce stade pour récupérer l'uid de l'enregistrement courant. En revanche, une fois l'enregistrement créé ou chargé depuis la base de données, son uid devient disponible.</p>				
	<pre>
	function postCreate(){
		$this->data['options']['selectItemsSQL']['WHERE'] = 
		"depart=".$this->data['uid']['value']." AND statut_vente='option'";
	}
	</pre>
				</div>				
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>moduleName</strong></td>
				<td><p>Clé du module courant. Peut être laissée vide.</p>		
				
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>subModuleName</strong></td>
				<td><p>Clé du sous-module courant. Peut être laissée vide.</p>	
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>mainKey</strong></td>
				<td><p>Champ/colonne du tableau dont le titre sera clicable</p>	
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'mainKey' => 'last_name',
	</pre>				
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>keyProcessors</strong></td>
				<td><p>Permet de personnaliser l'affichage d'une valeur remontée de la base de données à la volée, au moyen d'une fonction. Par exemple si i_prix==1, afficher 'A'.
				</p>		
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'keyProcessors' => array(
		'date'=>'depart::formatDateDepart',
		'i_prix'=>'depart::formatIPrix'
	),
	</pre>
				</div>				
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>page</strong></td>
				<td><p>Permet de forcer un numéro de pagination pour l'affichage des résultats. On laissera généralement la valeur à null.
				</p>		
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'page' => '2',
	</pre>	
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>selectActions</strong></td>
				<td><p>Liste des actions disponibles dans la colonne "actions" sur la droite du tableau. Référez-vous à la documentation phpDoc de la fonction showRecordEditTable() pour plus de détails.
				</p>
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'mainKey' => array(
                    'create' => 1,
                    'view' => 1,
                    'edit' => 1,
                    'delete' => 1,
                    'copyPaste'=> 1,
                ),
	</pre>					
				</td>
			</tr>
			
			
			<tr class="row-even">
				<td><strong>advancedOptions</strong></td>
				<td><p>Liste des options avancées en appel de showRecordEditTable(). Référez-vous à la documentation de la fonction showRecordEditTable() pour plus de détails.
				</p>
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'advancedOptions'=>  array(
		'ajaxActions' => true,
		'buttons' => null, //assigné dans postCreate + postLoad
		'columnClasses' => array(
			'creation_date' => 'hidden-xs',
			'email' => 'hidden-xs hidden-sm',
			'deleted' => 'hidden-xs hidden-sm',
		),
		'debugSQL' => 0,
		'editRecordButtons' => array(
			'showSaveButton' => true,
			'showSaveCloseButton' => true,
			'showPreviewButton' => false,
			'showCloseButton' => true
		),
		'showPrint' => false,
		'actionOnTitleClick' => 'edit',
		'forceNumRowsPerPage' => '6',
        'copyPasteToParentUidColumn' => 'myCustomColumnName',
	    'copyPasteToParentClassColumn' => 'myCustomClassName',
	),
	</pre>	
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>recordEditTableID</strong></td>
				<td><p>Spécifie un string qui servira d'ID unique pour ce recordEditTable. Si aucun recordEditTableID n'est fourni, une valeur numérique aléatoire sera générée. Si vous avez besoin d'accéder au recordEditTable via javascript, vous aurez besoin d'un ID prédictible et vous voudrez donc spécifier ici une valeur personnalisée.
				</p>
	Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'recordEditTableID' => 'depart_options',
	</pre>					
				</td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>fullWidth</strong></td>
				<td><p>Permet d'étendre le recordEditTable à toute la largeur du formulaire, si positionné à true.
				</p>		
	Exemple d'usage:
	<div class="highlight-php5">
	<pre>
	'fullWidth' => 'true',
	</pre></div>
				</td>
			</tr>
			
			
			
			
		</tbody>
	</table>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
|
|
|

	
Champ "rich_text"
^^^^^^^^^^^^^^^^^^^^^^^^^
Affiche un éditeur de texte enrichi basé sur Ckeditor.

.. image:: images/ckeditor.png
    :alt: Le dossier mf_websites/

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td></td>
				<td>Aucun champ spécifique
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
Champ "textarea"
^^^^^^^^^^^^^^^^^^^^^^^^^
Affiche un champ textarea.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>dataType</strong></td>
				<td><p>On spécifiera en second argument le nombre de colonnes et en troisième argument le nombre de lignes.</p>
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'dataType' => 'textarea 50 8'
	</pre>
				</div>
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
Champ "files"
^^^^^^^^^^^^^^^^^^^^^^^^^
Affiche un champ d'upload de fichiers. Ces fichiers seront placés dans le sous-dossier 'uploads/' du dossier défini pour le site courant dans mf_websites/

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>AllowedExtensions</strong></td>
				<td><p>Liste séparée par des virgules des extensions de fichiers autorisées en upload. Si la clé n'est pas définie, la valeur par défaut suivante s'applique : gif, jpg, jpeg, png, xls, xlsx, doc, docx, ppt, pptx, pdf, odb, odc, odf, odg, odp, ods, odt, oxt
				<br /><br />
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'allowedExtensions' => 'jpg,jpeg,gif,png',
	</pre>
				</div>
				</p>					
				</td>
			</tr>
			
			<tr class="row-even">
				<td><strong>jsCallbackFunc</strong></td>
				<td><p>Spécifier une fonction javascript qui sera appelée une fois l'upload terminé
				<div class="highlight-php5"></p>
						<pre>
	'jsCallbackFunc' => 'statAdhesion'
	</pre></div>
	<p>La fonction sera alors définie comme suit dans votre code :</p>
	<div class="highlight-php5">
						<pre>
	&lt;script&gt;
	function statAdhesion(){
		alert('statAdhesion appellée !');
	}
	&lt;/script&gt;

	</pre>
				</div>				
				</td>
			</tr>
			<tr class="row-odd">
				<td><strong>maxItems</strong></td>
				<td><p>Indique le nombre maximum de fichiers autorisés en upload dans ce champ.
				<br /><br />
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'maxItems' => 6,
	</pre>
				</div>
				</p>					
				</td>
			</tr>
			
		</tbody>
	</table>

.. important::

    L'attribut 'value' du champ files est sauvegardée par MindFlow sous la forme d'une donnée structurée au format suivant :

    .. code-block:: php

        array (
          0 =>
          array (
            'timestamp' => 1452700155,
            'filename' => 'velo-birmanie-2_1452700155.jpg',
            'filepath' => 'circuit/circuit_726/',
            'width' => 1920,
            'height' => 1080,
          ),
          1 =>
          array (
            'timestamp' => 1452700185,
            'filename' => 'velo-birmanie-4_1452700185.jpg',
            'filepath' => 'circuit/circuit_726/',
            'width' => 1920,
            'height' => 1080,
          ),
        )

    L'ordre des entrées dans le tableau reflète le classement des fichiers, initialement selon leur ordre d'upload, ou selon le tri manuel effectué en drag & drop dans le backend.

    * ``timestamp`` : moment ou le fichier a été uploadé, au format timestamp unix
    * ``filename`` : nom du fichier sur le disque
    * ``filepath`` : chemin relatif par rapport au dossier d'upload défini dans la variable $config['uploads_directory']. Pour retrouver le chemin relatif à la racine de l'hébergement, concaténez la valeur avec ``$mf->getUploadsDir().DIRECTORY_SEPARATOR``. Pour retrouver le chemin absolu sur le disque, préfixez avec la constante ``DOC_ROOT``.
    * ``width``, ``height`` : Ces champs mettent en cache après l'upload, pour les fichiers images uniquement, la largeur et la hauteur, de manière à ne pas avoir a les extraire de l'image à chaque accès et affichage de celle-ci, ce qui serait préjudiciable au niveau des performances.

    D'autres meta-informations sont susceptibles d'être ajoutées dans le futur, par exemple un attribut 'alt' serait bienvenu. Vous devriez également être en mesure d'ajouter vos meta-informations personnalisées au tableau, mais cet aspect n'a pas encore été testé et validé.

    Pour afficher une image, vous pouvez procéder comme suit :

    .. code-block:: php

        $photo = $record->data['mon_champ_files']['value'];

        $imageSrc = $mf->getUploadsDir().$photo['filepath'].$photo['filename'];

        $html = '<img src="'.$imageSrc.'" width="'.$photo['width'].'" height="'.$photo['height'].'" />';



.. note::

    Le champ files fonctionne en conjonction avec la fonction ``mfResizeImage()`` du fichier **utils.php** qui optimise la génération de vignettes en les gardant en cache sur le système de fichiers du serveur, pour éviter d’avoir à les ré-génerer à chaque consultation, ce qui soulage le CPU du serveur. La fonction préserve aussi le ratio et la qualité de l’image lors du re-dimensionnement, en se basant notamment sur les meta-données width et height enregistrées avec l’image dans le tableau serialisé.

    Voici un exemple d’usage, qui extrait une série d’images d’une fiche circuit et les travaille en plusieurs résolution avant leur affichage. Comme vous pourrez le constater, avec ce système, les images sont facilement et rapidement exploitables :


    .. code-block:: php

        $imagesFiche = $circuit->data['imgs']['value']; // le tableau est déjà déserialisé automatiquement
                                                    // quand l’enregistrement est chargé via sa fonction load()

        if(is_array($imagesFiche) and sizeof($imagesFiche) > 0){

            foreach($imagesFiche as $bigImage){

                $smallImage = mfResizeImage($bigImage, 302, 200, false); //on spécifie les nouvelles largeur / hauteur pour le redimensionnement
                $mediumImage = mfResizeImage($bigImage, 592, 592); //les images redimensionnées sont écrites dans le même dossier que $bigImage ['filepath']
                                                                   // et spécifiées dans l’array() retourné par la fonction mfResizeImage()

                if(isset($smallImage['filepath'])){
                    $smallImageSrc = $mf->getUploadsDir().$smallImage['filepath'].$smallImage['resizedFilename'] ;
                }
                if(isset($mediumImage['filepath'])){
                    $mediumImageSrc = $mf->getUploadsDir().$mediumImage['filepath'].$mediumImage['resizedFilename'] ;
                }
                if(isset($bigImage['filepath'])){
                    $bigImageSrc = $mf->getUploadsDir().$bigImage['filepath'].$bigImage['filename'] ;
                }

                //ici on peut exploiter facilement les metadonnées
                $carousel .= "<a class='CarImgLink' style=’margin: 30px;width:".$smallImage['width']."px; height: ".$smallImage['height']."px; background-repeat:no-repeat; background-image: url('$smallImageSrc')’ href=’".$bigImageSrc."’ data-size=’".$bigImage['width']."x".$bigImage['height']."’ data-med=’".$mediumImageSrc."’ data-med-size=’".$mediumImage['width']."x".$mediumImage['height']."’ ><img src=’".$smallImageSrc."’  alt=’’ /></a>";
            }
        }

|
|
|

	
Champ "fieldset"
^^^^^^^^^^^^^^^^^^^^^^^^^
Ajoute un cadre fieldset autour de certains champs du formulaire, qui démarre à  l'emplacement ou est inséré le champ et qui se termine après le champ spécifié dans la clé 'closeAfterKey'

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>closeAfterKey</strong></td>
				<td><p>Spécifier la clé champ après lequel fermer ce fieldset</p>
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'closeAfterKey' => 'email',
	</pre>
				</div>
			</td>
			</tr>
		</tbody>
	</table>
	
	

|
|
|

	
Champ "html"
^^^^^^^^^^^^^^^^^^^^^^^^^
Ajoute un code HTML personnalisé dans le formulaire à l'emplacement ou est inséré le champ.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>value</strong></td>
				<td><p>Le code HTML qui doit être inséré</p>
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'value' => '&lt;hr /&gt;',
	</pre>
				</div>
			</td>
			</tr>
		</tbody>
	</table>
	
	


|
|
|

	
Champ "template_name"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ce champ est toujours associé à un champ template_data.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>value</strong></td>
				<td><p>Indique la clé du template qui défini la structure de données stockée dans le champ template_data généralement associé à ce champ. Cette clé correspond à une entrée existante dans le tableau $mf->templates.</p>
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'value' => '404-page',
	</pre>
				</div>
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|

	
Champ "template_data"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ce champ est toujours associé à un champ template_name.

Champ permettant de stocker une structure de données de type template. Il n'est pas destiné à être affiché en tant que champ de formulaire. 

Un objet template est une structure de données de spécification similaire à ce qu'on peut trouver pour le tableau 'data'. Néanmoins celle-ci est sérialisée, puis stockée en base de données dans un seul champ de type longblob, au lieu d'avoir une colonne dans la table par clé/champ. Ceci permet d'avoir une structure de données souple, modifiable dynamiquement et enregistrable en base de données sans avoir à modifier la structure de la table MySQL. 

La structure template_data est utilisée par la classe 'page' du Plugin 'pages' et par la classe 'mfUserGroup' du plugin 'mf_users' si vous voulez voir des exemples d'utilisation. 

Dans la majorité des cas, vous n'en aurez pas besoin, sauf si vous voulez générer des formulaires dynamiques dont vous ne pouvez prévoir la structure à l'avance.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td></td>
				<td>Aucun champ spécifique
			</td>
			</tr>
		</tbody>
	</table>
	

|
|
|


Champ "microtemplate"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Permet d'insérer des microtemplates HTML dans la page.

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>AllowedMicrotemplateTypes</strong></td>
				<td><p>Liste de clés de microtemplates séparées par des virgules pour indiquer quels types de microtemplates sont autorisés en insertion de ce champ. <strong>Un microtemplate ou micro-gabarit est une portion de code HTML, qui n'inclus pas une page entière, mais seulement une fraction de cette page,</strong> par exemple le microtemplate permettant l'affichage d'une actualité, que l'on pourra répéter et afficher autant de fois qu'il y aura d'actualités à montrer au sein d'une même page.</p>
				Exemple d'usage:
				<div class="highlight-php5">
						<pre>
	'allowedMicrotemplateTypes' => 'infoBlock, purchaseBlock',
	</pre>
				</div>
			</td>
			</tr>
		</tbody>
	</table>
	
|
|
|
|
	
Types SQL selon les types de champs
-----------------------------------

Vous créez donc un champ dans un enregistrement de base de données. Mais quel type SQL lui donner ? 

Alors que vous êtes libre d'adopter le type qui vous convient, voici des types éprouvés par l'expérience qui fonctionneront dans la plupart des cas :

.. raw:: html

	<table border="1" class="docutils">
		<colgroup>
			<col width="14%">
			<col width="86%">
		</colgroup>
		<tbody valign="top">
			<tr class="row-odd">
				<td><strong>input</strong></td>
				<td><p>varchar(255)</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>date</strong></td>
				<td><p>DATE</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>datetime</strong></td>
				<td><p>DATETIME</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>time</strong></td>
				<td><p>TIME</p></td>
			</tr>
			
			<tr class="row-odd">
				<td><strong>color</strong></td>
				<td><p>VARCHAR(10)</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>hidden</strong></td>
				<td><p>Dépend de la nature de la valeur portée par le champ. varchar(255) devrait convenir dans la plupart des cas.</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>urlinput</strong></td>
				<td><p>varchar(255)</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>checkbox</strong></td>
				<td><p>TINYINT(1) NOT NULL DEFAULT '0'</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>select</strong></td>
				<td><p>varchar(255) ou ENUM('valeur1','valeur2',etc..) si on veut contraindre les valeurs dès le niveau SQL</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>record</strong></td>
				<td><p>varchar(255) ou TEXT si le nombre d'enregistrement susceptibles d'être ajoutés est important</p></td>
			</tr>
            <tr class="row-odd">
				<td><strong>recordEditTable</strong></td>
				<td><p>Les champs recordEditTable fournissent une vue sur une autre table en fonction des critères indiqués dans la requête SQL associée. A ce titre, il n'y a aucune donnée stockée dans la table de l'objet local. Il ne faut donc pas définir de colonne associée à un champ recordEditTable</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>rich_text</strong></td>
				<td><p>TEXT</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>textarea</strong></td>
				<td><p>TEXT</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>files</strong></td>
				<td><p>BLOB</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>template_name</strong></td>
				<td><p>varchar(255)</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>microtemplate</strong></td>
				<td><p>Sans objet : un microtemplate est un sous ensemble d'un champ template. C'est le champ template qui est stocké en base de données.</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>template_data</strong></td>
				<td><p>LONGBLOB</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>fieldset</strong></td>
				<td><p>Sans objet (non stocké en BDD)</p></td>
			</tr>
			<tr class="row-odd">
				<td><strong>html</strong></td>
				<td><p>Sans objet (non stocké en BDD)</p></td>
			</tr>
		</tbody>
	</table>

	
	
