<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';

class welcome implements module
{
    private $moduleType = 'backend';

    function prepareData(){}

    function render(&$mainTemplate){

        global $mf,$l10n;

        $pagesBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/welcome/ressources/templates/welcome.html');
        $pagesBody = str_replace("{welcome_title}",$l10n->getLabel('welcome','welcome_title'),$pagesBody);
        $pagesBody = str_replace("{welcome_intro}",$l10n->getLabel('welcome','welcome_intro'),$pagesBody);

        $this->userGroup = $mf->currentUser->getUserGroup();
        //echo 'right='.$this->userGroup->getUserRight('welcome','welcomeStartPage');

        if( $this->userGroup && $this->userGroup->getUserRight('fiches','welcomeStartPage')!='' ){
            $startLink = $this->userGroup->getUserRight('welcome','welcomeStartPage');
        }
        else $startLink = SUB_DIR."/mf/index.php?module=pages";

        $startBtn = '<p><a href="'.$startLink.'" class="btn btn-primary btn-lg">'.$l10n->getLabel('welcome','welcome_start_btn').' &raquo;</a></p>';

        $pagesBody = str_replace("{welcome_start_btn}",$startBtn,$pagesBody);

        $mainTemplate = str_replace("{current_module}",$pagesBody,$mainTemplate);

    }

    function getType(){
        return $this->moduleType;
    }



}
