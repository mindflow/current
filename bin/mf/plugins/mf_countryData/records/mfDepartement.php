<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

class mfDepartement extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_departement';

    static $departements;

    static $createTableSQL= "
        `departement_code` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
        `departement_nom` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
        `departement_nom_uppercase` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
        `departement_slug` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
        `departement_nom_soundex` varchar(20) DEFAULT NULL,
        `departement_id_insee` int(11) NOT NULL,

    ";

    static $createTableKeysSQL="
       KEY `departement_slug` (`departement_slug`),
       KEY `departement_code` (`departement_code`),
       KEY `departement_nom_soundex` (`departement_nom_soundex`)
    ";

    function init(){

        parent::init();

        $this->data = array_merge($this->data, array(

            'departement_code'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'departement_nom'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'departement_nom_uppercase'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'departement_slug'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'departement_nom_soundex'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'departement_id_insee'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),


        ));




        $this->showInEditForm = array(
            'tab_main'=> array('departement_code','departement_nom','departement_nom_uppercase','departement_slug','departement_nom_soundex','departement_id_insee',)
        );



    }

    static $enableImportInitializationData = 1;

    static function importInitializationData(&$html=array()){

        dbRecord::importSQLDump(dirname(__FILE__).'/mf_departements.sql',1,$html);

    }


    static function listDepartments(){
        if(!is_array(self::$departements)){
            //optimisation, on n'effectue la requête SQL que si elle n'a jamais été faite.
            global $pdo;

            $sql = "SELECT departement_code, departement_nom FROM ".self::$tableName." WHERE 1 ORDER BY departement_code ASC";
            $stmt = $pdo->prepare($sql);

            try{
                $stmt->execute(array());
            }
            catch(PDOException $e){
                echo $e->getMessage();
                return 0;
            }

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            self::$departements = array(0 => '');

            foreach($rows as $row){
                self::$departements[$row['departement_code']] = $row['departement_nom'];
            }
            return self::$departements;
        }
        else return self::$departements;
    }



}


