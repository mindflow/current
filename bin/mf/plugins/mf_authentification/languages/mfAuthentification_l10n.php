<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'en' => array(
        'invalid_identification' => 'Error : Your username or password is invalid',
        'log_invalid_identification' => 'Error : invalid identification from user {username}',
        'username' => 'Username: ',
        'password' => 'Password: ',
        'your_username' => 'Your username',
        'your_password' => 'Your password',
        'please_identify' => 'Please identify yourself:',
        'connect' => 'Connect',
        'user_email' => 'Please type your email address:',
        'your_email' => '',
        'receive_email' => 'Reset my password',
        'void_email' => 'You have typed a void email',
        'email_sent' => 'Your new password has been sent. Check your email',
        'email_notfound' => 'Sorry this email address can not be found. Please check your input.',
        'back_to_login' => 'Back to login screen',
        'forgotten_password' => 'Forgotten password ?',
        'password_recovery' => 'Password recovery'
    ),

    'fr' => array(
        'invalid_identification' => 'Erreur  : Votre identifiant ou mot de passe est invalide',
        'log_invalid_identification' => 'Erreur : identification invalide de l\'utilisateur {username}',
        'username' => 'Identifiant : ',
        'password' => 'Mot de passe : ',
        'your_username' => 'Votre identifiant',
        'your_password' => 'Votre mot de passe',
        'please_identify' => 'Identifiez-vous SVP :',
        'connect' => 'Connexion',
        'user_email' => 'Veuillez saisir votre adresse email :',
        'your_email' => '',
        'receive_email' => 'Réinitialiser mon mot de passe',
        'void_email' => 'L\'email que vous avez saisi est vide',
        'email_sent' => 'Votre nouveau mot de passe vient de vous être envoyé. Relevez votre email.',
        'email_notfound' => 'Désolé cette adresse email n\'a pas été trouvée. Vérifiez l\'adresse donnée.',
        'back_to_login' => 'Retour à l\'écran de connexion',
        'forgotten_password' => 'Mot de passe oublié ?',
        'password_recovery' => 'Récupération du mot de passe'
    ),
);