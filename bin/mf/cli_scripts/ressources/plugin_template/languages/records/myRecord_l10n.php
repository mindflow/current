<?php
$l10nText = array(

    '{defaultLocale}' => array(
        '{recordTitleField}' => "Title",
        'tab_main' => "Main",

        /*mandatory records*/
        'record_name' => "{recordNaturalName}",
        'new_record' => "Add {recordNaturalName}",
    ),

);

?>