<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alban
 * Date: 15/10/12
 * Time: 14:33
 * To change this template use File | Settings | File Templates.
 */
class contact extends dbRecord
{
    static $tableName = 'mf_contacts';

    function init(){
        $mf = $GLOBALS['mf'];
        $l10n = $mf->l10n;
        //load translations
        $this->loadL10n('contacts');

        parent::init();

        $this->data = array_merge($this->data, array(
            'civility'   =>  array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    ' ' => '',
                    'Mr' => $l10n->getLabel('contact','mr'),
                    'Mme' => $l10n->getLabel('contact','mrs')
                ),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('',' '),
                ),
                'div_attributes' => array('class' => 'col-lg-5'),
            ),
            'first_name'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),
            'last_name'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),
            'organization'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
            ),
            'email'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
                'validation' => array(
                    'validation' => array(
                        'mandatory' => '0',
                        'filterFunc' => 'validateEmail'
                    ),
                ),
            ),
            'phone'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
            ),
            'recipient'   =>  array(
                'value' => $GLOBALS['config']['plugin-contacts']['email-destination'],
                'dataType' => 'hidden',
                'div_attributes' => array('class' => 'col-lg-5'),
            ),
            'subject'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-9'),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),
            'message'   =>  array(
                'value' => '',
                'dataType' => 'textarea',
                'div_attributes' => array('class' => 'col-lg-9'),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),
            'visit'   =>  array(
                'value' => '',
                'dataType' => 'date',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'infos_produits'   =>  array(
                'value' => '',
                'dataType' => 'checkbox',
                'div_attributes' => array('class' => 'col-lg-9'),
                'field_attributes' => array('class' => 'left-check'),
                'swap' => 1
            ),

        ));



        $this->showInEditForm = array(
            'tab_contact'=> array('civility','first_name','last_name','organization', 'phone','email','visit','infos_produits','subject','message')
        );


        static::$createTableSQL = "
          `civility` VARCHAR(4) NOT NULL DEFAULT '0',
          `first_name` varchar(255) NOT NULL,
          `last_name` varchar(255) NOT NULL,
          `organization` varchar(255) NOT NULL,
          `email` varchar(255) NOT NULL,
          `phone` varchar(20) NOT NULL,
          `recipient` varchar(255) NOT NULL,
          `subject` text NOT NULL,
          `message` text NOT NULL,
          `visit` date,
          `infos_produits` TINYINT(1) NOT NULL DEFAULT '0',

        ";

        static::$createTableKeysSQL = "";

    }





    /*
     * Loads the language file for the current class
     * @param $pluginName the name / folder name of the plugin this class belongs to
     * @param $subDir (optional) a folder name located below the languages folder, with a resulting path like :  '/mf/plugins/'.$pluginName.'/languages/'.$subDir
     */
    function loadL10n($pluginName, $subDir=''){
        //make sure base class dbRecord l10n text is loaded for any record type
        if(!isset($GLOBALS['l10n_text']['dbRecord'])){
            include DOC_ROOT.SUB_DIR.'/mf/languages/dbRecord_l10n.php';
            $GLOBALS['l10n_text']['dbRecord'] =  $l10nText;
        }

        //load l10n text for inherited record types
        if(get_class($this) != 'dbRecord' && !isset($GLOBALS['l10n_text'][get_class($this)])){

            //load the current record's translation files
            //include for custom contact class or base contact class by default
            if(isset($GLOBALS['config']['plugin-contacts']['contact-class-l10n']))
                include DOC_ROOT.SUB_DIR.$GLOBALS['config']['plugin-contacts']['contact-class-l10n'];
            else include DOC_ROOT.SUB_DIR.'/mf/plugins/'.$pluginName.'/languages/'.$subDir.'/'.get_class($this).'.php';

            //merging base class dbRecord existing Labels
            $l10nWithParent = array_merge_recursive($GLOBALS['l10n_text']['dbRecord'],$l10nText);
            $GLOBALS['l10n_text'][get_class($this)] =  $l10nWithParent;
        }

    }


    //faire un override de showRecordRditTable si on veut afficher dans toutes les langues, ou mieux ajouter l'option à la fonction

}


