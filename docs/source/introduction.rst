.. |vspace| raw:: latex

   \vspace{5mm}
   
.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>
	
|embed_js|

Introduction
************

En 13 ans de développement de sites Internet pour de nombreux clients, nous avons eu l'occasion d'utiliser différents outils de publication. Chaque outil avait ses qualités mais aussi un certain nombre de défauts et aucun ne répondait totalement à nos attentes ni à celles de nos clients. Aussi nous avons développé notre propre outil de publication, le CMS MindFlow, conçu autour des impératifs suivants :

* **Un apprentissage facile, une utilisation fluide et intuitive du module d'administration**. L'outil doit être suffisamment simple pour pouvoir être pris en main sans mode d'emploi.

|vspace|

* **Un outil de publication puissant capable de s'adapter à tous les types de mise en page** et toutes les problématiques de publication (affichages de pages simples ou applications complexes reposant sur des bases de données, possibilité de gérer plusieurs sites et plusieurs langues)

|vspace|

* **Un jeu de briques logicielles capable de s'adapter à toutes les problématiques de programmation d'applications sur le web, avec un développement rapide et une maintenance facilitée.**

|vspace|

* **D'excellentes performances à l'affichage**

|vspace|

* Affichage du module d'administration ayant recours aux **technologies web les plus conviviales, les plus efficaces et les plus récentes**

|vspace|

* Ecriture des traitements d'affichage **directement en PHP, aucun langage de script propriétaire** ne devant limiter les possibilités de programmation, fusse-t-il au bénéfice de la simplification.

L'outil MindFlow se veut simple et convivial. Il est adapté à la gestion d'applications web petite et de grande taille. Il est en mesure de gérer des sites Internet élaborés, tels que http://www.velorizons.com ou http://annuaire.syndicat-sophrologues.fr . L'outil est affiné et enrichi au cours du temps et en fonction des besoins que nous rencontrons dans nos problématiques de tous les jours.


Bases techniques de MindFlow
============================

* **MindFlow est programmé en PHP 5 / MySQL 5 ce qui lui permet de fonctionner sur la plupart des hébergements existants sous Linux ou Windows.**

|vspace|

* **MindFlow est sécurisé** : les envois de formulaires sont accompagnés d'un hash de validation qui permet de vérifier que les formulaires n'ont pas été forgés par un tiers, et les requêtes en base de données sont protégées contre les injections SQL.

|vspace|

* **MindFlow est programmé selon les concepts de la programmation Orienté Objet**, une technique de programmation qui a fait ses preuves et qui est à la base de la quasi-totalité des grandes applications existantes. Celle-ci offre à la fois un code compact, léger, facile à maintenir et évolutif. Il génère un nombre de fichiers bien moins important que les techniques MVC, et offre plus de souplesse.

|vspace|

* Le module d'administration de MindFlow est **basé sur les technologies web les plus courantes : HTML5 W3C, Bootstrap, Ajax et JQuery**. Ceci nous permet d'offrir à l'utilisateur une expérience riche et intuitive de l'interface : actions en glisser / déplacer, mise à jour d'éléments d'interface sans recharger toute la page, animations sur les blocs, etc. L'emploi de Bootstrap rend l'affichage des formulaires nativement compatible avec les périphériques mobiles.

|vspace|

* Une **interface graphique stylée en CSS, personnalisable**.

|vspace|

* MindFlow dispose d'un **système de plugins** permettant de facilement étendre le jeu de fonctionnalités du framework de base.


MindFlow n'est pas qu'un simple CMS : **c'est surtout un jeu de librairies de programmation (framework)** disposant de fonctionnalités orientées vers la publication web et la gestion de bases de données.

Par exemple, MindFlow dispose de fonctions de génération de formulaires automatisées. Dans le cadre d'un site e-commerce, qui comporte de nombreux formulaires, ceci nous permet de nous focaliser sur la nature et la qualité des fonctionnalités offertes plutôt que sur la récupération des valeurs de chaque champ des formulaires, ce qui représente un travail considérable et répétitif. **La génération automatisée permet un développement et une maintenance plus efficaces et plus faciles. C'est un atout dans le développement et le suivi de grosses applications.**

La capacité de MindFlow à mêler gestion d'applications et publication web permet en outre une intégration facile et rapide des données issues de l'application sur un site web.


