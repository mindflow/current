<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Defines a microtemplate field, where the developper can specify a list of field and map them to a portion of HTML code, such as a news block for exemple
 */
class microtemplateField implements formField
{
    /**
     * Returns the HTML code for displaying the field, initialized with the data supplied in $fieldData
     * @param $f fieldData : this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
     * $f->formID           // value of the id attribute of the <form> tag this field is included in
     * $f->mode             // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
     * $f->dataType         // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
     * $f->key              // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
     * $f->fieldData        // this is the data array of the current field
     * $f->recordUid        // the uid (Unique ID) of the record containing the current field
     * $f->recordClassName  // the class name of the record containing the current field
     * $f->record           // all the data for the record containing the current field
     * $f->microKey         // field name when processing a microTemplate field (only used when mode=="micro|")
     * $f->microIndex       // sorting index when processing a microTemplate field (only used when mode=="micro|")
     * @returns String a string containing the HTML for displaying the field
     */
    static public function displayField($f)
    {
        global $mf,$l10n,$config;

        $html = array();
        $divAttr = formsManager::getTagAttributes('div',$f->key, $f->fieldData);

        //display the label in front of the field or not
        $showLabel      = (isset($f->fieldData['showLabel']))?      $f->fieldData['showLabel']      :true;

        //display a <div> around the field or not
        $showFieldDiv   = (isset($f->fieldData['showFieldDiv']))?   $f->fieldData['showFieldDiv']   :true;

        //display custom HTML in front of the field
        $preFieldHTML   = (isset($f->fieldData['preFieldHTML']))?   $f->fieldData['preFieldHTML']   :'';

        //display custom HTML in after the field
        $postFieldHTML  = (isset($f->fieldData['postFieldHTML']))?  $f->fieldData['postFieldHTML']  :'';

        //display custom HTML in front of the field
        $preDivHTML   = (isset($f->fieldData['preDivHTML']))?   $f->fieldData['preDivHTML']   :'';

        //display custom HTML in after the field
        $postDivHTML  = (isset($f->fieldData['postDivHTML']))?  $f->fieldData['postDivHTML']  :'';

        if(isset($mf->microtemplates[$f->key])){
            $unique_id = str_replace('-','_',$f->key);

            if($showLabel)$html[] = formsManager::getFieldLabel($f->fieldData, $f->mode, $f->key, $f->recordClassName);

            if(!isset($f->fieldData['editable']))$f->fieldData['editable']=true;
	        $isEditable = filterBoolean($f->fieldData['editable']);

            if(!$isEditable){
                //read only record display, no input editing
                $html[] = $preDivHTML;
                $html[] = ($showFieldDiv)?'<div class="'.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
                $html[] = $preFieldHTML;
                $html[] = 'FormasManager->makeFieldSet() : TODO : uneditable input editing not managed yet for MicroTemplate field type
                                <input type="hidden" value="'.$f->fieldData['value'].'" name="'.$f->mode.$f->key.'" id="'.$f->mode.$f->key.'"/>';
                $html[] = $postFieldHTML;
                $html[] = ($showFieldDiv)?'</div>':'';
                $html[] = $postDivHTML;
            }
            else {
                //input editing
                $html[] = $preDivHTML;
                $html[] = ($showFieldDiv)?'<div class="col-lg-8 '.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
                $html[] = $preFieldHTML;
                $html[] = '<div class="microtemplates"><a href="#microtemplateList_'.$f->key.'" class="microTemplateButton"><i class="glyphicon glyphicon-plus"></i>&nbsp;'.$l10n->getLabel('pages','add_microtemplate').'</a>';
                $html[] = '<input type="hidden" name="microtemplate|'.$f->key.'" value="">';
                $html[] = '<ul id="sortable_'.$unique_id.'" class="sortableItem">';

                foreach($f->fieldData['value'] as $index => $microtemplate){

                    $imageSrc = $microtemplate['microtemplate_image'];
                    $title = htmlentities($microtemplate['microtemplate_title_field'], ENT_QUOTES, "UTF-8");

                    $fieldTitle = '';
                    if(isset($microtemplate['microtemplate_show_field_value'])){
                        if($microtemplate['microtemplate_data'][$microtemplate['microtemplate_show_field_value']]['dataType']=='input'){
                            $fieldTitle = $microtemplate['microtemplate_data'][$microtemplate['microtemplate_show_field_value']]['value'];
                            $fieldTitle = ($fieldTitle != '')?' : '.$fieldTitle:'';
                        }
                        else if(substr($microtemplate['microtemplate_data'][$microtemplate['microtemplate_show_field_value']]['dataType'],0,5)=='files'){
                            $image = $microtemplate['microtemplate_data'][$microtemplate['microtemplate_show_field_value']]['value'];
                            if(isset($image[0])){
                                $fieldTitle = '&nbsp;&nbsp;&nbsp;<img src="'.$mf->getUploadsDir().$image[0]['filepath'].$image[0]['filename'].'" style="height:30px;width:auto" />';
                            }
                        }
                    }

                    $html[] = '<li id="'.$index.'"><div> <img src="'.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR."microtemplates".DIRECTORY_SEPARATOR.$imageSrc.'" width=30 height=30 />'.$microtemplate['microtemplate_title_field'].$fieldTitle.'
                                            <span class="pageActions">
                                            <span class="recordAction"><a><i class="glyphicon glyphicon-move"></i></a></span>
                                            <span class="recordAction"><a class="pointer microEditLink" href="#microeditor" onclick="editMicrotemplate(\''.$f->recordUid.'\',\''.$f->recordClassName.'\',\''.$f->key.'\',\''.$index.'\');"><i class="glyphicon glyphicon-pencil"></i></a></span>
                                            <span class="recordAction">
                                                <a href="javascript:getDeleteModalMt(\''.$f->recordUid.'\',\''.$f->recordClassName.'\',\''.$f->key.'\',\''.$index.'\', \''.$title.'\');" title="'.$l10n->getLabel('backend','delete').'"><i class="glyphicon glyphicon-trash"></i></a>
                                            </span>
                                            </div></li>';

                }

                $html[] = '</ul>';
                $html[] = $postFieldHTML;
                $html[] = '</div><!-- /.microtemplates -->';
                $html[] = ($showFieldDiv)?'</div>':'';
                $html[] = $postDivHTML;

                //form security variables
                if($f->recordUid != ''){
                    $secKeys = array(
                        'action' => "sortMicrotemplates",
                        'recordUid' => $f->recordUid,
                        'recordClassName' => $f->recordClassName,
                        'key' => $f->key,
                        'mode' => $mf->mode,
                    );
                }else{
                    $secKeys = array(
                        'action' => "sortMicrotemplates",
                        'recordClassName' => $f->recordClassName,
                        'key' => $f->key,
                        'mode' => $mf->mode,
                    );
                }

                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                $html[] = "<script>
                $(document).ready(function(){
                    $('#sortable_".$unique_id."').sortable({
                        handle: '.glyphicon-move',
                        update: function() {
                            console.log($(this).sortable('toArray'));
                            var order = $(this).sortable('serialize') + '&action=sortMicrotemplates&sec=".$secHash."&fields=".$secFields."&recordUid=".$f->recordUid."&recordClassName=".$f->recordClassName."&key=".$f->key."&mode=".$mf->mode."';
                            $.post('".SUB_DIR."/mf/core/ajax-core-json.php', order);
                        }
                    });

                });
                </script>";



                $allowedMicrotemplateTypes = explode(',',$f->fieldData['allowedMicrotemplateTypes']);

                $html[] = '<div id="microtemplateList_'.$f->key.'" style="display:none;"><h5>'.$l10n->getLabel('pages','select_microtemplate').'</h5>';

                foreach($mf->microtemplates as $microtemplateKey => $microtemplate){

                    if(in_array($microtemplateKey, $allowedMicrotemplateTypes)){
                        $imageSrc = $microtemplate['microtemplate_image'];
                        if(isset($microtemplate['microtemplate_show_field_value'])){
                            $fieldTitle = $microtemplate['microtemplate_data'][$microtemplate['microtemplate_show_field_value']]['value'];
                            $fieldTitle = ($fieldTitle != '')?' : '.$fieldTitle:'';
                        }
                        else $fieldTitle = '';
                        $html[] ='<a onclick="'.$f->key.'AddMicroTemplate(\''.$microtemplate['microtemplate_title_field'].'\',\''.$fieldTitle.'\');$.fancybox.close();" class="pointer"><img src="'.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR."microtemplates".DIRECTORY_SEPARATOR.$imageSrc.'" /></a>';
                    }
                }
                $html[] = '</div>';

                //form security variables
                if($f->recordUid != ''){
                    $secKeys = array(
                        'action' => "addMicrotemplate",
                        'record_uid' => $f->recordUid,
                        'record_class' => $f->recordClassName,
                        'micro_key' => $f->key,
                        'mode' => $mf->mode
                    );
                }else{
                    $secKeys = array(
                        'action' => "addMicrotemplate",
                        'record_class' => $f->recordClassName,
                        'micro_key' => $f->key,
                        'mode' => $mf->mode
                    );
                }
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));


                $html[] = '<script>
                    function '.$f->key.'AddMicroTemplate(microtemplateName,microtemplateTitleFieldValue){
                        title = microtemplateName;
                        //submit the file name and update the record in the database
                        var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action: "addMicrotemplate", record_uid: "'.$f->recordUid.'", record_class: "'.$f->recordClassName.'",  micro_key: "'.$f->key.'", sec:"'.$secHash.'", fields:"'.$secFields.'", mode:"'.$mf->mode.'" })
                        .success(function(jsonData) {
                            index = jsonData.added_index;
                            //console.log(jsonData);

                            $("input[name=\'microtemplate|'.$f->key.'\']").next(".sortableItem").append(
                                "<li id=\'"+index+"\'><div><img src=\''.SUB_DIR.$config['templates_directory'].DIRECTORY_SEPARATOR."microtemplates".DIRECTORY_SEPARATOR.$mf->microtemplates[$f->key]['microtemplate_image'].'\' width=30 height=30 /> "+microtemplateName+microtemplateTitleFieldValue+
                                "<span class=\'pageActions\'>"+
                                    "<span class=\'recordAction\'><i class=\'glyphicon glyphicon-move\'></i></span><span class=\'recordAction\'><a class=\'pointer microEditLink\' href=\'#microeditor\' onclick=\"editMicrotemplate(\''.$f->recordUid.'\',\''.$f->recordClassName.'\',\''.$f->key.'\',\'"+index+"\');return false;\"><i class=\'glyphicon glyphicon-pencil\'></i></a></span>"+
                                    "&nbsp;<span class=\'recordAction\'><a data-controls-modal=\'windowTitleDialog\' data-backdrop=\'true\' data-keyboard=\'true\' href=\"javascript:getDeleteModalMt(\''.$f->recordUid.'\',\''.$f->recordClassName.'\',\''.$f->key.'\',\'"+index+"\', \'\' );\" title=\''.$l10n->getLabel('backend','delete').'\'><i class=\'glyphicon glyphicon-trash\'></i></a></span>"+
                                 "</span></div></li>"
                            );


                        })
                        .fail( function(xhr, textStatus, errorThrown) {
                            alert("microTemplateField.php '.$f->key.'AddMicroTemplate() : " + xhr.responseText);
                        })

                    }
                    </script>';
            }
            if(isset($f->fieldData['noClearFix']) and $f->fieldData['noClearFix'] == '1');
            else $html[] = "<div class='clearfix'></div>";

        }
        else $mf->addWarningMessage($l10n->getLabel('backend','microtemplate_not_found1')." \"".$f->key."\" ".$l10n->getLabel('backend','microtemplate_not_found2'));


        //form security variables
        $secKeys = array(
            'action' => "editMicrotemplate",
            'mode' => $mf->mode,
        );
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));


        //microtemplate functions
        $mf->formsManager->addBottomJs('microtemplateField','
            <div id="microeditorHolder"></div>
            <div id="microDeleteDialog"></div>
            <script>

            var microeditor;
            var microEditorJSID;

            var microDeleteDialog = $("#microDeleteDialog" ).dialog({
               autoOpen: false,
            });

            function closeMicroEditor(){
                microeditor.dialog( "close" );
            }

            //actions to be performed when the window is closed
            function execMicroCloseActions(){
                console.log("closeMicroEditor() calling "+microEditorJSID+"_purgeCKeditor()");
                eval(microEditorJSID+"_purgeCKeditor()");

                microeditor.dialog("destroy").remove();
                $("#microeditor").html("");
                $("#microeditorHolder" ).empty();
            }

            //microtemplate functions
            function editMicrotemplate( uid, recordClass, microtemplateKey, microtemplateIndex) {
                var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action:"editMicrotemplate", record_uid: uid, record_class: recordClass, micro_key: microtemplateKey, micro_index: microtemplateIndex,sec: "'.$secHash.'",fields: "'.$secFields.'",mode: "'.$mf->mode.'"})
                .success(function(jsonData) {

                    $("#microeditorHolder" ).html("<div id=\"microeditor\"></div>");

                    microeditor = $("#microeditor" ).dialog({
                       autoOpen: false,
                    });

                    //display the edit form
                    microeditor.dialog( {
                        closeText: "hide",
                        requestFailed: "Request failed. Please try again",
                        width: "70%",
                        position: { my: "center", at: "center", of: window },
                        modal: false,
                        //show: { effect: "fadeIn", duration: 500 }
                    } );
                    microeditor.html(jsonData.html);
                    microeditor.on( "dialogclose", function( event, ui ) {execMicroCloseActions();} );

                    microeditor.dialog( "open");
                })
                .fail( function(xhr, textStatus, errorThrown) {
                    alert("microTemplateField.php editMicrotemplate() : " + xhr.responseText);
                })
            }

            var modifyRecordUid = -1;

            function getDeleteModalMt( uid, recordClass, microtemplateKey, microtemplateIndex, title) {
                modifyRecordUid = uid;
                recClass = recordClass;
                recKey =  microtemplateKey;
                recIndex = microtemplateIndex;

                microDeleteDialog.dialog( {
                    closeText: "hide",
                    requestFailed: "Request failed. Please try again",
                    width: "40%",
                    position: { my: "center", at: "top+30%", of: recordDialog },
                    modal: true,
                    //show: { effect: "fadeIn", duration: 500 }
                } );

                microDeleteDialog.html(\'<div class="modal-header"><h4>'.$l10n->getLabel('backend','deletion').'</h4></div><div class="modal-body"><p id="deleteMessageMt">'.addslashes($l10n->getLabel('backend','areyousure_delete')).'\'+ title +\' ?</p></div><div class="modal-footer"><button type="button" class="btn btn-primary" onclick="okClickedMt();">'.$l10n->getLabel('backend','ok').'</button><button type="button" class="btn btn-default" onclick="closeMicroDeleteDialog();" >'.$l10n->getLabel('backend','close').'</button></div>\');

                microDeleteDialog.on( "dialogclose", function( event, ui ) {execMicroDeleteCloseActions();} );
                microDeleteDialog.dialog( "open");
            };

            function closeMicroDeleteDialog(){
                microDeleteDialog.dialog( "close" );
            }
            function execMicroDeleteCloseActions(){
                microDeleteDialog.html("");
            }
            ');

        //form security variables
        $secKeys = array(
            'action' => "deleteMicrotemplate",
            'mode' => $mf->mode,
        );
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));

        $mf->formsManager->addBottomJs('microtemplateField2','
            function okClickedMt() {
               var jqxhr = $.post("'.SUB_DIR.'/mf/core/ajax-core-json.php", { action:"deleteMicrotemplate", record_uid: modifyRecordUid, record_class: recClass, micro_key: recKey, micro_index: recIndex,sec: "'.$secHash.'",fields: "'.$secFields.'",mode: "'.$mf->mode.'"})
                .success(function(jsonData) {
                     //delete the <li> entry of the page from the DOM on success
                     $("#"+recIndex).remove();
                     closeMicroDeleteDialog();
                })
                .fail( function(xhr, textStatus, errorThrown) {
                    alert("microTemplateField.php okClickedMt() : " + xhr.responseText);
                })

                closeDialogMt();
            };
            </script>');

        return implode(chr(10),$html);
    }


    /**
     * Process the field value prior saving it to the database, whenever inner value adaptation is required, for SQL strict compliance for example.
     * This is executed after 'preRecord' processing functions.
     * @param $fieldData this is the data array of the current field
     * @param $value this is the submited value
     * @return mixed
     */
    static public function onSave($fieldData, $value){
        return $value;
    }

    static public function getCSS(){return "";}
    static public function getTopJS(){return "";}
    static public function getBottomJS(){return "";}







}
