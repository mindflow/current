<?php
if(class_exists('mfUserGroup')) mfUserGroup::defineUserRight('{pluginKey}', 'allowEdit{moduleClassNameUcFirst}',array(
            'value' => '0',
            'dataType' => 'checkbox',
            'valueType' => 'number',
            'processor' => '',
            'div_attributes' => array('class' => 'col-lg-2'),
        ),
        $relativePluginDir.'/languages/userRights_l10n.php'
    );