<?php
/**************************
 * Add Plugin settings
 * Fill this section in all cases
 **************************/

//Natural name of the plugin
$pluginName = "CN activités";

//Short description
$pluginDesc = "Gestion des activités des voyages";

//Base ISO2 locale for localized files
$defaultLocale = "fr";

//Class name of the plugin
$pluginClassName = "CnActivite";

//Folder name of the website where the plugin should be created
$websiteFolderName = "dynamo";

//Folder name / plugin key of the plugin
$pluginDirName = "cn_activites";


/**************************
 * Add module settings
 * Fill this section when generating a module
 **************************/

//Natural name of the module
$moduleNaturalName = 'Gestion des "activités"';

//Short description
$moduleDesc = "Permet de créer et éditer des activités";

//Class name of the module
$moduleClassName = "ActivitesManager";

//module type. Value should be either : frontend / backend or frontend,backend
$moduleTypeFrontend = 1;
$moduleTypeBackend = 1;
$moduleTypeAuth = 0;



/**************************
 * Add record settings
 * Fill this section if adding a record to an existing module
 **************************/

//Inserted record class name
$recordClassName = 'Activite';

//SQL table name where the records will be stored
$SQLTableName = 'cn_activites';

//Inserted record natural name
$recordNaturalName = "Enregistrement d'activité";

//Should the record be listed/edited into some module
$moduleWillListRecords = true;

//Column name of the listed record's title field
$recordTitleField = 'title';

//create record Label
$createRecordLabel = 'Créer une activité';

//edit record label
$editRecordLabel = "Editer l'activité";


/**************************
 * Add form settings
 * Fill this section if adding a filter form to an existing module
 **************************/

//Displayed record class name
$formClassName = 'activiteForm';
