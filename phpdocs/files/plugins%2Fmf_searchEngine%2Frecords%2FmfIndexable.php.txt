<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * List the methods an indexable subclass of dbRecord should implement to be indexed in the database.
 *
 * A search engine index features the following columns : 'class_name','object_uid','title','digest','ressource_url','tags'
 *
 */
interface mfIndexable
{
    /**
     * Indexes the current record by gathering and filtering from this record the pertinent value and feed them to the indexer by calling the searchIndexer::indexItem($this,$title,$digest,$tags,$ressourceUrl);
     * The the record page for an exemple of an advanced implementation.
     *
     * @return mixed
     */
    public function index();

    /**
     * Indexes all the records of the current class. This function is called when refreshing the whole index of the website or application
     *
     * @return mixed
     */
    public function indexAll();
}

