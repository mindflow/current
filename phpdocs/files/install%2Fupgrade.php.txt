<?php
$html[] = '<style>
    h5{color:#8f00a4;font-size:16px;}
</style>';

if ($_REQUEST['action'] == 'upgrade') {
    //print_r($mf->pluginManager->records);


    $html[] = '<h3>' . $l10n->getLabel('installTool', 'upgrade_tool') . '</h3>';
    $html[] = '<div style="margin:20px 0 0 30px;width:800px;display:block;">';
    $html[] = '<div style="margin:20px 0 0 30px;display:block;">
                    <div class="alert alert-danger" role="alert-danger"><strong>' . $l10n->getLabel('installTool', 'upgrade-warning') . '</strong></div>
                </div>';

    $html[] = '<h4>' . $l10n->getLabel('installTool', '1.9') . '&nbsp;&nbsp;&nbsp;<a class="btn btn-primary btn-sm" role="button" data-toggle="collapse" href="#scripts_19" aria-expanded="false" aria-controls="collapseExample">
  ' . $l10n->getLabel('installTool', 'view_scripts') . '
</a></h4>';

    $html[] = '<div class="collapse" id="scripts_19">
                <div class="well">';


    //SQL Strict conformance for dates
    $html[] = '<div style="margin:20px 0 0 30px;display:block;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5>' . $l10n->getLabel('installTool', '1.9_dates') . '</h5>
                            <p><strong>' . $l10n->getLabel('installTool', '1.9_dates_desc') . '</strong></p>
                            <button onclick="document.location=\'{subdir}/mf/install/index.php?action=run-script&script=dates\'">' . $l10n->getLabel('installTool', 'run_script') . '</button>
                        </div>
                    </div>
                </div>';


    //creator
    $html[] = '<div style="margin:20px 0 0 30px;display:block;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5>' . $l10n->getLabel('installTool', '1.9_creator') . '</h5>
                            <p><strong>' . $l10n->getLabel('installTool', '1.9_creator_desc') . '</strong></p>
                            <button onclick="document.location=\'{subdir}/mf/install/index.php?action=run-script&script=creator\'">' . $l10n->getLabel('installTool', 'run_script') . '</button>
                        </div>
                    </div>
                </div>';

    //parent
    $html[] = '<div style="margin:20px 0 0 30px;display:block;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5>' . $l10n->getLabel('installTool', '1.9_parent') . '</h5>
                            <p><strong>' . $l10n->getLabel('installTool', '1.9_parent_desc') . '</strong></p>
                            <button onclick="document.location=\'{subdir}/mf/install/index.php?action=run-script&script=parent\'">' . $l10n->getLabel('installTool', 'run_script') . '</button>
                        </div>
                    </div>
                </div>';

    //uploadsDir
    $html[] = '<div style="margin:20px 0 0 30px;display:block;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5>' . $l10n->getLabel('installTool', '1.9_uploadsDir') . '</h5>
                            <p><strong>' . $l10n->getLabel('installTool', '1.9_uploadsDir_desc') . '</strong></p>
                            <button onclick="document.location=\'{subdir}/mf/install/index.php?action=run-script&script=uploadsDir\'">' . $l10n->getLabel('installTool', 'run_script') . '</button>
                        </div>
                    </div>
                </div>';

    //record history
    $html[] = '<div style="margin:20px 0 0 30px;display:block;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5>' . $l10n->getLabel('installTool', '1.9_history') . '</h5>
                            <p><strong>' . $l10n->getLabel('installTool', '1.9_history_desc') . '</strong></p>
                            <button onclick="document.location=\'{subdir}/mf/install/index.php?action=run-script&script=history\'">' . $l10n->getLabel('installTool', 'run_script') . '</button>
                        </div>
                    </div>
                </div>';



    $html[] = '
        </div>
    </div>';



    $html[] = '</div>';

}
if ($_REQUEST['action'] == 'run-script') {

    switch ($_REQUEST['script']) {
        case 'dates':
            require_once 'update_scripts/1.9/dates.php';
            break;

        case 'uploadsDir':
            require_once 'update_scripts/1.9/uploadsDir.php';
            break;

        case 'creator':
            require_once 'update_scripts/1.9/creator.php';
            break;

        case 'parent':
            require_once 'update_scripts/1.9/parent.php';
            break;

        case 'history':
            require_once 'update_scripts/1.9/history.php';
            break;
    }

}

