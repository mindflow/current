<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

$execution_start = time();

require_once '../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//we are serving json
header('Content-type: application/json');

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();
$mf = $mindFlowBackend->mf;
$l10n = $mf->l10n;

//print_r($_REQUEST);

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec()){



    if(isset($_REQUEST['action'])){

        if($_REQUEST['action'] == 'searchIndexFilter'){

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm()){



                $sqlConditions = '';

                //traitement des paramètres du formulaire


                if(trim($form->data['MotClef_filter']['value']) != '')
                    $sqlConditions .= ' AND MATCH(`title`,`digest`,`tags`) AGAINST(\''.$form->data['MotClef_filter']['value'].'*\' IN BOOLEAN MODE)';

                $deletedCondition = ' AND tbl_offre_service.deleted=0';
                if($form->data['Deleted_filter']['value'] != '' and $form->data['Deleted_filter']['value'] == '1')
                    $deletedCondition = ' AND deleted=1';

//echo "conditions = ".$sqlConditions;

                $module = new manageSearchIndex();
                $html = $module->listRecords($sqlConditions,'');


                $html = str_replace("{subdir}",SUB_DIR,$html);
                $html = str_replace("{module}",$_REQUEST['module'],$html);
                isset($_REQUEST['submodule'])?$html = str_replace("{submodule-id}",'consulterOffres',$html):$html = str_replace("{submodule-id}",'',$html);
                isset($_REQUEST['module'])?$html = str_replace("{module-id}",'offres',$html):$html = str_replace("{module-id}",'',$html);
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}');
            }
            else{
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('backend','form-process-error')).'}');
            }
        }


    }
    else {
        $mf->db->closeDB();
        die('{"jsonrpc" : "2.0", "result" : "error", "message" : "No action specified."}');
    }
    //echo "Error : No action specified.";
}
else {
    $mf->db->closeDB();
    die('{"jsonrpc" : "2.0", "result" : "error", "message" : "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}');
}

$mf->db->closeDB();




