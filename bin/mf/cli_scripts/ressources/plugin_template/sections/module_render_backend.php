<?php
if($mf->mode == 'backend')
        {
            //process backend display
            if (isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])) {

                $moduleBody = file_get_contents(DOC_ROOT . SUB_DIR . $config['website_pluginsdir'] . '/{pluginDirName}/ressources/templates/{moduleClassName}.html');
                $moduleBody = str_replace("{module-body}", $this->pageOutput, $moduleBody);

                $moduleBody = str_replace("{local-menu}", $this->localMenu, $moduleBody);
                $moduleBody = str_replace("{menu-title}", $l10n->getLabel('{moduleClassName}', 'menu_title'), $moduleBody);

                $mainTemplate = str_replace("{current_module}", $moduleBody, $mainTemplate);
                //breadcrumb
                $mainTemplate = str_replace("{section}", $this->section, $mainTemplate);
                $mainTemplate = str_replace("{module-name}", $this->moduleName, $mainTemplate);
                $mainTemplate = str_replace("{submodule-name}", $this->subModuleName, $mainTemplate);

            }
        }