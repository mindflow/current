<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_countryData/records/mfCountry.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_countryData/forms/countriesFilter.php';

class manageCountries implements module
{
    private $moduleKey = 'manageCountries';
    private $moduleType = 'backend';
    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';
    var $localMenu='';

     var $userGroup;


    function prepareData(){
        global $mf,$l10n;

        $this->pageOutput = '';

        if($mf->mode == 'backend'){


            //check access rights
            $this->userGroup = $mf->currentUser->getUserGroup();

            if($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('mf_countryData','allowEditCountries')==1))){

                if(!$mf->pluginManager->backendMenuExists('settings'))$mf->pluginManager->addBackendMenu('settings', 350);
                //add the plugin to the backend menus
                $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=manageCountries"><span class="glyphicon glyphicon-menu glyphicon-globe" aria-hidden="true"></span>'.$l10n->getLabel('manageCountries','menu_title').'</a>','settings', LOW_PRIORITY);


                if(isset($_REQUEST['module']) && ($_REQUEST['module'] == $this->moduleKey)){


                    //breadcrumb
                    $this->section = '';//$l10n->getLabel('backend','settings');
                    $this->moduleName = " <span class=\"glyphicon glyphicon-menu glyphicon-globe\" aria-hidden=\"true\"></span> ".$l10n->getLabel('manageCountries','menu_title');
                    $this->subModuleName = "";

                     $this->localMenu .='<li rel="manageCountries"><a href="{subdir}/mf/index.php?module=manageCountries&submodule=">'.$l10n->getLabel('manageCountries','menu_title').'</a></li>';


                    if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];


                    switch($this->action){

/*
                        case "create":
                            $this->pageOutput .= '<h3>'.$l10n->getLabel('manageCountries','new_record').'</h3>';
                            $this->pageOutput .= $this->create();
                            break;

                        case "edit":
                            $this->pageOutput .= '<h3>'.$l10n->getLabel('manageCountries','edit_record').'</h3>';
                            //case edit requested by some module as a GET url parameter
                            if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                $this->pageOutput .= $this->edit(intval($_GET['uid']));
                            }

                            break;

                        case "view":
                            $this->pageOutput .= '<h3>'.$l10n->getLabel('manageCountries','view_record').'</h3>';
                            //case edit requested by some module as a GET url parameter
                            if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                $this->pageOutput .= $this->edit(intval($_GET['uid']));
                            }

                            break;


                        case "delete":
                            $this->pageOutput .= $this->delete($_REQUEST['uid']);
                            break;
*/

                        case "list":
                        default:

                            //composition du filtre d'affichage
                            $countriesFilter = new countriesFilter();

                            $form = $mf->formsManager;

                            $this->pageOutput .= '<div id="formFilter">';
                            $this->pageOutput .= $form->editForm(SUB_DIR.'/mf/plugins/mf_countryData/ajax-search-json.php', 'countriesFilter', 'updateContent', $countriesFilter, $this->moduleKey, '',  true,$l10n->getLabel('backend','search'),null);


                            $this->pageOutput .= '<script>
                                function updateContent(jsonData){
                                    $("#recordEditTable_mfCountry").html(jsonData.message);
                                }
                            </script></div>';





                            $this->pageOutput .= '<div id="section-to-print">'.$this->listRecords('').'</div>';
                            break;
                    }

                }
            }

        }

    }

    function render(&$mainTemplate){
        global $mf,$l10n;

        if($mf->mode == 'backend'){
            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                if($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('mf_countryData','allowEditCountries')==1))){

                    $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/mf_countryData/ressources/templates/manageCountries.html');
                    $moduleBody = str_replace("{submodule-body}",$this->pageOutput,$moduleBody);
                    $moduleBody = str_replace("{menu-title}",$l10n->getLabel('manageCountries','menu_title'),$moduleBody);

                    $mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);
                    $mainTemplate = str_replace("{local-menu}",$this->localMenu,$mainTemplate);

                    //breadcrumb
                    $mainTemplate = str_replace("{section}",$this->section,$mainTemplate);
                    $mainTemplate = str_replace("{module-name}",$this->moduleName,$mainTemplate);
                    $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);
                }
            }

        }
        else if($mf->mode == 'frontend'){}
    }


    function getType(){
        return $this->moduleType;
    }

    function listRecords($sqlConditions = ''){

        global $mf,$l10n,$pdo;

        $userGroup = $mf->currentUser->data['user_group']['value'];
        $country = new mfCountry();


        //count entries in the index
        $sql = "SELECT COUNT(*) FROM mf_countries WHERE deleted=0";
        $stmt = $pdo->prepare($sql);


        $stmt->execute();
        $indexCount = $stmt->fetchColumn();
        if($indexCount == false)$indexCount = 0;

        $secKeys = array('action' => 'createRecord', 'record_class' => 'mfCountry');
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));


        $buttons = ' '.$indexCount.' '.(($indexCount > 1)?$l10n->getLabel('manageCountries','entries_count_many'):$l10n->getLabel('manageCountries','entries_count_single'));


        return $country->showRecordEditTable(
            array(
                'SELECT' => 'cn_iso_2,cn_short_fr,cn_official_name_local,zip_regexp',
                'FROM' => 'mf_countries',
                'JOIN' => '',
                'WHERE' => 'deleted=0'.$sqlConditions,
                'ORDER BY' => 'cn_iso_2',
                'ORDER DIRECTION' => 'ASC',
            ),
            'manageCountries',
            '',
            'cn_short_fr',

            $keyProcessors = array(
                //'cn_short_fr' => 'mfCountry::utf8Name',
            ),
            $page = NULL,

            array(
                'create' => 1,
                'view' => 1,
                'edit'=> 1,
                'delete'=> 1,
            ),
            array(
                'ajaxActions' => true,
                'buttons' => $buttons,
                'hideColumns' => '',
                'listRecordsInAllLanguages' => 'true',
                'debugSQL' => 0,
                ),
            'mfCountry'
        );


    }

    /*function create(){
        $mf = &$GLOBALS['mf'];
        $userGroup = $mf->currentUser->data['user_group']['value'];

        $country = new mfCountry();
        //$searchIndex->data['fieldName']['value'] = $mf->currentUser->data['NoAccorderie']['value'];
        //$searchIndex->data['fieldName']['editable'] = false;

        $form = $mf->formsManager;
        return $form->editRecord($country, 'manageCountries', $this->moduleKey, true,true,false,true, 1, null, null, 'countries');

    }


    function edit($uid){
        $mf = &$GLOBALS['mf'];
        $userGroup = $mf->currentUser->data['user_group']['value'];

        $country = new mfCountry();
        $country->load($uid);

        $form = $mf->formsManager;
        return $form->editRecord($country, 'manageCountries', $this->moduleKey, true,true,false,true, 1, null, null, 'countries');



    }

    function delete($uid){
        $country = new mfCountry();
        $country->load($uid);
        $country->delete();
        return $this->listRecords();
    }*/



}

