<?php
/**************************
 * Add Plugin settings
 * Fill this section in all cases
 **************************/

//Natural name of the plugin
$pluginName = "Tâches";

//Short description
$pluginDesc = "Gestion des tâches de production";

//Base ISO2 locale for localized files
$defaultLocale = "fr";

//Class name of the plugin
$pluginClassName = "taches";

//Folder name of the website where the plugin should be created
$websiteFolderName = "dynamo";

//Folder name / plugin key of the plugin
$pluginDirName = "dyn_taches";


/**************************
 * Add module settings
 * Fill this section when generating a module
 **************************/

//Natural name of the module
$moduleNaturalName = "Tâches de production";

//Short description
$moduleDesc = "Gestion des tâches de production";

//Class name of the module
$moduleClassName = "gestionTaches";

//module type. Value should be either : frontend / backend or frontend,backend
$moduleTypeFrontend = 0;
$moduleTypeBackend = 1;
$moduleTypeAuth = 0;



/**************************
 * Add record settings
 * Fill this section if adding a record to an existing module
 **************************/

//Inserted record class name
$recordClassName = 'tacheAerien';

//SQL table name where the records will be stored
$SQLTableName = 'dyn_taches_aerien';

//Inserted record natural name
$recordNaturalName = 'Tâches aérien';

//Should the record be listed/edited into some module
$moduleWillListRecords = false;

//Column name of the listed record's title field
$recordTitleField = 'titre';

//create record Label
$createRecordLabel = 'Créer une tâche';

//edit record label
$editRecordLabel = 'Editer la tâche';


/**************************
 * Add form settings
 * Fill this section if adding a filter form to an existing module
 **************************/

//Displayed record class name
$formClassName = 'filtreTaches';
