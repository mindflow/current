<?php
/*
   Copyright 2017 Alban Cousinié <info@mindflow-framework.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Short Description
 *
 * Long Description
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';



class siteInfos implements module {
	private $moduleKey = 'siteInfos';
	private $moduleType = 'backend,frontend';
	private $dependencies = '';
	private $action = 'list';

	var $siteInfoOutput;

	function __construct() {
		global $mf, $siteInfoTemplate, $config, $l10nManager;

		//Load and add the siteInfo template definition to the $mf->templates array
		if ( isset( $config['plugins']['siteInfos']['siteInfo-template-def'] ) && $config['plugins']['siteInfos']['siteInfo-template-def'] != '' ) {
			include DOC_ROOT . SUB_DIR . $config['plugins']['siteInfos']['siteInfo-template-def'];
		} else {
			include DOC_ROOT . SUB_DIR . '/mf/plugins/siteInfos/templates/siteInfo-template.php';
		}

		//Load and add the siteInfo template localization file
		if(isset($config['plugins']['siteInfos']['siteInfo-class-language']) && $config['plugins']['siteInfos']['siteInfo-class-language'] != '')
			l10nManager::loadL10nFile('templates', $config['plugins']['siteInfos']['siteInfo-class-language']);
		else
			l10nManager::loadL10nFile('templates',  '/mf/plugins/siteInfos/languages/records/siteInfo_l10n.php');

		$mf->addTemplates( $siteInfoTemplate );

	}


	function prepareData() {
		global $mf, $l10n;
		$this->pageOutput = '';

		if ( $mf->mode == 'frontend' ) {

			//$currentPageUid = $mf->info['currentPageUid'];

		} else if ( $mf->mode == 'backend' ) {

			//add the plugin to the backend menus
			$mf->pluginManager->addEntryToBackendMenu( '<a href="{subdir}/mf/index.php?module=siteInfos"><span class="glyphicon glyphicon-menu glyphicon glyphicon-info-sign" aria-hidden="true"></span>' . $l10n->getLabel( 'siteInfos', 'menu_title' ) . '</a>', 'settings', LOW_PRIORITY );


			if ( isset( $_REQUEST['module'] ) && ( $this->moduleKey == $_REQUEST['module'] ) ) {

				$this->moduleName = '<span class="glyphicon glyphicon-menu glyphicon glyphicon-info-sign" aria-hidden="true"></span>' . $l10n->getLabel( 'siteInfos', 'menu_title' );

				if ( isset( $_REQUEST['action'] ) ) {
					$this->action = $_REQUEST['action'];
				}

				$this->pageOutput .= $this->editSiteInfo();

			}
		}

	}

	function render( &$mainTemplate ) {
		global $mf;

		if ( $mf->mode == 'backend' ) {

			if ( ! isset( $_REQUEST['module'] ) || ( $this->moduleKey == $_REQUEST['module'] ) ) {

				$siteInfosBody = file_get_contents( DOC_ROOT . SUB_DIR . '/mf/plugins/siteInfos/ressources/templates/siteInfos.html' );

				$siteInfosBody = str_replace( "{siteInfo}", $this->pageOutput, $siteInfosBody );
				$siteInfosBody = str_replace( "{module-name}", $this->moduleName, $siteInfosBody );

				$mainTemplate = str_replace( "{current_module}", $siteInfosBody, $mainTemplate );
			}
		} else if ( $mf->mode == 'frontend' ) {
			$siteInfo = siteInfo::getSiteInfo();

			foreach ( $siteInfo->data['template_content']['value'] as $key => $array ) {
				$mainTemplate = str_replace( "{" . $key . "}", $array['value'], $mainTemplate );
			}

		}
	}

	function setup() {
		/*
		$siteInfo = new siteInfo();
		$siteInfo->createSQLTable();*/
	}

	function getType() {
		return $this->moduleType;
	}


	function createSiteInfo() {
		global $mf;
		$siteInfo = new siteInfo();
		$form     = $mf->formsManager;

		return $form->editRecord( $siteInfo, $this->moduleKey, '' );
	}

	function editSiteInfo() {
		global $mf;
		//load siteInfo object
		$siteInfo = siteInfo::getSiteInfo();

		//edit object
		$form = $mf->formsManager;

		return $form->editRecord( $siteInfo, $this->moduleKey, '', true, false, false, false );

	}


	function getDependencies() {
		return $this->dependencies;
	}

}