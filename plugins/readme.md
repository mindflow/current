
Mindflow non-standard plugins. 

How to use :
- Place the folder of the desired plugin inside the /mf_websites/mywebsite/plugins folder
- Add the plugin key to the 'load_plugins' variable of your website configuration file ( /mf_websites/mywebsite/config*.php )
- Copy the content of /mf_websites/mywebsite/plugins/myPlugin/defaultConfiguration.php or sampleConfiguration.php into your website configuration file.
- Customize the plugin configuration.
- Use !

For now, the plugins are not documented. The source code is your friend. 
All these plugins are currently being used and work beautifully if properly configured. 
 