<?

$l10nText = array(

    'fr' => array(
        'titre-adresse1' => 'Titre de votre adresse',
        'sstitre-adresse1' => 'Sous-titre de votre adresse',
        'adresse1' => 'Votre adresse',
        'latitude-adresse1' => 'Latitude (valeur décimale) pour Google maps',
        'longitude-adresse1' => 'Longitude (valeur décimale) pour Google maps',
        'social-facebook' => 'Lien vers votre page facebook',
        'tel-work' => 'Téléphone',
        'tel-mobile' => 'Téléphone mobile',
        'template_content' => '',

        /*mandatory records*/
        'no_record' => 'Il n\'existe pas d\'info genérale pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=siteInfos&action=create">créer une info générale</a>.',
    ),


);



?>