<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
//{module_include_record}

//[module_include_form_FullSyncFilter]
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/table_sync/forms/FullSyncFilter.php';
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/table_sync/FullSyncUtils.php';
//[/module_include_form_FullSyncFilter]
//{module_include_form}



class FullSyncManager implements module
{

    private $moduleKey = 'FullSyncManager';
    private $moduleType = 'backend';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function __construct()
    {

    }


    function prepareData()
    {
        global $mf,$l10n,$config;

        $this->pageOutput = '';

        //[prepareData_backend]
		if($mf->mode == 'backend')
        {

            //process backend display
            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('table_sync', 'allowEditSync') == 1)));

            if ($this->moduleAccess) {
                //add the plugin to the backend menus
                //$mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=FullSyncManager"><span class="glyphicon glyphicon-menu glyphicon-refresh" aria-hidden="true"></span>' . $l10n->getLabel('FullSyncManager', 'menu_title') . '</a>', 'datas', LOW_PRIORITY);

                if (isset($_REQUEST['submodule']) && ($this->moduleKey == $_REQUEST['submodule'])) {

                    //$this->localMenu = '<li rel="' . $this->moduleKey . '" class="active"><a href="{subdir}/mf/index.php?module=' . $this->moduleKey . '"><span class="glyphicon glyphicon-menu glyphicon-refresh" aria-hidden="true"></span>{menu-title}</a></li>';


                    //breadcrumb
	                $this->section = $l10n->getLabel('backend', 'administration');
	                $this->subModuleName = ' / '. $l10n->getLabel('FullSyncManager', 'menu_title'); //<span class="glyphicon glyphicon-menu glyphicon-refresh" aria-hidden="true"></span>

                    if (isset($_REQUEST['action'])) $this->action = $_REQUEST['action'];

                    //[module_list_backend]
					switch($this->action){

                        case "list":
                        default:

                            //[module_insert_form_FullSyncFilter]
							//composition du filtre d'affichage
                            $recordsFilter = new FullSyncFilter();

                            $form = $mf->formsManager;

                            $this->pageOutput .= '<div id="formFilter">';
                            $this->pageOutput .= $form->editForm(SUB_DIR.$config['website_pluginsdir'].'/table_sync/tableSync-json-backend.php', 'FullSyncFilter', 'updateContent', $recordsFilter, $this->moduleKey, '', true, $l10n->getLabel('backend','search'));
                            $this->pageOutput .= '<script>
                                function updateContent(jsonData){
                                    $("#recordEditTable_FullSync").html(jsonData.message);
                                }
                            </script></div>';
							//[/module_insert_form_FullSyncFilter]
							//{module_insert_form}


                            $this->pageOutput .= $this->listRecords('','');
                            break;
                    }
					//[/module_list_backend]
                    //{module_list_backend}


                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >' . $l10n->getLabel('backend', 'module_no_access') . '</div>';
                $this->localMenu = '';
            }
        }
		//[/prepareData_backend]
		//{module_prepareData}
    }


    function render(&$mainTemplate)
    {
        global $mf,$l10n,$config;

        //[render_backend]
		if($mf->mode == 'backend')
        {
            //process backend display
            if (isset($_REQUEST['submodule']) && ($this->moduleKey == $_REQUEST['submodule'])) {

                $mainTemplate = str_replace("{module-body}",$this->pageOutput,$mainTemplate);
                $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);
                $mainTemplate = str_replace("{submodule-id}",$this->moduleKey,$mainTemplate);

            }
        }
		//[/render_backend]
		//{module_render}
    }


    function getType()
    {
        return $this->moduleType;
    }

    //[module_list_function_FullSync]
	function listRecords($sqlConditions = '', $dateConditions = '',  $deletedCondition=null)
{
        global $mf,$l10n;

        //using time to store session SQL in case of multiple open tabs in the browser.
        //$time will allow distincting the good tab because the session now knows when the form was generated
        $sec = md5(microtime());

        if($deletedCondition==null)$deletedCondition=' AND '.FullSync::getTableName().'.deleted=0';



        $record = new FullSync();

        $buttons = '<button class="btn-primary mf-btn-new btn-xs smallBtnText" type="button" onClick="openRecord(\''.SUB_DIR.FullSyncUtils::createFullSync().'\',\'90%\',\'\',\''.$l10n->getLabel('FullSync','record_name').'\');">
                <i class="glyphicon glyphicon-plus"></i> '.$l10n->getLabel('FullSync','new_record').'
            </button>';

	$js = "<script>
		function replay(uid,secHash){
		    
		    $('#state_'+uid).html('<span class=\"orange\">".$l10n->getLabel('FullSync','Sending')."</span> &nbsp;<img src=\"".SUB_DIR."/mf/backend/templates/mf-default/img/small-loading.gif\" />');
		    $('#result_'+uid).html('');
		    
		    $.ajax({
	        url: '".SUB_DIR.$GLOBALS['config']['website_pluginsdir']."/table_sync/tableSync-json-backend.php',
	        type: 'POST',
	        data: {
	            action:\"replayFull\",
	            uid: uid,
	            sec: secHash,
	            fields: 'action,uid'
	        },
	        dataType: 'json',
	        success: function(jsonData) {
	            if (jsonData.result == 'success') {
	
	                //update table row data
	                 $('#state_'+uid).replaceWith(jsonData.out_state);
	                 $('#result_'+uid).replaceWith(jsonData.out_result);
	                 
	            } else {
	                
	                 if (jsonData.message != '') {
	                     showError(jsonData.message);
	            		 return false;
	                 }
	                
	                 //update table row data
	                 $('#state_'+uid).replaceWith(jsonData.out_state);
	                 $('#result_'+uid).replaceWith(jsonData.out_result);
	            }
	        },
	        error: function(jsonData) {
	            if (jsonData.message != '') {
                     showError(jsonData.message);
                     return false;
                 }
                 else showError(jsonData);
	            return false;
	        }
	    });
		}
		
		
		function resetState(uid,recordClass,secHash){
		    
		    $('#state_'+uid).html('');
		    $('#result_'+uid).html('');
		    
		    $.ajax({
	        url: '".SUB_DIR.$GLOBALS['config']['website_pluginsdir']."/table_sync/tableSync-json-backend.php',
	        type: 'POST',
	        data: {
	            action:\"resetState\",
	            uid: uid,
	            record_class: recordClass, 
	            sec: secHash,
	            fields: 'action,uid,record_class' 
	        },
	        dataType: 'json',
	        success: function(jsonData) {
	            if (jsonData.result == 'success') {
	
	                //update table row data
	                 $('#state_'+uid).replaceWith(jsonData.state);
	                 $('#result_'+uid).replaceWith(jsonData.result);
	                 
	            } else {
	                
	                 if (jsonData.message != '') {
	                     showError(jsonData.message);
	            		 return false;
	                 }
	                
	                 //update table row data
	                 $('#state_'+uid).replaceWith(jsonData.state);
	                 $('#result_'+uid).replaceWith(jsonData.result);
	            }
	        },
	        error: function(jsonData) {
	            if (jsonData.message != '') {
                     showError(jsonData.message);
                     return false;
                 }
                 else showError(jsonData);
	            return false;
	        }
	    });
		}
		
		</script>
		
		";

	return $js.$record->showRecordEditTable(
            array(
                'SELECT' => 'name,enabled,local_table,out_rule,destination,state,time_last_fired,result',
                'FROM' => FullSync::getTableName(),
                'JOIN' => '',
                'WHERE' => '1=1'.$dateConditions.$sqlConditions.$deletedCondition,
                'ORDER BY' => 'creation_date',
                'ORDER DIRECTION' => 'ASC',
            ),
            'FullSyncManager',//'table_sync',
            '',
            'name',
            $keyProcessors = array(
                'time_last_fired' => 'dbRecord::formatDateTime',
                'enabled' => 'FullSyncManager::enabledStatus',
                'state' => 'FullSync::getState',
                'result' => 'FullSync::getResult',
            ),
            $page = NULL,

            array(
                'create' => 0,
                'view' => 1,
                'edit' => 1,
                'delete'=> 1
            ),
            array(
                'ajaxActions' => true,
                'buttons' => $buttons,
                'columnClasses' => array(
                    'creation_date' => 'hidden-xs',
                    'email' => 'hidden-xs hidden-sm',
                    'deleted' => 'hidden-xs hidden-sm',
                ),
                'debugSQL' => 0,
                'showRefresh' => true,
                'editRecordButtons' => array(
                    'showSaveButton' => true,
                    'showSaveCloseButton' => true,
                    'showPreviewButton' => false,
                    'showCloseButton' => true
                ),

            ),
            'FullSync' //recordEditTableID
        );
    }
	//[/module_list_function_FullSync]
	//{module_list_function}

	static function enabledStatus($key, $value, $locale, $row){
		if($value==1){
			return '<img src="'.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/table_sync/ressources/img/green-dot.png" />';
		}
		else{
			return '<img src="'.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/table_sync/ressources/img/grey-dot.png" />';
		}
	}

}

