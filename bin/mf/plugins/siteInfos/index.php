<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/siteInfos/modules/siteInfos.php';


class siteInfosPlugin implements plugin{

    function setup(){

    }

    function getName(){
        global $l10n;
        return $l10n->getLabel('siteInfosPlugin','name');
    }

    function getDesc(){
        global $l10n;
        return $l10n->getLabel('siteInfosPlugin','desc');
    }

    function getPath(){
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey(){
        return "siteInfos";
    }

    function getVersion(){
        return "1.1";
    }

    function getDependencies(){
        $dependencies = array();
        return $dependencies;
    }

    function init(){
        global $mf,$l10n,$config;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');
        $l10n->loadL10nFile('templates', $relativePluginDir.'/languages/templates_l10n.php');

        //inform MindFlow of the new dbRecords supplied by this plugin

	    //include custom class or base class by default
	    if(isset($config['plugins']['siteInfos']['siteInfo-class']) && $config['plugins']['siteInfos']['siteInfo-class'] != '')
//		    require_once DOC_ROOT.SUB_DIR.$config['plugins']['siteInfos']['siteInfo-class'];
//            $mf->pluginManager->addRecord('siteInfos', 'siteInfo', DOC_ROOT.SUB_DIR.$config['plugins']['siteInfos']['siteInfo-class']);
            $mf->pluginManager->addRecord('siteInfos', 'siteInfo', $config['plugins']['siteInfos']['siteInfo-class']);
	    else
		    $mf->pluginManager->addRecord('siteInfos', 'siteInfo', $relativePluginDir.'/records/siteInfo.php');
//	    	require_once DOC_ROOT.SUB_DIR.'/mf/plugins/siteInfos/records/siteInfo.php';


//        $mf->pluginManager->addRecord('siteInfos', 'siteInfo', $relativePluginDir.'/records/siteInfo.php');

        $mf->pluginManager->loadModule($relativePluginDir,"modules","siteInfos", true);
    }
}
$mf->pluginManager->addPluginInstance(new siteInfosPlugin());

