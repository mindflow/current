<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/mf.php';

/**
 * Handles processing and display of the MindFlow backend. The backend is the administration user interface for an application or website.
 *
 * The backend class creates a $mf object and configures it for backend display. It also loads the backend template and substitutes the required markers for backend display.
 *
 *
 */
class backend
{
    var $currentModule = NULL;
    var $currentAction = NULL;
    var $currentRecord = NULL;
    var $template = '';



    //initializes the CMS objects first so they can be exploited by all data processing functions
    function __construct(){
        //speed measurement stat
        $GLOBALS['execution_start'] = microtime(true);

        global $mf,$l10n,$config,$logger;


        //the $mf object holds the data and provides the standard cms function set to all modules, plugins and services
        $mf = new mf('backend');



        //store a reference to this object the $mf object for further reference from all backend modules
        $mf->xEnd = &$this;

        //log the backend initialization
        $logger->info($l10n->getLabel('main','init_backend'));

        if(isset($config['backend_template']))$this->template = $config['backend_template'];
        else $this->template = SUB_DIR.'/mf/backend/templates/mf-default';

    }



    //process all the functions that have to be executed for the data / page processing
    function prepareData(){
        global $mf,$config;

        $this->currentModule = $this->getCurrentModule();
        $this->currentAction = (isset($_REQUEST['action']))?$_REQUEST['action']:'';
        $this->currentRecord = (isset($_REQUEST['uid']))?$_REQUEST['uid']:'';

        //load the templates
        require_once DOC_ROOT.SUB_DIR.$config['templates_directory'].'/templates.php';


        //MindFlow Backend
        $mf->addCss('<link rel="stylesheet" href="'.SUB_DIR.$this->template.'/css/backend.css" type="text/css" media="screen">', true, false);
        $mf->addCss('<link rel="stylesheet" href="'.SUB_DIR.$this->template.'/css/background.css.php" type="text/css" media="screen">', true, false);
        $mf->addCss('<link rel="stylesheet" href="'.SUB_DIR.$this->template.'/css/backend-print.css" type="text/css" media="print">', true, false);
        $mf->addTopJs('<script src="'.SUB_DIR.$this->template.'/js/backend-top.js"></script>', true, false);
        $mf->addBottomJs('<script src="'.SUB_DIR.$this->template.'/js/backend-bottom.js"></script>', true, false);

        if(isset($config['backend_additional_css'])){
            $this->addCss('<link rel="stylesheet" href="'.SUB_DIR.$config['backend_additional_css'].'" type="text/css" media="screen">', true, false);
        }

        //$mf->formsManager->initFormCssAndJS();
        if(CLI_MODE){
            //create default cli user with all rights by default
            $mf->currentUser = new mfUser();
            $mf->currentUser->data['uid']['value'] = 0;
            $mf->currentUser->data['backend_access']['value'] = 1;
            $mf->currentUser->data['administrator']['value'] = 1;
            $mf->currentUser->data['last_name']['value'] = 'CLI User';

            //then prepare backend data if the user is identified
            $mf->mode = 'backend';
            //performance tuning : no $mf->formsManager->prepareData() in cli mode (because there should be no HTML, JS or CSS to display)
            $mf->pluginManager->prepareData();
        }
        else if(isset($_SESSION['current_be_user'])){
            // WEB_MODE : there is no $_SESSION in cli mode
            //reload current user in case his profile has been modified during a backend operation (user profile self editing)
            $_SESSION['current_be_user']->reload();
            $mf->currentUser = $_SESSION['current_be_user'];
            //then prepare backend data if the user is identified
            $mf->mode = 'backend';
            $mf->formsManager->prepareData();
            $mf->pluginManager->prepareData();
        }
        else{
            //if no user is identified, prepare identification
            $mf->mode = 'authentification';
            $mf->formsManager->prepareData();
            $mf->pluginManager->prepareData();
        }

    }

    //executes the render() function of every object and outputs the page
    function renderPage(){
        global $mf,$l10n,$config,$logger;
        $html = &$mf->html;

        if(isset($mf->currentUser)){
            //user logged in

            //process page header
            $bodyParams=" id='main'";
            $mf->header = $this->getStandardHeader($bodyParams);

            //display admin template
            $mf->body = file_get_contents(DOC_ROOT.SUB_DIR.$this->template.'/main.html');

            //rendering backend plugins
            $mf->mode = 'backend';
            $mf->pluginManager->render($mf->body);

            if(isset($config['backend_logo']) && $config['backend_logo'] != '') $logoHTML = '<a href="'.SUB_DIR.'/mf/"><img src="'.$config['backend_logo'].'"></a>';
            else $logoHTML = '<h1>MindFlow <span class=version>( {version} )</span></h1>';


            $mf->body = str_replace("{site_admin}",$l10n->getLabel('backend','site_admin'),$mf->body);
            $mf->body = str_replace("{logo}",$logoHTML,$mf->body);
            $mf->body = str_replace("{version}",$GLOBALS['mf_version'],$mf->body);
            $mf->body = str_replace("{sitename}",$config['sitename'],$mf->body);
            $mf->body = str_replace("{languages-select}",$this->getLanguagesForm(),$mf->body);
            $mf->body = str_replace("{subdir}",SUB_DIR,$mf->body);
            $mf->body = str_replace("{user}",$l10n->getLabel('backend','user'),$mf->body);
            $mf->body = str_replace("{menu}",$l10n->getLabel('backend','menu'),$mf->body);
            $mf->body = str_replace("{toggle_nav}",$l10n->getLabel('backend','toggle_nav'),$mf->body);
            $mf->body = str_replace("{logged_username}",$mf->currentUser->data['first_name']['value'].' '.$mf->currentUser->data['last_name']['value'],$mf->body);
            if(isset($_REQUEST['submodule']))$mf->body = str_replace("{submodule-name}",$_REQUEST['submodule'],$mf->body);

            //process page footer
            $mf->footer = $this->getStandardFooter();

        }
        else{
            //user not identified, display authentification

            //process page header
            $bodyParams=" id='login'";
            $mf->header = $this->getStandardHeader($bodyParams);

            //display login template
            $mf->body = file_get_contents(DOC_ROOT.SUB_DIR.$this->template.'/login.html');

            //rendering backend plugins
            $mf->mode = 'authentification';
            $mf->pluginManager->render($mf->body);

            //process page footer
            $mf->footer = $this->getStandardFooter();
        }

        //processing UI messages
        $warnings=array();
        foreach($mf->warningMessages as $message) $warnings[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{warning-messages}",implode(chr(10),$warnings),$mf->header);

        $errors=array();
        foreach($mf->errorMessages as $message) $errors[] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{error-messages}",implode(chr(10),$errors),$mf->header);

        $success=array();
        foreach($mf->successMessages as $message) $success[] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{success-messages}",implode(chr(10),$success),$mf->header);

        $infos=array();
        foreach($mf->infoMessages as $message) $infos[] = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
        $mf->header = str_replace("{info-messages}",implode(chr(10),$infos),$mf->header);


        $execDurationText = $mf->l10n->getLabel('main','execution_duration');
        $logger->notice($execDurationText." ".   number_format(   (  microtime(true)  -  $GLOBALS['execution_start'])    ,4)      . "ms");

        $memUsageText = $mf->l10n->getLabel('main','memory_usage');
        $memUsageVal = memory_get_usage(true);

        $logger->notice($memUsageText.' '.convert($memUsageVal).' ('.$memUsageVal.' '.$mf->l10n->getLabel('main','bytes').')');



        $html[]= $mf->header;
        $html[]= $mf->body;
        $html[]= $mf->footer;
        echo implode(chr(10),$html);
    }

    // $bodyParams : attributes to be added to the page bodytag
    function getStandardHeader($bodyParams,$pageTitle=''){
        global $mf,$l10n,$config;
        $header = file_get_contents(DOC_ROOT.SUB_DIR.$this->template.'/header.html');
        $header = str_replace("{charset}",$config['charset'],$header);
        $header = str_replace("{subdir}",SUB_DIR,$header);

        if($pageTitle != ''){
            $header = str_replace("{page-title}",$pageTitle,$header);
        }
        else if(isset($mf->info['pageTitle'])){

            $header = str_replace("{page-title}",strtoupper($mf->getLocale())." : ".$mf->info['pageTitle'],$header);
        }
        else $header = str_replace("{page-title}",strtoupper($mf->getLocale())." : ".$l10n->getLabel('backend','backend_title'),$header);

        $header = str_replace("{body-params}",$bodyParams,$header);

        //adding custom CSS
        $additionnalCss = '';
        $cssList = $mf->getCssList();
        foreach($cssList as $css){
            $additionnalCss .= $css.chr(10);
        }
        $header = str_replace("{additional-css}",$additionnalCss,$header);

        //adding custom JS for the top of the page
        $additionalJS = '';
        $topJsList = $mf->getTopJsList();
        foreach($topJsList as $js){
            $additionalJS .= $js.chr(10);;
        }
        $header = str_replace("{additional-js-top}",$additionalJS,$header);

        if(isset($config['backend_favicon']) && $config['backend_favicon'] != '')$favicon = $config['backend_favicon'];
        else $favicon = '/mf/backend/templates/mf-default/ico/favicon.ico';

        $header = str_replace("{backend_favicon}",$favicon,$header);

        return $header;
    }

    function getStandardFooter(){
        global $mf;
        $footer = file_get_contents(DOC_ROOT.SUB_DIR.$this->template.'/footer.html');
        $footer = str_replace("{subdir}",SUB_DIR,$footer);

        //adding custom JS for the bottom of the page
        $additionalJS = '';
        $bottomJsList = $mf->getBottomJsList();
        foreach($bottomJsList as $js){
            $additionalJS .= $js.chr(10);
        }
        $footer = str_replace("{additional-js-bottom}",$additionalJS,$footer);

        return $footer;
    }

    //clean up all ressources and free memory after MindFlow execution
    function freeRessources(){

        global $db;
        $db->closeDB();
    }

    function getCurrentModule(){

        global $mf,$config;

        //use first module as the default module
        $currentModule = $config['default_plugin'];

        //loading system modules (all modules are plugins)
        if(isset($mf->pluginManager->modulesByMode['backend'])){
            $availableModules = $mf->pluginManager->modulesByMode['backend'];

            //validate the requested module is available in our modules list
            foreach($availableModules as $name => $module){
                if(isset($_REQUEST['module']) && $_REQUEST['module'] == $name) {
                    $currentModule = $name;
                }
            }

            return $currentModule;
        }
        else return null;

    }

    function getLanguagesForm(){
        global $mf,$l10n,$config;
        $html = array();

        //show the language selector if there are more than 1 language available
        if(sizeof($config['frontend_locales']) > 1){
            $html [] = '<form class="pull-right" id="languageSelectForm" action="">
            <label for="languageSelect"  id="languageSelectLabel">'.$l10n->getLabel('backend','current_language').' : </label>
             <select id="languageSelect" onchange="switchLanguage();">';
            foreach($config['frontend_locales'] as $key => $value){
                $html [] = '    <option value="'.$key.'"'.(($key == $mf->getDataEditingLanguage())?' selected':'').'>'.$value.'</option>';
            }

            $html [] = '
             </select>
            </form>';

            $secKeys = array(
                'action' => "switchLanguage",
                'mode' => $mf->mode,
            );
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $html [] = '<script>
            function switchLanguage(){
                var BE_language = $("#languageSelect").val();

                var jqxhr = $.post(SUB_DIR + "/mf/core/ajax-core-json.php", { action:"switchLanguage", language: BE_language, sec:"'.$secHash.'", fields:"'.$secFields.'",mode: "'.$mf->mode.'"})
                    .success(function(jsonData) {
                        window.location.reload();
                    })
                    .fail( function(xhr, textStatus, errorThrown) {
                        alert("backend.php switchLanguage() : " + xhr.responseText);
                    })
            }
            </script>';
        }

        return implode(chr(10),$html);
    }


}

