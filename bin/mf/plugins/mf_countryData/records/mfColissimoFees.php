<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

class mfColissimoFees extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_colissimo';

    static $createTableSQL= "
        `area_name` varchar(4) NOT NULL,
        `min_weight` DECIMAL(5,2) DEFAULT 0,
        `max_weight` DECIMAL(5,2) DEFAULT 0,
        `fee` DECIMAL(6,2) DEFAULT 0,
    ";

    static $createTableKeysSQL="
        KEY `fk_mf_colissimo_areaname` (`area_name`),
    ";

    function init(){

        global $l10n;

        parent::init();


        $this->data = array_merge($this->data, array(

            'area_name'   =>  array(
                'value' => '',
                'dataType' => 'select',
                'valueType' => 'text',
                'possibleValues' => array(
                    0 => '',
                    'FR' => $l10n->getLabel('mfColissimoFees','name_FR'),
                    'OM1' => $l10n->getLabel('mfColissimoFees','name_OM1'),
                    'OM2' => $l10n->getLabel('mfColissimoFees','name_OM2'),
                    'INTA' => $l10n->getLabel('mfColissimoFees','name_INTA'),
                    'INTB' => $l10n->getLabel('mfColissimoFees','name_INTB'),
                    'INTC' => $l10n->getLabel('mfColissimoFees','name_INTC'),
                    'INTD' => $l10n->getLabel('mfColissimoFees','name_INTD'),
                    'SEC' => $l10n->getLabel('mfColissimoFees','name_SEC'),
                ),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('',0)
                ),
            ),
            'min_weight'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'max_weight'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'fee'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),

        ));


        $this->showInEditForm = array(
            'tab_main'=> array('area_name','min_weight','max_weight','fee')
        );



    }

    static $enableImportInitializationData = 1;

    static function importInitializationData(&$html=array()){

        dbRecord::importSQLDump(dirname(__FILE__).'/mf_colissimo.sql',1,$html);

    }

    /*
     * Returns the shipping fees for an e-Colissimo parcel
     * @param $countrycode string ISO-3166-alpha2 format (example : 'FR')
     * @param $weight float the weight of the parcel in kilograms (example : 0.65)
     * @return float cost in euros or false if not found.
     */
    static function getColissimoFee($countryCode,$weight){
        global $pdo;

        //Get the Colissimo area first
        $sql = "SELECT colissimo_area FROM mf_countries WHERE cn_iso_2 = :country_code AND deleted=0";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array(":country_code" => $countryCode));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        if($stmt->rowCount()>0){

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $colissimoArea = $row['colissimo_area'];

            //Get the Colissimo fee
            $sql = "SELECT fee FROM ".self::$tableName." WHERE area_name = ".$colissimoArea." AND min_weight < :weight AND max_weight > :weight AND deleted=0";
            $stmt = $pdo->prepare($sql);

            try{
                $stmt->execute(array(":weight" => $weight));
            }
            catch(PDOException $e){
                echo $e->getMessage();
                return 0;
            }
            if($stmt->rowCount()>0){
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row['fee'];
            }
            else return false;

        }
        else return false;

    }



}


