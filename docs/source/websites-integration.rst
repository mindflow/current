﻿.. |vspace| raw:: latex

   \vspace{5mm}

.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>

|embed_js|

*************************************************
Les sites web
*************************************************



Définition d'un site web
-------------------------

**Un site web dans MindFlow, c'est :**



* **Une série de gabarits HTML** déposée dans le dossier ``mf_websites/www.sample-website.com/templates/``

|vspace|

* **Une structure de données définie pour chaque gabarit** dans le fichier ``mf_websites/www.sample-website.com/templates.php``

|vspace|

* **Une arborescence de pages** définies et éditées dans le backend

|vspace|

* Très probablement, **des fonctions PHP** définies dans le fichier  ``mf_websites/www.sample-website.com/functions.php``. Par exemple une fonction pour générer le menu de navigation sur toutes les pages.

|vspace|

* Eventuellement, **une série de micro-gabarits** (microtemplates) servant à afficher des blocs HTML répétés au sein d'une même page (pour l'affichage d'une série d'actualités par exemple).

|vspace|

* Eventuellement, **un ou plusieurs fichiers de langue** pour traduire les labels du site (navigation, footer...) si le site est multilingue

|vspace|

* Eventuellement, **des plugins** pour gérer des enregistrements personnalisés en backend et générer du contenu dynamique dans les pages.

|
|

De préférence, chaque site web disposera de son propre sous-dossier dans le dossier ``mf_websites/``.

Dans ce sous dossier, on pourra trouver 1 ou plusieurs fichiers de configuration pour le site qui seront appelés depuis le fichier ``mf_config/config.php`` en fonction du nom de domaine d'appel.

Reportez-vous à la section :ref:`install` pour voir comment préparer le fichier de configuration de base et spécifier le nom de domaine de votre site web.

.. important::

	A ce stade, assurez-vous que les plugins **URLRewriter** et **Pages** sont bien chargés dans le fichier de configuration de votre site. Normalement ils sont définis par défaut.

|
|
|
|

Définition des gabarits HTML et des marqueurs
---------------------------------------------

Vos gabarits HTML seront faits de code HTML standard, hormis le fait que vous allez y substituer des marqueurs dont le nom sera défini entre crochets comme suit :

::

	{nom_du_marqueur}
		
Vous pouvez en insérer un peu partout dans vos gabarits. Définissons pour l'exemple un gabarit nommé ``default.html`` minimaliste, basé sur l'exemple "stickyFooter" de Bootstrap 3.0 :

.. code-block:: html

	<!DOCTYPE html>
	<html lang="{lang}">
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<title>{page_title}</title>

			<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
			<link href="{template_path}/css/sticky-footer.css" rel="stylesheet">
		</head>

		<body>

			<div class="container">
				<div class="page-header">
					<h1>{page_title}</h1>
				</div>
				<p class="lead">{page_html}</p>
			</div>

			{footer}

		</body>
	</html>
	
Nous avons donc glissé dans ce gabarit quelques marqueurs :

* ``{lang}`` le code ISO2 de la langue du site
* ``{template_path}`` le chemin vers notre dossier de gabarits
* ``{page_title}`` le titre de la page en cours
* ``{page_html}`` le texte de la page en cours
* ``{footer}`` le pied de page, commun à toutes les pages du site

Comme expliqué dans la note ci-dessous, les marqueurs ``{page_title}`` et ``{template_path}`` seront remplacés automatiquement par le plugin **Pages**. Pour les autres, nous allons poser les définitions ci-après dans la section du **fichier templates.php**

|
|

.. note::

	**En règle générale il vous appartient de définir le contenu qui doit être substitué au marqueur dans le fichier templates.php. Mais il existe un certain nombre de marqueurs prédéfinis dans le plugin Pages dont le contenu sera substitué automatiquement. Les voici :**
	
	* ``{page_title}`` le titre de la page courante

	|vspace|
	
	* ``{template_path}`` le chemin vers notre dossier de gabarits

	|vspace|

	* ``{image_path}`` le dossier ou ont été uploadées  depuis le backend les images de la page courante

	|vspace|

	* ``{meta_description}`` renvoie un tag meta description complet de la page en cours

	|vspace|

	* ``{page_description}`` idem que pour {meta_description}, mais sans insérer le tag **<meta>** (renvoie uniquement le contenu de l'attribut **content**). Ceci permet d'utiliser la description saisie pour la page dans d'autres types de tags, par exemple des définitions open graph.
	
	|vspace|

	* ``{meta_keywords}`` renvoie un tag meta keywords complet de la page en cours

	|vspace|

	* ``{page_keywords}`` idem que pour {meta_keywords}, mais sans insérer le tag **<meta>** (renvoie uniquement le contenu de l'attribut **content**). Ceci permet d'utiliser les mots clefs saisis pour la page dans d'autres types de tags, par exemple des définitions open graph.

	|vspace|

	* ``{meta_robots}`` renvoie un tag meta robots complet de la page en cours

	|vspace|

	* ``{page_robots}`` idem que pour {meta_robots}, mais sans insérer le tag **<meta>** (renvoie uniquement le contenu de l'attribut **content**). 

	|vspace|

	* ``{mindflow-messages}`` affiche les messages d'avertissement renvoyés par MindFlow dans le gabarit de la page.

	**Pages substitue également des tags avec des arguments dynamiques :**
	
	* ``{link pageUid class id}`` crée un lien vers la page ayant pour uid la valeur spécifiée au niveau de l'argument **pageUid**, en lui appliquant la classe définie dans l'argument **class** et l'id défini dans l'argument **id**. Exemple d'usage : ``{link 81 maClasse1,maClasse2 monId}``
	
	|vspace|
	
	* ``{url pageUid}`` renvoie l'URL la page ayant pour uid la valeur spécifiée au niveau de l'argument **pageUid**, sans générer de tag <a> autour, contrairement au marqueur {link}. Exemple d'usage : ``{url 81}``

	|vspace|
	
	* ``{label domain label}`` rapatrie un label localisé dans la langue courante affichée par l'internaute. 
	
	Un fichier de langue devra avoir été chargé préalablement en insérant un appel en tête du fichier templates.php tel que : 
	
	.. code-block:: php
	
		$mf->l10n->loadL10nFile(
			'nomDomaine', 
			'/mf_websites/www.sample-website.com/templates/languages/monFichier_l10n.php'
		);
	
	On pourra ensuite appeler une clef définie dans le fichier ainsi : ``{label nomDomaine nomClef}`` 
		
|
|
|
|
	
Le fichier templates.php	
^^^^^^^^^^^^^^^^^^^^^^^^

Dans le fichier templates.php, nous allons définir les structures de données associées pour chaque gabarit que nous utiliserons pour notre site. 

.. important::

	Pour que le fichier templates.php soit appellé automatiquement par MindFlow, il faut définir correctement la variable ``$config['templates_directory']`` en la pointant vers le dossier contenant templates.php
	
Nous allons maintenant ajouter la structure de données qui définit notre gabarit (template) à MindFlow. Une fois encore, cette structure de données est définie sous la forme d'un tableau PHP que l'on ajoute via la fonction ``$mf->addTemplates()`` :

.. code-block:: php

	$mf->addTemplates(array(

		// Un gabarit avec le nom 'default' est obligatoire
		// Les nouvelles pages utilisent ce gabarit par défaut
		'default' => array(
			'template_name' => "Default",
			'template_file' => 'default.html',
			'template_data' => array(
			
				// Execute une fonction PHP qui affiche le code ISO2 de la langue du site
				'lang'  => array(
					'dataType' => 'function',
					'value' => '',
					'processor' => 'getLang',
				),
				
				//Editeur de texte enrichi pour saisir le corps de texte de la page courante.
				'page_html' => array(
					'value' => '',
					'dataType' => 'rich_text',
					'valueType' => 'text',
					'div_attributes' => array('class' => 'col-lg-8'),
					'index' => 'digest'
				),
				
				// Includes a PHP file (avoid if possible, requires output buffering which may lead to unpredictable results when accessing MindFlow variables, prefer running a function)
				'footer' => array(
					'dataType' => 'include',
					'value' => 'footer.php',
					'processor' => '',
				),


			),
			// List of fields which must show up when you edit the page in backend context. They show up in the specified order.
			'showInEditForm' =>'page_title,page_html',
		),
	));	
	
Comme vous pouvez le constater, sous les clefs ``['default']['template_data']``, on retrouve les même types de définitions de champs que celles employées pour réaliser des formulaires ou des enregistrements de base de données dans les plugins.
Par conséquent, toutes les spécifications de ces champs telles que documentées dans la section :ref:`keys` s'appliquent ici. 

.. note ::
	
	Le plugin Pages prend en charge 2 nouveaux ``'dataTypes'`` :
	
	* ``'include'`` permet d'inclure le fichier HTML ou PHP spécifié dans le champ ``'value'`` associé.
	
	Par exemple :
	
	.. code-block:: php
	
		'footer' => array(
			'dataType' => 'include',
			'value' => 'footer.php',
			'processor' => '',
		),	
	
	|
	|
	
	* ``'function'`` provoque l'appel d'une fonction PHP. Le nom de celle-ci est défini dans une clef nommée ``'processor'``, mais sans parenthèses ni arguments. En effet des arguments sont déjà automatiquement passés à la fonction par MindFlow (voir après).
	
	Par exemple :
	
	.. code-block:: php
	
		'lang'  => array(
			'dataType' => 'function',
			'value' => '',
			'processor' => 'getLang',
		),

	Appelera la fonction ``getLang()``. Celle-ci devra être définie dans le fichier templates.php ou dans un autre fichier passé en include. Une pratique courante est de placer un appel ``include 'functions.php';`` en tête du fichier ``templates.php``
	
	Notre fonction ``getLang()`` sera définie comme suit :
	
	.. code-block:: php
	
		function getLang($page, $key, $value){
			global $mf;
			return $mf->getFrontendLanguage(); //renvoie 'fr' si le site est en français
		}
	
	La valeur retournée par la fonction est celle qui sera substituée au {marqueur} du champ courant.
	
	Notez les arguments de la fonction :
	
		* ``page`` passe l'objet page courant. Ceci vous permet, dans votre fonction, d'accéder aux valeurs de tous les champs définis dans l'objet page, pour par exemple en concaténer 2 et les renvoyer à l'affichage.
	
		* ``key`` donne le nom de la clef courante. Par exemple, vous pouvez vouloir appeller une même fonction pour différentes clefs, mais renvoyer un résultat différent en fonction des clefs. 
	
		* ``value`` donne la valeur du champ courant si elle est défine. **Note : il y a un bug, cette valeur ne remonte pas, mais jamais eu besoin de l'utiliser en pratique. A corriger.**
	

Hormis les champs qui ne requièrent pas d'affichage en backend (datatype function, include, etc.), **les champs définis ici apparaitront dans le formulaire d'édition de notre page**. Dans l'exemple précédent, ceci ne concerne que le champ ``'page_html'``. Notez qu'on ne définit pas ``'page_title'`` car il est déjà prédéfini dans la classe ``page``.

.. image:: images/page-default.jpg

.. note::

	Sous le champ "Titre de la page" dans la capture d'écran ci-dessus, vous voyez un champ "Titre de navigation". C'est également un champ prédéfini par le plugin **Pages**. Sa clef est ``'nav_title'``. 
	
	Celui-ci sert à définir un titre court à faire figurer dans un menu de navigation du site. En effet, pour des questions de référencement, on truffe parfois les titres de pages de mots-clés qui, s'ils s'avèrent pertinents vis à vis des moteurs de recherche et des internautes, sont généralement un peu longs et sont moins pertinents dans l'affichage d'un menu ou d'une navigation.
	
	Pour consulter la liste des champs prédéfinis dans l'objet ``page``, reportez-vous à sa classe PHP dans ``/mf/plugins/pages/records/page.php``
	
	Il existe également d'autres champs prédéfinis dans la classe parente ``dbRecord``, que vous retrouverez dans la classe PHP ``/mf/core/records/dbRecord.php``, comme par exemple la **date de dernière modification** ('modification_date') que vous pourriez vouloir afficher sur votre page.

|
|

.. important::

	**Ainsi donc, la création d'une clef dans tableau template définit à la fois un {marqueur} que vous allez pouvoir substituer dans votre page, et un champ dans le formulaire d'édition de votre page en backend. MindFlow substituera automatiquement dans votre gabarit HTML le marqueur avec la valeur saisie dans le champ de l'objet page en backend.**
	
	**L'intégration d'un site sous MindFlow consiste donc essentiellement à définir consciencieusement les champs de vos gabarits/templates.**

|
|
	
Continuons l'examen de notre bloc de code :

.. code-block:: php

	$mf->addTemplates(array(

		'default' => array(
			'template_name' => "Default",
			'template_file' => 'default.html',
			'template_data' => array(
					//...
					),
			'showInEditForm' =>'page_title,page_html',
		),
		
	));	
	
* L'entrée ``'default'`` est la clef de notre gabarit. **Celle-ci doit être unique.** En effet, si vous ajoutez d'autres gabarits et qu'ils ont la même clef, ils écraseront alors les valeurs du premier gabarit portant la clef, ce qui n'est nomalement pas le résultat souhaité.

|vspace|

* ``'template_name'`` est le nom du gabarit tel qu'il apparaitra dans la liste de sélection du gabarit pour choisir l'apparence de votre page en backend.

|vspace|

* ``'template_file'`` pointe vers le fichier HTML utilisé pour le gabarit, ici ``'default.html'``. C'est ce gabarit qui sera affiché par MindFlow en frontend et dans lequel seront substitués les {marqueurs}.

|vspace|

* ``'template_data'`` correspond à la définition de la liste des champs exploités dans le gabarit

|vspace|

* ``'showInEditForm'`` est la liste des champs qui s'afficheront lorsque vous éditerez la page en backend. Ils s'affichent dans l'ordre spécifié.

**Notez que vous pouvez définir plusieurs gabarits à la suite, dans le même appel addTemplates()**

.. code-block:: php

	$mf->addTemplates(array(

		'default' => array(
			
			// définition du gabarit 'default'
		
		),
		
		'homepage' => array(
		
			// définition du gabarit 'homepage'
			
		)
		
		// etc.
		
	));	
		
|
|
|
|

	
Le fichier functions.php
^^^^^^^^^^^^^^^^^^^^^^^^

Bien que ce ne soit pas un fichier appelé par MindFlow, nous aimons bien déporter les définitions de nos fonctions PHP appellées depuis nos gabarits dans un fichier séparé de templates.php.

Nous créons donc un fichier ``functions.php`` au même niveau que ``templates.php`` puis ajoutons en tête du fichier templates.php un simple :

.. code-block:: php

	include_once 'functions.php';
	
|
|
|
|

Localisation du gabarit
^^^^^^^^^^^^^^^^^^^^^^^

Comme dans tout formulaire MindFlow, il faut associer un fichier de langue à notre gabarit. Ceci permet de traduire les noms des champs lorsque le backend est affiché dans une langue différente.

Nous créons donc un fichier ``templates/languages/templates_l10n.php`` défini comme suit :

.. code-block:: php

		<?php

		$l10nText = array(

			'fr' => array(
				'page_html' => 'Corps de texte de la page',
			),

			'en' => array(
				'page_html' => 'Page bodytext',
			),

		);
		
Pour le gabarit 'default', nous n'avons besoin de définir que le label page_html car c'est le seul champ qui apparait dans l'interface et qui n'est pas déjà prédéfini dans dbRecord.
Si vous avez défini d'autres gabarits dans le fichier templates.php, vous ajouterez les traductions de champs de ces gabarits dans ce fichier ``templates/languages/templates_l10n.php``, au cotés de la clef ``'page_html'``.

Pour pouvoir utiliser ce fichier de langue, il nous faut encore le charger dans MindFlow. Pour ce faire on ajoutera en tête du fichier ``templates/templates.php`` la commande suivante :

.. code-block:: php

		l10nManager::loadL10nFile('templates',$config['templates_directory'].'/languages/templates_l10n.php');

Notez bien que le fichier de langue est ajouté au domaine de localisation nommé 'templates'. C'est à partir de ce nom que MindFlow extrait toutes les traductions de champs appartenant à des gabarits / templates.

.. note::

	Si vous réalisez un second appel de ``l10nManager::loadL10nFile('templates','votre_fichier_l10n.php');`` un peu plus loin, les clefs définies dans le nouveau fichier seront ajoutées au domaine de localisation ``'templates'``. Elles ne remplaceront donc pas les clefs existantes comme on pourrait le craindre.

	
|
|
|
|

Création d'une page et association d'un gabarit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Une fois notre gabarit défini dans le fichier templates.php, nous allons l'associer à une page. A cette fin, nous cliquons sur le bouton **"Ajouter une page"** :

.. image:: images/templates-1.jpg

|
|

La page apparait alors en tête de la liste des pages déjà présentes. 

.. image:: images/templates-2.jpg

|
|

On pourra **la positionner dans le site en faisant glisser la barre grise** symbolisant la page. Ceci se réalise **en effectuant un drag & drop à partir de l'icône en forme de double-flèches** :

.. image:: images/templates-2-2.jpg

|
|

On édite ensuite la page en cliquant sur **l'icône en forme de crayon** :

.. image:: images/templates-2-3.jpg		

|
|

Arrivé sur le formulaire d'édition, on pourra évidemment saisir un titre de page dans l'onglet **"Contenu de la page"**, par exemple nous l'intitulerons 'ma-page', puis on basculera sur l'onglet **"Propriétés de la page"**. On voit alors apparaître le champ **"Gabarit de la page"** qui liste les différents gabarits disponibles. On sélectionne le gabarit 'Default', puis il nous reste à cliquer sur **le bouton "Prévisualiser"** qui va à la fois sauver la page et nous l'afficher dans un open window :

.. image:: images/templates-3.jpg	

|
|
	
.. note::

	Si l'open window de prévisualisation de la page n'apparait pas, c'est généralement parce qu'il est bloqué par l'anti-popup de votre navigateur web. Dans ce cas veillez à autoriser le site définitivement pour ne plus rencontrer de problème.
	
.. important::

	S'il s'agit de la première page que vous créez sur le site, vous pourriez rencontrer le message d'erreur ci-dessous :

	**Aucune page racine n'a été trouvée dans la base de données. Créez d'abord une page et cochez lui la propriété "Racine du site".**
		
	Dans ce cas, assurez-vous de bien cocher la casse "Racine du site" sur votre page de plus haut niveau dans l'onglet **"Propriétés de la page"** :
	
	.. image:: images/racine.jpg

|
|

Notre page apparait alors, sans autre contenu que le titre déjà saisi :

.. image:: images/templates-4.jpg

|
|

Retournons maintenant dans l'onglet **"Contenu de la page"** saisir du texte dans le champ de texte enrichi que l'on avait défini dans le champ 'page_html' pour le gabarit "default" : 

.. image:: images/templates-5.jpg

|
|

Prévisualisons : **notre texte apparaît en frontend.**

.. image:: images/templates-6.jpg
	
|
|
|
|
	
Les micro-gabarits
^^^^^^^^^^^^^^^^^^^

Si un éditeur de texte enrichi permet déjà de répondre à un grand nombre de problématiques de publication sur un site Internet, il reste limité au niveau des possibilités de mise en page, nottament lorsqu'il est nécessaire de publier des blocs dont les éléments comportent des classes CSS.

A cette fin, MindFlow propose les micro-gabarits (microtemplates), des blocs de HTML avec leur propre formulaire d'édition, qui pourront être insérés de multiples fois dans une page.

En backend, un micro-gabarit est un bloc d'édition, présentant, une fois saisis, de multiples sous-éléments éditables :

.. image:: images/microtemplates-1.jpg

|
|

Lorsqu'on clique sur le lien "Ajouter un élement", une liste des différents types de micro-gabarits admissibles dans ce champ est présentée sous forme de vignettes (une seule dans l'exemple ci-dessous) :

.. image:: images/microtemplates-2.jpg

|
|

L'utilisateur clique sur le type de micro-gabarit souhaité, et l'élément s'insère dans la page. Il peut ensuite accéder au formulaire d'édition du micro-gabarit en cliquant sur le l'icône en forme de crayon :

.. image:: images/microtemplates-3.jpg

|
|
|

**Maintenant que nous en avons exposé le fonctionnement, définissons notre micro-grabarit :**

Nous pouvons écrire cette définition dans le fichier templates.php, mais nous aimons bien cloisonner les choses et les specifier dans un fichier à coté.

Nous ajoutons donc en tête du fichier templates.php la déclaration :

.. code-block:: php

	include_once 'microtemplates/microtemplates.php';
	
Puis nous créons un fichier ``microtemplates.php`` dans un sous dossier ``microtemplates/``

.. code-block:: php

	<?php

	// Nous aimons bien séparer aussi la définition des fonctions associées au microtemplates
	include 'microtemplate_functions.php';

	//nous spécifions à MindFlow les micro-gabarits utilisés sur le site
	$mf->addMicrotemplates(
		array(

			'infos' => array(
				'microtemplate_name' => 'Infos',
				
				//important! spécifier la même clef que dans le champ parent défini dans le template qui appelle le microtemplate
				'microtemplate_key' => 'infos',
				
				//le fichier HTML contenant la mise en page de notre microtemplate
				'microtemplate_file' => 'infos.html',
				
				//la vignette (dimension 150x150 pixels) permettant à l'utilisateur de sélectionner le microtemplate
				'microtemplate_image' => 'infos.png',
				
				//le champ titre qui apparait dans la fenêtre de supression du micro-gabarit
				'microtemplate_title_field' => 'info-titre',
				
				//le champ utilisé comme titre qui apparait dans la liste des micro-grabarits saisis dans une page
				//il est possible d'indiquer un champ files / image : dans ce cas celle ci-sera réduite à une image de 30px de haut et affichée dans la barre
				'microtemplate_show_field_value' =>'info-titre',
				
				//définition des champs du micro-gabarit
				'microtemplate_data' => array(
				
					//on définit les champs comme dans un gabarit normal
					'info-photo' => array(
						'dataType' => 'files jpg,jpeg,gif,png 166 102',
						'value' => '',
						'processor' => '',
						'div_attributes' => array('class' => 'col-lg-8'),
						'processor' => 'photoPersonne',
					),
					'info-titre' => array(
						'dataType' => 'input',
						'valueType' => 'text',
						'value' => '',
						'field_attributes' => array('class' => 'input-lg'),
						'div_attributes' => array('class' => 'col-lg-8'),
						'index' => 'digest'
					),
					'info-description' => array(
						'dataType' => 'textarea',
						'value' => '',
						'div_attributes' => array('class' => 'col-lg-8'),
						'index' => 'digest'
					),
				),
				
				//liste des champs à afficher dans notre micro-gabarit
				'showInEditForm' =>'info-photo,info-titre,info-description,',
			),
			
			//on peut spécifier d'autres microtemplates à la suite
			'infos-detail' => array(
			
				// ...
				
			),
		)
	);
	
|
|
|

Notre micro-gabarit est appelé depuis un champ du gabarit de la page (ou de l'enregistrement de base de données, en effet il est aussi possible de placer des microtemplates dans les objets hérités de dbRecord. C'est par exemple ce que fait le plugin siteInfos pour enregistrer en base de données une série de champs dont la structure ne peut être connue à l'avance et pour lesquelles il ne saurait définir une structure de table).

Définissons donc le champ qui fait appel à notre microtemplate dans notre template :

.. code-block:: php
	
	'infos' => array(
		'dataType' => 'microtemplate',
		'allowedMicrotemplateTypes' => 'infos',
		'value' => array(),
		'processor' => '',
		'div_attributes' => array('class' => 'col-lg-8'),
		'index' => 'digest'
	),
	
|
|
|

Enfin il nous faut définir le HTML 'infos.html' de notre microgabarit :

.. code-block:: html

	<table class="infos">
	<tr>
		<td class="image">{info-photo}</td>
		<td>    
			<h3>{info-titre}</h3>
			{info-description}
			<br />
			<a class="more" data-toggle="collapse" data-target="#equi"><span></span>{label vz read_more}</a>
		</td>
	</tr>
	</table>

|
|
|

N'oubliez pas non-plus de créer l'image associée au micro-gabarit :

.. image:: images/infos.png
	
|
|
|
|

Sites multilingues
------------------

Pour gérer des sites multilingues avec MindFlow, vous avez 2 possibilités : 

* **Créer plusieurs sites différents,** 1 par langue, avec chacun son nom de domaine, sa base de données et son fichier de configuration propres. Dans ce cas Il y aura aussi un backend par site. En somme cette solution consiste plus en du multisite que du multilingue. Toutefois comme les chemins des dossiers sont configurables, les 3 sites pourront pointer vers les mêmes dossier ``templates/`` et ``plugins/``, ce qui leur permettra de disposer d'une base de code commune.

|vspace|

* **Créer un site en plusieurs langues,** administrable à partir d'un même backend. Dans ce cas, le contenu de chaque site sera accessible en frontend via un sous-dossier correspondant au code ISO2 de la langue, par exemple :

::

	http://www.sample-website.com/fr/
	
	http://www.sample-website.com/en/
	
	http://www.sample-website.com/it/
	
**La page d'accueil chargée par défaut** quand on accèdera à ``http://www.sample-website.com`` sans spécifier de sous-dossier **sera celle de la langue par défaut**.

Les langues sont définies dans le fichier de configuration de votre site, dans les variables suivantes

.. code-block:: php

	$config = array(
	
		// ...
	
		//on crée une entrée pour chaque langue du site en frontend
		'frontend_locales' => array(
			'fr' => 'Français',
			'en' => 'Anglais',
			'it' => 'Italien',
		),
		
		// les backend locales servent uniquement si on veut traduire 
		// le backend en plusieurs langues, c'est à dire si l'on a des 
		// webmasters dans plusieurs pays différents et ne parlant pas 
		// nécessairement la langue de la maison mère
		// Dans ce cas ils peuvent choisir la langue d'affichage de leur 
		// backend dans les propriétés de leur profil utilisateur backend
		'backend_locales' => array(
			'fr' => 'Français',
			'en' => 'English',
		),
		
		//langue par défaut du site
		'default_frontend_locale' => 'fr',
		
		//langue par défaut du backend
		'default_backend_locale' => 'fr',
		
		// ...
	
	)
	
A partir du moment ou l'on ajoute plus d'une langue dans la variable ``$config['frontend_locales']``, une boite de sélection de la langue du site courament éditée apparait dans le backend :

.. image:: images/language-1.jpg

Dès lors, chaque fois que vous créerez un nouvel enregistrement dans le backend, qu'il s'agisse d'une page, d'un utilisateur ou d'une actualité, celui-ci aura le code ISO2 de la langue courante saisi dans le champ `language` de son enregistrement.

.. note ::

	Le champ `language` est un champ hérité de dbRecord. Il est donc défini nativement dans tous les enregistrements / classes hérités de dbRecord.

Voici par exemple un extrait d'une vue d'une table mf_news dans phpmyadmin, on y voit les enregistrement créés en langue française :

.. image:: images/language-2.jpg	

.. note ::

	Si le champ `language` d'un enregistrement a la valeur ``'ALL'``, alors celui-ci sera affiché dans toutes les langues d'édition du backend.
	
.. note::

	Dans un plugin, pour filtrer et afficher les enregistrements en langue italienne, il suffira d'ajouter la condition ``AND language='it'`` à la section WHERE de votre requête.
	
|
|

MindFlow définit également un champ ``alt_language`` qui stocke, sous la forme d'un tableau, les UIDs des pages correspondant à la page actuelle, mais dans les autres langues du site. Le but de ce champ est essentiellement de permettre de sélectionner, pour une page donnée, les version alternatives définies dans les autres langues et de pouvoir afficher sur le site des liens vers la traduction de la page, sous forme de petits drapeaux cliquables par exemple.

Lorsque plusieurs langues sont actives sur le site, le champ "Langues alternatives de la page" apparait dans l'onglet "Propriétés de la page" :

.. image:: images/language-3.jpg

Voici un exemple de récupération des langues alternatives d'une page en générant des liens sous forme d'images de drapeaux, que l'on pourra définir au sein d'une fonction PHP mappée sur un champ de gabarit :

.. code-block:: php

	function getFlags(){
	
		global $mf;
		
		//récupération de la langue actuellement affichée en frontend
		$locale = $mf->getLocale();

		$page = new page();

		$currentPageUid = $mf->info['currentPageUid'];
		
		//on récupère les langues alternatives de la page
		if($locale=='fr'){

			$AltLangURL_EN = $mf->getPageURL($page->data['alt_language']['value']['en']);
			$AltLangURL_IT = $mf->getPageURL($page->data['alt_language']['value']['it']);
			$AltLangURL_FR = $mf->getPageURL($currentPageUid);
		}
		else if($locale=='en') {
			
			$AltLangURL_FR = $mf->getPageURL($page->data['alt_language']['value']['fr']);
			$AltLangURL_IT = $mf->getPageURL($page->data['alt_language']['value']['it']);
			$AltLangURL_EN = $mf->getPageURL($currentPageUid);
		}
		else if($locale=='it') {
			
			$AltLangURL_FR = $mf->getPageURL($page->data['alt_language']['value']['fr']);
			$AltLangURL_EN = $mf->getPageURL($page->data['alt_language']['value']['en']);
			$AltLangURL_IT = $mf->getPageURL($currentPageUid);
		}
		
		//génération du code HTML avec les liens de nos drapeaux
		$flagFR = '<a href="'.$AltLangURL_FR.'" class="imgRollover'.(($locale=='fr')?' active':'').'" title="Français" ><img src="{template_path}/images/fr_off.png" width="18" height="13" id="french" alt="Français" /></a>';
		
		$flagEN = '<a href="'.$AltLangURL_EN.'" class="imgRollover'.(($locale=='en')?' active':'').'" title="English" ><img src="{template_path}/images/gb_off.png" width="18" height="13" id="english" alt="English"/></a>';
		
		$flagIT = '<a href="'.$AltLangURL_IT.'" class="imgRollover'.(($locale=='it')?' active':'').'" title="Italiano" ><img src="{template_path}/images/it_off.png" width="18" height="13" id="italian" alt="Italiano"/></a>';
		

        return '<div id="flags">'.$flagEN.$flagFR.$flagIT.'</div>';
	}
	
|