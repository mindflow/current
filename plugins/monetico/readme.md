
This plugin manages the communication with the online payment sytem CMCIC / Monetico using the v3.0 API
It does generates the form sending the payment to Monetico, but does not generate your order form.
It also processes the feedback from the bank.
The data for these operations can be consulted using the moneticoPayment and moneticoBankfeedback objects in backend.