<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


/**
 * Serves ajax requests returning data encoded as in the JSON format.
 *  See file ajax-core-html.php for serving pure HTML answers.
 */

$execution_start = time();

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/frontend/frontend.php';

//we are serving json
header('Content-type: application/json; charset=utf-8');

if(isset($_REQUEST['mode']) && $_REQUEST['mode']=='backend'){
    //create a backend for being able to use all the mindflow objects
    $xEnd = new backend();
}
else{
    //create a backend for being able to use all the mindflow objects
    $xEnd = new frontend();
}

global $mf,$l10n,$config,$pdo,$formsManager;

$xEnd->prepareData();


if($mf->mode == "authentification" || ($mf->mode == "backend" && $mf->currentUser == null)){

    echo '{"jsonrpc" : "2.0", "result" : "error", "message" : "'.$l10n->getLabel('backend','session_expired').'" }';
}

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
else if(checkSec()){



    if(isset($_REQUEST['action'])){

        //cloning an existing record
        if($_REQUEST['action'] == 'clone'){

            $uid = intval($_REQUEST['record_uid']);

            $record = new $_REQUEST['record_class']();
            eval('$tableName = '.$_REQUEST['record_class'].'::$tableName;');

            //now load our record with correct sorting specified
            $record->load($uid);

            $newRecord = clone $record;

            echo '{"jsonrpc" : "2.0", "result" : "clone done", "record_uid" : "'.$record->data['uid']['value'].'", "clone_uid" : "'.$newRecord->data['uid']['value'].'"}';

        }

        if($_REQUEST['action'] == 'close_record'){

            $recordUid = intval($_REQUEST['record_uid']);
            $recordClass = new $_REQUEST['record_class']();

	        $sql         = "SELECT edit_lock, editor_uid, editor_class FROM " . $recordClass::getTableName() . " WHERE uid=" . $pdo->quote($recordUid);
	        $stmt        = $pdo->query( $sql );
	        $row = $stmt->fetch(PDO::FETCH_ASSOC);

//	        echo "mode=".$mf->mode.chr(10);
//	        echo "isset(\$mf->currentUser)=".isset($mf->currentUser).chr(10);
//	        echo "\$row['editor_uid']=".$row['editor_uid'].chr(10);
//	        echo "\$mf->currentUser->data['uid']['value']=".$mf->currentUser->data['uid']['value'].chr(10);
//	        echo "\$row['editor_class']=".$row['editor_class'].chr(10);
//	        echo "\get_class($mf->currentUser)=".get_class($mf->currentUser).chr(10);

	        //unlock on close only if the current user is the user who had locked the record
	        //otherwise the user will have to click the unlock button inside the warning message
	        if(isset($mf->currentUser)) {
	        	if($row['edit_lock']=='1'){
			        if($row['editor_uid'] == $mf->currentUser->data['uid']['value']) {
				        if ( $row['editor_class'] == get_class($mf->currentUser) ) {
					        $sql         = "UPDATE " . $recordClass::getTableName() . " SET edit_lock=0, editor_uid=0, editor_class=''  WHERE uid=" . $pdo->quote($recordUid);
					        $stmt        = $pdo->query( $sql );
				        }
			        }
		        }
	        }

            echo '{"jsonrpc" : "2.0", "result" : "success"}';

        }

	    if($_REQUEST['action'] == 'unlock_record'){

		    $recordUid = intval($_REQUEST['record_uid']);
		    $recordClass = new $_REQUEST['record_class']();

		    if ( isset( $mf->currentUser ) ) {
			    //set the creator user
			    $editor_uid   = $mf->currentUser->data['uid']['value'];
			    $editor_class = get_class( $mf->currentUser );
		    } else if ( $mf->mode == 'frontend' ) {

			    $editor_uid   = 0;
			    $editor_class = 'unidentified (frontend)';
		    } else {

			    $editor_uid   = 0;
			    $editor_class = 'unidentified (backend)';
		    }

		    //reassign record editing to currently editing user
		    $sql         = "UPDATE " . $recordClass::getTableName() . " SET edit_lock=1, editor_uid=" . $editor_uid . ", editor_class=" . $pdo->quote( $editor_class ) . " , edit_start_time=NOW() WHERE uid=" . $pdo->quote($recordUid);
		    $stmt        = $pdo->query( $sql );

		    echo '{"jsonrpc" : "2.0", "result" : "success"}';

	    }

        //copy selected options from a multiple select (formsManager->makeRecordSelectField())
        else if($_REQUEST['action'] == 'copyRecordsFromSelect'){

            $_SESSION['clipboard']=array(
                'copied_class' => $_REQUEST['option_class'],
                'copied_uids' => $_REQUEST['option_uids'],
            );

            echo '{"jsonrpc" : "2.0", "result" : "success" }';
        }

        //copy selected options from a recordEditTable
        else if($_REQUEST['action'] == 'copyRecordsFromRET'){

	        $_SESSION['clipboard']=array(
                'copied_class' => $_REQUEST['record_class'],
		        'copied_uids' => $_REQUEST['copy_uids'],
		        'recordEditTableID' => $_REQUEST['recordEditTableID'],
	        );
	        $copiedUids = explode(',',$_SESSION['clipboard']['copied_uids']);
	        if(isset($copiedUids[0]) && $copiedUids[0]=='')unset($copiedUids[0]);

	        echo '{"jsonrpc" : "2.0", "result" : "success", "nbElements" : "'.count($copiedUids).'" }';
        }

        //paste selected options from a multiple select (formsManager->makeRecordSelectField())
        else if($_REQUEST['action'] == 'pasteRecordsToSelect'){

	        $recordUid = $_REQUEST['record_uid'];
	        $recordKey = $_REQUEST['record_key'];
	        $recordClass = $_REQUEST['record_class'];
	        $targetOptionsClass = $_REQUEST['options_class'];
	        $formID = $_REQUEST['form_id'];

	        //retreive clipboard data from session
	        $optionClass = $_SESSION['clipboard']['copied_class'];
	        $optionUids = $_SESSION['clipboard']['copied_uids'];

	        $dataView = array();
	        $dataEdit = array();
	        $dataDelete = array();
	        $optionTexts = array();

	        $record = new $recordClass;
	        $record->load($recordUid);


	        //the target select may display different titles than the source select. We need to figure out which columns to show in the target.
	        if(is_array($record->data[$recordKey]['addItemsSQL']))$selectMode = 'addItemsSQL';
	        else $selectMode = 'initItemsSQL';
	        $titles = explode(',',$record->data[$recordKey][$selectMode]['SELECT']);
	        $fieldData = $record->data[$recordKey];

	        if($targetOptionsClass == $optionClass){

		        $options = explode(',',$optionUids);
		        $newOptionsUids = array();

		        $dummyIndex = 0;
		        foreach($options as $optionUid){
			        eval('$option'.$optionUid.' = new '.$optionClass.';');
			        eval('$option'.$optionUid.'->load('.$optionUid.');');
			        eval('$newOption = clone $option'.$optionUid.';');
			        $newOption->data['parent_uid']['value'] = $recordUid ;
			        $newOption->data['deleted']['value'] = 0;
			        $newOption->data['language']['value'] = $mf->getLocale();
			        eval('$newOptionsUids[] = $newOptionUid = $newOption->store();');

			        //extract title data and compose the titles for the new options
			        $fullTitle = '';
			        $columnSeparator = ((isset($fieldData['columnSeparator']))?$fieldData['columnSeparator']:' ');

			        foreach ($titles as $title)
			        {
				        $title = trim(trim($title, '`'));

				        if (isset($fieldData['keyProcessors'][$title]))
				        {
					        eval('$modTitle = ' . $fieldData['keyProcessors'][$title] . '($newOption->data[$title][\'value\']);');
					        $fullTitle .= $modTitle . $columnSeparator;
				        }
				        else $fullTitle .= $newOption->data[$title]['value'] . $columnSeparator;
			        }
			        if(isset($fieldData['columnSeparator'])) $fullTitle = rtrim($fullTitle, $columnSeparator);

			        $optionTexts[] = rtrim($fullTitle, ' ');

			        if(isset($record->data[$recordKey]['selectActions']['view']) && $record->data[$recordKey]['selectActions']['view']==1) $dataView[] = makeHTMLActionLink('viewRecord', $optionClass,$newOptionUid, false).'&formID='.$formID.'&key='.$recordKey;
			        else $dataView[] = '#'.$dummyIndex;

			        if(isset($record->data[$recordKey]['selectActions']['edit']) && $record->data[$recordKey]['selectActions']['edit']==1) $dataEdit[] = makeHTMLActionLink('editRecord', $optionClass,$newOptionUid, false).'&formID='.$formID.'&key='.$recordKey;
			        else $dataEdit[] = '#'.$dummyIndex;

			        if(isset($record->data[$recordKey]['selectActions']['delete']) && $record->data[$recordKey]['selectActions']['delete']==1) $dataDelete[] = makeHTMLActionLink('deleteRecord', $optionClass,$newOptionUid, false).'&formID='.$formID.'&key='.$recordKey;
			        else $dataDelete[] = '#'.$dummyIndex;

			        $dummyIndex++;
		        }

		        echo '{"jsonrpc" : "2.0", "result" : "success", "pasted_class": '.json_encode($optionClass).', "pasted_uids": '.json_encode($newOptionsUids).', "pasted_texts": '.json_encode($optionTexts).', "data_edit":'.json_encode($dataEdit).', "data_view":'.json_encode($dataView).', "data_delete":'.json_encode($dataDelete).'}';
		        //echo '{"jsonrpc" : "2.0", "result" : "success", pasted_class: '.json_encode($optionClass).'}';

	        }
	        else{
		        echo '{"jsonrpc" : "2.0", "result" : "error", "message" : "'.$l10n->getLabel('backend','clipboard_class_mismatch').'" }';
	        }
        }

        //paste selected options to a recordEditTable
        else if($_REQUEST['action'] == 'pasteRecordsToRET'){

	        $copyPasteToParentClass =  $_REQUEST["copyPasteToParentClass"];
	        $copyPasteToParentUid = $_REQUEST["copyPasteToParentUid"];

 	        //class shown in the RET
	        $recordClass = $_REQUEST['record_class'];

	        //retreive clipboard data from session
	        $copiedClass = $_SESSION['clipboard']['copied_class'];
	        $copiedUids = explode(',',$_SESSION['clipboard']['copied_uids']);

	        $session_recordEditTableID = $_REQUEST['session_recordEditTableID'];
	        $RETParameters = $_SESSION['mf']['recordEditTables'][$session_recordEditTableID];
	        $request = $RETParameters['request'];
	        $advancedOptions = $RETParameters['advancedOptions'];
	        $mainKey = $RETParameters['mainKey'];
	        $keyProcessors = $RETParameters['keyProcessors'];
	        $availableActions = $RETParameters['availableActions'];
	        $recordEditTableID = $RETParameters['recordEditTableID'];
	        $moduleName = $RETParameters['moduleName'];
	        $subModuleName = $RETParameters['subModuleName'];

	        $copyPasteToParentClassColumn = (isset($advancedOptions["copyPasteToParentClassColumn"]))? $advancedOptions["copyPasteToParentClassColumn"] : 'parent_class';
	        $copyPasteToParentUidColumn = (isset($advancedOptions["copyPasteToParentUidColumn"]))? $advancedOptions["copyPasteToParentUidColumn"]: 'parent_uid';

	        if($copiedClass == $recordClass){

		        $pastedRecordsUids = array();
		        $pastedRecordsHTML = array();

		        $dummyIndex = 0;

		        //paste rows in the database
		        foreach($copiedUids as $copiedUid){
			        eval('$src'.$copiedUid.' = new '.$copiedClass.';');
			        eval('$src'.$copiedUid.'->load('.$copiedUid.');');
			        eval('$pastedRecord = clone $src'.$copiedUid.';');
			        $pastedRecord->data[$copyPasteToParentUidColumn]['value'] = $copyPasteToParentUid;
			        $pastedRecord->data[$copyPasteToParentClassColumn]['value'] = $copyPasteToParentClass;
			        $pastedRecord->data['deleted']['value'] = 0;
			        $pastedUids[] = $pastedRecordUid = $pastedRecord->store();

		        }

		        //get HTML for pasted rows


		        //RET initializations
		        $language = $mf->getDataEditingLanguage();

		        if(!isset($request['SELECT']) || trim($request['SELECT']) == '') $request['SELECT'] = '*';

		        if(!isset($request['FROM'])  || trim($request['FROM']) == '') $request['FROM'] = static::getTableName();

		        $fromDef = explode(' ',$request['FROM']);

		        //OK
		        if(sizeof($fromDef)==2){
			        $alias = $fromDef[1];
		        }
		        else $alias = $fromDef[0];

		        if(!isset($request['JOIN'])) $request['JOIN'] = '';

		        $customJoinSupplement = '';
		        if(trim($request['JOIN']) != '') {
			        $customJoinSupplement = ' ';
		        }


		        if(isset($advancedOptions['forceDistinct']) && $advancedOptions['forceDistinct']==true) $distinct = "DISTINCT ";
		        else $distinct = '';



				//At last, we can do our SQL request
		        $sql = "SELECT ".$distinct.$alias.".*,".$request['SELECT']." FROM ".$request['FROM'].$customJoinSupplement.$request['JOIN']." WHERE UID in (".implode(',',$pastedUids).")";
		        //echo "sql = ".$sql.chr(10);

		        try{
			        $stmt = $pdo->query($sql);
		        }
		        catch(PDOException $e){
			        /*echo "<strong>Message =</strong> ".$e->getMessage()."<br/>".chr(10);
					echo "<strong>SQL =</strong> ".$sql."<br/>".chr(10);
					return 0;*/
			        $html[] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
			                  "<strong>Message =</strong> ".$e->getMessage()."<br/>".
			                  "<strong>SQL =</strong> ".$sql."<br/>".
			                  '</div>';
			        echo '{"jsonrpc" : "2.0", "result" : "error", "message" : "'.$e->getMessage().' SQL='.$sql.'" }';

		        }

		        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		        //echo "rows = ";
		        //print_r($rows);

		        foreach($rows as $row) {
			        $pastedRecordsHTML[] = $pastedRecord->RETGetRow($row,false,$session_recordEditTableID);
		        }

		        echo '{"jsonrpc" : "2.0", "result" : "success", "nbElements": "'.count($rows).'", "pasted_class": '.json_encode($copiedClass).', "pasted_uids": '.json_encode($pastedUids).', "pasted_html": '.json_encode($pastedRecordsHTML).'}';
	        }
	        else{
		        echo '{"jsonrpc" : "2.0", "result" : "error", "message" : "'.$l10n->getLabel('backend','clipboard_class_mismatch').'" }';
	        }
        }



        //sorting with dbRecord showEditTable
        else if($_REQUEST['action'] == 'sort'){

            $direction=$_REQUEST['direction'];
            $uid = intval($_REQUEST['record_uid']);

            $record = new $_REQUEST['record_class'];
            eval('$tableName = '.$_REQUEST['record_class'].'::$tableName;');

            //first make sure sorting is consistent in the table : renumerate every entry
            $sql = "set @position := 0;";
            $stmt = $pdo->prepare($sql);
            try{
                $stmt->execute(array());
            }
            catch(PDOException $e){
                echo $e->getMessage();
            }

            $sql = "UPDATE ".$tableName." SET sorting = (@position := @position + 1) ORDER BY sorting ASC;";
            $stmt = $pdo->prepare($sql);
            try{
                $stmt->execute(array());
            }
            catch(PDOException $e){
                echo $e->getMessage();
            }

            //now load our record with correct sorting specified
           $record->load($uid);

            //then update the record immediatly above and decrement its sorting
            if($direction == "up"){
                $sql = "UPDATE ".$tableName." SET sorting = sorting-1 WHERE sorting = ".($record->data['sorting']['value']+1).";";
                $stmt = $pdo->prepare($sql);
                try{
                    $stmt->execute(array());
                }
                catch(PDOException $e){
                    echo $e->getMessage();
                }

                //increment ou record's sorting
                $record->data['sorting']['value'] += 1;
                $record->store();

                echo '{"jsonrpc" : "2.0", "result" : "sortUp done"}';
            }

            //then update the record immediatly below and increment its sorting
            if($direction == "down"){
                $sql = "UPDATE ".$tableName." SET sorting = sorting+1 WHERE sorting = ".($record->data['sorting']['value']-1).";";
                $stmt = $pdo->prepare($sql);
                try{
                    $stmt->execute(array());
                }
                catch(PDOException $e){
                    echo $e->getMessage();
                }

                //increment ou record's sorting
                $record->data['sorting']['value'] -= 1;
                $record->store();

                echo '{"jsonrpc" : "2.0", "result" : "sortUp done"}';
            }
        }


        //sorting with jQuery sortable
        else if($_REQUEST['action'] == 'sortRecords'){

            //find the list of sorted records
            $sortedArray = array();
            $sortedField = '';
            foreach($_REQUEST as $field => $item){
                if(is_array($item)){
                    $sortedArray = $item;
                    $sortedField = $field;
                }
            }

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class'];
            //load its content
            $record->load(intval($_REQUEST['record_uid']));

            if($_REQUEST['record_mode'] == 'record|'){
                //the field is not located inside a template data structure, but straight in the record
                //simply update the requested field
                $currentValue = $record->data[$sortedField]['value'];

                $unsorted = explode(',',ltrim($currentValue,','));
                $sorted = array();

                //search each item ID in the transmited array and move the file values accordingly
                //thank you jQuery for having us do such complicate things because it can not transmit a filename using sortable()
                foreach($unsorted as $item){
                    $itemSlug = Slug($item, '');
                    $itemPos = 0;
                    $itemPos = array_search($itemSlug, $sortedArray);
                    $sorted[$itemPos] = $item;
                }
                ksort($sorted);

                $newFieldValue = $record->data[$sortedField]['value'] = ltrim(implode(',', $sorted), ',');

            }
            else if($_REQUEST['record_mode'] == 'template|'){
                //the field is located inside a template data structure
                //update the field inside the template_content field
                $currentValue = $record->data['template_content']['value'][$sortedField]['value'];

                $unsorted = explode(',',ltrim($currentValue,','));
                $sorted = array();

                //search each item ID in the transmited array and move the file values accordingly
                //thank you jQuery for having us do such complicate things because it can not transmit a filename using sortable()
                foreach($unsorted as $item){
                    $itemSlug = Slug($item, '');
                    $itemPos = 0;
                    $itemPos = array_search($itemSlug, $sortedArray);
                    $sorted[$itemPos] = $item;
                }
                ksort($sorted);

                $newFieldValue = $record->data['template_content']['value'][$sortedField]['value'] = ltrim(implode(',', $sorted), ',');
            }

            //save the updated record
            $record->store();



            echo '{"jsonrpc" : "2.0", "result" : "records have been sorted"}';

        }



        //sorting with jQuery sortable
        else if($_REQUEST['action'] == 'sortRecordsFile'){

            //find the list of sorted records
            $sortedArray = array();
            $sortedField = '';
            foreach($_REQUEST as $field => $item){
                if(is_array($item)){
                    $sortedArray = $item;
                    $sortedField = $field;
                }
            }

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class'];
            //load its content
            $record->load(intval($_REQUEST['record_uid']));

            if($_REQUEST['record_mode'] == 'record|'){
                //the field is not located inside a template data structure, but straight in the record
                //simply update the requested field
                $unsorted = $record->data[$sortedField]['value'];

                /*//mf < 1.4 data conversion
                if(!is_array($unsorted)){
                    $unsorted = $record->convertFilesValue($unsorted);
                }*/

                $sorted = array();

                //search each item ID in the transmited array and move the file values accordingly
                //thank you jQuery for having us do such complicate things because it can not transmit a filename using sortable()
                foreach($unsorted as $key => $item){
                    $itemSlug = Slug($item['filename'], '');
                    $itemPos = 0;
                    $itemPos = array_search($itemSlug, $sortedArray);
                    $sorted[$itemPos] = $item;
                }
                ksort($sorted);

                $record->data[$sortedField]['value'] = $sorted;

            }
            else if($_REQUEST['record_mode'] == 'template|'){
                //the field is located inside a template data structure
                //update the field inside the template_content field
                $unsorted = $record->data['template_content']['value'][$sortedField]['value'];

                //mf < 1.4 data conversion
                /*if(!is_array($unsorted)){
                    $unsorted = $record->convertFilesValue($unsorted);
                }*/

                $sorted = array();

                //search each item ID in the transmited array and move the file values accordingly
                //thank you jQuery for having us do such complicate things because it can not transmit a filename using sortable()
                foreach($unsorted as $key => $item){
                    $itemSlug = Slug($item['filename'], '');
                    $itemPos = 0;
                    $itemPos = array_search($itemSlug, $sortedArray);
                    $sorted[$itemPos] = $item;
                }
                ksort($sorted);

                $record->data['template_content']['value'][$sortedField]['value'] =  $sorted;
            }


            //save the updated record
            $record->store();



            echo '{"jsonrpc" : "2.0", "result" : "records have been sorted"}';

        }


        else if($_REQUEST['action'] == 'sortMicrotemplates'){
            $recordClassName    = $_REQUEST['recordClassName']; //class in which the microtemplate is located
            $recordUid          = $_REQUEST['recordUid'];       //record instance unique id
            $key                = $_REQUEST['key'];             //microtemplate key identifier
            $m                  = $_REQUEST['m'];               //list of the indexes supplied in the new order by sortable

            //load existing values
            $record = new $recordClassName();
            $record->load($recordUid);
            $existingMicrotemplatesArray = $record->data['template_content']['value'][$key]['value'];
            /*echo "key=".$key."
            ";
            print_r($existingMicrotemplatesArray);*/

            //reordering microtemplates
            $newMicrotemplatesArray = array();
            foreach($m as $newIndex){
                $newMicrotemplatesArray['m_'.$newIndex] = $existingMicrotemplatesArray['m_'.$newIndex];
            }

            $record->data['template_content']['value'][$key]['value'] = $newMicrotemplatesArray;

            $record->store();


            echo '{"jsonrpc" : "2.0", "result" : "Microtemplates have been sorted"}';
        }

        else if($_REQUEST['action'] == 'updateAltLanguage'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){
                $key = $_REQUEST['record_key'];
                $altLang = $_REQUEST['alt_lang'];
                $altLangUid = $_REQUEST['altLang_uid'];

                if(!isset($record->data['alt_language'])){
                    $record->data['alt_language'] = array();
                }
                if(!isset($record->data['alt_language']['value'])){
                    $record->data['alt_language']['value'] = array();
                }

                $record->data['alt_language']['value'][$altLang] = $altLangUid;

                $status = $record->store();


                $action = ' alternative language \''.$_REQUEST['alt_lang'].'\' updated';
                echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].$action.'", "store_status" : "'.$status.'" }';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The record could not be found."}';
            }

        }



        else if($_REQUEST['action'] == 'deleteRecord'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){

                //delete the record
                $record->delete();

                echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].' has been deleted"}';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The record could not be deleted."}';
            }
        }

        else if($_REQUEST['action'] == 'deleteRecords'){

            $recordUid = $_REQUEST['record_uid'];
            $recordKey = $_REQUEST['record_key'];
            $recordClass = $_REQUEST['record_class'];
            $optionUids = $_REQUEST['option_uids'];
            $optionClass = $_REQUEST['option_class'];
            $targetOptionsClass = $_REQUEST['option_class'];
            $formID = $_REQUEST['form_id'];

            if($targetOptionsClass == $optionClass){

                $options = explode(',',$optionUids);
                $newOptionsUids = array();

                $dummyIndex = 0;
                foreach($options as $optionUid){
                    eval('$option'.$optionUid.' = new '.$optionClass.'();');
                //echo('$option'.$optionUid.' = new '.$optionClass.'();');
                    eval('$option'.$optionUid.'->load('.$optionUid.');');
                //echo('$option'.$optionUid.'->load('.$optionUid.');');
                    eval('$option'.$optionUid.'->delete();');
                //echo('$option'.$optionUid.'->delete();');

                    eval('$deletedOptionsUids[] = '.$optionUid.';');

                    $dummyIndex++;
                }

                //retreive all active options data and return them as an additional info, so the javascript callback functions have direct access to the content of all the options listed.

                $record = new $recordClass();
                if(get_parent_class($record)=="dbRecord") $record->load($recordUid);

                echo '{"jsonrpc" : "2.0", "result" : "success", "deleted_class": '.json_encode($optionClass).', "deleted_uids": '.json_encode($deletedOptionsUids).'}';

            }
            else{
                echo '{"jsonrpc" : "2.0", "result" : "error", "message" : "'.$l10n->getLabel('backend','clipboard_class_mismatch').'" }';
            }

        }

        else if($_REQUEST['action'] == 'restoreRecord'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){
                //delete the record
                $record->restore();

                echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].' has been restored"}';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The record could not be deleted."}';
            }
        }

        // typeahead search in recordSelectField
        else if($_REQUEST['action'] == 'recordSearch'){
            //Typeahead autocompletion
            $searchValue = $_POST['value'];
            $formID = $_POST['form_ID'];
            $searchFulltextFields = $_POST['search_fulltext_fields'];
            $searchVarcharFields = $_POST['search_varchar_fields'];
            $searchEqualFields = $_POST['search_equal_fields'];
            $searchTitleFields = $_POST['search_title_fields'];
            $searchWhere = $_POST['search_where'];
            $searchOrderByFields = $_POST['search_orderby_fields'];
            $searchTableName = $_POST['search_table_name'];
            $searchClassName = $_POST['search_class_name'];
            $searchDebug = $_POST['search_debug'];
            $callingFieldName = $_POST['calling_field_name'];
            $callingRecordClass = $_POST['calling_record_class'];

            if(trim($searchTitleFields) ==''){
                echo '{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('main','define_searchTitleFields')).'}';
            }
            else if(trim($searchFulltextFields) == '' && trim($searchVarcharFields) == '' && trim($searchEqualFields) == ''){
                echo '{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('main','define_searchTargetFields')).'}';
            }
            else {
                $items = dbRecord::searchRecordTypeahead($searchValue,$formID,$searchFulltextFields,$searchVarcharFields,$searchEqualFields,$searchWhere,$searchOrderByFields,$searchTitleFields,$searchTableName,$searchClassName,$callingFieldName,$callingRecordClass,$searchDebug);
                echo '{"jsonrpc" : "2.0", "result" : "success", "items" : '.json_encode($items).'}';
            }
        }


        else if($_REQUEST['action'] == 'getPreviewUrl'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($_REQUEST['record_uid']!='' && $record->load(intval($_REQUEST['record_uid'])) > 0){
                $previewUrl = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$record->getPreviewUrl() : "http://".$_SERVER['SERVER_NAME'].$record->getPreviewUrl();


                echo '{"jsonrpc" : "2.0", "result" : '.json_encode($previewUrl).'}';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The specified record could not be loaded (invalid UID ?)."}';
            }
        }

        else if($_REQUEST['action'] == 'showHideRecord'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){
                //update the record
                if($record->data['hidden']['value'] == '1')
                    $record->data['hidden']['value'] = '0';
                else $record->data['hidden']['value'] = '1';

                $record->store();

                $action = ($record->data['hidden']['value'] == 1)?' has been hidden':' has been shown';
                echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].$action.'", "hidden" : "'.$record->data['hidden']['value'].'"}';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The record could not be shown of hidden."}';
            }
        }

        else if($_REQUEST['action'] == 'createRecord'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();
            //create the record
            if($uid = $record->store()){

                echo '{"jsonrpc" : "2.0", "result" : "Record '.$uid.' has been created", "uid" : "'.$uid.'"}';
            }
            else{

                echo '{"jsonrpc" : "2.0", "error" : "The record could not be created."}';
            }
        }

        else if($_REQUEST['action'] == 'closeRecord'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();
            $record->load($_REQUEST['record_uid']);

            //when closing a record that has been stored automatically for technical reasons, but has never been saved by the user, delete the record forever
            if($record->data['deleted']['value']==2){
                $record->delete_forever();
            }

            echo '{"jsonrpc" : "2.0", "result" : "success", "message" : "Close action processed."}';

        }



        //saves a record in AJAX from a formsManager form, but processes only the fields specified in $_REQUEST['saveThoseFields'] (comma separated list of field names)
        //other fields are ignored and the form is saved, even if some fields not specified in the saveThoseFields list are not valid (defaut values for those field should match the SQL table definition constraints though, otherwise the database will not accept to save the record).
        else if($_REQUEST['action'] == 'saveRecord'){

            if(isset($_REQUEST['saveThoseFields']) && $_REQUEST['saveThoseFields'] != '')$saveThoseFields = explode(',',$_REQUEST['saveThoseFields']);
            else $saveThoseFields = array();

            $formResult = $formsManager->checkAndProcessForm($saveThoseFields);

            $errorFields = $formResult['errorFields'];


            $record = $formResult['record'];


            //filter field errors if saving only the field list specified in $_REQUEST['saveThoseFields']
            if(sizeof(array_filter($saveThoseFields)) > 0){
                foreach($errorFields as $fieldName => $errorMessage){
                    if(!in_array($fieldName, $saveThoseFields)){
                        unset($errorFields[$fieldName]);
                    }
                }
            }

            if(sizeof($errorFields) == 0){

                //if the record has no uid yet and we have a partial save, create a temporary record with data['deleted']['value']=2
                if($record->data['uid']['value']=='' && $_REQUEST['saveThoseFields'] != '')
                    $record->data['deleted']['value']= 2 ;

                //keep temporary state if a new partial save occurs and the record is already in temporary state
                elseif ($_REQUEST['saveThoseFields'] != '' && $record->data['deleted']['value']== 2)
                    $record->data['deleted']['value']= 2 ;

                //switch to active state in other cases
                elseif ($record->data['deleted']['value'] != 1) //otherwise, set the record to active state, if not deleted
                    $record->data['deleted']['value']= 0 ;

                //create / save the record
                $uid = $record->store(true,false,false);

                //print_r($record);

                if($uid >0){

                    //update the record's form

                    $subModuleName          = (isset($_REQUEST['submodule']))?$_REQUEST['submodule']:'';
                    $showSaveButton         = (isset($_REQUEST['showSaveButton']))?$_REQUEST['showSaveButton']:null;
                    $showSaveCloseButton    = (isset($_REQUEST['showSaveCloseButton']))?$_REQUEST['showSaveCloseButton']:null;
                    $showPreviewButton      = (isset($_REQUEST['showPreviewButton']))?$_REQUEST['showPreviewButton']:null;
                    $showCloseButton        = (isset($_REQUEST['showCloseButton']))?$_REQUEST['showCloseButton']:null;
                    $ajaxEditing            = (isset($_REQUEST['ajaxEditing']))?$_REQUEST['ajaxEditing']:null;
                    $callbackFunction       = (isset($_REQUEST['callbackFunction']))?$_REQUEST['callbackFunction']:null;
                    $recordEditTableID      = (isset($_REQUEST['recordEditTableID'])?$_REQUEST['recordEditTableID']:null);


                    if(isset($_REQUEST['advancedSettings'])){
                        parse_str(base64_decode(urldecode($_REQUEST['advancedSettings'])),$advancedSettings);
                    }
                    else $advancedSettings = null;

                    if(isset($_REQUEST['formAttributes'])){
                        parse_str(base64_decode(urldecode($_REQUEST['formAttributes'])),$formAttributes);
                    }
                    else $formAttributes = null;

                    if($callbackFunction != ''){
                        //call the PHP callback function
                        eval('$phpResult = '.$callbackFunction.'("success",$record);');
                    }

                    //re-generate the form if $reloadFormOnSave == true
                    if(isset($advancedSettings['reloadFormOnSave']))$reloadFormOnSave = filterBoolean($advancedSettings['reloadFormOnSave']);
                    else $reloadFormOnSave = false;
                    if($reloadFormOnSave) $formHTML = $formsManager->editRecord($record, $_REQUEST['module'], $subModuleName, $showSaveButton, $showSaveCloseButton, $showPreviewButton, $showCloseButton, $ajaxEditing, $advancedSettings, $formAttributes, $recordEditTableID);



                    //update form security variables
                    $secKeys = array(
                        'action' => 'saveRecord',
                        'class' => $_REQUEST['class'],
                        'uid' => $uid
                    );
                    if($callbackFunction != '')$secKeys['callbackFunction'] = $callbackFunction;
                    $secHash = formsManager::makeSecValue($secKeys);
                    $secFields = implode(',',array_keys($secKeys));

                    //return values as JSON
                    echo '{"jsonrpc" : "2.0", "result" : "success", "message" : "Record '.$uid.' has been saved", "action_status" : "ok", "uid" : "'.$uid.'", "class" : "'.$_REQUEST['class'].'", "message" : '.json_encode('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert">×</button>'.$l10n->getLabel('backend','object-save-success').'</div>').((isset($phpResult))?', "phpResult" : '.json_encode($phpResult):'').((($reloadFormOnSave))?', "form" : '.json_encode($formHTML):'').', "saveRecordHash" : "'.$secHash.'", "saveRecordFields" : "'.$secFields.'"}';

                }
                else{

                    echo '{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button>'.$l10n->getLabel('backend','object-save-error').'</div>').'}';
                }
            }
            else{

                $callbackFunction       = (isset($_REQUEST['callbackFunction']))?$_REQUEST['callbackFunction']:null;

                if($callbackFunction != ''){
                    //call the PHP callback function
                    eval('$phpResult = '.$callbackFunction.'("error",$record);');
                }

                //some fields are not valid

                echo '{"jsonrpc" : "2.0", "result" : "success", "message" : "Record has erroneous fields", "action_status" : "fail",  "message" : '.json_encode('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button>'.$l10n->getLabel('backend','object-field-error').'</div>').', "failed_fields" : '.json_encode($errorFields).', "error_count_by_tab" : '.json_encode($formResult['errorCountByTab']).((isset($phpResult))?', "phpResult" : '.json_encode($phpResult):'').'}';

            }
        }



        //Handles form processing. Detects erroneous fields. The generated form object can be obtained in a custom phpFunction specified in the parameter $advancedSettings['PHPCallbackFunctionAfterSubmit'] specified when calling formsManager->editForm

        else if($_REQUEST['action'] == 'processForm'){

            //on reprend le code de saveRecord
            if(isset($_REQUEST['saveThoseFields']) && $_REQUEST['saveThoseFields'] != '')$saveThoseFields = explode(',',$_REQUEST['saveThoseFields']);
            else $saveThoseFields = array();

            $formResult = $formsManager->checkAndProcessForm($saveThoseFields);

            $errorFields = $formResult['errorFields'];
            $form = $formResult['record'];

            //filter field errors if saving only the field list specified in $_REQUEST['saveThoseFields']
            if(sizeof(array_filter($saveThoseFields)) > 0){
                foreach($errorFields as $fieldName => $errorMessage){
                    if(!in_array($fieldName, $saveThoseFields)){
                        unset($errorFields[$fieldName]);
                    }
                }
            }

            if(sizeof($errorFields) == 0){

                //no form processing occurs here. It is done in the formsManager checkAndProcessForm() and processform() functions.

                $callbackFunction       = (isset($_REQUEST['callbackFunction']))?$_REQUEST['callbackFunction']:null;


                //update form security variables
                $secKeys = array();
                if($callbackFunction != '')$secKeys['callbackFunction'] = $callbackFunction;
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                if($callbackFunction != ''){
                    //call the PHP callback function
                    eval('$phpResult = '.$callbackFunction.'("success",$form);');
                }

                echo '{"jsonrpc" : "2.0", "result" : "success", "action_status" : "ok"'.((isset($phpResult))?', "phpResult" : '.json_encode($phpResult):'').', "saveRecordHash" : "'.$secHash.'", "saveRecordFields" : "'.$secFields.'"}';

            }
            else{

                $callbackFunction       = (isset($_REQUEST['callbackFunction']))?$_REQUEST['callbackFunction']:null;

                if($callbackFunction != ''){
                    //call the PHP callback function
                    eval('$phpResult = '.$callbackFunction.'("error",$form);');
                }

                //some fields are not valid
                echo '{"jsonrpc" : "2.0", "result" : "error", "message" : "Record has erroneous fields", "action_status" : "fail",  "message" : '.json_encode('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button>'.$l10n->getLabel('backend','object-field-error').'</div>').', "failed_fields" : '.json_encode($errorFields).', "error_count_by_tab" : '.json_encode($formResult['errorCountByTab']).((isset($phpResult))?', "phpResult" : '.json_encode($phpResult):'').'}';

            }

        }



        // loads a record's editing form
        else if($_REQUEST['action'] == 'loadRecordForm'){

            $class = $_REQUEST['record_class'];
            $uid = $_REQUEST['record_uid'];


            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){

                //update the record's form

                $subModuleName          = (isset($_REQUEST['submodule']))?$_REQUEST['submodule']:'';
                $showSaveButton         = (isset($_REQUEST['showSaveButton']))?$_REQUEST['showSaveButton']:null;
                $showSaveCloseButton    = (isset($_REQUEST['showSaveCloseButton']))?$_REQUEST['showSaveCloseButton']:null;
                $showPreviewButton      = (isset($_REQUEST['showPreviewButton']))?$_REQUEST['showPreviewButton']:null;
                $showCloseButton        = (isset($_REQUEST['showCloseButton']))?$_REQUEST['showCloseButton']:null;
                $ajaxEditing            = (isset($_REQUEST['ajaxEditing']))?$_REQUEST['ajaxEditing']:null;
                $callbackFunction       = (isset($_REQUEST['callbackFunction']))?$_REQUEST['callbackFunction']:null;
                $recordEditTableID=(isset($_REQUEST['recordEditTableID'])?$_REQUEST['recordEditTableID']:null);


                if(isset($_REQUEST['advancedSettings'])){
                    parse_str(base64_decode(urldecode($_REQUEST['advancedSettings'])),$advancedSettings);
                }
                else $advancedSettings = null;

                if(isset($_REQUEST['formAttributes'])){
                    parse_str(base64_decode(urldecode($_REQUEST['formAttributes'])),$formAttributes);
                }
                else $formAttributes = null;

                if($callbackFunction != ''){
                    //call the PHP callback function
                    eval($callbackFunction.'("success","'.$class.'","'.$uid.'");');
                }

                $formHTML = $formsManager->editRecord($record, $_REQUEST['module'], $subModuleName, $showSaveButton, $showSaveCloseButton, $showPreviewButton, $showCloseButton, $ajaxEditing, $advancedSettings, $formAttributes,$recordEditTableID);



                //update form security variables
                $secKeys = array(
                    'action' => 'saveRecord',
                    'class' => $class,
                    'uid' => $uid
                );
                if($callbackFunction != '')$secKeys['callbackFunction'] = $callbackFunction;
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                //return values as JSON
                echo '{"jsonrpc" : "2.0", "result" : "success", "action_status" : "ok", "uid" : "'.$uid.'", "class" : "'.$class.'", "message" : '.json_encode('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert">×</button>'.$l10n->getLabel('backend','object-save-success').'</div>').', "form" : '.json_encode($formHTML).', "saveRecordHash" : "'.$secHash.'", "saveRecordFields" : "'.$secFields.'"}';
            }
            else{
                //some fields are not valid

                echo '{"jsonrpc" : "2.0", "result" : "error", "action_status" : "fail",  "message" : '.json_encode('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button>'.$l10n->getLabel('backend','object-could-not-load').'</div>').', "failed_fields" : '.json_encode($errorFields).'}';

            }
        }








        //changes the backend language
        else if($_REQUEST['action'] == 'switchLanguage'){

            $success = $mf->setDataEditingLanguage($_REQUEST['language']);

             //create the record
            if($success){
                echo '{"jsonrpc" : "2.0", "result" : "Language has been switched", "language" : "'.$_REQUEST['language'].'"}';
            }
            else{
                echo '{"jsonrpc" : "2.0", "error" : "There is no such language available"}';
            }
        }



        else if($_REQUEST['action'] == 'addMicrotemplate'){
            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){
                //update the record

                //get the microtemplate slot
                $microtemplateSlot = $record->data['template_content']['value'][$_REQUEST['micro_key']]['value'];
                $newMicrotemplate = $mf->microtemplates[$_REQUEST['micro_key']];

                //add the new microtemplate
                $index = "m_".time();
                $microtemplateSlot[$index]=$newMicrotemplate;
                $record->data['template_content']['value'][$_REQUEST['micro_key']]['value'] = $microtemplateSlot;
                $status = $record->store();


                $action = ' microtemplate added';
                echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].$action.'", "added_index" : "'.$index.'", "store_status" : "'.$status.'"}';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The microtemplate could not be added because the record could not be loaded."}';
            }
        }

        else if($_REQUEST['action'] == 'deleteMicrotemplate'){
            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){
                //update the record

                //get the microtemplate slot
                $microtemplateSlot = $record->data['template_content']['value'][$_REQUEST['micro_key']]['value'];

                unset($microtemplateSlot[$_REQUEST['micro_index']]);
                //delete the new microtemplate

                $record->data['template_content']['value'][$_REQUEST['micro_key']]['value'] = $microtemplateSlot;
                $status = $record->store();


                $action = ' microtemplate deleted';
                echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].$action.'", "deleted_index" : "'.$_REQUEST['micro_index'].'", "store_status" : "'.$status.'"}';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The microtemplate could not be deleted because the record could not be loaded."}';
            }
        }


        else if($_REQUEST['action'] == 'editMicrotemplate'){
            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //prepare edition form
            $html = $formsManager->getMicroTemplateForm($_REQUEST['record_uid'], $_REQUEST['record_class'], $_REQUEST['micro_key'], $_REQUEST['micro_index']);



            $action = ' microtemplate edition';
            echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].$action.'", "html" : '.json_encode($html).'}';

        }
        else if($_REQUEST['action'] == 'saveMicrotemplate'){
            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();

            //load its content
            if($record->load($_REQUEST['record_uid']) > 0){

                $micro_key = $_REQUEST['micro_key'];
                $micro_index = $_REQUEST['micro_index'];


                //update the record's values

                $microtemplateData = $record->data['template_content']['value'][$micro_key]['value'][$micro_index]['microtemplate_data'];
                $fieldNames = array_keys($microtemplateData);

                //print_r($microtemplateData);

                foreach($fieldNames as $fieldName){
                    $dataType = explode(' ',trim($microtemplateData[$fieldName]['dataType']));
                    if($dataType[0] != 'files' && $dataType[0] != 'files2'){
                        $microtemplateData[$fieldName]['value'] = $_REQUEST['micro|'.$fieldName];
                    }
                }
                //echo "***".chr(10).chr(10);
                //print_r($microtemplateData);


                $record->data['template_content']['value'][$micro_key]['value'][$micro_index]['microtemplate_data'] = $microtemplateData;

                $status = $record->store();


                $html = "Enregistrement sauvegardé.";
                $action = " saved successfuly.";

                echo '{"jsonrpc" : "2.0", "result" : "Record '.$_REQUEST['record_uid'].$action.'", "html" : '.json_encode($html).'}';
            }
            else {

                echo '{"jsonrpc" : "2.0", "error" : "The microtemplate could not be deleted because the record could not be loaded."}';
            }
        }




        else if($_REQUEST['action'] == 'updateRecordEditTable'){


	        $session_recordEditTableID = (isset($_REQUEST['session_recordEditTableID']) ? $_REQUEST['session_recordEditTableID'] : null);

	        if(isset($_SESSION['mf']['recordEditTables'][$session_recordEditTableID])) {
		        $RETSession = $_SESSION['mf']['recordEditTables'][ $session_recordEditTableID ];

		        //print_r($RETSession);
		        //echo 'ORDER BY = '.$RETSession['request']['ORDER BY'];

		        //create an instance of the object class displayed in the recordEditTable
		        $record = new $RETSession['recordClass']();

		        $request = $RETSession['request'];

		        if ( isset( $_REQUEST['sorting_field'] ) && ( $_REQUEST['sorting_field'] != 'undefined' ) ) {
			        $request['ORDER BY'] = urldecode( $_REQUEST['sorting_field'] );
		        }

		        if ( isset( $_REQUEST['sorting_direction'] ) ) {
			        $request['ORDER DIRECTION'] = $_REQUEST['sorting_direction'];
		        }

		        $keyProcessors = $RETSession['keyProcessors'];
		        $actions       = $RETSession['availableActions'];

		        $advancedOptions = $RETSession['advancedOptions'];
		        if ( isset( $_REQUEST['copy_paste'] ) ) {
			        $advancedOptions['copyPasteBuffer'] = $_REQUEST['copy_paste'];
		        }

		        $recordEditTableID = $RETSession['recordEditTableID'];

		        $moduleName    = $RETSession['moduleName'];
		        $subModuleName = $RETSession['subModuleName'];
		        $mainKey       = $RETSession['mainKey'];

		        $copyPasteUids = $_REQUEST['copy_paste'];
		        if ( isset( $_REQUEST['show_page_num'] ) ) {
			        $page = $_REQUEST['show_page_num'];
		        } else {
			        $page = 0;
		        }

		        //regenerate an updated recordEditTable view
		        $html = $record->showRecordEditTable( $request, $moduleName, $subModuleName, $mainKey, $keyProcessors, $page, $actions, $advancedOptions, $recordEditTableID );
		        $html = str_replace( "{subdir}", SUB_DIR, $html );

		        //and return the generated view's HTML
		        echo '{"jsonrpc" : "2.0", "result" : "success", "html" : ' . json_encode( $html ) . '}';
	        }
	        else{
		        echo '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','session_expired')).'}';
		    }

        }


        else if($_REQUEST['action'] == 'getOption'){

            $recordUid = $_REQUEST['record_uid'];
            $recordClass = $_REQUEST['record_class'];
            $recordKey = $_REQUEST['record_key'];
            $optionUid = $_REQUEST['option_uid'];
            $optionClass = $_REQUEST['option_class'];
            $formID = $_REQUEST['form_id'];

            $record = new $recordClass();
            if(get_parent_class($record)=="dbRecord") $record->load($recordUid);

            if(isset($record->data[$recordKey]['initItemsSQL']))
                $titleFields =  $record->data[$recordKey]['initItemsSQL']['SELECT'];
            else if(isset($record->data[$recordKey]['addItemsSQL']))
                $titleFields =  $record->data[$recordKey]['addItemsSQL']['SELECT'];
            else $titleFields = '';

            $option = new $optionClass();
            $option->load($optionUid);
            $fieldData = $record->data[$recordKey];

            $titles = explode(',',$titleFields);

            $fullTitle='';
            $columnSeparator = ((isset($fieldData['columnSeparator']))?$fieldData['columnSeparator']:' ');

            foreach($titles as $title)
            {
                $title = trim(trim($title, '`'));

                if(isset($fieldData['keyProcessors'][$title])){
                    $funcName = $fieldData['keyProcessors'][$title];
                    $funcArg = $option->data[$title]['value'];

                    eval('$modTitle = '.$funcName.'($funcArg);');

                    $fullTitle .= $modTitle. $columnSeparator;
                }
                else $fullTitle .= $option->data[$title]['value']. $columnSeparator;
            }
            if(isset($fieldData['columnSeparator'])) $fullTitle = rtrim($fullTitle, $columnSeparator);

            $text = rtrim($fullTitle, ' ');

            if(isset($fieldData['selectActions']['view']) && $fieldData['selectActions']['view']==1) $dataView = makeHTMLActionLink('viewRecord', $optionClass,$optionUid, false).'&formID='.$formID.'&key='.$recordKey;
            else $dataView = '';

            if(isset($fieldData['selectActions']['edit']) && $fieldData['selectActions']['edit']==1) $dataEdit = makeHTMLActionLink('editRecord', $optionClass,$optionUid, false).'&formID='.$formID.'&key='.$recordKey;
            else $dataEdit = '';

            if(isset($fieldData['selectActions']['delete']) && $fieldData['selectActions']['delete']==1) $dataDelete = makeHTMLActionLink('deleteRecord', $optionClass,$optionUid, false).'&formID='.$formID.'&key='.$recordKey;
            else $dataDelete = '';

            echo '{"jsonrpc" : "2.0", "result" : "option", "uid" : "'.$optionUid.'", "text" : "'.$text.'", "dataView" : '.json_encode($dataView).', "dataEdit" : '.json_encode($dataEdit).', "dataDelete" : '.json_encode($dataDelete).'}';
        }

        else if($_REQUEST['action'] == 'listRecordSelectItemsData'){

            $recordUid = $_REQUEST['record_uid'];
            $recordClass = $_REQUEST['record_class'];
            $recordKey = $_REQUEST['record_key'];

            $record = new $recordClass();
            //if(get_parent_class($record)=="dbRecord") {
            if(is_subclass_of($record,"dbRecord")) {

                if($record->load($recordUid)){

                    //retreive all active options data and return them as an additional info, so the javascript callback functions have direct access to the content of all the options listed.
                    $f = new stdClass();
                    $f->key = $recordKey;
                    $f->recordClassName = $recordClass;
                    $f->fieldData = $record->data[$recordKey];
                    $f->record = $record;

                    $allOptions = recordSelectField::getSelectItems($f,true);

                    echo '{"jsonrpc" : "2.0", "result" : "success", "optionsList" : '.json_encode($allOptions).'}';
                }
                else echo '{"jsonrpc" : "2.0", "result" : "error", "message" : '.$l10n->getLabel('main','could_not_load_record').'}';
            }
            else echo '{"jsonrpc" : "2.0", "result" : "error", "message" : '.$l10n->getLabel('main','must_subclass_dbrecord').'}';

        }

        else if($_REQUEST['action'] == 'selectRecordHistoryEntry'){
			global $formsManager;

            $recordUid = $_REQUEST['record|history_uid'];
            $recordClass = $_REQUEST['record|history_class'];
            $mode = $_REQUEST['mode'];
            $formID = $_REQUEST['formID'];

            if(isset($_REQUEST['record|history_field']))$recordField = $_REQUEST['record|history_field'];
            else $recordField = null;

            $selectedTimestamp = $_REQUEST['record|changes'];

	        $record = new $recordClass();
	        if($record->load($recordUid)){
//echo "dbRecord::getHistoryVersion($selectedTimestamp,$record,$recordField);".chr(10);
		        $tsSelectedFields = dbRecord::getHistoryVersion($selectedTimestamp,$record,$recordField);

		        if($tsSelectedFields) {
			        $html = '';

// print_r($tsSelectedFields);

			        foreach ( $tsSelectedFields as $fieldName => $fieldData ) {
				        $html .= $formsManager->getFieldHTML( $record, $fieldData, $fieldName, $formID, $mode, '', '', true ) . chr( 10 );
			        }

			        echo '{"jsonrpc" : "2.0", "result" : "success", "fieldsHTML" : ' . json_encode( $html ) . '}';
		        }
		        else echo '{"jsonrpc" : "2.0", "result" : "success", "fieldsHTML" : ' . json_encode('') . '}';


	        }
	        else echo '{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('main','error').' '.$l10n->getLabel('main','could_not_load_record')).'}';

        }


        else if($_REQUEST['action'] == 'historyRestoreRecord'){

            $recordUid = $_REQUEST['record|history_uid'];
            $recordClass = $_REQUEST['record|history_class'];
            $mode = $_REQUEST['mode'];
            $formID = $_REQUEST['formID'];

            $selectedTimestamp = $_REQUEST['record|changes'];


	        $subModuleName          = (isset($_REQUEST['record|submodule']))?$_REQUEST['record|submodule']:'';
	        $showSaveButton         = (isset($_REQUEST['record|showSaveButton']))?$_REQUEST['record|showSaveButton']:null;
	        $showSaveCloseButton    = (isset($_REQUEST['record|showSaveCloseButton']))?$_REQUEST['record|showSaveCloseButton']:null;
	        $showPreviewButton      = (isset($_REQUEST['record|showPreviewButton']))?$_REQUEST['record|showPreviewButton']:null;
	        $showCloseButton        = (isset($_REQUEST['record|showCloseButton']))?$_REQUEST['record|showCloseButton']:null;
	        $ajaxEditing            = (isset($_REQUEST['record|ajaxEditing']))?$_REQUEST['record|ajaxEditing']:null;
	        $callbackFunction       = (isset($_REQUEST['record|callbackFunction']))?$_REQUEST['record|callbackFunction']:null;
	        $recordEditTableID=(isset($_REQUEST['recordEditTableID'])?$_REQUEST['recordEditTableID']:null);



	        if(isset($_REQUEST['record|advancedSettings'])){
		        parse_str(base64_decode(urldecode($_REQUEST['record|advancedSettings'])),$advancedSettings);
	        }
	        else $advancedSettings = null;

	        if(isset($_REQUEST['record|formAttributes'])){
		        parse_str(base64_decode(urldecode($_REQUEST['record|formAttributes'])),$formAttributes);
	        }
	        else $formAttributes = null;


	        $record = new $recordClass();

	        if($record->load($recordUid)){
//echo "dbRecord::getHistoryVersion($selectedTimestamp,$record,$recordField);".chr(10);
		        $tsSelectedFields = dbRecord::getHistoryVersion($selectedTimestamp,$record,null);

		        foreach($tsSelectedFields as $fieldName => $fieldValue){
		        	//replace current record's field values with those from history
		        	$record->data[$fieldName]['value'] = $fieldValue['value'];
		        }


		        $formHTML = $formsManager->editRecord($record, $_REQUEST['module'], $subModuleName, $showSaveButton, $showSaveCloseButton, $showPreviewButton, $showCloseButton, $ajaxEditing, $advancedSettings, $formAttributes, $recordEditTableID);

		        //update form security variables
		        $secKeys = array(
			        'action' => 'saveRecord',
			        'class' => $recordClass,
			        'uid' => $recordUid
		        );
		        if($callbackFunction != '')$secKeys['callbackFunction'] = $callbackFunction;
		        $secHash = formsManager::makeSecValue($secKeys);
		        $secFields = implode(',',array_keys($secKeys));

		        $reloadFormOnSave = $record->reloadFormOnSave;

		        //return values as JSON
		        echo '{"jsonrpc" : "2.0", "result" : "success", "message" : "Record '.$recordUid.' has been saved", "action_status" : "ok", "uid" : "'.$recordUid.'", "class" : "'.$recordClass.'", "message" : '.json_encode('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert">×</button>'.$l10n->getLabel('backend','object-save-success').'</div>').((isset($phpResult))?', "phpResult" : '.json_encode($phpResult):'').((($reloadFormOnSave))?', "form" : '.json_encode($formHTML):'').', "saveRecordHash" : "'.$secHash.'", "saveRecordFields" : "'.$secFields.'"}';

	        }
	        else echo '{"jsonrpc" : "2.0", "result" : "error", "message" : '.json_encode($l10n->getLabel('main','error').' '.$l10n->getLabel('main','could_not_load_record')).'}';

        }

        else if($_REQUEST['action'] == 'uploadFile'){

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();
            //load its content
            $record->load($_REQUEST['record_uid']);

            if($_REQUEST['record_mode'] == 'record|'){
                //the field is not located inside a template data structure, but straight in the record
                //simply update the requested field
                $record->data[$_REQUEST['field_name']]['value'] .= ','.$_REQUEST['file'];
                $newFieldValue = $record->data[$_REQUEST['field_name']]['value'] = ltrim($record->data[$_REQUEST['field_name']]['value'], ',');

            }
            else if($_REQUEST['record_mode'] == 'template|'){
                //the field is located inside a template data structure
                //update the field inside the template_content field
                $record->data['template_content']['value'][$_REQUEST['field_name']]['value'] .= ','.$_REQUEST['file'];
                $newFieldValue = $record->data['template_content']['value'][$_REQUEST['field_name']]['value'] = ltrim($record->data['template_content']['value'][$_REQUEST['field_name']]['value'], ',');
            }
            else if($_REQUEST['record_mode'] == 'micro|'){
                //the field is located inside a template data structure
                //update the field inside the template_content field
                $micro_key = $_REQUEST['micro_key'];
                $micro_index = $_REQUEST['micro_index'];
                $field_name = $_REQUEST['field_name'];
                $record->data['template_content']['value'][$micro_key]['value'][$micro_index]['microtemplate_data'][$field_name]['value']  .= ','.$_REQUEST['file'];
                $newFieldValue = $record->data['template_content']['value'][$micro_key]['value'][$micro_index]['microtemplate_data'][$field_name]['value'] = ltrim($record->data['template_content']['value'][$micro_key]['value'][$micro_index]['microtemplate_data'][$field_name]['value'], ',');
            }

            //save the updated record
            $record->store();

            //update record with uploaded file
            //echo "success ".$_REQUEST['file'];

            echo '{"jsonrpc" : "2.0", "result" : "new field value ='.$newFieldValue.'"}';

        }

        if($_REQUEST['action'] == 'removeFile'){
            $fileDir = '';

            //for security reasons, files not found in the record wil not be processed
            //otherwise it would enable a remote attacker to erase any file on the hosting by altering the $_REQUEST['file'] value and use it to escalade directories.
            $fileFound = false;

            //create an instance of the object to be updated
            $record = new $_REQUEST['record_class']();


	        //if instance of dbRecord
	        if(is_subclass_of ($record,'dbRecord')) {

		        //load its content
		        $record->load( intval( $_REQUEST['record_uid'] ) );


		        switch ( $_REQUEST['record_mode'] ) {
			        case 'record|':
				        $files = $record->data[ $_REQUEST['field_name'] ]['value'];
				        break;

			        case 'template|':
				        $files = $record->data['template_content']['value'][ $_REQUEST['field_name'] ]['value'];
				        break;

			        case 'micro|':
				        $micro_key   = $_REQUEST['micro_key'];
				        $micro_index = $_REQUEST['micro_index'];
				        $field_name  = $_REQUEST['field_name'];

				        $files = $record->data['template_content']['value'][ $micro_key ]['value'][ $micro_index ]['microtemplate_data'][ $field_name ]['value'];
				        break;
		        }

		        $newFileList = array();

		        //regenerate the file list
		        foreach ( $files as $file ) {
			        if ( $file['filename'] == $_REQUEST['file'] ) {
				        //this is our file to be deleted
				        $fileFound = true;
			        } else {
				        //copy the file to the new file list if not marked for deletion
				        $newFileList[] = $file;
			        }
		        }

		        switch ( $_REQUEST['record_mode'] ) {
			        case 'record|':
				        //the field is not located inside a template data structure, but straight in the record
				        //simply update the requested field
				        $record->data[ $_REQUEST['field_name'] ]['value'] = $newFileList;
				        break;

			        case 'template|':
				        //the field is located inside a template data structure
				        //update the field inside the template_content field
				        $record->data['template_content']['value'][ $_REQUEST['field_name'] ]['value'] = $newFileList;
				        break;

			        case 'micro|':
				        //the field is located inside a template data structure
				        //update the field inside the template_content field
				        $record->data['template_content']['value'][ $micro_key ]['value'][ $micro_index ]['microtemplate_data'][ $field_name ]['value'] = $newFileList;
				        break;


		        }


		        $success = false;


		        //process our filepath if the filename has been found in this field
		        $filePath = DOC_ROOT . $mf->getUploadsDir() . $record->getFilesDir();

		        $fileAskedForRemoval = $filePath . $_REQUEST['file'];

		        //now delete our file
		        if ( $fileFound ) {
			        $filePrefix   = strstr( $_REQUEST['file'], '.', true );
			        $removedFiles = array();

			        //scan the directory for the original file and eventually its resized instances (if it is an image)
			        if ( $directoryContent = scandir( $filePath ) ) {
				        foreach ( $directoryContent as $fileInDir ) {
					        if ( substr( $fileInDir, 0, strlen( $filePrefix ) ) === $filePrefix ) {
						        $removeFile = $filePath . $fileInDir;
						        unlink( $removeFile );
						        $removedFiles[] = $removeFile;
						        $success        = true;
					        }
				        }
			        }
		        }


		        if ( $success ) {
			        $record->store();
			        echo '{"jsonrpc" : "2.0", "result" : "files ' . implode( ',', $removedFiles ) . ' have been erased"}';
		        } else {
			        $record->store(); //update the record in all cases
			        if ( ! $fileFound ) {
				        echo '{"jsonrpc" : "2.0", "result" : "file ' . $_REQUEST['file'] . ', does not exist in the record."}';
			        } else if ( ! is_file( $fileAskedForRemoval ) ) {
				        echo '{"jsonrpc" : "2.0", "result" : "file ' . $fileAskedForRemoval . ', does not exist."}';
			        } else {
				        echo '{"jsonrpc" : "2.0", "error" : "could not erase file ' . $fileAskedForRemoval . ', access denied ?"}';
			        }
		        }
	        }

	        //if instance of dbForm
            else if(is_subclass_of ($record,'dbForm')) {

	        	$form = $record;

	            $success = false;


	            //process our filepath if the filename has been found in this field
	            $filePath = DOC_ROOT . $mf->getUploadsDir() . get_class($form).DIRECTORY_SEPARATOR;

	            $fileAskedForRemoval = $filePath . $_REQUEST['file'];

	            //now delete our file
	            $filePrefix   = strstr( $_REQUEST['file'], '.', true );
	            $removedFiles = array();

	            //scan the directory for the original file and eventually its resized instances (if it is an image)
	            if ( $directoryContent = scandir( $filePath ) ) {
		            foreach ( $directoryContent as $fileInDir ) {
			            if ( substr( $fileInDir, 0, strlen( $filePrefix ) ) === $filePrefix ) {
				            $removeFile = $filePath . $fileInDir;
				            unlink( $removeFile );
				            $removedFiles[] = $removeFile;
				            $success        = true;
			            }
		            }
	            }

	            if ( $success ) {
		            echo '{"jsonrpc" : "2.0", "result" : "files ' . implode( ',', $removedFiles ) . ' have been erased"}';
	            } else {

		            if ( ! is_file( $fileAskedForRemoval ) ) {
			            echo '{"jsonrpc" : "2.0", "result" : "file ' . $fileAskedForRemoval . ', does not exist."}';
		            } else {
			            echo '{"jsonrpc" : "2.0", "error" : "could not erase file ' . $fileAskedForRemoval . ', access denied ?"}';
		            }
	            }
	        }
        }

    }
    else {

        echo '{"jsonrpc" : "2.0", "error" : "No action specified."}';
    }
    //echo "Error : No action specified.";
}
else {
    echo '{"jsonrpc" : "2.0", "error" : "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}';
}
$mf->db->closeDB();
    //echo "Error : Invalid securityToken supplied.";



