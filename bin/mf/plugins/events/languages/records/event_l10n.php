<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'tab_event' => 'Properties',
        'title' => 'Event title',
        'text' => 'Event text',
        'image' => 'Associated image (60x60px)',
        'category' => 'Category',

        /*mandatory records*/
        'record_name' => 'event',
        'new_record' => 'New event',

    ),

    'fr' => array(
        'tab_event' => 'Propriétés',
        'title' => 'Titre de l\'évènement',
        'text' => 'Texte de l\'évènement',
        'image' => 'Image associée (60x60px)',
        'category' => 'Catégorie',

        /*mandatory records*/
        //'no_record' => 'Il n\'existe pas d\'évènement pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=events&action=create">créer un évènement</a>.',
        'record_name' => 'évènement',
        'new_record' => 'Nouvel évènement',

    ),
);

?>