<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow portfolio",
        'desc' => "<p>The portfolio plugin provides records for storing portfolio pictures and display them on a website, organized by categories.</p><p>This plugin, created in the early stages of Mindflow has not been tested for some time and may require some adaptations.</p>"


    ),


    'fr' => array(
        'name' => "Portfolio MindFlow",
        'desc' => "<p>Le plugin Portfolio fournit un type d'enregistrement pour stocker et afficher des images de portfolio sur un site.</p><p>Ce plugin, créé aux débuts de MindFlow, n'a pas été testé depuis longtemps et pourrait requerrir des adaptations</p>"


    ),
);

?>