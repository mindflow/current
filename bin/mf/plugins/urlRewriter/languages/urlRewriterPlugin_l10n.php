<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow URL rewriting and analysis",
        'desc' => "<p>Provides rewrited URL generation, and URL analysis for retreiving the called page.</p>"

    ),


    'fr' => array(
        'name' => "Réécriture d'URL et analyse MindFlow",
        'desc' => "<p>Fournit la génération d'URL réécrites et l'analyse d'URL pour récupérer la page appelée par l'internaute.</p>"

    ),
);

?>