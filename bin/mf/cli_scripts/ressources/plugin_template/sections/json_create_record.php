<?php

if($_REQUEST['action'] == 'create{listedRecordsClassName}')
{
    $user = new {recordClassName}();

    //remove useless keys
    $user->showInEditFormRemoveField('tab_main',array('creation_date', 'modification_date'));

    $html = $mf->formsManager->editRecord($user, $this->pluginName, '', true, true,false,true);

    die('{"jsonrpc" : "2.0", "error" : '.json_encode($html).'}');
}