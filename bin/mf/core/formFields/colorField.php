<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Defines a color picker based on bootstrap 3 colorpicker
 */
class colorField implements formField
{
    /**
     * Returns the HTML code for displaying the field, initialized with the data supplied in $fieldData
     * @param $f fieldData : this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
     * $f->formID           // value of the id attribute of the <form> tag this field is included in
     * $f->mode             // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
     * $f->dataType         // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
     * $f->key              // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
     * $f->fieldData        // this is the data array of the current field
     * $f->recordUid        // the uid (Unique ID) of the record containing the current field
     * $f->recordClassName  // the class name of the record containing the current field
     * $f->record           // all the data for the record containing the current field
     * $f->microKey         // field name when processing a microTemplate field (only used when mode=="micro|")
     * $f->microIndex       // sorting index when processing a microTemplate field (only used when mode=="micro|")
     * @returns String a string containing the HTML for displaying the field
     */
    static public function displayField($f)
    {
        global $mf;

        $mf->formsManager->addCss('colorField','<link rel="stylesheet" href="'.SUB_DIR.'/mf_librairies/colorpicker/css/colorpicker.css">
            <style type="text/css">
            .colorpicker-saturation{background-image:url('.SUB_DIR.'/mf_librairies/colorpicker/img/saturation.png);}
            .colorpicker-hue{background-image:url('.SUB_DIR.'/mf_librairies/colorpicker/img/hue.png);}
            .colorpicker-alpha{background-image:url('.SUB_DIR.'/mf_librairies/colorpicker/img/alpha.png);}
            </style>');
        $mf->formsManager->addBottomJs('colorField','<script src="'.SUB_DIR.'/mf_librairies/colorpicker/js/bootstrap-colorpicker.js"></script>');


        $html = array();
        $fieldAttr = formsManager::getTagAttributes('field',$f->key, $f->fieldData);
        $divAttr = formsManager::getTagAttributes('div',$f->key, $f->fieldData);

        //display the label in front of the field or not
        $showLabel      = (isset($f->fieldData['showLabel']))?      $f->fieldData['showLabel']      :true;

        //display a <div> around the field or not
        $showFieldDiv   = (isset($f->fieldData['showFieldDiv']))?   $f->fieldData['showFieldDiv']   :true;

        //display custom HTML in front of the field
        $preFieldHTML   = (isset($f->fieldData['preFieldHTML']))?   $f->fieldData['preFieldHTML']   :'';

        //display custom HTML in after the field
        $postFieldHTML  = (isset($f->fieldData['postFieldHTML']))?  $f->fieldData['postFieldHTML']  :'';

        //display custom HTML in front of the field
        $preDivHTML   = (isset($f->fieldData['preDivHTML']))?   $f->fieldData['preDivHTML']   :'';

        //display custom HTML in after the field
        $postDivHTML  = (isset($f->fieldData['postDivHTML']))?  $f->fieldData['postDivHTML']  :'';

        if(!isset($f->dataType[1])) $f->dataType[1]='hex';

        if($showLabel)$html[] = formsManager::getFieldLabel($f->fieldData, $f->mode, $f->key, $f->recordClassName);

        if(!isset($f->fieldData['editable']))$f->fieldData['editable']=true;
	    $isEditable = filterBoolean($f->fieldData['editable']);

        if(!$isEditable){
            //read only record display, no input editing
            $html[] = $preDivHTML;
            $html[] = ($showFieldDiv)?'<div class="'.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
            $html[] = $preFieldHTML;
            $html[] = '<span id="noedit|'.$f->key.'" class="noedit '.$fieldAttr['classes'].'" '.$fieldAttr['attributes'].'>'.formsManager::preDisplay($f->key,$f->fieldData['value'],$f->fieldData).'</span>';
            $html[] = $postFieldHTML;
            $html[] = '<input type="hidden" value="'.$f->fieldData['value'].'" name="'.$f->mode.$f->key.'" id="'.$f->mode.$f->key.'"/>';
            $html[] = ($showFieldDiv)?'</div>':'';
            $html[] = $postDivHTML;
        }
        else {
            //input editing
            $html[] = $preDivHTML;
            $html[] = ($showFieldDiv)?'<div class="'.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
            $html[] = $preFieldHTML;
            $html[] = '<input type="text" value="'.formsManager::preDisplay($f->key,$f->fieldData['value'],$f->fieldData).'" name="'.$f->mode.$f->key.'" id="'.$f->mode.$f->key.'" data-color-format="'.$f->dataType[1].'" class="colorpicker form-control input-micro '.$fieldAttr['classes'].'" '.$fieldAttr['attributes'].'/>';
            $html[] = $postFieldHTML;
            $html[] = ($showFieldDiv)?'</div>':'';
            $html[] = $postDivHTML;
            $html[] = '<span class="errorMsg col-lg-offset-4 col-lg-8"></span>';

            $html[] = '<script>$("#'.$f->mode.$f->key.'").colorpicker();</script>';
        }
        if(isset($f->fieldData['noClearFix']) and $f->fieldData['noClearFix'] == '1');
        else $html[] = "<div class='clearfix'></div>";

        return implode(chr(10),$html);



    }


    /**
     * Process the field value prior saving it to the database, whenever inner value adaptation is required, for SQL strict compliance for example.
     * This is executed after 'preRecord' processing functions.
     * @param $fieldData this is the data array of the current field
     * @param $value this is the submited value
     * @return mixed
     */
    static public function onSave($fieldData, $value){
        return $value;
    }

    static public function getCSS(){return "";}
    static public function getTopJS(){return "";}
    static public function getBottomJS(){return "";}







}
