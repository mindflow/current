<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class FullSync extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_sync_full';

    static $createTableSQL= "
        #classe et données concernée
        `local_table` varchar(30) DEFAULT '',
`local_data` longblob,
        
        #nom de la règle d'envoi appliquée
        `out_rule` varchar(30) DEFAULT '',
        
        #système cible
		`destination` varchar(1024) DEFAULT '',
        
        #etat et résultat de l'opération
        `state` varchar(10) DEFAULT 'stopped',
        `result` varchar(10) DEFAULT '',
        
        #log des opérations
        `local_log` longtext,
        `remote_log` longtext,
        
        #schedule
        name varchar(128) default NULL,
        enabled tinyint(1) NOT NULL DEFAULT '1',
        minute varchar(2) default NULL,
        hour varchar(2) default NULL,
        dom varchar(2) default NULL,
        dow varchar(1) default NULL,
        exec_asap tinyint(1) NOT NULL DEFAULT '0',
		time_last_fired datetime default NULL,
		
		#time_interval int(11) default NULL,
		#fire_time int(11) NOT NULL default '0',
		#currently_running BOOLEAN NOT NULL DEFAULT '0', 
    ";

    static $createTableKeysSQL="
          KEY `k_local_table` (`local_table`),
          KEY `k_time_last_fired` (time_last_fired)
        ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init()
    {
	    parent::init();

	    global $mf,$l10n;

	    $this->data = array_merge($this->data, array(

		    'local_table' => array(
			    'value' => '',
                'dataType' => 'select',
                'possibleValues' => self::listTableNames(),
                'valueType' => 'text',
                    'div_attributes' => array(
				    'class' => 'col-lg-4'
			    ),
			    'validation' => array(
				    'mandatory' => '1',
				    'fail_values' => array('','0'),
			    ),
		    ),
		    'local_data' => array(
			    'value' => '',
			    'dataType' => 'textarea 30 5',
			    'valueType' => 'text',
			    'div_attributes' => array('class' => 'col-md-8'),
			    'noEcho' => 1,
		    ),
		    'out_rule' => array(
			    'value' => '',
			    'dataType' => 'select',
			    'possibleValues' => self::listOutRules(),
			    'valueType' => 'text',
			    'div_attributes' => array(
				    'class' => 'col-lg-3'
			    ),
			    'validation' => array(
				    'mandatory' => '1',
				    'fail_values' => array('','0'),
			    ),
		    ),
		   'destination' => array(
			    'value' => '',
			    'dataType' => 'input',
			    'valueType' => 'text',
			    'div_attributes' => array(
				    'class' => 'col-lg-4'
			    ),
			    'editable' => false,
		    ),
		    'state' => array(
			    'value' => 'Inactive',
			    'dataType' => 'select',
			    'possibleValues' => array(
				    'Scheduled' => $l10n->getLabel('FullSync','Scheduled'),
				    'Sending' => $l10n->getLabel('FullSync','Sending'),
				    'Inactive' => $l10n->getLabel('FullSync','Inactive'),
			    ),
			    'field_attributes' => array(
				    'class' => 'input-md',
			    ),
			    'div_attributes' => array('class' => 'col-md-2'),
			    'editable' => false,
		    ),
		    'result' => array(
			    'value' => '',
			    'dataType' => 'select',
			    'possibleValues' => array(
				    '' => '',
				    'Success' => $l10n->getLabel('FullSync','Success'),
				    'Failure' => $l10n->getLabel('FullSync','Failure'),
			    ),
			    'field_attributes' => array(
				    'class' => 'input-md',
			    ),
			    'div_attributes' => array('class' => 'col-md-2'),
			    'editable' => false,
		    ),
		    'local_log' => array(
			    'value' => '',
			    'dataType' => 'textarea 50 8',
			    'valueType' => 'text',
			    'div_attributes' => array('class' => 'col-md-8'),
			    //'editable' => false,
		    ),
		    'remote_log' => array(
			    'value' => '',
			    'dataType' => 'textarea 50 8',
			    'valueType' => 'text',
			    'div_attributes' => array('class' => 'col-md-8'),
			    //'editable' => false,
		    ),
		    'name' => array(
			    'value' => '',
			    'dataType' => 'input',
			    'valueType' => 'text',
			    'validation' => array(
				    'mandatory' => '1',
				    'fail_values' => array('')
			    ),
			    'field_attributes' => array(
				    'class' => 'input-md',
			    ),
			    'div_attributes' => array('class' => 'col-md-4'),
		    ),
		    'enabled' => array(
			    'value' => '1',
			    'dataType' => 'checkbox',
			    'valueType' => 'boolean',
			    'swap' => 0
		    ),
		    'minute' => array(
			    'value' => '*',
			    'dataType' => 'input',
			    'valueType' => 'text',
			    'validation' => array(
				    'mandatory' => '1',
				    'filterFunc' => 'FullSync::validateMinute'
			    ),
			    'field_attributes' => array(
				    'class' => 'input-md',
			    ),
			    'div_attributes' => array('class' => 'col-md-1'),
		    ),
		    'hour' => array(
			    'value' => '22',
			    'dataType' => 'input',
			    'valueType' => 'text',
			    'validation' => array(
				    'mandatory' => '1',
				    'filterFunc' => 'FullSync::validateHour'
			    ),
			    'field_attributes' => array(
				    'class' => 'input-md',
			    ),
			    'div_attributes' => array('class' => 'col-md-1'),
		    ),
		    'dom' => array(
			    'value' => '*',
			    'dataType' => 'input',
			    'valueType' => 'text',
			    'validation' => array(
				    'mandatory' => '1',
				    'filterFunc' => 'FullSync::validateDom'
			    ),
			    'field_attributes' => array(
				    'class' => 'input-md',
			    ),
			    'div_attributes' => array('class' => 'col-md-1'),
		    ),
		    'dow' => array(
			    'value' => '*',
			    'dataType' => 'input',
			    'valueType' => 'text',
			    'validation' => array(
				    'mandatory' => '1',
				    'filterFunc' => 'FullSync::validateDow'
			    ),
			    'field_attributes' => array(
				    'class' => 'input-md',
			    ),
			    'div_attributes' => array('class' => 'col-md-1'),
		    ),
		    'exec_asap' => array(
			    'value' => '0',
			    'dataType' => 'checkbox',
			    'valueType' => 'boolean',
			    'swap' => 0
		    ),
		    'time_last_fired' =>  array(
			    'value' => '',
			    'dataType' => 'datetime',
			    'valueType' => 'datetime',
			    'div_attributes' => array(
				    'class' => 'col-lg-3'
			    ),
			    'editable' => false,
		    ),


	    ));


	    $this->showInEditForm = array(

		    'tab_main'=>array(
			    'name',
			    'enabled',
			    'destination',
			    'local_table','out_rule',//'destination',
			    'minute',
		        'hour',
		        'dom',
		        'dow',
		        'exec_asap',
				'time_last_fired',
			    'state','result',
			    'local_log','remote_log',
		    )
	    );


	    static::$postFormHTML[] = "<script></script>";

    }

    function preStore() {
	    global $config, $l10n;

	    $syncConfig = $config['plugins']['table_sync'];

    	//when the backend user saves the record with exec_asap ticked, we clear time_last_fired so the FullSync is run on next execution
	    if($this->data['enabled']['value'] == '1' && $this->data['exec_asap']['value'] == '1') {
		    if ( $this->data['time_last_fired']['value'] != null ) {
			    $this->data['time_last_fired']['value'] = null;
			    $this->data['state']['value'] = 'Scheduled';
			    $this->data['result']['value'] = '';
		    }
	    }

	    // set the destination field of this record immediately,
	    // otherwise its value remains the former value and may be invalid when displayed in the FullSyncManager showRecordEditTable
	    $ruleName = $this->data['out_rule']['value'];

	    if ( isset( $syncConfig['out_rules'][ $ruleName ] ) ) {
		    $rule = $syncConfig['out_rules'][ $ruleName ];
		    $destinations = $syncConfig['out_rules'][$ruleName]['destinations'];

		    if(is_array($destinations)){
			    $this->data['destination']['value'] = implode(',',$destinations);
		    }
		    else {
			    //empty destination specified, failure to send object
			    $this->data['destination']['value'] = $l10n->getLabel('SyncManager','empty_destination');
		    }
	    }





    }


	/**
	 * Quickly updates a column value in the database without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $name the column name in the database
	 * @param String $value the value to set
	 */
	function setField($name,$value){
		global $pdo,$logger;

		try{
			$sql = "UPDATE " . self::getTableName() . " SET ".$name."=" . $pdo->quote( $value ) . " WHERE uid=" . $this->data['uid']['value'] . ";";
			$pdo->query( $sql );

			$this->data[$name]['value'] = $value;
		}
		catch(PDOException $e){
			$logger->critical("FullSync setField PDOException code=".$e->getCode()." message=".$e->getMessage());
		}

	}

	/**
	 * Quickly updates the local_log field without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $logMessage text of the log entry
	 */
	function addToLocalLog($logMessage){
		global $pdo;

		$now = new DateTime('NOW');
		$nowStr = $now->format('d/m/Y H:i:s');

		$this->data['local_log']['value'] .= $nowStr.' : '.$logMessage;
		$sql = "UPDATE " . self::getTableName() . " SET local_log=" . $pdo->quote($this->data['local_log']['value']) . " WHERE uid=" . $this->data['uid']['value'] . ";";
		$pdo->query( $sql );
	}

	static function getState($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;

			$sec = getSec(array('action' => 'replayFull', 'uid' => $row['uid']));
			$resetSec = getSec(array('action' => 'resetState', 'uid' => $row['uid'], 'record_class' => 'FullSync'));

			switch($value){
				case 'Inactive':
					return '<span id="state_'.$row['uid'].'"><span class="darkgrey">'.$l10n->getLabel('FullSync','Inactive').' &nbsp;<a onclick="replay('.$row['uid'].',\''.$sec->hash.'\');" class="play" title="'.$l10n->getLabel('FullSync','replay').'"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a></span></span>';

				case 'Sending':
					return '<span id="state_'.$row['uid'].'"><span class="orange">'.$l10n->getLabel('FullSync','Sending').'</span> &nbsp;<img src="'.SUB_DIR.'/mf/backend/templates/mf-default/img/small-loading.gif" />&nbsp;<a onclick="resetState('.$row['uid'].',\'FullSync\',\''.$resetSec->hash.'\');" class="play" title="'.$l10n->getLabel('FullSync','reset').'"><span class="glyphicon glyphicon-stop" aria-hidden="true"></span></a></span>';

				case 'Scheduled':
					return '<span id="state_'.$row['uid'].'"><span class="blue">'.$l10n->getLabel('FullSync','Scheduled').' &nbsp;<a onclick="replay('.$row['uid'].',\''.$sec->hash.'\');" class="play" title="'.$l10n->getLabel('FullSync','replay').'"><span></span>';
			}
		}
		else return '';
	}

	static function getResult($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;
			switch($value){
				case 'Success':
					return '<span id="result_'.$row['uid'].'"><span class="green">'.$l10n->getLabel('FullSync','Success').'</span></span>';

				case 'Failure':
					return '<span id="result_'.$row['uid'].'"><span class="red">'.$l10n->getLabel('FullSync','Failure').'</span></span>';
			}
		}
		else return '';
	}


	static function getLocalUid($key,$value,$locale,$row){
		if($value != '*' && $value != ''){
			$uids = explode(',',$value);
			if(count($uids) > 3) {
				return $uids[0].','.$uids[1].','.$uids[2].','.'...';
			}
			else return $value;
		}
		else return $value;
	}

	static function listTableNames(){
		global $config,$pdo;
		$sql = "SELECT table_name FROM information_schema.tables where table_schema='".$config['db_name']."';";
		$stmt = $pdo->query( $sql );
		$rows = $stmt->fetchAll( PDO::FETCH_ASSOC );

		$names = array();
		$names['0'] = '';
		foreach ($rows as $row){
			$names[$row['table_name']] = $row['table_name'];
		}

		return $names;
	}
/*
	static function listDestinations(){
		global $config;

		$destinations = array();
		$destinations['0'] = '';
		foreach ($config['plugins']['table_sync']['remote_hosts'] as $hostName => $hostDef){
			$destinations[$hostName] = $hostName;
		}
		return $destinations;
	}
*/
	static function listOutRules(){
		global $config;

		$rules = array();
		$rules['0'] = '';
		foreach ($config['plugins']['table_sync']['out_rules'] as $ruleName => $ruleDef){
			$rules[$ruleName] = $ruleName;
		}
		return $rules;
	}


	static function validateMinute($fieldValue, $fullSubmitedRecord){
		global $l10n;
		//vérification de la valeur du champ courant
		if($fieldValue == '*' || is_numeric($fieldValue)){
			if($fieldValue == '*' || ($fieldValue >= 0 && $fieldValue < 60))return true;
			else return false;
		}
		else return $l10n->getLabel('FullSync','not_numeric');
	}

	static function validateHour($fieldValue, $fullSubmitedRecord){
		global $l10n;
		//vérification de la valeur du champ courant
		if($fieldValue == '*' || is_numeric($fieldValue)){
			if($fieldValue == '*' || ($fieldValue >= 0 && $fieldValue < 24))return true;
			else return false;
		}
		else return $l10n->getLabel('FullSync','not_numeric');
	}

	static function validateDom($fieldValue, $fullSubmitedRecord){
		global $l10n;
		//vérification de la valeur du champ courant
		if($fieldValue == '*' || is_numeric($fieldValue)){
			if($fieldValue == '*' || ($fieldValue >= 1 && $fieldValue <= 31))return true;
			else return false;
		}
		else return $l10n->getLabel('FullSync','not_numeric');
	}

	static function validateDow($fieldValue, $fullSubmitedRecord){
		global $l10n;
		//vérification de la valeur du champ courant
		if($fieldValue == '*' || is_numeric($fieldValue)){
			if($fieldValue == '*' || ($fieldValue >= 1 && $fieldValue <= 7))return true;
			else return false;
		}
		else return $l10n->getLabel('FullSync','not_numeric');
	}

	static function checkTime($row,$dateNOW){
		global $logger;

		if ( $row['minute'] == '*' ) {
			//$logger->debug(" m*");
			//if the given minute is *, execute if other temporal criteria match
			self::checkHour($row,$dateNOW);

		} else if ( $row['minute'] > 0 ) {
			//$logger->debug(" m".$row['minute']);

			if($row['minute'] == $dateNOW->format('i')) {
				//if the given minute matches current minute, execute if other temporal criteria match
				self::checkHour($row,$dateNOW);
			}
		}
	}


	static function checkHour($row,$dateNOW){
		global $logger;

		if ( $row['hour'] == '*' ) {
			//$logger->debug(" h*");
			//if the given hour is *, execute if other temporal criteria match
			self::checkDay($row,$dateNOW);
		}
		else if ( $row['hour'] > 0 ) {
			//$logger->debug(" h".$row['hour'] ." vs ".$dateNOW->format('H'));

			if($row['hour'] == $dateNOW->format('H')) {
				//if the given hour matches current hour, execute if other temporal criteria match
				self::checkDay( $row, $dateNOW );
			}

		}
	}


	static function checkDay($row,$dateNOW){
		global $logger, $pdo;

		if ( $row['dom'] == '*' ) {
			//$logger->debug(" dom*");
			//if the given day of month is *, check day of week
			if($row['dow'] == '*'){
				//$logger->debug(" dow*");
				//if the day of week is *, execute
				$pdo->query( "UPDATE " . FullSync::$tableName . " SET exec_asap=1 WHERE uid=".$row['uid'] );
				$logger->debug( "fullSync uid=".$row['uid']." SET exec_asap=1" );
			}
			else if($row['dow'] > 0) {
				//if the day of week is specified, check it and execute if there is a match
				//$logger->debug( " dow" . $row['dow']." vs ".$dateNOW->format('N') );
				if($row['dow'] == $dateNOW->format('N')) {
					$pdo->query( "UPDATE " . FullSync::$tableName . " SET exec_asap=1 WHERE uid=".$row['uid'] );
					$logger->debug( "fullSync uid=".$row['uid']." SET exec_asap=1" );
				}
			}
		}
		else if ( $row['dom'] > 0 ) {
			//$logger->debug(" d".$row['dom'].' vs '.$dateNOW->format('j'));
			//if the given day of month  matches current month's day, execute
			if($row['dom'] == $dateNOW->format('j') ) {
				$pdo->query( "UPDATE " . FullSync::$tableName . " SET exec_asap=1 WHERE uid=".$row['uid'] );
				$logger->debug( "fullSync uid=".$row['uid']." SET exec_asap=1" );
			}
		}
	}

	/**
	 * Checks the database for the fullSync tasks list and runs those required.
	 * This function must be called every minute by a cron task / mindflow's cron.php file
	 * When a fullSync is due to be executed, its flag exec_asap is set to 1, then any further execution of the cron task will attemp to run the fullSync until the flag resets to 0;
	 * Only 1 fullSync task will be executed per cron run
	 * This behavior gives a better resilience when long syncs on big tables may prevent other fullSync entries to execute properly due to script execution timeout or database wait timeout
	 *
	 */
	static function fireSync() {


		global $pdo, $logger, $config;

		$logger->debug( 'fullSync::fireSync()' );

		//a fullSync will not be re-evaluated for execution unless it has been executed more than $time_window seconds ago or has his exec_asap flag set to 1
		$time_window = 1000;

		$time_and_window = time() - $time_window;

		$dateNOW = new DateTime( 'NOW' );
		//$dateNOW = $date->format('Y-m-d H:i:s');

		$date      = new DateTime();
		$dateCheck = $date->setTimestamp( $time_and_window );

		//$logger->debug("datecheck = ".$dateCheck->format('Y-m-d H:i:s'));

		//Analyze what entries are due to be executed and set their flag exec_asap to 1
		$sql  = "SELECT * FROM " . FullSync::$tableName . " WHERE state != 'Sending' AND deleted=0 AND enabled=1 AND (time_last_fired IS NULL OR time_last_fired <= '" . $dateCheck->format( 'Y-m-d H:i:s' ) . "')";
		$stmt = $pdo->query( $sql );

		$logger->debug( $sql );

		if ( $stmt->rowCount() > 0 ) {

			$rows = $stmt->fetchAll( PDO::FETCH_ASSOC );

			foreach ( $rows as $row ) {
				//checks which fullSyncs are scheduled for run and set their flag exec_asap to 1
				self::checkTime( $row, $dateNOW );
			}
		}

		//Now run the fullSyncs exec_asap=1, with only one fullSync per run
		$sql  = "SELECT * FROM " . FullSync::$tableName . " WHERE state != 'Sending' AND deleted=0 AND exec_asap = 1 ORDER BY time_last_fired ASC LIMIT 1";
		$stmt = $pdo->query( $sql );

		$logger->debug( $sql );
//debug_print_backtrace();
		$logger->debug( "FullSync fireSync() found " . $stmt->rowCount() . " rows to process" );

		if ( $stmt->rowCount() > 0 ) {

			$rows = $stmt->fetchAll( PDO::FETCH_ASSOC );

			foreach ( $rows as $row ) {
				FullSync::execSync( $row );
			}

		}



	}


	/**
	 * Effectively does the synchronization right now thru SyncManager::syncTable
	 * @param array $row
	 */
	static function execSync(Array $row){
		global $pdo, $logger,$mf;

		$logger->debug("FullSync::execSync() : Executing Fullsync task ".$row['name']." right now");

		//update time_last_fired for FullSync object
		$date = new DateTime('NOW');
		$dateNOW = $date->format($GLOBALS['site_locales'][$mf->getLocale()]['php_datetime_format']);

		//read the FullSync info
		$fullSync = new FullSync();
		$fullSync->load($row['uid']);

		//update the status fields
		$fullSync->data['state']['value'] = 'Sending';
		//reset the exec_asap even before executing the script, so if the script fails, it doesn't re-execute and fail every minute
		$fullSync->data['exec_asap']['value'] = 0;
		$fullSync->data['result']['value'] = '';
		$fullSync->data['time_last_fired']['value'] = $dateNOW;
		$fullSync->data['local_log']['value'] = '';
		$fullSync->data['remote_log']['value'] = '';

		//desactivating time_last_fired reset on manual store
		$fullSync->enablePreStore=false;

		$fullSync->store();

		//do the sync
		$out = SyncManager::syncTable($row['local_table'],$row['out_rule']);

		//processing result for log and display
		$fullSync->data['state']['value'] = $out->data['state']['value'];
		$fullSync->data['result']['value'] = $out->data['result']['value'];
		$fullSync->data['local_log']['value'] = $out->data['local_log']['value'];
		$fullSync->data['remote_log']['value'] = $out->data['remote_log']['value'];
		$fullSync->data['destination']['value'] = $out->data['destination']['value'];

		$status = $fullSync->store();
	}




}















