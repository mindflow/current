<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow web pages editing and templating system",
        'desc' => "<p>Provides web pages editing in backend and rendering in frontend. Supplies the {marker} based templating system at the heart of MindFlow</p>"


    ),


    'fr' => array(
        'name' => "Pages web et système de gabarits MindFlow",
        'desc' => "<p>Fournit la fonctionnalité d'édition de pages en backend et leur rendu en frontend. Fournit le système de gabarits basé sur les {marqueurs} qui est au coeur de MindFlow</p>"


    ),
);

?>