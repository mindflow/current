<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';

//load test payment configuration if current_user is logged into the backend or if test mode is forced
//allows testing using simulated payments rather than real CB
if(isset($_SESSION['current_be_user']) || $GLOBALS['config']['plugins']['monetico']['forceTestPayments']==1){
    if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['test_config_file'];
}
else {
    if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['prod_config_file'];
}

class moneticoCapture extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_monetico_capture';
    static $oneTablePerLocale = true;

    //see document /monetico/ressources/doc/Monetico_Paiement_documentation_technique_v1_0e.pdf page 23 for field specification
    static $createTableSQL= "
        `type_paiement` int(10) unsigned NOT NULL DEFAULT '0',
        `version` decimal(2,1) unsigned NOT NULL DEFAULT '3.0',
        `tpe` int(11) unsigned NOT NULL DEFAULT '0',
        `date` datetime NOT NULL,
        `devise` varchar(3) ,
        `montant` varchar(13) ,
        `reference` varchar(50) ,
        `texte-libre` text ,
        `lge` varchar(2) ,
        `societe` varchar(30) ,
        `MAC` varchar(40) ,

        #Capture sur paiement différé / partiel / fractionné / récurrent
        `date_commande` date NOT NULL,
        `montant_a_capturer` varchar(13) ,
        `montant_deja_capture` varchar(13) ,
        `montant_restant` varchar(13) ,
        `stoprecurrence` boolean DEFAULT '0',
        `phonie` varchar(255) default NULL,

    ";

    static $createTableKeysSQL="
          KEY `k_mac` (`MAC`),
          KEY `k_date` (`date_commande`),
          KEY `k_type` (`type_paiement`),
          KEY `k_montant1` (`montant`),
          KEY `k_montant2` (`montant_a_capturer`),
          FULLTEXT KEY `vz_demande_search` (`reference`,`texte-libre`)
    ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init(){

        parent::init();


        $this->data = array_merge($this->data,array(
            'type_paiement' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'fractionné'    => 'Fractionné',
                    'différé'       => 'Différé',
                    'partiel'       => 'Partiel',
                    'récurrent'     => 'Récurrent',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),

            //type paiement : classique, fractionné, différé, partiel, récurrent

            'version'    =>  array(
                'value' => MONETICOPAIEMENT_VERSION,
                'dataType' => 'input',
                'valueType' => 'number',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),

            'tpe'   => array(
                'value' => MONETICOPAIEMENT_EPTNUMBER,
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'date'   =>  array(
                'value' => '',
                'dataType' => 'datetime',
                'valueType' => 'datetime',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),

            'devise' => array(
                'value' => 'EUR',
                'dataType' => 'select',
                'possibleValues' => array(
                    'EUR'    => 'EUR',
                    'GBP'       => 'GBP',
                    'USD'       => 'USD',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
            ),

            'montant'   => array(
                'value' => '0.00',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),



            'reference'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'texte-libre'   => array(
                'value' => '',
                'dataType' => 'textarea 50 4',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'lge'   => array(
                'value' => 'FR',
                'dataType' => 'select',
                'possibleValues' => self::listLanguages(),
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),

            'societe'   => array(
                'value' => MONETICOPAIEMENT_COMPANYCODE,
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'MAC'   => array(
                'value' => '',
                'dataType' => 'hidden',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
                'editable' => false
            ),



            //Capture sur paiement différé / partiel / fractionné / récurrent

            'date_commande'   =>  array(
                'value' => '',
                'dataType' => 'date',
                'valueType' => 'date',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),

            'montant_a_capturer'   => array(
                'value' => '0.00',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'montant_deja_capture'   => array(
                'value' => '0.00',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'montant_restant'   => array(
                'value' => '0.00',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'stoprecurrence'   => array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'div_attributes' => array('class' => 'col-lg-3'),
                'swap' => '0'
            ),

            'phonie'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),


        ));






        $this->showInEditForm = array(
            'tab_main'=>array(
                'parent_uid','type_paiement','version','tpe','date','devise','montant','reference','texte-libre','lge','societe','MAC','date_commande','montant_a_capturer','montant_deja_capture','montant_restant','stoprecurrence','phonie',
                'protocole',
            ),
        );



        //static::$postFormHTML[] = '<script>';
        //static::$postFormHTML[] = "</script>";


    }





    static function listLanguages(){
        return array(
            'FR' => 'FR',
            'EN' => 'EN',
            'DE' => 'DE',
            'IT' => 'IT',
            'ES' => 'ES',
            'NL' => 'NL',
            'PT' => 'PT',
            'SV' => 'SV',
        );
    }
/*
    function getCheckboxStateColored($key, $value, $locale, $row){
        $l10n = $GLOBALS['mf']->l10n;
        $display = '';
        switch($value){
            case '0':
                $display = '<span class="mf_deleted">'.$l10n->getLabel('vzDemandeCatalogue','checkbox_0').'</span>';
                break;
            case '1':
                $display = '<span class="mf_active">'.$l10n->getLabel('vzDemandeCatalogue','checkbox_1').'</span>';
                break;
        }
        return $display;
    }

    function getCheckboxState($key, $value, $locale, $row){
        $l10n = $GLOBALS['mf']->l10n;

        return $l10n->getLabel('vzDemandeCatalogue','checkbox_'.$value);

    }
*/


}















