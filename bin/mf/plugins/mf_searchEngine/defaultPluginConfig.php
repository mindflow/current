<?php

//default configuration for the plugin
//add and edit these values in your config file

$config['plugins']['mf_searchEngine'] = array(
    'results-per-page' => '10',
    'search-page-uid' => array(
        'fr' => '1',
        'en' => '1',
        'de' => '1',
    ),
    'indexClasses' => 'page,newsItem,circuit',
);