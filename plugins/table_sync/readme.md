
This plugin allows exchanging records and/or SQL requests between different MindFlow installations.
It can synchronize single records remotely for each operation (create, edit, delete...) or a whole set of records in one pass.
The data is sent between hosts using JSON files in which the record content is zipped for size optimization and can be synchronized up to once per minute.
If the request is sent over an SSL connexion, the data is also cyphered.
Full synchonizations can also be scheduled at due time.
An IP adress + password check ensures the remote host is allowed to connect.
The plugin configuration file allows to specify which records or which fields inside records should be synchronized.
The plugin must be installed both on local and remote hosts for ensuring communication
