<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

$execution_start = time();

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';


/**
 * Exports the content of a table as a CSV file.
 *
 * It uses the 'valueType' attribute of the form field in order to process the exported data according to its type.
 * By default, all the table values are exported. Some columns can be filtered by specifying their name in the 'skipKeys' attribute.
 * KeyProcessors can be defined for translating the values into a more usable format (such as human readable) before exporting them.
 * The set of values exported is defined by the SQL request defined in the 'sql' attribute.
 */
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();

global $mf,$l10n,$pdo;

if($mf->mode=="backend")$locale = $mf->getBackendLanguage();
else $locale = $mf->getDataEditingLanguage();

if(isset($_REQUEST['sec'])){


    if(isset($_SESSION['actions'])){
        $sec = $_REQUEST['sec'];

        $action = $_SESSION['actions'][$sec]['action'];

        if(isset($_SESSION['actions'][$sec]['filename']))$filename = $_SESSION['actions'][$sec]['filename'];
        else $filename = 'export.csv';

        //we are serving excel csv
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv;charset=UTF-8"');
        header('Content-Disposition: attachment; filename="'.$filename.'"');


        //SQL for collecting all the records that will be exported
        $sql = $_SESSION['actions'][$sec]['sql'];

        //Class defining the record (optional)
        $class = $_SESSION['actions'][$sec]['record_class'];

        //keys from the record that won't be exported
        $skipKeys = $_SESSION['actions'][$sec]['skipKeys'];

        //display or not column names on first row of the export
        $print_column_names = (isset($_SESSION['actions'][$sec]['print_column_names']))?$_SESSION['actions'][$sec]['print_column_names']:false;

        //substitute the specified custom column names in first line instead of extracting the column names of the database
        //this can be usefull to match the column names of a target database which me be different than the column names of the internal database
        $force_column_names = (isset($_SESSION['actions'][$sec]['force_column_names']))?$_SESSION['actions'][$sec]['force_column_names']:'';

        //flag in order to figure if column names have already been printed or not
        $printed_column_names = false;

        $keyProcessors = $_SESSION['actions'][$sec]['keyProcessors'];
        $computeData = (isset($_SESSION['actions'][$sec]['computeData']))?$_SESSION['actions'][$sec]['computeData']:null;

        //here we store our CSV exported lines
        $csvBuffer = "";

        if(isset($action)){
            if($action == 'exportCSV'){

                if(isset($_SESSION['actions'][$sec]['CSV_charset']))$charset = $_SESSION['actions'][$sec]['CSV_charset'];
                else $charset = 'Windows-1252';

                if(isset($_SESSION['actions'][$sec]['CSV_separator']))$separator = $_SESSION['actions'][$sec]['CSV_separator'];
                else $separator = ';';

                //create an instance of the object type in order to get the record definition
                if(trim($class) != '') $record = new $class();

                //query the data to export
                if(trim($sql) != ''){
                    $stmt = $pdo->prepare($sql);

                    try{
                        $stmt->execute(array());
                    }
                    catch(PDOException $e){
                        echo $e->getMessage();
                        return 0;
                    }

                    while($row = $stmt->fetch(PDO::FETCH_ASSOC) ){

                            $line = "";
                            if(!$printed_column_names) {
                                if($print_column_names){
                                    if ($force_column_names == '') {
                                        //affichage du nom de la colonne en première ligne
                                        foreach ($row as $key => $value) {
                                            //on saute les colonnes sans intérêt pour le client
                                            if (in_array($key, $skipKeys)) continue;
                                            //$line .= iconv("UTF-8", "Windows-1252", $key).$separator;
                                            $line .= $key . $separator;
                                        }
                                        if (isset($computeData)) {
                                            foreach ($computeData as $key => $function) {
                                                //$line .= iconv("UTF-8", "Windows-1252", $key).$separator;
                                                $line .= $key . $separator;
                                            }
                                        }
                                        $line = rtrim($line, $separator);
                                        $csvBuffer .= $line . chr(10);
                                    }
                                    else{
                                        //column names forced
                                        $csvBuffer .= $force_column_names . chr(10);
                                    }
                                    $printed_column_names = true;
                                }
                            }
                            $line = "";
                            foreach($row as $key => $value){

                                //on saute les colonnes sans intérêt pour le client
                                if(in_array($key,$skipKeys)) continue;

                                //processing values featuring a keyProcessor function
                                if(isset($keyProcessors[$key])){
                                    //process key for display with optional user supplied processing function.
                                    $value = $keyProcessors[$key]($key,$value,$locale,$row);
                                }
                                //processing regular keys
                                else{
                                    //cas particuliers
                                    $value = str_replace('"','',$value);

                                    $value = trim($value);

                                    if($record && isset($record->data[$key]['valueType'])){

                                        $valueType = $record->data[$key]['valueType'];

                                        if($value!=''){
                                            switch($valueType){
                                                case 'text':
                                                    if($value!='')$value = '"'.$value.'"';
                                                    break;

                                                case 'date':
                                                    try{
                                                        $date = new DateTime($value);
                                                        $value = $date->format($GLOBALS['site_locales'][$locale]['php_date_format']);
                                                    }
                                                    catch(Exception $e){$value = '';}
                                                    break;

                                                case 'datetime':
                                                    try{
                                                        $date= new DateTime($value);
                                                        $value = $date->format($GLOBALS['site_locales'][$locale]['php_datetime_format']);
                                                    }
                                                    catch(Exception $e){$value = '';}
                                                    break;

                                                case 'datetime.ms':
                                                    try{
                                                        $date= new DateTime($value);
                                                        $value = $date->format($GLOBALS['site_locales'][$locale]['php_datetime_ms_format']);
                                                    }
                                                    catch(Exception $e){$value = '';}
                                                    break;

                                                case 'boolean':
                                                    if($value)$value = '1';
                                                    else $value = '0';
                                                    break;

                                                case 'dummy':
                                                    $value = '';
                                                    break;
                                            }
                                        }

                                    }
                                }

                                //conversion du charset de la valeur pour excel
                                //$line .= ''.iconv("UTF-8", "Windows-1252//TRANSLIT", $value).$separator;
                                //$line .= ''.mb_convert_encoding($value, 'Windows-1252', 'UTF-8').$separator;
                                $line .= ''.$value.$separator;
                            }
                            if(isset($computeData)){
                                foreach($computeData as $key => $function){
                                    eval('$value = '.$function.$separator);
                                    //$line .= ''.mb_convert_encoding($value, 'Windows-1252', 'UTF-8').$separator;
                                    $line .= ''.$value.$separator;
                                }
                            }
                            $line = rtrim($line, $separator);
                            $csvBuffer .= $line;

                            $csvBuffer .= chr(10);

                        }

                        if(strtolower($charset) == 'windows-1252') {
                            // Print separator
                            echo 'sep='.$separator.chr(10);
                            // Print CSV data
                            echo mb_convert_encoding($csvBuffer, 'Windows-1252', 'UTF-8') . $separator;
                        }
                        else if(strtolower($charset) == 'utf-8' || strtolower($charset) == 'utf-16') {
                            // Print BOM
                            echo "\xEF\xBB\xBF"; // UTF-8 BOM
                            // Print separator
                            echo 'sep='.$separator.chr(10);
                            // Print CSV data
                            echo mb_convert_encoding($csvBuffer, 'UTF-16LE', 'UTF-8') . $separator;
                        }
                    }

                }
                else echo "error, no SQL specified for CSV exportation. Read the manual, buddy !";
            }
        
    }
    else echo "\$_SESSION['actions'] is not defined and must be defined for proper operation of the CSV exporter.";
}else echo "No sec parameter defined. Please define one.";

$mf->db->closeDB();



