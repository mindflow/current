<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/events/records/event.php';

class events implements module
{
    private $moduleKey = 'events';

    private $moduleType = 'backend,frontend';

    private $action='list';

    var $eventOutput;

    function __construct(){
        //$mf = &$GLOBALS['mf'];

    }


    function prepareData(){
        global $mf,$l10n;
        $this->pageOutput = '';

        if($mf->mode == 'frontend'){

            //$currentPageUid = $mf->info['currentPageUid'];

        }
        else if($mf->mode == 'backend'){

            //add the plugin to the backend menus
            $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=events"><span class="glyphicon glyphicon-menu glyphicon-calendar" aria-hidden="true"></span>'.$l10n->getLabel('events','menu_title').'</a>','datas');


            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                /*
                //process eventual record submission
                $form = $mf->formsManager;
                $form->prepareData();

                if(($currentRecordUid = $form->processForm()) > 0){
                    $this->pageOutput .= '<div class="alert alert-success fade in">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    '.$l10n->getLabel('events','event_recorded').'
                  </div>';
                }*/


                if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                /*
                echo "_REQUEST['action']=".$_REQUEST['action']."<br />".chr(10);
                echo "_GET['uid']=".$_REQUEST['uid']."<br />".chr(10);
                echo "currentRecordUid=".$currentRecordUid."<br />".chr(10);
                */


                //echo "events.php ACTION=".$this->action."<br />";
                switch($this->action){
                    case "list":

                        $this->listRecords();

                        break;

                  /*  case "create":
                        $this->pageOutput .= $this->createEvent();
                        break;

                    case "edit":
                        //case edit requested by some module as a GET url parameter
                        if(isset($_GET['uid']) && ($_GET['uid']!='')){
                            $this->pageOutput .= $this->editEvent($_GET['uid']);
                        }
                        //case we just re-edit the last saved record
                        else {
                            $this->pageOutput .= $this->editEvent($currentRecordUid);
                        }
                        break;


                    case "delete":
                        $this->pageOutput .= $this->deleteEvent($_REQUEST['uid']);
                        break;*/
                }
            }
        }

    }

    function render(&$mainTemplate){
        global $mf,$config;

        if($mf->mode == 'backend'){
            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                $eventsBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/events/ressources/templates/events.html');

                $eventsBody = str_replace("{event}",$this->pageOutput,$eventsBody);

                $mainTemplate = str_replace("{current_module}",$eventsBody,$mainTemplate);
            }
        }
        else if($mf->mode == 'frontend'){
            $html = array();

            if($mf->info['currentPageUid'] == $config['plugins']['events']['uid-page-info-locale']){

                $mainTemplate = str_replace("{plugin-events-col1}",$this->getEvents('escapade', 'uid', 'ASC'),$mainTemplate);
                $mainTemplate = str_replace("{plugin-events-col2}",$this->getEvents('alentours', 'uid', 'ASC'),$mainTemplate);

            }

        }
    }


    function getType(){
        return $this->moduleType;
    }

    function listRecords($sqlConditions = ''){

        $secKeys = array('action' => 'createRecord', 'record_class' => 'event');
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));

        //$buttons = '<button class="btn-sm btn-primary mf-btn-new" type="button" onClick="openRecord(\'{subdir}/mf/core/ajax-core-html.php?action=createRecord&record_classevent&header=0&footer=0&sec='.$secHash.'&fields='.$secFields.'&ajax=1&showSaveButton=1&showSaveCloseButton=1&showPreviewButton=&showCloseButton=1&mode='.$mf->mode.'\',\'70%\',\'\',\''.$l10n->getLabel('event','record_name').'\');">'.$l10n->getLabel('events','new_record').'</button>';

        $event = new event();

        $this->pageOutput .= $event->showRecordEditTable(
            array(
                'SELECT' => 'category, title',
                'FROM' => event::$tableName,
                'JOIN' => '',
                'WHERE' => event::$tableName.".deleted = '0'",
                'ORDER BY' => '',
                'ORDER DIRECTION' => 'DESC',
            ),
            'events',
            '',
            'title',
            null,
            null,
            array(
                'create'=>1,
                'view'=>1,
                'edit'=>1,
                'delete'=>1
            ),
            array(
                'ajaxActions' => 1,
                'showNumRowsPerPageSelector' => true,
                'showResultsCount' => true,
                //'buttons' => $buttons,
            ),
            null,
            'events'
        );
    }

/*
    function createEvent(){
        $mf = &$GLOBALS['mf'];
        $event = new event();
        $form = $mf->formsManager;
        return $form->editRecord($event, $this->moduleKey,  '', true, true, true, true, 1, null, null, 'events');
    }

    function editEvent($uid){
        $mf = &$GLOBALS['mf'];
        $event = new event();
        $event->load($uid);
        $form = $mf->formsManager;
        return $form->editRecord($event, $this->moduleKey, '', true,true,false,true, 1, null, null, 'events');

    }

    function deleteEvent($uid){
        $event = new event();
        $event->load($uid);
        $event->delete();
        return $event->showRecordEditTable(
            array(
                'SELECT' => 'category, title',
                'FROM' => event::$tableName,
                'JOIN' => '',
                'WHERE' => event::$tableName.".deleted = '0'",
                'ORDER BY' => 'uid',
                'ORDER DIRECTION' => 'DESC',
            ),
            'events',
            '',
            'title',
            null,
            null,
            array('view'=>1,'edit'=>1,'delete'=>1),
            array(
                'ajaxActions' => 1,
                'showNumRowsPerPageSelector' => true,
                'showResultsCount' => true,

            ),
            null,
            'events'
        );
    }*/


    function getEvents($category, $sortingField, $sortingDirection){
        global $mf,$pdo;

        //preparing request only once
        $sql = "SELECT * FROM mf_events WHERE deleted = '0' AND category = '".$category."' ORDER BY ".$sortingField." ".$sortingDirection;

        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array());
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }


        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);



        $html = array();

        if($stmt->rowCount() >0){

            foreach($rows as $row){

                $html[] = "<div class=event>";
                $html[] = "<h3>".$row['title']."</h3>";
                $html[] = $row['text'];

                $imagesArray = explode(',', $row['image']);
                $image_path = $mf->getUploadsDir().DIRECTORY_SEPARATOR.'event'.DIRECTORY_SEPARATOR.'event_'.$row['uid'].DIRECTORY_SEPARATOR;

                foreach($imagesArray as $image){
                    if($image != ''){
                        list($width, $height, $type, $attr)= getimagesize(DOC_ROOT.SUB_DIR.$image_path.$image);
                        if(trim($image) != '')$html[] = '<img src="'.$image_path.$image.'" width="'.$width.'" height="'.$height.'" alt="">';
                    }
                }

                $html[] = "</div>";

            }
        }

        return implode(chr(10),$html);
    }
}


