<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'module-title' => 'Portfolio management',
        'no_portfolio' => 'There currently exists no portfolio item. You can <a href="{subdir}/mf/index.php?module=portfolio&action=create&class=portfolioItem">create a portfolio item</a>.',
        'item_recorded' => 'The item has been successfully recorded',
        'portfolio-menu-name' => 'Portfolio editing',
        'list-portfolio' => 'List portfolio',
        'create-portfolio' => 'Add portfolio item',
        'list-categories' => 'List categories',
        'create-category' => 'Add category item',
    ),

    'fr' => array(
        'module-title' => 'Gestion des portfolios',
        'no_portfolio' => 'Il n\'existe pas d\'entrée de portfolio  pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=portfolio&action=create&class=portfolioItem">créer une entrée de portfolio</a>.',
        'item_recorded' => 'L\'entrée a été enregistré avec succès',
        'portfolio-menu-name' => 'Edition du portfolio',
        'list-portfolio' => 'Lister le portfolio',
        'create-portfolio' => 'Ajouter une entrée',
        'list-categories' => 'Lister les catégories',
        'create-category' => 'Ajouter une catégorie',
    ),
);