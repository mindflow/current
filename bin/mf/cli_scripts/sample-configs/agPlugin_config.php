<?php
/**************************
 * Add Plugin settings
 * Fill this section in all cases
 **************************/

//Natural name of the plugin
$pluginName = "AG records";

//Short description
$pluginDesc = "Sample auto-generated plugin";

//Base ISO2 locale for localized files
$defaultLocale = "fr";

//Class name of the plugin
$pluginClassName = "agRecords";

//Folder name of the website where the plugin should be created
$websiteFolderName = "dynamo";

//Folder name / plugin key of the plugin
$pluginDirName = "ag_records";


/**************************
 * Add module settings
 * Fill this section when generating a module
 **************************/

//Natural name of the module
$moduleNaturalName = "Sample module";

//Short description
$moduleDesc = "Sample auto-generated module";

//Class name of the module
$moduleClassName = "agRecordsManager";

//module type. Value should be either : frontend / backend or frontend,backend
$moduleTypeFrontend = 1;
$moduleTypeBackend = 1;
$moduleTypeAuth = 0;



/**************************
 * Add record settings
 * Fill this section if adding a record to an existing module
 **************************/

//Inserted record class name
$recordClassName = 'agRecord';

//SQL table name where the records will be stored
$SQLTableName = 'ag_records';

//Inserted record natural name
$recordNaturalName = 'sample auto-generated record';

//Should the record be listed/edited into some module
$moduleWillListRecords = true;

//Column name of the listed record's title field
$recordTitleField = 'title';

//create record Label
$createRecordLabel = 'Create a sample record';

//edit record label
$editRecordLabel = 'Edit the sample record';


/**************************
 * Add form settings
 * Fill this section if adding a filter form to an existing module
 **************************/

//Displayed record class name
$formClassName = 'agPluginForm';
