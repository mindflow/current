<?php
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';

class Migration implements plugin{

    function setup()
    {

    }

    function getName()
    {
        global $l10n;
        return $l10n->getLabel('Migration','name');
    }

    function getDesc()
    {
        global $l10n;
        return $l10n->getLabel('Migration','desc');
    }

    function getPath()
    {
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey()
    {
        return "migration";
    }


    function getVersion()
    {
        return "1.0";
    }

    function getDependencies()
    {
        $dependencies = array();
        return $dependencies;
    }

    function init()
    {
        global $mf;
        $l10n = $mf->l10n;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');


        //inform MindFlow of the new dbRecords supplied by this plugin
        //{index_load_record}


        //load modules
        //[load_module_MigrationManager]
		$mf->pluginManager->loadModule($relativePluginDir,"modules","MigrationManager", true);
		//[/load_module_MigrationManager]
		//{index_load_module}


        //add editing rights
        //[user_right_MigrationManager]
		if(class_exists('mfUserGroup')) mfUserGroup::defineUserRight('migration', 'allowEditMigrationManager',array(
            'value' => '0',
            'dataType' => 'checkbox',
            'valueType' => 'number',
            'processor' => '',
            'div_attributes' => array('class' => 'col-lg-2'),
        ),
        $relativePluginDir.'/languages/userRights_l10n.php'
    );
		//[/user_right_MigrationManager]
		//{index_add_module_editing_right}



    }
}
$mf->pluginManager->addPluginInstance(new Migration());


