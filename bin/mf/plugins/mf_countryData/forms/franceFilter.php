<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * User: Alban
 * Date: 19/09/13
 * Time: 16:13
 */
require_once DOC_ROOT.SUB_DIR.'/mf/core/dbForm.php';

class franceFilter extends dbForm{

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)

    function init(){

        $this->loadL10n('/mf/plugins/mf_countryData','forms');
        parent::init();

        //columns descriptions, including autogenerated form settings
        $this->data['htmlTableOpen_filter']             = array(
            'value' => '<div class="row"><div class="col-lg-3"><div class="row">',
            'dataType' => 'html',
            'valueType' => 'dummy',
        );

        $this->data['htmlTableHalf_filter']             = array(
            'value' => '</div></div><div class="col-lg-4"><div class="row">',
            'dataType' => 'html',
            'valueType' => 'dummy',
        );

        $this->data['htmlTableClose_filter']             = array(
            'value' => '</div></div></div>',
            'dataType' => 'html',
            'valueType' => 'dummy',
        );




        $this->data['MotClef_filter'] = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'number',
            'div_attributes' => array(
                'class' => 'col-lg-3',
            )
        );

        $this->data['Departement_filter'] = array(
            'value' => '',
            'dataType' => 'select',
            'possibleValues' => mfDepartement::listDepartments(),
            'valueType' => 'number',
            'div_attributes' => array(
                'class' => 'col-lg-3',
            )
        );

        $this->data['Regions_filter'] = array(
            'value' => '',
            'dataType' => 'select',
            'possibleValues' => mfFrance::listRegions(),
            'valueType' => 'number',
            'div_attributes' => array(
                'class' => 'col-lg-3',
            ),
            'field_attributes' => array(
                'onChange' => 'updateDepartements();'
            ),
        );




        $this->showInEditForm = array(
            //'tab_main'=>array('htmlTableOpen_filter', 'MotClef_filter',   'htmlTableHalf_filter','Deleted_filter', 'htmlTableClose_filter' )
            'tab_main'=>array('MotClef_filter','Regions_filter','Departement_filter')
        );

        //form security variables
        $secKeys = array(
            'action' => "getDepartements"
        );
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));


        static::$postFormHTML[] = "<script>
            function updateDepartements(){
                var codeRegion = $('#record\\\\|Regions_filter').val();

                //retreive list of pointDeService
                var jqxhr = $.post('".SUB_DIR."/mf/plugins/mf_countryData/ajax-search-json.php', { action: 'getDepartements', code_region: codeRegion, sec:'".$secHash."', fields:'".$secFields."'})
                .success(function(jsonData) {
                    var optionsAsString = '';
                    items = jsonData.items;
                    for(key in items){
                        optionsAsString += '<option value=\'' + key + '\'>' + items[key] + '</option>';
                    }
                    //update pointDeService select
                    $('#record\\\\|Departement_filter').find('option').remove().end().append($(optionsAsString));

                    //sort alphabetically as javascript is not respecting supplied data order
                    var options = $('#record\\\\|Departement_filter option');
                    if(options.length > 1){
                        var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
                        arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
                        options.each(function(i, o) {
                          o.value = arr[i].v;
                          $(o).text(arr[i].t);
                        });
                    }

                })
                .fail( function(xhr, textStatus, errorThrown) {
                    alert('franceFilter.php updateDepartements() : '+ xhr.responseText);
                })
            }
            </script>";

    }




}
