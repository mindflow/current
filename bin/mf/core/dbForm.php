<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';


/**
 * Manages the content of simple forms.
 *
 * dbForms can be used for creating wizards, sending emails and such operations which do not require immediate database storage when the form is transfered. If you need to record the form data in the database immediately, use a dbRecord subclass.
 *
 */
class dbForm{

    var $data;              // data value
    var $showInEditForm;    // lists the data to show in the edit form by default
    static $postFormHTML=array(); // Array of chunks of HTML code to be displayed after the object's forms
    static $dummyFieldTypes = array('fieldset'); //fields that have no value and should not be inserted in the database (usually page layout fields)
    static $DBEngine = "InnoDB"; //SELECTED MYSQL engine for the inherited class
    static $preRecord='';

    /*
    * Constructeur
    */
    function __construct(){
        $this->init();
    }

    function init(){

        $this->dataType = array();
        $this->dataEditingEnable = array();

        $this->showInEditForm = array();

        $this->data = array(
            //form unique id
            'uid'   =>  array(
                'value' => time(),
                'dataType' => 'input',
                'valueType' => 'number',
            )
        );
    }



    /*
     * Loads the language file for the current class
     * @param $pluginRelativePath the path to the plugin folder
     * @param $subDir (optional) a folder name located below the languages folder, with a resulting path like :  '/mf/plugins/'.$pluginName.'/languages/'.$subDir
     */
    function loadL10n($pluginRelativePath, $subDir=''){
        //load l10n text for inherited record types
        if(!isset($GLOBALS['l10n_text'][get_class($this)])){

            //load the current record's translation files
            include DOC_ROOT.SUB_DIR.$pluginRelativePath.'/languages/'.$subDir.(($subDir!='')?'/':'').get_class($this).'_l10n.php';
            $GLOBALS['l10n_text'][get_class($this)] =  $l10nText;
        }
    }



    /**
     * Outputs the record as html
     * @return string
     */
    function toHTML(){
        $html = array();
        $html[] = "<div>".chr(10);
        foreach($this->data as $key => $item){
            if((isset($this->data[$key]['dataType']) and !in_array($this->data[$key]['dataType'],self::$dummyFieldTypes)))
                $html[] = $key.' = '.$item['value']."<br>".chr(10);
        }
        $html[] = "</div>".chr(10);
        return implode(chr(10),$html);
    }

	/*
	 * Gets executed prior processing the form when it has been submited
	 * inside the formsManager::processForm() function
	 * Allows pre-processing the data in order to alter the data structure before injecting the submited data insite it.
	 */
	function preProcessForm(){}

	/*
	 * Gets executed after processing the form when it has been submited
	 * inside the formsManager::processForm() function.
	 * Replaces function onSubmit() which has become obsolete.
	 */
	function postProcessForm(){}



	/**
	 * Removes the fields specified in array $fieldNames from $tabName in order to prevent their display
	 * @param string $tabName name of the tab containing the field. If the field is displayed prior tabs, set $tabName to ''.
	 * @param array $fieldNames containing the fieldnames to be removed
	 */
	function showInEditFormRemoveField($tabName,$fieldNames){
		$this->showInEditForm[$tabName] = array_diff($this->showInEditForm[$tabName], $fieldNames);
	}

	/**
	 * Inserts the specified fields after the $fieldAfter field in the specified tab. Set $fieldAfter to 0 if you need to perform insertion in the first position.
	 * @param string $tabName name of the tab containing the field. If the field is displayed prior tabs, set $tabName to ''.
	 * @param array $fieldNames containing the fieldnames to be added
	 * @param string $fieldAfter name of the field after which the insertion should be performed. Set $fieldAfter to 0 if you need to perform insertion in the first position.
	 */
	function showInEditFormInsertField($tabName,$fieldNames,$fieldAfter){
		if($fieldAfter === 0) {
			$offset = 0;
		}
		else{
			//seek $fieldAfter's position
			$offset = array_search($fieldAfter, $this->showInEditForm[$tabName]);
		}

		if($offset){
			//insert the elements
			$input = $this->showInEditForm[$tabName];
			$this->showInEditForm[$tabName] = array_splice($input, $offset+1, 0, $fieldNames);
			$this->showInEditForm[$tabName] = $input;
		}
	}

	/**
	 * Appends the specified fields at the end of the specified tab
	 * @param string $tabName name of the tab containing the field. If the field is displayed prior tabs, set $tabName to ''.
	 * @param array $fieldNames containing the fieldnames to be added
	 */
	function showInEditFormAppendField($tabName,$fieldNames){
		foreach($fieldNames as $fieldName) {
			$this->showInEditForm[ $tabName ][] = $fieldName;
		}
	}


	/**
	 * Turns to string a list of the values contained in the form
	 * @return string
	 */
	function __toString(){

		$string = get_class($this).' [ uid='.$this->data['uid']['value'].' ]'.chr(10).'{'.chr(10);

		foreach($this->data as $key => $keyData){
			foreach($keyData as $index => $value){
				if($index == 'value') {
					//ignore dummy fields
					if ((isset($this->data[$key]['dataType']) and !in_array($this->data[$key]['dataType'], self::$dummyFieldTypes))) {
						//ignore keys with noEcho==1
						if(isset($this->data[$key]['noEcho']) && filterBoolean($this->data[$key]['noEcho'])==true)continue;
						//echo other keys to the string
						if (is_array($value)) {
							$string .= chr(9). $key . ' => ' . print_r($value, true);
						} else $string .= chr(9).$key . ' => ' . $value . chr(10);
					}
				}
			}
		}
		$string.= '}'.chr(10);
		return $string;
	}

}
