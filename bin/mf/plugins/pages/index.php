<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';

class pagesPlugin implements plugin{

    function setup(){
        /*
        $pdo = $GLOBALS['mf']->db->pdo;
        $page = new page();
        $page->createSQLTable();


        //create the mother of all web pages, with language="ALL" and void title
        $sql = "INSERT INTO `mf_pages` (`uid`, `deleted`, `hidden`, `sorting`, `creation_date`, `modification_date`, `creator`, `change_log`, `edit_lock`, `start_time`, `end_time`, `language`, `alt_language`, `hide_in_nav`, `parent_uid`, `url_path_segment`, `title`, `nav_title`, `meta_description`, `meta_keywords`, `meta_robots`, `type`, `is_siteroot`, `no_delete`, `shortcut`, `shortcut_mode`, `shortcut_to`, `template_name`, `template_content`, `hide_from_sitemap`, `changefreq`, `priority`, `view_count`, `hide_lastmod`) VALUES ('1', '0', '0', '0', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '0', '', '0', '1970-01-01 00:00:00', '9999-01-01 00:00:00', 'ALL', NULL, '0', NULL, '', 'ROOT', '', NULL, NULL, NULL, '', '0', '0', '0', '', '', '', NULL, '0', '', '0.5', '0', '0');";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return "Table \"".page::$tableName."\" created sucessfully !";*/
    }

    function getName(){
        global $l10n;
        return $l10n->getLabel('pagesPlugin','name');
    }

    function getDesc(){
        global $l10n;
        return $l10n->getLabel('pagesPlugin','desc');
    }

    function getPath(){
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey(){
        return "pages";
    }

    function getVersion(){
        return "1.1";
    }

    function getDependencies(){
        $dependencies = array();
        return $dependencies;
    }

    function init(){
        global $mf,$l10n;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');

        //inform MindFlow of the new dbRecords supplied by this plugin
        $mf->pluginManager->addRecord('pages', 'page', $relativePluginDir.'/records/page.php');

        $mf->pluginManager->loadModule($relativePluginDir,"modules","pages");

        if(class_exists('mfUserGroup')) mfUserGroup::defineUserRight('pages', 'allowEditPages',array(
            'value' => '0',
            'dataType' => 'checkbox',
            'valueType' => 'number',
            'processor' => '',
            'div_attributes' => array('class' => 'col-lg-2'),
        ),
            $relativePluginDir.'/languages/userRights_l10n.php'
        );
    }
}
$mf->pluginManager->addPluginInstance(new pagesPlugin());

