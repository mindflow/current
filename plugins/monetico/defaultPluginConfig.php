<?php

//default configuration for the plugin
//add and edit these values in your config file
$GLOBALS['config']['plugins']['monetico'] = array(
    'prod_config_file' => '/home/SECURE_DATADIR/CIC_CONFIG/MoneticoPaiement_Config_FR.php', //ABSOLUTE PATH to Monetico config file
    'test_config_file' => '/home/SECURE_DATADIR/CIC_CONFIG/MoneticoPaiement_Config_FR_dev.php', //ABSOLUTE PATH to Monetico config file
    'showInMenus' => '0', //displays Monetico modules in menus.
    'enableCaptureModule' => '1', //displays module for dealing with captured payment types
    'enablePaymentModule' => '1', //displays module for dealing with payments
    'enableBankFeedbackModule' => '1', //displays module for dealing with bank feedbacks
    'forceTestPayments' => '1', //force calling the bank form in test mode in all situations. If value is 0, bank is called in test mode only if the current user is logged into the backend. DISABLE IN PRODUCTION.

    //functions called on bank feedback. Each function should accept an object of type moneticoBankFeedback as parameter, supplying the information retreived from the bank.
    'paymentFailureCallbackFunc' => 'paymentsProcessor::paymentFailure',
    'testPaymentSuccessCallbackFunc' => 'paymentsProcessor::testPaymentSuccess',
    'paymentSuccessCallbackFunc' => 'paymentsProcessor::paymentSuccess',
    'multipartPaymentSuccessCallbackFunc' => 'paymentsProcessor::multipartPaymentSuccess',
    'multipartPaymentFailureCallbackFunc' => 'paymentsProcessor::multipartPaymentFailure',



);