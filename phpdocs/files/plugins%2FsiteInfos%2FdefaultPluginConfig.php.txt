<?php

//default configuration for the plugin
//add and edit these values in your config file
$config['plugins']['siteInfos'] = array(
    'siteInfo-template-def' => '/mf_websites/www.sample-website.com/templates/plugins/siteInfos/templates/siteInfo-template.php',
    'siteInfo-class-language' => '/mf_websites/www.sample-website.com/templates/plugins/siteInfos/languages/siteInfo.php',
);
