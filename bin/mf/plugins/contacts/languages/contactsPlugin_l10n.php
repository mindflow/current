<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow website contact form",
        'desc' => "<p>The contacts plugin provides a standard contact form to use on your website.</p><p>The submited data is stored in the site database and sent to the email adress specified in the plugin configuration as well.</p><p>A custom form can also be specified</p>"


    ),


    'fr' => array(
        'name' => "Formulaire de contact pour sites MindFlow",
        'desc' => "<p>Le plugin contacts fournit un formulaire de contact standard pour utiliser sur les sites web.</p><p>Les données soumises sont enregistrées dans la base de données du site et envoyée également à l'adresse emails spécifiée dans la configuration du plugin.</p><p>Il est également possible de substituer un formulaire personnalisé.</p>"


    ),
);

?>