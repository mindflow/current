<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/*
 * This is the JSON answering PHP file for ajax requests
 * See file ajax-core-html.php for serving pure HTML answers.
*/

$execution_start = time();

require_once '../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/frontend/frontend.php';

//we are serving json
header('Content-type: application/json');

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec()){

    global $mf,$l10n,$pdo;

    if(isset($_REQUEST['mode']) && $_REQUEST['mode']=='backend'){
        //create a backend for being able to use all the mindflow objects
        $xEnd = new backend();
    }
    else{
        //create a backend for being able to use all the mindflow objects
        $xEnd = new frontend();
    }

    $xEnd->prepareData();

    if(isset($_REQUEST['action'])) {

        if ($_REQUEST['action'] == 'sortPages') {
            $pages = $_REQUEST['page'];

            //updating parent->child relationships
            foreach ($pages as $pageID => $parentID) {
                if ($mf->info['pagesByUid'][$pageID]->data['parent_uid']['value'] != $parentID) {
                    //update page parent
                    if ($parentID != 'null') {
                        $mf->info['pagesByUid'][$pageID]->data['parent_uid']['value'] = $parentID;
                        $mf->info['pagesByUid'][$pageID]->store();
                    } else {
                        $mf->info['pagesByUid'][$pageID]->data['parent_uid']['value'] = 0;
                        $mf->info['pagesByUid'][$pageID]->store();
                    }

                }
            }
            //update the page tree so we can read up-to-date information on page structure
            $mf->modules->urlRewriter->setPageTree();

            // now updating sorting
            // poor performance (updates all records in the tree while only one has moved)
            // but easyest way to do due to poor info collected from the $pages data supplied
            $order = 0;
            foreach ($pages as $pageID => $parentID) {
                $mf->info['pagesByUid'][$pageID]->data['sorting']['value'] = $order++;
                $mf->info['pagesByUid'][$pageID]->store();
            }

            $mf->db->closeDB();
            die('{"jsonrpc" : "2.0", "result" : "Pages have been sorted"}');
        }


        else if ($_REQUEST['action'] == 'createPage') {

            //create an instance of the object to be updated
            $page = new page();
            $page->data['parent_uid']['value'] = '0';
            $page->data['sorting']['value'] = '0';

            //insert defaut template structure in template_data field so the page can be used immediately without having to hit the save button
            $currentTemplateName = $page->data['template_name']['value'];
            if (isset($mf->templates[$currentTemplateName])) $page->data['template_content']['value'] = $mf->templates[$currentTemplateName]['template_data'];

            //create the record
            if ($uid = $page->store()) {

                // now updating sorting for next pages (newly created page appears first)
                // poor performance (updates all records in the tree while only one has moved)
                // but easyest way to do due to poor info collected from the $pages data supplied
                $order = 0;
                $currentLanguage = $mf->getDataEditingLanguage();
                foreach ($mf->info['pagesByUid'] as $existingPage) {
                    if ($existingPage->data['language']['value'] == $currentLanguage) {
                        $existingPage->data['sorting']['value'] = ++$order;
                        $existingPage->store();

                    }
                }

                $html = array();
                $html[] = "<li id=page_" . $uid . " class=pageItem>";
                $html[] = "<div><span class='disclose'><span></span></span>";
                $html[] = "<i class='glyphicon glyphicon-file' title='" . $uid . "'></i> <span class='pageTitle'>" . $page->data['title']['value'] . "</span>";
                $html[] = "<span class=pageActions>";
                $html[] = "<span class=recordAction><a><i class='glyphicon glyphicon-move'></i></a></span>";

                //form security variables
                $secKeys = array(
                    'action' => 'editRecord',
                    'record_class' => 'page',
                    'record_uid' => $page->data['uid']['value'],
                    'mode' => $mf->mode,
                );
                $advancedSettings = array(
                    'reloadFormOnSave' => 'true',
                    'JSCallbackFunctionAfterSubmit'=>'refreshPageEntry',
                );
                $secHash = formsManager::makeSecValue($secKeys);
                $secFields = implode(',',array_keys($secKeys));

                $buttonParams = '&showSaveButton=true&showSaveCloseButton=true&showPreviewButton=true&showCloseButton=true&advancedSettings='.urlencode(base64_encode(http_build_query($advancedSettings)));
                $linkClass = "";
                $editLink = SUB_DIR . "/mf/core/ajax-core-html.php?action=editRecord&record_class=page&record_uid=" . $page->data['uid']['value'] . "&ajax=1" . $buttonParams . "&header=0&footer=0&sec=" . $secHash . "&fields=" . $secFields . "&mode=" . $mf->mode;
                $html[] = '<span class=recordAction><a' . $linkClass . ' onclick="mf.dialogManager.openDialog(\'page_dialog\',\'\',\'\',\'' . $editLink . '\',\'90%\', \'\', \'' . $l10n->getLabel('page', 'record_name') . '\');" title="' . $l10n->getLabel('backend', 'edit') . '"><i class="glyphicon glyphicon-pencil"></i></a></span>';

                //$html[] = "<span class=recordAction><a href='".SUB_DIR."/mf/index.php?module=pages&action=edit&uid=".$uid."' title='".$l10n->getLabel('backend','edit')."'><i class='glyphicon glyphicon-pencil'></i></a></span>";

                $html[] = "<span class=recordAction><a onclick='javascript:showHideRecord(\"" . $page->data['uid']['value'] . "\");' title='" . $l10n->getLabel('pages', 'showhide') . "'><i class='glyphicon glyphicon-eye-open showHide'></i></a></span>";
                $html[] = "<span class=recordAction><a data-controls-modal='windowTitleDialogPages' data-backdrop='true' data-keyboard='true' href='javascript:getDeleteModal(" . $uid . ", \"" . $page->data['title']['value'] . "\");' title='" . $l10n->getLabel('backend', 'delete') . "'><i class='glyphicon glyphicon-trash'></i></a></span>";
                $html[] = "</span>";
                $html[] = "</div>";
                $html[] = "</li>";

                $execDuration = ', "duration" : "' . ((time() - $execution_start)) . 'ms"';

                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "Record ' . $uid . ' has been created", "uid" : "' . $uid . '", "html" : ' . json_encode(implode(chr(10), $html)) . $execDuration . '}');
            } else {
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "error" : "The record could not be created."}');
            }

        }

        else if ($_REQUEST['action'] == 'getPageTitle') {

            $pageUid = $_REQUEST['record_uid'];

            $page = new page();
            $loadSuccess = $page->load($pageUid);

            $mf->db->closeDB();
            if($loadSuccess) {
                die('{"jsonrpc" : "2.0", "result" : "success", "title" : "' . $page->data['title']['value'] . '"}');
            }
            else {
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "error", "message": ' . json_encode($l10n->getLabel('pages', 'page_noexist')).'}');
            }
        }


    }
    else {
        $mf->db->closeDB();
        die('{"jsonrpc" : "2.0", "error" : "No action specified."}');
    }
    //echo "Error : No action specified.";
}
else {
    die('{"jsonrpc" : "2.0", "error" : "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}');
}

    //echo "Error : Invalid securityToken supplied.";



