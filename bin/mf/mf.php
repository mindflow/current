<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/dbManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/pluginManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/l10nManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/Autoloader.php';





if (version_compare(phpversion(), '5.5.0', '<')) {
    //This library is intended to provide forward compatibility with the password_* functions of PHP 5.5.
    require_once DOC_ROOT.SUB_DIR.'/mf_librairies/password_compat/password.php';
}

class mf{


    //this is the PDO database object which can be used directly from any class to access the database
    var $db;

    //this is the localisation (l10n) manager which takes car of dealing with translation files
    var $l10n;

    //loads and manages plugins
    var $pluginManager;
    var $modules; //shortcut to the modules list

    //handles forms
    var $formsManager;

    //allows checking if mindflow is running in 'backend' or 'frontend' mode
    var $mode;

    //access to the backend object
    //var $backend;
    //access to the frontend object
    //var $frontend;

    //stores the current frontend or backend instance, according to the execution mode of MindFlow
    var $x_end;

    //currentBackendUser
    var $currentUser;



    //stores the various templates available for the current website
    var $templates = array();
    var $microtemplates = array();

    //html objects used for outputing the page
    var $header;
    var $body;
    var $footer;
    var $frontendMessages = array(); //used to show errors in the frontend. The templates must supply the {mindflow-messages} marker for those messages to show up

    //usefull datasets
    var $info = array(); //stores all informations related to the current website one might need


    //var $mainCSS = '';

    private $cssList = array();         //List of css files to link in the page
    private $topJsList = array();       //Javascript files called in the page header
    private $bottomJsList = array();    //Javascript files called at the end of the page to prevent page loading slowdowns
    private $microJsList = array();     //Javascript files called when loading microtemplates

    var $customMessages = array();
    var $warningMessages = array();
    var $errorMessages = array();
    var $successMessages = array();
    var $infoMessages = array();

    static $localizeUploadsDir; //true if the uploads directory should feature the locale into the path.

    public function __construct($mode){

        global $mf,$db,$pdo,$l10n,$config,$pluginManager,$formsManager,$logger,$loader;

        //add to global var for easier access and nicer syntax
        $mf = $GLOBALS['mf'] = $this;

        //add to global var for easier access and nicer syntax
        $config = $GLOBALS['config'];

        if(isset($config['preload_classes'])){
            foreach($config['preload_classes'] as $classPath){
                require_once($classPath);
            }
        }

        //allow many concurrent sessions : sessions are garbage collected too quickly on some servers
        ini_set('session.gc_probability', 1);
        ini_set('session.gc_divisor', 1000);

        //creating session for storing data
        if(WEB_MODE && !isset($_SESSION))session_start();

        //set default locales
        //frontend language currently being edited in the backend
        if(WEB_MODE && isset($_SESSION['data_editing_locale'])) $this->info['data_editing_locale'] = $_SESSION['data_editing_locale'];
        else $this->info['data_editing_locale'] = $config['default_frontend_locale'];

        //language currently displayed in the frontend
        $this->info['frontend_locale'] = $config['default_frontend_locale'];

        //language of the backend UI
        $this->info['backend_locale'] = $config['default_backend_locale'];


        // instantiate the autoloader
        $loader = new \mf\core\Autoloader;

        // register the autoloader
        $loader->register();

        // register the base directories for the namespace prefix of the required librairies
        $loader->addNamespace('Psr', DOC_ROOT.SUB_DIR.'/mf_librairies/Psr');
        $loader->addNamespace('Monolog', DOC_ROOT.SUB_DIR.'/mf_librairies/Monolog-1.23.0');

        //define a unique render ID, so if 2 pages are rendered at the same time, the ID will allow to distinct the render run in the log
        $renderID = rand();


		//Monolog Logger
	    if(isset($config['enable_logger']) && filterBoolean($config['enable_logger'])) {

		    // Create the Monolog logger
		    $logger = new \Monolog\Logger('Mindflow_'.$renderID);

		    // Now add the configured handlers

		    //look for user configured handlers
		    if ( isset( $config['monolog'] ) ) {
			    require_once DOC_ROOT . SUB_DIR . DIRECTORY_SEPARATOR . $config['monolog'];
		    } //if not available, fallback to Mindflow sample default configuration
		    else if ( is_file( DOC_ROOT . SUB_DIR . DIRECTORY_SEPARATOR . 'mf_config/monolog-config.php' ) ) {
			    require_once DOC_ROOT . SUB_DIR . DIRECTORY_SEPARATOR . 'mf_config/monolog-config.php';
		    } //if not available, fallback to a solution that works in every case
		    else {
			    $logger->pushHandler( new \Monolog\Handler\StreamHandler( DOC_ROOT . SUB_DIR . '/mindflow.log', \Monolog\Logger::DEBUG ) );
		    }
		    $logger->info('');
		    $logger->info( '*** Mindflow logger is ready ***' );
	    }


        $this->mode = $mode;
        $logger->info("Running mode = ".$this->mode);

        //default HTTP status code
        $this->info['http_status'] = 200;

        //an array of all page objects sorted by parent / child relationship. This is filled up by urlRewriter->setPageTree()
        $this->info['pageTree'] = array();

        //an array of all page objects sorted by uid. This is filled up by urlRewriter->setPageTree()
        $this->info['pagesByUid'] = array();
        //var $pagesByUid

        //setting local timezone for timestamping debug log
        date_default_timezone_set($config['timezone']);

        //create localisation manager
        $this->l10n = new l10nManager();
        $l10n = $this->l10n;

        //create database manager and open connexion
        $this->db = new dbManager();
        $this->db->openDB();
        $db = $this->db;
        $pdo = $db->pdo;


        //create html object for storing html to be displayed
        $this->html = array();

        //array for storing template objects
        $this->templates = array();



        //Set up default CSS and JS, common to frontend and backend
        $this->addCss('<link rel="stylesheet" href="/mf_librairies/bootstrap-3.3.7/css/bootstrap.min.css">',true,true);
        $this->addCss('<link rel="stylesheet" href="'.SUB_DIR.'/mf_librairies/jQuery-ui-1.12.1/jquery-ui.min.css" type="text/css" media="screen">',true,true);
        //$this->addCss('<link rel="stylesheet" href="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />',true,true);

        $this->addTopJs('<script>var SUB_DIR = "'.SUB_DIR.'";</script>',true,true);
        //mf init JS
        $this->addTopJs('<script src="'.SUB_DIR.'/mf/core/ressources/js/mf.js?v=1"></script>',true,true);

        //jQuery
	    //$this->addTopJs('<script src="'.SUB_DIR.'/mf_librairies/jQuery/jquery-1.11.2.js?v=1"></script>',true,true);
        //$this->addTopJs('<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>',true,true);
        //$this->addTopJs('<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>',true,true);
        $this->addTopJs('<script src="'.SUB_DIR.'/mf_librairies/jQuery/jquery-1.12.4.js?v=1"></script>',true,true);
        $this->addTopJs('<script src="'.SUB_DIR.'/mf_librairies/jQuery/jquery-migrate-1.4.1.js?v=1"></script>',true,true);
        $this->addTopJs('<script src="'.SUB_DIR.'/mf_librairies/jQuery-ui-1.12.1/jquery-ui.min.js"></script>',true,true);

        $this->addTopJs('<script>
        //fix for CKEditor Input fields not accessible from inside jquery-ui modal dialog
        //the fix works, but raises maximum call stack exceeded error
//        orig_allowInteraction = $.ui.dialog.prototype._allowInteraction;
//        $.ui.dialog.prototype._allowInteraction = function(event) {
//          if ($(event.target).closest(\'.cke_dialog\').length) {
//            return true;
//          }
//          return orig_allowInteraction.apply(this, arguments);
//        };

		//test alternative solution, but this will allow interaction with any element outside of the dialog 
		$.ui.dialog.prototype._allowInteraction = function(event) {
		  return true;
		};
        </script>',true,true);

        //Bootstrap
        $this->addBottomJs('<script src="'.SUB_DIR.'/mf_librairies/bootstrap-3.3.7/js/bootstrap.min.js?v=1"></script>',true,true);

        //Fancybox
        $this->addTopJs('<script src="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>',true,true);

        //create formsManager
        $this->formsManager = new formsManager();
        $formsManager = $this->formsManager;

        //create plugin manager last so all standard objects are already built and accessible from plugins
        $this->pluginManager = new pluginManager();
        $pluginManager = $this->pluginManager;
        $this->pluginManager->loadAllPlugins();
        $this->modules = (object) $this->pluginManager->modulesByName;

        //initialize javascript framework
        $this->addTopJs('<script>loadScript("'.SUB_DIR.'/mf/core/ressources/js/mfDialogs.js?v=1");</script>',true,true);
        $this->addTopJs('<script>loadScript("'.SUB_DIR.'/mf/core/ressources/js/mfForms.js?v=1");</script>',true,true);
        //$this->addTopJs('<script src="'.SUB_DIR.'/mf/core/ressources/js/mfDialogs.js?v=1"></script>',true,true);
        //$this->addTopJs('<script src="'.SUB_DIR.'/mf/core/ressources/js/mfForms.js?v=1"></script>',true,true);
        $this->addTopJs('<script>
            //make frontend language available for js
            mf.locale = "'.$this->getLocale().'";
            
            //set labels required in js files
            mf.l10n["backend"]=[];
            mf.l10n["backend"]["record_must_be_saved"]="'.$this->l10n->getLabel('backend','record_must_be_saved').'";
        </script>',true,true);



    }


    /**
     * Returns the current locale for frontend display
     * @return String the current locale displayed in frontend
     */
    public function getFrontendLanguage(){
        return $this->info['frontend_locale'];
    }

    /**
     * Returns the language of the backend User Interface
     * @return String the current locale for backend UI
     */
    public function getBackendLanguage(){
        //print_r($this->currentUser->data);
        //echo "<br/><br/>";
        if(isset($this->currentUser->data['backend_locale']['value']) && $this->currentUser->data['backend_locale']['value'] != ''){

            $this->info['backend_locale'] =  $this->currentUser->data['backend_locale']['value'];
            return $this->info['backend_locale'];
        }
        else return $this->info['backend_locale'];
    }

    /**
     * Returns the currently used language locale, whether you are in frontend or backend mode
     * TODO : make this function obsolete and replace it with getEditedLocale() / getDisplayedLocale() ?
     * @return String the current locale displayed in frontend or edited in backend
     */
    public function getLocale(){
        return $this->getEditedLocale();
    }

    public function getEditedLocale(){
        if($this->mode=="backend")return $this->getDataEditingLanguage();
        else return $this->getFrontendLanguage();
    }

	/**
	 * Returns the current locale for data currently being edited in backend or frontend
	 *
	 * @return String the current locale for data being edited in backend
	 */
	public function getDataEditingLanguage(){
		if($this->mode == "frontend") return $this->info['frontend_locale'];
		else return $this->info['data_editing_locale'];
	}

    /**
     * Returns the currently selected locale for current form display
     * This function is especially usefull when editing a form widget/field. You may edit some language that is not your mother language
     * and at the same time have the backend displayed in your mother language. Calling this function in your widget will ensure the widget will display
     * with your current editing language in backend (say in French if your mother language is French) while you are editing a foreign language record (say in German).
     * The form widget/field will then also display correctly in frontend with the intended locale (German). For example, an area of use is dates, which do not
     * display the same in French (01/12/2016) and in German (01.12.2016), thus you need to adapt the widget/field display
     *
     * @return String the current locale being edited in backend
     */
    public function getDisplayedLocale(){
        if($this->mode=="frontend")return $this->getFrontendLanguage();
        else return $this->getBackendLanguage();

    }

    /**
     * Sets the current locale for data currently being edited in backend or frontend
     * @param $language String an ISO2 string for the required locale
     * @return bool
     */
    public function setDataEditingLanguage($language){
        global $config;
        //filter values to prevent SQL injections
        foreach($config['frontend_locales'] as $key => $value){
            if($language == $key){
                //update language
                if(WEB_MODE) {
                	$_SESSION['data_editing_locale'] = $this->info['data_editing_locale'] = $key;
	                $this->info['frontend_locale'] = $key;
                }
                else {
                	$this->info['data_editing_locale'] = $key;
	                $this->info['frontend_locale'] = $key;
                }
                return true;
            }
        }
        return false;
    }





    public function getSiteRootPages(){
        $rootPages = array();
        $language = $this->getDataEditingLanguage();

       // print_r($this->info['pageTree']);

        foreach($this->info['pageTree'] as $page){
            if($page->data['is_siteroot']['value'] == '1' && $page->data['language']['value'] == $language){
                //recherche de la page accueil (uid=1) et de ses sous-pages sous la racine du site
                $rootPages[] = $page;
            }
        }
        if(sizeof($rootPages) == 0) {
            echo '<!doctype html><html><head><meta charset="utf-8"></head><body>';
            echo '<br />'.$this->l10n->getLabel('main','no_root').'<br>'.chr(10);
            echo '</body></html>';
            die();
        }
        return $rootPages;
    }

    /*
     * Returns all the pages in the rootLine from bottom to top
     * @param $leafUid the uid of the leaf page we are studying the rootline above
     * @param &$rootLine an empty array which will be populated by the function
     */
    public function getRootline($leafUid, &$rootLine=array()){
        if(isset($this->info['pagesByUid'][$leafUid])){
            $currentPage = $this->info['pagesByUid'][$leafUid];
            $rootLine[] = $currentPage;
            if($currentPage->data['parent_uid']['value'] > 0){
                $this->getRootline($currentPage->data['parent_uid']['value'], $rootLine);
            }
        }
    }

    /*
    * Returns all the page UIDs in the rootLine from bottom to top
    * @param $leafUid the uid of the leaf page we are studying the rootline above
    * @param &$rootLine an empty array which will be populated by the function
    */
    public function getRootlineUids($leafUid, &$rootLine){
        if(isset($this->info['pagesByUid'][$leafUid])){
            $currentPage = $this->info['pagesByUid'][$leafUid];
            $rootLine[] = $currentPage->data['uid']['value'];
            if($currentPage->data['parent_uid']['value'] > 0){
                $this->getRootlineUids($currentPage->data['parent_uid']['value'], $rootLine);
            }
        }
    }

    public function addTemplates($templatesArray){
	    $this->templates = array_merge_recursive_distinct($this->templates,$templatesArray);
    }

    public function addMicrotemplates($templatesArray){
        $this->microtemplates = array_merge_recursive_distinct($this->microtemplates,$templatesArray);
    }


	function addCustomMessage($messageText){
		$this->customMessages[] = $messageText;
	}

    function addWarningMessage($messageText){
        $this->warningMessages[] = $messageText;
    }

    function addErrorMessage($messageText){
        $this->errorMessages[] = $messageText;
    }

    function addSuccessMessage($messageText){
        $this->successMessages[] = $messageText;
    }

    function addInfoMessage($messageText){
        $this->infoMessages[] = $messageText;
    }



    /**
     *  Adds a CSS tag to the rendered pages
     * @param $cssTag string the full css tag, such as '<link rel="stylesheet" href="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />'
     * @param $useInBackend boolean displays the tag in backend
     * @param $useInFrontend boolean displays the tag in frontend
     */
    function addCss($cssTag,$useInBackend, $useInFrontend){
        if((($this->mode=="backend" || $this->mode=="authentification") && $useInBackend) or ($this->mode=="frontend" && $useInFrontend)){
            if(!in_array($cssTag,$this->cssList))$this->cssList[]= $cssTag;
        }
    }

    /**
     *  Adds a script tag to the top of the rendered pages
     * @param $jsTag string the full js tag, such as '<script src="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>'
     * @param $useInBackend boolean displays the tag in backend
     * @param $useInFrontend boolean displays the tag in frontend
     */
    function addTopJs($jsTag,$useInBackend, $useInFrontend){
        if((($this->mode=="backend" || $this->mode=="authentification") && $useInBackend) or ($this->mode=="frontend" && $useInFrontend)){
            if(!in_array($jsTag,$this->topJsList))$this->topJsList[]= $jsTag;
        }
    }

    /**
     *  Adds a script tag to the bottom of the rendered pages
     * @param $jsTag string the full js tag, such as '<script src="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>'
     * @param $useInBackend boolean displays the tag in backend
     * @param $useInFrontend boolean displays the tag in frontend
     */
    function addBottomJs($jsTag,$useInBackend, $useInFrontend){
        if((($this->mode=="backend" || $this->mode=="authentification") && $useInBackend) or ($this->mode=="frontend" && $useInFrontend)){
            if(!in_array($jsTag,$this->bottomJsList))$this->bottomJsList[]= $jsTag;
        }
    }

    /**
     *  Adds a script tag to the microtemplates
     * @param $jsTag string the full js tag, such as '<script src="'.SUB_DIR.'/mf_librairies/fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>'
     * @param $useInBackend boolean displays the tag in backend
     * @param $useInFrontend boolean displays the tag in frontend
     */
    function addMicroJs($jsTag,$useInBackend, $useInFrontend){
        if((($this->mode=="backend" || $this->mode=="authentification") && $useInBackend) or ($this->mode=="frontend" && $useInFrontend)){
            if(!in_array($jsTag,$$this->microJsList))$this->microJsList[]= $jsTag;
        }
    }

    /**
     * Retreives the list of CSS tags that should be output
     * @return array the list of css tags to be displayed
     */
    function getCssList(){
        return $this->cssList;
    }

    /**
     * Retreives the list of javascript tags that should be output on top of the page
     * @return array the list of <script> tags to be displayed
     */
    function getTopJsList(){
        return $this->topJsList;
    }

    /**
     * Retreives the list of javascript tags that should be output on bottom of the page
     * @return array the list of <script> tags to be displayed
     */
    function getBottomJsList(){
        return $this->bottomJsList;
    }

    /**
     * Retreives the list of CSS tags that should be output
     * @return array the list of css tags to be displayed
     */
    function getMicroJsList(){
        return $this->cssList;
    }

    /*
     * Returns the absolute URL below the homepage for the uid supplied
     * @param $pageUid uid of the page for which the URL is requested
     * @param $appendHtmlSuffix boolean appends '.html' at the end of the url if true
     * @return string the url
     */
    function getPageUrl($pageUid, $appendHtmlSuffix=true){
        if(isset($this->modules->urlRewriter))
            return $this->modules->urlRewriter->getPageURL($pageUid,$appendHtmlSuffix);
        else return "\$mf->getPageUrl() ".$this->l10n->getLabel('frontend','urlRewriter_missing');
    }

    /*
     * Returns the absolute URL below the homepage for the uid supplied
     * @param $pageUid uid of the page for which the URL is requested
     * @param $class string the html class to apply to the link
     * @param $id string the html id to apply to the link
     * @return string the url
     */
    function getPageLink($pageUid, $class='', $id=''){
        if(isset($this->modules->urlRewriter))
            return $this->modules->urlRewriter->getPageLink($pageUid, $class, $id);
        else return "\$mf->getPageLink() ".$this->l10n->getLabel('frontend','urlRewriter_missing');
    }


    /*
     * Returns the current page object displayed in frontend. Hence, this is a frontend context only function.
     * @return page the current page page object
     */
    function getCurrentPage(){
        if(isset($this->modules->urlRewriter)){

            $currentPage = new page();
            $currentPage->load($this->info['currentPageUid']);
            return $currentPage;
        }
        else return "\$mf->getCurrentPage() ".$this->l10n->getLabel('frontend','urlRewriter_missing');
    }


    /*
     * Returns the record for the current logged in user, being either a frontend or a backend user, or null if no user is logged in.
     * You can then figure what class is involved by executing get_class() on the returned object or by checking $mf->mode variable
     * If the user is connected in frontend, the programmer should have injected his identified user instance into $_SESSION['current_fe_user']
     * @return record the current logged in user
     */
    function getCurrentUser(){
        return $this->currentUser;
    }

    /**
     * Returns the page title for the page with the given uid
     * @param $pageUid uid of the page for which the title is requested
     * @return string the title
     */
    function getPageTitle($pageUid){
        return $this->info['pagesByUid'][$pageUid]->data['title']['value'];
    }

    /**
     * Returns the page navigation title for the page with the given uid
     * The navigation title is usually a shorter version meant to not overload navigation menus
     * This function will return the navigation title if specified, otherwise it will return the page title
     * @param $pageUid uid of the page for which the navigation title is requested
     * @return string the title
     */
    function getPageNavTitle($pageUid){
        if(isset($this->info['pagesByUid'][$pageUid]->data['nav_title']['value']))
            return $this->info['pagesByUid'][$pageUid]->data['nav_title']['value'];
        else return $this->info['pagesByUid'][$pageUid]->data['title']['value'];
    }

    /*
     * Returns the upload directory relative to the website root
     * @return string the directory
     */
	function getUploadsDir(){
		global $mf,$config;

		if(!isset(self::$localizeUploadsDir))
			self::$localizeUploadsDir = (isset($config['localize_uploads_dir']))?$config['localize_uploads_dir']:false;

		$separator = '';
		if( strlen($config['uploads_directory'])>0 && substr($config['uploads_directory'], -1) != DIRECTORY_SEPARATOR ) $separator = DIRECTORY_SEPARATOR;

		$uploadsDir = SUB_DIR.$config['uploads_directory'].((self::$localizeUploadsDir)?$separator.$mf->getLocale().DIRECTORY_SEPARATOR:'');

		return $uploadsDir;
	}


	/**
	 * Adds a function call to be executeed when the cron.php file  located at the root of mindflow's file tree is called.
	 * This function call will be executed using eval.
	 * @param string $funcCall
	 */
    function addCronFunc($funcCall){
	    if( !isset($this->info['cronFuncs'])) $this->info['cronFuncs'] = array();
	    $this->info['cronFuncs'][] = $funcCall;
    }
}
?>