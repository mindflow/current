<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow sitewide informations storage",
        'desc' => "<p>Allows to specify more easily a custom site information form inside a template object. This will be usefull to store information that can be reused sitewide and easily edited by the end admin user, such as company adress, phone and logo.</p>"


    ),


    'fr' => array(
        'name' => "Informations de site MindFlow",
        'desc' => "<p>Permet d'éditer et de stocker plus facilement des informations transversales à toutes les pages d'un site depuis un objet gabarit. Ceci sera utile par exemple pour afficher une adresse ou un logo sur le site, tout en les rendant facilement éditables par l'utilisateur final.</p>"


    ),
);

?>