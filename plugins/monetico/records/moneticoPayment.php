<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';



//load test payment configuration if current_user is logged into the backend or if test mode is forced
//allows testing using simulated payments rather than real CB
if(isset($_SESSION['current_be_user']) || $GLOBALS['config']['plugins']['monetico']['forceTestPayments']==1){
    if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['test_config_file'];
}
else {
    if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['prod_config_file'];
}

require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/MoneticoPaiement_Ept.inc.php';

class moneticoPayment extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_monetico_payment';
    static $oneTablePerLocale = true;

    //see document /monetico/ressources/doc/Monetico_Paiement_documentation_technique_v1_0e.pdf page 23 for field specification
    static $createTableSQL= "
        `type_paiement` int(10) unsigned NOT NULL DEFAULT '0',
        `version` decimal(2,1) unsigned NOT NULL DEFAULT '3.0',
        `tpe` int(11) unsigned NOT NULL DEFAULT '0',
        `date` datetime NOT NULL,
        `devise` varchar(3) ,
        `montant` varchar(10) ,
        `reference` varchar(50) ,
        `texte-libre` varchar(255) ,
        `mail` varchar(60) ,
        `lge` varchar(2) ,
        `societe` varchar(30) ,
        `MAC` varchar(40) ,
        `aliascb` varchar(60) ,
        `forcesaisiecb` boolean DEFAULT '0',
        `3dsdebrayable` boolean DEFAULT '0',


        #champs propres au paiement fractionné
        `nbrech` tinyint(1) NOT NULL DEFAULT '0',
        `dateech1` date default NULL,
        `montantech1` varchar(13) ,
        `dateech2` date default NULL,
        `montantech2` varchar(13) ,
        `dateech3` date default NULL,
        `montantech3` varchar(13) ,
        `dateech4` date default NULL,
        `montantech4` varchar(13) ,

        #champs propres aux paiements via partenaires
        `protocole` varchar(13) ,
    ";

    static $createTableKeysSQL="
          KEY `k_mac` (`MAC`),
          KEY `k_mail` (`mail`),
          KEY `k_montant` (`montant`),
          KEY `k_type` (`type_paiement`),
          FULLTEXT KEY `vz_demande_search` (`reference`,`texte-libre`)
    ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init(){
        parent::init();

        $this->data = array_merge($this->data,array(
            'type_paiement' => array(
                'value' => 'classique',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'classique'     => 'Classique',
                    'fractionné'    => 'Fractionné',
                    'différé'       => 'Différé',
                    'partiel'       => 'Partiel',
                    'récurrent'     => 'Récurrent',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),

            //type paiement : classique, fractionné, différé, partiel, récurrent

            'version'    =>  array(
                'value' => MONETICOPAIEMENT_VERSION,
                'dataType' => 'hidden',
                'valueType' => 'number',
                'div_attributes' => array('class' => 'col-lg-3'),

            ),

            'tpe'   => array(
                'value' => MONETICOPAIEMENT_EPTNUMBER,
                'dataType' => 'hidden',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'date'   =>  array(
                'value' => '',
                'dataType' => 'datetime',
                'valueType' => 'datetime',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),

            'devise' => array(
                'value' => 'EUR',
                'dataType' => 'select',
                'possibleValues' => array(
                    'EUR'    => 'EUR',
                    'GBP'       => 'GBP',
                    'USD'       => 'USD',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
            ),

            'montant'   => array(
                'value' => '0.00',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),



            'reference'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'texte-libre'   => array(
                'value' => '',
                'dataType' => 'textarea 50 4',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'mail'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'lge'   => array(
                'value' => 'FR',
                'dataType' => 'select',
                'possibleValues' => self::listLanguages(),
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),

            'societe'   => array(
                'value' => MONETICOPAIEMENT_COMPANYCODE,
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'MAC'   => array(
                'value' => '',
                'dataType' => 'hidden',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
                'editable' => false
            ),

            'aliascb'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),

            'forcesaisiecb'   => array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'div_attributes' => array('class' => 'col-lg-3'),
                'swap' => '0'
            ),

            '3dsdebrayable'   => array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'div_attributes' => array('class' => 'col-lg-3'),
                'swap' => '0'
            ),





            'nbrech' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    ''  => '',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
            ),
            'dateech1'   =>  array(
                'value' => '',
                'dataType' => 'date',
                'valueType' => 'date',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),
            'montantech1'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'dateech2'   =>  array(
                'value' => '',
                'dataType' => 'date',
                'valueType' => 'date',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),
            'montantech2'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'dateech3'   =>  array(
                'value' => '',
                'dataType' => 'date',
                'valueType' => 'date',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),
            'montantech3'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'dateech4'   =>  array(
                'value' => '',
                'dataType' => 'date',
                'valueType' => 'date',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                )
            ),
            'montantech4'   => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array(
                    'class' => 'input-lg',
                ),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'protocole' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    ''  => '',
                    'paypal' => 'Paypal',
                    '1euro' => 'Cofidis 1Euro',
                    '3xcb' => 'Cofidis 3xCB',
                    'fivory' => 'Fivory',
                ),
                'valueType' => 'text',
                'div_attributes' => array(
                    'class' => 'col-lg-3'
                ),
            ),

        ));



        //No field should be editable
      /*  foreach($this->data as $key => &$value){
            $value['editable'] = false;
        }*/


        $this->showInEditForm = array(
            'tab_main'=>array(
                'parent_uid','type_paiement','version','tpe','date','devise','montant','mail','reference','texte-libre','lge','societe','MAC','3dsdebrayable',
                //'protocole','aliascb','forcesaisiecb',
                //'nbrech','dateech1','montantech1','dateech2','montantech2','dateech3','montantech3','dateech4','montantech4',
            ),
        );



        //static::$postFormHTML[] = '<script>';
        //static::$postFormHTML[] = "</script>";


    }





    static function listLanguages(){
        return array(
            'FR' => 'FR',
            'EN' => 'EN',
            'DE' => 'DE',
            'IT' => 'IT',
            'ES' => 'ES',
            'NL' => 'NL',
            'PT' => 'PT',
            'SV' => 'SV',
        );
    }



    /**
     * Returns the payment form and stores the generated MAC value during form generation into current object.
     * It is recommended to call $this->store() after call to $this->getForm() in order to retain in database the value of the field $this->data['MAC']['value'] which is generated with the form
     */
    function getForm(){

        //echo "test=".$GLOBALS['config']['plugins']['monetico']['forceTestPayments'];

        $tpe = new MoneticoPaiement_Ept($this->data['lge']['value']);
        $hmac = new MoneticoPaiement_Hmac($tpe);


        // Control String for support
        $CtlHmac = sprintf(MONETICOPAIEMENT_CTLHMAC, $tpe->sVersion, $tpe->sNumero, $hmac->computeHmac(sprintf(MONETICOPAIEMENT_CTLHMACSTR, $tpe->sVersion, $tpe->sNumero)));

        $html = array();
        $options='';

        if($this->data['aliascb']['value'] != '')$options .= '&aliascb='.$this->data['aliascb']['value'];
        if($this->data['forcesaisiecb']['value'] == 1)$options .= '&forcesaisiecb=1';
        if($this->data['3dsdebrayable']['value'] == 1)$options .= '&3dsdebrayable=1';

        $options = ltrim($options,'&');

        $dateArray = explode (" ",str_replace ('.','/',$this->data['date']['value']));
        $dateFormated = $dateArray[0].":".$dateArray[1];

        // Data to certify
        $phase1go_fields = sprintf(MONETICOPAIEMENT_PHASE1GO_FIELDS,     $tpe->sNumero,
            $dateFormated,
            $this->data['montant']['value'],
            $this->data['devise']['value'],
            $this->data['reference']['value'],
            $this->data['texte-libre']['value'],
            $tpe->sVersion,
            $tpe->sLangue,
            $tpe->sCodeSociete,
            $this->data['mail']['value'],
            $this->data['nbrech']['value'],
            $this->data['dateech1']['value'],
            $this->data['montantech1']['value'],
            $this->data['dateech2']['value'],
            $this->data['montantech2']['value'],
            $this->data['dateech3']['value'],
            $this->data['montantech3']['value'],
            $this->data['dateech4']['value'],
            $this->data['montantech4']['value'],
            $options);

        // MAC computation
        $sMAC = $hmac->computeHmac($phase1go_fields);

        //record MAC value
        $this->data['MAC']['value'] = $sMAC;

        $html[] = '
        <form action="'.$tpe->sUrlPaiement.'" method="post" id="PaymentRequest">
            <p>
                <input type="hidden" name="version"             id="version"        value="'.$tpe->sVersion.'" />
                <input type="hidden" name="TPE"                 id="TPE"            value="'.$tpe->sNumero.'" />
                <input type="hidden" name="date"                id="date"           value="'.$dateFormated.'" />
                <input type="hidden" name="montant"             id="montant"        value="'.$this->data['montant']['value'] . $this->data['devise']['value'].'" />
                <input type="hidden" name="reference"           id="reference"      value="'.$this->data['reference']['value'].'" />
                <input type="hidden" name="MAC"                 id="MAC"            value="'.$sMAC.'" />
                <input type="hidden" name="url_retour"          id="url_retour"     value="'.$tpe->sUrlKO.'" />
                <input type="hidden" name="url_retour_ok"       id="url_retour_ok"  value="'.$tpe->sUrlOK.'" />
                <input type="hidden" name="url_retour_err"      id="url_retour_err" value="'.$tpe->sUrlKO.'" />
                <input type="hidden" name="lgue"                id="lgue"           value="'.$tpe->sLangue.'" />
                <input type="hidden" name="societe"             id="societe"        value="'.$tpe->sCodeSociete.'" />
                <input type="hidden" name="texte-libre"         id="texte-libre"    value="'.HtmlEncode($this->data['texte-libre']['value']).'" />
                <input type="hidden" name="mail"                id="mail"           value="'.$this->data['mail']['value'].'" />';
        $html[] = '<input type="hidden" name="options" id="options" value="'.$options.'" />';
        /*if($this->data['aliascb']['value'] != '')$html[] = '<input type="hidden" name="aliascb" id="aliascb" value="'.$this->data['aliascb']['value'].'" />';
        if($this->data['forcesaisiecb']['value'] == 1)$html[] = '<input type="hidden" name="forcesaisiecb" id="forcesaisiecb" value="'.$this->data['forcesaisiecb']['value'].'" />';
        if($this->data['3dsdebrayable']['value'] == 1)$html[] = '<input type="hidden" name="3dsdebrayable" id="3dsdebrayable" value="'.$this->data['3dsdebrayable']['value'].'" />';*/


        // SECTION PAIEMENT FRACTIONNE - Section spécifique au paiement fractionné
        // INSTALLMENT PAYMENT SECTION - Section specific to the installment payment

        if($this->data['nbrech']['value'] != ''){
            $html[] = '<input type="hidden" name="nbrech"              id="nbrech"         value="'.$this->data['nbrech']['value'].'" />';
            for($i=1;$i<5;$i++){
                if($this->data['dateech'.$i]['value'] != ''){
                    $html[] = '<input type="hidden" name="dateech'.$i.'"            id="dateech'.$i.'"       value="'.$this->data['dateech'.$i]['value'].'" />
                    <input type="hidden" name="montantech'.$i.'"         id="montantech'.$i.'"    value="'.$this->data['montantech'.$i]['value'].'" />';
                }
            }

        }
        //<input type="submit" name="bouton"              id="bouton"         value="Connexion / Connection" />

        $html[] = '
            </p>
            </form>
        ';

        return implode(chr(10),$html);
    }

}















