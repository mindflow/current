<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'tab_portfolioCategory' => 'Properties',
        'name' => 'Category name',
        'description' => 'Category description',
        'image' => 'Associated image (60x60px)',
        'category' => 'Category',
        'parent_uid' => 'Parent category',

        /*mandatory records*/
        //'no_record' => 'There currently exists no portfolio category. You can <a href="{subdir}/mf/index.php?module=portfolio&action=create&class=portfolioCategory">create a category</a>.',
        'record_name' => 'Portfolio category',

    ),

    'fr' => array(
        'tab_portfolioCategory' => 'Propriétés',
        'name' => 'Nom de la catégorie',
        'description' => 'Description de la catégorie',
        'image' => 'Image associée (60x60px)',
        'category' => 'Catégorie',
        'parent_uid' => 'Catégorie parente',

        /*mandatory records*/
        //'no_record' => 'Il n\'existe pas de catégorie pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=portfolio&action=create&class=portfolioCategory">créer une catégorie</a>.',
        'record_name' => 'Catégorie de portfolio',

    ),
);

?>