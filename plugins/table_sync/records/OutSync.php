<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class OutSync extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_sync_out';
    //static $oneTablePerLocale = true;

    static $createTableSQL= "
        #classe et données concernée
        `local_table` varchar(30) DEFAULT '',
        `local_uid` varchar(100) DEFAULT '',
        `local_data` longblob,
        
        #nom de la règle d'envoi appliquée
        `out_rule` varchar(30) DEFAULT '',
        
        #système cible
        `destination` varchar(1024) DEFAULT '',
        
        #etat et résultat de l'opération
        `state` varchar(10) DEFAULT 'stopped',
        `result` varchar(10) DEFAULT '',
        
        #log des opérations
        `local_log` longtext,
        `remote_log` longtext,
       
        
    ";

    static $createTableKeysSQL="
          KEY `k_local_table` (`local_table`),
          KEY `k_local` (`local_table`,`local_uid`)
        ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init()
    {
        parent::init();

        global $mf,$l10n;

        $this->data = array_merge($this->data, array(

            'local_table' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
            ),
            'local_uid' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
            ),
            'local_data' => array(
                'value' => '',
                'dataType' => 'textarea 30 5',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-md-8'),
                'noEcho' => 1,
            ),
            'out_rule' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
            ),
            'destination' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-3'),
            ),
            'state' => array(
                'value' => 'Inactive',
                'dataType' => 'select',
                'possibleValues' => array(
                    'Scheduled' => $l10n->getLabel('OutSync','Scheduled'),
                    'Sending' => $l10n->getLabel('OutSync','Sending'),
                    'Inactive' => $l10n->getLabel('OutSync','Inactive'),
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
                'editable' => false,
            ),
            'result' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'Success' => $l10n->getLabel('OutSync','Success'),
                    'Failure' => $l10n->getLabel('OutSync','Failure'),
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
                'editable' => false,
            ),
            'local_log' => array(
                'value' => '',
                'dataType' => 'textarea 50 8',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-md-8'),
                //'editable' => false,
            ),
            'remote_log' => array(
                'value' => '',
                'dataType' => 'textarea 50 8',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-md-8'),
                //'editable' => false,
            ),

        ));


        $this->showInEditForm = array(

            'tab_main'=>array(
                'local_table','local_uid','out_rule','destination','state','result','local_log','remote_log',
            )
        );


        static::$postFormHTML[] = "<script></script>";

    }

	/**
	 * Quickly updates a column value in the database without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $name the column name in the database
	 * @param String $value the value to set
	 */
	function setField($name,$value){
    	global $db,$pdo,$logger;

    	try {
		    $sql = "UPDATE " . self::getTableName() . " SET " . $name . "=" . $pdo->quote( $value ) . " WHERE uid=" . $this->data['uid']['value'] . ";";
		    $pdo->query( $sql );

		    $this->data[ $name ]['value'] = $value;
	    }
	    catch(PDOException $e){
    		$logger->critical("OutSync setField PDOException code=".$e->getCode()." message=".$e->getMessage());
	    }

	}

	/**
	 * Quickly updates the local_log field without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $logMessage text of the log entry
	 */
	function addToLocalLog($logMessage){
		global $pdo;

		$now = new DateTime('NOW');
		$nowStr = $now->format('d/m/Y H:i:s');

		$this->data['local_log']['value'] .= $nowStr.' : '.$logMessage;
		$sql = "UPDATE " . self::getTableName() . " SET local_log=" . $pdo->quote($this->data['local_log']['value']) . " WHERE uid=" . $this->data['uid']['value'] . ";";
		$pdo->query( $sql );
	}

	static function getState($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;

			$sec = getSec(array('action' => 'replayOut', 'uid' => $row['uid']));
			$resetSec = getSec(array('action' => 'resetState', 'uid' => $row['uid'], 'record_class' => 'OutSync'));

			switch($value){
				case 'Inactive':
					return '<span id="state_'.$row['uid'].'"><span class="darkgrey">'.$l10n->getLabel('OutSync','Inactive').' &nbsp;<a onclick="replay('.$row['uid'].',\''.$sec->hash.'\');" class="play" title="'.$l10n->getLabel('OutSync','replay').'"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a></span></span>';

				case 'Sending':
					return '<span id="state_'.$row['uid'].'"><span class="orange">'.$l10n->getLabel('OutSync','Sending').'</span> &nbsp;<img src="'.SUB_DIR.'/mf/backend/templates/mf-default/img/small-loading.gif" />&nbsp;<a onclick="resetState('.$row['uid'].',\'OutSync\',\''.$resetSec->hash.'\');" class="play" title="'.$l10n->getLabel('FullSync','reset').'"><span class="glyphicon glyphicon-stop" aria-hidden="true"></span></a></span>';

				case 'Scheduled':
					return '<span id="state_'.$row['uid'].'"><span class="blue">'.$l10n->getLabel('OutSync','Scheduled').' &nbsp;<a onclick="replay('.$row['uid'].',\''.$sec->hash.'\');" class="play" title="'.$l10n->getLabel('OutSync','replay').'"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a></span></span>';
			}
		}
		else return '<span id="state_'.$row['uid'].'"></span>';
	}

	static function getResult($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;
			switch($value){
				case 'Success':
					return '<span id="result_'.$row['uid'].'"><span class="green">'.$l10n->getLabel('OutSync','Success').'</span></span>';

				case 'Failure':
					return '<span id="result_'.$row['uid'].'"><span class="red">'.$l10n->getLabel('OutSync','Failure').'</span></span>';
			}
		}
		else return '<span id="result_'.$row['uid'].'"></span>';
	}


	static function getLocalUid($key,$value,$locale,$row){
		if($value != '*' && $value != ''){
			$uids = explode(',',$value);
			if(count($uids) > 3) {
				return $uids[0].','.$uids[1].','.$uids[2].','.'...';
			}
			else return $value;
		}
		else return $value;
	}



	/**
	 * Checks the database for tasks list
	 * This function must be called regularly by a cron task / mindflow's cron.php file
	 *
	 */
	static function fireSync(){
		global $pdo, $logger, $config;

		$logger->debug( 'OutSync::fireSync()' );

		//as the cron script is called every minute, numerous instances of the script mays stack
		//echo 'ps aux | grep "'.$_SERVER['SCRIPT_FILENAME'].'"';
		exec('ps aux | grep "'.$_SERVER['SCRIPT_FILENAME'].'"', $output);
		//$logger->debug( 'SqlOutSync : '.'ps aux | grep "'.$_SERVER['SCRIPT_FILENAME'].'"' );
		//$logger->debug( print_r($output,true) );


		$output = implode(chr(10),$output);
		$nbOccurences = substr_count ( $output , "php ".$_SERVER['SCRIPT_FILENAME']);

		//do not run more than 2 instances of this script concurrently
		if($nbOccurences < 5){

			//List and process all scheduled operations
			$sql = "SELECT * FROM ".self::getTableName()." WHERE deleted=0 AND state='Scheduled' ORDER BY creation_date ASC";
			$stmt = $pdo->query($sql);

			$logger->info("OutSync fireSync() found ".$stmt->rowCount()." rows to process");

			if($stmt->rowCount() > 0) {

				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

				foreach($rows as $row) {

					// now we need to reload each row right before processing it in order to make sure it has not been
					// already processed by another iteration of the cron task
					$sql = "SELECT * FROM ".self::getTableName()." WHERE deleted=0 AND state='Scheduled' AND uid = ".$row['uid'];
					$stmt2 = $pdo->query($sql);
					if($stmt2->rowCount() > 0) {

						$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

						if($row2)self::execSync( $row2 );
						else $logger->critical('ERROR OutSync::fireSync SQL='.$sql);
					}
					//else skip the row
				}
			}

			//purge old records and prevent database bloating
			if(isset($config['plugins']['table_sync']['purge-delay'])) {
				$purgeDelay = $config['plugins']['table_sync']['purge-delay'];
			}
			else $purgeDelay = 60; //value in days

			$sql  = "DELETE FROM " . OutSync::getTableName() . " WHERE creation_date < DATE_SUB(NOW(), INTERVAL ".$purgeDelay." DAY);  ";
			//$logger->debug( $sql );
			$pdo->query( $sql );

			$sql  = "DELETE FROM " . InSync::getTableName() . " WHERE creation_date < DATE_SUB(NOW(), INTERVAL ".$purgeDelay." DAY);  ";
			//$logger->debug( $sql );
			$pdo->query( $sql );

		}
		else $logger->debug( 'OutSync : '.$nbOccurences.' instances already running. Aborting.' );




	}


	static function execSync(Array $row){
		global $pdo,$logger,$mf,$config,$l10n;

		$syncConfig = $config['plugins']['table_sync'];

$logger->debug("Running OutSync::execSync() on uid=".$row['uid']);

		$out = new OutSync();
		$out->load($row['uid']);

		$ruleName = $out->data['out_rule']['value'] ;
		$rule = $syncConfig['out_rules'][$ruleName];

		if ( isset( $rule['sendFunc'] ) ) {
			//apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
			$sendFunc = $rule['sendFunc'];
		} else {
			//or use default JSON based protocol function
			$out->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sendFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
			$sendFunc = "SyncManager::jsonOutRequest";
		}

		//execute the defined sendFunc and effectively transmit the data
		$sendFunc( $out );
		$out->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sending_performed' ) . chr( 10 ) );

	}
}















