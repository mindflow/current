<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
//{module_include_record}

//{module_include_form}



class SyncManager implements module{

    private $moduleKey = 'SyncManager';
    private $moduleType = 'backend';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';

    //javascripts généraux au module
    var $scripts='';


    function __construct(){

    }


    function prepareData()
    {
        global $mf,$l10n;

        $this->pageOutput = '';

        //[prepareData_backend]
		if($mf->mode == 'backend') {

            //process backend display
            $userGroup = $mf->currentUser->getUserGroup();

            $editSync = (($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('table_sync','allowEditSync')==1))));

            if ($editSync) {
                $this->localMenu = '';

                //if(!$mf->pluginManager->backendMenuExists('prod'))$mf->pluginManager->addBackendMenu('administration', MEDIUM_PRIORITY);

                //add the plugin to the backend menus
                $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=SyncManager"><span class="glyphicon glyphicon-menu glyphicon-refresh" aria-hidden="true"></span>' . $l10n->getLabel('SyncManager', 'menu_title') . '</a>', 'administration', TOP_PRIORITY);

                if (isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])) {

                    //set default submodule
                    if(!isset($_REQUEST['submodule'])|| $_REQUEST['submodule']=='')$_REQUEST['submodule']='OutSyncManager';

                    //on selectionne l'utilisateur dont on visualise les tâches


                    //breadcrumb
                    $this->section = ''; //$l10n->getLabel('backend', 'prod').' > ';
	                $this->moduleName = '<span class="glyphicon glyphicon-menu glyphicon-refresh" aria-hidden="true"></span>' . $l10n->getLabel('SyncManager', 'menu_title');



	                if($editSync) {
	                    $this->localMenu .= '<li rel="OutSyncManager"' . ( ( $_REQUEST['submodule'] == 'OutSyncManager' ) ? ' class="active"' : '' ) . '><a href="{subdir}/mf/index.php?module=SyncManager&submodule=OutSyncManager"> <span class="glyphicon glyphicon-menu glyphicon-arrow-right" aria-hidden="true"></span> ' . $l10n->getLabel( 'OutSyncManager', 'menu_title' ) . '</a></li>';
	                    $this->localMenu .= '<li rel="InSyncManager"' . ( ( $_REQUEST['submodule'] == 'InSyncManager' ) ? ' class="active"' : '' ) . '><a href="{subdir}/mf/index.php?module=SyncManager&submodule=InSyncManager"> <span class="glyphicon glyphicon-menu glyphicon-arrow-left" aria-hidden="true"></span> ' . $l10n->getLabel( 'InSyncManager', 'menu_title' ) . '</a></li>';
	                    $this->localMenu .= '<li rel="FullSyncManager"' . ( ( $_REQUEST['submodule'] == 'FullSyncManager' ) ? ' class="active"' : '' ) . '><a href="{subdir}/mf/index.php?module=SyncManager&submodule=FullSyncManager"> <span class="glyphicon glyphicon-menu glyphicon-refresh" aria-hidden="true"></span> ' . $l10n->getLabel( 'FullSyncManager', 'menu_title' ) . '</a></li>';
                     	$this->localMenu .='<li rel="SqlOutSyncManager"'.(( $_REQUEST['submodule'] == 'SqlOutSyncManager' )?' class="active"':'') . '><a href="{subdir}/mf/index.php?module=SyncManager&submodule=SqlOutSyncManager"> <span class="glyphicon glyphicon-menu  glyphicon-tasks" aria-hidden="true"></span> ' . $l10n->getLabel( 'SqlOutSyncManager','menu_title') . '</a></li>';
	                	$this->localMenu .='<li rel="SqlInSyncManager"'.(( $_REQUEST['submodule'] == 'SqlInSyncManager' )?' class="active"':'') . '><a href="{subdir}/mf/index.php?module=SyncManager&submodule=SqlInSyncManager"> <span class="glyphicon glyphicon-menu  glyphicon-tasks" aria-hidden="true"></span> ' . $l10n->getLabel( 'SqlInSyncManager','menu_title') . '</a></li>';
	                }

	                if(isset($_REQUEST['submodule'])){

                        if($_REQUEST['submodule']=='FullSyncManager'){

                            $this->FullSyncManager = new FullSyncManager();
                            $this->FullSyncManager->prepareData();
                        }
                        else if($_REQUEST['submodule']=='OutSyncManager'){
                            
                            $this->OutSyncManager = new OutSyncManager();
                            $this->OutSyncManager->prepareData();
                        }
                        else if($_REQUEST['submodule']=='InSyncManager'){

                            $this->InSyncManager = new InSyncManager();
                            $this->InSyncManager->prepareData();
                        }
                        else if( $_REQUEST['submodule'] == 'SqlOutSyncManager' ){

	                        $this->SqlOutSyncManager = new SqlOutSyncManager();
	                        $this->SqlOutSyncManager->prepareData();
                        }
                        else if( $_REQUEST['submodule'] == 'SqlInSyncManager' ){

	                        $this->SqlInSyncManager = new SqlInSyncManager();
	                        $this->SqlInSyncManager->prepareData();
                        }
                    }

                    $this->scripts = '<script></script>';



                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >' . $l10n->getLabel('backend', 'module_no_access') . '</div>';
                $this->localMenu = '';
            }
        }
		//[/prepareData_backend]
		//{module_prepareData}
    }


    function render(&$mainTemplate){
        global $mf,$l10n;

        //[render_backend]
		if($mf->mode == 'backend') {

            //process backend display
            if (isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])) {

                $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/table_sync/ressources/templates/table_sync.html');

                $mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);
                $mainTemplate = str_replace("{section}",$this->section,$mainTemplate);

                $mainTemplate = str_replace("{module-name}",$this->moduleName,$mainTemplate);
                $mainTemplate = str_replace("{module-id}",$this->moduleKey,$mainTemplate);
                $mainTemplate = str_replace("{local-menu}",$this->localMenu,$mainTemplate);


                if(isset($_REQUEST['submodule'])){
                    if($_REQUEST['submodule']=='FullSyncManager'){
                        $this->FullSyncManager->render($mainTemplate);
                    }
                    else if($_REQUEST['submodule']=='OutSyncManager'){
                        $this->OutSyncManager->render($mainTemplate);
                    }
                    else if($_REQUEST['submodule']=='InSyncManager'){
                        $this->InSyncManager->render($mainTemplate);
                    }
                    else if( $_REQUEST['submodule'] == 'SqlOutSyncManager' ){
	                    $this->SqlOutSyncManager->render($mainTemplate);
                    }
                    else if( $_REQUEST['submodule'] == 'SqlInSyncManager' ){
	                    $this->SqlInSyncManager->render($mainTemplate);
                    }
                }

                $mainTemplate = str_replace("{scripts-taches}",$this->scripts,$mainTemplate);
            }
        }
		//[/render_backend]
		//{module_render}
    }


    function getType(){
        return $this->moduleType;
    }


	/**
	 * Send an object to another host.
	 *
	 * @param $object
	 * @param $destination
	 */
  /*  static function sendObject($object,$destination)
    {
        global $config,$l10n;

        $syncConfig = $config['plugins']['table_sync'];
echo "sendObject('object',$destination)";

        $objClass = get_class($object);
        $objUid = $object->data['uid']['value'];

        $out = new OutSync();
        $out->data['local_class']['value'] = $objClass;

        // we will store our objects in an array where they are indexed by their uid
        $objects = array();

        // we only store / transmit the values in the object
	    // so we strip away the template info in order to reduce the size of the object
	    $objects[$objUid] = array();

	    foreach($object->data as $key => $keyData) {
		    if ( array_key_exists( 'value', $keyData ) ) {

			    if ( isset( $keyData['dataType'] ) ) {

				    $dataType = explode( ' ', trim( $keyData['dataType'] ) );
				    $fieldType = $dataType[0];

				    //dummy fields are ignored, only real fields are kept
				    if ( ! in_array( $fieldType, dbRecord::$dummyFieldTypes ) ) {

					    if($fieldType != 'template_data') {
					    	//main case, typical fields, we only keep the content of the 'value' attribute
						    $objects[$objUid][$key]['value'] = $keyData['value'];
					    }
					    else{
					    	//template fields need to have their template definition removed from their 'value' attribute
						    $objects[$objUid][$key]['value'] = dbRecord::stripTemplateInfo($keyData['value']);
					    }
				    }
			    }
		    }
	    }


	    //compressing data for storage in the database and for sending
	    $out->data['local_data']['value'] = gzcompress(serialize($objects),9);
	    $out->data['local_uid']['value'] = $objUid;
	    $out->data['destination']['value'] = $destination;

	    $out->store();

	    if(trim($destination) != '') {


            //seek the first rule matching the object class and the specified destination
		    foreach($syncConfig['out_rules'] as $ruleLabel => $ruleData){
			    if($ruleData['local_class']==$objClass){
				    if(in_array($destination,$ruleData['sources'])){
					    $out->data['out_rule']['value'] = $outRule = $ruleLabel;
					    //exit at first matching rule found
					    break;
				    }
			    }
		    }

		    if(!isset($outRule)){
			    //error, no matching send rule found, failure to send object
			    $out->data['result']['value'] = 'Failure';
			    $out->data['local_log']['value'] .= $l10n->getLabel('SyncManager','rule_not_found').chr(10);
			    $out->store();
		    }
		    else{
		    	//rule found, send object
			    $out->data['local_log']['value'] .= $l10n->getLabel('SyncManager','rule_out_found').chr(10);
			    $out->store();

			    self::jsonOutRequest($out);
		    }


	    }
	    else {
	    	//empty destination specified, failure to send object
		    $out->data['local_log']['value'] .= $l10n->getLabel('SyncManager','sendObject_empty_destination').chr(10);
		    $out->data['result']['value'] = 'Failure';

		    $out->store();
	    }

    }*/






	/**
	 * Syncs an object with the hosts specified into $ruleName.
	 * The function assumes the object has already been saved into the database, as its values will be reloaded from the database.
	 *
	 * @param $object an instance of an object to send
	 * @param $ruleName the name of the rule to apply
	 * @return OutSync the OutSync object created for the outgoing sync
	 */
	static function syncObject($object,$ruleName, $sendImmediately = true)
	{
		return self::sendRecordsFromDB($object::getTableName(), array($object->data['uid']['value']) , $ruleName,true, $sendImmediately);
	}



	/**
	 * Syncs a whole table with hosts specified in the rule $ruleName.
	 *
	 * @param $tableName the name of the table to sync in MySQL
	 * @param $ruleName the name of the rule to apply
	 * @return OutSync the OutSync object created for the outgoing sync
	 */
	static function syncTable($tableName,$ruleName, $sendImmediately = true)
	{
		return self::sendRecordsFromDB($tableName, true , $ruleName,true, $sendImmediately);
	}


	/**
	 * Sends a set of rows from the database to a remote host
	 *
	 * @param $object_class class name of the records to synchronize
	 * @param array $uids array of uids of records to synchronise, OR true if synchronizing the whole table, the uids being the values of the primary key.
	 *                    The primary key column name can be changed from 'uid' to something else for synchronizing non-MindFlow tables, by setting the parameter $primaryKeyName
	 * @param string $ruleName ruleName to apply to this sync. Must be referenced in $config['plugins']['table_sync']['out_rules'].
	 * @param bool $includeDeleted syncs deleted records as weel if true
	 * @param bool $sendImmediately makes the connection immediately, but will likely slow down the UI when  sendRecordsFromDB() or syncObject() are called in the postStore() trigger of an object.
	 * When set to false, asynchronous sending is triggered by the cron.php script, but you must make sure it is called regularly by the cron service of your web hosting
	 * @param string $primaryKeyName the name of the column to look for as primary key
	 * @return OutSync the OutSync object created for the outgoing sync
	 */
	static function sendRecordsFromDB($tableName, $uids, $ruleName, $includeDeleted = false, $sendImmediately = true, $primaryKeyName='uid') {
		global $config, $l10n, $pdo, $logger;

		$execution_start = time();
		$logger->info("SyncManager::sendRecordsFromDB initiated, ruleName=".$ruleName);

		$syncConfig = $config['plugins']['table_sync'];

		//echo "sendObject('object',$ruleName)";

		//create our outSync object for this request
		$out = new OutSync();
		$out->data['local_table']['value'] = $tableName;
		$out->store();


		if ( isset( $syncConfig['out_rules'][ $ruleName ] ) ) {

			$rule = $syncConfig['out_rules'][ $ruleName ];
			//rule found, send object
			$out->data['out_rule']['value'] = $ruleName;

			$out->addToLocalLog($l10n->getLabel('SyncManager','rule_out_found').chr(10));
			$logger->debug($l10n->getLabel('SyncManager','rule_out_found'));

		}
		else {
			//error, no matching send rule found, failure to send object
			$rule = false;

			$out->setField('result','Failure');

			$errMsg = $l10n->getLabel('SyncManager','empty_destination');

			$out->addToLocalLog($errMsg.chr(10));
			$logger->critical("sendRecordsFromDB FAILED : ".$errMsg);

		}

		//seek for the destinations
		if($rule) {
			$destinations = $syncConfig['out_rules'][$ruleName]['destinations'];

			if(is_array($destinations)){
				$out->data['destination']['value'] = implode(',',$destinations);
				$logger->debug("destinations = ".$out->data['destination']['value']);
			}
			else {
				//empty destination specified, failure to send object
				$out->setField('result','Failure');

				$errMsg = $l10n->getLabel('SyncManager','empty_destination');

				$out->addToLocalLog($errMsg.chr(10));
				$logger->critical("sendRecordsFromDB FAILED : ".$errMsg);

			}
		}

		$out->store();

		if($rule && is_array($destinations)) {

			if(isset($rule['filter_fields'])){
				if(is_array($rule['filter_fields'])){
					$filterfields = $rule['filter_fields'];
				}
				else{
					$out->addToLocalLog($l10n->getLabel('SyncManager','filter_fields_not_array').$ruleName.chr(10));
					$filterfields = array();
				}
			}
			else{
				$out->addToLocalLog($l10n->getLabel('SyncManager','filter_fields_not_defined').$ruleName.chr(10));
				$filterfields = array();
			}
			$logger->debug("filter_fields = ".implode(',',$filterfields));

			if(isset($rule['key_processors'])){
				if(is_array($rule['key_processors'])){
					$keyProcessors = $rule['key_processors'];
				}
				else{
					$out->addToLocalLog($l10n->getLabel('SyncManager','key_proc_not_array').$ruleName.chr(10));
					$keyProcessors = array();
				}
			}
			else{
				$out->addToLocalLog($l10n->getLabel('SyncManager','key_proc_not_defined').$ruleName.chr(10));
				$keyProcessors = array();
			}
			$logger->debug("key_processors = ".implode(',',array_keys($keyProcessors)));


			if(isset($rule['remap_fields'])){
				if(is_array($rule['remap_fields'])){
					$remapFields = $rule['remap_fields'];
				}
				else{
					$out->addToLocalLog($l10n->getLabel('SyncManager','remap_not_array').$ruleName.chr(10));
					$remapFields = array();
				}
			}
			else{
				$out->addToLocalLog($l10n->getLabel('SyncManager','remap_not_defined').$ruleName.chr(10));
				$remapFields = array();
			}
			$logger->debug("remap_fields = ".implode(',',array_keys($remapFields)));

			if(isset($rule['match_locales'])){
				if(is_array($rule['match_locales'])){
					$matchLocales = $rule['match_locales'];
				}
				else{
					$out->addToLocalLog($l10n->getLabel('SyncManager','locales_not_array').$ruleName.chr(10));
					$matchLocales = array();
				}
			}
			else{
				$out->addToLocalLog($l10n->getLabel('SyncManager','locales_not_defined').$ruleName.chr(10));
				$matchLocales = array();
			}
			$logger->debug("match_locales = ".implode(',',$matchLocales));

			//extract the requested object data from the database

			if ( is_array($uids) ) {
				//list of uids supplied
				$uidAsString = implode( ',', $uids );
				$sqlWhere = $primaryKeyName." IN (" . $uidAsString . ")";
			}
			else if($uids === true){
				//sync whole table
				$sqlWhere = "1";
				$uidAsString = '*';
			}


			if ( $includeDeleted ) {
				$sqlDeleted = "";
			} else {
				$sqlDeleted = " AND DELETED=0";
			}

			if(count($matchLocales) > 0){
				//list of locales supplied
				$localesList = '';
				foreach($matchLocales as $setLocale){
					$localesList .= "'".$setLocale."',";
				}
				$localesList = rtrim( $localesList,',');
				$sqlLocales = " AND language IN (" . $localesList . ")";
			}
			else $sqlLocales = '';

			$data = array();

			try {
				$sql = "SELECT * FROM " . $tableName . " WHERE " . $sqlWhere . $sqlDeleted . $sqlLocales; //." LIMIT 1000"

				$logger->debug( "sendRecordsFromDB SQL =" );
				$logger->debug( $sql );
				$out->addToLocalLog("SQL=".$sql.chr(10));

				$stmt = $pdo->query( $sql );


				$rows    = $stmt->fetchAll( PDO::FETCH_ASSOC );
				$numRows = $stmt->rowCount();

				$logger->debug( "Found " . $numRows . " rows" );

				if ( $numRows > 0 ) {

					foreach ( $rows as $row ) {

						$data[ $row['uid'] ] = array();

						foreach ( $row as $colName => $colData ) {

							//filtering fields forbiden in the OUT rule
							if ( ! in_array( $colName, $filterfields ) ) {

								//remap field if necessary
								if ( isset( $remapFields[ $colName ] ) ) {
									$colName = $remapFields[ $colName ];
								}

								if ( isset( $keyProcessors[ $colName ] ) ) {
									//process key for display with optional user supplied processing function in OUT rule
									eval( '$data[ $row["uid"] ][ $colName ] = ' . $keyProcessors[ $colName ] . '($colName,$colData,$row["language"],$row);' );

								} else {
									$data[ $row['uid'] ][ $colName ] = $colData;
								}
							}
						}
					}

					//print_r( $data );

					$out->data['local_uid']['value'] = $uidAsString;
					//echo "data=".print_r( $data, true);

					//compressing data for storage in the database and for sending
					$out->data['local_data']['value'] =  gzcompress(serialize( $data ), 9 );

					//$logfile = DOC_ROOT.SUB_DIR.'/mf_logs/sent.log';
					//file_put_contents($logfile, $out->data['local_data']['value']);

					$out->store();


					if ( isset( $rule['sendFunc'] ) ) {
						//apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
						$sendFunc = $rule['sendFunc'];
					} else {
						//or use default JSON based protocol function
						$out->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sendFunc_not_set' ) . ' ' . $ruleName . chr( 10 ) );
						$sendFunc = "SyncManager::jsonOutRequest";
					}

					//execute the defined sendFunc and effectively transmit the data
					if ( $sendImmediately ) {
						$sendFunc( $out );
						$out->addToLocalLog( $numRows . ' ' . $l10n->getLabel( 'SyncManager', 'num_rows_sent' ) . chr( 10 ) );
					} else {
						$out->setField( 'state', 'Scheduled' );
					}


				} else {
					$out->setField( 'result', 'Success' );

					$errMsg = $l10n->getLabel( 'SyncManager', 'no_rows_found' );

					$out->addToLocalLog( $errMsg . chr( 10 ) );
					$logger->info( "sendRecordsFromDB : " . $errMsg );

				}
			}
			catch(PDOException  $e){
				ob_start();
				echo "sendRecordsFromDB ERROR:".chr(10).$e->getMessage().chr(10).
				     "ERROR=".$pdo->errorInfo()[2].chr(10).
				     "SQL=".$sql.chr(10).chr(10);

				//debug_print_backtrace();

				$errorMsg = ob_get_clean();
				$logger->critical($errorMsg);
				$out->addToLocalLog( $errorMsg );
				$out->setField( 'result', 'Failure' );

			}
		}

		$logger->info("out result=".$out->data['result']['value']);
		$logger->info("out state=".$out->data['state']['value']);

		$logger->info("sendRecordsFromDB END duration : ". ((time() - $execution_start)) . " secondes");
		return $out;
	}

	/**
	 * Sends an SQL request to be executed on the remote host
	 * @param $sql
	 */
	static function sendSQLRequest($sql,$ruleName,$sendImmediately = true){
		global $config, $l10n, $pdo, $logger;

		$execution_start = time();
		$logger->info("sendSQLRequest START ruleName=".$ruleName);


		$syncConfig = $config['plugins']['table_sync'];

		$sqlOut = new sqloutSync();
		$sqlOut->data['sql_request']['value'] = $sql;
		$sqlOut->store();

		if ( isset( $syncConfig['sql_out_rules'][ $ruleName ] ) ) {

			$rule = $syncConfig['sql_out_rules'][ $ruleName ];
			//rule found, send object
			$sqlOut->data['sql_rule']['value'] = $ruleName;

			$sqlOut->addToLocalLog($l10n->getLabel('SyncManager','rule_out_found').chr(10));
		}
		else {
			//error, no matching send rule found, failure to send object
			$rule = false;

			$sqlOut->setField('result','Failure');

			$errMsg = str_replace('{nom}', $ruleName,$l10n->getLabel('SyncManager','rule_not_found_name'));

			$sqlOut->addToLocalLog($errMsg.chr(10));
			$logger->critical("sendSQLRequest FAILED : ".$errMsg);
		}

		//seek for the destinations
		if($rule) {
			$destinations = $syncConfig['sql_out_rules'][$ruleName]['destinations'];

			if(is_array($destinations)){
				$sqlOut->data['destination']['value'] = implode(',',$destinations);
			}
			else {
				//empty destination specified, failure to send object
				$sqlOut->setField('result','Failure');

				$errMsg = $l10n->getLabel('SyncManager','empty_destination');

				$sqlOut->addToLocalLog( $errMsg.chr(10));
				$logger->critical("sendSQLRequest FAILED : ".$errMsg);
			}
		}

		$sqlOut->store();

		if($rule && is_array($destinations)) {

			if ( isset( $rule['sendFunc'] ) ) {
				//apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
				$sendFunc = $rule['sendFunc'];
			} else {
				//or use default JSON based protocol function
				$sqlOut->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sendFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
				$sendFunc = "SyncManager::jsonSqlOutRequest";
			}

			//execute the defined sendFunc and effectively transmit the data
			if($sendImmediately) {
				$sendFunc( $sqlOut );
				$sqlOut->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sql_sent' ) . chr( 10 ).$sql );
			}
			else $sqlOut->setField( 'state', 'Scheduled' );


		}
		$logger->info("sqlOut result=".$sqlOut->data['result']['value']);
		$logger->info("sqlOut state=".$sqlOut->data['state']['value']);

		$logger->info("sendSQLRequest END duration : ". ((time() - $execution_start)) . " secondes");
		return $sqlOut;

	}


	/**
	 * Emits the HTTP(S) request for transmitting the object to the remote system
	 * the request will be encrypted only if an https url is specified in the 'interface_url' attribute of the selected rule.
	 * @param $out the OutSync object configured for the request
	 * @return OutSync the processed OutSync object containing the result of the request
	 */
    static function jsonOutRequest(&$out){
	    global $config,$l10n,$mf,$logger;

	    $execution_start = time();
	    $logger->info("jsonOutRequest START destination=".$out->data['destination']['value']." local_table=".$out->data['local_table']['value']." local_uid=".$out->data['local_uid']['value']);

	    $out->setField('state','Sending');
	    $out->setField('result','');

	    $syncConfig = $config['plugins']['table_sync'];

	    //reset remote log
	    $out->data['remote_log']['value'] = '';

	    if(isset($out->data['destination']['value']) && $out->data['destination']['value']!='')
	    	$destinations = explode(',',$out->data['destination']['value']);
	    else{
		    $destinations = array();
	    }

	    if(count($destinations)>0) {
		    foreach ( $destinations as $destination ) {

			    $out->addToLocalLog( 'Destination = '.$destination. chr( 10 ) );

			    if ( isset( $syncConfig['remote_hosts'][ $destination ] ) ) {
				    //URL de l'interface ou se connecter au serveur
				    if ( isset( $syncConfig['remote_hosts'][ $destination ]['interface_url'] ) ) {
					    $interfaceURL = $syncConfig['remote_hosts'][ $destination ]['interface_url'];
				    } else {
				    	$errMsg = $destination . ' ' . $l10n->getLabel( 'SyncManager', 'attr_not_found1' . ' ' . 'interface_url' . ' ' . 'attr_not_found2' );
					    $out->addToLocalLog( $errMsg . chr( 10 ) );
					    $logger->critical("jsonOutRequest FAILED : ".$errMsg);
				    }

				    //Mot de passe de connexion au serveur
				    if ( isset( $syncConfig['remote_hosts'][ $destination ]['password'] ) ) {
					    $password = $syncConfig['local_password'];
				    } else {
				    	$errMsg = $destination . ' ' . $l10n->getLabel( 'SyncManager', 'attr_not_found1' . ' ' . 'password' . ' ' . 'attr_not_found2' );
					    $out->addToLocalLog( $errMsg . chr( 10 ) );
					    $logger->critical("jsonOutRequest FAILED : ".$errMsg);

				    }

			    } else {
				    $out->addToLocalLog( $destination . ' ' . $l10n->getLabel( 'SyncManager', 'destination_not_found_in_config' ) . chr( 10 ) );
			    }


			    if ( isset( $interfaceURL ) && isset( $password ) ) {
				    $syncData = array(
					    'request_type' => 'base_sync',
					    'source'       => $_SERVER['SERVER_NAME'],
					    'password'     => $password,
					    'remote_table' => $out->data['local_table']['value'],
					    'remote_uid'   => $out->data['local_uid']['value'],
					    'remote_data'  => $out->data['local_data']['value'],
					);

				    $log = "SyncManager : out connexion of type ".$syncData['request_type'].chr(10).
				           "source = ".$_SERVER['SERVER_NAME'].chr(10).
				           "destination = ".$destination.chr(10).
				           "remote_table = ".$syncData['remote_table'].chr(10);

				    $logger->debug($log);

				    $headers = array('Content-Type: multipart/form-data; Content-Length: ' . strlen(http_build_query($syncData)));

				    if($out->data['local_data']['value'] != '') {
					    //effective connection to remote host
					    $ch = curl_init();
					    curl_setopt( $ch, CURLOPT_HEADER, 0 );
					    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
					    curl_setopt( $ch, CURLOPT_URL, $interfaceURL );
					    curl_setopt( $ch, CURLOPT_TIMEOUT, 0 );
					    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
					    //curl_setopt( $ch, CURLOPT_POST, 1 );
					    //curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
					    curl_setopt( $ch, CURLOPT_POSTFIELDS, $syncData );

					    $out->addToLocalLog( "Connexion Curl vers ".$interfaceURL." ...".chr(10) );

					    $curlResponse = curl_exec( $ch );
					    //echo "response=".$curlResponse.chr(10);

					    if ( $curlResponse === false ) {
						    // connexion failure
						    $out->setField( 'result', 'Failure' );
						    $out->setField( 'state', 'Inactive' );

						    $errMsg = curl_error( $ch );

						    $out->addToLocalLog( $errMsg . chr( 10 ) );
						    $logger->critical("jsonOutRequest FAILED : ".$errMsg);
					    }
					    else {

						    //decode and process json response
						    $response = json_decode( $curlResponse, true );

						    if ( json_last_error() == JSON_ERROR_NONE ) {

							    if ( $response['result'] == 'success' ) {
								    //We do set the result to success only if prior destinations haven't failed
								    if ( $out->data['result']['value'] != 'Failure' ) {
									    $out->setField( 'result', 'Success' );
								    }
								    $out->addToLocalLog( $l10n->getLabel( 'SyncManager', 'success_contacting_dest' ) . ' ' . $destination . chr( 10 ) . chr( 10 ) );

							    } else {
								    $out->setField( 'result', 'Failure' );

								    $errMsg = $l10n->getLabel( 'SyncManager', 'failed_contacting_dest' ) . ' ' . $destination;

								    $out->addToLocalLog( $errMsg . chr( 10 ) . chr( 10 ) );
								    $logger->critical("jsonOutRequest FAILED : ".$errMsg);

							    }

							    //logging the JSON response
							    $out->data['remote_log']['value'] .= chr(10);
							    $out->data['remote_log']['value'] .=  $response['log']. chr( 10 );

						    } else {

						    	//if the response is not JSON (likely it is a PHP error message)
							    $out->setField( 'result', 'Failure' );

							    $errMsg = $l10n->getLabel( 'SyncManager', 'failed_contacting_dest' ) . ' ' . $destination;

							    $out->addToLocalLog( $errMsg . chr( 10 ) . chr( 10 ) );
							    $logger->critical("jsonOutRequest FAILED : ".$errMsg);

							    //loging the CURL Response rather than the JSON response
							    $out->data['remote_log']['value'] .= chr(10);
							    $out->data['remote_log']['value'] .= $curlResponse. chr( 10 );
						    }
					    }

					    curl_close( $ch );
				    }
				    else{
					    $out->setField( 'result', 'Failure' );

					    $errMsg = $l10n->getLabel( 'SyncManager', 'empty_data' );

					    $out->addToLocalLog( $errMsg . chr( 10 ) . chr( 10 ) );
					    $logger->critical("jsonOutRequest FAILED : ".$errMsg);

				    }

			    } else {
				    $out->setField( 'result', 'Failure' );
			    }

		    }
	    }
	    else{
		    $out->setField( 'result', 'Failure' );

		    $errMsg = $l10n->getLabel( 'SyncManager', 'empty_destination' );

		    $out->addToLocalLog(  $errMsg. chr( 10 ) . chr( 10 ) );
		    $logger->critical("jsonOutRequest FAILED : ".$errMsg);
	    }

	    $out->data['state']['value'] = 'Inactive';
	    $out->store();

	    $logger->info("out result=".$out->data['result']['value']);
	    $logger->info("out state=".$out->data['state']['value']);

	    $logger->info("jsonOutRequest END duration : ". ((time() - $execution_start)) . " secondes");

	    return $out;
    }



	/**
	 * Emits the HTTP(S) request for transmitting the object to the remote system
	 * the request will be encrypted only if an https url is specified in the 'interface_url' attribute of the selected rule.
	 * @param $sqlOut the sqlOutSync object configured for the request
	 * @return OutSync the processed OutSync object containing the result of the request
	 */
	static function jsonSqlOutRequest(&$sqlOut){
		global $config,$l10n,$mf,$logger;

		$execution_start = time();
		$logger->info("jsonSqlOutRequest START request=".$sqlOut->data['sql_request']['value']);


		$sqlOut->setField('state','Sending');
		$sqlOut->setField('result','');

		$syncConfig = $config['plugins']['table_sync'];

		$sqlOut->data['remote_log']['value'] = '';


		if(isset($sqlOut->data['destination']['value']) && $sqlOut->data['destination']['value']!='')
			$destinations = explode(',',$sqlOut->data['destination']['value']);
		else{
			$destinations = array();
		}

		if(count($destinations)>0) {
			foreach ( $destinations as $destination ) {

				$sqlOut->addToLocalLog( 'Destination = '.$destination. chr( 10 ) );

				if ( isset( $syncConfig['remote_hosts'][ $destination ] ) ) {
					//URL de l'interface ou se connecter au serveur
					if ( isset( $syncConfig['remote_hosts'][ $destination ]['interface_url'] ) ) {
						$interfaceURL = $syncConfig['remote_hosts'][ $destination ]['interface_url'];
					} else {
						$errMsg = $destination . ' ' . $l10n->getLabel( 'SyncManager', 'attr_not_found1' . ' ' . 'interface_url' . ' ' . 'attr_not_found2' );

						$sqlOut->addToLocalLog( $errMsg . chr( 10 ) );
						$logger->critical("jsonSqlOutRequest FAILED : ".$errMsg );
					}

					//Mot de passe de connexion au serveur
					if ( isset( $syncConfig['remote_hosts'][ $destination ]['password'] ) ) {
						$password = $syncConfig['local_password'];
					} else {
						$errMsg = $destination . ' ' . $l10n->getLabel( 'SyncManager', 'attr_not_found1' . ' ' . 'password' . ' ' . 'attr_not_found2' );

						$sqlOut->addToLocalLog( $errMsg . chr( 10 ) );
						$logger->critical("jsonSqlOutRequest FAILED : ".$errMsg );
					}

				} else {
					$sqlOut->addToLocalLog( $destination . ' ' . $l10n->getLabel( 'SyncManager', 'destination_not_found_in_config' ) . chr( 10 ) );
				}

				if ( isset( $interfaceURL ) && isset( $password ) ) {
					$syncData = array(
						'request_type' => 'sql_sync',
						'sql_request'  => base64_encode($sqlOut->data['sql_request']['value']),
						'source'       => $_SERVER['SERVER_NAME'],
						'password'     => $password
					);

					$headers = array('Content-Type: multipart/form-data; Content-Length: ' . strlen(http_build_query($syncData)));

					if($sqlOut->data['sql_request']['value'] != '') {
						//effective connection to remote host
						$ch = curl_init();
						curl_setopt( $ch, CURLOPT_HEADER, 0 );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
						curl_setopt( $ch, CURLOPT_URL, $interfaceURL );
						curl_setopt( $ch, CURLOPT_TIMEOUT, 0 );
						curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
						//curl_setopt( $ch, CURLOPT_POST, 1 );
						//curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
						curl_setopt( $ch, CURLOPT_POSTFIELDS, $syncData );

						$sqlOut->addToLocalLog( "Connexion Curl vers ".$interfaceURL." ...".chr(10) );

						$curlResponse = curl_exec( $ch );
						//echo "response=".$curlResponse.chr(10);

						if ( $curlResponse === false ) {
							// connexion failure

							$sqlOut->setField( 'result', 'Failure' );
							$sqlOut->setField( 'state', 'Inactive' );

							$errMsg = curl_error( $ch );
							$sqlOut->addToLocalLog( $errMsg . chr( 10 ) );
							$logger->critical("jsonSqlOutRequest FAILED : ".$errMsg);

						} else {

							//decode and process json response
							$response = json_decode( $curlResponse, true );

							if ( json_last_error() == JSON_ERROR_NONE ) {

								if ( $response['result'] == 'success' ) {
									//We do set the result to success only if prior destinations haven't failed
									if ( $sqlOut->data['result']['value'] != 'Failure' ) {
										$sqlOut->setField( 'result', 'Success' );
										$sqlOut->setField( 'sql_result', $response['sql_result'] );
									}
									$sqlOut->addToLocalLog( $l10n->getLabel( 'SyncManager', 'success_contacting_dest' ) . ' ' . $destination . chr( 10 ) . chr( 10 ) );

								} else {
									$sqlOut->setField( 'result', 'Failure' );

									$errMsg = $l10n->getLabel( 'SyncManager', 'failed_contacting_dest' ) . ' ' . $destination;

									$sqlOut->addToLocalLog( $errMsg . chr( 10 ) . chr( 10 ) );
									$logger->critical("jsonSqlOutRequest FAILED : ".$errMsg);


								}

								//logging the JSON response
								$sqlOut->data['remote_log']['value'] .=  chr( 10 );
								$sqlOut->data['remote_log']['value'] .=  $response['log']. chr( 10 );

							} else {

								//if the response is not JSON (likely it is a PHP error message)
								$sqlOut->setField( 'result', 'Failure' );

								$errMsg = $l10n->getLabel( 'SyncManager', 'failed_contacting_dest' ) . ' ' . $destination;

								$sqlOut->addToLocalLog( $errMsg . chr( 10 ) . chr( 10 ) );
								$logger->critical("jsonSqlOutRequest FAILED : ".$errMsg);


								//logging the CURL Response rather than the JSON response
								$sqlOut->data['remote_log']['value'] .= chr( 10 );
								$sqlOut->data['remote_log']['value'] .= $curlResponse. chr( 10 );
							}
						}

						curl_close( $ch );
					}
					else{
						$sqlOut->setField( 'result', 'Failure' );

						$errMsg = $l10n->getLabel( 'SyncManager', 'empty_sql' );
						$sqlOut->addToLocalLog( $errMsg. chr( 10 ) . chr( 10 ) );
						$logger->critical("jsonSqlOutRequest FAILED : ".$errMsg);

					}

				} else {
					$sqlOut->setField( 'result', 'Failure' );
				}

			}
		}
		else{
			$sqlOut->setField( 'result', 'Failure' );

			$errMsg = $l10n->getLabel( 'SyncManager', 'empty_destination' );

			$sqlOut->addToLocalLog( $errMsg . chr( 10 ) . chr( 10 ) );
			$logger->critical("jsonSqlOutRequest FAILED : ".$errMsg);
		}

		$sqlOut->data['state']['value'] = 'Inactive';
		$sqlOut->store();

		$logger->info("jsonSqlOutRequest END duration : ". ((time() - $execution_start)) . " secondes");

		return $sqlOut;
	}


	/**
	 * Processes the content of an incoming (in) JSON request
	 * @param $in the InSync object containing the entering request
	 * @param $rtLog set to true to update the log in real time (slows down performance, use for debugging only)
	 */
	static function jsonInRequest(&$in, $rtLog = false){

		global $l10n,$config,$pdo,$logger;

		$execution_start = time();
		$logger->info("jsonInRequest START source=".$in->data['source']['value']." remote_table=".$in->data['remote_table']['value']." in_rule=".$in->data['in_rule']['value']);

		$syncConfig = $config['plugins']['table_sync'];

		$ruleName = $in->data['in_rule']['value'];
		$rule = $syncConfig['in_rules'][$ruleName];

		$localLog = array();

		$in->data['local_log']['value'] .= 'Local host = '.$_SERVER['SERVER_NAME']. chr( 10 );

		//rule found
		//create/update object
		if($rtLog) $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'rule_in_found' ) . chr( 10 ) );
		else $localLog[] = $l10n->getLabel( 'SyncManager', 'rule_in_found' ) ;

		if ( isset( $rule['filter_fields'] ) ) {
			if ( is_array( $rule['filter_fields'] ) ) {
				$filterfields = $rule['filter_fields'];
			} else {
				if($rtLog) $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'filter_fields_not_array' ) . $ruleName . chr( 10 ) );
				else $localLog[] = $l10n->getLabel( 'SyncManager', 'filter_fields_not_array' ) . $ruleName;

				$filterfields = array();
			}
		} else {
			if($rtLog) $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'filter_fields_not_defined' ) . $ruleName . chr( 10 ) );
			else $localLog[] = $l10n->getLabel( 'SyncManager', 'filter_fields_not_defined' ) . $ruleName;

			$filterfields = array();
		}

		if ( isset( $rule['key_processors'] ) ) {
			if ( is_array( $rule['key_processors'] ) ) {
				$keyProcessors = $rule['key_processors'];
			} else {
				if($rtLog) $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'key_proc_not_array' ) . $ruleName . chr( 10 ) );
				else $localLog[] = $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'key_proc_not_array' ) . $ruleName);

				$keyProcessors = array();
			}
		} else {
			if($rtLog) $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'key_proc_not_defined' ) . $ruleName . chr( 10 ) );
			else $localLog[] = $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'key_proc_not_defined' ) . $ruleName);

			$keyProcessors = array();
		}

		if ( isset( $rule['remap_fields'] ) ) {
			if ( is_array( $rule['remap_fields'] ) ) {
				$remapFields = $rule['remap_fields'];
			} else {
				if($rtLog) $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'remap_not_array' ) . $ruleName . chr( 10 ) );
				else $localLog[] = $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'remap_not_array' ) . $ruleName);

				$remapFields = array();
			}
		} else {
			if($rtLog) $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'remap_not_defined' ) . $ruleName . chr( 10 ) );
			else $localLog[] = $in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'remap_not_defined' ) . $ruleName);

			$remapFields = array();
		}

		//$logfile = DOC_ROOT.SUB_DIR.'/mf_logs/received.log';
		//file_put_contents($logfile, $in->data['remote_data']['value']);

		//retreive received and uncompress data to be processed
		$uncompressed =  @gzuncompress($in->data['remote_data']['value']); //@gzuncompress( );

		if ( $uncompressed === false ) {
			$in->setField('result','Failure');

			$errMsg = $l10n->getLabel( 'SyncManager', 'uncompress_error' );

			if($rtLog) $in->addToLocalLog($errMsg . chr( 10 ));
			else $localLog[] = $errMsg;

			$logger->critical("jsonInRequest FAILED : ".$errMsg);

		} else {


			$objectsData  = unserialize( $uncompressed );
			//$objectsTable = $in->data['remote_table']['value'];

			$logger->debug("jsonInRequest IN DATA=");
			$logger->debug("Number of objects to process = ".count($objectsData));


			if ( isset( $rule['local_table'] ) ) {

				//first, check if object already exists in the table

				//uids for local records in table to be updated
				$localUids = array();

				//last modification date of local records. Same last modification dates will be skipped
				$localModDates = array();

				$sql = "SELECT uid, modification_date FROM " . $rule['local_table'] . " WHERE 1";

				$stmt    = $pdo->query( $sql );
				$rows    = $stmt->fetchAll( PDO::FETCH_ASSOC );
				foreach($rows as $row) {
					$localUids[$row['uid']] = true;
					$localModDates[$row['uid']] = $row['modification_date'];
				}

				$logger->info("jsonInRequest processing ".count($localUids)." rows.");


				//process all objects one by one

				foreach ( $objectsData as $uid => $data ) {

					//$logger->info("modification_date : ".$data['modification_date']. " vs " .$localModDates[$uid]);

					//if the uid does not exist locally or if the remote and local modification dates mismatch
					if(!isset($localModDates[$uid]) || $data['modification_date'] != $localModDates[$uid]) {

						if ( isset( $localUids[ intval( $uid ) ] ) ) {

							//UPDATE
							$sql = "UPDATE " . $rule['local_table'] . " SET ";
							foreach ( $data as $keyName => $keyData ) {

								//filtering fields forbiden in the IN rule
								if ( ! in_array( $keyName, $filterfields ) ) {

									//remap field if necessary
									if ( isset( $remapFields[ $keyName ] ) ) {
										$keyName = $remapFields[ $keyName ];
									}

									// Assign the language specified in the rule to the record
									// as it may come from another language
									if($keyName == 'language' && $rule['set_locale']!=''){
										$keyData = $rule['set_locale'];
										$logger->debug('jsonInRequest set_locale='.$keyData);
									}

									//caution : keyProcessor applies on the remaped field name !
									if ( isset( $keyProcessors[ $keyName ] ) ) {

										//process key for display with optional user supplied processing function in the IN rule
										eval( '$value = ' . $keyProcessors[ $keyName ] . '($keyName,$keyData,$data["language"],$data);' );

										//set SQL
										$sql .= "`" . $keyName . "` = " . $value . ',';
									} else {

										//set SQL
										$sql .= "`" . $keyName . "` = " . $pdo->quote( $keyData ) . ',';
									}

								}
							}
							$sql = rtrim( $sql, ", " );
							$sql .= " WHERE uid = " . intval( $uid ) . ";";

							try {
								$updateStmt = $pdo->query( $sql );

								if ( $in->data['result']['value'] != 'Failure' ) {
									$in->setField( 'result', 'Success' );
								}
								if($rtLog) $in->addToLocalLog( "UPDATE uid ".$uid." SUCCESS". chr( 10 ) );
								else $localLog[] = "UPDATE uid ".$uid." SUCCESS";

									$logger->debug( $in->data['in_rule']['value']." UPDATE uid " . $uid );


							} catch ( PDOException $e ) {
								$in->setField( 'result', 'Failure' );

								$errMsg = "uid = " . intval( $uid ) . " , " . $l10n->getLabel( 'SyncManager', 'update_error' ) . chr( 10 ) . chr( 10 ) .
								          "ERROR=".$pdo->errorInfo()[2].chr(10).
								          "SQL=" . $sql . chr( 10 ) . chr( 10 );

								if($rtLog) $in->addToLocalLog( "UPDATE uid " . $uid." FAILED : " .$errMsg );
								else $localLog[] = "UPDATE uid " . $uid." FAILED : " .$errMsg;

									$logger->critical( "jsonInRequest UPDATE uid " . $uid." FAILED : " . $errMsg );
							}
							$in->store();
						} else {

							//CREATE
							//prepare SQL
							$sql = "INSERT INTO " . $rule['local_table'] . " SET ";

							foreach ( $data as $keyName => $keyData ) {

								//filtering fields forbiden in the IN rule
								if ( ! in_array( $keyName, $filterfields ) ) {

									if ( isset( $keyProcessors[ $keyName ] ) ) {

										//remap field if necessary
										if ( isset( $remapFields[ $keyName ] ) ) {
											$keyName = $remapFields[ $keyName ];
										}

										//process key for display with optional user supplied processing function in the IN rule
										eval( '$value = ' . $keyProcessors[ $keyName ] . '($keyName,$keyData,$data["language"],$data);' );

										//set SQL
										$sql .= "`" . $keyName . "` =" . $value . ',';

									} else {
										//remap field if necessary
										if ( isset( $remapFields[ $keyName ] ) ) {
											$keyName = $remapFields[ $keyName ];
										}

										//set SQL
										$sql .= "`" . $keyName . "` =" . $pdo->quote( $keyData ) . ',';
									}
								}
							}

							$sql = rtrim( $sql, ", " ) . ';';

							try {
								$createStmt = $pdo->query( $sql );

								$status = $pdo->lastInsertId();

								if ( $in->data['result']['value'] != 'Failure' ) {
									$in->setField( 'result', 'Success' );
								}
								if($rtLog) $in->addToLocalLog( "INSERT uid ".$uid." SUCCESS" . chr( 10 ) );
								else $localLog[] = "INSERT uid ".$uid." SUCCESS";

									$logger->debug( $in->data['in_rule']['value']." INSERT uid " . $uid );

							} catch ( PDOException $e ) {
								$in->setField( 'result', 'Failure' );

								$errMsg = chr( 10 ) . $l10n->getLabel( 'SyncManager', 'create_error' ) . chr( 10 ) . chr( 10 ) .
								          "ERROR=".$pdo->errorInfo()[2].chr(10).
								          "SQL=" . $sql . chr( 10 ) . chr( 10 );

								if($rtLog) $in->addToLocalLog( "INSERT uid " . $uid." FAILED : " .$errMsg );
								else $localLog[] = "INSERT uid " . $uid." FAILED : " .$errMsg;

									$logger->critical( "jsonInRequest INSERT uid " . $uid." FAILED : " . $errMsg );
							}


						}
					}
					else{
						//same modification dates for remote and local record, skip update
						$logger->debug( $in->data['in_rule']['value']." SKIP uid ".$uid );
						if($rtLog) $in->addToLocalLog("SKIP uid ".$uid );
						else $localLog[] = "SKIP uid ".$uid;

						$in->setField( 'result', 'Success' );
					}
				}
			} else {
				$errMsg = $l10n->getLabel( 'SyncManager', 'undefined_local_table' ) . ' ' . $ruleName . chr( 10 );
				if($rtLog) $in->addToLocalLog($errMsg);
				else $localLog[] = $errMsg;

				$logger->critical($in->data['in_rule']['value']." jsonInRequest FAILED : ".$errMsg);

				$in->setField('result','Failure');
			}
		}

		if(!$rtLog) $in->addToLocalLog( implode( chr(10), $localLog) );

		$remoteLog = $in->data['local_log']['value'];

		switch($in->data['result']['value']){
			case 'Success':
				$response = '{"jsonrpc" : "2.0", "result" : "success", "log" : '.json_encode($remoteLog).'}';
				break;

			case 'Failure':
				$response = '{"jsonrpc" : "2.0", "result" : "error", "log" : '.json_encode($remoteLog).'}';
				break;
		}

		$in->setField('state','Inactive');

		$logger->info("in result=".$in->data['result']['value']);
		$logger->info("in state=".$in->data['state']['value']);
		$logger->info("jsonInRequest END duration : ". ((time() - $execution_start)) . " secondes");

		return $response;

	}

	/**
	 * Converts empty dates to null dates for strict SQL conformity
	 * @param $key
	 * @param $value
	 * @param $locale
	 * @param $row
	 *
	 * @return null
	 */
	static function checkNullDate($key,$value,$locale,$row){
		global $pdo;

		if($value == '0000-00-00' || $value == '0000-00-00 00:00:00' || $value == '0000-00-00 00:00:00.000000' || $value == '' || $value == 'NULL' || $value == null || $value == 'null') return 'NULL';
		else return $pdo->quote($value);
	}

	/**
	 * Converts empty dates to current date for creation_date and modification_date (these can not be null)
	 * @param $key
	 * @param $value
	 * @param $locale
	 * @param $row
	 *
	 * @return null
	 */
	static function checkCreateModDate($key,$value,$locale,$row){
		global $pdo;

		if($value == '0000-00-00' || $value == '0000-00-00 00:00:00' || $value == '0000-00-00 00:00:00.000000' || $value == '' || $value == 'NULL' || $value == null || $value == 'null') {
			$date = new DateTime('NOW');
			return $pdo->quote($date->format('Y-m-d H:i:s.u'));
		}
		else return $pdo->quote($value);
	}
}

