<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(



    'fr' => array(
        'cn_iso_2' => 'Code ISO 3166-1 alpha-2',
        'cn_iso_3' => 'Code ISO 3166-1 alpha-3',
        'cn_iso_nr' => 'Code ISO 3166-1 numérique',
        'cn_short_fr' => 'Nom court français',
        'cn_short_de' => 'Nom court allemand',
        'fieldset_zip' => 'Vérification du code postal',
        'zip_format' => 'Format code postal',
        'zip_regexp' => 'Expression régulière code postal',
        'zip_nocodes' => 'Pas de code postal pour ce pays',
        'zip_single' => 'Code postal unique pour le pays',
        'disable_zip_check' => 'Désactiver la vérification du code postal',
        'zip_notes' => 'Notes relatives au code postal',
        'cn_parent_tr_iso_nr' => 'Cose ISO numérique pays parent',
        'cn_official_name_local' => 'Nom officiel localisé',
        'cn_short_local' => 'Nom court localisé',
        'cn_official_name_en' => 'Nom officiel anglais',
        'cn_short_en' => 'Nom court anglais',
        'cn_capital' => 'Capitale',
        'cn_tldomain' => 'Extension Top Level Domain Name',
        'cn_currency_iso_3' => 'Devise ISO alpha-3',
        'cn_currency_iso_nr' => 'Devise ISO numérique',
        'cn_phone' => 'Indicatif téléphonique international',
        'cn_eu_member' => 'Membre de l\'Union Européenne',
        'cn_uno_member' => 'Membre des Nations Unies',
        'cn_address_format' => 'Format de l\'adresse',
        'cn_zone_flag' => 'Drapeau de la zone',
        'cn_short_nl' => 'Nom court Néerlandais',
        'colissimo_area' => 'Zone Colissimo',

        'name_FR' => 'France',
        'name_OM1' => 'Outre Mer 1',
        'name_OM2' => 'Outre Mer 2',
        'name_INTA' => 'International Zone A',
        'name_INTB' => 'International Zone B',
        'name_INTC' => 'International Zone C',
        'name_INTD' => 'International Zone D',
        'name_SEC' => 'Secteurs postaux',

        /*mandatory records*/
        'record_name' => 'Pays',
        //'no_record' => 'Aucun résultat.',
        'new_record' => 'Ajout pays'
    ),

);

?>