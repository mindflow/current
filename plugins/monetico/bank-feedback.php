<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

//https://dev.velorizons.com/mf_websites/velorizons/plugins/monetico/bank-feedback.php?TPE=6327980&date=12/09/2016_a_15:44:33&montant=875EUR&reference=vzCBOrderFrontend_30&MAC=ACDA1EE21A8C0B0129C64D6EE0ECEB4B52DEFBF4&texte-libre=Solde CR0173&code-retour=payetest&cvx=oui&vld=1219&brand=MC&status3ds=4&numauto=000000&originecb=FRA&bincb=000003&hpancb=3614D4F3166C8644EF4512EF113A18F9A2972DB8&ipclient=78.231.132.127&originetr=FRA&veres=&pares=&modepaiement=CB

//https://dev.velorizons.com/mf_websites/velorizons/plugins/monetico/bank-feedback.php?TPE=6327980&date=17/12/2014_a_17:23:43&montant=1EUR&reference=4871&MAC=1BA9687FE6C70E5BA1681CC3B370F3678C0D2511&texte-libre=&code-retour=paiement&cvx=oui&vld=0917&brand=VI&status3ds=1&numauto=318440&modepaiement=CB?TPE=6327980&date=17/12/2014_a_17:23:43&montant=1EUR&reference=4871&MAC=1BA9687FE6C70E5BA1681CC3B370F3678C0D2511&texte-libre=&code-retour=paiement&cvx=oui&vld=0917&brand=VI&status3ds=1&numauto=318440&modepaiement=CB&KCFINDER_showname=on&KCFINDER_showsize=off&KCFINDER_showtime=off&KCFINDER_order=name&KCFINDER_orderDesc=off&KCFINDER_view=thumbs&KCFINDER_displaySettings=off&PHPSESSID=efa88b77707298d5e82d5042107d2852

$execution_start = time();

require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/frontend/frontend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//load test payment configuration if current_user is logged into the backend or if test mode is forced
//allows testing using simulated payments rather than real CB
if(isset($_SESSION['current_be_user']) || $GLOBALS['config']['plugins']['monetico']['forceTestPayments']==1){
   if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['test_config_file'];
}
else {
   if(!defined('MONETICOPAIEMENT_KEY')) require_once $GLOBALS['config']['plugins']['monetico']['prod_config_file'];
}

require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/MoneticoPaiement_Ept.inc.php';
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/records/moneticoBankFeedback.php';

//LOGGING Monetico call to text file. Log entry can be used to replay the transaction if necessary by copying and pasting the called url in the adress bar of your browser.
$request = '';
foreach($_REQUEST as $param => $value){
    $request .= "&".$param."=".$value;
}
$request = ltrim($request,'&');
$callingUrl = chr(10).date('Y-m-d H:i:s') . " - [".$_SERVER['REMOTE_ADDR']."] - ".(((!empty($_SERVER['HTTPS'])) ? "https://" : "http://") . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']."?".$request).chr(10);

//we are serving json
header("Pragma: no-cache");
header("Content-type: text/plain");

//create a backend for being able to use all the mindflow objects
$mindFlowFrontend = new frontend();
$mindFlowFrontend->prepareData();
$mf = $mindFlowFrontend->mf;
$pdo = $mf->db->pdo;
$l10n = $mf->l10n;

//$logfile = DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/log-'.$_SERVER['HTTP_HOST'].'.txt';
$logfile = DOC_ROOT.SUB_DIR.'/mf_logs/'.$_SERVER['HTTP_HOST'].'-monetico-bank-feedback.log';

// Write the contents to the file,
// using the FILE_APPEND flag to append the content to the end of the file
// and the LOCK_EX flag to prevent anyone else writing to the file at the same time
if(file_exists ( $logfile ))file_put_contents($logfile, $callingUrl, FILE_APPEND);
else file_put_contents($logfile, $callingUrl);

// Begin Main : Retrieve Variables posted by Monetico Paiement Server
$MoneticoPaiement_bruteVars = getMethode();

// TPE init variables
$oEpt = new MoneticoPaiement_Ept();
$oHmac = new MoneticoPaiement_Hmac($oEpt);

// Message Authentication
$phase2back_fields = sprintf(MONETICOPAIEMENT_PHASE2BACK_FIELDS, $oEpt->sNumero,
    (isset($MoneticoPaiement_bruteVars['date']))?       $MoneticoPaiement_bruteVars['date']:'',
    (isset($MoneticoPaiement_bruteVars['montant']))?    $MoneticoPaiement_bruteVars['montant']:'',
    (isset($MoneticoPaiement_bruteVars['reference']))?  $MoneticoPaiement_bruteVars['reference']:'',
    (isset($MoneticoPaiement_bruteVars['texte-libre']))?$MoneticoPaiement_bruteVars['texte-libre']:'',
    $oEpt->sVersion,
    (isset($MoneticoPaiement_bruteVars['code-retour']))?$MoneticoPaiement_bruteVars['code-retour']:'',
    (isset($MoneticoPaiement_bruteVars['cvx']))?        $MoneticoPaiement_bruteVars['cvx']:'',
    (isset($MoneticoPaiement_bruteVars['vld']))?        $MoneticoPaiement_bruteVars['vld']:'',
    (isset($MoneticoPaiement_bruteVars['brand']))?      $MoneticoPaiement_bruteVars['brand']:'',
    (isset($MoneticoPaiement_bruteVars['status3ds']))?  $MoneticoPaiement_bruteVars['status3ds']:'',
    (isset($MoneticoPaiement_bruteVars['numauto']))?    $MoneticoPaiement_bruteVars['numauto']:'',
    (isset($MoneticoPaiement_bruteVars['motifrefus']))? $MoneticoPaiement_bruteVars['motifrefus']:'',
    (isset($MoneticoPaiement_bruteVars['originecb']))?  $MoneticoPaiement_bruteVars['originecb']:'',
    (isset($MoneticoPaiement_bruteVars['bincb']))?      $MoneticoPaiement_bruteVars['bincb']:'',
    (isset($MoneticoPaiement_bruteVars['hpancb']))?     $MoneticoPaiement_bruteVars['hpancb']:'',
    (isset($MoneticoPaiement_bruteVars['ipclient']))?   $MoneticoPaiement_bruteVars['ipclient']:'',
    (isset($MoneticoPaiement_bruteVars['originetr']))?  $MoneticoPaiement_bruteVars['originetr']:'',
    (isset($MoneticoPaiement_bruteVars['veres']))?      $MoneticoPaiement_bruteVars['veres']:'',
    (isset($MoneticoPaiement_bruteVars['pares']))?      $MoneticoPaiement_bruteVars['pares']:''
);


// Store bank feeback object into database
$feedback = new moneticoBankFeedback();

//conversion format de date merdique CIC
$dateArr = explode('_a_',$MoneticoPaiement_bruteVars['date']);
$date = implode(' ', $dateArr);

if(isset($MoneticoPaiement_bruteVars['MAC']))           $feedback->data['MAC']['value']             = $MoneticoPaiement_bruteVars['MAC'];
if(isset($MoneticoPaiement_bruteVars['date']))          $feedback->data['date']['value']            = $date;
if(isset($MoneticoPaiement_bruteVars['tpe']))           $feedback->data['tpe']['value']             = $MoneticoPaiement_bruteVars['tpe'];
if(isset($MoneticoPaiement_bruteVars['montant']))       $feedback->data['montant']['value']         = $MoneticoPaiement_bruteVars['montant'];
if(isset($MoneticoPaiement_bruteVars['reference']))     $feedback->data['reference']['value']       = $MoneticoPaiement_bruteVars['reference'];
if(isset($MoneticoPaiement_bruteVars['texte-libre']))   $feedback->data['texte-libre']['value']     = $MoneticoPaiement_bruteVars['texte-libre'];
if(isset($MoneticoPaiement_bruteVars['code-retour']))   $feedback->data['code-retour']['value']     = $MoneticoPaiement_bruteVars['code-retour'];
if(isset($MoneticoPaiement_bruteVars['cvx']))           $feedback->data['cvx']['value']             = $MoneticoPaiement_bruteVars['cvx'];
if(isset($MoneticoPaiement_bruteVars['vld']))           $feedback->data['vld']['value']             = $MoneticoPaiement_bruteVars['vld'];
if(isset($MoneticoPaiement_bruteVars['brand']))         $feedback->data['brand']['value']           = $MoneticoPaiement_bruteVars['brand'];
if(isset($MoneticoPaiement_bruteVars['status3ds']))     $feedback->data['status3ds']['value']       = $MoneticoPaiement_bruteVars['status3ds'];
if(isset($MoneticoPaiement_bruteVars['numauto']))       $feedback->data['numauto']['value']         = $MoneticoPaiement_bruteVars['numauto'];
if(isset($MoneticoPaiement_bruteVars['motifrefus']))    $feedback->data['motifrefus']['value']      = $MoneticoPaiement_bruteVars['motifrefus'];
if(isset($MoneticoPaiement_bruteVars['originecb']))     $feedback->data['originecb']['value']       = $MoneticoPaiement_bruteVars['originecb'];
if(isset($MoneticoPaiement_bruteVars['bincb']))         $feedback->data['bincb']['value']           = $MoneticoPaiement_bruteVars['bincb'];
if(isset($MoneticoPaiement_bruteVars['hpancb']))        $feedback->data['hpancb']['value']          = $MoneticoPaiement_bruteVars['hpancb'];
if(isset($MoneticoPaiement_bruteVars['ipclient']))      $feedback->data['ipclient']['value']        = $MoneticoPaiement_bruteVars['ipclient'];
if(isset($MoneticoPaiement_bruteVars['originetr']))     $feedback->data['originetr']['value']       = $MoneticoPaiement_bruteVars['originetr'];
if(isset($MoneticoPaiement_bruteVars['veres']))         $feedback->data['veres']['value']           = $MoneticoPaiement_bruteVars['veres'];
if(isset($MoneticoPaiement_bruteVars['pares']))         $feedback->data['pares']['value']           = $MoneticoPaiement_bruteVars['pares'];
if(isset($MoneticoPaiement_bruteVars['montantech']))    $feedback->data['montantech']['value']      = $MoneticoPaiement_bruteVars['montantech'];
if(isset($MoneticoPaiement_bruteVars['filtragecause'])) $feedback->data['filtragecause']['value']   = $MoneticoPaiement_bruteVars['filtragecause'];
if(isset($MoneticoPaiement_bruteVars['filtragevaleur']))$feedback->data['filtragevaleur']['value']  = $MoneticoPaiement_bruteVars['filtragevaleur'];
if(isset($MoneticoPaiement_bruteVars['cbenregistree'])) $feedback->data['cbenregistree']['value']   = $MoneticoPaiement_bruteVars['cbenregistree'];
if(isset($MoneticoPaiement_bruteVars['cbmasquee']))     $feedback->data['cbmasquee']['value']       = $MoneticoPaiement_bruteVars['cbmasquee'];
if(isset($MoneticoPaiement_bruteVars['modepaiement']))  $feedback->data['modepaiement']['value']    = $MoneticoPaiement_bruteVars['modepaiement'];

$feedback->store();

if (isset($MoneticoPaiement_bruteVars['MAC']) && $oHmac->computeHmac($phase2back_fields) == strtolower($MoneticoPaiement_bruteVars['MAC'])){



    switch($MoneticoPaiement_bruteVars['code-retour']) {

        case "Annulation" :
            // Payment has been refused
            // put your code here (email sending / Database update)
            // Attention : an authorization may still be delivered for this payment
            if(isset($GLOBALS['config']['plugins']['monetico']['paymentFailureCallbackFunc']) && $GLOBALS['config']['plugins']['monetico']['paymentFailureCallbackFunc'] != ''){
                eval($GLOBALS['config']['plugins']['monetico']['paymentFailureCallbackFunc'].'($feedback);');
            }
            break;

        case "payetest":
            // Payment has been accepted on the test server
            // put your code here (email sending / Database update)
            if(isset($GLOBALS['config']['plugins']['monetico']['testPaymentSuccessCallbackFunc']) && $GLOBALS['config']['plugins']['monetico']['testPaymentSuccessCallbackFunc'] != ''){
                eval($GLOBALS['config']['plugins']['monetico']['testPaymentSuccessCallbackFunc'].'($feedback);');
            }
            break;

        case "paiement":
            // Payment has been accepted on the productive server
            // put your code here (email sending / Database update)
            if(isset($GLOBALS['config']['plugins']['monetico']['paymentSuccessCallbackFunc']) && $GLOBALS['config']['plugins']['monetico']['paymentSuccessCallbackFunc'] != ''){
                eval($GLOBALS['config']['plugins']['monetico']['paymentSuccessCallbackFunc'].'($feedback);');
            }
            break;


        /***              ONLY FOR MULTIPART PAYMENT              ***/
        case "paiement_pf2":
        case "paiement_pf3":
        case "paiement_pf4":
            // Payment has been accepted on the productive server for the part #N
            // return code is like paiement_pf[#N]
            // put your code here (email sending / Database update)
            // You have the amount of the payment part in $MoneticoPaiement_bruteVars['montantech']
            if(isset($GLOBALS['config']['plugins']['monetico']['multipartPaymentSuccessCallbackFunc']) && $GLOBALS['config']['plugins']['monetico']['multipartPaymentSuccessCallbackFunc'] != ''){
                eval($GLOBALS['config']['plugins']['monetico']['multipartPaymentSuccessCallbackFunc'].'($feedback);');
            }
            break;

        case "Annulation_pf2":
        case "Annulation_pf3":
        case "Annulation_pf4":
            // Payment has been refused on the productive server for the part #N
            // return code is like Annulation_pf[#N]
            // put your code here (email sending / Database update)
            // You have the amount of the payment part in $MoneticoPaiement_bruteVars['montantech']
        if(isset($GLOBALS['config']['plugins']['monetico']['multipartPaymentFailureCallbackFunc']) && $GLOBALS['config']['plugins']['monetico']['multipartPaymentFailureCallbackFunc'] != ''){
            eval($GLOBALS['config']['plugins']['monetico']['multipartPaymentFailureCallbackFunc'].'($feedback);');
        }
            break;
    }

    $receipt = MONETICOPAIEMENT_PHASE2BACK_MACOK;

}
else
{
    // your code if the HMAC doesn't match
    $receipt = MONETICOPAIEMENT_PHASE2BACK_MACNOTOK.$phase2back_fields;
}

/*
//in case of cancelation, the HMAC check fails for some reason (bug in the bank API ?). Thus, the code has to be processed here.
if( $MoneticoPaiement_bruteVars['code-retour'] == "Annulation") {
    // Payment has been refused
    // put your code here (email sending / Database update)
    // Attention : an authorization may still be delivered for this payment
    if(isset($GLOBALS['config']['plugins']['monetico']['paymentFailureCallbackFunc']) && $GLOBALS['config']['plugins']['monetico']['paymentFailureCallbackFunc'] != ''){
        eval($GLOBALS['config']['plugins']['monetico']['paymentFailureCallbackFunc'].'($feedback);');
    }
}*/

//-----------------------------------------------------------------------------
// Send receipt to Monetico Paiement server
//-----------------------------------------------------------------------------
printf (MONETICOPAIEMENT_PHASE2BACK_RECEIPT, $receipt);



$mf->db->closeDB();
?>



