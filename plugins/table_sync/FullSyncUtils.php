<?php
class FullSyncUtils{


	/**
	 *  Création d'une synchronisation intégrale de tables
	 *  @return string the link to get the content action executed
	 */
	static function createFullSync(){
		//form security variables
		$sec = getSec(array('action' => 'createFullSync'));

		return $GLOBALS['config']['website_pluginsdir'].'/table_sync/dyn-sync-html.php?'.$sec->parameters.'&header=0&footer=0&sec='.$sec->hash.'&fields='.$sec->fields;
	}




}




?>