<?php

/*                                                                        *
 * This script belongs to the MindFlow framework.                         *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * Author : Mind2Machine                                                  *
 * The MindFlow project                                                   *
 *                                                                        */

$execution_start = time();

require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//we are serving json
header('Content-type: application/json');

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();
$mf = $mindFlowBackend->mf;
$pdo = $mf->db->pdo;
$l10n = $mf->l10n;

//print_r($GLOBALS['config']['load_plugins']);
//print_r($_REQUEST);

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if((isset($_REQUEST['sec']) && isset($_REQUEST['fields']) )&& formsManager::checkSecValue($_REQUEST['sec'],explode(',',$_REQUEST['fields']))){

    $currentUser = $mf->currentUser;
    //$groupeDeSecurite = $currentUser->data['GroupeDeSecurite']['value'];


    if(isset($_REQUEST['action'])){

        if($_REQUEST['action'] == 'myPluginForm'){

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm()){

                if(isset($mf->currentUser)){
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('my_plugin','allowEditMyContacts')==1)));

                    if($moduleAccess){
                        $sqlConditions = '';
                        $dateConditions = '';
                        $deletedCondition = '';
                        $matchCondition =' AND (';
                        $oneCond=false; //pour voir si une condition a déjà été activée ou pas

                        //Processing form data
                        if(trim($form->data['keyword_filter']['value']) != '')
                            $sqlConditions .= ' AND MATCH(`name`,`email`) AGAINST('.$pdo->quote($form->data['keyword_filter']['value'].'*').' IN BOOLEAN MODE)';

                        if(isset($form->data['deleted_filter']) && $form->data['deleted_filter']['value'] == '1')$deletedCondition = ' AND deleted=1';
                        else $deletedCondition = ' AND deleted=0';

                        $matchCondition .=')';
                        if($matchCondition ==' AND ()')$matchCondition ='';


                        $module = new editMyContacts();
                        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition.$matchCondition);


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}');
                    }
                    else{
                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}');
                    }
                }
                else{
                    $mf->db->closeDB();
                    die('{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','currentUser_missing')).'}');
                }
            }
            else{
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','form-process-error')).'}');
            }
        }

        else if($_REQUEST['action'] == 'createContact'){
            $user = new myContact();

            //remove useless keys
            $user->showInEditForm['tab_main'] = array_diff($user->showInEditForm['tab_main'], array('creation_date', 'modification_date'));

            $html = $mf->formsManager->editRecord($user, $this->pluginName, '', true, true,false,true);

            die('{"jsonrpc" : "2.0", "error" : '.json_encode($html).'}');
        }


    }
    else {
        $mf->db->closeDB();
        die('{"jsonrpc" : "2.0", "error" : "No action specified."}');
    }
    //echo "Error : No action specified.";
}
else {
    $mf->db->closeDB();
    die('{"jsonrpc" : "2.0", "result" : "error", "message" : "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}');
}

$mf->db->closeDB();




