<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


/**
 * Handles localization of Mindflow frontend and backend.
 *
 * Provides functions for loading localization files containing translation labels for objects and templates.
 * Allows retreiving labels in the current displayed language for display in the user interface.
 */
class l10nManager{



    function __construct(){

        //storage for localized labels
        $GLOBALS['l10n_text'] = array();

        //base mindflow labels
        include DOC_ROOT.SUB_DIR.'/mf/languages/main_l10n.php';

        $GLOBALS['l10n_text']['main'] = $l10nText;

        //database labels
        include DOC_ROOT.SUB_DIR.'/mf/languages/database_l10n.php';
        $GLOBALS['l10n_text']['database'] = $l10nText;

        //backend labels
        include DOC_ROOT.SUB_DIR.'/mf/languages/backend_l10n.php';
        $GLOBALS['l10n_text']['backend'] = $l10nText;

        //initialize storage for templates labels
        $GLOBALS['l10n_text']['templates'] = array();
    }

    /**
     * Retreives the localized label for the given domain.
     *
     * @param $domain string A domain is the unique name referencing the content loaded from a localization file, for exemple 'newsItem' if you have loaded the labels for a newsItem object.
     * @param $label string The label is the unique key for which you want to retreive the translation, for example 'news_date'
     * @param null $forceLocale string By default the currently displayed language in MindFlow is used. But there are cases you may want to force the language you get the translation for. You can specify an ISO2 language key here, such as 'fr'.
     * @return string the localized label
     */
    function getLabel($domain, $label, $forceLocale=null){

        global $mf,$l10n;
        $label = trim(trim($label,'`'));

        if($forceLocale!=null)$locale=$forceLocale;
        else{
            $locale = $mf->getDisplayedLocale();
        }

       // if($locale=='de')//print_r($GLOBALS['l10n_text'][$domain][$locale]);
     /* if($label == 'ref_circuit') {
          echo "domain=".$domain." locale=".$locale." label=".$label."<br>".chr(10);
          print_r($GLOBALS['l10n_text'][$domain]);
      }*/

        if(isset($GLOBALS['l10n_text'][$domain])) {
            if(isset($GLOBALS['l10n_text'][$domain][$locale])) {
                $labelArray = $GLOBALS['l10n_text'][$domain][$locale];
            }
            else {
                $mf->addWarningMessage($l10n->getLabel('backend','locale_warning1').' "'.$locale.'" '.$l10n->getLabel('backend','locale_warning2').' "'.$domain.'" <br>'.
                '<strong>function:</strong> getLabel(), <strong>domain:</strong> '.$domain.', <strong>locale:</strong> '.$locale.', <strong>label:</strong> '.$label.'<br/>'.
                '<strong>file:</strong> '.debug_backtrace()[1]['file'].', <strong>line:</strong> '.debug_backtrace()[1]['line'].'');
                return '';
            }
        }
        else {
            $backtraceArray = debug_backtrace();
            if($forceLocale==null)$forceLocale='null';
            else $forceLocale='"'.$forceLocale.'"';
            $backtrace = nl2br(generateCallTrace());

            //$mf->addWarningMessage($l10n->getLabel('backend','domain_warning1').' "'.$domain.'" '.$l10n->getLabel('backend','domain_warning2').'<br /><br />'.$backtrace);
            $mf->addWarningMessage($l10n->getLabel('backend','domain_warning1').' "'.$domain.'" '.$l10n->getLabel('backend','domain_warning2').'<br>'.
            '<strong>function:</strong> getLabel(), <strong>domain:</strong> '.$domain.', <strong>locale:</strong> '.$locale.', <strong>label:</strong> '.$label.'<br/>'.
            $backtrace);
            return "";
        }

        if(isset($labelArray[$label])){
            return $labelArray[$label];
        }
        else{
            $backtraceArray = debug_backtrace();
            if($forceLocale==null)$forceLocale='null';
            else $forceLocale='"'.$forceLocale.'"';
            $backtrace = nl2br(generateCallTrace());

            $mf->addWarningMessage($l10n->getLabel('backend','label_warning1').' "'.$label.'" '.$l10n->getLabel('backend','label_warning2').' "'.$domain.'" '.'<br /><br />'.$backtrace);

            return "";
        }
    }

    /**
     * Load a template file and replaces all the marker {tags} with the definitions from the given domain, applying the current locale
     * @param $domain String the domain where the marker {tags} substitution definitions should be looked for
     * @param $template_file String the file, usually html, containing some marker {tags}
     * @return mixed|string the file content with replaced markers
     */
    function loadFileReplaceLabels($domain, $template_file) {

        global $mf,$l10n,$config;
        //set default locale
        $locale = $config['default_frontend_locale'];

        if($mf->mode == 'frontend'){
            $locale = $mf->getFrontendLanguage();
        }
        else {
            $locale = $mf->getBackendLanguage();
        }

        $labelArray = $GLOBALS['l10n_text'][$domain][$locale];

        if(is_array($labelArray)){
        
            $source = file_get_contents($template_file);
    
            foreach ($labelArray as $tag => $value) {
                $source = str_replace('{'.$tag.'}', $value, $source);
        	}
            
            return $source;
        
        } else {
            $source = file_get_contents($template_file);

            if ($source != '') {
                return $source;
            } else {
                return '';
            }
            
        }
    }

    /*
     * Loads a language file
     * @param $domain string the domain identifier that will be used when calling $l10n->getLabel( $domain, $theFieldKey);
     * @param $languageFilePath string the complete path to the file, relative to the mindflow hosting directory, such as '/mf/plugins/somePlugin/languages/someClass_l10n.php'
     */
    static function loadL10nFile($domain, $languageFilePath){
        global $mf;

        $locale = $mf->getLocale();

        if(!isset($GLOBALS['l10n_text'][$domain]))$GLOBALS['l10n_text'][$domain]=array();

//echo $languageFilePath." test=".is_file(DOC_ROOT.SUB_DIR.$languageFilePath)."<br />";

        //load the current record's translation files
        if(is_file(DOC_ROOT.SUB_DIR.$languageFilePath)){
            include DOC_ROOT.SUB_DIR.$languageFilePath;
            $GLOBALS['l10n_text'][$domain] = array_merge_recursive_distinct($GLOBALS['l10n_text'][$domain], $l10nText);

        }
        //else $mf->addErrorMessage('l10nManager::loadL10nFile( "'.$domain.'", "'.$languageFilePath.'" ) : '.$mf->l10n->getLabel('backend','file_not_found').' !');
        else {
            $mf->addWarningMessage('l10nManager::loadL10nFile( "'.$domain.'", "'.$languageFilePath.'" ) : <strong>'.$mf->l10n->getLabel('backend','file_not_found').'</strong> !<br /><br />'.nl2br(generateCallTrace()));
            //return '';
            //echo 'l10nManager::loadL10nFile( "'.$domain.'", "'.$languageFilePath.'" ) : '.$mf->l10n->getLabel('backend','file_not_found').' !<br />'.chr(10);
        }


    }

    /*
     * Loads the language file for a dbRecord's inherited class
     * @param $recordClassName string the class name of the record wich is also the domain identifier that will be used when calling $l10n->getLabel( $domain, $theFieldKey);
     * @param $languageFilePath string the complete path to the file, relative to the mindflow hosting directory, such as '/mf/plugins/somePlugin/languages/someClass_l10n.php'
     */
    function loadL10nRecord($recordClassName, $languageFilePath){

        //load l10n text for inherited record types
        if($recordClassName != 'dbRecord'){

            if(!isset($GLOBALS['l10n_text'][$recordClassName])) { //make sure we do not load twice an l10n file : ignore it if already defined


                //make sure base class dbRecord l10n text is loaded for any record type
                if (!isset($GLOBALS['l10n_text']['dbRecord']) || (isset($GLOBALS['l10n_text']['dbRecord']) && count($GLOBALS['l10n_text']['dbRecord']) == 0)) {
                    include DOC_ROOT . SUB_DIR . '/mf/languages/dbRecord_l10n.php';
                    $GLOBALS['l10n_text']['dbRecord'] = $l10nText;
                }

                //preload dbRecord labels
                $GLOBALS['l10n_text'][$recordClassName] = $GLOBALS['l10n_text']['dbRecord'];

                //then add user defined labels
                $this->loadL10nFile($recordClassName, $languageFilePath);

            }
        }

    }

    /**
     * Adds the content of an existing l10n array to the existing domain
     * @param $domain string unique name for an existing array of labels
     * @param $l10nArray array the array of labels to merge with the existing one.
     */
    static function loadL10nArray($domain,$l10nArray){ //addTemplatesL10n
        $GLOBALS['l10n_text'][$domain] = array_merge_recursive_distinct($GLOBALS['l10n_text'][$domain], $l10nArray);
    }

}
