<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class portfolioCategory extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_portfolio_categories';

    static $createTableSQL= "
        `category` varchar(255) NOT NULL DEFAULT 'escapade',
        `name` varchar(255) DEFAULT NULL,
        `description` text NOT NULL,
        `image` text NOT NULL,
        `slug` varchar(255) NOT NULL,
    ";

    static $createTableKeysSQL="
        UNIQUE KEY `URL` (`slug`),
        KEY `parent-category` (`parent_uid`)
    ";

    function init(){

        //load translations
        //$this->loadL10n('/mf/plugins/portfolio','records');

        parent::init();

        $this->data = array_merge($this->data, array(

            'name'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array('class' => 'input-lg'),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'parent_uid'   =>  array(
                'value' => '',
                'possibleValues' => $this->listCategoriesByUid(),
                'dataType' => 'select',
                'div_attributes' => array(
                    'class' => 'col-lg-4'
                )
            ),
            'description'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array('class' => 'input-lg'),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'image' => array(
                'dataType' => 'files jpg,jpeg,gif,png 620 0',
                'value' => '',
                'processor' => 'imagesPortfolioCategory',
                'div_attributes' => array(
                    'class' => 'col-lg-8'
                ),
            ),
            'slug'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'field_attributes' => array('class' => 'input-lg'),
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

        ));



        $this->showInEditForm = array(
            'tab_portfolioItem'=> array('name', 'description','image', 'parent_uid')
        );

    }

    function store($updateDates = true, $forceCreate = false, $debug=false){
        global $mf;

        $this->data['slug']['value'] = GenerateUrl($this->data['name']['value']);
        return parent::store($updateDates,$forceCreate,$debug);
    }

    function listCategoriesByUid(){
        global $mf,$pdo;

        if(tableExists(static::getTableName())) {
            //preparing request only once
            $sql = "SELECT * FROM " . static::getTableName() . " WHERE deleted = '0' AND language = '" . $mf->getDataEditingLanguage() . "' ORDER BY name";
            $stmt = $pdo->prepare($sql);


            try {
                $stmt->execute(array());
            } catch (PDOException $e) {
                echo $e->getMessage();
                return 0;
            }

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $categories = array(0 => '');

            foreach ($rows as $row) {
                $categories[$row['uid']] = $row['name'];
            }

            return $categories;
        }
        else return array();

    }

    function listCategoriesBySlug(){
        global $mf,$pdo;

        //preparing request only once
        $sql = "SELECT * FROM ".static::getTableName()." WHERE deleted = '0' AND language = '".$mf->getDataEditingLanguage()."' ORDER BY name";
        $stmt = $pdo->prepare($sql);


        try{
            $stmt->execute(array());
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $categories = array(0 => '');

        foreach($rows as $row){
            $categories[$row['slug']] = array('name' => $row['name'], 'uid' => $row['uid']);
        }

        return $categories;

    }



}


