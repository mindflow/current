<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/dbForm.php';


/**
 * This form provides the identification fields to the install tool.
 */
class installToolLoginForm extends dbForm{

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)

    var $sourceDB; //source database for records migration
    var $targetDB; //target / local database for records migration

    function init(){
        global $mf,$l10n;

        $l10n->loadL10nFile('installToolLoginForm', '/mf/languages/installToolLoginForm_l10n.php');

        parent::init();


        $this->data['password']            = array(
            'value' => '',
            'dataType' => 'input password',
            'valueType' => 'text',
            'field_attributes' => array('class' => 'input-md'),
            'div_attributes' => array('class' => 'col-lg-6'),

        );


        $this->showInEditForm = array(
            'tab_main'=>array('password')
        );


        static::$postFormHTML[] = "";

    }


}