<?php
/*
   Copyright 2017 Alban Cousinié <info@mindflow-framework.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Executes maintenance tasks when called on a regular basis.
 *
 * Executes maintenance tasks, such as cleaning up ressource identifier storage or executing a database indexing.
 * Add your commands using  $mf->addCronFunc('your_command()');, preferably from your plugin's index.php init() function
 * and call at regular interval the cron.php file from the crontab of your web hosting.
 *
 * sudo -iu dyntest1 php /var/dynamo/www/dyntest1/web/cron.php
 *
 */

//set you http host in order to load the your configuration file with the database parameters and all other settings
$_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'] = 'dyntest1.velorizons.com';

//read  CMS configuration
require_once 'mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';

#creation and execution of the frontend render
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();



global $mf;


if(isset($mf->info['cronFuncs']) && is_array($mf->info['cronFuncs'])){

	foreach($mf->info['cronFuncs'] as $cronFunc){
		eval($cronFunc);
	}
}