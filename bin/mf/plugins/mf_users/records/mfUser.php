<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf_librairies/Swift-5.4.5/lib/swift_required.php';

class mfUser extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_user';

    static $createTableSQL= "
        `civility` tinyint(4) DEFAULT NULL,
        `first_name` varchar(255) DEFAULT NULL,
        `last_name` varchar(255) DEFAULT NULL,
        `username` varchar(255) NOT NULL,
        `password` varchar(255) NOT NULL,
        `email` varchar(255),
        `frontend_locale` varchar(5) NOT NULL,
        `backend_locale` varchar(5) NOT NULL,
        `administrator` tinyint(4) DEFAULT 0,
        `user_group` int(11) DEFAULT NULL,
        `backend_access` int(11) NOT NULL DEFAULT 0,

        `address` text,
        `home_phone` varchar(255),
        `mobile_phone` varchar(255),
        `work_phone` varchar(255),

        `city` int(10) unsigned NOT NULL,
        `region` int(10) unsigned NOT NULL,
        `zip` varchar(7) default NULL,
        `notes` text,
        `sex` text,
        `birthdate` date default NULL,
    ";

    static $createTableKeysSQL="
          KEY `fk_mf_user_username` (`username`),
          KEY `fk_mf_user_group` (`user_group`),
          KEY `fk_mf_user_email` (`email`),
          FULLTEXT KEY `mf_user_search` (`first_name`,`last_name`,`username`,`email`)
        ";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    static $users; //cache for uid => username mapping table

    var $userGroup;

    static $enableImportInitializationData = 1;

    static function importInitializationData(&$html=array()){

        global $mf,$l10n,$pdo;

        //create default admin user (user "admin" / password "1234")
        $sql = "INSERT INTO `mf_user` (`uid`, `deleted`, `hidden`, `sorting`, `creation_date`, `modification_date`, `creator_uid`,  `edit_lock`, `start_time`, `end_time`, `language`, `alt_language`, `civility`, `first_name`, `last_name`, `username`, `password`, `email`, `frontend_locale`, `backend_locale`, `administrator`, `user_group`, `backend_access`, `address`, `home_phone`, `mobile_phone`, `work_phone`, `city`, `region`, `zip`, `notes`, `sex`, `birthdate`) VALUES
        (1, 0, 0, 0, NOW(), NOW(), 0, 0, null, null, 'ALL', 's:0:\"\";', 0, 'default', 'administrator', 'admin', '".password_hash("1234", PASSWORD_BCRYPT, array("cost" => 10))."', '', 'fr', 'fr', 1, 0, 1, '', '', '', '', 0, 0, '', '', '', NOW());
        ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $html[] = "<p><strong>".$l10n->getLabel('mfUser','default_user')."</strong></p>";

    }

    function init(){
        parent::init();

        global $mf,$l10n,$config;

        //$this->loadL10n('/mf/plugins/mf_users','records');

        $this->data['civility']                  = array(
            'value' => '0',
            'dataType' => 'select',
            'valueType' => 'number',
            'possibleValues' => array(
                0 => '',
                1 => $l10n->getLabel('mfUser','mr'),
                2 => $l10n->getLabel('mfUser','mme'),
                3 => $l10n->getLabel('mfUser','mlle'),
                //1 => $l10n->getLabel('mfUser','mrs'),
                //2 => $l10n->getLabel('mfUser','mr'),
            ),
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('',0)
            ),
            'field_attributes' => array('class' => 'input-md'),
            'div_attributes' => array('class' => 'col-lg-2'),
        );

        $this->data['last_name']                  = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('')
            ),
            'field_attributes' => array('class' => 'input-lg'),
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        $this->data['first_name']               = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('')
            ),
            'field_attributes' => array('class' => 'input-lg'),
            'div_attributes' => array('class' => 'col-lg-4'),

        );




        $this->data['username']        = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array(''),
                'filterFunc' => '$this->validateUsername'
            ),
            'div_attributes' => array('class' => 'col-lg-4'),

        );
        $this->data['password']            = array(
            'value' => '',
            'dataType' => 'input password',
            'valueType' => 'text',
            'preRecord' => '$this->cryptPassword',
            'preDisplay' => 'mfUser::showPasswordDummy',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('')
            ),
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        $this->data['password_confirmation']            = array(
            'value' => '',
            'dataType' => 'dummy-input password',
            'valueType' => 'text',
            'preDisplay' => 'mfUser::showPasswordDummy',
            'validation' => array(
                'mandatory' => '1',
                'filterFunc' => '$this->confirmPassword'
            ),
            'div_attributes' => array('class' => 'col-lg-4'),
        );


        $this->data['email']             = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '0',
                'filterFunc' => 'validateEmail'
            ),
            'div_attributes' => array('class' => 'col-lg-4'),
        );
		
		$this->data['backend_access']             = array(
            'value' => '1',
            'dataType' => 'checkbox',
            'valueType' => 'number',
			'swap' => true,
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        //default locale for frontend display
        $this->data['frontend_locale'] = array(
            'value' => $config['default_frontend_locale'],
            'dataType' => 'input',
            'valueType' => 'text',
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        //default locale for backend display
        $this->data['backend_locale'] = array(
            'value' => $config['default_backend_locale'],
            'dataType' => 'select',
            'possibleValues' => $config['backend_locales'],
            'valueType' => 'text',
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        $this->data['administrator']             = array(
            'value' => '0',
            'dataType' => 'checkbox',
            'valueType' => 'number',
            'swap' => true,
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        //userGroup to inherit rights from
        //userGroup to inherit rights from
        $this->data['user_group'] = array(
            'value' => '0',
            'dataType' => 'record', // mfUserGroup group_name
            'editType' => 'single', //single or multiple
            'editClass' => 'mfUserGroup',
            'valueType' => 'number',
            'div_attributes' => array('class' => 'col-lg-4'),

            'initItemsSQL' => array(
                'SELECT' => 'group_name',
                'FROM' => 'mf_usergroup',
                'WHERE' => 'deleted=0', 
                'ORDER BY' => 'group_name',
                'ORDER DIRECTION' => 'ASC',
            ),

            'selectActions' => array(
                'view' => 0,
                'edit' => 1,
                'create' => 1,
                'delete' => 1,
                'sort' => 0
            ),
            'icon' => '<b class="glyphicon glyphicon-user"></b>',
            'icons-wrap' => false,
            'debugSQL' => false
        );



        $this->data['address']              = array(
            'value' => '',
            'dataType' => 'textarea 8 3',
            'valueType' => 'text',

            'div_attributes' => array('class' => 'col-lg-6'),
        );

        $this->data['home_phone']           = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text' ,
            'field_attributes' => array('class' => 'input-small'),
            'noClearFix' => '1',
            'div_attributes' => array('class' => 'col-lg-3'),

        );

        $this->data['mobile_phone']           = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text' ,
            'field_attributes' => array('class' => 'input-small'),
            'noClearFix' => '1',
            'div_attributes' => array('class' => 'col-lg-3'),

        );

        $this->data['work_phone']           = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text' ,
            'field_attributes' => array('class' => 'input-small'),
            'noClearFix' => '1',
            'div_attributes' => array('class' => 'col-lg-3'),

        );


        $this->data['city']              = array(
            'value' => '0',
            'dataType' => 'select',
            'possibleValues' => array(),//ville::listVillesByUid(),
            'valueType' => 'number',
            'field_attributes' => array(
                'onChange' => '', //'updateArrondissements();updateQuartiers();'
            ),

            'div_attributes' => array('class' => 'col-lg-4'),

        );


        $this->data['region']             = array(
            'value' => '0',
            'dataType' => 'select',
            'possibleValues' => array(), //region::listRegionsByUid(),
            'valueType' => 'number',
            'field_attributes' => array(
                'onChange' => '', //'updateVilles();'
            ),

            'div_attributes' => array('class' => 'col-lg-4'),
        );



        $this->data['zip']           = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',

            'div_attributes' => array('class' => 'col-lg-2'),
        );


        $this->data['notes']         = array(
            'value' => '',
            'dataType' => 'textarea',
            'valueType' => 'text',
            'div_attributes' => array('class' => 'col-lg-6'),
        );

        $this->data['sex']                  = array(
            'value' => '',
            'dataType' => 'select',
            'valueType' => 'number',
            'possibleValues' => array(0=> 'femme', 1=>'homme'),
            'div_attributes' => array('class' => 'col-lg-3'),
        );

        $this->data['birthdate']        = array(
            'value' => '',
            'dataType' => 'date',
            'valueType' => 'date',
            'div_attributes' => array(
                'class' => 'col-lg-3'
            ),
            'settings'=> array(
                'minDate' => '"01/01/1900"',
                'showToday' => 'true',
                'defaultDate' => '"01/01/1950"',
            )
        );


        //disable editing of some fields
        $this->data['creation_date']['field_attributes']['disabled'] = '';
        $this->data['modification_date']['field_attributes']['disabled'] = '';
        $this->data['creation_date']['editable']=0;
        $this->data['modification_date']['editable']=0;




        $this->showInEditForm = array(
            'tab_main'=>array(
                'creation_date','modification_date', 'civility', 'first_name', 'last_name', 'username', 'password', 'password_confirmation','backend_access', 'email', 'administrator', 'user_group', 'backend_locale'			//'frontend_locale', 'backend_locale',
                //'address', 'home_phone', 'mobile_phone', 'work_phone',  'city', 'region', 'zip', 'notes', 'sex', 'birthdate')
				)
        );




    }


    function validateUsername($username){
        global $pdo;

        if($this->data['uid']['value'] !=''){
            $currentUserUid = $this->data['uid']['value'];

            $sql = "SELECT COUNT(*) FROM ".self::$tableName." WHERE uid != :currentUserUid AND username = :username AND deleted = '0' ";
        }
        else{
            $sql = "SELECT COUNT(*) FROM ".self::$tableName." WHERE username = :username AND deleted = '0' ";
        }

        $stmt = $pdo->prepare($sql);

        try{
            if($this->data['uid']['value'] !=''){
                $stmt->execute(array(':currentUserUid' => $currentUserUid,':username' => $username));
            }
            else $stmt->execute(array(':username' => $username));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $totalRecordCount = $stmt->fetchColumn();

        if ($totalRecordCount > 0) return false;
        else return true;
    }


    static function showPasswordDummy($key,$value,$locale,$additionalValue=''){
        return 'password';
    }

    function cryptPassword($newValue, $previousValue){
        if($newValue != 'password'){
            //if the password has changed, crypt the new password
            //echo "Password has changed, cryptPassword returns ".crypt($newValue, $config['salt'])."<br />".chr(10);
            return password_hash($newValue, PASSWORD_BCRYPT, array("cost" => 10));
        }
        //else do not alter the password, as it is already encrypted
        else {
            //echo "Password is the same, cryptPassword returns ".$previousValue."<br />";
            return $previousValue;
        }
    }

    function confirmPassword($newValue){

        if($_REQUEST['record|password'] != 'password' or $newValue != ''){
            $hash =  password_hash($_REQUEST['record|password'], PASSWORD_BCRYPT, array("cost" => 10));

            if($_REQUEST['record|password'] == $this->data['password']['value'] or $hash == $this->data['password']['value']){
                //echo "password has not changed confirmPassword returns 1<br />".chr(10);
                return 1;

            }
            //else validate
            else {
                //if the password has changed, test against confirmation
                //echo "password has changed, test against confirmation confirmPassword returns ".($_REQUEST['record|password_confirmation'] == $_REQUEST['record|password'])."<br />.chr(10)";
                return ($_REQUEST['record|password_confirmation'] == $_REQUEST['record|password']);
            }
        }
        else return 1;
    }



    static function sendPassword($email){
        global $mf,$l10n,$pdo,$config;

        $sql = "SELECT uid, email, first_name, last_name, username FROM ".self::$tableName." WHERE email=:email AND deleted=0;";

        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array(':email'=>$email));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        if($stmt->rowCount() > 0){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $validEmail = $row['email'];

            //Generate new password
            $newPassword = generateStrongPassword();


            //build recipient list
            $recipients = array($validEmail => '');

            //Send the password
            // Create the message
            $message = Swift_Message::newInstance();

            try{
                $message->setSubject($config['sitename']." : ".$l10n->getLabel('mfUser','reset_password'))
                    ->setFrom(array($config[$mf->getLocale()]['siteEmailSMTPEmail'] => $config['sitename']))
                    ->setTo($recipients)
                    ->setCharset('utf-8');
            }
            catch (Swift_RfcComplianceException $e){
                $html[] = $l10n->getLabel('mfUser','error').' : '.$e->getMessage().'<br />';
            }

            $StyleCSS = '<style type="text/css">
                    </style>';

            $body = "<html>
            <head>
            ".$StyleCSS."
            </head>
            <body>
            <p>".$l10n->getLabel('mfUser','hello')." ".$row['first_name']." ".$row['last_name']."</p>
            <p>".$l10n->getLabel('mfUser','reset_password2').":<br><br>
            ".$l10n->getLabel('mfUser','username')." : <strong>".$row['username']."</strong><br />
            ".$l10n->getLabel('mfUser','password')." : <strong>".$newPassword."</strong></p>
            <p><a href='https://".$_SERVER['SERVER_NAME']."/mf/index.php'>".$l10n->getLabel('mfUser','click_here')."</a> ".$l10n->getLabel('mfUser','to_connect')."</p>
            </body>
         </html>";

            //put client logo
            //$image = '<p><img src="' .$message->embed(Swift_Image::fromPath(DOC_ROOT.SUB_DIR.$config['plugins']['contacts']['email-image'])).'" alt="Image" /></p>';
            //$emailTemplate = str_replace("{email-image}",$image,$emailTemplate);

            //form recording success. Send notification email
            $message->addPart($body, 'text/html');

            // Create the Transport
            $transport = Swift_SmtpTransport::newInstance($config[$mf->getLocale()]['siteEmailSMTPServer'],$config[$mf->getLocale()]['siteEmailSMTPPort'])
                ->setUsername($config[$mf->getLocale()]['siteEmailSMTPUsername'])
                ->setPassword($config[$mf->getLocale()]['siteEmailSMTPPassword']);

            if(isset($config[$mf->getLocale()]['siteEmailSMTPEncryption']) && $config[$mf->getLocale()]['siteEmailSMTPEncryption']!=null)$transport->setEncryption($config[$mf->getLocale()]['siteEmailSMTPEncryption']);
            // Create the Mailer using created Transport
            $mailer = Swift_Mailer::newInstance($transport);

            $logMails = (isset($config[$mf->getLocale()]['enableEmailLog']) && $config[$mf->getLocale()]['enableEmailLog']== true);
            if($logMails){
                $logger = new Swift_Plugins_Loggers_ArrayLogger();
                $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
            }

            try{
                // Send the message
                $numSent = $mailer->send($message);
            }
            catch (Exception $e){
                $html[] = $l10n->getLabel('mfUser','error').' : '.$e->getMessage().'<br />';
            }

            if($logMails){
                logMails(
                    $config[$mf->getLocale()]['siteEmailSMTPEmail'],
                    $validEmail,
                    $subject,
                    $logger->dump()
                );
            }

            //Update user's password with new password
            $user = new mfUser();
            $user->load($row['uid']);
            $user->data['password']['value'] = password_hash($newPassword, PASSWORD_BCRYPT, array("cost" => 10));
            $user->store();

            return 1;
        }
        else return 0;
    }


    static function getUserName($key,$uid,$locale,$row){
        if($uid != null && $uid != '' && $uid > 0){
            //performance tweak, accorderies are loaded only once if repetitive calls
            if(!self::$users)self::$users = self::listUsersByUid();
            else if(isset(self::$users[$uid]))return self::$users[$uid];
            else return $uid;
        }
        else return '';
    }

    static function listUsersByUid(){

        //no SQL query performed if array is already loaded in singleton
        if(!is_array(self::$users)){
            global $pdo;

            //preparing request only once
            $sql = "SELECT * FROM ".self::$tableName." WHERE deleted = '0' ORDER BY last_name";
            $stmt = $pdo->prepare($sql);


            try{
                $stmt->execute(array());
            }
            catch(PDOException $e){
                echo $e->getMessage();
                return 0;
            }

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            self::$users = array(0 => '');

            foreach($rows as $row){
                self::$users[$row['uid']] = strtoupper($row['last_name']).' '.$row['first_name'];
            }
            return self::$users;
        }
        else return self::$users;
    }

    /*
     * Returns the user's userGroup or null if no group is defined
     */
    function getUserGroup(){
        if(!isset($this->userGroup)){
            if($this->data['user_group']['value']!=0){
                $this->userGroup = new mfUserGroup();
                $this->userGroup->load($this->data['user_group']['value']);
                return $this->userGroup;
            }
            else return null;
        }
        else return $this->userGroup;
    }

    function isAdmin(){
        return $this->data['administrator']['value'];
    }

    function reload(){
        //reload user profile
        $this->load($this->data['uid']['value']);

        //reload userGroup as well
        $this->userGroup = new mfUserGroup();
        $this->userGroup->load($this->data['user_group']['value']);

    }

	static function nameUC($key,$value,$locale,$row){
		global $pdo;
		return $pdo->quote(strtoupper($value));
	}

}















