<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

define("TOP_PRIORITY",     0);
define("HIGH_PRIORITY",    100);
define("MEDIUM_PRIORITY",  200);
define("LOW_PRIORITY",     300);
define("LOOSE_PRIORITY",   400);

/**
 * Handles all plugin related tasks.
 *
 * The plugin manager handles loading of a plugin and its localization files at startup, display of the plugin entry in backend menus.
 * It also supplies plugin tables management functions from the install tool.
 */
class pluginManager{

    //stores plugin instances
    var $plugins;
    var $modulesByMode;
    var $modulesByName;
    var $renderFunction;

    //stores references to all available dbRecord subclasses
    var $records;

    //stores all the items that should be rendered in the menus
    private $backendMenus;
    //stores all the buttons that should be rendered in the menus
    private $backendButtons;

    function __construct(){

        global $mf,$logger;

        $this->plugins = array();
        $this->modulesByMode = array();
        $this->modulesByName = array();
        $this->records = array();


        $this->backendMenus = array(0,100,200,300,400);
        foreach($this->backendMenus as $priority => $menu){
            $this->backendMenus[$priority] = array();
        }
        $this->backendButtons = array(0,100,200,300,400);
        foreach($this->backendButtons as $priority => $button){
            $this->backendButtons[$priority] = array();
        }

    }



    function prepareData(){
        global $mf,$logger;

        if($mf->mode == 'backend'){
            //current user must be defined / logged in to access backend+
            if(isset($this->modulesByMode[$mf->mode]))
                foreach($this->modulesByMode[$mf->mode] as $module){

                    //currentUser must be tested for every plugin in case of logout otherwise it will raise an error
                    if(isset($mf->currentUser)){
                    	$module->prepareData();
                    }
                }
        }

        else if($mf->mode == 'frontend' || $mf->mode == 'authentification'){

            if(isset($this->modulesByMode[$mf->mode]))
                foreach($this->modulesByMode[$mf->mode] as $module){
                    $module->prepareData();
                }

        }
    }

	/**
	 * Alters the default render function and replaces it by the specified code which will be evaluated
	 * This is useful when using an API to render JSON, REST or XML, when a specific url segment is being called
	 * @param string $renderFunc the code to be evaluated when executing the render() stage
	 */
    function setRenderFunction($renderFunc){
	    global $logger;

	    $logger->debug("pluginManager->setRenderFunction($renderFunc);");

    	$this->renderFunction = $renderFunc;
    }

    function render(&$mainTemplate){
        global $mf,$l10n,$logger;

        //execute alternate render function, such as when using an API to render JSON, REST or XML when an url segment is being called
        if(isset($this->renderFunction))eval($this->renderFunction);

        //or run usual Mindflow render pipeline
	    else if($mf->mode == 'backend'){

            //current user must be defined / logged in to access backend
            if(isset($mf->currentUser)){

                //debug_print_backtrace();die();
                //running plugins
                if(isset($this->modulesByMode[$mf->mode][$mf->xEnd->currentModule])){
                    $this->modulesByMode[$mf->mode][$mf->xEnd->currentModule]->render($mainTemplate);
                }
                else{
                    $errorMsg  = "<p class=text-error>".str_replace("%p",$mf->xEnd->currentModule,$l10n->getLabel('main','plugin_not_loaded'))."</p>";
                    $errorMsg  .= "<p class=text-error>".nl2br(generateCallTrace())."</p>";
                    $mainTemplate = str_replace("{current_module}",$errorMsg,$mainTemplate);
                }

                //must be run AFTER plugins so the backend menu gets populated
                $mainTemplate = str_replace("{backend-menus}",$this->getBackendMenusHTML(),$mainTemplate);
                $mainTemplate = str_replace("{locale}",$mf->getLocale(),$mainTemplate);
            }

        }

        else if($mf->mode == 'frontend' || $mf->mode == 'authentification'){

            if(isset($this->modulesByMode[$mf->mode]))
                foreach($this->modulesByMode[$mf->mode] as $module){

                	$module->render($mainTemplate);
                }

        }
    }



    /*
    * Loads in memory all the plugins specified into $config['load_plugins']
    */
    function loadAllPlugins(){
        global $mf,$l10n,$config;

        $loadPluginList = explode(',',str_replace (' ','',$config['load_plugins'])); //get rid of nasty spaces

        foreach( $loadPluginList as $pluginName){

            if(trim($pluginName) != ''){

                //loading mindflow plugins
                if(file_exists(DOC_ROOT.SUB_DIR.'/mf/plugins/'.$pluginName.'/index.php')){
                    //mindflow 1.3+ plugin style loading
                    //allows loading several plugins per folder
                    //the file index.php commands plugin loading
                    require_once DOC_ROOT.SUB_DIR.'/mf/plugins/'.$pluginName.'/index.php';
                }
                //loading website plugins
                else if(file_exists(DOC_ROOT.SUB_DIR.$config['website_pluginsdir'].'/'.$pluginName.'/index.php')){
                    //mindflow 1.3+ plugin style loading
                    //allows loading several plugins per folder
                    //the file index.php commands plugin loading
                    require_once DOC_ROOT.SUB_DIR.$config['website_pluginsdir'].'/'.$pluginName.'/index.php';

                }
                else if(is_dir(DOC_ROOT.SUB_DIR.'/mf/plugins/'.$pluginName)){
                    //MindFlow 1.2 plugin style loading
                    //allows loading only one plugin per plugin folder
                    //the plugin class name has to be the same as the folder name

                    //create a plugin instance
                    $this->loadModule('/mf/plugins/'.$pluginName,"",$pluginName);
                }
                else {$mf->addErrorMessage("<p>pluginManager->loadAllPlugins() : ".$l10n->getLabel('main','the_plugin')." \"".$pluginName."\" ".$l10n->getLabel('main','not_found').'</p>'.
                    '<p>'.DOC_ROOT.SUB_DIR.'/mf/plugins/'.$pluginName.'/index.php'.'<br/>'.
                    DOC_ROOT.SUB_DIR.$config['website_pluginsdir'].'/'.$pluginName.'/index.php'.'<br/>'.
                    '/mf/plugins/'.$pluginName.'</p>'
                );}
            }
        }
    }

    /**
     * Loads a plugin' module in memory and make it available for access from other classes. Make sure parameter autoRendering is set to true if the module should be added to the render list in frontend or backend.
     * @param $pluginRelativePath string Plugin folder path relative to website document root. ex : $config['website_pluginsdir']."/my_videos"
     * @param $moduleSubDirName string Subfolder where the module php file will be found. ex : modules
     * @param $moduleClassName string module class name. ex : editVideos.php
     * @param bool $autoRendering States if the plugin should be added to render list or not. Some plugins do not need to be shown in frontend or backend and have autorendering set to false.
     */
    function loadModule($pluginRelativePath,$moduleSubDirName,$moduleClassName, $autoRendering = true){
        global $mf,$l10n,$logger;

        //create a plugin instance

	    include_once DOC_ROOT.SUB_DIR.$pluginRelativePath.(($moduleSubDirName != '')?'/'.$moduleSubDirName:'').'/'.$moduleClassName.'.php';

        //$logger->info($l10n->getLabel('main','loading_plugin').DOC_ROOT.SUB_DIR.$pluginRelativePath.(($moduleSubDirName != '')?'/'.$moduleSubDirName:'').'/'.$moduleClassName.'.php');
        $currentModule =  new $moduleClassName();

        //$logger->info($l10n->getLabel('main','init_plugin').$moduleClassName);

        if($autoRendering){
            //check if the current plugin type is registered
            //if not, automatically create a plugin type classification in the array for it
            $currentModuleTypes = explode(',', $currentModule->getType());
            foreach($currentModuleTypes as $pluginType){
                if(!isset($this->modulesByMode[$pluginType])){
                    $this->modulesByMode[$pluginType]=array();
                    //$logger->info($l10n->getLabel('main','create_plugin_type').$pluginType);
                }
                //store the plugin classified by its plugin type. This will be used as a render list.
                $this->modulesByMode[$pluginType][$moduleClassName] = $currentModule;
                //or by its name
                $this->modulesByName[$moduleClassName] = $currentModule;
            }
        }
        //do not add the plugin to the render list, only store it by its name
        else $this->modulesByName[$moduleClassName] = $currentModule;

        //load the plugin's translation files
        include DOC_ROOT.SUB_DIR.$pluginRelativePath.'/languages'.(($moduleSubDirName != '')?'/'.$moduleSubDirName:'').'/'.$moduleClassName.'_l10n.php';

        eval( '$GLOBALS[\'l10n_text\'][\''.$moduleClassName.'\'] =  $l10nText;');

    }

    /**
     * Checks if a module has been loaded by MindFlow
     * @param $moduleClassName string the class name of the requested module
     * @return bool true if the module correctly loaded. false otherwise.
     */
    function isModuleLoaded($moduleClassName){
        return isset($this->modulesByName[$moduleClassName]);
    }

    /**
     * Returns the current running instance of the requested module.
     * @param $moduleClassName string the class name of the requested module
     * @return mixed object returns the instanciated class of the module
     */
    function getModuleInstance($moduleClassName){
        return $this->modulesByName[$moduleClassName];
    }

    /**
     * Adds a plugin to MindFlow and executes its init() method
     * @param $plugin an instance of a class extending the plugin interface
     */
    function addPluginInstance($plugin){
        global $l10n;
        l10nManager::loadL10nFile(get_class($plugin), $plugin->getPath().'/languages/'.get_class($plugin).'_l10n.php');

        $this->plugins[$plugin->getKey()] = $plugin;
        $plugin->init();

    }

    /**
     * Adds a menu to the backend navigation
     * @param string $menuKey must match a key from backend_l10n so the menu label can be written translated. You can extend it and add your own values this way :
     *                  include DOC_ROOT.SUB_DIR.'/mf/plugins/my_plugin/languages/backendMenus_l10n.php';
     *                  $GLOBALS['l10n_text']['backend'] = array_merge_recursive_distinct ($GLOBALS['l10n_text']['backend'],$l10nText);
     * @param int $menuPriority indicates if this entry should be shown with higher or lower priority (before or after other items). Look at the priority constants at the top of pluginManager.php for values.
     * @param string $type either "menu" or "button"
     */
    function addBackendMenu($menuKey, $menuPriority = MEDIUM_PRIORITY, $type = "menu"){
        if(!isset($this->backendMenus[$menuPriority][$menuKey]))$this->backendMenus[$menuPriority][$menuKey] = array(
            'entries'=>array(),
            'type'=>'menu'
        );
    }

    /**
     * Checks if a menu key is already defined or not.
     * @param $menuKey the menu key, for example 'datas'
     * @return bool true if the key already exists
     */
    function backendMenuExists($menuKey){
        foreach($this->backendMenus as $priority => $menu){
            if(isset($menu[$menuKey]))return true;
        }
        return false;
    }

    /*
     * Adds a button to the backend navigation between menus
     * $buttonKey string  must match a key from backend_l10n so the button label can be written translated. You can extend it and add your own values this way :
     *                  include DOC_ROOT.SUB_DIR.'/mf/plugins/my_plugin/languages/backendButtons_l10n.php';
     *                  $GLOBALS['l10n_text']['backend'] = array_merge_recursive_distinct ($GLOBALS['l10n_text']['backend'],$l10nText);
     * $buttonHTML int  the HTML including the link to the module, like <a href="{subdir}/mf/index.php?module=accueil">My button</a>
     * $menuPriority int indicates if this entry should be shown with higher or lower priority (before or after other items)

    function addBackendMenuButton($buttonHTML, $buttonKey, $menuPriority = MEDIUM_PRIORITY){

        if(!isset($this->backendMenus[$menuPriority][$buttonKey]))$this->backendMenus[$menuPriority][$buttonKey] = array(
            'entries'=>array(0 => $buttonHTML),
            'type'=>'button'
        );
    }*/

    /**
     * @param $entryHTML
     * @param $menuKey
     * @param int $itemPriority indicates if this entry should be shown with higher or lower priority (before or after other items). Look at the priority constants at the top of pluginManager.php for values.
     */
    function addEntryToBackendMenu($entryHTML, $menuKey, $itemPriority = MEDIUM_PRIORITY){
        //echo "addEntryToBackendMenu $entryHTML, $menuKey, $itemPriority <br />".chr(10);
        $found = false;
        $menuPriority = -1;

        //seek menu containing $menuKey
        foreach($this->backendMenus as $priority => $menu){
            if(isset($menu[$menuKey])){
                $found=true;
                $menuPriority = $priority;
                break;
            }
        }
        //create menu if it doesn't exist
        if(!$found){
            //apply given priority
            $menuPriority = $itemPriority;
            $this->backendMenus[$menuPriority][$menuKey]=array(
                'entries' => array(),
                'type' => 'menu'
            );
        }
        if(!isset($this->backendMenus[$menuPriority][$menuKey]['entries'][$itemPriority]))$this->backendMenus[$menuPriority][$menuKey]['entries'][$itemPriority]=array();
        $this->backendMenus[$menuPriority][$menuKey]['entries'][$itemPriority][] = $entryHTML;

    }

    /*
    function getBackendMenuIDs(){
        $top_keys       = array_keys($this->backendMenus[TOP_PRIORITY]);
        $high_keys      = array_keys($this->backendMenus[HIGH_PRIORITY]);
        $medium_keys    = array_keys($this->backendMenus[MEDIUM_PRIORITY]);
        $low_keys       = array_keys($this->backendMenus[LOW_PRIORITY]);
        $lose_keys       = array_keys($this->backendMenus[LOOSE_PRIORITY]);

        return array_merge($top_keys, $high_keys, $medium_keys, $low_keys, $lose_keys);
        //return array_keys($this->backendMenus);
    }*/

    function getBackendMenusHTML(){

        global $mf,$l10n;

        $html = array();

        ksort($this->backendMenus);

        //print_r($this->backendMenus);
        //print_r($GLOBALS['l10n_text']['backend']);

        foreach($this->backendMenus as $menuPriority => $menu){

            foreach($menu as $menuKey => $menuContent){

                if($menuContent['type']=='menu'){
                    //echo "path 1 k=".$menuKey."<br />\n";

                    //print_r($menuKey);echo "\n\n";
                    $html[] = '<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$l10n->getLabel('backend',$menuKey).'<b class="caret"></b></a>
                                    <ul class="dropdown-menu">';

                    ksort($menuContent['entries']);

                    foreach($menuContent['entries'] as $itemPriority => $menuItem){
                        foreach($menuItem as $key => $menuEntry){
                            $html[] = '<li>'.$menuEntry.'</li>';
                        }
                    }

                    $html[] = '     </ul></li>';
                }
                else if($menuContent['type']=='button'){

                    //echo "path 2 k=".$menuKey."<br />\n";

                    $html[] = '<li>'.$menuContent['entries']['0'].'</li>';
                }

            }

        }
        return implode(chr(10),$html);

    }

    /**
     * Registers a dbRecord class with the plugin manager.
     * Use it in the index.php file of you plugin to inform MindFlow of the new dbRecord classes supplied by your plugin.
     * This will enable your dbRecord class to appear in the install tool.
     *
     * @param string $pluginKey Plugin key (should match plugin directory name) of the plugin featuring this record
     * @param string $recordClassName Class name of the record
     * @param string $recordClassRelativePath Path below the web hosting root where the class can be found
     * @param bool $enableSQLTableCreation Sometimes, when a record subclasses another (for example when you have a record class for frontend view and another for backend view), you may not want the installer to attempt to create the SQL table twice. Set this parameter to false for that purpose.
     */
    function addRecord($pluginKey, $recordClassName, $recordClassRelativePath, $enableSQLTableCreation=true){
        global $mf,$l10n;

        //load plugin class file
        require_once(DOC_ROOT.SUB_DIR.$recordClassRelativePath);

        if(!isset($this->records[$pluginKey]))$this->records[$pluginKey]=array();
        if(!isset($this->records[$pluginKey][$recordClassName]))$this->records[$pluginKey][$recordClassName]=array();
        $this->records[$pluginKey][$recordClassName]['path'] = $recordClassRelativePath;
        $this->records[$pluginKey][$recordClassName]['createTable'] = $enableSQLTableCreation;

        //Automatically load language file for record
        $l10n->loadL10nRecord($recordClassName,$this->plugins[$pluginKey]->getPath().'/languages/records/'.$recordClassName.'_l10n.php');

    }

    /*
     * Used by the install tool to create all the tables of all plugins required by dbRecord classes in use.
     */
    /*function createAllTables(&$html){
        global $mf,$l10n;

        //process all records
        foreach($this->records as $pluginKey => $recordsList){
            foreach($recordsList as $recordClassName => $recordClassItem){
                if($recordClassItem['createTable']) {
                    require_once(DOC_ROOT . SUB_DIR . $recordClassItem['path']);
                    $currentRecord =  new $recordClassName();
                    $status = 0;
                    eval('$status = $currentRecord->createSQLTable(1, $html);');
                    if ($status == 0) {
                        $html[] = '<div class="alert alert-danger">' . $l10n->getLabel('installTool', 'create_table_error') . ' ' . $recordClassName . '</div><br />';
                    } else $html[] = '<div class="alert alert-success">' . $l10n->getLabel('installTool', 'table') . ' ' . $recordClassName . ' ' . $l10n->getLabel('installTool', 'create_table_success') . '</div><br/>';
                }
            }
        }
    }*/

    /*
     * Used by the install tool to create all the tables of a specified plugin required by dbRecord classes in use.
     * The function also imports the tables initialization data if available.
     */
    function createTables($pluginKey='', &$html){
        global $mf,$l10n,$config;

        //process all records
        foreach($this->records[$pluginKey] as $recordClassName => $recordClassItem){
            if($recordClassItem['createTable']) {

                require_once(DOC_ROOT . SUB_DIR . $recordClassItem['path']);
                $currentRecord =  new $recordClassName();
                $status = 0;
                if($currentRecord::$oneTablePerLocale == true){
                    //if the record maintains one table per locale

                    //save current locale
                    $currentLocale = $mf->info['data_editing_locale'] ;

                    //create a table for each locale
                    foreach ($config['frontend_locales'] as $locale => $localeName){
                        //switch the locale
                        $mf->info['data_editing_locale'] = $locale;
                        //create the table
                        $status &= $currentRecord->createSQLTable(1, $html,true);
                    }

                    //restore current locale
                    $mf->setDataEditingLanguage($currentLocale);
                }
                else $status = $currentRecord->createSQLTable(1, $html,true);

                if ($status == 0) {
                    $html[] = '<div class="alert alert-danger">' . $l10n->getLabel('installTool', 'create_table_error') . ' ' . $recordClassName . '</div><br />';
                } else $html[] = '<div class="alert alert-success">' . $l10n->getLabel('installTool', 'table') . ' ' . $recordClassName . ' ' . $l10n->getLabel('installTool', 'create_table_success') . '</div><br/>';

            }
        }
    }

    /**
     * Used by the install tool to create a single table required by dbRecord classes in use.
     * @param string $pluginKey Plugin key (should match plugin directory name) of the plugin featuring this record
     * @param string $recordClassName Class name of the record
     * @param array $html Array where the HTML code giving feedback from the function will be put
     */
    function createTable($pluginKey, $recordClassName, $locale=null, &$html){
        global $mf,$l10n,$config;

        $locale = (isset($locale))?$locale:$mf->getLocale();

        require_once(DOC_ROOT . SUB_DIR . $this->records[$pluginKey][$recordClassName]['path']);

        $currentRecord =  new $recordClassName();

        $status = 0;

        if($currentRecord::$oneTablePerLocale == true){
            //if the record maintains one table per locale

            //save current locale
            $currentLocale = $mf->info['data_editing_locale'] ;
            //alter the locale
            $mf->info['data_editing_locale'] = $locale;
            //create the table
            $status = $currentRecord->createSQLTable(1, $html,false);

            //restore current locale
	        $mf->setDataEditingLanguage($currentLocale);
        }
        else $status = $currentRecord->createSQLTable(1, $html,false);

        if($status == 0){
            $html[] = '<div class="alert alert-danger">'.$l10n->getLabel('installTool','create_table_error').' '.$recordClassName.'</div><br />';
        }
        else $html[] = '<div class="alert alert-success">'.$l10n->getLabel('installTool','table').' '.$recordClassName.' '.$l10n->getLabel('installTool','create_table_success').'</div><br/>';

    }


    /**
     * Used by the install tool to import the initialization data associated to a dbRecord table.
     * @param string $pluginKey Plugin key (should match plugin directory name) of the plugin featuring this record
     * @param string $recordClassName Class name of the record
     * @param array $html Array where the HTML code giving feedback from the function will be put
     */
    function importTable($pluginKey, $recordClassName, &$html){

        eval($recordClassName.'::importInitializationData($html);');

    }


    /**
     * Used by the install tool to delete a single dbRecord table.
     * @param string $pluginKey Plugin key (should match plugin directory name) of the plugin featuring this record
     * @param string $recordClassName Class name of the record
     * @param array $html Array where the HTML code giving feedback from the function will be put
     */
    function deleteTable($pluginKey, $recordClassName, &$html){
        global $l10n,$pdo;

        eval('$tableName = '.$recordClassName.'::getTableName();');

        //fix for blocking session error if the admin is logged in the backend and deletes table mf_user
        if($tableName == 'mf_user')unset($_SESSION['current_be_user']);

        //create default admin user (user "admin" / password "1234")
        $sql = "DROP TABLE ".$tableName.";";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $html[] = "<strong>".$l10n->getLabel('main','table_delete1')." ".$tableName." ".$l10n->getLabel('main','table_delete2')."</strong>";

    }

}



?>