<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';

class mfUserGroup  extends dbRecord
{

    //table name in the SQL database
    static $tableName = 'mf_usergroup';

    static $createTableSQL= "
        `group_name` varchar(255) DEFAULT NULL,
        `template_name` varchar(255) NOT NULL,
        `template_content` longblob,
    ";

    //ALTER TABLE  `mf_usergroup` ADD  `parent_uid` INT NOT NULL DEFAULT  '0'


    static $createTableKeysSQL="";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)

    // base group template for storing rights values
    static $groupTemplate = array(
        'userRights' => array(
            'template_name' => 'userRights',
            'template_file' => '',
            'template_data' => array(
                'group_description' => array(
                    'dataType' => 'textarea 4 3',
                    'value' => '',
                    'valueType' => 'text',
                    'index' => 'digest',
                    'processor' => '',
                    'div_attributes' => array(
                        'class' => 'col-lg-4'
                    )
                ),

            ),
            'showInEditForm' =>'group_description',
            'hideInTemplateList' => 1,
        )
    );

    static $groups; //cache for uid => group mapping table

    function init(){
        parent::init();

        global $mf;
        //$this->loadL10n('/mf/plugins/mf_users','records');


        //add the group template to mindflow templates list, but it is hidden (see setting 'hideInTemplateList' above)
        $mf->addTemplates(self::$groupTemplate);


        $this->data['group_name']                  = array(
            'value' => '',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('')
            ),
            'field_attributes' => array('class' => 'input-lg'),
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        //the template_content key will hold the user rights in a custom form
        $this->data['template_content']   =  array(
            'value' => self::$groupTemplate['userRights']['template_data'], //array(),
            'dataType' => 'template_data',
            'processor' => '',
            'valueType' => 'template',
            'div_attributes' => array(
                'class' => 'templateContentBlock col-lg-8'
            ),
        );

         $this->data['template_name']   =  array(
            'value' => 'userRights',
            'dataType' => 'template_name',
            'valueType' => 'text',
            'processor' => '',
            'div_attributes' => array('class' => 'hidden'),
        );

        //overwrite definition for parent_uid
        $this->data['parent_uid']                  = array(
            'value' => '0',
            'dataType' => 'input',
            'valueType' => 'text',
            'validation' => array(
                'mandatory' => '1',
                'fail_values' => array('')
            ),
            'field_attributes' => array('class' => 'input-lg'),
            'div_attributes' => array('class' => 'col-lg-4'),
        );

        $this->showInEditForm = array(
            'tab_main'=>array(
                'group_name','template_content','template_name'
            )
        );



    }

    

    function load($uid, $forceLocale = null){
        $loadstatus = parent::load($uid, $forceLocale);
        if(!is_array($this->data['template_content']['value'])){
            $this->data['template_content']['value'] = unserialize($this->data['template_content']['value']);
        }
        return $loadstatus;
    }


    /*
     * Adds a user right item to the user rights form
     * @param  $moduleKey string the name of the plugin requesting the key to be set
     * @param  $keyName string the name of the key, such as 'allowNewsDateEditing'
     * @param  $formFieldDefinition array a minfdlow form field definition, such as :
     *    array(
                'value' => '0',
                'dataType' => 'checkbox',
                'valueType' => 'number',
                'processor' => '',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
     *  @param $l10nFile relative path to the user rights translation file (example : '/mf/plugins/mf_countryData/languages/userRights_l10n.php')
     */
    static function defineUserRight($moduleKey, $keyName,$formFieldDefinition, $l10nFile){
//echo "defineUserRight $moduleKey <br />";
        global $mf;

        //load the user rights translation file
        $mf->l10n->loadL10nFile('temp_l10n', $l10nFile);

        //add the form field to the userRights form
        self::$groupTemplate['userRights']['template_data'][$moduleKey.'_'.$keyName] = $formFieldDefinition;
        self::$groupTemplate['userRights']['showInEditForm'] .= ','.$moduleKey.'_'.$keyName;
        $mf->addTemplates(self::$groupTemplate);

        //copy localization keys into the templates localization
        foreach($GLOBALS['l10n_text']['temp_l10n'] as $languageKey => $labels){
            if(!isset($GLOBALS['l10n_text']['templates'][$languageKey]))$GLOBALS['l10n_text']['templates'][$languageKey]=array();

            if(isset($GLOBALS['l10n_text']['temp_l10n'][$languageKey][$keyName]))
                $GLOBALS['l10n_text']['templates'][$languageKey][$moduleKey.'_'.$keyName] = $GLOBALS['l10n_text']['temp_l10n'][$languageKey][$keyName];
        }
//if($moduleKey=='pages')
//print_r($GLOBALS['l10n_text']['templates']);
        //cleanup temp localization
        unset($GLOBALS['l10n_text']['temp_l10n']);

    }

    function setUserRight($moduleKey, $keyName, $value){
        $this->data['template_content']['value'][$moduleKey.'_'.$keyName]['value'] = $keyName;
    }

    function getUserRight($moduleKey, $keyName){
        //print_r($this->data['template_content']['value']);

        if(isset($this->data['template_content']['value'][$moduleKey.'_'.$keyName]['value']))return $this->data['template_content']['value'][$moduleKey.'_'.$keyName]['value'];
        else return '$mfUserGroup->getUserRight Key not found '.$moduleKey.'_'.$keyName;

    }
}
