<?php

    /**
     * Creates a directory at dirPath if it does not already exists.
     * @param $dirPath String relative path of the target directory
     */
    function createDir($dirPath){
        global $colors;
        if(!file_exists($dirPath)){
            mkdir ($dirPath , 0775 , true);
            echo chr(10).$colors->getColoredString("Creating directory ".$dirPath, 'green', null);
        }
        else echo chr(10).$colors->getColoredString("Directory ".$dirPath." already exists, skipping it.", 'green', null);
    }


    /**
     * Writes the fileContent into a file located at filePath. Queries the user for authorization to overwrite the file if it already exists.
     * This function is meant to be used in cli mode.
     * @param $filePath String relative path to the target file
     * @param $fileContent String the content to be written into the file
     */
    function writeFile($filePath, $fileContent){
        global $colors;
        $doWriteFile = true;
        $acceptedValues = array('y','n','yes','no');
        $answer = '';

        if(file_exists($filePath)){
            echo chr(10);
            while (!in_array($answer, $acceptedValues)){
                echo "File ".$filePath." already exists, modify ? [yes/no] : ";
                $answer = trim(fgets(STDIN));
            }
            if($answer=='n' || $answer=='no'){
                $doWriteFile = false;
            }
        }
        if($doWriteFile){
            echo chr(10).$colors->getColoredString("Writing ".$filePath."...", 'green', null);
            file_put_contents ($filePath,$fileContent);
            echo $colors->getColoredString("done.", 'green', null);
        }
    }

    /**
     * Seeks and replaces text between 2 markers ($needle_start, $needle_end) into a target string
     * @param $str  String the target string
     * @param $needle_start String marker for start
     * @param $needle_end String marker for end
     * @param $replacement String the new text to be inserted between the markers
     * @return mixed
     */
    function replace_between($str, $needle_start, $needle_end, $replacement) {
        $pos = strpos($str, $needle_start);
        $start = $pos === false ? 0 : $pos + strlen($needle_start);

        $pos = strpos($str, $needle_end, $start);
        $end = $pos === false ? strlen($str) : $pos;

        return substr_replace($str, $replacement, $start, $end - $start);
    }

    /**
     * Inserts or replaces a section of code into existing code.
     *
     * Here is how the code would look prior the insertion :
     *
     *  This is some previous code
     *  //{targetName}
     *  This is some following code
     *
     * Now here is how it would look after the insertion :
     *
     *  This is some previous code
     *   //[tagName]
     *   the code that should be inserted (section code)
     *  //[/tagName]
     *  //{targetName}
     *  This is some following code
     *
     *  Any new insertion using the same tag name will replace the existing section. Any new insertion using a different tag name will append the section to the existing code.
     *
     * @param $targetName String this is the name of the target marker where the section to be inserted should be positionned if the section tags are not already present. A target named "module_insert_form" looks like //{module_insert_form} in the code.
     * @param $tagName String this is the tag name for a given section of code, which allows to locate it and replace it at a later time if the code of this section needs to be replaced. For example, if the tag name is "item_123", the function will attempt to replace the code between //[item_123] and //[/item_123]
     * @param $sectionText String this is the section text to be inserted, as a replacement of the existing section matching the tagName markers if they exist, or at the emplacement of the target marker if none exists.
     * @param $targetString String This is the string of code to be processed for replacement
     * @param $decay String The characters, tabs or spaces, to put in front of the inserted code so it is correctly indented
     * @return mixed
     */
    function setSection($targetName,$tagName,$sectionCode,$targetCode,$decay){
        global $colors;

        //trim first line if <?php marker is present
        $sectionCode = str_replace("<?php
","",$sectionCode);

        if (strpos($targetCode,'//['.$tagName.']') === false) {
           /* echo "***test=false".chr(10).chr(10);
            echo "***targetName=".$targetName.chr(10).chr(10);
            echo "***tagName=".$tagName.chr(10).chr(10);
            echo "***sectionCode=".$sectionCode.chr(10).chr(10);
            echo "***targetCode=".$targetCode.chr(10).chr(10);*/

            //the tag $tagName can't be found in $targetCode
            //insert section at target location, if it exists
            return str_replace("//{".$targetName."}",
                '//['.$tagName.']'.chr(10).$decay.$sectionCode.chr(10).$decay.'//[/'.$tagName.']'.chr(10).$decay."//{".$targetName."}",
                $targetCode);
        }
        else {
            //echo "***test=true".chr(10);
            //replace existing section
            echo chr(10).$colors->getColoredString('Existing section '.'//[' . $tagName . ']'.' found, replacing it', 'green', null);
            return replace_between($targetCode, '//[' . $tagName . ']', '//[/' . $tagName . ']', chr(10) .$decay.$sectionCode.chr(10).$decay);
        }
    }
/*
    $targetString = "abcdefg 
    //[drop]
    kdjflksjd
    //[/drop] efhg";

    //echo $targetString.chr(10);
    echo setSection("","drop","nouveau texte ",$targetString).chr(10);*/