<?php

require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//we are serving json
header('Content-type: application/json');

//enlarge time execution of the php script for long transactions
set_time_limit(0);

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();

global $mf,$pdo,$l10n,$config;

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec())
{

    $currentUser = $mf->currentUser;

    if(isset($_REQUEST['action']))
    {

        //[json_process_form_FullSyncFilter]
		
        if($_REQUEST['action'] == 'FullSyncFilter')
        {

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm())
            {
                if(isset($mf->currentUser))
                {
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('table_sync','allowEditSync')==1)));

                    if($moduleAccess)
                    {
                        $sqlConditions = '';
                        $dateConditions = '';
                        $deletedCondition = '';

                        $oneCond=false; //check if a condition has already been activated or not

                        //Processing form data
	                    if(isset($form->data['local_table_filter']) && $form->data['local_table_filter']['value'] != '')$sqlConditions .= ' AND local_table=\''.$form->data['local_table_filter']['value'].'\'';

	                    if(isset($form->data['out_rule_filter']) && $form->data['out_rule_filter']['value'] != '')$sqlConditions .= ' AND out_rule=\''.$form->data['out_rule_filter']['value'].'\'';

	                    if(isset($form->data['deleted_filter']) && $form->data['deleted_filter']['value'] == '1')$deletedCondition = ' AND deleted=1';
                        else $deletedCondition = ' AND deleted=0';

	                    if(isset($form->data['destination_filter']) && $form->data['destination_filter']['value'] != '')$sqlConditions .= ' AND destination=\''.$form->data['destination_filter']['value'].'\'';

	                    if(isset($form->data['state_filter']) && $form->data['state_filter']['value'] != '')$sqlConditions .= ' AND state=\''.$form->data['state_filter']['value'].'\'';

	                    if(isset($form->data['result_filter']) && $form->data['result_filter']['value'] != '')$sqlConditions .= ' AND result=\''.$form->data['result_filter']['value'].'\'';

	                    if($form->data['DateDebut_filter']['value'] != '')
		                    $dateConditions .= 'time_last_fired >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], $form->data['DateDebut_filter']['dataType'], $mf->getLocale()));

	                    if($form->data['DateFin_filter']['value'] != ''){
		                    $boolean = '';
		                    if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
		                    $dateConditions .= $boolean.' time_last_fired <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], $form->data['DateFin_filter']['dataType'], $mf->getLocale()));
	                    }
	                    if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';


                        $module = new FullSyncManager();
                        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition);


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

                        $mf->db->closeDB();
	                    $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}';
                    }
                    else
                    {
                        $mf->db->closeDB();
	                    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}';
                    }
                }
                else
                {
                    $mf->db->closeDB();
	                $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','session_expired')).'}';
                }
            }
            else
            {
                $mf->db->closeDB();
	            $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','form-process-error')).'}';
            }
        }
		//[/json_process_form_FullSyncFilter]
		//[json_process_form_OutSyncFilter]

        else if($_REQUEST['action'] == 'OutSyncFilter')
        {

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm())
            {
                if(isset($mf->currentUser))
                {
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('table_sync','allowEditSync')==1)));

                    if($moduleAccess)
                    {
                        $sqlConditions = '';
                        $dateConditions = '';
                        $deletedCondition = '';

                        $oneCond=false; //check if a condition has already been activated or not

                        //Processing form data

                        if(isset($form->data['local_table_filter']) && $form->data['local_table_filter']['value'] != '')$sqlConditions .= ' AND local_table=\''.$form->data['local_table_filter']['value'].'\'';

                        if(isset($form->data['out_rule_filter']) && $form->data['out_rule_filter']['value'] != '')$sqlConditions .= ' AND out_rule=\''.$form->data['out_rule_filter']['value'].'\'';

                        if(isset($form->data['destination_filter']) && $form->data['destination_filter']['value'] != '')$sqlConditions .= ' AND destination=\''.$form->data['destination_filter']['value'].'\'';

                        if(isset($form->data['state_filter']) && $form->data['state_filter']['value'] != '')$sqlConditions .= ' AND state=\''.$form->data['state_filter']['value'].'\'';

                        if(isset($form->data['result_filter']) && $form->data['result_filter']['value'] != '')$sqlConditions .= ' AND result=\''.$form->data['result_filter']['value'].'\'';

	                    if($form->data['DateDebut_filter']['value'] != '')
		                    $dateConditions .= 'creation_date >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], $form->data['DateDebut_filter']['dataType'], $mf->getLocale()));

	                    if($form->data['DateFin_filter']['value'] != ''){
		                    $boolean = '';
		                    if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
		                    $dateConditions .= $boolean.' creation_date <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], $form->data['DateFin_filter']['dataType'], $mf->getLocale()));
	                    }
	                    if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';



                        $module = new OutSyncManager();
                        $html = $module->listRecords($sqlConditions,$dateConditions,'');


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

                        $mf->db->closeDB();
	                    $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}';
                    }
                    else
                    {
                        $mf->db->closeDB();
                        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}';
                    }
                }
                else
                {
                    $mf->db->closeDB();
	                $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','session_expired')).'}';
                }
            }
            else
            {
                $mf->db->closeDB();
	            $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','form-process-error')).'}';
            }
        }
		//[/json_process_form_OutSyncFilter]
		//[json_process_form_InSyncFilter]

        else if($_REQUEST['action'] == 'InSyncFilter')
        {

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm())
            {
                if(isset($mf->currentUser))
                {
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('table_sync','allowEditSync')==1)));

                    if($moduleAccess)
                    {
	                    $sqlConditions = '';
	                    $dateConditions = '';
	                    $deletedCondition = '';

	                    $oneCond=false; //check if a condition has already been activated or not

	                    //Processing form data

	                    if(isset($form->data['remote_table_filter']) && $form->data['remote_table_filter']['value'] != '')$sqlConditions .= ' AND remote_table=\''.$form->data['remote_table_filter']['value'].'\'';

	                    if(isset($form->data['in_rule_filter']) && $form->data['in_rule_filter']['value'] != '')$sqlConditions .= ' AND in_rule=\''.$form->data['in_rule_filter']['value'].'\'';

	                    if(isset($form->data['destination_filter']) && $form->data['destination_filter']['value'] != '')$sqlConditions .= ' AND destination=\''.$form->data['destination_filter']['value'].'\'';

	                    if(isset($form->data['state_filter']) && $form->data['state_filter']['value'] != '')$sqlConditions .= ' AND state=\''.$form->data['state_filter']['value'].'\'';

	                    if(isset($form->data['result_filter']) && $form->data['result_filter']['value'] != '')$sqlConditions .= ' AND result=\''.$form->data['result_filter']['value'].'\'';

	                    if($form->data['DateDebut_filter']['value'] != '')
		                    $dateConditions .= 'creation_date >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], $form->data['DateDebut_filter']['dataType'], $mf->getLocale()));

	                    if($form->data['DateFin_filter']['value'] != ''){
		                    $boolean = '';
		                    if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
		                    $dateConditions .= $boolean.' creation_date <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], $form->data['DateFin_filter']['dataType'], $mf->getLocale()));
	                    }
	                    if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';


	                    $module = new InSyncManager();
                        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition);


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

	                    $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}';
                    }
                    else
                    {
	                    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}';
                    }
                }
                else
                {
	                $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','session_expired')).'}';
                }
            }
            else
            {
	            $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','form-process-error')).'}';
            }
        }
		//[/json_process_form_InSyncFilter]
        //{add_json_action}


        else if($_REQUEST['action'] == 'SqlOutSyncFilter')
        {

	        $formMgr = $mf->formsManager;

	        //process the form
	        if($form = $formMgr->processForm())
	        {
		        if(isset($mf->currentUser))
		        {
			        $currentUser = $mf->currentUser;
			        $userGroup = $currentUser->getUserGroup();
			        $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('table_sync','allowEditSync')==1)));

			        if($moduleAccess)
			        {
				        $sqlConditions = '';
				        $dateConditions = '';
				        $deletedCondition = '';

				        $oneCond=false; //check if a condition has already been activated or not

				        //Processing form data

				        if(isset($form->data['local_table_filter']) && $form->data['local_table_filter']['value'] != '')$sqlConditions .= ' AND local_table=\''.$form->data['local_table_filter']['value'].'\'';

				        if(isset($form->data['sql_rule_filter']) && $form->data['sql_rule_filter']['value'] != '')$sqlConditions .= ' AND sql_rule=\''.$form->data['sql_rule_filter']['value'].'\'';

				        if(isset($form->data['destination_filter']) && $form->data['destination_filter']['value'] != '')$sqlConditions .= ' AND destination=\''.$form->data['destination_filter']['value'].'\'';

				        if(isset($form->data['state_filter']) && $form->data['state_filter']['value'] != '')$sqlConditions .= ' AND state=\''.$form->data['state_filter']['value'].'\'';

				        if(isset($form->data['result_filter']) && $form->data['result_filter']['value'] != '')$sqlConditions .= ' AND result=\''.$form->data['result_filter']['value'].'\'';

				        if($form->data['DateDebut_filter']['value'] != '')
					        $dateConditions .= 'creation_date >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], $form->data['DateDebut_filter']['dataType'], $mf->getLocale()));

				        if($form->data['DateFin_filter']['value'] != ''){
					        $boolean = '';
					        if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
					        $dateConditions .= $boolean.' creation_date <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], $form->data['DateFin_filter']['dataType'], $mf->getLocale()));
				        }
				        if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';



				        $module = new SqlOutSyncManager();
				        $html = $module->listRecords($sqlConditions,$dateConditions,'');


				        $html = str_replace("{subdir}",SUB_DIR,$html);
				        $html = str_replace("{module}",$_REQUEST['module'],$html);

				        $mf->db->closeDB();
				        $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}';
			        }
			        else
			        {
				        $mf->db->closeDB();
				        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}';
			        }
		        }
		        else
		        {
			        $mf->db->closeDB();
			        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','session_expired')).'}';
		        }
	        }
	        else
	        {
		        $mf->db->closeDB();
		        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','form-process-error')).'}';
	        }
        }



        else if($_REQUEST['action'] == 'SqlInSyncFilter')
        {

	        $formMgr = $mf->formsManager;

	        //process the form
	        if($form = $formMgr->processForm())
	        {
		        if(isset($mf->currentUser))
		        {
			        $currentUser = $mf->currentUser;
			        $userGroup = $currentUser->getUserGroup();
			        $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('table_sync','allowEditSync')==1)));

			        if($moduleAccess)
			        {
				        $sqlConditions = '';
				        $dateConditions = '';
				        $deletedCondition = '';

				        $oneCond=false; //check if a condition has already been activated or not

				        //Processing form data

				        if(isset($form->data['destination_filter']) && $form->data['destination_filter']['value'] != '')$sqlConditions .= ' AND destination=\''.$form->data['destination_filter']['value'].'\'';

				        if(isset($form->data['state_filter']) && $form->data['state_filter']['value'] != '')$sqlConditions .= ' AND state=\''.$form->data['state_filter']['value'].'\'';

				        if(isset($form->data['result_filter']) && $form->data['result_filter']['value'] != '')$sqlConditions .= ' AND result=\''.$form->data['result_filter']['value'].'\'';

				        if($form->data['DateDebut_filter']['value'] != '')
					        $dateConditions .= 'creation_date >= '.$pdo->quote(dateToSQL($form->data['DateDebut_filter']['value'], $form->data['DateDebut_filter']['dataType'], $mf->getLocale()));

				        if($form->data['DateFin_filter']['value'] != ''){
					        $boolean = '';
					        if($form->data['DateDebut_filter']['value'] != '')$boolean = ' AND';
					        $dateConditions .= $boolean.' creation_date <= '.$pdo->quote(dateToSQL($form->data['DateFin_filter']['value'], $form->data['DateFin_filter']['dataType'], $mf->getLocale()));
				        }
				        if($dateConditions != '')$dateConditions = ' AND ('.$dateConditions.')';


				        $module = new SqlInSyncManager();
				        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition);


				        $html = str_replace("{subdir}",SUB_DIR,$html);
				        $html = str_replace("{module}",$_REQUEST['module'],$html);

				        $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}';
			        }
			        else
			        {
				        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}';
			        }
		        }
		        else
		        {
			        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','session_expired')).'}';
		        }
	        }
	        else
	        {
		        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','form-process-error')).'}';
	        }
        }



        else if($_REQUEST['action'] == 'replayOut') {
	        if(isset($mf->currentUser)) {
		        $currentUser  = $mf->currentUser;
		        $userGroup    = $currentUser->getUserGroup();
		        $moduleAccess = ( $currentUser->isAdmin() || ( class_exists( 'mfUserGroup' ) && ( $userGroup && $userGroup->getUserRight( 'table_sync', 'allowEditSync' ) == 1 ) ) );

		        if ( $moduleAccess ) {
			        $uid = intval($_REQUEST['uid']);

			        $out = new OutSync();
			        $out->load( $uid );

			        $syncConfig = $config['plugins']['table_sync'];
			        $ruleName = $out->data['out_rule']['value'];

			        if ( isset( $syncConfig['out_rules'][ $ruleName ] ) ) {
				        $rule = $syncConfig['out_rules'][ $ruleName ];
				        //rule found, send object
				        $out->data['out_rule']['value'] = $ruleName;
				        $out->addToLocalLog($l10n->getLabel('SyncManager','rule_out_found').chr(10));
			        }
			        else {
				        //error, no matching send rule found, failure to send object
				        $rule = false;
				        $out->setField('result','Failure');
				        $out->addToLocalLog(str_replace('{nom}', $ruleName,$l10n->getLabel('SyncManager','rule_not_found_name')).chr(10));
			        }

			        if($rule){
				        if ( isset( $rule['sendFunc'] ) ) {
					        //apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
					        $sendFunc = $rule['sendFunc'];
				        } else {
					        //or use default JSON based protocol function
					        $out->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sendFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
					        $sendFunc = "SyncManager::jsonOutRequest";
				        }

				        $result = $sendFunc( $out );
				        //SyncManager::jsonOutRequest( $out );
			        }


			        $state = OutSync::getState('',$out->data['state']['value'],'',array('uid'=>$uid));
			        $result = OutSync::getResult('',$out->data['result']['value'],'',array('uid'=>$uid));

			        if($out->data['result']['value'] == 'Success')
				        $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : "", "out_result" : ' . json_encode( $result ) . ', "out_state" : ' . json_encode( $state ) . '}';
			        else
				        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "", "out_result" : ' . json_encode( $result ) . ', "out_state" : ' . json_encode( $state ) . '}';

		        } else {
			        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( $l10n->getLabel( 'backend', 'module_no_access' ) ) . '}';
		        }
	        }
        }

	    else if($_REQUEST['action'] == 'replayFull') {
		    if(isset($mf->currentUser)) {
			    $currentUser  = $mf->currentUser;
			    $userGroup    = $currentUser->getUserGroup();
			    $moduleAccess = ( $currentUser->isAdmin() || ( class_exists( 'mfUserGroup' ) && ( $userGroup && $userGroup->getUserRight( 'table_sync', 'allowEditSync' ) == 1 ) ) );

			    if ( $moduleAccess ) {

				    $uid = intval($_REQUEST['uid']);

				    $fullSync = new FullSync();
				    $fullSync->load( $uid );

				    $fullSyncConfig = $config['plugins']['table_sync'];
				    $ruleName = $fullSync->data['out_rule']['value'];

				    if ( isset( $fullSyncConfig['out_rules'][ $ruleName ] ) ) {

					    $rule = $fullSyncConfig['out_rules'][ $ruleName ];
					    //rule found, send object
					    $fullSync->data['out_rule']['value'] = $ruleName;
					    $fullSync->addToLocalLog($l10n->getLabel('SyncManager','rule_out_found').chr(10));
				    }
				    else {
					    //error, no matching send rule found, failure to send object
					    $rule = false;
					    $fullSync->setField('result','Failure');
					    $fullSync->addToLocalLog(str_replace('{nom}', $ruleName,$l10n->getLabel('SyncManager','rule_not_found_name')).chr(10));
				    }

				    if($rule){


						//rule has been found, schedule for execution
					    if ( isset( $rule['sendFunc'] ) ) {
						    //apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example

						    $fullSync->data['exec_asap']['value'] = 1;
						    $fullSync->data['state']['value'] = 'Scheduled';
						    $fullSync->data['result']['value'] = '';
						    $status = $fullSync->store();
						    $logger->debug("tableSync-json-backend 'replayFull' : fullsync object created OK");

					    }
					    //rule not found, error
					    else {
						    //or use default JSON based protocol function
						    $fullSync->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sendFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
						    $sendFunc = "SyncManager::jsonOutRequest";
					    }


				    }

				    $state = FullSync::getState('',$fullSync->data['state']['value'],'',array('uid'=>$uid));
				    $result = FullSync::getResult('',$fullSync->data['result']['value'],'',array('uid'=>$uid));

				    if($fullSync->data['result']['value'] == 'Success')
					    $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : "", "out_result" : ' . json_encode( $result ) . ', "out_state" : ' . json_encode( $state ) . '}';
				    else
					    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "", "out_result" : ' . json_encode( $result ) . ', "out_state" : ' . json_encode( $state ) . '}';

			    } else {
				    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( $l10n->getLabel( 'backend', 'module_no_access' ) ) . '}';
			    }
		    }
	    }

	    else if($_REQUEST['action'] == 'resetState') {
		    if(isset($mf->currentUser)) {
			    $currentUser  = $mf->currentUser;
			    $userGroup    = $currentUser->getUserGroup();
			    $recordClass = $_REQUEST['record_class'];

			    $moduleAccess = ( $currentUser->isAdmin() || ( class_exists( 'mfUserGroup' ) && ( $userGroup && $userGroup->getUserRight( 'table_sync', 'allowEditSync' ) == 1 ) ) );

			    if ( $moduleAccess ) {

				    $classCheck = array('OutSync','InSync','FullSync','SqlOutSync','SqlInSync');

				    if(in_array($recordClass,$classCheck)) {

					    $uid = intval( $_REQUEST['uid'] );

					    $sync = new $recordClass();
					    $sync->load( $uid );

					    if((isset($sync->data['exec_asap']['value'])))$sync->data['exec_asap']['value'] = 0;
					    $sync->data['state']['value'] = 'Inactive';

					    $sync->store();

					    $state  = $recordClass::getState( '', $sync->data['state']['value'], '', array( 'uid' => $uid ) );
					    $result = $recordClass::getResult( '', $sync->data['result']['value'], '', array( 'uid' => $uid ) );

					    if ( $sync->data['result']['value'] == 'Success' ) {
						    $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : "", "result" : ' . json_encode( $result ) . ', "state" : ' . json_encode( $state ) . '}';
					    } else {
						    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "", "result" : ' . json_encode( $result ) . ', "state" : ' . json_encode( $state ) . '}';
					    }
				    }
				    else {
					    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "Attempt to use an unallowed class", "result" : "", "state" : ""}';
				    }

			    } else {
				    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( $l10n->getLabel( 'backend', 'module_no_access' ) ) . ', "result" : "", "state" : ""}';
			    }
		    }
	    }

	    else if($_REQUEST['action'] == 'replaySqlOut') {
		    if(isset($mf->currentUser)) {
			    $currentUser  = $mf->currentUser;
			    $userGroup    = $currentUser->getUserGroup();
			    $moduleAccess = ( $currentUser->isAdmin() || ( class_exists( 'mfUserGroup' ) && ( $userGroup && $userGroup->getUserRight( 'table_sync', 'allowEditSync' ) == 1 ) ) );

			    if ( $moduleAccess ) {

				    $uid = intval($_REQUEST['uid']);

				    $SqlOutSync = new SqlOutSync();
				    $SqlOutSync->load( $uid );

				    $syncConfig = $config['plugins']['table_sync'];
				    $ruleName = $SqlOutSync->data['sql_rule']['value'];

				    if ( isset( $syncConfig['sql_out_rules'][ $ruleName ] ) ) {

					    $rule = $syncConfig['sql_out_rules'][ $ruleName ];
					    //rule found, send object
					    $SqlOutSync->data['sql_rule']['value'] = $ruleName;
					    $SqlOutSync->addToLocalLog($l10n->getLabel('SyncManager','rule_sql_found').chr(10));
				    }
				    else {
					    //error, no matching send rule found, failure to send object
					    $rule = false;
					    $SqlOutSync->setField('result','Failure');
					    $SqlOutSync->addToLocalLog(str_replace('{nom}', $ruleName,$l10n->getLabel('SyncManager','rule_not_found_name')).chr(10));
				    }

				    if($rule){
					    if ( isset( $rule['sendFunc'] ) ) {
						    //apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
						    $sendFunc = $rule['sendFunc'];
					    } else {
						    //or use default JSON based protocol function
						    $SqlOutSync->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sendFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
						    $sendFunc = "SyncManager::jsonSqlRequest";
					    }

					    $result = $sendFunc( $SqlOutSync );
					    //SyncManager::jsonSqlRequest( $SqlOutSync );
				    }



				    $state = SqlOutSync::getState('',$SqlOutSync->data['state']['value'],'',array( 'uid' =>$uid));
				    $result = SqlOutSync::getResult('',$SqlOutSync->data['result']['value'],'',array( 'uid' =>$uid));

				    if($SqlOutSync->data['result']['value'] == 'Success')
					    $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : "", "sql_result" : ' . json_encode( $result ) . ', "sql_state" : ' . json_encode( $state ) . '}';
				    else
					    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "", "sql_result" : ' . json_encode( $result ) . ', "sql_state" : ' . json_encode( $state ) . '}';

			    } else {
				    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( $l10n->getLabel( 'backend', 'module_no_access' ) ) . '}';
			    }
		    }
	    }

	    else if($_REQUEST['action'] == 'replayIn') {
		    if(isset($mf->currentUser)) {
			    $currentUser  = $mf->currentUser;
			    $userGroup    = $currentUser->getUserGroup();
			    $moduleAccess = ( $currentUser->isAdmin() || ( class_exists( 'mfUserGroup' ) && ( $userGroup && $userGroup->getUserRight( 'table_sync', 'allowEditSync' ) == 1 ) ) );

			    if ( $moduleAccess ) {

				    $uid = intval($_REQUEST['uid']);

				    $in = new InSync();
				    $in->load( $uid );

				    $syncConfig = $config['plugins']['table_sync'];
				    $ruleName = $in->data['in_rule']['value'];
				    $rule = $syncConfig['in_rules'][$ruleName];


				    if ( isset( $syncConfig['in_rules'][ $ruleName ] ) ) {

					    $rule = $syncConfig['in_rules'][ $ruleName ];
					    //rule found, send object
					    $in->data['in_rule']['value'] = $ruleName;
					    $in->addToLocalLog($l10n->getLabel('SyncManager','rule_in_found').chr(10));
				    }
				    else {
					    //error, no matching send rule found, failure to send object
					    $rule = false;
					    $in->setField('result','Failure');
					    $in->addToLocalLog(str_replace('{nom}', $ruleName,$l10n->getLabel('SyncManager','rule_not_found_name')).chr(10));
				    }

					if($rule) {
						if ( isset( $rule['recvFunc'] ) ) {
							//apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
							$recvFunc = $rule['recvFunc'];
						} else {
							//or use default JSON based protocol function
							$in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'recvFunc_not_set' ) .' '. $ruleName . chr( 10 ) );

							$recvFunc = "SyncManager::jsonInRequest";
						}

						//reset log
						$in->data['local_log']['value'] = '';

						//process incoming request once again
						$recvFunc( $in );

						$state  = InSync::getState( '', $in->data['state']['value'], '', array( 'uid' => $uid ) );
						$result = InSync::getResult( '', $in->data['result']['value'], '', array( 'uid' => $uid ) );

						if ( $in->data['result']['value'] == 'Success' ) {
							$response = '{"jsonrpc" : "2.0", "result" : "success", "message" : "", "in_result" : ' . json_encode( $result ) . ', "in_state" : ' . json_encode( $state ) . '}';
						} else {
							$response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "", "in_result" : ' . json_encode( $result ) . ', "in_state" : ' . json_encode( $state ) . '}';
						}
					}
					else {
						$response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( str_replace('{nom}', $ruleName,$l10n->getLabel('SyncManager','rule_not_found_name')) ) . '}';
					}

			    } else {
				    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( $l10n->getLabel( 'backend', 'module_no_access' ) ) . '}';
			    }
		    }
	    }

	    else if($_REQUEST['action'] == 'replaySqlIn') {
		    if(isset($mf->currentUser)) {
			    $currentUser  = $mf->currentUser;
			    $userGroup    = $currentUser->getUserGroup();
			    $moduleAccess = ( $currentUser->isAdmin() || ( class_exists( 'mfUserGroup' ) && ( $userGroup && $userGroup->getUserRight( 'table_sync', 'allowEditSync' ) == 1 ) ) );

			    if ( $moduleAccess ) {

				    $uid = intval($_REQUEST['uid']);

				    $sqlInSync = new SqlInSync();
				    $sqlInSync->load( $uid );

				    $sqlInSync->setField('state','Receiving');

				    $syncConfig = $config['plugins']['table_sync'];
/*

				    $ruleName = $sqlInSync->data['in_rule']['value'];
				    $rule = $syncConfig['in_rules'][$ruleName];

				    if ( isset( $syncConfig['in_rules'][ $ruleName ] ) ) {

					    $rule = $syncConfig['in_rules'][ $ruleName ];
					    //rule found, send object
					    $sqlInSync->data['in_rule']['value'] = $ruleName;
					    $sqlInSync->addToLocalLog($l10n->getLabel('SyncManager','rule_in_found').chr(10));
				    }
				    else {
					    //error, no matching send rule found, failure to send object
					    $rule = false;
					    v
				    }
*/
					$sql = $sqlInSync->data['sql_request']['value'];

				    if($sql != '') {
					    /*if ( isset( $rule['recvFunc'] ) ) {
						    //apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
						    $recvFunc = $rule['recvFunc'];
					    } else {
						    //or use default JSON based protocol function
						    $sqlInSync->addToLocalLog( $l10n->getLabel( 'SyncManager', 'recvFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
						    $recvFunc = "SyncManager::jsonInRequest";
					    }

					    //process incoming request once again
					    $recvFunc( $sqlInSync );*/

					    $rows = serialize(array());

					    try{

						    $sqlInSync->setField('sql_request', $sql);

						    $stmt = $pdo->query($sql);

						    if($syncConfig['logSqlResults']==true){
						    	$rows = serialize($stmt->fetchAll(PDO::FETCH_ASSOC));
							    $sqlInSync->setField('sql_result', $rows);
						    }

						    $sqlInSync->setField('result','Success');
					    }
					    catch(PDOException  $e)
					    {
						    $sqlInSync->setField('result','Failure');
						    $sqlInSync->addToLocalLog($l10n->getLabel('SyncManager','sql_exception').chr(10).$e->getMessage().chr(10).$l10n->getLabel('main','number').$e->getCode().chr(10));

					    }



					    $sqlInSync->setField('state','Inactive');

					    $state  = SqlInSync::getState( '', $sqlInSync->data['state']['value'], '', array( 'uid' => $uid ) );
					    $result = SqlInSync::getResult( '', $sqlInSync->data['result']['value'], '', array( 'uid' => $uid ) );

					    if ( $sqlInSync->data['result']['value'] == 'Success' ) {
						    $response = '{"jsonrpc" : "2.0", "result" : "success", "message" : "", "in_result" : ' . json_encode( $result ) . ', "in_state" : ' . json_encode( $state ). ', "rows" : ' . json_encode( $rows ) . '}';
					    } else {
						    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "", "in_result" : ' . json_encode( $result ) . ', "in_state" : ' . json_encode( $state ) . '}';
					    }
				    }
				    else {
					    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( $l10n->getLabel( 'SyncManager', 'empty_sql' ) ) . '}';
				    }

			    } else {
				    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" :  ' . json_encode( $l10n->getLabel( 'backend', 'module_no_access' ) ) . '}';
			    }
		    }
		    else
		    {
			    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "\$mf->currentuser is not set"}';
		    }
	    }
	    else
	    {
		    $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "Specified action not supported"}';
	    }
    }
    else
    {
        $response = '{"jsonrpc" : "2.0", "result" : "error", "message" : "No action specified."}';
    }
}
else
{
    $response = '{"jsonrpc" : "2.0", "result" : "error", "MindFlow : Request was not accepted. The \'sec\' and \'fields\' values must be present in the request and properly set."}';
}

echo $response;
$mf->db->closeDB();




