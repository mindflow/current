<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

$execution_start = time();

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

require_once DOC_ROOT.SUB_DIR.'/mf/core/forms/installToolLoginForm.php';

//we are serving HTML
header('Content-type: text/html');

//force logout from backend as it may raise database errors.
//session_start();
//unset($_SESSION['current_be_user']);
//print_r($_SESSION);

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();

global $mf,$l10n,$config,$pluginManager;

$html = array();
if(!isset($_SESSION['installToolLogin']))$_SESSION['installToolLogin']=false;
$errorMessage="";

//echo "SESSION=";
//print_r($_SESSION);

$mf->l10n->loadL10nFile('installTool', '/mf/languages/installTool_l10n.php');

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
if(checkSec()){

     if(isset($_REQUEST['action'])){

         if($_REQUEST['action'] == 'installToolLogin'){
             $formMgr = $mf->formsManager;
             $form = $formMgr->processForm();

             //password check
             if($config['installTool_password']== $form->data['password']['value']){
                 //login success !
                 $_SESSION['installToolLogin']=true;
             }
             else $errorMessage = $l10n->getLabel('installTool','bad_password');
         }

    }
}

//user is connected
if($_SESSION['installToolLogin']==true){

    $mf->mode = "backend";

    if (isset($_SESSION['installTool_LAST_ACTIVITY']) && (time() - $_SESSION['installTool_LAST_ACTIVITY'] > 18000)) {
        // last request was more than 30 minutes ago
        $_SESSION['installToolLogin']=false; //logout current user
        $errorMessage = $l10n->getLabel('installTool','session_expired');
    }
    $_SESSION['installTool_LAST_ACTIVITY'] = time(); // update last activity time stamp

    //if the session has not expired, show the install tool menu
    if($_SESSION['installToolLogin']==true){
        $html[] = '<div id="installToolMenu">
                <div class="row">
                    <h1>'.$l10n->getLabel('installTool','installToolTitle').'</h1><br />
                    ';

        if(!isset($_REQUEST['action']) || $_REQUEST['action']=='' || $_REQUEST['action']=='installToolLogin'){

            $html[] = '
            <div class="bg-warning" style="margin:0 0 30px 30px;padding:20px;width:800px;display:block;">'.$l10n->getLabel('installTool','db_warning').'</div>
            <div style="margin:0 0 30px 30px;width:800px;display:block;">

                <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{subdir}/mf/install/index.php?action=createTables" class="list-group-item">
                                <h4>'.$l10n->getLabel('installTool','create_tables').'</h4>
                            </a>
                        </div>
                        <div class="panel-body">
                            <p class="list-group-item-text">'.$l10n->getLabel('installTool','create_tables_desc').'</p>
                        </div>
                </div>
                <br /><br/>
                
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{subdir}/mf/install/index.php?action=upgrade" class="list-group-item">
                                <h4>'.$l10n->getLabel('installTool','upgrade_tool').'</h4>
                            </a>
                        </div>
                        <div class="panel-body">
                            <p class="list-group-item-text">'.$l10n->getLabel('installTool','upgrade_tool_desc').'</p>
                        </div>
                </div>
                <br /><br/>
                
            </div>
            ';


        }
        else if(isset($_REQUEST['action'])) {
            $html[] = '<div id="installToolContent">';

            require_once 'tables.php';
            require_once 'upgrade.php';
            $html[] = "<br/><br/>".echoBackButton();

            $html[] = '</div><!-- / id="installToolContent" -->';
        }

        $html[] = '</div></div>';
    }
}

//user is not connected
if($_SESSION['installToolLogin']==false){
    //show login form
    $installToolLoginForm = new installToolLoginForm();

    $form = $mf->formsManager;

    $html[] = '<div id="installToolLogin">
                <div class="row">
                    <div style="margin:0 auto 0 auto;max-width:340px;display:block;">';
    if($errorMessage!='')$html[] = '<p style="color:red;margin-left:10px;">'.$errorMessage.'</p>';


    $html[] = $form->editForm(SUB_DIR.'/mf/install/index.php','installToolLogin','update',              $installToolLoginForm,'','',true, $l10n->getLabel('installTool','submit'),'btn btn-primary col-lg-offset-5',false);

    $html[] = '</div></div></div>';
}

function echoBackButton(){
    global $l10n;
    return '<div style="margin:10px 0 30px 10px;"><p><button class="btn btn-primary" type="submit" onclick="window.history.go(-1)">'.$l10n->getLabel('installTool','back').'</button></p></div>';
}

$installBody = implode(chr(10),$html);


//Build the HTML
$html = array();

//process page header
$mf->header = $mindFlowBackend->getStandardHeader(' id="installer"');

//processing UI messages
$customMsgs=array();
foreach($mf->customMessages as $message) $customMsgs[] = $message.'</div>';
$mf->header = str_replace("{custom-messages}",implode(chr(10),$customMsgs),$mf->header);

$warnings=array();
foreach($mf->warningMessages as $message) $warnings[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
$mf->header = str_replace("{warning-messages}",implode(chr(10),$warnings),$mf->header);

$errors=array();
foreach($mf->errorMessages as $message) $errors[] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
$mf->header = str_replace("{error-messages}",implode(chr(10),$errors),$mf->header);

$success=array();
foreach($mf->successMessages as $message) $success[] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
$mf->header = str_replace("{success-messages}",implode(chr(10),$success),$mf->header);

$infos=array();
foreach($mf->infoMessages as $message) $infos[] = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
$mf->header = str_replace("{info-messages}",implode(chr(10),$infos),$mf->header);

$html[]= $mf->header;

$mf->body = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/backend/templates/install/index.html');
$mf->body = str_replace("{installer}",$installBody,$mf->body);
$mf->body = str_replace("{subdir}",SUB_DIR,$mf->body);
$html[]= $mf->body;

//process page footer
$mf->footer = $mindFlowBackend->getStandardFooter();
$html[]= $mf->footer;


//output HTML
echo implode(chr(10),$html);

$mf->db->closeDB();

