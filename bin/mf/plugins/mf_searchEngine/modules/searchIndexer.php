<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/mf_searchEngine/records/mfSearchIndex.php';


class searchIndexer implements module
{
    private $moduleKey = 'searchIndexer';
    private $moduleType = 'backend';
    private $action='index-searchIndexer';

    private $indexedClasses = array();

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';
    var $numDemandesModeration = '';


    function prepareData(){
        global $mf,$l10n,$config;
        $this->pageOutput = '';

        $searchClasses = explode(',',$config['plugins']['mf_searchEngine']['indexClasses']);
        foreach($searchClasses as $searchClassName){
            $this->registerIndex(trim($searchClassName));
        }

        if($mf->mode == 'backend'){


            //check access rights
            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('mf_searchEngine','allowEditSearchIndex')==1)));

            if($this->moduleAccess){

                //add the plugin to the backend menus
                if(!$mf->pluginManager->backendMenuExists('prod'))$mf->pluginManager->addBackendMenu('administration', LOOSE_PRIORITY);
                $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=searchIndexer"><span class="glyphicon glyphicon-menu glyphicon-search" aria-hidden="true"></span>'.$l10n->getLabel('searchIndexer','menu_title').'</a>','administration', LOW_PRIORITY);


                if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                    //breadcrumb
                    $this->moduleName = "<span class=\"glyphicon glyphicon-menu glyphicon-search\" aria-hidden=\"true\"></span>".$l10n->getLabel('searchIndexer','menu_title');
                    $this->subModuleName = "";

                    $this->localMenu ='<li rel="manageSearchIndex" class="active"><a href="{subdir}/mf/index.php?module=searchIndexer&submodule=manageSearchIndex">'.$l10n->getLabel('searchIndexer','manage_search_index').'</a></li>';

                    //default value
                    if(!isset($_REQUEST['submodule']))$_REQUEST['submodule']='manageSearchIndex';

                    if(isset($_REQUEST['submodule'])){

                        if($_REQUEST['submodule']=='manageSearchIndex'){

                            $this->manageSearchIndex = new manageSearchIndex();
                            $this->manageSearchIndex->prepareData();
                        }
                    }
                }

            } else {
                $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
                $this->localMenu = '';
            }
        }

    }

    function render(&$mainTemplate){
        global $mf;
        if($mf->mode == 'backend'){

            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/mf_searchEngine/ressources/templates/searchIndex.html');
                $mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);
                $mainTemplate = str_replace("{section}",$this->section,$mainTemplate);
                $mainTemplate = str_replace("{module-name}",$this->moduleName,$mainTemplate);
                $mainTemplate = str_replace("{module-id}",$this->moduleKey,$mainTemplate);
                $mainTemplate = str_replace("{local-menu}",$this->localMenu,$mainTemplate);


                if(isset($_REQUEST['submodule'])){
                    if($_REQUEST['submodule']=='manageSearchIndex'){
                        $this->manageSearchIndex->render($mainTemplate);
                    }

                }

            }
        }
    }



    function getType(){
        return $this->moduleType;
    }



    /**
     * adds or updates an item to the search index. This function should be called in the postStore() and postRestore() functions of the related dbRecord instance
     * @param $record the dbRecord subclass to index
     * @param $title the title of the ressource wich will appear in the search results
     * @param $digest a prepared plain text digest of the text data to be indexed. HTML tags must be stripped out !
     * @param $tags semantics tags associated with the record entry, separated by comas or spaces
     * @param $ressourceUrl relative path including url parameters from the website root used to access the page holding the record data
     */
    static function indexItem($record,$title, $digest,$tags,$ressourceUrl){
        global $pdo;

        //verifying the record exists in the database
        $sql = 'SELECT * FROM mf_search_index WHERE class_name = '.$pdo->quote(get_class($record)).' AND object_uid = '.$pdo->quote($record->data['uid']['value']);
        $stmt = $pdo->query($sql);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);


        if($stmt->rowCount() > 0){
            //update
            $index = new mfSearchIndex();

            $index->load($row['uid']);

            $index->data['hidden']['value'] = $record->data['hidden']['value'];
            $index->data['start_time']['value'] = $record->data['start_time']['value'];
            $index->data['end_time']['value'] = $record->data['end_time']['value'];

            $index->data['title']['value'] = $title;
            $index->data['digest']['value'] = $digest;
            $index->data['tags']['value'] = $tags;
            $index->data['ressource_url']['value'] = $ressourceUrl;
            $index -> store();

        }
        else{
            //insert
            $index = new mfSearchIndex();
            $index->data['object_uid']['value'] = $record->data['uid']['value'];
            $index->data['class_name']['value'] = get_class($record);

            $index->data['hidden']['value'] = $record->data['hidden']['value'];
            $index->data['start_time']['value'] = $record->data['start_time']['value'];
            $index->data['end_time']['value'] = $record->data['end_time']['value'];

            $index->data['title']['value'] = $title;
            $index->data['digest']['value'] = $digest;
            $index->data['tags']['value'] = $tags;
            $index->data['ressource_url']['value'] = $ressourceUrl;
            $index -> store();
        }

    }

    /**
     * deleted an item from the search index. This function should be called in the postDelete() function of the related dbRecord instance
     * @param $record the dbRecord subclass to remove from the index
     */
    static function unindexItem($record){
        global $pdo;

        //$index = new mfSearchIndex();
        //verifying the record exists in the database
        $sql = "DELETE FROM mf_search_index WHERE class_name = :class_name AND object_uid = :object_uid";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array(
                ':class_name' => get_class($record),
                ':object_uid' => $record->data['uid']['value'],
            ));
            return 1;
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }
    }

    /**
     * adds a dbRecord Class to the indexer. This will make its indexAll() function called each time the index is rebuilt.
     * call this function in the constructor of your class
     * @param $recordClass the dbRecord subclass to index
     */
    function registerIndex($recordClassName){
        if(!in_array($recordClassName,$this->indexedClasses)){
            $this->indexedClasses[] = $recordClassName;
        }
    }


    function rebuildIndex(){
        global $pdo;

        //empty the search index
        $sql = "TRUNCATE TABLE mf_search_index";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array());

        foreach($this->indexedClasses as $className){
            $item = new $className();
            $item->indexAll();
        }
    }


    /**
     * Returns the given text with tags to highlight the specified words.
     * @param string $text
     * @param array $words
     * @return string
     *
     */
    static function highlightWords($text, $words)
{
    /*** loop of the array of words ***/
    foreach ($words as $word)
    {
        /*** quote the text for regex ***/
        $word = preg_quote($word);
        /*** highlight the words ***/
        $text = preg_replace("/\b($word)\b/i", '<span class="highlight_word">\1</span>', $text);
    }
    /*** return the text ***/
    return $text;
}
}

