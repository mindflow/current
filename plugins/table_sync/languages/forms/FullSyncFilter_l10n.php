<?php

$l10nText = array(

    'fr' => array(
        'keyword_filter' => "Keyword",
        'deleted_filter' => "Display deleted items",
        'local_table_filter' => "Table locale",
        'out_rule_filter' => "Règle sortante",
        'destination_filter' => "Destination",
        'state_filter' => "Etat",
        'result_filter' => "Résultat",
        'DateDebut_filter' => 'Date min',
        'DateFin_filter' => 'Date max',


    ),
);

?>