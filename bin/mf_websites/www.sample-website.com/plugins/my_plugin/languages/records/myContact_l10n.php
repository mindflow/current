<?php

/*                                                                        *
 * This script belongs to the MindFlow framework.                         *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * Author : Mind2Machine                                                  *
 * The MindFlow project                                                   *
 *                                                                        */


$l10nText = array(


    'fr' => array(
        'civility' => 'Civilité',
        'civility_fail' => 'Vous devez indiquer une civilité',
        'mrs' => 'Mme',
        'mr' => 'Mr',
        'miss' => 'Mlle',
        'name' => 'Prénom et Nom',
        'name_fail' => 'Vous devez indiquer un prénom et un nom',
        'password' => 'Mot de passe',
        'password_fail' => 'Le mot de passe spécifié est vide',
        'password_confirmation' => 'Confirmez',
        'password_confirmation_fail' => 'Le mot de passe et sa confirmation ne correspondent pas',
        'email' => 'Email',
        'email_fail' => 'Adresse email manquante, invalide, ou déjà utilisée',

        'tab_main' => 'Infos générales',

        /*mandatory records*/
        'record_name' => 'Contact',
        'new_record' => 'Nouveau contact',

    ),

    'en' => array(
        'civility' => 'Civility',
        'civility_fail' => 'You have to supply a civility',
        'mrs' => 'Mrs',
        'mr' => 'Mr',
        'miss' => 'Miss',
        'name' => 'First name and last name',
        'name_fail' => 'You have to supply your first name and last name',
        'password' => 'Password',
        'password_fail' => 'The given password is empty',
        'password_confirmation' => 'Confirm',
        'password_confirmation_fail' => 'The password and its confirmation do not match',
        'email' => 'Email',
        'email_fail' => 'Email missing, not valid or already used',

        'tab_main' => 'Global info',

        /*mandatory records*/
        'record_name' => 'Contact',
        'new_record' => 'New contact',
    ),

);

?>