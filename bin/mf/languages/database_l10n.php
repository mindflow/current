<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'connexion_error' => 'Error : the database connection information is invalid. Please check your settings.',
        'host_error' => 'Error : the db host address is unreachable. The database is not configured or the database server is unavailable.',
        'connect_db' => 'Connecting to DB {db_name}',
        'go_install_tool' => "The database does not seem to be configured. Check your configuration.",
    ),

    'fr' => array(
        'connexion_error' => 'Erreur : Les informations de connexion à la base de données sont invalides. Veuillez les vérifier.',
        'host_error' => 'Erreur : l\'adresse de l\'hôte de base de données est injoignable. La base de données n\'est pas configurée ou le serveur est indisponible.',
        'connect_db' => 'Connexion à la base de données {db_name}',
        'go_install_tool' => "La base de données ne semble pas configurée. Vérifiez votre configuration.",
    ),

    'de' => array(
	    'connexion_error' => 'Error : the database connection information is invalid. Please check your settings.',
	    'host_error' => 'Error : the db host address is unreachable. The database is not configured or the database server is unavailable.',
	    'connect_db' => 'Connecting to DB {db_name}',
	    'go_install_tool' => "The database does not seem to be configured. Check your configuration.",
    ),
);