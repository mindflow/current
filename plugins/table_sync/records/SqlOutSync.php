<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class SqlOutSync extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_sync_sql_out';
    //static $oneTablePerLocale = true;

    static $createTableSQL= "
        #classe et données concernée
        `sql_request` text,
        `sql_result` longblob,
        
        #nom de la règle d'envoi appliquée
        `sql_rule` varchar(30) DEFAULT '',
        
        #système cible
        `destination` varchar(1024) DEFAULT '',
        
        #etat et résultat de l'opération
        `state` varchar(10) DEFAULT 'stopped',
        `result` varchar(10) DEFAULT '',
        
        #log des opérations
        `local_log` longtext,
        `remote_log` longtext,
       
        
    ";

    static $createTableKeysSQL="";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init()
    {
        parent::init();

        global $mf,$l10n;

        $this->data = array_merge($this->data, array(

            'sql_request' => array(
                'value' => '',
                'dataType' => 'textarea 30 5',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-md-8'),
            ),
            'sql_result' => array(
	            'value' => '',
	            'dataType' => 'textarea 30 5',
	            'valueType' => 'text',
	            'div_attributes' => array('class' => 'col-md-8'),
	            'noEcho' => 1,
            ),
            'sql_rule' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
            ),
            'destination' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-3'),
            ),
            'state' => array(
                'value' => 'Inactive',
                'dataType' => 'select',
                'possibleValues' => array(
                    'Scheduled' => $l10n->getLabel( 'SqlOutSync','Scheduled'),
                    'Sending' => $l10n->getLabel( 'SqlOutSync','Sending'),
                    'Inactive' => $l10n->getLabel( 'SqlOutSync','Inactive'),
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
                'editable' => false,
            ),
            'result' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'Success' => $l10n->getLabel( 'SqlOutSync','Success'),
                    'Failure' => $l10n->getLabel( 'SqlOutSync','Failure'),
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
                'editable' => false,
            ),
            'local_log' => array(
                'value' => '',
                'dataType' => 'textarea 50 8',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-md-8'),
                //'editable' => false,
            ),
            'remote_log' => array(
                'value' => '',
                'dataType' => 'textarea 50 8',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-md-8'),
                //'editable' => false,
            ),

        ));


        $this->showInEditForm = array(

            'tab_main'=>array(
                'sql_request','sql_result','sql_rule','destination','state','result','local_log','remote_log',
            )
        );


        static::$postFormHTML[] = "<script></script>";

    }

	/**
	 * Quickly updates a column value in the database without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $name the column name in the database
	 * @param String $value the value to set
	 */
	function setField($name,$value){
    	global $pdo, $logger;

    	try{
		    $sql = "UPDATE " . self::getTableName() . " SET ".$name."=" . $pdo->quote( $value ) . " WHERE uid=" . $this->data['uid']['value'] . ";";
		    $pdo->query( $sql );

			$this->data[$name]['value'] = $value;
	    }
	    catch(PDOException $e){
		    $logger->critical("SqlOutSync setField PDOException code=".$e->getCode()." message=".$e->getMessage());
	    }

	}

	/**
	 * Quickly updates the local_log field without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $logMessage text of the log entry
	 */
	function addToLocalLog($logMessage){
		global $pdo;

		$now = new DateTime('NOW');
		$nowStr = $now->format('d/m/Y H:i:s');

		$this->data['local_log']['value'] .= $nowStr.' : '.$logMessage;
		$sql = "UPDATE " . self::getTableName() . " SET local_log=" . $pdo->quote($this->data['local_log']['value']) . " WHERE uid=" . $this->data['uid']['value'] . ";";
		$pdo->query( $sql );
	}

	static function getState($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;

			$sec = getSec(array('action' => 'replaySqlOut', 'uid' => $row['uid']));
			$resetSec = getSec(array('action' => 'resetState', 'uid' => $row['uid'], 'record_class' => 'SqlOutSync' ));

			switch($value){
				case 'Inactive':
					return '<span id="state_'.$row['uid'].'"><span class="darkgrey">'.$l10n->getLabel( 'SqlOutSync','Inactive') . ' &nbsp;<a onclick="replay(' . $row['uid'] . ',\'' . $sec->hash . '\');" class="play" title="' . $l10n->getLabel( 'SqlOutSync','replay') . '"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a></span></span>';

				case 'Sending':
					return '<span id="state_'.$row['uid'].'"><span class="orange">'.$l10n->getLabel( 'SqlOutSync','Sending') . '</span> &nbsp;<img src="' . SUB_DIR . '/mf/backend/templates/mf-default/img/small-loading.gif" />&nbsp;<a onclick="resetState(' . $row['uid'] . ',\'SqlOutSync\',\'' . $resetSec->hash . '\');" class="play" title="' . $l10n->getLabel('FullSync','reset') . '"><span class="glyphicon glyphicon-stop" aria-hidden="true"></span></a></span>';

				case 'Scheduled':
					return '<span id="state_'.$row['uid'].'"><span class="blue">'.$l10n->getLabel( 'SqlOutSync','Scheduled') . ' &nbsp;<a onclick="replay(' . $row['uid'] . ',\'' . $sec->hash . '\');" class="play" title="' . $l10n->getLabel( 'SqlOutSync','replay') . '"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a></span></span>';
			}
		}
		else return '<span id="state_'.$row['uid'].'"></span>';
	}

	static function getResult($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;
			switch($value){
				case 'Success':
					return '<span id="result_'.$row['uid'].'"><span class="green">'.$l10n->getLabel( 'SqlOutSync','Success') . '</span></span>';

				case 'Failure':
					return '<span id="result_'.$row['uid'].'"><span class="red">'.$l10n->getLabel( 'SqlOutSync','Failure') . '</span></span>';
			}
		}
		else return '<span id="result_'.$row['uid'].'"></span>';
	}






	/**
	 * Checks the database for tasks list
	 * This function must be called regularly by a cron task / mindflow's cron.php file
	 *
	 */
	static function fireSync(){
		global $pdo, $logger, $config;

		$logger->debug( 'SqlOutSync::fireSync()' );

		//as the cron script is called every minute, numerous instances of the script mays stack
		exec('ps aux | grep "'.$_SERVER['SCRIPT_FILENAME'].'"', $output);

		$output = implode(chr(10),$output);
		$nbOccurences = substr_count ( $output , "php ".$_SERVER['SCRIPT_FILENAME']);

		//do not run more than 2 instances of this script concurrently
		if($nbOccurences < 5) {

			$sql  = "SELECT * FROM " . self::getTableName() . " WHERE deleted=0 AND state='Scheduled' ORDER BY creation_date ASC";
			$stmt = $pdo->query( $sql );

			$logger->debug( "SqlOutSync fireSync() found " . $stmt->rowCount() . " rows to process" );


			if ( $stmt->rowCount() > 0 ) {

				$rows = $stmt->fetchAll( PDO::FETCH_ASSOC );

				foreach ( $rows as $row ) {
					//self::execSync( $row );

					// now we need to reload each row right before processing it in order to make sure it has not been
					// already processed by another iteration of the cron task
					$sql   = "SELECT * FROM " . self::getTableName() . " WHERE deleted=0 AND state='Scheduled' AND uid = " . $row['uid'];
					$stmt2 = $pdo->query( $sql );
					if ( $stmt2->rowCount() > 0 ) {

						$row2 = $stmt2->fetch( PDO::FETCH_ASSOC );

						if($row2)self::execSync( $row2 );
						else $logger->critical('ERROR SqlOutSync::fireSync SQL='.$sql);
					}
					//else skip the row
				}
			}

			//purge old records and prevent database bloating
			if ( isset( $config['plugins']['table_sync']['purge-delay'] ) ) {
				$purgeDelay = $config['plugins']['table_sync']['purge-delay'];
			} else {
				$purgeDelay = 60;
			}

			$sql = "DELETE FROM " . SqlOutSync::getTableName() . " WHERE creation_date < DATE_SUB(NOW(), INTERVAL " . $purgeDelay . " DAY);  ";
			//$logger->debug( $sql );
			$pdo->query( $sql );

			$sql = "DELETE FROM " . SqlInSync::getTableName() . " WHERE creation_date < DATE_SUB(NOW(), INTERVAL " . $purgeDelay . " DAY);  ";
			//$logger->debug( $sql );
			$pdo->query( $sql );
		}
		else $logger->debug( 'SqlOutSync : '.$nbOccurences.' instances already running. Aborting.' );
	}


	static function execSync(Array $row){
		global $pdo,$logger,$mf,$config,$l10n;

		$syncConfig = $config['plugins']['table_sync'];

$logger->debug("Running SqlOutSync::execSync() on uid=".$row['uid']);

		$SqlOutSync = new SqlOutSync();
		$SqlOutSync->load($row['uid']);

		$ruleName = $SqlOutSync->data['sql_rule']['value'] ;
		$rule = $syncConfig['sql_out_rules'][$ruleName];

		if ( isset( $rule['sendFunc'] ) ) {
			//apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
			$sendFunc = $rule['sendFunc'];
		} else {
			//or use default JSON based protocol function
			$SqlOutSync->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sendFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
			$sendFunc = "SyncManager::jsonSqlOutRequest";
		}

		//execute the defined sendFunc and effectively transmit the data
		$sendFunc( $SqlOutSync );
		$SqlOutSync->addToLocalLog( $l10n->getLabel( 'SyncManager', 'sending_performed' ) . chr( 10 ) );

	}
}















