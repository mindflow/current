<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'installToolLogin' => 'Login to install tool',
        'submit' => 'Login',
        'bad_password' => 'Invalid password',
        'installToolTitle' => 'MindFlow install tool',
        'session_expired' => 'The session has expired, please re-login',
        'create_tables' => 'Create SQL tables',
        'create_tables_desc' => 'Creates the database tables required to have MindFlow functional.',
        'tables' => 'Tables',
        'table' => 'Table',
        'create_table_error' => 'Error creating table',
        'create_table_success' => 'created successfully',
        'mf_system_classes' => 'MindFlow CMS system classes.',
        'configure_plugin' => 'Configure plugins',
        'configure_plugin_desc' => 'Setup plugins parameters.',
        'create_all_tables_plugin' => 'Create all tables for this plugin',
        'create_table' => 'Create this table',
        'import_data' => 'Import data',
        'delete_table' => 'Delete this table',
        'table_exists' => 'Table active',
        'db_warning' => 'Make sure the database is properly configured in your mf_websites/ configuration file before creating plugins tables.',
        'back' => 'Back',
        'version' => 'Version',
        'upgrade_tool' => 'Upgrade tool',
        'upgrade_tool_desc' => 'The upgrade tool executes scripts to update data to the current MindFlow version',
        'run_script' => "Run script",
        'upgrade-warning' => "IMPORTANT : Do not forget to backup your database before executing any of these scripts !",
        'view_scripts' => "View scripts",

        '1.9' => "Upgrade to MindFlow 1.9",
        '1.9.3' => "Upgrade to MindFlow 1.9.3",
        '1.9.4' => "Upgrade to MindFlow 1.9.4",
        '1.9_uploadsDir' => "Correct uploads path in file fields",
        '1.9_uploadsDir_desc' => "Seeks for records featuring files fields and updates their filepath for MindFlow 1.9, removing the uploadsDir from the filePath. The file path is now relative to the uploads directory, rather than to the web root. Beware this script can be long to execute if there are numerous objects to update. Be patient.",

        '1.9_dates' => "Correct date columns for SQL_STRICT conformity",
        '1.9_dates_desc' => "Corrects DATE and DATETIME columns to allow null dates when not defined. Improves stability and compatibility with latest versions of MySQL.",

        '1.9.3_dates' => " High precision creation_date and modification_date",
        '1.9.3_dates_desc' => "Changes creation_date and modification_date to include microseconds (requires PHP 7.1)",

        '1.9_history' => "Add history managment to records",
        '1.9_history_desc' => "Adds columns for managing history of dbRecords.",

        '1.9.4_editor' => "Notify if a record is currently being edited by another editor",
        '1.9.4_editor_desc' => "Adds to the dbRecord class necessary fields to notify if a record is currently being edited by another editor",

        '1.9.4_history' => "History fields fix",
        '1.9.4_history_desc' => "Adds default values for fields history_past and history_last",

    ),

    'fr' => array(
        'installToolLogin' => 'Connexion à l\'outil d\'installation',
        'submit' => 'Connexion',
        'bad_password' => 'Mot de passe invalide',
        'installToolTitle' => 'Outil d\'installation de MindFlow',
        'session_expired' => 'La session a expirée. Merci de vous re-connecter.',
        'create_tables' => 'Créer les tables SQL',
        'create_tables_desc' => 'Crée les tables SQL dans la base de données pour rendre MindFlow fonctionnel.',
        'tables' => 'Tables',
        'table' => 'Table',
        'create_table_error' => 'Erreur lors de la création de la table',
        'create_table_success' => 'créée avec succès',
        'mf_system_classes' => 'Classes systèmes du CMS MindFlow.',
        'configure_plugin' => 'Configurer les plugins',
        'configure_plugin_desc' => 'Accéder aux paramètres de configuration des plugins.',
        'create_all_tables_plugin' => 'Installer le plugin (crée toutes les tables et importe les données)',
        'create_table' => 'Créer cette table',
        'import_data' => 'Import données',
        'delete_table' => 'Supprimer cette table',
        'table_exists' => 'Table active',
        'db_warning' => 'Assurez-vous que votre base de données est correctement configurée dans le fichier de configuration du dossier mf_websites/ avant de vous lancer la création des tables des plugins.',
        'back' => 'Retour',
        'version' => 'Version ',
        'upgrade_tool' => 'Outil de mise à jour',
        'upgrade_tool_desc' => "L'outil de mise à jour exécute les scripts nécessaires pour mettre les données à jour avec la version courante de MindFlow",
        'run_script' => "Exécuter le script",
        'upgrade-warning' => "IMPORTANT : N'oubliez pas de sauvegarder votre base de données avant d'exécuter ces scripts !",
        'view_scripts' => "Voir les scripts",

        '1.9' => "Mise à jour vers MindFlow 1.9",
        '1.9.3' => "Mise à jour vers MindFlow 1.9.3",
        '1.9.4' => "Mise à jour vers MindFlow 1.9.4",
        '1.9_uploadsDir' => "Corriger le chemin d'upload dans les champs 'file'",
        '1.9_uploadsDir_desc' => "Recherche les enregistrements contenant des champs de file et met à jour le chemin de fichier pour MindFlow 1.9, en supprimant le dossier d'upload du chemin. Le chemin est désormais relatif au dossier d'upload plutôt qu'à la racine du site.<br/> Attention ! Ce script peut être long à s'exécuter si la base de données contient de nombreux objets. Soyez patient.",

        '1.9_dates' => "Corriger les dates pour la conformité SQL_STRICT",
        '1.9_dates_desc' => "Corrige les colones de type DATE et DATETIME pour autoriser les dates null si non définies. Améliore la stabilité et la compatibilité avec les serveurs MySQL récents.",

        '1.9.3_dates' => " Dates de création et dates de modification en haute précision",
        '1.9.3_dates_desc' => "Change les champs creation_date et modification_date pour inclure les microsecondes (requiert PHP 7.1)",

        '1.9_creator' => "Mise à jour champ 'creator'",
        '1.9_creator_desc' => "Mise à jour du champ 'creator' dans toutes les tables.",

        '1.9_parent' => "Ajout du champ 'parent_class'",
        '1.9_parent_desc' => "Ajout du champ 'parent_class' dans toutes les tables.",

        '1.9_history' => "Ajout gestion de l'historique des champs",
        '1.9_history_desc' => "Ajoute des colonnes pour assurer la gestion de l'historique des champs.",

        '1.9.4_editor' => " Signalement de l'édition d'un enregistrement par un tiers",
        '1.9.4_editor_desc' => "Ajoute à la classe dbRecord les champs nécessaires pour signaler si l'enregistrement courant est déjà édité par un tiers",

        '1.9.4_history' => "Correction des champs history",
        '1.9.4_history_desc' => "Ajout des valeurs par défaut history_past et history_last",
    ),
);