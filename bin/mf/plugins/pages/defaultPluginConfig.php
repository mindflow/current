<?php

//default configuration for the plugin
//add and edit these values in your config file
$config['plugins']['pages'] = array(
    //opens page editing in an ajax dialog rather than by replacing current view
    //'useAJAX' => 'true',

    //executes this function prior rendering any page if defined
    //define the function in your templates.php file
    'prepareDataFunc' => 'pagePrepareData()',
);