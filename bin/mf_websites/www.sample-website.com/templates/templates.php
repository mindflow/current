<?php

//load page template fields labels localisation
l10nManager::loadL10nFile('templates',$GLOBALS['config']['templates_directory'].'/languages/templates_l10n.php');

// You may load custom website labels localisation
$mf->l10n->loadL10nFile('sample_website', $GLOBALS['config']['templates_directory'].'/languages/sample_website_l10n.php');

//important : include the functions used by the template fields
include 'functions.php';

//you may include some microtemplates if you use these
//include 'microtemplates/microtemplates.php';

//templates definition
$mf->addTemplates(array(

    //A template with name 'default' is mandatory
    //newly created pages will use this template by default
    'default' => array(
        'template_name' => "Default",
        'template_file' => 'default.html',
        'template_data' => array(
            // Echoes frontend language key
            'lang'  => array(
                'dataType' => 'function',
                'value' => '',
                'processor' => 'getLang',
            ),
            //rich text field for the page body
            'page_html' => array(
                'value' => '',
                'dataType' => 'rich_text',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-8'),
                'index' => 'digest'
            ),
            // Includes a PHP file (avoid if possible, requires output buffering which may lead to unpredictable results when accessing MindFlow variables, prefer running a PHP function)
            'footer' => array(
                'dataType' => 'include',
                'value' => 'footer.php',
                'processor' => '',
            ),


        ),
        // List of fields which must show up when you edit the page in backend context. They show up in the specified order.
        'showInEditForm' =>'page_html',
    ),

    // now let's define a template for our homepage
    'index' => array(
        'template_name' => "Homepage",
        'template_file' => 'home.html',
        'template_data' => array(
            // Includes a HTML file
            'header' => array(
                'dataType' => 'include',
                'value' => 'header.html',
                'processor' => '',
            ),
            // Runs a PHP function defined in this file or in functions.php if functions.php is included from this file (this is more clean when you have a lot of functions)
            'navigation' => array(
                'dataType' => 'function',
                'value' => '',
                'processor' => 'getNavigation',
            ),
            // Includes a PHP file (avoid if possible, requires output buffering which may lead to unpredictable results when accessing MindFlow variables, prefer running a PHP function)
            'footer' => array(
                'dataType' => 'include',
                'value' => 'footer.php',
                'processor' => '',
            ),
            // Another PHP function call
            'google-analytics' => array(
                'dataType' => 'function',
                'value' => '',
                'processor' => 'getGoogleAnalytics',
            ),
            // Echoes frontend language key
            'lang'  => array(
                'dataType' => 'function',
                'value' => '',
                'processor' => 'getLang',
            ),
            // Shows the website logo
            'logo'  => array(
                'dataType' => 'function',
                'value' => '',
                'processor' => 'getLogo',
            ),
            // Show the website name
            'sitename'  => array(
                'dataType' => 'function',
                'value' => '',
                'processor' => 'getSiteName',
            ),

            //rich text field for the page body
            'page_html' => array(
                'value' => '',
                'dataType' => 'rich_text',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-8'),
                'index' => 'digest'
            ),


        ),
        // List of fields which must show up when you edit the page in backend context. They show up in the specified order.
        'showInEditForm' =>'page_html',
    ),


    

));


?>