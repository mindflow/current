<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(



    'fr' => array(
        'area_name' => 'Zone e-Colissimo',
        'min_weight' => 'Poids minimal (Kg)',
        'max_weight' => 'Poids maximal (Kg)',
        'fee' => 'Tarif (€)',

        'name_FR' => 'France',
        'name_OM1' => 'Outre Mer 1',
        'name_OM2' => 'Outre Mer 2',
        'name_INTA' => 'International Zone A',
        'name_INTB' => 'International Zone B',
        'name_INTC' => 'International Zone C',
        'name_INTD' => 'International Zone D',
        'name_SEC' => 'Secteurs postaux',


        /*mandatory records*/
        //'no_record' => 'Aucun résultat.',
        'record_name' => 'Tarifs colissimo',

    ),

);

?>