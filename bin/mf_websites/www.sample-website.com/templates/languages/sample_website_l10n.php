<?php
/**
 * This file contains the localised labels that you may call from your templates using either :
 *
 * IN PHP :
 *      $l10->getLabel('sample_website','label_name');
 *
 * IN HTML :
 *      {label sample_website label_name}
 *
 */


$l10nText = array(


    'fr' => array(
        'hello' => "Bienvenue sur le site d'exemple !",
        'logo' => "Ceci est un logotype",
        'learn_more' => "En savoir +",
    ),

    'en' => array(
        'hello' => "Welcome on the sample website !",
        'logo' => "This is a logotype",
        'learn_more' => "Learn more",
    ),

);