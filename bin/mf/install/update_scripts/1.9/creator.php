<?php
/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

$execution_start = time();

global $pdo,$config;


function makeTheUpgrade($instance,$className){

    global $mf,$pdo,$config,$l10n,$html,$logger;

    $tableName = $instance::getTableName();

    $html[] = "Analyzing record ".$className." table name=".$tableName."<br />".chr(10);


    try{

        $sql = "ALTER TABLE `".$tableName."` CHANGE `creator` `creator_uid` int(11) DEFAULT 0;";
        $html[] = $sql."<br />".chr(10);
        $stmt = $pdo->query($sql);
    }
    catch(PDOException  $e)
    {
        $html[] = $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
        $html[] = $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
        $logger->error($l10n->getLabel('main','error').$e->getMessage());
        $logger->error($l10n->getLabel('main','number').$e->getCode());

    }

    try{

        $sql = "ALTER TABLE `".$tableName."` ADD `creator_class` VARCHAR(50) NOT NULL DEFAULT '' AFTER `creator_uid`;";
        $html[] = $sql."<br />".chr(10);
        $stmt = $pdo->query($sql);
    }
    catch(PDOException  $e)
    {
        $html[] = $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
        $html[] = $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
        $logger->error($l10n->getLabel('main','error').$e->getMessage());
        $logger->error($l10n->getLabel('main','number').$e->getCode());

    }

    try{

        $sql = "ALTER TABLE `".$tableName."` ADD `modificator_uid` int(11) DEFAULT '0' AFTER `creator_class`;";
        $html[] = $sql."<br />".chr(10);
        $stmt = $pdo->query($sql);
    }
    catch(PDOException  $e)
    {
        $html[] = $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
        $html[] = $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
        $logger->error($l10n->getLabel('main','error').$e->getMessage());
        $logger->error($l10n->getLabel('main','number').$e->getCode());

    }

    try{

        $sql = "ALTER TABLE `".$tableName."` ADD `modificator_class` VARCHAR(50) AFTER `modificator_uid`;";
        $html[] = $sql."<br />".chr(10);
        $stmt = $pdo->query($sql);
    }
    catch(PDOException  $e)
    {
        $html[] = $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
        $html[] = $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
        $logger->error($l10n->getLabel('main','error').$e->getMessage());
        $logger->error($l10n->getLabel('main','number').$e->getCode());

    }


    $html[] = "<br />".chr(10);
}


foreach($mf->pluginManager->records as $pluginKey => $record){
    foreach($record as $className => $classData){
        $instance = new $className();

        if($instance::$oneTablePerLocale){
            //save current locale
            $currentLocale = $mf->info['data_editing_locale'] ;

            //alter table for each locale
            foreach ($config['frontend_locales'] as $locale => $localeName){
                //switch the locale
	            $mf->setDataEditingLanguage($locale);

                makeTheUpgrade($instance,$className);
            }

            //restore current locale
	        $mf->setDataEditingLanguage($currentLocale);
        }
        else {
            makeTheUpgrade($instance,$className);
        }



    }
}
$html[] = "Done.<br />".chr(10);

/*
foreach($mf->pluginManager->records as $pluginKey => $record){
    foreach($record as $className => $classData){
        eval('$instance = new '.$className.';');

        $tableName = $instance::$tableName;

        $html[] = "Analyzing record ".$className." table name=".$tableName."<br />".chr(10);


        try{

            $sql = "ALTER TABLE `".$tableName."` CHANGE `creator` `creator_uid` int(11) DEFAULT 0;";
            $html[] = $sql."<br />".chr(10);
            $stmt = $pdo->query($sql);
        }
        catch(PDOException  $e)
        {
            $html[] = $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
            $html[] = $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
            $mf->debugLog($l10n->getLabel('main','error').$e->getMessage());
            $mf->debugLog($l10n->getLabel('main','number').$e->getCode());

        }

        try{

            $sql = "ALTER TABLE `".$tableName."` ADD `creator_class` VARCHAR(50) NOT NULL DEFAULT '' AFTER `creator_uid`;";
            $html[] = $sql."<br />".chr(10);
            $stmt = $pdo->query($sql);
        }
        catch(PDOException  $e)
        {
            $html[] = $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
            $html[] = $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
            $mf->debugLog($l10n->getLabel('main','error').$e->getMessage());
            $mf->debugLog($l10n->getLabel('main','number').$e->getCode());

        }



        $html[] = "<br />".chr(10);

    }
}
$html[] = "Done.<br />".chr(10);*/
?>