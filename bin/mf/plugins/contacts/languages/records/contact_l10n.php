<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'tab_contact' => 'Properties',
        'first_name' => 'Your first name',
        'last_name'=> 'Your last name',
        'email'=> 'Your email',
        'phone'=> 'Your phone',
        'recipient'=> 'Recipient',
        'subject'=> 'Message subject',
        'message'=> 'Your message',

        /*mandatory records*/
        //'no_record' => 'There currently exists no contact request. You can <a href="{subdir}/mf/index.php?module=contacts&action=create">create a contact request</a>.',
        'record_name' => 'contact',
    ),

    'fr' => array(
        'tab_contact' => 'Propriétés',
        'first_name' => 'Votre prénom',
        'last_name'=> 'Votre nom',
        'email'=> 'Votre email',
        'phone'=> 'Votre téléphone',
        'recipient'=> 'Destinataire',
        'subject'=> 'Sujet du message',
        'message'=> 'Votre message',

        /*mandatory records*/
        //'no_record' => 'Il n\'existe pas de demande de contact pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=contacts&action=create">créer une demande de contact</a>.',
        'record_name' => 'contact',

    ),

    'de' => array(
        'tab_contact' => 'Informations',
        'first_name' => 'Vorname',
        'last_name'=> 'Name',
        'email'=> 'E-mail',
        'phone'=> 'Telefon',
        'recipient'=> 'Destinataire',
        'subject'=> 'Subjekt',
        'message'=> 'Ihre Mitteilung',

        /*mandatory records*/
        //'no_record' => 'Il n\'existe pas de demande de contact pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=contacts&action=create">créer une demande de contact</a>.',
        'record_name' => 'contact',

    ),
);

?>