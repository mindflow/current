<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alban
 * Date: 26/02/13
 * Time: 09:28
 * To change this template use File | Settings | File Templates.
 */
include 'microtemplate_functions.php';

$mf->addMicrotemplates(
    array(
        //A template with name 'default' is mandatory
        //newly created pages will use this template by default
        'infos' => array(
            'microtemplate_name' => 'Infos',
            'microtemplate_key' => 'infos',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'infos.html',
            'microtemplate_image' => 'infos.png',
            'microtemplate_title_field' => 'info-titre',
            'microtemplate_sorting' => '',
            'microtemplate_show_field_value' =>'info-titre',
            'microtemplate_data' => array(
                'info-photo' => array(
                    'dataType' => 'files jpg,jpeg,gif,png 166 102',
                    'value' => '',
                    'processor' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'processor' => 'photoPersonne',
                ),
                'info-titre' => array(
                    'dataType' => 'input',
                    'valueType' => 'text',
                    'value' => '',
                    'field_attributes' => array('class' => 'input-lg'),
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'info-description' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'info-suite' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
            ),

            'showInEditForm' =>'info-photo,info-titre,info-description,info-suite,',
        ),

        'infos2' => array(
            'microtemplate_name' => 'Infos2',
            'microtemplate_key' => 'infos2',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'infos.html',
            'microtemplate_image' => 'infos.png',
            'microtemplate_title_field' => 'info-titre',
            'microtemplate_sorting' => '',
            'microtemplate_show_field_value' =>'info-titre',
            'microtemplate_data' => array(
                'info-photo' => array(
                    'dataType' => 'files jpg,jpeg,gif,png 166 102',
                    'value' => '',
                    'processor' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'processor' => 'photoPersonne',
                ),
                'info-titre' => array(
                    'dataType' => 'input',
                    'valueType' => 'text',
                    'value' => '',
                    'field_attributes' => array('class' => 'input-lg'),
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'info-description' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'info-suite' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
            ),

            'showInEditForm' =>'info-photo,info-titre,info-description,info-suite,',
        ),

        'roues' => array(
            'microtemplate_name' => 'roues',
            'microtemplate_key' => 'roues',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'roues.html',
            'microtemplate_image' => 'roues.png',
            'microtemplate_title_field' => '',
            'microtemplate_sorting' => '',
            'microtemplate_show_field_value' =>'roues-nbr',
            'microtemplate_data' => array(

                'roues-nbr' => array(
                    'dataType' => 'input',
                    'valueType' => 'text',
                    'value' => '',
                    'field_attributes' => array('class' => 'input-lg'),
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'roues-distance' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'roues-vousetes' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
            ),

            'showInEditForm' =>'roues-nbr,roues-distance,roues-vousetes,',
        ),        

        'guidons' => array(
            'microtemplate_name' => 'guidons',
            'microtemplate_key' => 'guidons',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'guidons.html',
            'microtemplate_image' => 'guidons.png',
            'microtemplate_title_field' => '',
            'microtemplate_sorting' => '',
            'microtemplate_show_field_value' =>'guidons-nbr',
            'microtemplate_data' => array(

                'guidons-nbr' => array(
                    'dataType' => 'input',
                    'valueType' => 'text',
                    'value' => '',
                    'field_attributes' => array('class' => 'input-lg'),
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'guidons-techniques' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'guidons-terrains' => array(
                    'dataType' => 'textarea',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
            ),

            'showInEditForm' =>'guidons-nbr,guidons-techniques,guidons-terrains,',
        ),

        'achat1' => array(
            'microtemplate_name' => 'achat1',
            'microtemplate_key' => 'achat1',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'achat1.html',
            'microtemplate_image' => 'achat1.png',
            'microtemplate_title_field' => '',
            'microtemplate_sorting' => '',
            'microtemplate_show_field_value' =>'achat-titre',
            'microtemplate_data' => array(
                'achat-photo' => array(
                    'dataType' => 'files jpg,jpeg,gif,png',
                    'value' => '',
                    'processor' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'processor' => 'photoPersonne',
                ),
                'achat-titre' => array(
                    'dataType' => 'input',
                    'valueType' => 'text',
                    'value' => '',
                    'field_attributes' => array('class' => 'input-lg'),
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
                'achat-description' => array(
                    'dataType' => 'rich_text',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
            ),

            'showInEditForm' =>'achat-photo,achat-titre,achat-description,',
        ),

        'achat2' => array(
            'microtemplate_name' => 'achat2',
            'microtemplate_key' => 'achat2',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'achat2.html',
            'microtemplate_image' => 'achat2.png',
            'microtemplate_title_field' => '',
            'microtemplate_sorting' => '',
            'microtemplate_show_field_value' =>'achat-photo',
            'microtemplate_data' => array(
                'achat-photo' => array(
                    'dataType' => 'files jpg,jpeg,gif,png',
                    'value' => '',
                    'processor' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'processor' => 'photoPersonne',
                ),
                'achat-description' => array(
                    'value' => '',
                    'dataType' => 'rich_text',
                    'valueType' => 'text',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
            ),

            'showInEditForm' =>'achat-photo,achat-description,',
        ),        
        
        'achat3' => array(
            'microtemplate_name' => 'achat3',
            'microtemplate_key' => 'achat3',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'achat3.html',
            'microtemplate_image' => 'achat3.png',
            'microtemplate_title_field' => '',
            'microtemplate_sorting' => '',
            'microtemplate_show_field_value' =>'achat-photo',
            'microtemplate_data' => array(
                'achat-photo' => array(
                    'dataType' => 'files jpg,jpeg,gif,png',
                    'value' => '',
                    'processor' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'processor' => 'photoPersonne',
                ),
                'achat-description' => array(
                    'dataType' => 'rich_text',
                    'value' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'index' => 'digest'
                ),
            ),

            'showInEditForm' =>'achat-photo,achat-description,',
        ),
        
        'client' => array(
            'microtemplate_name' => 'Client',
            'microtemplate_key' => 'client',//important! same as parent
            'microtemplate_hidden' => '0',
            'microtemplate_file' => 'client.html',
            'microtemplate_image' => 'client.png',
            'microtemplate_title_field' => 'photo',
            'microtemplate_sorting' => '',
            'microtemplate_data' => array(
                'photo' => array(
                    'dataType' => 'files jpg,jpeg,gif,png',
                    'value' => '',
                    'processor' => '',
                    'div_attributes' => array('class' => 'col-lg-8'),
                    'processor' => 'photoPersonne',
                ),
            ),

            'showInEditForm' =>'photo',
        ),
    )
);