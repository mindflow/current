<?php

/**
 * This wizards assists you in creating a new plugin.
 * It is possible to specify in the command line a config file featuring the preconfigured used variables in order to avoid having to seize them at every run when configuring the wizard.
 * In this cas, call the script using the following syntax :
 *
 * php plugin_create.php config_file.php
 *
 * See file sample-configs/agPlugin_config.php for the available variables to configure.
 */

//http://php.net/manual/en/features.commandline.php

require_once "colors.php";
require_once "functions.php";

// Create new Colors class
$colors = new Colors();

if(isset($argv[1])){
    echo chr(10);
    if(is_file ( $argv[1] )) {
        echo $colors->getColoredString("config file specified, loading defaults from the file...", 'cyan', null);
        include $argv[1];
        echo $colors->getColoredString("done", 'cyan', null).chr(10).chr(10);
    }
    else echo $colors->getColoredString("Specified config.php file not found, running 100% interactive mode.", 'dark_gray', null).chr(10).chr(10);
}



echo "Natural name of the plugin you want to create ? [My Plugin] : ";
$prefix = chr(10);
if(!isset($pluginName)){
    $pluginName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginName=='')$pluginName = 'My Plugin';

echo $prefix.$colors->getColoredString("Plugin will be named \"".$pluginName."\"", 'cyan', null).chr(10).chr(10);



echo "Short description ? [This plugin does ...] : ";
$prefix = chr(10);
if(!isset($pluginDesc)){
    $pluginDesc = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginDesc=='')$pluginDesc = 'This plugin does ...';

echo $prefix.$colors->getColoredString("Plugin description will be \"".$pluginDesc."\"", 'cyan', null).chr(10).chr(10);

echo "Base ISO2 locale for localized files ? [en] : ";
$prefix = chr(10);
if(!isset($defaultLocale)){
    $defaultLocale = trim(fgets(STDIN));
    $prefix = '';
}
if($defaultLocale=='')$defaultLocale = 'en';

echo $prefix.$colors->getColoredString("Plugin default locale will be \"".$defaultLocale."\"", 'cyan', null).chr(10).chr(10);


//suggest class name from pluginName
$nameChunks = explode(' ',$pluginName);
$isFirst=true;
$suggestedName = '';
foreach($nameChunks as $chunk){
    if($isFirst){
        $suggestedName = strtolower($chunk);
        $isFirst=false;
    }
    else $suggestedName .= ucfirst($chunk);
}
$suggestedName=trim($suggestedName);

echo "Class name of the plugin you want to create ? [".$suggestedName."] : ";
$prefix = chr(10);
if(!isset($pluginClassName)){
    $pluginClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginClassName=='')$pluginClassName = $suggestedName;
echo $prefix.$colors->getColoredString("Plugin class will be named \"".$pluginClassName."\"", 'cyan', null).chr(10).chr(10);



echo "Folder name of the website where the plugin should be created (in /mf_websites) [default] : ";
$prefix = chr(10);
if(!isset($websiteFolderName)){
    $websiteFolderName = trim(fgets(STDIN));
    $prefix = '';
}
if($websiteFolderName=='')$websiteFolderName = 'default';
echo $prefix.$colors->getColoredString("Plugin will be created in /mf_websites/".$websiteFolderName.'/plugins', 'cyan', null).chr(10).chr(10);


echo "Folder name / plugin key of the plugin you want to create ? [$pluginClassName] : ";
$prefix = chr(10);
if(!isset($pluginDirName)){
    $pluginDirName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginDirName=='')$pluginDirName = $pluginClassName;
$pluginKey = $pluginDirName;

echo $prefix.$colors->getColoredString("Will create plugin folder /mf_websites/".$websiteFolderName."/plugins/".$pluginDirName, 'cyan', null).chr(10).chr(10);


$ready = "";
$acceptedValues = array('y','n','yes','no');
while (!in_array($ready, $acceptedValues)){
    echo "Will now create plugin files, ready to write ? [yes/no] : ";
    $ready = trim(fgets(STDIN));
}
if($ready=='y' || $ready=='yes'){
    echo chr(10).$colors->getColoredString("Now writing files...", 'green', null);

    //value for moving to mf root dir
    $chdir = "../.." ;

    //create plugin folder
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName);

    //create plugin index.php file
    $fileContent = file_get_contents ("ressources/plugin_template/index.php");
    $fileContent = str_replace("{pluginClassName}",$pluginClassName,$fileContent);
    $fileContent = str_replace("{pluginDirName}",$pluginDirName,$fileContent);
    $fileContent = str_replace("{pluginKey}",$pluginKey,$fileContent);
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/index.php',$fileContent);

    //create language folder
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName."/languages");

    //create plugin language main file
    $fileContent = file_get_contents ("ressources/plugin_template/languages/myPlugin_l10n.php");
    $fileContent = str_replace("{defaultLocale}",$defaultLocale,$fileContent);
    $fileContent = str_replace("{pluginNaturalName}",str_replace('"','\"',$pluginName),$fileContent);
    $fileContent = str_replace("{pluginDescription}",str_replace('"','\"',$pluginDesc),$fileContent);
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/languages/'.$pluginClassName.'_l10n.php',$fileContent);

    //create plugin backend menu file
    $fileContent = file_get_contents ("ressources/plugin_template/languages/backendMenus_l10n.php");
    $fileContent = str_replace("{defaultLocale}",$defaultLocale,$fileContent);
    $fileContent = str_replace("{pluginNaturalName}",str_replace('"','\"',$pluginName),$fileContent);
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/languages/backendMenus_l10n.php',$fileContent);


    echo chr(10).$colors->getColoredString("Done.", 'cyan', null);
    echo chr(10).$colors->getColoredString("Do not forget your plugin must feature at least one module. Run plugin_add_module.php to add a module.", 'purple', null);
    echo chr(10).$colors->getColoredString("Do not forget to add the plugin to your website's config file so it gets executed by MindFlow.", 'purple', null).chr(10).chr(10);
}
else echo chr(10).$colors->getColoredString("Aborting write. Done.", 'red', null).chr(10).chr(10);

echo chr(10);

