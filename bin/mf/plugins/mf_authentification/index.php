<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';


class mfAuthentificationPlugin implements plugin{

    function setup(){

    }


    function getKey(){
        return "mf_authentification";
    }

    function getName(){
        global $l10n;
        return $l10n->getLabel('mfAuthentificationPlugin','name');
    }

    function getDesc(){
        global $l10n;
        return $l10n->getLabel('mfAuthentificationPlugin','desc');
    }

    function getPath(){
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getVersion(){
        return "1.1";
    }

    function getDependencies(){
        $dependencies = array();
        return $dependencies;
    }

    function init(){
        global $mf;

        //load default module
        $mf->pluginManager->loadModule("/mf/plugins/mf_authentification","","mfAuthentification");
    }
}

$mf->pluginManager->addPluginInstance(new mfAuthentificationPlugin());








