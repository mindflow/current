.. |vspace| raw:: latex

   \vspace{5mm}
   
.. |embed_js| raw:: html

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
	$(document).ready(function() {
		$("a[href^='http']").attr('target','_blank');
	});
	</script>
	
|embed_js|

*******
Support
*******

Signaler un bug
###############

.. important::

	**Pour signaler un problème rencontré avec MindFlow, utilisez l'issue tracker sur GITLAB :** `https://gitlab.com/mindflow/current/issues <https://gitlab.com/mindflow/current/issues>`_ 
	
	Si vous voulez que votre problème soit examiné, nous vous recommandons de présenter une description aussi précise que possible et de fournir si possible un exemple minimal ou fichier permettant de reproduire le problème.
	Les descriptions laconiques du genre 'xxx ne marche pas' ne seront pas traitées.
	


FAQ
###

Compatibilité
*************

MindFlow a été exécuté avec succès sur les configurations suivantes :

* **PHP :** PHP 5.3 à 7.1

|vspace|

* **MYSQL :** MySQL 5.1 à MySQL 5.6, MariaDB 5.6

|vspace|

* **Serveur HTTP :** Apache 2.0, Nginx (cf: :ref:`nginx`)

|
|
|

Edition de pages
****************

Les images transférées via le module KCFinder de l'éditeur de texte enrichi ne s'affichent pas.
-----------------------------------------------------------------------------------------------

KCFinder crée un fichier .htaccess dans le dossier uploads/ de votre site web pour renforcer la sécurité et éviter nottament l'upload de scripts PHP. 

Nous avons constaté, sur certains hébergements, que les directives ``php_flag engine Off`` présentes dans le fichier généraient une erreur 500, notament lorsque PHP est n'est pas executé au niveau de l'hébergement en tant que module (dans ce cas PHP est généralement executé en cgi, fcgi, suphp ou php-fpm).

La suppression des commandes ``php_flag engine Off`` permet de restaurer le fonctionnement, mais diminue la sécurité. Il serait possible de la re-configurer via le fichier php.ini, mais attention, cette directive doit être restreinte au dossier uploads/ uniquement, sinon vous bloqueriez l'execution des scripts PHP sur tout l'hébergement.

|
|
|

Plugins
*******

Mon module ne s'affiche pas
---------------------------

* Vérifiez la valeur de retour de la fonction ``getType()`` dans votre classe héritée de module :

.. code-block:: php

	function getType(){
		return 'backend,frontend';
	}

Si par exemple 'frontend' n'est pas mentionné et que vous souhaitez afficher votre plugin en frontend, alors celui-ci ne sera pas évalué lors d'un rendu frontend. Idem pour le backend. Ce fonctionnement permet d'optimiser les performances, et de ne pas charger et évaluer des plugins inutilisés sur les interfaces frontend ou backend.

* Vérifiez que votre module est bien déclaré dans le fichier ``index.php`` à la racine du dossier de votre plugin :

.. code-block:: php

	$mf->pluginManager->loadModule(
		$config['website_pluginsdir']."/dossier_du_plugin",
		"sous_dossier_du_module",
		"nom_du_plugin", 
		true
	);

* Vérifiez que la valeur du paramètre ``autorendering`` est à « true » dans votre appel à la fonction ``->loadPlugin()``

* Vérifiez qu'une instance de votre plugin est bien ajoutée au ``pluginManager`` dans le fichier ``index.php`` à la racine de votre plugin :

.. code-block:: php

	$mf->pluginManager->addPluginInstance(new myPlugin());
	
	

|
|
|

Environnement serveur
*********************

.. _nginx:

MindFlow sous Nginx
-------------------

MindFlow est développé et testé extensivement sous environnement Apache. Néanmoins, il est capable de tourner avec succès sous Nginx avec la configuration suivante :

::
	
	server {
        # listen ports
        listen 80;
        listen [::]:80; # IPv6


		# spdy for  Nginx < 1.9.5
		#listen 443 ssl spdy;
		#listen [::]:443 ssl spdy;
		#spdy_headers_comp 9;

		# http2 for Nginx >= 1.9.5
		listen 443 ssl http2;
		listen [::]:443 ssl http2;

		# If in need to study url rewriting problems, writes to website error log
		#rewrite_log on;

		# website domain name 
		server_name www.sample-website.com;

		# MindFlow hosting directory
		root /var/www/html/www.sample-website.com;

		
		# If you want to redirect all requests to https 
		#if ($scheme = http) {
		#   return 301 https://$server_name$request_uri;
		#}	

		# If you want to redirect all requests to a main domain 
		#if ($http_host != $server_name) {
		#	return 301 https://$server_name$request_uri;
		#}

		# index file names
		index index.php index.htm index.html;

		# logg files for this virtual host
		access_log /var/log/nginx/www.sample-website.com/access.log;
		error_log /var/log/nginx/www.sample-website.com/error.log;


		# Charset
		charset utf-8;

		# Add trailing slash if missing
		rewrite ^([^?#]*/)([^?#./]+)([?#].*)?$ $1$2/$3 permanent;	

		# Cache Static Files
		location ~* \.(html|css|js|png|jpg|jpeg|gif|ico|svg|eot|woff|ttf)$ { expires max; }

		# Do not allow access to hidden files.
		location ~ /\. { deny all; }
		location ~ /\_ { deny all; }

		location = /favicon.ico {
			log_not_found off;
			access_log off;
		}

		location = /robots.txt {
			allow all;
			log_not_found off;
			access_log off;
		}

		#GZip compression
		gzip on;
		gzip_vary on;
		gzip_types text/css text/javascript text/plain text/xml application/json application/x-javascript application/xml application/xml+rss;

		#Enable to increase compresssion. Sacrifices CPU.
		gzip_comp_level 9;


		#Include your SSL configuration
		include nginx.ssl.conf;

		client_max_body_size 100M;

		location ~ /\.(js|css)$ {
			expires 604800s;
		}

		if (!-e $request_filename){
			rewrite ^/(.+)\.(\d+)\.(js|css|png|jpg|gif|gzip)$ /$1.$3 last;
		}



		# Protect .git files
		location ~ /\.git 
		{	 
			deny all; 
		}

		# Block execution of PHP files in uploads folders
		#location ~* /(?:uploads|files)/.*\.php$ 
		#{
		#	deny all;
		#}

		# protect configuration files
		location ~* /mf_config/.*.php$
		{
			deny all;
		}

		location = /mf_websites/www.sample-website.com/config-dev.php{
			deny all;
		}

		location = /mf_websites/www.sample-website.com/config-prod.php{
			deny all;
		}

		# Reecriture pour le sitemap (*)
		location /sitemap {
				rewrite ^/sitemap\.xml/?$ /mf/plugins/pages/sitemap-pages.php break;
		}

		
		location / {
			try_files $uri $uri/ /index.php;
			fastcgi_index index.php;
			include fastcgi_params;
			fastcgi_pass unix:/run/php/php7.0-fpm.sock;
			fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
			fastcgi_intercept_errors off;
			fastcgi_buffer_size 128k;
			fastcgi_buffers 256 16k;
			fastcgi_busy_buffers_size 256k;
			fastcgi_temp_file_write_size 256k;
			fastcgi_read_timeout 1200;
		}
		
		# pass the PHP scripts to FastCGI server
			location ~ \.php$ {
			fastcgi_index index.php;
			
			include fastcgi_params;
			fastcgi_pass unix:/run/php/php7.0-fpm.sock;
			fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
			fastcgi_intercept_errors off;
			fastcgi_buffer_size 128k;
			fastcgi_buffers 256 16k;
			fastcgi_busy_buffers_size 256k;
			fastcgi_temp_file_write_size 256k;
			fastcgi_read_timeout 1200;
		}
		
		location ~* ^/(mf/|mf_websites/|mf_librairies/|favicon\.ico) {
		}
	}

