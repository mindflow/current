<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow search engine and search indexer",
        'desc' => "<p>Provides search indexing medium using the mfIndexable interface for your records and provides storage of the indexed data in the mf_search_index table</p><p>The search result page uid must be specified in the plugin configuration, and the page should be called with the request parameter ?q=my+search+terms in order to display the search result. Look into plugin 'pages' for an exemple of usage of the search indexing.</p>"


    ),


    'fr' => array(
        'name' => "Moteur de recherche et indexeur MindFlow",
        'desc' => "<p>Fournit un outil d'indexation au moyen de l'interface mfIndexable pour vos enregistrements et fournit le stockage des données d'indexation via la table mf_search_index</p><p>L'uid de la page de résultats doit être spécifié dans la configuration du plugin, et la page doit être appelée avec le paramètre de recherche ?q=my+search+terms pour afficher des résultats. Examinez le plugin 'pages' pour un exemple d'indexation dans le moteur de recherche.</p>"


    ),
);

?>