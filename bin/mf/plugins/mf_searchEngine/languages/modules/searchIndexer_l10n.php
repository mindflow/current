<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(
    'en' => array(
        'menu_title' => 'Search engine',
        'manage_search_index' => 'Manage search index',

    ),

    'fr' => array(
        'menu_title' => 'Moteur de recherche',
        'manage_search_index' => 'Gérer l\'index de recherche',

    ),
);