<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/records/moneticoBankFeedback.php';
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/forms/bankFeedbacksForm.php';

class editBankFeedbacks implements module
{
    private $pluginName = 'editBankFeedbacks';
    private $pluginVersion = '1.0';
    private $pluginType = 'backend,frontend';
    private $dependencies = '';

    private $errorMessage='';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function __construct(){

    }


    function prepareData(){
        global $mf,$l10n;
        $locale = $mf->getLocale();
        $this->pageOutput = '';


        if($mf->mode == 'backend'){

            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('monetico','allowEditMoneticoPayments')==1)));

            if($this->moduleAccess){
                //add the plugin to the backend menus

                if(isset($_REQUEST['submodule']) && ($_REQUEST['submodule'] == $this->pluginName)){
                    //breadcrumb
                    $this->section = $l10n->getLabel('backend','datas');
                    $this->moduleName = " / ".$l10n->getLabel('editBankFeedbacks','menu_title');
                    $this->subModuleName = "";

                   // $this->localMenu ='<li rel="'.$this->pluginName.'" class="active"><a href="{subdir}/mf/index.php?module='.$this->pluginName.'">{menu-title}</a></li>';




                    if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                    switch($this->action){

                        case "list":
                        default:

                                //composition du filtre d'affichage
                                $recordsFilter = new bankFeedbacksForm();

                                $form = $mf->formsManager;

                                $this->pageOutput .= '<div id="formFilter">';
                                $this->pageOutput .= $form->editForm(SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/monetico/monetico-json-backend.php', 'bankFeedbacksForm', 'updateContent', $recordsFilter, $this->pluginName, '',true, $l10n->getLabel('backend','search'), null);
                                $this->pageOutput .= '<script>
                                    function updateContent(jsonData){
                                        $("#recordEditTable_moneticoBankFeedback").html(jsonData.message);
                                    }
                                </script></div>';


                                $this->pageOutput .= $this->listRecords('','');
                                break;
                    }
                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
                $this->localMenu = '';
            }

        }
    }

    function render(&$mainTemplate){
        global $mf,$l10n;
        $locale = $mf->getLocale();



        if($mf->mode == 'backend'){
            if(isset($_REQUEST['submodule']) && ($_REQUEST['submodule'] == $this->pluginName)){

                $mainTemplate = str_replace("{module-body}",$this->pageOutput,$mainTemplate);
                $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);
                $mainTemplate = str_replace("{submodule-id}",$this->pluginName,$mainTemplate);

            }

        }
    }

    function setup(){
      /*  $pdo = $GLOBALS['mf']->db->pdo;

        $user = new vzSatisfaction();
        $user->createSQLTable();

        $html = "Table \"".vzSatisfaction::$tableName."\" created sucessfully !<br />";

       return $html;*/
    }


    function getType(){
        return $this->pluginType;
    }

    function getName(){
        return $this->pluginName;
    }

    function getVersion(){
        return $this->pluginVersion;
    }

    function getDependencies(){
        return $this->dependencies;
    }


    function listRecords($sqlConditions = '', $dateConditions = '',  $deletedCondition=' AND mf_monetico_bank_feedback.deleted=0'){

        global $mf,$l10n;

        $html= array();

        //using time to store session SQL in case of multiple open tabs in the browser.
        //$time will allow distincting the good tab because the session now knows when the form was generated
        $sec = md5(microtime());

        //requête pour l'export CSV stockée en $_SESSION
        if(!isset($_SESSION['actions']))$_SESSION['actions']=array();
        $_SESSION['actions'][$sec]=array(
            'action' => 'exportCSV',
            'sql' => 'SELECT * FROM mf_monetico_bank_feedback WHERE deleted=0'.$sqlConditions,
            'sqlConditions' => $sqlConditions,
            'record_class' => 'moneticoBankFeedback',
            'skipKeys' => array('uid', 'deleted', 'hidden', 'sorting', 'creator_uid','creator_class','parent_uid','parent_class', 'edit_lock', 'start_time', 'end_time', 'language', 'alt_language','creation_date','modification_date','parent_uid'

            ),
            'print_column_names' => true,
            'keyProcessors' => array(
                //'civility'=>'moneticoBankFeedback::getCivility',
            ),
        );

        $object = new moneticoBankFeedback();

        $buttons = '<button type="button" class="btn-sm mf-btn-new" id="exportCSV" name="exportCSV" onclick="document.location=\''.getHTTPHost().SUB_DIR.'/mf/core/csv-exporter.php?sec='.$sec.'\';"><span class="glyphicon glyphicon-th"></span> '.$l10n->getLabel('backend','export_to_csv').'</button>';

        //default date conditions
        //if($dateConditions=='')$dateConditions = ' AND DateAffichage <= CURDATE()'; //AND DateFin >= CURDATE()

        $html[] = $object->showRecordEditTable(
            array(
                'SELECT' => 'creation_date,montant,reference,`code-retour`', //,deleted
                'FROM' => '',
                'JOIN' => '',
                'WHERE' => '1=1'.$dateConditions.$sqlConditions.$deletedCondition,
                'ORDER BY' => 'creation_date',
                'ORDER DIRECTION' => 'DESC',
            ),
            'monetico',
            'editBankFeedbacks',
            'montant',
            $keyProcessors = array(
                'creation_date' => 'dbRecord::formatDatetime',
                'date' => 'dbRecord::formatDate',
                'code-retour' => 'moneticoBankFeedback::formatCodeRetour',
            ),
            $page = NULL,

            array(
                'create' => 1,
                'view' => 1,
                'edit' => 1,
                'delete'=> 1
            ),
            array(
                'ajaxActions' => true,
                'buttons' => $buttons,
                'columnClasses' => array(
                    //'creation_date' => 'hidden-xs',
                ),
                'debugSQL' => 0,
                'editRecordButtons' => array(
                    'showSaveButton' => true,
                    'showSaveCloseButton' => true,
                    'showPreviewButton' => false,
                    'showCloseButton' => true
                )
            ),
            'moneticoBankFeedback'
        );

        //Ouverture d'un enregistrement si appel du backend via un lien mail de notification de nouveau paiement Monetico
        if(isset($_REQUEST['uid'])){

            $secKeys = array('action' => 'editRecord', 'record_class' => 'moneticoBankFeedback', 'record_uid'=> intval($_REQUEST['uid']));
            $secHash = formsManager::makeSecValue($secKeys);
            $secFields = implode(',',array_keys($secKeys));

            $html[] = '<script>
            $(document).ready(function() {
                openRecord("/mf/core/ajax-core-html.php?action=editRecord&record_class=moneticoBankFeedback&record_uid='.intval($_REQUEST['uid']).'&header=0&footer=0&sec='.$secHash.'&fields='.$secFields.'&ajax=1&showSaveButton=1&showSaveCloseButton=1&showPreviewButton=&showCloseButton=1&mode='.$mf->mode.'","90%","","'.$l10n->getLabel('moneticoBankFeedback','record_name').'");
             });
             </script>';
        }

        return implode(chr(10),$html);


    }




}

