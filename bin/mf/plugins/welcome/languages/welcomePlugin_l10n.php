<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'name' => "MindFlow backend welcome screen",
        'desc' => "<p>Shows a welcome screen after login. You may duplicate this plugin and customize it according to your needs.</p>"

    ),


    'fr' => array(
        'name' => "Ecran d'accueil du backend MindFlow",
        'desc' => "<p>Affiche un écran d'accueil après la connexion. Vous pouvez dupliquer ce plugin et le personnaliser selon vos besoins.</p>"

    ),
);

?>