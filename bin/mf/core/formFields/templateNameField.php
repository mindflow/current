<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Allows to select a template name among a list of templates.
 */
class templateNameField implements formField
{
    /**
     * Returns the HTML code for displaying the field, initialized with the data supplied in $f->fieldData
     * @param $f fieldData : this stdClass object contains all the field info. Defining the function arguments into an object avoids redefining all existing functions already issued when a new argument requires to be added to the calls.
     * $f->formID           // value of the id attribute of the <form> tag this field is included in
     * $f->mode             // the mode value allows discriminating between templates (mode=="template|") and microtemplates (mode=="micro|")
     * $f->dataType         // this is an numeric array of every arguments included in the 'dataType' index of the field. example : $MyField['dataType']='input text' gets available as  array(0 => 'input', 1=> 'text')
     * $f->key              // this is the unique key (or field name/identifier ) for the currentField. Exemple : 'clientName'
     * $f->fieldData        // this is the data array of the current field
     * $f->recordUid        // the uid (Unique ID) of the record containing the current field
     * $f->recordClassName  // the class name of the record containing the current field
     * $f->record           // all the data for the record containing the current field
     * $f->microKey         // field name when processing a microTemplate field (only used when mode=="micro|")
     * $f->microIndex       // sorting index when processing a microTemplate field (only used when mode=="micro|")
     * @returns String a string containing the HTML for displaying the field
     */
    static public function displayField($f)
    {
        global $mf;
        $html = array();
        $fieldAttr = formsManager::getTagAttributes('field',$f->key, $f->fieldData);
        $divAttr = formsManager::getTagAttributes('div',$f->key, $f->fieldData);

        if(!isset($f->dataType[1])) $f->dataType[1]='text';

        //display the label in front of the field or not
        $showLabel      = (isset($f->fieldData['showLabel']))?      $f->fieldData['showLabel']      :true;

        //display a <div> around the field or not
        $showFieldDiv   = (isset($f->fieldData['showFieldDiv']))?   $f->fieldData['showFieldDiv']   :true;

        //display custom HTML in front of the field
        $preFieldHTML   = (isset($f->fieldData['preFieldHTML']))?   $f->fieldData['preFieldHTML']   :'';

        //display custom HTML in after the field
        $postFieldHTML  = (isset($f->fieldData['postFieldHTML']))?  $f->fieldData['postFieldHTML']  :'';

        //display custom HTML in front of the field
        $preDivHTML   = (isset($f->fieldData['preDivHTML']))?   $f->fieldData['preDivHTML']   :'';

        //display custom HTML in after the field
        $postDivHTML  = (isset($f->fieldData['postDivHTML']))?  $f->fieldData['postDivHTML']  :'';

        //we need a unique id for the .wrap("<span class=\"input-group-addon\"></span>") function below
        $dt_uid = time();

        if($showLabel)$html[] = formsManager::getFieldLabel($f->fieldData, $f->mode, $f->key, $f->recordClassName);


        if(!isset($f->fieldData['editable']))$f->fieldData['editable']=true;
	    $isEditable = filterBoolean($f->fieldData['editable']);

        if(!$isEditable){
            //read only record display, no input editing
            //seek selected value
            $selectedValues=array();
            foreach($mf->templates as $template_id => $template){
                if($template_id == $f->fieldData['value'])$selectedValues[]=$template['template_name'];
            }

            //read only record display, no input editing
            $html[] = $preDivHTML;
            $html[] = ($showFieldDiv)?'<div class="'.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
            $html[] = $preFieldHTML;
            $html[] = '<span id="noedit|'.$f->key.'" class="noedit '.$fieldAttr['classes'].'" '.$fieldAttr['attributes'].'>'.implode('<br/>'.chr(10),$selectedValues).'</span>';
            $html[] = $postFieldHTML;
            $html[] = '<input type="hidden" value="'.$f->fieldData['value'].'" name="'.$f->mode.$f->key.'" id="'.$f->mode.$f->key.'"/>';
            $html[] = ($showFieldDiv)?'</div>':'';
            $html[] = $postDivHTML;

        }
        else {
            //input editing
            $html[] = $preDivHTML;
            $html[] = ($showFieldDiv)?'<div class="'.$divAttr['classes'].'" '.$divAttr['attributes'].'>':'';
            $html[] = $preFieldHTML;
            $html[] = '<select name="'.$f->mode.$f->key.'" id="'.$f->mode.$f->key.'" class="form-control '.$fieldAttr['classes'].'" '.$fieldAttr['attributes'].'>';
            foreach($mf->templates as $template_id => $template){

                //do not show template name in list if specified to do so
                if(isset($template['hideInTemplateList']) && $template['hideInTemplateList']==1)$hide=1; else $hide=0;

                //select the current template
                //if no template name is defined in $f->fieldData, the first one in the available template list will be used
                $selected = ($template_id == $f->fieldData['value'])?" selected=\"selected\"":"";
                $html[] = '<option value="'.$template_id.'"'.$selected.(($hide)?' class="hidden"':'').'>'.$template['template_name'].'</option>';
            }
            $html[] = '</select>';
            $html[] = $postFieldHTML;
            $html[] = ($showFieldDiv)?'</div>':'';
            $html[] = $postDivHTML;
            $html[] = '<span class="errorMsg col-lg-offset-4 col-lg-8"></span>';

        }
        if(isset($f->fieldData['noClearFix']) and $f->fieldData['noClearFix'] == '1');
        else $html[] = "<div class='clearfix'></div>";

        return implode(chr(10),$html);



    }


    /**
     * Process the field value prior saving it to the database, whenever inner value adaptation is required, for SQL strict compliance for example.
     * This is executed after 'preRecord' processing functions.
     * @param $fieldData this is the data array of the current field
     * @param $value this is the submited value
     * @return mixed
     */
    static public function onSave($fieldData, $value){
        return $value;
    }

    static public function getCSS(){return "";}
    static public function getTopJS(){return "";}
    static public function getBottomJS(){return "";}







}
