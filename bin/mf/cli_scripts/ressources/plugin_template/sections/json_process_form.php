<?php

        if($_REQUEST['action'] == '{formClassName}')
        {

            $formMgr = $mf->formsManager;

            //process the form
            if($form = $formMgr->processForm())
            {
                if(isset($mf->currentUser))
                {
                    $currentUser = $mf->currentUser;
                    $userGroup = $currentUser->getUserGroup();
                    $moduleAccess = ($currentUser->isAdmin() || (class_exists('mfUserGroup') && ($userGroup && $userGroup->getUserRight('{pluginKey}','allowEdit{moduleClassNameUcFirst}')==1)));

                    if($moduleAccess)
                    {
                        $sqlConditions = '';
                        $dateConditions = '';
                        $deletedCondition = '';
                        $matchCondition =' AND (';
                        $oneCond=false; //check if a condition has already been activated or not

                        //Processing form data
                        if(trim($form->data['keyword_filter']['value']) != '')
                            $sqlConditions .= ' AND MATCH(`{recordTitleField}`) AGAINST('.$pdo->quote($form->data['keyword_filter']['value'].'*').' IN BOOLEAN MODE)';

                        if(isset($form->data['deleted_filter']) && $form->data['deleted_filter']['value'] == '1')$deletedCondition = ' AND deleted=1';
                        else $deletedCondition = ' AND deleted=0';

                        $matchCondition .=')';
                        if($matchCondition ==' AND ()')$matchCondition ='';


                        $module = new {moduleClassName}();
                        $html = $module->listRecords($sqlConditions,$dateConditions,$deletedCondition.$matchCondition);


                        $html = str_replace("{subdir}",SUB_DIR,$html);
                        $html = str_replace("{module}",$_REQUEST['module'],$html);

                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "success", "message" : '.json_encode($html).'}');
                    }
                    else
                    {
                        $mf->db->closeDB();
                        die('{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','module_no_access')).'}');
                    }
                }
                else
                {
                    $mf->db->closeDB();
                    die('{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','session_expired')).'}');
                }
            }
            else
            {
                $mf->db->closeDB();
                die('{"jsonrpc" : "2.0", "result" : "error", "message" :  '.json_encode($l10n->getLabel('backend','form-process-error')).'}');
            }
        }