<?php
if ($_REQUEST['action'] == 'createTables') {
    //print_r($mf->pluginManager->records);


    $html[] = '<h3>' . $l10n->getLabel('installTool', 'create_tables') . '</h3>';
    $html[] = '<div style="margin:0 0 0 30px;width:800px;display:block;">

                                <div class="list-group">';

    foreach ($mf->pluginManager->plugins as $plugin) {

        $html[] = '
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>' . $plugin->getName() . '&nbsp;&nbsp;-&nbsp;&nbsp;<span style="color:black;font-size:14px;">' . $l10n->getLabel('installTool', 'version') . ': ' . $plugin->getVersion() . '</span></h3>
                        <p>' . $plugin->getDesc() . '</p></div>
                        <div class="panel-body">
                            ';

        if (isset($mf->pluginManager->records[$plugin->getKey()])) {
            //install plugin link
            $html[] = '<p><a class="installPluginLink" href="{subdir}/mf/install/index.php?action=createTablesForPlugin&plugin=' . $plugin->getKey() . '" ><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'create_all_tables_plugin') . '</a></p>';
            $html[] = '<strong>' . $l10n->getLabel('installTool', 'tables') . ' : </strong>';

            $html[] = '
                                        <div class="table-responsive">
                                            <table  class="table table-striped table-condensed table-hover">
                                                ';

            foreach ($mf->pluginManager->records[$plugin->getKey()] as $className => $recordItem) {

                if ($recordItem['createTable'])
                    $html[] = '<tr><th class="installTableThClassName" colspan="12">' . $className . '</th></tr>';



                require_once(DOC_ROOT . SUB_DIR . $recordItem['path']);
                $currentRecord =  new $className();

                if($currentRecord::$oneTablePerLocale == true) {
                    //one table per locale, show one line per table
                    //if the record maintains one table per locale

                    //save current locale
                    $currentLocale = $mf->info['data_editing_locale'] ;

                    //create a table for each locale
                    foreach ($config['frontend_locales'] as $locale => $localeName){
                        //switch the locale
	                    $mf->setDataEditingLanguage($locale);

                        eval('$tablename = ' . $className . '::getTableName();');

                        $html[] = '<tr>
                                                    <th class="installTableThTableName">' . $className::getTableName() .'</th>';

                        //create table link
                        if ($tableExists = tableExists($tablename))
                            $html[] = '<td class="installTableTdTable"><span class="green"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'table_exists') . '</span></td>';
                        else
                            $html[] = '<td class="installTableTdTable"><a href="{subdir}/mf/install/index.php?action=createTable&tableLocale='.$locale.'&plugin=' . $plugin->getKey() . '&record=' . $className . '" ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'create_table') . '</a></td>';

                        //import initialization data link
                        if ($tableExists && $className::$enableImportInitializationData)
                            $html[] = '<td class="installTableTdTable"><a href="{subdir}/mf/install/index.php?action=importTable&tableLocale='.$locale.'&plugin=' . $plugin->getKey() . '&record=' . $className . '" ><span class="glyphicon glyphicon-open-file" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'import_data') . '</a></td>';
                        else
                            $html[] = '<td class="installTableTdTable"></td>';

                        //$html[] = ' <td class="installTableTd"><!--empty--></td>';

                        //delete table link
                        if ($tableExists) $html[] = ' <td class="installTableTdTable"><a href="{subdir}/mf/install/index.php?action=deleteTable&tableLocale='.$locale.'&plugin=' . $plugin->getKey() . '&record=' . $className . '" ><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'delete_table') . '</a></td>';
                        else $html[] = ' <td class="installTableTdTable"></td>';
                        $html[] = '</tr>';
                    }

                    //restore current locale
	                $mf->setDataEditingLanguage($currentLocale);

                }
                else {
                    //regular case
                    eval('$tablename = ' . $className . '::getTableName();');

                    $html[] = '<tr>
                                                    <th class="installTableThTableName">' . $className::getTableName().'</th>';

                    //create table link
                    if ($tableExists = tableExists($tablename))
                        $html[] = '<td class="installTableTdTable"><span class="green"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'table_exists') . '</span></td>';
                    else
                        $html[] = '<td class="installTableTdTable"><a href="{subdir}/mf/install/index.php?action=createTable&plugin=' . $plugin->getKey() . '&record=' . $className . '" ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'create_table') . '</a></td>';

                    //import initialization data link
                    if ($tableExists && $className::$enableImportInitializationData)
                        $html[] = '<td class="installTableTdTable"><a href="{subdir}/mf/install/index.php?action=importTable&plugin=' . $plugin->getKey() . '&record=' . $className . '" ><span class="glyphicon glyphicon-open-file" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'import_data') . '</a></td>';
                    else
                        $html[] = '<td class="installTableTdTable"></td>';

                    //$html[] = ' <td class="installTableTd"><!--empty--></td>';

                    //delete table link
                    if ($tableExists) $html[] = ' <td class="installTableTdTable"><a href="{subdir}/mf/install/index.php?action=deleteTable&plugin=' . $plugin->getKey() . '&record=' . $className . '" ><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;' . $l10n->getLabel('installTool', 'delete_table') . '</a></td>';
                    else $html[] = ' <td class="installTableTdTable"></td>';
                    $html[] = '</tr>';
                }

            }
            $html[] = '
                                                
                                            </table>
                                        </div>';
        }
        $html[] = '</div>';
        $html[] = '</div>';
    }

}

else if ($_REQUEST['action'] == 'createTablesForPlugin') {

    $pluginClassName = $_REQUEST['plugin'];
    $pluginManager->createTables($pluginClassName, $html);

}

else if ($_REQUEST['action'] == 'createTable') {

    $pluginKey = $_REQUEST['plugin'];
    $recordClassName = $_REQUEST['record'];
    $locale = (isset($_REQUEST['tableLocale']))?$_REQUEST['tableLocale']:$mf->getLocale();

    $pluginManager->createTable($pluginKey, $recordClassName, $locale, $html);

}

else if ($_REQUEST['action'] == 'importTable') {

    $pluginKey = $_REQUEST['plugin'];
    $recordClassName = $_REQUEST['record'];
    $locale = (isset($_REQUEST['tableLocale']))?$_REQUEST['tableLocale']:$mf->getLocale();

    $html[] = '<div style="margin:0 0 30px 30px;width:800px;display:block;">';
    $pluginManager->importTable($pluginKey, $recordClassName, $html);

    $html[] = '</div>';

}

else if ($_REQUEST['action'] == 'deleteTable') {
    $pluginKey = $_REQUEST['plugin'];
    $recordClassName = $_REQUEST['record'];
    $locale = (isset($_REQUEST['tableLocale']))?$_REQUEST['tableLocale']:$mf->getLocale();

    if ($pluginKey == 'system') {

    } else {
        //save current locale
        $currentLocale = $mf->info['data_editing_locale'] ;
        //alter the locale
	    $mf->setDataEditingLanguage($locale);
        //create the table
        $pluginManager->deleteTable($pluginKey, $recordClassName, $html);

        //restore current locale
	    $mf->setDataEditingLanguage($currentLocale);


    }
}

else if ($_REQUEST['action'] == 'pluginConfiguration') {

    foreach ($mf->pluginManager->modulesByName as $pluginKey => $pluginInstance) {
        $html[] = '<a href="{subdir}/mf/install/index.php?action=pluginSetup&plugin=' . $pluginKey . '" class="list-group-item">
                                <h4 class="list-group-item-heading">' . $pluginKey . '</h4>';

        $html[] = '</a>';
    }

}

else if ($_REQUEST['action'] == 'pluginSetup') {

    $plugin = $_REQUEST['plugin'];

    if (isset($pluginManager->plugins[$plugin])) {
        $html[] = '<div><h4 class="list-group-item-heading">' . $plugin . '</h4>';

        $html[] = $pluginManager->plugins[$plugin]->setup();

        $html[] = '</div>';
    } else $html[] = '<p>Plugin ' . $plugin . ' not found</p>';

}

