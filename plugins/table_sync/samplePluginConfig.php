<?php

/**
 * Configuration du plugin table_sync pour Dynamo
 *
 * Long Description
 */

$GLOBALS['config']['plugins']['table_sync'] = array(
    //mot de passe de l'hôte courant employé pour communiquer avec les autres hôtes
    'local_password' => 'abcdefgh123456',

	//Nombre de jours d'attente avant la purge automatique des entrées des objets InSync, OutSync, SqlInSync, SqlOutSync
	'purge-delay' => 60,

	//enregistre-t-on les résultats des requêtes SQL dans les logs SqlInSync et SqlOutSync ? Risque de générer beaucoup de données si activé
	'logSqlResults' => false,

    //adresses IP et mots de passes attendus de la part des hôtes distants.
    //pour révoquer un accès, modifiez ou supprimez son mot de passe
    'remote_hosts' => array(

    	'fr.mywebsite.com' => array(
		    'ip' => '*',//adresse IP ou '*' pour autoriser tous les hôtes
		    'password' => 'fr_abcdefgh123456',
		    'interface_url' => 'https://fr.mywebsite.com/mf_websites/mywebsite/plugins/table_sync/syncServer.php',
	    ),
	    'en.mywebsite.com' => array(
		    'ip' => '192.168.1.1',//adresse IP ou '*' pour autoriser tous les hôtes
		    'password' => 'en_abcdefgh123456',
		    'interface_url' => 'https://en.mywebsite.com/mf_websites/mywebsite/plugins/table_sync/syncServer.php',
	    ),
    ),


    //Ajout des hooks sur les objets pour déclencher les appels de synchro de table sur les triggers, c'est à dire lorsque les objets sont créés / modifiés / supprimés
	//Les enregistrements sont alors synchronisés automatiquement avec les sites précisés dans la règle indiquée dans le champ 'parameters'
    'hooks' => [

    	//utilisateurs backend, la modif d'un utilisateur le synchronise avec tous les sites simultanément
    	array(
		    'triggers' => 'postCreate,postStore,postDelete,postRestore',
		    'recordClass' => 'mfUser',
		    'callback' => "SyncManager::syncObject",
		    'parameters' => array('www_out_generic', false),
		    'locales' => ['fr','en']
	    ),

	    //niveaux
	    array(
		    'triggers' => 'postCreate,postStore,postDelete,postRestore',
		    'recordClass' => 'Niveau',
		    'callback' => "SyncManager::syncObject",
		    'parameters' => array('www_out_gen_fr', false),
		    'locales' => ['fr']
	    ),
	    array(
		    'triggers' => 'postCreate,postStore,postDelete,postRestore',
		    'recordClass' => 'Niveau',
		    'callback' => "SyncManager::syncObject",
		    'parameters' => array('www_out_gen_en', false),
		    'locales' => ['en']
	    ),

    ],

	//règles pour les données synchronisées en sortie
    'out_rules' => array(

	    //règle générique synchronisant un objet vers tous les sites web simultanément : par exemple quand on ajoute un utilisateur au backend, celui-ci a alors accès aux différents sites également.
	    'www_out_generic' => array(

		    //liste des hôtes distants destinataires de cette règle
	        'destinations' => array('fr.mywebsite.com','en.mywebsite.com'),

	        // Sélectionne uniquement les enregistrements correspondant à la locale indiquée.
		    // Commenter ou laisser vide si la règle s'applique aux enregistrements de toutes les locales.
	        'match_locales' => array(),

	        //si certains champs doivent changer de nom, indiquer les correspondance sous la forme 'nom_champ_remote' => 'nom_champ_local'
	        //sinon le nom du champ est conservé
	        //généralement on essayera plutôt d'avoir un champ correct dès l'envoi en utilisant le 'remap_fields' des send_rules
		    //attention, si le champ est remapé, les keyProcessor s'appliquent alors sur le nom du champ cible
	        'remap_fields' => array(
		        //'first_name' => 'last_name',
		        //'last_name' => 'first_name',
	        ),

	        //les champs suivants ne seront pas envoyés
	        //'filter_fields' => array('field1','field2',...),
	        'filter_fields' => array(),

	        //indiquer les fonctions de pré-traitement qui s'appliquent pour mettre au format un champ avant de l'enregistrer dans l'objet local
	        //préciser sous la forme 'nom_champ_local' => 'nom_objet::nom_fonction'
	        'key_processors' => array(
	        	//'last_name' => 'mfUser::nameUC',
	        ),

	        //la fonction qui sera invoquée pour traiter les données envoyées ( exemple de profil : static function sendOutRequest($outSyncObject){} )
	        //créez une fonction personnalisée pour envoyer du XML plutôt que du JSON par exemple
	        'sendFunc' => 'SyncManager::jsonOutRequest',
        ),






	    //règles génériques, synchronisant un enregistrement vers un seul site web à la fois (celui correspondant à la locale concernée)
	    'www_out_gen_fr' => array(
		    'destinations' => array('fr.mywebsite.com'),
			'match_locales' => array('fr'),
			'remap_fields' => array(),
		    'filter_fields' => array(),
		    'key_processors' => array(),
		    'sendFunc' => 'SyncManager::jsonOutRequest',
	    ),
	    'www_out_gen_en' => array(
		    'destinations' => array('en.mywebsite.com'),
		    'match_locales' => array('en'),
		    'remap_fields' => array(),
		    'filter_fields' => array(),
		    'key_processors' => array(),
		    'sendFunc' => 'SyncManager::jsonOutRequest',
	    ),


    ),

    'sql_out_rules' => array(

	    //nom de la règle
	    'sql_out_fr' => array(

		    //liste des hôtes distants destinataires de cette règle
		    'destinations' => array('fr.mywebsite.com'),

		    //la fonction qui sera invoquée pour traiter les données envoyées ( exemple de profil : static function sendOutRequest($outSyncObject){} )
		    //créez une fonction personnalisée pour envoyer du XML plutôt que du JSON par exemple
		    'sendFunc' => 'SyncManager::jsonSqlOutRequest',
	    ),
	    'sql_out_en' => array(
		    'destinations' => array('en.mywebsite.com'),
		    'sendFunc' => 'SyncManager::jsonSqlOutRequest',
	    ),



    ),

	//règles de translation pour les objets reçus
    'in_rules' => array(

    	//retours banque Monetico
	    'bankFeedback_www_fr' => array(

		    // liste des hôtes distants auxquels s'applique cette règle.
		    // Si plusieurs hôtes sont spécifiés, les données seront reçues depuis chacun de ces hôtes
		    'sources' => array('fr.mywebsite.com'),

		    //applique la locale définie à l'enregistrement entrant
		    'set_locale' => 'fr',

		    //nom de la classe sur le système distant
		    'remote_table' => 'mf_monetico_bank_feedback',

		    //nom de la classe sur le système local
		    'local_table' => 'mf_monetico_bank_feedback_fr',

		    //optionnel : si certains champs doivent changer de nom, indiquer les correspondance sous la forme 'nom_champ_remote' => 'nom_champ_local'
		    //sinon le nom du champ est conservé
		    //généralement on essayera plutôt d'avoir un champ correct dès l'envoi en utilisant le 'remap_fields' des send_rules
		    //attention, si le champ est remapé, les keyProcessor s'appliquent alors sur le nom du champ cible
		    'remap_fields' => array(),

		    //optionnel : les champs suivants ne seront pas envoyés
		    'filter_fields' => array(),

		    //optionnel : indiquer les fonctions de pré-traitement qui s'appliquent pour mettre au format un champ avant de l'enregistrer dans l'objet local
		    //préciser sous la forme 'nom_champ_local' => 'nom_objet::nom_fonction'
		    'key_processors' => array(
			    'creation_date' => 'SyncManager::checkCreateModDate',
			    'modification_date' => 'SyncManager::checkCreateModDate',
			    'start_time' => 'SyncManager::checkNullDate',
			    'end_time' => 'SyncManager::checkNullDate',
			    'date' => 'SyncManager::checkNullDate',
		    ),

		    //optionnel : la fonction qui sera invoquée pour traiter les données reçues
		    //créez une fonction personnalisée pour recevoir du XML plutôt que du JSON par exemple
		    'recvFunc' => 'SyncManager::jsonInRequest',
	    ),
	    'bankFeedback_www_en' => array(
		    'sources' => array('en.mywebsite.com'),
		    'set_locale' => 'en',
		    'remote_table' => 'mf_monetico_bank_feedback',
		    'local_table' => 'mf_monetico_bank_feedback_en',
		    'key_processors' => array(
			    'creation_date' => 'SyncManager::checkCreateModDate',
			    'modification_date' => 'SyncManager::checkCreateModDate',
			    'start_time' => 'SyncManager::checkNullDate',
			    'end_time' => 'SyncManager::checkNullDate',
			    'date' => 'SyncManager::checkNullDate',
		    ),
	    ),

    ),
);

