<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

include 'ExceptionThrower.php';

/**
 * Gives access to a database using MindFlow's configuration parameters
 *
 * The dbManager aims to offer minimalistic database access tools. These summup essentially to open the database, close it and check is a table exists.
 * The opened database object is stored in the variable $mf->db.
 * We didn't want to implement a full database API because Mindflow relies on PDO which is already a good database API.
 * Also we wanted to let all the possible freedom to the developper for database access.
 * If you need to use several databases concurrently, create one dbManager per database and pass the customized following arguments to the constructor for each dbManager : $dbHost, $dbPort, $dbName, $dbUsername, $dbPassword, $forceUtf8
 *
 */
class dbManager{

    var $pdo;
    var $host;
    var $forceUtf8;
    var $dbHost;
    var $dbPort;
    var $dbName;
    var $dbUsername;
    var $dbPassword;

    function __construct(){
        $argv = func_get_args();
        switch( func_num_args() ) {
            //constructor with DB parameters supplied as arguments
            case 6:
                self::getDbFromArgs( $argv[0], $argv[1], $argv[2], $argv[3], $argv[4], $argv[5] );
                break;
            //default constructor
            case 0:
            default:
                self::getDbFromConfig();
        }

    }

    private function getDbFromConfig(){
        global $mf,$config,$logger;

        $texteConnexion = $mf->l10n->getLabel('database','connect_db');
        //$logger->info(str_replace("{db_name}",$config['db_name'],$texteConnexion));

        $config = $config;
        $this->dbHost = $config['db_host'];
        $this->dbPort = $config['db_port'];
        $this->dbName = $config['db_name'];
        $this->dbUsername = $config['db_username'];
        $this->dbPassword = $config['db_password'];
        $this->forceUtf8 = (isset($config['db_force_utf8']) && $config['db_force_utf8'] == true)?';charset=UTF8':'';
    }

    private function getDbFromArgs($dbHost, $dbPort, $dbName, $dbUsername, $dbPassword, $forceUtf8 = true){
        $this->dbHost = $dbHost;
        $this->dbPort = $dbPort;
        $this->dbName = $dbName;
        $this->dbUsername = $dbUsername;
        $this->dbPassword = $dbPassword;
        $this->forceUtf8 = ($forceUtf8 == true)?';charset=UTF8':'';
    }

    function openDB() {
        global $mf,$l10n,$config,$logger;

        $config = $config;

        //force catching PHP warnings due to PDO failure to report an Exception in case the db host address is invalid
        ExceptionThrower::Start();

        try
        {
            //enable PDO errors (not shown by default). See http://php.net/manual/fr/pdo.error-handling.php for more info
            $opts = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
            //$opts = array(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

            if($this->dbHost != 'mysql.sample-website.com') {
                $dsn = 'mysql:host=' . $this->dbHost . ';port=' . $this->dbPort . ';dbname=' . $this->dbName . $this->forceUtf8;

                $this->pdo = new PDO($dsn, $this->dbUsername, $this->dbPassword, $opts);

            }
            else{
                echo $l10n->getLabel('database','go_install_tool').'<br />'.chr(10);
                $logger->emergency($l10n->getLabel('database','go_install_tool'));
                die();
            }
        }
        catch(PDOException  $e)
        {
            echo '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>';
            if($e->getCode() == 1045){
                echo $l10n->getLabel('database','connexion_error').'<br />'.chr(10);
                $logger->emergency($l10n->getLabel('database','connexion_error'));
            }
            else{
                //invalid db host address exception
                if($e->getCode()=='2002') echo '<strong>'.$l10n->getLabel('database','host_error').'</strong><br />'.chr(10);

                echo $l10n->getLabel('main','error').$e->getMessage().'<br />'.chr(10);
                echo $l10n->getLabel('main','number').$e->getCode().'<br />'.chr(10);
                $logger->emergency($l10n->getLabel('main','error').$e->getMessage());
                $logger->emergency($l10n->getLabel('main','number').$e->getCode());
            }
            echo "</body></html>";
            die();
        }

        ExceptionThrower::Stop();

    }

    function closeDB() {
        //echo "Mindflow closing MYSQL connexion";
        unset($this->pdo);
    }


    static function SQLTableExists($tableName){
        global $pdo;

        if ($pdo->query("SHOW TABLES LIKE '" . $tableName . "'")->rowCount() > 0) return true;
        else return false;
    }

}

