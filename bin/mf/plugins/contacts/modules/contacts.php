<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//include custom contact class or base contact class by default
/*if(isset($config['plugins']['contacts']['contact-class']))
    require_once DOC_ROOT.SUB_DIR.$config['plugins']['contacts']['contact-class'];
else require_once DOC_ROOT.SUB_DIR.'/mf/plugins/contacts/records/contact.php';*/

//mailer class
require_once DOC_ROOT.SUB_DIR.'/mf_librairies/Swift-5.4.5/lib/swift_required.php';

class contacts implements module
{
    private $moduleKey = 'contacts';
    private $moduleType = 'backend,frontend';
    private $action='list';

    var $pageOutput;





    function prepareData(){
        global $mf,$l10n,$config;

        $this->pageOutput = '';
        $language = $mf->getFrontendLanguage();

        if($mf->mode == 'frontend'){


            //$mf->addBottomJs('<script src="'.SUB_DIR.$config['backend_template'].'/js/backend-bottom.js"></script>', true, false);

            $display = (isset($config['plugins']['contacts']['display-form']))?$config['plugins']['contacts']['display-form']:'true';

                if($display == 'true'){
                    //displaying form
                    $contact = new contact();
                    $contactFormMgr = $mf->formsManager;

                    //$contactFormMgr->initFormCssAndJS();

                    $this->pageOutput = '<div id="contactForm">';
                    $this->pageOutput .= $contactFormMgr->editRecord($contact,$this->moduleKey, $subModuleName = '', $showSaveButton=true, $showSaveCloseButton=false, $showPreviewButton=false, $showCloseButton=false, $ajaxEditing = true,
                        array(
                            'JSCallbackFunctionAfterSubmit'=>'callBackSubmit',
                            'saveButtonLabel' => $l10n->getLabel('contacts','send'),
                            'PHPCallbackFunctionAfterSubmit'=>'contacts::emailContact'
                        )
                    );
                    $this->pageOutput .= '</div>';

                    ////document.location = "'.((!empty($_SERVER['HTTPS'])) ? "https://" : "http://") . $_SERVER['SERVER_NAME'].'";

                    $successMessage = (isset($config['plugins']['contacts']['email-confirm'][$language]))?$config['plugins']['contacts']['email-confirm'][$language]:$l10n->getLabel('contacts','successMessage');
                    $this->pageOutput .= '
                    <script>
                    function callBackSubmit(status){
                        if(status=="success"){
                            $("#contactForm").html("<div class=\"alert alert-success\" role=\"alert\">'.$successMessage.'</div>");
                        }
                    }
                    </script>';

                    }
            //}

        }
        else if($mf->mode == 'backend'){

            if(isset($mf->currentUser)){
                //check access rights
                $this->userGroup = $mf->currentUser->getUserGroup();
                $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('contacts','allowEditContacts')==1)));

                if($this->moduleAccess){
                    //add the plugin to the backend menus
                    $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=contacts"><span class="glyphicon glyphicon-menu glyphicon-envelope" aria-hidden="true"></span>'.$l10n->getLabel('contacts','menu_title').'</a>','datas');

                    if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                        $this->localMenu ='<li rel="'.$this->moduleKey.'" class="active"><a href="{subdir}/mf/index.php?module='.$this->moduleKey.'">{menu-title}</a></li>';

                        //breadcrumb
                        $this->section = '';//$l10n->getLabel('backend','datas');
                        $this->moduleName = '<span class="glyphicon glyphicon-menu glyphicon-envelope" aria-hidden="true"></span>'.$l10n->getLabel('contacts','menu_title');
                        $this->subModuleName = "";

                        $this->breadcrumb = $this->section.(($this->moduleName != '')?' / '.$this->moduleName:'').(($this->subModuleName != '')?' / '.$this->subModuleName:'');


                        if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                        switch($this->action){
                            case "list":
                               $this->listRecords();
                               break;
/*
                            case "create":
                                $this->pageOutput .= $this->createContact();
                                break;

                            case "edit":
                                //case edit requested by some module as a GET url parameter
                                if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                    $this->pageOutput .= $this->editContact(intval($_GET['uid']));
                                }
                                //case we just re-edit the last saved record
                                break;


                            case "delete":
                                $this->pageOutput .= $this->deleteContact($_REQUEST['uid']);
                                break;*/
                        }
                    }
                }
                else {
                    $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
                    $this->localMenu = '';
                }
            }
        }


    }

    function render(&$mainTemplate){
        global $mf,$l10n;

        if($mf->mode == 'frontend'){

            $mainTemplate = str_replace("{plugin-contacts}",$this->pageOutput,$mainTemplate);

        }
        else if($mf->mode == 'backend'){


            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){
                global $mf,$l10n;

                $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/contacts/ressources/templates/simple-module.html');
                $moduleBody = str_replace("{module_body}",$this->pageOutput,$moduleBody);

                $moduleBody = str_replace("{local-menu}",$this->localMenu,$moduleBody);
                $moduleBody = str_replace("{menu-title}",$l10n->getLabel('contacts','menu_title'),$moduleBody);

                $mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);

                //breadcrumb
                $mainTemplate = str_replace("{section}",$this->section,$mainTemplate);
                $mainTemplate = str_replace("{module-name}",$this->moduleName,$mainTemplate);
                $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);
                $mainTemplate = str_replace("{breadcrumb}",$this->breadcrumb,$mainTemplate);
            }
        }

    }


    function getType(){
        return $this->moduleType;
    }

    function listRecords($sqlConditions = ''){
        global $config;

        $contact = new Contact();
        $this->pageOutput .= $contact->showRecordEditTable(
            array(
                'SELECT' => 'first_name, last_name, email, subject, creation_date',
                'FROM' => '',
                'JOIN' => '',
                'WHERE' => "mf_contacts.deleted = '0'",
                'ORDER BY' => 'creation_date',
                'ORDER DIRECTION' => 'DESC',
            ),
            'contacts',
            '',
            'subject',
            array('creation_date'=>'$this->formatDateTime'),
            NULL,
            array('view'=>1,'edit'=>0,'delete'=>0),
            array(
                'ajaxActions' => 1,
                'showNumRowsPerPageSelector' => true,
                'showResultsCount' => true,
                'listRecordsInAllLanguages' => (isset($config['plugins']['contacts']['listRecordsInAllLanguages']))?$config['plugins']['contacts']['listRecordsInAllLanguages']:true,
                'debugSQL'=>0,
                'columnClasses' => array(
                    'first_name' => 'hidden-xs hidden-sm',
                    'last_name' => 'hidden-xs',
                    'email' => 'hidden-xs',
                    'creation_date' => 'hidden-xs',
                ),
            ),
            null,
            'contacts'
        );
    }

    /**
     * Send a contact request by email
     * @param $status
     * @param $recordClass
     * @param $recordUid
     * @return int|string returns the count of sent mail if successfull, else return the error message
     */
    static function emailContact($status,$contact){
        global $mf,$l10n,$config;

        // Create the message
        $message = Swift_Message::newInstance()
            ->setSubject($config['plugins']['contacts']['email-subject'])
            ->setFrom($config['plugins']['contacts']['email-from'])
            //->setFrom($contact->data['email']['value'])
            //->setTo(array($contact->data['recipient']['value'] => $contact->data['first_name']['value'].' '.$contact->data['last_name']['value']))
            ->setTo(array($config['plugins']['contacts']['email-destination'] => $config['plugins']['contacts']['email-destination-name']))
            ->setCharset('utf-8');

        //load email template
        if(isset($config['plugins']['contacts']['email-template']))
            $emailTemplate = file_get_contents(DOC_ROOT.SUB_DIR.$config['plugins']['contacts']['email-template']);
        else
            $emailTemplate = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/contacts/ressources/templates/email.html');

        //put email intro
        $emailTemplate = str_replace("{email-intro}",$config['plugins']['contacts']['email-intro'],$emailTemplate);

        //write contact information
        $html = array();
        //$contact = new contact();
        //$contact->load($recordUid);
        $tabs = array_keys($contact->showInEditForm);
        $displayFields = $contact->showInEditForm[$tabs[0]];
        foreach($displayFields as $fieldKey){
            $html[] = '<tr>';
            $html[] = '<td class="tdLabel"><strong>'.$l10n->getLabel('contact',$fieldKey).' : </strong></td>';
            if($fieldKey != 'email')$html[] = '<td class="tdValue">'.nl2br($contact->data[$fieldKey]['value']).'</td>';
            else {
                $bodyLines = explode(PHP_EOL, $contact->data['message']['value']);
                $bodyText='%0D%0A%0D%0A';
                foreach($bodyLines as $line){
                    $bodyText .= '> '.$line.'%0D%0A';
                }
                $html[] = '<td class="tdValue"><a href="mailto:'.$contact->data[$fieldKey]['value'].'?subject='."Re: ".htmlentities(utf8_decode($contact->data['subject']['value'])).'&body='.htmlentities(utf8_decode($bodyText)).'">'.$contact->data[$fieldKey]['value'].'</a></td>';
            }
            $html[] = '</tr>';
        }
        $emailTemplate = str_replace("{contact-info}",implode(chr(10),$html),$emailTemplate);

        //put client logo
        $image = '<p><img src="' .$message->embed(Swift_Image::fromPath(DOC_ROOT.SUB_DIR.$config['plugins']['contacts']['email-image'])).'" alt="Image" /></p>';
        $emailTemplate = str_replace("{email-image}",$image,$emailTemplate);

        //form recording success. Send notification email
        $message->addPart($emailTemplate, 'text/html');

        // Create the Transport
        //$transport = Swift_MailTransport::newInstance();
        $transport = Swift_SmtpTransport::newInstance($config[$mf->getLocale()]['siteEmailSMTPServer'], $config[$mf->getLocale()]['siteEmailSMTPPort'])
            ->setUsername($config[$mf->getLocale()]['siteEmailSMTPUsername'])
            ->setPassword($config[$mf->getLocale()]['siteEmailSMTPPassword']);
        if(isset($config[$mf->getLocale()]['siteEmailSMTPEncryption']) && $config[$mf->getLocale()]['siteEmailSMTPEncryption']!=null)$transport->setEncryption($config[$mf->getLocale()]['siteEmailSMTPEncryption']);
        // Create the Mailer using created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        $logMails = (isset($config[$mf->getLocale()]['enableEmailLog']) && $config[$mf->getLocale()]['enableEmailLog']== true);
        if($logMails){
            $logger = new Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
        }

        try{
            // Send the message
            $numSent = $mailer->send($message);
        }
        catch (Exception $e){
            return $e->getMessage();
        }

        if($logMails){
            //the key for the first entry of this associative array is the "from" email
            reset($config['plugins']['contacts']['email-from']);
            $from = key($config['plugins']['contacts']['email-from']);
            logMails(
                $from,
                $config['plugins']['contacts']['email-destination'],
                $config['plugins']['contacts']['email-subject'],
                $logger->dump()
            );
        }

        return $numSent;
    }


}

