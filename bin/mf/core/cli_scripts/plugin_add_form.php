<?php

/**
 * This wizards assists you in creating a new form.
 * It is possible to specify in the command line a config file featuring the preconfigured used variables in order to avoid having to seize them at every run when configuring the wizard.
 * In this cas, call the script using the following syntax :
 *
 * php plugin_add_form.php config_file.php
 *
 * See file sample-configs/agPlugin_config.php for the available variables to configure.
 */

//http://php.net/manual/en/features.commandline.php

require_once "colors.php";
require_once "functions.php";

// Create new Colors class
$colors = new Colors();

if(isset($argv[1])){
    echo chr(10);
    if(is_file ( $argv[1] )) {
        echo $colors->getColoredString("config file specified, loading defaults from the file...", 'cyan', null);
        include $argv[1];
        echo $colors->getColoredString("done", 'cyan', null).chr(10).chr(10);
    }
    else echo $colors->getColoredString("Specified config.php file not found, running 100% interactive mode.", 'dark_gray', null).chr(10).chr(10);
}


echo "Class name of the form you want to create ? [myForm] : ";
$prefix = chr(10);
if(!isset($formClassName)){
    $formClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($formClassName=='')$formClassName = 'myForm';
echo $prefix.$colors->getColoredString("Form class name will be \"".$formClassName."\"", 'cyan', null).chr(10).chr(10);

echo "Class name of the record you want to filter display with the form ? [myPlugin] : ";
$prefix = chr(10);
if(!isset($recordClassName)){
    $recordClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($recordClassName=='')$recordClassName = 'myPlugin';
echo $prefix.$colors->getColoredString("Filtered record class will be \"".$recordClassName."\"", 'cyan', null).chr(10).chr(10);


echo "In the record definition, what is the column name of the record's title field ? [title] : ";
$prefix = chr(10);
if (!isset($recordTitleField)) {
    $recordTitleField = trim(fgets(STDIN));
    $prefix = '';
}
if ($recordTitleField == '') $recordTitleField = 'title';

echo $prefix . $colors->getColoredString("Record's title field is \"" . $recordTitleField . "\"", 'cyan', null) . chr(10) . chr(10);





echo "Base ISO2 locale for localized files ? [en] : ";
$prefix = chr(10);
if(!isset($defaultLocale)){
    $defaultLocale = trim(fgets(STDIN));
    $prefix = '';
}
if($defaultLocale=='')$defaultLocale = 'en';

echo $prefix.$colors->getColoredString("Plugin default locale will be \"".$defaultLocale."\"", 'cyan', null).chr(10).chr(10);

echo "Folder name of the website where the form should be created (in /mf_websites) [default] : ";
$prefix = chr(10);
if(!isset($websiteFolderName)){
    $websiteFolderName = trim(fgets(STDIN));
    $prefix = '';
}
if($websiteFolderName=='')$websiteFolderName = 'default';
echo $prefix.$colors->getColoredString("Record will be created in /mf_websites/".$websiteFolderName.'/plugins', 'cyan', null).chr(10).chr(10);

echo "Class name of the plugin holding the form ? [myPlugin] : ";
$prefix = chr(10);
if(!isset($pluginClassName)){
    $pluginClassName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginClassName=='')$pluginClassName = 'myPlugin';
echo $prefix.$colors->getColoredString("Plugin class will be named \"".$pluginClassName."\"", 'cyan', null).chr(10).chr(10);


echo "Folder name / plugin key of the plugin holding the record ? [$pluginClassName] : ";
$prefix = chr(10);
if(!isset($pluginDirName)){
    $pluginDirName = trim(fgets(STDIN));
    $prefix = '';
}
if($pluginDirName=='')$pluginDirName = $pluginClassName;
$pluginKey = $pluginDirName;

echo $prefix.$colors->getColoredString("Will create record in plugin folder /mf_websites/".$websiteFolderName."/plugins/".$pluginDirName, 'cyan', null).chr(10).chr(10);

$answer = '';

$acceptedValues = array('y','n','yes','no');
while (!in_array($answer, $acceptedValues)){
    echo "Will now create form files, ready to write ? [yes/no] : ";
    $answer = trim(fgets(STDIN));
}
if($answer=='y' || $answer=='yes'){
    echo chr(10).$colors->getColoredString("Now writing files...", 'green', null);

    //value for moving to mf root dir
    $chdir = "../.." ;

    //create records folder
    echo chr(10).$colors->getColoredString("Creating form directory ... ", 'green', null);
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName."/forms");
    echo $colors->getColoredString("Done.", 'green', null);

    //create record php file
    echo chr(10).$colors->getColoredString("Creating form class file ... ", 'green', null);
    $formContent = file_get_contents ("ressources/plugin_template/forms/myForm.php");
    $formContent = str_replace("{formClassName}",$formClassName,$formContent);
    //$formContent = str_replace("{recordClassName}",$recordClassName,$formContent);
    $formContent = str_replace("{pluginDirName}",$pluginDirName,$formContent);
    //$formContent = str_replace("{pluginKey}",$pluginKey,$formContent);
    $formContent = str_replace("{recordTitleField}",$recordTitleField,$formContent);
    //$formContent = str_replace("{SQLTableName}",$SQLTableName,$formContent);

    //finally, write plugin file
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/forms/'.$formClassName.'.php',$formContent);
    echo chr(10).$colors->getColoredString("Done.", 'green', null);

    //create language folder
    createDir($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName."/languages/forms");

    //create the record's l10n file
    $l10nRecordContent = file_get_contents("ressources/plugin_template/languages/forms/myForm_l10n.php");
    $l10nRecordContent = str_replace("{defaultLocale}",$defaultLocale,$l10nRecordContent);

    writeFile($chdir.'/mf_websites/'.$websiteFolderName.'/plugins/'.$pluginDirName.'/languages/forms/'.$formClassName.'_l10n.php',$l10nRecordContent);



    echo chr(10).$colors->getColoredString("Updating module ".$chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php ... ', 'green', null);

    $moduleContent = file_get_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php');

    //add //[/module_include_form] statement
    $moduleIncludeForm = file_get_contents ("ressources/plugin_template/sections/module_include_form.php");
    $moduleIncludeForm = str_replace("{pluginDirName}",$pluginDirName,$moduleIncludeForm);
    $moduleIncludeForm = str_replace("{formClassName}",$formClassName,$moduleIncludeForm);

    $moduleContent = setSection('module_include_form',"module_include_form_".$formClassName,$moduleIncludeForm,$moduleContent, '');

    //add //[/module_insert_form] section
    // inserts form into the prepareData() record listing instruction set
    $moduleInsertForm = file_get_contents ("ressources/plugin_template/sections/module_insert_form.php");
    $moduleInsertForm = str_replace("{formClassName}",$formClassName,$moduleInsertForm);
    $moduleInsertForm = str_replace("{pluginDirName}",$pluginDirName,$moduleInsertForm);
    $moduleInsertForm = str_replace("{pluginClassName}",$pluginClassName,$moduleInsertForm);
    $moduleInsertForm = str_replace("{recordClassName}",$recordClassName,$moduleInsertForm);
    $moduleContent = setSection("module_insert_form","module_insert_form_".$formClassName,$moduleInsertForm,$moduleContent, chr(9).chr(9).chr(9).chr(9).chr(9).chr(9).chr(9));

    //write the module file
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/modules/'.$moduleClassName.'.php',$moduleContent);
    echo $colors->getColoredString("Done.", 'green', null);


    echo chr(10).$colors->getColoredString("Inserting form processing code in file  ".$chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/'.$pluginClassName.'-json-backend.php ... ', 'green', null);

    $jsonContent = file_get_contents ($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/'.$pluginClassName.'-json-backend.php');

    $jsonProcessForm = file_get_contents ("ressources/plugin_template/sections/json_process_form.php");
    $jsonProcessForm = str_replace("{formClassName}",$formClassName,$jsonProcessForm);
    $jsonProcessForm = str_replace("{moduleClassNameUcFirst}",ucfirst($moduleClassName),$jsonProcessForm);
    $jsonProcessForm = str_replace("{recordTitleField}",$recordTitleField,$jsonProcessForm);
    $jsonProcessForm = str_replace("{moduleClassName}",$moduleClassName,$jsonProcessForm);
    $jsonProcessForm = str_replace("{pluginKey}",$pluginKey,$jsonProcessForm);
    $jsonContent = setSection("add_json_action","json_process_form_".$formClassName,$jsonProcessForm,$jsonContent, chr(9).chr(9));

    //write the json file
    writeFile($chdir."/mf_websites/".$websiteFolderName."/plugins/".$pluginDirName.'/'.$pluginClassName.'-json-backend.php',$jsonContent);
    echo $colors->getColoredString("Done.", 'green', null);


}
else echo chr(10).$colors->getColoredString("Aborting write. Done.", 'red', null).chr(10).chr(10);

echo chr(10);