<?php
/**
 * This file contains the localised labels for the page templates form fields displayed in the backend UI.
 * It must feature a label for every template field, the label name has to match the field name.
 * To load the file, use l10nManager::loadL10nFile('templates',$GLOBALS['config']['templates_directory'].'/languages/templates_l10n.php');
 * The label values are retreived automatically when the page template forms are displayed. In another context, you may use $mf->l10n->getLabel('templates','label_name'); to retreive the value of a label.
 */


$l10nText = array(


    'fr' => array(
        //'page_title' => 'Titre de la page',
        'page_html' => 'Corps de texte de la page',
    ),

    'en' => array(
        //'page_title' => 'Page title',
        'page_html' => 'Page bodytext',
    ),

);