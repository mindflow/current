.. MindFlow documentation master file

Documentation du Framework MindFlow
====================================

Le projet MindFlow est hébergé sur GitLab à l'adresse `https://gitlab.com/mindflow/current <https://gitlab.com/mindflow/current>`_ 

.. toctree::
   :maxdepth: 2
   :caption: Contenu:

   licence
   introduction
   install
   fundamentals
   inside-mindflow
   forms-records
   websites-integration
   plugins
   scripts
   support
   acknowledgements

   
.. documentation des plugins


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
