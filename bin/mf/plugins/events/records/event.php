<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


class event extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_events';

    static $createTableSQL= "
        `category` varchar(255) NOT NULL DEFAULT 'escapade',
        `title` varchar(255) DEFAULT NULL,
        `text` text NOT NULL,
        `image` text NOT NULL,
    ";

    static $createTableKeysSQL="";

    function init(){

        parent::init();

        $this->data = array_merge($this->data, array(
            'category'   =>  array(
                'value' => '',
                'possibleValues' => array(
                    'escapade' => 'A l\'Escapade',
                    'alentours' => 'Aux alentours',
                ),
                'dataType' => 'select',
                'div_attributes' => array('class' => 'col-lg-3'),
            ),
            'title'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'text'   =>  array(
                'value' => '',
                'dataType' => 'rich_text',
                'div_attributes' => array('class' => 'col-lg-8'),
            ),
            'image' => array(
                'dataType' => 'files jpg,jpeg,gif,png',
                'value' => '',
                'processor' => 'imagesCommerces',
            ),


        ));



        $this->showInEditForm = array(
            'tab_event'=> array('category','title', 'text','image')
        );


    }




}


