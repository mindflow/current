<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'menu_title' => 'Contact requests management',
        'contact_recorded' => 'The contact has been successfully recorded',
        'contacts_list' => 'Contact requests list',
        'contacts_listing' => 'Browse contact requests',
        'editing' => 'Editing',
        'send' => 'Send',
        'successMessage' => 'Your message has been send. Thank you for your interest.',
    ),

    'fr' => array(
        'menu_title' => 'Gestion des demandes de contact',
        'contact_recorded' => 'Le contact a été enregistré avec succès',
        'contacts_list' => 'Liste des demandes de contact',
        'contacts_listing' => 'Lister les demandes de contact',
        'editing' => 'Edition',
        'send' => 'Envoyer',
        'successMessage' => 'Le message nous a été envoyé. Merci de votre intérêt.',
    ),
);