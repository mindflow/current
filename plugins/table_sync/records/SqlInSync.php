<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/records/dbRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

class SqlInSync extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_sync_sql_in';
    //static $oneTablePerLocale = true;

    static $createTableSQL= "
        #classe et données concernée
        `sql_request` text,
        `sql_result` longblob,
        
        #système source
        `source` varchar(255) DEFAULT '',
        
        #etat et résultat de l'opération
        `state` varchar(10) DEFAULT 'Inactive',
        `result` varchar(10) DEFAULT '',
        
        #log des opérations
        `local_log` longtext,
       
    ";

    static $createTableKeysSQL="";

    static $postFormHTML=array(); //important declaration, do not remove (would cause bug)


    function init()
    {
        parent::init();

        global $mf,$l10n;

        $this->data = array_merge($this->data, array(

	        'sql_request' => array(
		        'value' => '',
		        'dataType' => 'textarea 30 5',
		        'valueType' => 'text',
		        'div_attributes' => array('class' => 'col-md-8'),
	        ),
	        'sql_result' => array(
		        'value' => '',
		        'dataType' => 'textarea 30 5',
		        'valueType' => 'text',
		        'div_attributes' => array('class' => 'col-md-8'),
		        'noEcho' => 1,
	        ),
	        'source' => array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('')
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-3'),
                'editable' => false,
            ),
            'state' => array(
                'value' => 'Inactive',
                'dataType' => 'select',
                'possibleValues' => array(
                    'Receiving' => $l10n->getLabel('SqlInSync','Receiving'),
                    'Inactive' => $l10n->getLabel('SqlInSync','Inactive'),
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
                'editable' => false,
            ),
            'result' => array(
                'value' => '',
                'dataType' => 'select',
                'possibleValues' => array(
                    '' => '',
                    'Success' => $l10n->getLabel('SqlInSync','Success'),
                    'Failure' => $l10n->getLabel('SqlInSync','Failure'),
                ),
                'field_attributes' => array(
                    'class' => 'input-md',
                ),
                'div_attributes' => array('class' => 'col-md-2'),
                'editable' => false,
            ),
            'local_log' => array(
                'value' => '',
                'dataType' => 'textarea 50 8',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-md-8'),
                //'editable' => false,
            ),


        ));


        $this->showInEditForm = array(

            'tab_main'=>array(
	            'sql_request','sql_result','source','state','result','local_log',
            )
        );


        static::$postFormHTML[] = "<script></script>";

    }

	/**
	 * Quickly updates a column value in the database without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $name the column name in the database
	 * @param String $value the value to set
	 */
	function setField($name,$value){
		global $pdo, $logger;

		try{
			$sql = "UPDATE " . self::getTableName() . " SET ".$name."=" . $pdo->quote( $value ) . " WHERE uid=" . $this->data['uid']['value'] . ";";
			$pdo->query( $sql );

			$this->data[$name]['value'] = $value;
		}
		catch(PDOException $e){
			$logger->critical("SqlInSync setField PDOException code=".$e->getCode()." message=".$e->getMessage());
		}

	}

	/**
	 * Quickly updates the local_log field without storing the whole object. The object must have an uid prior executing this function.
	 * @param String $logMessage text of the log entry
	 */
	function addToLocalLog($logMessage){
		global $pdo;

		$now = new DateTime('NOW');
		$nowStr = $now->format('d/m/Y H:i:s');

		$this->data['local_log']['value'] .= $nowStr.' : '.$logMessage;
		$sql = "UPDATE " . self::getTableName() . " SET local_log=" . $pdo->quote($this->data['local_log']['value']) . " WHERE uid=" . $this->data['uid']['value'] . ";";
		//echo $sql.chr(10);
		$pdo->query( $sql );
	}

	static function getState($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;

			$sec = getSec(array('action' => 'replaySqlIn', 'uid' => $row['uid']));
			$resetSec = getSec(array('action' => 'resetState', 'uid' => $row['uid'], 'record_class' => 'SqlInSync'));

			switch($value){
				case 'Inactive':
					return '<span id="state_'.$row['uid'].'"><span class="darkgrey">'.$l10n->getLabel('SqlInSync','Inactive').' &nbsp;<a onclick="replay('.$row['uid'].',\''.$sec->hash.'\');" class="play" title="'.$l10n->getLabel('SqlInSync','replay').'"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a></span></span>';

				case 'Sending':
					return '<span id="state_'.$row['uid'].'"><span class="orange">'.$l10n->getLabel('SqlInSync','Receiving').'</span> &nbsp;<img src="'.SUB_DIR.'/mf/backend/templates/mf-default/img/small-loading.gif" />&nbsp;<a onclick="resetState('.$row['uid'].',\'SqlInSync\',\''.$resetSec->hash.'\');" class="play" title="'.$l10n->getLabel('FullSync','reset').'"><span class="glyphicon glyphicon-stop" aria-hidden="true"></span></a></span>';
			}
		}
		else return '<span id="state_'.$row['uid'].'"></span>';
	}


	static function getResult($key,$value,$locale,$row){
		if($value != ''){
			global $l10n;
			switch($value){
				case 'Success':
					return '<span id="result_'.$row['uid'].'"><span class="green">'.$l10n->getLabel('SqlInSync','Success').'</span></span>';

				case 'Failure':
					return '<span id="result_'.$row['uid'].'"><span class="red">'.$l10n->getLabel('SqlInSync','Failure').'</span></span>';
			}
		}
		else return '<span id="result_'.$row['uid'].'"></span>';
	}

}















