<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'menu_title' => 'Events management',
        'no_events' => 'There currently exists no event. You can <a href="{subdir}/mf/index.php?module=events&action=create">create an event</a>.',
        'event_recorded' => 'The event has been successfully recorded',
        'new_record' => 'Create an event',
    ),

    'fr' => array(
        'menu_title' => 'Gestion des évènements',
        'no_events' => 'Il n\'existe pas d\'évènements pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=events&action=create">créer un évènement</a>.',
        'event_recorded' => 'L\'évènement a été enregistré avec succès',
        'new_record' => 'Créer un évènement',
    ),
);