<?php

/*                                                                        *
 * This script belongs to the MindFlow framework.                         *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * Author : Mind2Machine                                                  *
 * The MindFlow project                                                   *
 *                                                                        */


$l10nText = array(
//TODO: FICHIER VITE COPIE, FILTRER LES VALEURS INUTILES

    'fr' => array(
        'keyword_filter' => 'Mot-clé : nom, email...',
        'deleted_filter' => 'Afficher les membres supprimés',

    ),
);

?>