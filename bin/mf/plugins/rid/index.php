<?php
require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/plugin.php';

class ressourceIdentifier implements plugin{

    function setup(){

    }

    function getName(){
        global $l10n;
        return $l10n->getLabel('ressourceIdentifier','name');
    }

    function getDesc(){
        global $l10n;
        return $l10n->getLabel('ressourceIdentifier','desc');
    }

    function getPath(){
        return str_replace(DOC_ROOT.SUB_DIR, '', dirname(__FILE__));
    }

    function getKey(){
        return "rid";
    }


    function getVersion(){
        return "1.0";
    }

    function getDependencies(){
        $dependencies = array();
        return $dependencies;
    }

    function init(){
        global $mf;
        $l10n = $mf->l10n;

        $relativePluginDir = $this->getPath();

        //load the translation files
        $l10n->loadL10nFile('backend', $relativePluginDir.'/languages/backendMenus_l10n.php');


        //inform MindFlow of the new dbRecords supplied by this plugin
        //[load_record_rid]
		$mf->pluginManager->addRecord('rid', 'rid', $relativePluginDir.'/records/rid.php');
		//[/load_record_rid]
		//{index_load_record}


        //load modules
        //{index_load_module}


        //add editing rights
        //{index_add_module_editing_right}

	    //add cron function for executing FullSyncs
	    $mf->addCronFunc('rid::cleanup();');

    }
}
$mf->pluginManager->addPluginInstance(new ressourceIdentifier());


