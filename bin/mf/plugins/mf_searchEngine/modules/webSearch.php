<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';

class webSearch implements module
{
    private $moduleKey = 'webSearch';
    private $pluginVersion = '1.0';
    private $moduleType = 'frontend';
    private $dependencies = '';

    var $introText='';
    var $searchForm='';

   
    function prepareData(){
        global $mf,$l10n,$pdo,$config;
        $this->bodytext='';

        $_SESSION['searchTerms'] = $this->searchTerms = isset($_REQUEST['q'])?$_REQUEST['q']:'';
      

        if(trim($this->searchTerms) !=''){

            $page = ( (isset($page))? $page : (isset($_REQUEST['page'])? intval($_REQUEST['page']) : 0 ) );
            $maxRecordsPerPage = $config['plugins']['mf_searchEngine']['results-per-page'];

            $sql = "SELECT COUNT(*) FROM mf_search_index WHERE MATCH (`title`,`digest`,`tags`) AGAINST (".$pdo->quote($this->searchTerms.'*')." IN BOOLEAN MODE);";
            $stmt = $pdo->query($sql);
            $totalRecordCount = $stmt->fetchColumn();

            $sqlNoLimit = "SELECT class_name, object_uid, title, digest, ressource_url, MATCH (`title`,`digest`,`tags`) AGAINST (".$pdo->quote($this->searchTerms.'*')." IN BOOLEAN MODE) AS score  FROM mf_search_index WHERE MATCH (`title`,`digest`,`tags`) AGAINST (".$pdo->quote($this->searchTerms.'*')." IN BOOLEAN MODE) ORDER BY score DESC";

            //paginated SQL request
            if ($page > 0){
                $sql = $sqlNoLimit." LIMIT ".($page*$maxRecordsPerPage).",".$maxRecordsPerPage;
            }
            else{
                //preparing request
                $sql = $sqlNoLimit." LIMIT ".$maxRecordsPerPage;
            }

            $stmt = $pdo->query($sql);
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $html = array();

            if($stmt->rowCount() > 0){

                //display the record pager
                $pager = Array();
                //echoValue('totalRecordCount',$totalRecordCount);
                //echoValue('maxRecordsPerPage',$maxRecordsPerPage);
                if($totalRecordCount > $maxRecordsPerPage){

                    //get the current page URL and strip an eventual previous &page parameter
                    //parse the current URL request
                    $parsedUrl = j_parseUrl(selfURL());

                    //convert and decode $parsedUrl['query'] as an array of parameters
                    parse_str(urldecode($parsedUrl['query']), $parsedUrl['query']);

                    $thisUrl=Array();
                    $thisUrl[] = $parsedUrl['path'];
                    $index=0;

                    foreach($parsedUrl['query'] as $name => $value){
                        ($index++ == 0)?$separator='?':$separator='&';
                        if($name != 'page')$thisUrl[] = $separator.$name.'='.$value;
                    }

                    //figure if we should prefix page= with ? or &
                    $urlParamsCount = sizeof($thisUrl)-1;
                    if($urlParamsCount==0)$urlSeparator = "?";
                    else $urlSeparator = "&";

                    //convert url variable to string
                    $thisUrl = implode('',$thisUrl);

                    //display pager if there are more records than $config['max_records_per_page'], that is $maxRecordsPerPage
                    $pager[] = "<div class='pull-right col-lg-4 col-md-3 col-sm-12 col-xs-12'>";
                    $pager[] = "<ul class='pagination pagination-sm'>";
                    $numPages = ceil($totalRecordCount /  $maxRecordsPerPage);

                    if($page > 0) {
                        $href = $thisUrl."&amp;page=".($page-1);
                        $pager[] = "<li><a href='".$href."'>«</a></li>";
                    }
                    for($pageIndex=0;$pageIndex < $numPages; $pageIndex++){
                        //limit pager display to 10 pages around the current page
                        if($pageIndex > ($page-5) && $pageIndex < ($page+5)){
                            $href = $thisUrl."&amp;page=".$pageIndex;
                            $pager[] = "<li><a href='".$href."'".(($pageIndex == $page)?" class='active'":"").">".($pageIndex+1)."</a></li>";
                        }
                        //show "..." if there are more pages available than currently displayed
                        else if($pageIndex == ($page-5) || $pageIndex == ($page+5)){
                            $href = $thisUrl."&amp;page=".$pageIndex;
                            $pager[] = "<li><a href='".$href."'".(($pageIndex == $page)?" class='active'":"").">"."..."."</a></li>";
                        }
                    }
                    if($page < $numPages-1) {
                        $href = $thisUrl."&amp;page=".($page+1);
                        $pager[] = "<li><a href='".$href."'>»</a></li>";
                    }
                    $pager[] = "</ul></div>";
                    $html = array_merge($html,$pager);

                }


                foreach($rows as $row){

                    //Crop the digest
                    $cropLength=250;
                    if($before = stristr ( $row['digest'] , $this->searchTerms, true)){
                        //crop before and after the first searchTerm
                        $before = substr($before,-$cropLength / 2 );

                        //find first space within length
                        $first_space = stripos($before, ' ');

                        //make sure first word is cut before a space and not between letters
                        $before = '[...] '.substr($before, $first_space);

                        $after = substr(stristr( $row['digest'] , $this->searchTerms), 0 , $cropLength / 2 );

                        //find last space within length
                        $last_space = strrpos(substr($after, 0, $cropLength / 2), ' ');
                        //make sure last word is cut after a space and not between letters
                        $after = substr($after, 0, $last_space).' [...]';
                    }
                    else{
                        //searchTerms not found in the digest, simply crop the digest from the begining.
                        $before = substr( $row['digest'], 0 , $cropLength );

                        //find last space within length
                        $last_space = strrpos($before, ' ');
                        //make sure last word is cut after a space and not between letters
                        $before = substr($before, 0, $last_space);

                        $after=' [...]';
                    }

                    $html[] =   "<div class='searchResult'>";
                    $html[] =   "   <h3><a href='".$row['ressource_url']."'>".self::highlightWords($row['title'],explode(' ',$this->searchTerms))."</a></h3>";
                    $html[] =   "   <p>".self::highlightWords($before.' '.$after,explode(' ',$this->searchTerms))."</p>";
                    $html[] =   "   <a class='resultLink' href='".$row['ressource_url']."'>".$row['ressource_url']."</a>";
                    $html[] =   "</div>";
                }
                $this->bodytext = '<div id="searchResults">'.implode(chr(10),$html);

                //add another pager after the results
                $this->bodytext .= implode(chr(10),$pager);

                $this->bodytext .= '</div>';
            }
            else {
                $this->bodytext = $l10n->getLabel('webSearch','no_result');

            }

            $this->introText .= '<p><strong>Recherche pour "<em>'.$this->searchTerms.'</em>" : ';
            $this->introText .= ''.$totalRecordCount." résultat".(($totalRecordCount>1)?'s':'').'</strong></p>';
        }
        else {
            $this->bodytext = $l10n->getLabel('webSearch','type_keywords');
        }
    }

    function render(&$mainTemplate){
        global $mf,$l10n,$config;

        $this->searchForm = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/mf_searchEngine/ressources/templates/searchForm.html');
        $this->searchForm = str_replace("{searchForm-title}",$l10n->getLabel('webSearch','searchForm-title'),$this->searchForm);
        $this->searchForm = str_replace("{searchForm-placeholder}",$l10n->getLabel('webSearch','searchForm-placeholder'),$this->searchForm);
        $this->searchForm = str_replace("{searchForm-submit}",$l10n->getLabel('webSearch','searchForm-submit'),$this->searchForm);
        $this->searchForm = str_replace("{searchForm-value}",$this->searchTerms,$this->searchForm);


        $mainTemplate = str_replace("{webSearch_introText}",$this->introText, $mainTemplate);
        $mainTemplate = str_replace("{webSearch_searchForm}",$this->searchForm, $mainTemplate);
        $mainTemplate = str_replace("{webSearch_results}",$this->bodytext, $mainTemplate);

        $mainTemplate = str_replace("{searchForm-URL}",$mf->getPageUrl($config['plugins']['mf_searchEngine']['search-page-uid'][$mf->getLocale()]),$mainTemplate);


    }

    function setup(){}

    function getType(){
        return $this->moduleType;
    }

    function getName(){
        return $this->moduleKey;
    }

    function getVersion(){
        return $this->pluginVersion;
    }



    function getDependencies(){
        return $this->dependencies;
    }

    /**
     * Returns the given text with tags to highlight the specified words.
     * @param string $text
     * @param array $words
     * @return string
     *
     */
    static function highlightWords($text, $words)
    {
        foreach ($words as $word)
        {
            $word = preg_quote($word);
            $text = preg_replace("/\b($word)\b/i", '<span class="highlight_word">\1</span>', $text);
        }
        return $text;
    }

}
