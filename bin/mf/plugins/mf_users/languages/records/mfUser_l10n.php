<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(

    'en' => array(
        'civility' => 'Civilité',
        'mlle' => 'Miss',
        'mme' => 'Mme',
        'mr' => 'Mr',
        'first_name' => 'First name',
        'first_name_fail' => 'You must provide a first name',
        'last_name' => 'Last name',
        'last_name_fail' => 'You must provide a last name',
        'username' => 'Username',
        'username_fail' => 'Username already in use or missing',
        'password' => 'Password',
        'password_fail' => 'The provided password is empty',
        'password_confirmation' => 'Confirm',
        'password_confirmation_fail' => 'The password and its confirmation do not match',
        'email' => 'Email',
        'email_fail' => 'You must supply an email address',
        'frontend_locale' => 'Favorite language on the website',
        'backend_locale' => 'Language for the web site administration',
        'administrator' => 'Administrator',
        'user_group' => 'Security group',
        'address' => 'Address',
        'address_fail' => 'You must specify an address',
        'home_phone' => 'Phone (home)',
        'home_phone_fail' => 'This phone number is required',
        'mobile_phone' => 'Phone (mobile)',
        'mobile_phone_fail' => 'This phone number is required',
        'work_phone' => 'Phone (work)',
        'work_phone_fail' => 'This phone number is required',
        'city' => 'City',
        'city_fail' => 'You must provide your city',
        'region' => 'Region',
        'region_fail' => 'You must provide your region',
        'zip' => 'Zip code',
        'zip_fail' => 'You must provide a zip code',
        'notes' => 'Administrator\'s memo',
        'sex' => 'Sex',
        'birthdate' => 'Birth date',
		'backend_access' => 'Enable access to back office',
        'reset_password' => 'Reset your password',
        'reset_password2' => 'Your password has been reset.<br> Here are your new identifiers including your new password',
        'error' => 'Error',
        'hello' => 'Hello',
        'click_here' => 'Click here',
        'to_connect' => 'to connect',
        'default_user' => "Default admin user (user \"admin\" / password \"1234\") created sucessfully !",

        /*mandatory records*/
        'record_name' => 'Backend user',
        'new_record' => 'Add backend user',


    ),

    'fr' => array(
        'civility' => 'Civilité',
        'mlle' => 'Mademoiselle',
        'mme' => 'Madame',
        'mr' => 'Monsieur',
        'first_name' => 'Prénom',
        'first_name_fail' => 'Vous devez indiquer un prénom',
        'last_name' => 'Nom',
        'last_name_fail' => 'Vous devez indiquer un nom',
        'username' => 'Nom d\'utilisateur',
        'username_fail' => 'Nom d\'utilisateur déjà utilisé ou manquant',
        'password' => 'Mot de passe',
        'password_fail' => 'Le mot de passe spécifié est vide',
        'password_confirmation' => 'Confirmez',
        'password_confirmation_fail' => 'Le mot de passe et sa confirmation ne correspondent pas',
        'email' => 'Email',
        'email_fail' => 'Vous devez spécifier une adresse email',
        'frontend_locale' => 'Langue favorite sur le site',
        'backend_locale' => 'Langue du module d\'administration',
        'administrator' => 'Administrateur',
        'user_group' => 'Groupe de sécurité',
        'address' => 'Adresse',
        'address_fail' => 'Vous devez spécifier une adresse',
        'home_phone' => 'Téléphone (domicile)',
        'home_phone_fail' => 'Un téléphone est obligatoire',
        'mobile_phone' => 'Téléphone (mobile)',
        'mobile_phone_fail' => 'Un téléphone est obligatoire',
        'work_phone' => 'Téléphone (bureau)',
        'work_phone_fail' => 'Un téléphone est obligatoire',
        'city' => 'Ville',
        'city_fail' => 'Vous devez spécifier une ville',
        'region' => 'Région',
        'region_fail' => 'Vous devez spécifier une region',
        'zip' => 'Code postal',
        'zip_fail' => 'Vous devez spécifier un code postal',
        'notes' => 'Mémo des administrateurs',
        'sex' => 'Sexe',
        'birthdate' => 'Date de naissance',
		'backend_access' => 'Permettre l\'accès au module d\'administration',
        'reset_password' => 'Réinitialisation de votre mot de passe',
        'reset_password2' => 'Votre mot de passe a été réinitialisé.<br> Voici vos identifiants de connexion incluant votre nouveau mot de passe ',
        'error' => 'Erreur',
        'hello' => 'Bonjour',
        'click_here' => 'Cliquez ici',
        'to_connect' => 'pour vous connecter',
        'default_user' => "Administrateur par défaut (utilisateur \"admin\" / mot de passe \"1234\") créé avec succès !",

        /*mandatory records*/
        'record_name' => 'Utilisateur backend',
        'new_record' => 'Ajout utilisateur backend',

    ),
);

?>