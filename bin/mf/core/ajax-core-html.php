<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


/**
 * Serves ajax requests returning pure HTML code that can be displayed either as a full web page (requires parameters header and footer set 1 in this case) or as an HTML block
 * which could be substituted using jQuery.
 * See file ajax-core-json.php for serving pure JSON answers.
*/

$execution_start = time();

require_once '../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/frontend/frontend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/forms/recordHistoryForm.php';




if(isset($_REQUEST['getHTMLAsAJAX']) && $_REQUEST['getHTMLAsAJAX']>0){
    //sometimes we need to get a full page HTML output as json
    header('Content-type: application/json');
}
else{
    //main case : we are serving HTML
    header('Content-type: text/html');
}

if(isset($_REQUEST['mode']) && $_REQUEST['mode']=='backend'){
    //create a backend for being able to use all the mindflow objects
    $xEnd = new backend();
}
else{
    //create a backend for being able to use all the mindflow objects
    $xEnd = new frontend();
}



// Nicer HTML fatal error handling
ob_start('fatal_handler');

function fatal_handler($buffer){
    global $logger;
    $error=error_get_last();
    if($error['type'] == 1){
        $newBuffer='<html><header><title>Fatal Error </title></header>
                    <body style="text-align: center;">
                      <div class="alert alert-danger alert-dismissable">
                        <h1>Fatal error</h1>
                        <!--<b>Line :</b> '.$error['line'].'</br>-->
                        <b>Message :</b> '.nl2br($error['message']).'</br>
                        <!--<b>File :</b> '.$error['file'].'</br>-->                             
                      </div>
                    </body></html>';
        $logger->emergency($newBuffer);
        return $newBuffer;

    }
    return $buffer;
}


$xEnd->prepareData();
$xEnd->currentModule = null;
unset($_REQUEST['module']);

if($mf->mode == "authentification" || ($mf->mode == "backend" && $mf->currentUser == null)){

    echo "<div style='width:100%;font-weight:bold;text-align:center;margin:80px auto 80px auto;'><div class=\"alert alert-danger\" role=\"alert\"><p><span class='glyphicon glyphicon-ban-circle'></span> ".$l10n->getLabel('backend','session_expired')."</p></div></div>";
}

//check the security hash. No request will be honored if the security hash is not supplied or if there is a mismatch
else if((checkSec()) && trim($_REQUEST['fields']) !=''){


    //put you AJAX HTML in this var, the base HTML template from templates/your_backend_theme_name/ajax.html will be wrapped around with header + footer
    $htmlBody = '';

    //try {
        if (isset($_REQUEST['action'])) {



        	if ($_REQUEST['action'] == 'getTemplate') {
                $htmlBody = '{bodytext}';
            }




            if ($_REQUEST['action'] == 'createRecord') {

                //create an instance of the object to be updated
                $record = new $_REQUEST['record_class']();

                //mask creation_date and modification_date even if specified by the programmer : these values will be autofilled.
                reset($record->showInEditForm);
                $firstKey =  &$record->showInEditForm[key($record->showInEditForm)];
                $firstKey = array_diff($firstKey, array('creation_date', 'modification_date'));

                if (isset($_REQUEST['parent_uid'])) $record->data['parent_uid']['value'] = intval($_REQUEST['parent_uid']);
                if (isset($_REQUEST['parent_class'])) $record->data['parent_class']['value'] = $_REQUEST['parent_class'];


                $showSaveButton = (isset($_REQUEST['showSaveButton']) ? $_REQUEST['showSaveButton'] : true);
                $showSaveCloseButton = (isset($_REQUEST['showSaveCloseButton']) ? $_REQUEST['showSaveCloseButton'] : true);
                $showPreviewButton = (isset($_REQUEST['showPreviewButton']) ? $_REQUEST['showPreviewButton'] : false);
                $showCloseButton = (isset($_REQUEST['showCloseButton']) ? $_REQUEST['showCloseButton'] : true);
                $recordEditTableID = (isset($_REQUEST['recordEditTableID']) ? $_REQUEST['recordEditTableID'] : null);

                if (isset($_REQUEST['advancedSettings'])) {
                    parse_str(base64_decode(urldecode($_REQUEST['advancedSettings'])), $advancedSettings); //argument must be sent in the url using urlencode(base64_encode(http_build_query($advancedSettings)))
                } else $advancedSettings = array(
                    'JSCallbackFunctionAfterSubmit' => '',
                    'PHPCallbackFunctionAfterSubmit' => '',
                    'saveButtonLabel' => '',
                    'saveCloseButtonLabel' => '',
                    'previewButtonLabel' => '',
                    'closeButtonLabel' => '',
                );


                if (isset($_REQUEST['formAttributes'])) {
                    parse_str(base64_decode(urldecode($_REQUEST['formAttributes'])), $formAttributes); //argument must be sent in the url using urlencode(base64_encode(http_build_query($formAttributes)))
                } else $formAttributes = array(
                    'method' => 'POST',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'enctype' => 'x-www-form-urlencoded'
                );


                //setup AJAX output
                //create and publish the record form
                $form = $mf->formsManager;
                $htmlBody = '<div id="ajaxResult">' . $form->editRecord($record, '', '', $showSaveButton, $showSaveCloseButton, $showPreviewButton, $showCloseButton, 1, $advancedSettings, $formAttributes, $recordEditTableID) . '</div><!-- /.ajaxResult -->';

            }





            else if ($_REQUEST['action'] == 'viewRecord') {

                //create an instance of the object to be updated
                $record = new $_REQUEST['record_class']();
	            $recordEditTableID = (isset($_REQUEST['recordEditTableID']) ? $_REQUEST['recordEditTableID'] : null);
	            $session_recordEditTableID = (isset($_REQUEST['session_recordEditTableID']) ? $_REQUEST['session_recordEditTableID'] : null);
	            //$RETSession = $_SESSION['mf']['recordEditTables'][$session_recordEditTableID];

                $showSaveButton = (isset($_REQUEST['showSaveButton']) ? filterBoolean($_REQUEST['showSaveButton']) : false);
                $showSaveCloseButton = (isset($_REQUEST['showSaveCloseButton']) ? filterBoolean($_REQUEST['showSaveCloseButton']) : false);
                $showPreviewButton = (isset($_REQUEST['showPreviewButton']) ? filterBoolean($_REQUEST['showPreviewButton']) : false);
                $showCloseButton = (isset($_REQUEST['showCloseButton']) ? filterBoolean($_REQUEST['showCloseButton']) : true);

                if (isset($_REQUEST['advancedSettings'])) {
                    parse_str(base64_decode(urldecode($_REQUEST['advancedSettings'])), $advancedSettings); //argument must be sent in the url using urlencode(base64_encode(http_build_query($advancedSettings)))
                } else $advancedSettings = array(
                    'JSCallbackFunctionAfterSubmit' => '',
                    'PHPCallbackFunctionAfterSubmit' => '',
                    'saveButtonLabel' => '',
                    'saveCloseButtonLabel' => '',
                    'previewButtonLabel' => '',
                    'closeButtonLabel' => '',
                    'disableSave' => 'true',
                );


                if (isset($advancedSettings['formAttributes'])) {
                    $formAttributes = $advancedSettings['formAttributes'];
                } else $formAttributes = array(
                    'method' => 'POST',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'enctype' => 'x-www-form-urlencoded'
                );

                //load its content
                if ($record->load(intval($_REQUEST['record_uid'])) > 0) {

                    //setup AJAX output
                    foreach ($record->data as $key => $data) {
                        //disable fields editing
                        $record->data[$key]['editable'] = false;
                    }

                    //create and publish the record form
                    $form = $mf->formsManager;
                    $htmlBody = '<div id="ajaxResult">' . $form->editRecord($record, '', '', $showSaveButton, $showSaveCloseButton, $showPreviewButton, $showCloseButton, 1, $advancedSettings, $formAttributes, $recordEditTableID) . '</div><!-- /.ajaxResult -->';
                }
            }





            else if ($_REQUEST['action'] == 'editRecord') {
//echo "editRecord ";
                //create an instance of the object to be updated
                $record = new $_REQUEST['record_class']();

                $showSaveButton = (isset($_REQUEST['showSaveButton']) ? str_replace("'","",$_REQUEST['showSaveButton']) : true);
                $showSaveCloseButton = (isset($_REQUEST['showSaveCloseButton']) ? str_replace("'","",$_REQUEST['showSaveCloseButton']) : true);
                $showPreviewButton = (isset($_REQUEST['showPreviewButton']) ? str_replace("'","",$_REQUEST['showPreviewButton']) : false);
                $showCloseButton = (isset($_REQUEST['showCloseButton']) ? str_replace("'","",$_REQUEST['showCloseButton']) : true);
                $recordEditTableID = (isset($_REQUEST['recordEditTableID']) ? $_REQUEST['recordEditTableID'] : null);


                if (isset($_REQUEST['advancedSettings'])) {
                    parse_str(base64_decode(urldecode($_REQUEST['advancedSettings'])), $advancedSettings); //argument must be sent in the url using urlencode(base64_encode(http_build_query($advancedSettings)))
                } else $advancedSettings = array(
                    'JSCallbackFunctionAfterSubmit' => '',
                    'PHPCallbackFunctionAfterSubmit' => '',
                    'saveButtonLabel' => '',
                    'saveCloseButtonLabel' => '',
                    'previewButtonLabel' => '',
                    'closeButtonLabel' => '',
                );


                if (isset($_REQUEST['formAttributes'])) {
                    parse_str(base64_decode(urldecode($_REQUEST['formAttributes'])), $formAttributes); //argument must be sent in the url using urlencode(base64_encode(http_build_query($formAttributes)))
                } else $formAttributes = array(
                    'method' => 'POST',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'enctype' => 'x-www-form-urlencoded'
                );
//echo "load result=".$record->load(intval($_REQUEST['record_uid']));
                //load its content
                if ($record->load(intval($_REQUEST['record_uid'])) > 0) {

                    //setup AJAX output
                    //create and publish the record form
                    $form = $mf->formsManager;
                    $htmlBody = '<div id="ajaxResult">' . $form->editRecord($record, '', '', $showSaveButton, $showSaveCloseButton, $showPreviewButton, $showCloseButton, 1, $advancedSettings, $formAttributes, $recordEditTableID) . '</div><!-- /.ajaxResult -->';
                }
            }


            else if($_REQUEST['action'] == 'editHistory'){

	            $formMgr = $mf->formsManager;
	            $class = $_REQUEST['record_class'];
	            $uid = $_REQUEST['record_uid'];
	            $formID = $_REQUEST['formID'];

				if(isset($_REQUEST['field_name']))$fieldName = $_REQUEST['field_name'];
				else $fieldName = null;

	            $record = new $_REQUEST['record_class']();

	            $historyEntries = array();

	            //load its content
	            if($record->load($_REQUEST['record_uid']) > 0) {


		            $historyForm = new recordHistoryForm();

		            $historyForm->data['history_uid']['value']   = $uid;
		            $historyForm->data['history_class']['value'] = $class;
		            $historyForm->data['target_form_id']['value'] = $formID;

		            if ( $fieldName ) {
			            $fieldName                                   = $_REQUEST['field_name'];
			            $historyForm->data['history_field']['value'] = $fieldName;
		            }

		            $historyEntries = dbRecord::listHistoryEntries( $record, $fieldName, false );

//print_r($historyEntries);

		            if(count($historyEntries) > 0) {

			            //we want to track the editor user for each timestamp
			            $editorUid       = array();
			            $editorClass     = array();
			            $editorInstance  = array();
			            $editorName      = array();
			            $lastEditor      = 0;
			            $lastEditorClass = '';
			            $lastTimestamp   = 0;


			            //backend editors user list from the current MF installation
			            $beUsers = mfUser::listUsersByUid();

			            foreach ( $historyEntries as $timestamp => $entry ) {
				            //			            if ( $lastTimestamp == 0 ) {
				            //				            $lastTimestamp = $timestamp;
				            //			            }

//				            			            echo $timestamp."=<br />".chr(10);
//				            			            echo "isset(entry['modificator_uid'])=".isset( $entry['modificator_uid'] )."<br />".chr(10);
//				            			            echo "isset(entry['modificator_uid']['upd'])=".isset( $entry['modificator_uid']['upd'] )."<br />".chr(10);
//				            			            echo "isset(entry['modificator_uid']['upd']['value'])=".isset( $entry['modificator_uid']['upd']['value'] )."<br />".chr(10);
//				            			            echo "isset(entry['modificator_uid']['upd']['value']['new'])=".isset( $entry['modificator_uid']['upd']['value']['new'] )."<br />".chr(10);
//				            			            if(isset( $entry['modificator_uid'] ) && !isset( $entry['modificator_uid']['upd'] ))print_r($entry['modificator_uid']);
//
//				            			            if(isset($entry['creator_uid']['value']))echo 'creator_uid='.$entry['creator_uid']['value'].'<br />';
//				            			            if(isset($entry['creator_class']['value']))echo 'creator_class='.$entry['creator_class']['value'].'<br />';
//				            			            if(isset($entry['modificator_uid']['value']))echo 'modificator_uid='.$entry['modificator_uid']['value'].'<br />';
//				            			            if(isset($entry['modificator_class']['value']))echo 'modificator_class='.$entry['modificator_class']['value'].'<br />';
//				            			            echo "<br />";
				            //print_r($entry);
				            //record has been modified and its editor has changed
				            if ( isset( $entry['modificator_uid'] ) ) {

					            //get the new editor uid
					            $editorUid[ $timestamp ] = $entry['modificator_uid']['value'];

					            //if the class of the editor has changed, process it
					            if ( isset( $entry['modificator_class']['value'] ) && $entry['modificator_class']['value'] != '' ) {
						            $editorClass[ $timestamp ] = $entry['modificator_class']['value'];
					            } else {
						            $editorClass[ $timestamp ] = $entry['modificator_uid']['editClass'];
					            }
				            } //record has been created
				            else if ( $lastEditor == 0 || isset( $entry['creator_uid'] ) ) {

					            //get the creator uid
					            if ( isset( $entry['creator_uid'] ) ) {
						            $editorUid[ $timestamp ] = $entry['creator_uid']['value'];
					            } else {
						            $editorUid[ $timestamp ] = $record->data['creator_uid']['value'];
					            }

					            //if the class of the editor has changed, process it (should always occur)
					            if ( isset( $entry['creator_class']['value'] ) && $entry['creator_class']['value'] != '' ) {
						            $editorClass[ $timestamp ] = $entry['creator_class']['value'];
					            } else if ( $lastEditorClass != '' ) {
						            $editorClass[ $timestamp ] = $lastEditorClass;
					            } //empty $lastEditorClass, take record's 'creator_class' by default
					            else {
						            $editorClass[ $timestamp ] = $record->data['creator_class']['value'];
					            }
				            } //record has been modified, but the editor is the same as in previous saves
				            else if ( $lastEditor != 0 ) {

					            //revert editor uid and class values from previous version
					            $editorUid[ $timestamp ]   = $lastEditor;
					            $editorClass[ $timestamp ] = $lastEditorClass;
				            }
				            else{

				            	// editor really unknown. This can occur when history began to be stored while the record was already existing
					            // and the creator and modificator have not been modified, so they do not appear in the diff

					            //check if the current timestamp matches the lfirst key of our array, if so apply current editor
					            //first key == latest entry
					            reset($historyEntries);         // move the internal pointer to the end of the array
					            $firstKey = key($historyEntries);

					            if($timestamp == $firstKey) {
						            $editorUid[ $timestamp ]   = $record->data['editor_uid']['value'];
						            $editorClass[ $timestamp ] = $record->data['editor_class']['value'];
					            }
					            else{
						            $editorUid[ $timestamp ]   = 0;
						            $editorClass[ $timestamp ] = 'unidentified';
					            }
				            }

				            $lastEditor      = $editorUid[ $timestamp ];
				            $lastEditorClass = $editorClass[ $timestamp ];
				            $lastTimestamp   = $timestamp;


				            if ( class_exists( $editorClass[ $timestamp ] ) ) {
					            //load user with custom class and extract first + last name
					            $editorInstance[ $timestamp ] = new $editorClass[ $timestamp ]();
					            $editorInstance[ $timestamp ]->load( $editorUid[ $timestamp ] );

					            $editorName[ $timestamp ] = $editorInstance[ $timestamp ]->data['first_name']['value'] . ' ' . $editorInstance[ $timestamp ]->data['last_name']['value'];
				            } else {
					            //display for unidentified users
					            if ( $editorClass[ $timestamp ] != '' ) {
						            $editorName[ $timestamp ] = $l10n->getLabel( 'main', $editorClass[ $timestamp ] );
					            }
					            else $editorName[ $timestamp ] = $l10n->getLabel( 'main', 'unidentified' );
				            }

			            }
//print_r($editorName);

			            //get the list of history timestamps, ordering them from latest to initial version
			            $timestamps = array_reverse( array_keys( $historyEntries ) );

			            foreach ( $timestamps as $timestamp ) {

				            $date = new DateTime();
				            $date->setTimestamp( str_replace( ',', '.', $timestamp ) );

							$edName = $editorName[ $timestamp ];

				            $historyForm->data['changes']['possibleValues'][ $timestamp ] = $date->format( $GLOBALS['site_locales'][ $mf->getLocale() ]['php_datetime_format'] ) . ' ' . $l10n->getLabel( 'main', 'by' ) . ' ' . $edName;
			            }

			            //Now display the first field from the history list in the base form
			            //single field history : show only the selected field, even if there could be some other modified fields aside
			            if ( $fieldName ) {
				            //we do not want to show the history button towards another level.
				            $historyEntries[ $lastTimestamp ][ $fieldName ]['showFieldHistory'] = false;
				            $historyEntries[ $lastTimestamp ][ $fieldName ]['editable']         = false;
				            $historyForm->data['history_display']['value']                      = '<div id="history_display">' . $formsManager->getFieldHTML( $record, $historyEntries[ $lastTimestamp ][ $fieldName ], $fieldName, 'historyFormID', 'backend', '', '', true ) . '</div>' . chr( 10 );
			            } //whole record history : there may be multiple fields showing up in the base record
			            else {


				            $html = '<div id="history_display">';
				            $historyForm->data['history_display']['value'] = '';

				            $tsSelectedFields = dbRecord::getHistoryVersion($lastTimestamp,$record,null);

				            if($tsSelectedFields) {

					            foreach ( $tsSelectedFields as $fieldName => $fieldData ) {
						            $html .= $formsManager->getFieldHTML( $record, $fieldData, $fieldName, $formID, $mf->mode, '', '', true ) . chr( 10 );
					            }
				            }
				            $html                                          .= '</div>';
				            $historyForm->data['history_display']['value'] = $html;

			            }

			            $htmlBody = $historyForm->getHTML();
		            }
		            else $htmlBody = '<div class="alert alert-warning" role="alert">'.$l10n->getLabel('main','no_history_entries').'</div>';

	            }
	            else $htmlBody = $l10n->getLabel('main','error').' '.$l10n->getLabel('main','could_not_load_record');



            }



            else if ($_REQUEST['action'] == 'createForm') {

                //create an instance of the object to be updated
                $form = new $_REQUEST['form_class']();
                $actionURL = $_REQUEST['action_url'];
                $formAction = $_REQUEST['form_action'];
                $callBackJSFuncName = $_REQUEST['callback_jsfunc'];
                $moduleName = $_REQUEST['module_name'];
                $subModuleName = $_REQUEST['submodule_name'];
                $showSubmitButton = $_REQUEST['show_submit_button'];
                $submitButtonLabel = base64_decode($_REQUEST['submit_button_label']);
                $useAJAX = $_REQUEST['useAJAX'];
                $submitButtonClass = base64_decode($_REQUEST['submit_button_class']);


                if (isset($_REQUEST['formAttributes'])) {
                    parse_str(base64_decode(urldecode($_REQUEST['formAttributes'])), $formAttributes); //argument must be sent in the url using urlencode(base64_encode(http_build_query($formAttributes)))
                } else $formAttributes = array(
                    'method' => 'POST',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'enctype' => 'x-www-form-urlencoded'
                );


                //setup AJAX output
                //create and publish the record form
                $formsManager = $mf->formsManager;
                $html = $formsManager->editForm($actionURL, $formAction, $callBackJSFuncName, $form, $moduleName, $subModuleName, $showSubmitButton, $submitButtonLabel, $submitButtonClass, $useAJAX, null, $formAttributes);


                $htmlBody = '<div id="ajaxResult">' . $html . '</div><!-- /.ajaxResult -->';

            } else if ($_REQUEST['action'] == 'editMicroTemplate') {

                //create an instance of the object to be updated
                $record = new $_REQUEST['record_class']();

                $showSaveButton = (isset($_REQUEST['showSaveButton']) ? $_REQUEST['showSaveButton'] : true);
                $showSaveCloseButton = (isset($_REQUEST['showSaveCloseButton']) ? $_REQUEST['showSaveCloseButton'] : true);
                $showPreviewButton = (isset($_REQUEST['showPreviewButton']) ? $_REQUEST['showPreviewButton'] : false);
                $showCloseButton = (isset($_REQUEST['showCloseButton']) ? $_REQUEST['showCloseButton'] : true);
                $recordEditTableID = (isset($_REQUEST['recordEditTableID']) ? $_REQUEST['recordEditTableID'] : null);


                //load its content
                if ($record->load(intval($_REQUEST['record_uid'])) > 0) {

                    //setup AJAX output
                    //create and publish the record form
                    $form = $mf->formsManager;
                    $htmlBody = '<div id="ajaxResult">' . $form->editRecord($record, '', '', $showSaveButton, $showSaveCloseButton, $showPreviewButton, $showCloseButton, 1, null, null, $recordEditTableID) . '</div><!-- /.ajaxResult -->';
                }
            } else if ($_REQUEST['action'] == 'deleteRecord') {

                $deleteLink = base64_decode($_REQUEST['delete_url']);

                $isAjax = $_REQUEST['is_ajax'];

                if ($isAjax) $deleteCommand = "ajaxDelete('" . $deleteLink . "');";
                else $deleteCommand = "document.location=\''.$deleteLink.'\';";

                $html = array();
                $html[] = '<div class="container-fluid">
                                <div class="row" style="padding:20px;"></div>
                                    <p><strong>' . $l10n->getLabel('backend', 'areyousure_delete') . '"' . $_REQUEST['title'] . '" ?</strong></p><br/>
                                </div><!-- /.row -->
                           
                               <div class="mfSaveButtons">
                                    <div class="row">
                                        <div class="col-lg-11">
                                            <div class="formButtons">
                                                <button type="button" class="btn btn-primary" onclick="' . $deleteCommand . '">' . $l10n->getLabel('main', 'delete') . '</button>
                                                <button type="button" class="btn closeButton" onclick="mf.dialogManager.closeLastDialog();" >' . $l10n->getLabel('main', 'cancel') . '</button>
                                            </div>
                                        </div>
                                    </div>
                               </div><!-- /.mfSaveButtons -->
                           </div><!-- /.container-fluid -->
                ';

                if ($isAjax) $html[] = '
                            <script>
                                function ajaxDelete(deleteLink){
                                    $.ajax({
                                    url: deleteLink,
                                    success: function(response) {
                                        mf.dialogManager.closeLastDialog();
                                    },
                                    error: function(response) {
                                        alert(response);
                                    }
                                });
                                }
                            </script>';

                $htmlBody = implode(chr(10), $html);

            } else if ($_REQUEST['action'] == 'select_add_records_popup') {

                $key = $_REQUEST['key'];

                $parentRecord = new $_REQUEST['record_class']();
                $parentRecord->load($_REQUEST['parent_uid']);
                $dataType = trim($parentRecord->data[$key]['dataType']);
                $editClass = trim($parentRecord->data[$key]['editClass']);

                //required to load l10n file
                $listedRecord = new $editClass();

                $htmlBody = '<div class="container-fluid">';
                $htmlBody .= '<div class="row" style="padding:20px;"><div class="alert alert-info" role="alert">' . $l10n->getLabel('main', 'select_record') . '</div></div>';
                $htmlBody .= '<div class="panel panel-default">
                  <div class="panel-heading">' . $l10n->getLabel($editClass, 'record_name') . '</div>
                  <div class="panel-body">
                  ';

                if (isset($editClass)) {
                    $record = new $editClass();

                    if (isset($parentRecord->data[$key]['addItemsSQL'])) {

                        $processSQL = true;

                        if (isset($parentRecord->data[$key]['addItemsSQL']['SELECT'])) $titleFields = $parentRecord->data[$key]['addItemsSQL']['SELECT'];
                        else {
                            $mf->addWarningMessage("You must specify a SELECT column for record " . $_REQUEST['record_class'] . '->data[\'' . $key . '\'][\'addItemsSQL\']');
                            $processSQL = false;
                        }

                        if (isset($parentRecord->data[$key]['addItemsSQL']['FROM'])) $tableName = $parentRecord->data[$key]['addItemsSQL']['FROM'];
                        else {
                            $mf->addWarningMessage("You must specify a FROM table for record " . $_REQUEST['record_class'] . '->data[\'' . $key . '\'][\'addItemsSQL\']');
                            $processSQL = false;
                        }

                        if (isset($parentRecord->data[$key]['addItemsSQL']['WHERE']) && trim($parentRecord->data[$key]['addItemsSQL']['WHERE']) != '') $WHERE = " AND " . $parentRecord->data[$key]['addItemsSQL']['WHERE'] . " AND DELETED=0";
                        else $WHERE = ' AND DELETED=0';

                        if (isset($parentRecord->data[$key]['addItemsSQL']['JOIN']) && trim($parentRecord->data[$key]['addItemsSQL']['JOIN']) != '') $JOIN = " JOIN " . $parentRecord->data[$key]['addItemsSQL']['JOIN'];
                        else $JOIN = '';

                        if (isset($parentRecord->data[$key]['addItemsSQL']['ORDER BY']) && trim($parentRecord->data[$key]['addItemsSQL']['ORDER BY']) != '') $ORDER_BY = $parentRecord->data[$key]['addItemsSQL']['ORDER BY'];
                        else $ORDER_BY = '';

                        if (isset($parentRecord->data[$key]['addItemsSQL']['ORDER DIRECTION']) && trim($parentRecord->data[$key]['addItemsSQL']['ORDER DIRECTION']) != '') $ORDER_DIRECTION = " " . $parentRecord->data[$key]['addItemsSQL']['ORDER DIRECTION'];
                        else $ORDER_DIRECTION = '';

                        if ($processSQL) {
                            $titles = explode(',', $titleFields);

                            $htmlBody .= '<div id="ajaxResult">' . $record->showRecordEditTable(
                                    array(
                                        'SELECT' => 'uid,' . $titleFields,
                                        'FROM' => $tableName,
                                        'JOIN' => $JOIN,
                                        'WHERE' => '1=1' . $WHERE,
                                        'ORDER BY' => $ORDER_BY,
                                        'ORDER DIRECTION' => $ORDER_DIRECTION,
                                    ),
                                    '',
                                    '',
                                    $titles[0],
                                    $keyProcessors = array(//'deleted' => 'dbRecord::getDeletedName'
                                    ),
                                    $page = NULL,

                                    array(
                                        'view' => 0,
                                        'edit' => 0,
                                        'delete' => 0,
                                        'addToSelect' => array(
                                            'link' => "\"javascript:window.opener.updateOption_" . $key . "( 'add',{record_uid},'{record_class}')\"",
                                            'labelDomain' => 'backend',
                                            'linkText' => "<i class='glyphicon glyphicon-plus-sign'></i>",
                                            'isAjax' => false
                                        ),
                                    ),
                                    array(
                                        'ajaxActions' => true,
                                        'hideColumns' => 'uid',
                                    )
                                ) . '</div>';
                        }
                    } else $mf->errorMessages[] = "\$record->data[$key]['addItemsSQL'] is not defined for " . $_REQUEST['record_class'] . " Can not display content.";
                } else $mf->errorMessages[] = "\$record->data[$key]['dataType'] : second parameter must specify the class of object to be displayed in the select box, such as 'dataType' => 'record-multiple address'";

                $htmlBody .= ' 
                        </div>
                    </div>
                </div>';
            } else if ($_REQUEST['action'] == 'printRecordEditTable') {

	            $session_recordEditTableID = (isset($_REQUEST['session_recordEditTableID']) ? $_REQUEST['session_recordEditTableID'] : null);

	            if(isset($_SESSION['mf']['recordEditTables'][$session_recordEditTableID])) {
		            $RETSession = $_SESSION['mf']['recordEditTables'][ $session_recordEditTableID ];

		            //create an instance of the object class displayed in the recordEditTable
		            $record = new $RETSession['recordClass']();

		            $request                    = $RETSession['request'];
		            $request['ORDER BY']        = urldecode( $_REQUEST['sorting_field'] );
		            $request['ORDER DIRECTION'] = $_REQUEST['sorting_direction'];

		            $keyProcessors = $RETSession['keyProcessors'];
		            $actions       = $RETSession['availableActions'];


		            $advancedOptions   = $RETSession['advancedOptions'];
		            $recordEditTableID = $RETSession['recordEditTableID'];

		            $moduleName    = $RETSession['moduleName'];
		            $subModuleName = $RETSession['subModuleName'];
		            $mainKey       = $RETSession['mainKey'];

		            if ( $_REQUEST['show_page_num'] == "all" ) {
			            $numPage                                = 0;
			            $advancedOptions['forceNumRowsPerPage'] = 9999999;
			            $advancedOptions['hideColumns']         = 'actions';
		            } else {
			            $numPage = intval( $_REQUEST['show_page_num'] );
		            }
		            $advancedOptions['printMode'] = true;

		            $body     = '<div class="printPage">' . $record->showRecordEditTable( $request, $moduleName, $subModuleName, $mainKey, $keyProcessors, $numPage, $actions, $advancedOptions, $recordEditTableID ) . "</div>";
		            $htmlBody = str_replace( "{subdir}", SUB_DIR, $body );
	            }
            }

        }
   /*}
    catch (Exception $e) {
        $mf->addErrorMessage($e->getMessage());
    }*/

    //Build the HTML
    $html = array();

    //process page header
    if(isset($_REQUEST['header']) && $_REQUEST['header']>0){
        $mf->header = $xEnd->getStandardHeader(' id="ajax-container"');

    }
    else $mf->header='<div id="uiMessages" class="container-fluid">{error-messages}{custom-messages}{warning-messages}{success-messages}{info-messages}</div>';

    //processing UI messages
	$customMsgs=array();
	foreach($mf->customMessages as $message) $customMsgs[] = $message.'</div>';
	$mf->header = str_replace("{custom-messages}",implode(chr(10),$customMsgs),$mf->header);

	$warnings=array();
    foreach($mf->warningMessages as $message) $warnings[] = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
    $mf->header = str_replace("{warning-messages}",implode(chr(10),$warnings),$mf->header);

    $errors=array();
    foreach($mf->errorMessages as $message) $errors[] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
    $mf->header = str_replace("{error-messages}",implode(chr(10),$errors),$mf->header);

    $success=array();
    foreach($mf->successMessages as $message) $success[] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
    $mf->header = str_replace("{success-messages}",implode(chr(10),$success),$mf->header);

    $infos=array();
    foreach($mf->infoMessages as $message) $infos[] = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
    $mf->header = str_replace("{info-messages}",implode(chr(10),$infos),$mf->header);

    $html[]= $mf->header;


    $mf->body .= file_get_contents(DOC_ROOT.SUB_DIR.$xEnd->template.'/ajax.html');

    $mf->body = str_replace("{subdir}",SUB_DIR,$mf->body);
    if(isset($_REQUEST['submodule']))$mf->body = str_replace("{submodule-name}",$_REQUEST['submodule'],$mf->body);

    //set AJAX body
    $mf->body = str_replace("{ajax}",$htmlBody,$mf->body);



    $html[]= $mf->body;

    if(isset($_REQUEST['footer']) && $_REQUEST['footer']>0){
        //process page footer
        $mf->footer = $xEnd->getStandardFooter();
        $html[]= $mf->footer;
    }

    //output HTML
    if(isset($_REQUEST['getHTMLAsAJAX']) && $_REQUEST['getHTMLAsAJAX']>0){
        echo '{"jsonrpc" : "2.0", "result" : "ajax-backend-html", "html" : '.json_encode(implode(chr(10),$html)).'}';
    }
    else echo implode(chr(10),$html);



}
else {

    echo 'MindFlow : Request was not accepted. Check the "sec" and "fields" values must be present in the request and properly set.';
}
$mf->db->closeDB();