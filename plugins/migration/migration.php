<?php
/*
   Copyright 2017 Alban Cousinié <info@mindflow-framework.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Proceeds to migration operations on tables. Can be operated from command line arguments or request parameters thru an AJAX call(the MigrationManager module does this).
 *
 * Proceeds to the action defined in (action,target) input parameters on the table associated with the configured target name
 */
//type octet-stream. make sure apache does not gzip this type, else it would get buffered
header('Content-Type: text/octet-stream');

// Explicitly disable caching so Varnish and other upstreams won't cache.
header("Cache-Control: no-cache, must-revalidate");

// Setting this header instructs Nginx to disable fastcgi_buffering and disable
// gzip for this request.
header('X-Accel-Buffering: no');


require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/frontend/frontend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();

global $mf,$l10n,$config;


if(CLI_MODE){
	if(count($argv) == 2) {
		$action = $argv[0];
		$target = $argv[1];
	}
	else echo "usage : migration.php action target".chr(10).
	"Available actions : TRUNCATE_LOCAL, IMPORT_DATA".chr(10).
	"Available targets : check plugin configuration variable \$GLOBALS['config']['plugins']['migration']['targets']".chr(10);
}
else {
	if ( isset( $_REQUEST['action'] ) ) {
		$action = $_REQUEST['action'];
	} else {
		die( "invalid action, aborting." );
	}

	if ( isset( $_REQUEST['target'] ) ) {
		$target = $_REQUEST['target'];
	} else {
		die( "invalid target, aborting." );
	}
}

//unlimited execution time for this script
set_time_limit(0);

//prepare streaming
ob_implicit_flush(1);
while (@ob_end_flush());

$mMgr = new MigrationManager();
$mMgr->import($action, $target);



