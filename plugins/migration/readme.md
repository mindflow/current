
Allows importing data from another database into the current MindFlow Installation using a dedicated user interface.
If the records already exist in the current database, they will be updated, otherwise they will be created.
It is possible to update the data or format of some columns on the fly while they are imported, and to ignore some columns.  