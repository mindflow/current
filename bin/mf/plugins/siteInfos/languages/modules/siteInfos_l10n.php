<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'menu_title' => 'General information management',
        'no_siteInfos' => 'There currently exists no siteInfo. You can <a href="{subdir}/mf/index.php?module=siteInfos&action=create">create an siteInfo</a>.',
        'siteInfo_recorded' => 'The siteInfo has been successfully recorded',
    ),

    'fr' => array(
        'menu_title' => 'Gestion des informations générales',
        'no_siteInfos' => 'Il n\'existe pas d\'infos générales pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=siteInfos&action=create">créer une info générale</a>.',
        'siteInfo_recorded' => 'L\'info générale a été enregistré avec succès',
    ),
);