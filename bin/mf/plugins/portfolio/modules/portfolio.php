<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/portfolio/records/portfolioItem.php';
require_once DOC_ROOT.SUB_DIR.'/mf/plugins/portfolio/records/portfolioCategory.php';

class portfolio implements module
{
    private $moduleKey = 'portfolio';
    private $moduleType = 'backend,frontend';

    private $action='list';

    var $portfolioItemOutput;



    function prepareData(){
        global $mf,$l10n;
        $this->pageOutput = '';

        if($mf->mode == 'frontend'){

            //$currentPageUid = $mf->info['currentPageUid'];

        }
        else if($mf->mode == 'backend'){

            //add the plugin to the backend menus
            $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=portfolio"><span class="glyphicon glyphicon-menu glyphicon glyphicon-picture" aria-hidden="true"></span>'.$l10n->getLabel('portfolio','module-title').'</a>','datas');

            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){


                /*
                 //process portfolioItemual record submission
                $form = $mf->formsManager;
                $form->prepareData();

                if(($currentRecordUid = $form->processForm()) > 0){
                    $this->pageOutput .= '<div class="alert alert-success fade in">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    '.$l10n->getLabel('portfolio','item_recorded').'
                  </div>';
                }     */


                if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                /*
                echo "_REQUEST['action']=".$_REQUEST['action']."<br />".chr(10);
                echo "_GET['uid']=".$_REQUEST['uid']."<br />".chr(10);
                echo "currentRecordUid=".$currentRecordUid."<br />".chr(10);
                */


                //echo "portfolio.php ACTION=".$this->action."<br />";
                switch($this->action){


                    case "list":
                        if(isset($_REQUEST['class'])){
                            if($_REQUEST['class']=='portfolioItem'){
                                $portfolioItem = new portfolioItem();
                                $this->pageOutput .= $portfolioItem->showRecordEditTable(
                                    array(
                                        'SELECT' => 'category, portfolio_date, title',
                                        'FROM' => 'mf_portfolio',
                                        'JOIN' => '',
                                        'WHERE' => 'deleted = 0',
                                        'ORDER BY' => 'uid',
                                        'ORDER DIRECTION' => 'DESC',
                                    ),
                                    'portfolio',
                                    '',
                                    'title',
                                    $keyProcessors = array('portfolio_date'=>'dbRecord::formatDate','category'=>'portfolioItem::getCategoryName'),
                                    $page = NULL,

                                    array(
                                        'view' => 1,
                                        'edit' => 1,
                                        'delete'=> 1
                                    ),
                                    array('ajaxActions' => true)
                                );
                            }
                            else if($_REQUEST['class']=='portfolioCategory'){
                                $category = new portfolioCategory();
                                $this->pageOutput .= $category->showRecordEditTable(
                                    array(
                                        'SELECT' => 'name, description',
                                        'FROM' => 'mf_portfolio_categories',
                                        'JOIN' => '',
                                        'WHERE' => 'deleted = 0',
                                        'ORDER BY' => 'uid',
                                        'ORDER DIRECTION' => 'DESC',
                                    ),
                                    'portfolio',
                                    '',
                                    'name',
                                    $keyProcessors = array(),
                                    $page = NULL,
                                    array(
                                        'view' => 1,
                                        'edit' => 1,
                                        'delete'=> 1
                                    ),
                                    array('ajaxActions' => true)
                                );
                            }
                        }
                        else{
                            //show portfolio by default
                            $portfolioItem = new portfolioItem();
                            $this->pageOutput .= $portfolioItem->showRecordEditTable(
                                array(
                                    'SELECT' => 'category, portfolio_date, title',
                                    'FROM' => 'mf_portfolio',
                                    'JOIN' => '',
                                    'WHERE' => 'deleted = 0',
                                    'ORDER BY' => 'uid',
                                    'ORDER DIRECTION' => 'DESC',
                                ),
                                'portfolio',
                                '',
                                'title',
                                $keyProcessors = array('portfolio_date'=>'dbRecord::formatDate','category'=>'portfolioItem::getCategoryName'),
                                $page = NULL,

                                array(
                                    'view' => 1,
                                    'edit' => 1,
                                    'delete'=> 1
                                ),
                                array('ajaxActions' => true)
                            );
                        }
                        break;



                    case "create":
                        if($_REQUEST['class']=='portfolioItem'){
                            $portfolioItem = new portfolioItem();
                            $form = $mf->formsManager;
                            $this->pageOutput .=$form->editRecord($portfolioItem, $this->moduleKey, '');
                        }
                        else if($_REQUEST['class']=='portfolioCategory'){
                            $category = new portfolioCategory();
                            $form = $mf->formsManager;
                            $this->pageOutput .= $form->editRecord($category, $this->moduleKey, '');
                        }
                        break;



                    case "edit":
                        if($_REQUEST['class']=='portfolioItem'){
                            //case edit requested by some module as a GET url parameter
                            if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                $this->pageOutput .= $this->editPortfolioItem($_GET['uid']);
                            }
                            //case we just re-edit the last saved record
                            else {
                                $this->pageOutput .= $this->editPortfolioItem($currentRecordUid);
                            }
                        }
                        else if($_REQUEST['class']=='portfolioCategory'){
                            //case edit requested by some module as a GET url parameter
                            if(isset($_GET['uid']) && ($_GET['uid']!='')){
                                $this->pageOutput .= $this->editCategory($_GET['uid']);
                            }
                            //case we just re-edit the last saved record
                            else {
                                $this->pageOutput .= $this->editCategory($currentRecordUid);
                            }
                        }
                        break;


                    case "delete":
                        if($_REQUEST['class']=='portfolioItem'){
                            $portfolioItem = new portfolioItem();
                            $portfolioItem->load($_REQUEST['uid']);
                            $portfolioItem->delete();
                            $this->pageOutput .= $portfolioItem->showRecordEditTable(
                                array(
                                    'SELECT' => 'category, portfolio_date, title',
                                    'FROM' => 'mf_portfolio',
                                    'JOIN' => '',
                                    'WHERE' => 'deleted = 0',
                                    'ORDER BY' => 'uid',
                                    'ORDER DIRECTION' => 'DESC',
                                ),
                                'portfolio',
                                '',
                                'title',
                                $keyProcessors = array('portfolio_date'=>'dbRecord::formatDate','category'=>'portfolioItem::getCategoryName'),
                                $page = NULL,

                                array(
                                    'view' => 1,
                                    'edit' => 1,
                                    'delete'=> 1
                                ),
                                array('ajaxActions' => true)
                            );
                        }
                        else if($_REQUEST['class']=='portfolioCategory'){
                            $category = new portfolioCategory();
                            $category->load($_REQUEST['uid']);
                            $category->delete();
                            $this->pageOutput .= $category->showRecordEditTable(
                                array(
                                    'SELECT' => 'name, description',
                                    'FROM' => 'mf_portfolio_categories',
                                    'JOIN' => '',
                                    'WHERE' => 'deleted = 0',
                                    'ORDER BY' => 'uid',
                                    'ORDER DIRECTION' => 'DESC',
                                ),
                                'portfolio',
                                '',
                                'name',
                                $keyProcessors = array(),
                                $page = NULL,
                                array(
                                    'view' => 1,
                                    'edit' => 1,
                                    'delete'=> 1
                                ),
                                array('ajaxActions' => true)
                            );
                        }
                        break;


                    default:
                        //list portfolio
                        $portfolioItem = new portfolioItem();
                        $this->pageOutput .= $portfolioItem->showRecordEditTable(
                            array(
                                'SELECT' => 'category, portfolio_date, title',
                                'FROM' => 'mf_portfolio',
                                'JOIN' => '',
                                'WHERE' => 'deleted = 0',
                                'ORDER BY' => 'uid',
                                'ORDER DIRECTION' => 'DESC',
                            ),
                            'portfolio',
                            '',
                            'title',
                            array('portfolio_date'=>'dbRecord::formatDate','category'=>'portfolioItem::getCategoryName')
                        );
                        break;
                }
            }
        }

    }

    function render(&$mainTemplate){
        global $mf,$l10n;

        if($mf->mode == 'backend'){
            if(!isset($_REQUEST['module']) || ($this->moduleKey == $_REQUEST['module'])){

                $portfolioBody = file_get_contents(DOC_ROOT.SUB_DIR.'/mf/plugins/portfolio/ressources/templates/portfolio.html');

                $portfolioBody = str_replace("{module-title}",$l10n->getLabel('portfolio','module-title'),$portfolioBody);
                $portfolioBody = str_replace("{portfolio-menu-name}",$l10n->getLabel('portfolio','portfolio-menu-name'),$portfolioBody);
                $portfolioBody = str_replace("{list-portfolio}",$l10n->getLabel('portfolio','list-portfolio'),$portfolioBody);
                $portfolioBody = str_replace("{create-portfolio}",$l10n->getLabel('portfolio','create-portfolio'),$portfolioBody);
                $portfolioBody = str_replace("{list-categories}",$l10n->getLabel('portfolio','list-categories'),$portfolioBody);
                $portfolioBody = str_replace("{create-category}",$l10n->getLabel('portfolio','create-category'),$portfolioBody);
                $portfolioBody = str_replace("{portfolioItem}",$this->pageOutput,$portfolioBody);

                $mainTemplate = str_replace("{current_module}",$portfolioBody,$mainTemplate);
            }
        }
        else if($mf->mode == 'frontend'){
            $html = array();

            //$portfolioPagesUids = explode(',',$config['plugins']['portfolio']['portfolio-pages-uids']);

            //if(in_array($mf->info['currentPageUid'],$portfolioPagesUids)){

                $mainTemplate = str_replace("{plugin-portfolio-col1}",$this->getPortfolio('escapade', 'uid', 'ASC'),$mainTemplate);
                $mainTemplate = str_replace("{plugin-portfolio-col2}",$this->getPortfolio('alentours', 'uid', 'ASC'),$mainTemplate);

            //}

        }
    }



    function getType(){
        return $this->moduleType;
    }




    function editPortfolioItem($uid){
        global $mf;

        $portfolioItem = new portfolioItem();
        $portfolioItem->load($uid);
        $form = $mf->formsManager;
        return $form->editRecord($portfolioItem, $this->moduleKey, '',true,true,false,true);

    }


    function editCategory($uid){
        global $mf;

        $category = new portfolioCategory();
        $category->load($uid);
        $form = $mf->formsManager;
        return $form->editRecord($category, $this->moduleKey, '',true,true,false,true);

    }


    function getPortfolio($category, $sortingField, $sortingDirection){
        global $mf,$pdo;

        //preparing request only once
        $sql = "SELECT * FROM mf_portfolio WHERE deleted = '0' AND category = :category ORDER BY :sortingField :sortingDirection";


        $stmt = $pdo->prepare($sql);


        try{
            $stmt->execute(array(':category' => $category,':sortingField' => $sortingField, ':sortingDirection' => $sortingDirection));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);



        $html = array();

        if($stmt->rowCount() >0){

            foreach($rows as $row){

                $html[] = "<div class=portfolioItem>";
                $html[] = "<h3>".$row['title']."</h3>";
                $html[] = $row['text'];

                $imagesArray = explode(',', $row['images']);
                $image_path = $mf->getUploadsDir().'portfolioItem'.DIRECTORY_SEPARATOR.'portfolioItem_'.$row['uid'].DIRECTORY_SEPARATOR;

                foreach($imagesArray as $image){
                    if($image != ''){
                        list($width, $height, $type, $attr)= getimagesize(DOC_ROOT.SUB_DIR.$image_path.$image);
                        if(trim($image) != '')$html[] = '<img src="'.$image_path.$image.'" width="'.$width.'" height="'.$height.'" alt="">';
                    }
                }

                $html[] = "</div>";

            }
        }

        return implode(chr(10),$html);
    }
}

