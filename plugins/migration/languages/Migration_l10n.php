<?php
$l10nText = array(

    'fr' => array(
        'name' => "Migration",
        'desc' => "<p>Outil de migration de données</p>",
        'remote_db' => "DB distante",
        'local_db' => "DB locale",
        'nb_records_select' => "Nb enregistrements sélectionnés",
        'nb_records_target' => "Nb enregistrements DB locale",
        'TRUNCATE_LOCAL' => "Effacer la table locale",
        'IMPORT_DATA' => "Importer les données",
        'complete' => "réalisés.",
        'done' => "Terminé",
        'time' => "Temps écoulé ",
        'table' => "Table",
        'table_del_success' => "effacée avec succès",
        'affected' => " lignes supprimées",
        'start_processing' => "Démarrage du traitement...",
        'importing' => "Importation de",
        'rows' => "lignes",
        'filter_fields_not_array' => "L'attribut 'filter_fields' doit être de type array() dans la règle ",
        'filter_fields_not_defined' => "L'attribut 'filter_fields' n'est pas défini dans la règle ",
        'key_proc_not_array' => "L'attribut 'key_processors' doit être de type array() dans la règle ",
        'key_proc_not_defined' => "L'attribut 'key_processors' n'est pas défini dans la règle ",
        'remap_not_array' => "L'attribut 'remap_fields' doit être de type array() dans la règle ",
        'remap_not_defined' => "L'attribut 'remap_fields' n'est pas défini dans la règle ",
        'check_log' => "Consultez le log de MindFlow pour plus de détails sur la requête concernée.",
        'create_error' => "Erreur lors du CREATE : ",
        'update_error' => "Erreur lors de l'UPDATE : ",
        'import_exit' => "Arrêt prématuré de l'import en raison d'une erreur.",

    ),

);
?>