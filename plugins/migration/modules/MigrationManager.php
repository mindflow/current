<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';


//{module_include_record}

//{module_include_form}

/*
 * In order for ob_flush and flush to work seamlessly in Nginx you can use following configuration:

fastcgi_buffer_size   1k;
	fastcgi_buffers       128 1k;  # up to 1k + 128 * 1k
	fastcgi_max_temp_file_size 0;
	gzip off;
 */


class MigrationManager implements module
{

    private $moduleKey = 'MigrationManager';
    private $moduleType = 'backend';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';

    //import stuff
	var $import_start;
	var $importAction;
	var $target;

	var $sourceDB;
	var $targetDB;
	var $targetRows;
	var $migrationData;
	var $filterfields;
	var $remapFields;
	var $keyProcessors;

	//nombre de lignes a garder en buffer avant de réaliser un envoi de statut JSON
	var $bufferSize = 250;
	var $buffer = array();


	function __construct()
    {

    }


    function prepareData()
    {
        global $mf,$l10n,$config,$pdo;

        $mConfig = $config['plugins']['migration'];

        $this->pageOutput = '';

        //[prepareData_backend]
		if($mf->mode == 'backend')
        {

            //process backend display
            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('migration', 'allowEditMigrationManager') == 1)));

            if ($this->moduleAccess) {
                //add the plugin to the backend menus
                $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=MigrationManager"><span class="glyphicon glyphicon-menu glyphicon-th-large" aria-hidden="true"></span>' . $l10n->getLabel('MigrationManager', 'menu_title') . '</a>', 'administration', LOW_PRIORITY);

                if (isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])) {

                    $this->localMenu = '<li rel="' . $this->moduleKey . '" class="active"><a href="{subdir}/mf/index.php?module=' . $this->moduleKey . '">{menu-title}</a></li>';


                    //breadcrumb
                    $this->section = $l10n->getLabel('backend', 'administration');
                    $this->moduleName = ' / <span class="glyphicon glyphicon-menu glyphicon-th-large" aria-hidden="true"></span>'. $l10n->getLabel('MigrationManager', 'menu_title');
                    $this->subModuleName = "";

                    if (isset($_REQUEST['action'])) $this->action = $_REQUEST['action'];

	                $this->targetDB = $mf->db;
	                $sourceDBs = array();

                    foreach($mConfig['targets'] as $migrationName => $migrationData){

	                    $sourceDBKey = $migrationData['source_db'];

						//open the database
	                    if(!isset($sourceDBs[$sourceDBKey])) {
		                    //open access to secondary database as sourceDB
		                    $dbHost = $mConfig['databases'][$sourceDBKey]['db_host'];
		                    $dbPort = $mConfig['databases'][$sourceDBKey]['db_port'];
		                    $dbName = $mConfig['databases'][$sourceDBKey]['db_name'];
		                    $dbUsername = $mConfig['databases'][$sourceDBKey]['db_username'];
		                    $dbPassword = $mConfig['databases'][$sourceDBKey]['db_password'];
		                    $forceUtf8 = $mConfig['databases'][$sourceDBKey]['db_force_utf8'];

		                    $sourceDBs[ $sourceDBKey ] = new dbManager( $dbHost, $dbPort, $dbName, $dbUsername, $dbPassword, $forceUtf8 );
		                    $sourceDBs[ $sourceDBKey ]->openDB();
	                    }

	                    $sourceSQL = $migrationData['source_query'];

	                    $sql = 'SELECT '.$sourceSQL['SELECT'].' FROM '.$migrationData['source_table'].' WHERE '.$sourceSQL['WHERE'];
	                    $stmt = $sourceDBs[ $sourceDBKey ]->pdo->query($sql);
	                    $numRowsSource = $stmt->rowCount();

	                    $sql = 'SELECT '.$sourceSQL['SELECT'].' FROM '.$migrationData['target_table'].' WHERE '.$sourceSQL['WHERE'];
	                    $stmt = $this->targetDB->pdo->query($sql);
	                    $numRowsTarget = $stmt->rowCount();

	                    $this->pageOutput .= "<h3>".$migrationName."</h3>";
	                    $this->pageOutput .= "<p><strong>".$l10n->getLabel('Migration','remote_db')." :</strong> ".$config['plugins']['migration']['databases'][$migrationData['source_db']]['db_host'].'.'.$config['plugins']['migration']['databases'][$migrationData['source_db']]['db_name']."<strong>&nbsp;&nbsp;&nbsp;&nbsp;".$l10n->getLabel('Migration','nb_records_select')." :</strong> ".$numRowsSource."</p>";
	                    $this->pageOutput .= "<p><strong>".$l10n->getLabel('Migration','local_db')." :</strong> ".$config['db_host'].'.'.$config['db_name']." <strong>&nbsp;&nbsp;&nbsp;&nbsp;".$l10n->getLabel('Migration','nb_records_target')." :</strong> ".$numRowsTarget."</p>";

	                    $this->pageOutput .= "
							<p>
								<a href=\"javascript:launchCommand('IMPORT_DATA','".$migrationName."')\">".$l10n->getLabel('Migration','IMPORT_DATA')."</a>&nbsp;&nbsp;|&nbsp;&nbsp;
								<a class=\"red\" href=\"javascript:launchCommand('TRUNCATE_LOCAL','".$migrationName."')\">".$l10n->getLabel('Migration','TRUNCATE_LOCAL')."</a>
								
							</p><br />";


	                    //$this->pageOutput .= "action = " . $this->action;
                    }


	                $this->pageOutput .= '
					<div id="feedbackDisplay" style="display:none;">
		                <div class="progress">
					        <div class="progress-bar" role="progressbar" aria-valuenow="70"
					             aria-valuemin="0" aria-valuemax="100" style="width:0">
					            <span id="barMessage"></span>
					        </div>
					    </div>
					    <textarea id="textMessage" style="width:100%;height:200px;"></textarea><br/>
					    <button type="button" class="btn closeButton" onclick="closeDialog();" >'.$l10n->getLabel('backend','close').'</button><br/>
	                </div>
	                
	                ';

	                $this->pageOutput .= "<script type='text/javascript'>

					var newDialog;
					var ajaxRequest;
					
					// as there is no way to abort a previous ajax request,
					// allows ignoring response of first request when a secondary request is launched
					var xhrCount = 0;
					
					function launchCommand(p_action, p_target){
					    
						var seqNumber = ++xhrCount;
						console.log('launchCommand('+p_action+', '+p_target+')')
						
						//reset the display
						$('#textMessage').val('');
					    $('#barMessage').text('');
					    $('.progress-bar').css('width', '0%');
						
					    //newDialog = $('#' + p_command+'_'+p_target).dialog({
					    newDialog = $('#feedbackDisplay').dialog({
			                autoOpen: false,
			            });
					    
					    //build our dialog
			            newDialog.dialog({
			                title: 'Migration',
			                closeText: 'hide',
			                requestFailed: 'Request failed. Please try again',
			                width: '80%',
			                position: {my: 'center', at: 'top+20%', of: window},
			                modal: true
			                //show: { effect: 'fadeIn', duration: 500 }
			            });
			            
			            $('#feedbackDisplay').show();
					    newDialog.dialog('open');

					    
	                    var lastResponseLength = false;
					    var ajaxRequest = $.ajax({
					        type: 'post',
					        url: '".SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/migration/migration.php'."',
					        data: { action: p_action, target: p_target },
					        dataType: 'json',
					        processData: true,
					        xhrFields: {
					            // Getting on progress streaming response
					            onprogress: function(e)
					            {
					                // this works because of the way closures work
							        if (seqNumber === xhrCount) {
							            //Process the response
							            var progressResponse;
						                var response = e.currentTarget.response;
						                if(lastResponseLength === false)
						                {
						                    progressResponse = response;
						                    lastResponseLength = response.length;
						                }
						                else
						                {
						                    progressResponse = response.substring(lastResponseLength);
						                    lastResponseLength = response.length;
						                }
						                var parsedResponse = JSON.parse(progressResponse);
						                $('#textMessage').val($('#textMessage').val()+parsedResponse.text_message);
						                $('#barMessage').text(parsedResponse.bar_message);
						                $('.progress-bar').css('width', parsedResponse.progress + '%');
							        } 
							        else {
							            //Ignore the response because it originates from a previous session
							        }
					                
					            }
					        }
					    });
					    // On completed
					    ajaxRequest.done(function(data)
					    {
					        console.log('Complete response = ' + data);
					        
					    });
					    // On failed
					    ajaxRequest.fail(function(error){
					        console.log('Error: ', error);
					    });
					    console.log('Request Sent');
					}
					
					function closeDialog(){
					    newDialog.dialog('close');
					    
					    //reset the display
						$('#textMessage').val('');
					    $('#barMessage').text('');
					    $('.progress-bar').css('width', '0%');
					}
			         </script>";

                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >' . $l10n->getLabel('backend', 'module_no_access') . '</div>';
                $this->localMenu = '';
            }
        }
		//[/prepareData_backend]
		//{module_prepareData}
    }


    function render(&$mainTemplate)
    {
        global $mf,$l10n,$config;

        //[render_backend]
		if($mf->mode == 'backend')
        {
            //process backend display
            if (isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])) {

                $moduleBody = file_get_contents(DOC_ROOT . SUB_DIR . $config['website_pluginsdir'] . '/migration/ressources/templates/MigrationManager.html');
                $moduleBody = str_replace("{module-body}", $this->pageOutput, $moduleBody);

                $moduleBody = str_replace("{local-menu}", $this->localMenu, $moduleBody);
                $moduleBody = str_replace("{menu-title}", $l10n->getLabel('MigrationManager', 'menu_title'), $moduleBody);

                $mainTemplate = str_replace("{current_module}", $moduleBody, $mainTemplate);
                //breadcrumb
                $mainTemplate = str_replace("{section}", $this->section, $mainTemplate);
                $mainTemplate = str_replace("{module-name}", $this->moduleName, $mainTemplate);
                $mainTemplate = str_replace("{submodule-name}", $this->subModuleName, $mainTemplate);

            }
        }
		//[/render_backend]
		//{module_render}
    }


    function getType()
    {
        return $this->moduleType;
    }

    //{module_list_function}

	function import($action, $target){
    	
		//syntax shortcuts for accessing configuration
		global $mf, $l10n, $config ;

		$this->import_start = microtime(true);
		$this->importAction = $action;
		$this->target = $target;

		//notify processing is starting
		$this->updateStatus($l10n->getLabel('Migration','start_processing').chr(10), 0);

		$mConfig = $config['plugins']['migration'];
		$this->migrationData = $mConfig['targets'][$this->target];
		$sourceDBKey = $this->migrationData['source_db'];

		//open access to secondary database as sourceDB
		$dbHost = $mConfig['databases'][$sourceDBKey]['db_host'];
		$dbPort = $mConfig['databases'][$sourceDBKey]['db_port'];
		$dbName = $mConfig['databases'][$sourceDBKey]['db_name'];
		$dbUsername = $mConfig['databases'][$sourceDBKey]['db_username'];
		$dbPassword = $mConfig['databases'][$sourceDBKey]['db_password'];
		$forceUtf8 = $mConfig['databases'][$sourceDBKey]['db_force_utf8'];

		//open the database
		$this->sourceDB = new dbManager($dbHost, $dbPort, $dbName, $dbUsername, $dbPassword, $forceUtf8);
		$this->sourceDB->openDB();

		//target/local/mindflow db. Database is already opened.
		$this->targetDB = $mf->db;

		if ( isset( $this->migrationData['filter_fields'] ) ) {
			if ( is_array( $this->migrationData['filter_fields'] ) ) {
				$this->filterfields = $this->migrationData['filter_fields'];
			} else {
				$this->updateStatus( $l10n->getLabel( 'Migration', 'filter_fields_not_array' ) . $this->target . chr( 10 ), 0 );
				$this->filterfields = array();
			}
		} else {
			$this->updateStatus( $l10n->getLabel( 'Migration', 'filter_fields_not_defined' ) . $this->target . chr( 10 ), 0 );
			$this->filterfields = array();
		}

		if ( isset( $this->migrationData['key_processors'] ) ) {
			if ( is_array( $this->migrationData['key_processors'] ) ) {
				$this->keyProcessors = $this->migrationData['key_processors'];
			} else {
				$this->updateStatus( $l10n->getLabel( 'Migration', 'key_proc_not_array' ) . $this->target . chr( 10 ), 0 );
				$this->keyProcessors = array();
			}
		} else {
			$this->updateStatus( $l10n->getLabel( 'Migration', 'key_proc_not_defined' ) . $this->target . chr( 10 ), 0 );
			$this->keyProcessors = array();
		}

		if ( isset( $this->migrationData['remap_fields'] ) ) {
			if ( is_array( $this->migrationData['remap_fields'] ) ) {
				$this->remapFields = $this->migrationData['remap_fields'];
			} else {
				$this->updateStatus( $l10n->getLabel( 'Migration', 'remap_not_array' ) . $this->target . chr( 10 ), 0 );
				$this->remapFields = array();
			}
		} else {
			$this->updateStatus( $l10n->getLabel( 'Migration', 'remap_not_defined' ) . $this->target . chr( 10 ), 0 );
			$this->remapFields = array();
		}


		switch($this->importAction){
			case 'TRUNCATE_LOCAL':
				$sql = 'TRUNCATE TABLE '.$this->migrationData['target_table'];
				$numRows = $this->targetDB->pdo->exec($sql);

				$processed = 100;
				$message = $l10n->getLabel('Migration','table').' '.$this->migrationData['target_table'].' '.$l10n->getLabel('Migration','table_del_success').'.'.chr(10);
				$this->updateStatus($message, $processed);
				break;

			case 'IMPORT_DATA':
				$processed = 0;

				//select rows to be imported from source
				$sql = 'SELECT '.$this->migrationData['source_query']['SELECT'].' FROM '.$this->migrationData['source_table'].' WHERE '.$this->migrationData['source_query']['WHERE'];
				$this->updateStatus('QUERY : '.$sql.chr(10), $processed);
				try {
					$sourceStmt = $this->sourceDB->pdo->query( $sql );
				}
				catch(Exception $e){
					$comment='';

					if($e->getCode() == '42S02') $comment = $l10n->getLabel('main','plugin_table_error1').' '.$l10n->getLabel('main','plugin_table_error2');

					$this->updateStatus($e->getMessage().chr(10), $processed);
					if($comment!='')$this->updateStatus($comment.chr(10), $processed);

				}

				//select existing rows in the target database
				$sql = 'SELECT '.$this->migrationData['source_query']['SELECT'].' FROM '.$this->migrationData['target_table'].' WHERE '.$this->migrationData['source_query']['WHERE'];
				//$this->updateStatus('QUERY : '.$sql.chr(10), $processed);
				try {
					$targetStmt = $this->targetDB->pdo->query( $sql );
				}
				catch(Exception $e){
					$comment='';

					if($e->getCode() == '42S02') $comment = $l10n->getLabel('main','plugin_table_error1').' '.$l10n->getLabel('main','plugin_table_error2');

					$this->updateStatus($e->getMessage().chr(10), $processed);
					if($comment!='')$this->updateStatus($comment.chr(10), $processed);
				}

				//retreive all local rows for the table, this data will be useful for the import function
				$rows = $targetStmt->fetchAll(PDO::FETCH_ASSOC);

				//index the rows according to their primary key
				foreach ($rows as $row){
					$this->targetRows[ $row[ $this->migrationData['target_primary_key'] ] ] = $row;
				}

				$this->updateStatus($l10n->getLabel('Migration','importing').' '.$sourceStmt->rowCount().' '.$l10n->getLabel('Migration','rows').chr(10), $processed);

				//get the user defined importFunction or use default function importRow()
				if(!isset($this->migrationData['import_function']))$importFunction = 'MigrationManager::importRow';
				else $importFunction = $this->migrationData['import_function'];

				$counter = 0;
				while($sourceRow = $sourceStmt->fetch(PDO::FETCH_ASSOC)) {
					//update the JSON status
					$processed = round($counter++ * 100 / $sourceStmt->rowCount(),3,PHP_ROUND_HALF_EVEN);
					$this->updateStatus($l10n->getLabel('Migration','importing').' primaryKey='.$sourceRow[ $this->migrationData['source_primary_key'] ].chr(10), $processed);

					//invoke the import function
					$importFunction($sourceRow, $processed, $this);
				}


				break;
		}

		$this->statusComplete($l10n->getLabel('Migration','done').'.');
	}




	/**
	 * Imports a row into the target DB
	 * @param $row
	 */
	static function importRow($sourceRow, $progress, $migrationManager){

		global $l10n, $logger;

		$sourcePrimaryKey = $migrationManager->migrationData['source_primary_key'];
		$targetPrimaryKey = $migrationManager->migrationData['target_primary_key'];

		if ( isset($migrationManager->targetRows[$sourceRow[$sourcePrimaryKey]]) ) {

			//If the row already exists, UPDATE
			$sql = "UPDATE " . $migrationManager->migrationData['target_table'] . " SET ";

			//for each column in the row
			foreach ( $sourceRow as $keyName => $keyData ) {

				//filtering fields forbiden in the IN rule
				if ( ! in_array( $keyName, $migrationManager->filterfields ) ) {

					//remap field if necessary
					if ( isset( $migrationManager->remapFields[ $keyName ] ) ) {
						$keyName = $migrationManager->remapFields[ $keyName ];
					}

					//caution : keyProcessor applies on the remaped field name !
					if ( isset( $migrationManager->keyProcessors[ $keyName ] ) ) {

						//process key for display with optional user supplied processing function in the IN rule
						eval( '$value = ' . $migrationManager->keyProcessors[ $keyName ] . '($keyName,$keyData,$sourceRow["language"],$sourceRow);' );

						//set SQL
						$sql .= "`".$keyName."` = " . $value . ',';
					} else {

						//set SQL
						$sql .= "`".$keyName ."` = " . $migrationManager->targetDB->pdo->quote( $keyData ) . ',';
					}
				}
			}
			$sql = rtrim( $sql, ", " );
			$sql .= " WHERE ".$targetPrimaryKey." = " . intval( $sourceRow[$sourcePrimaryKey] ) . ";";

			try {
				$updateStmt = $migrationManager->targetDB->pdo->query( $sql );
			} catch ( PDOException $e ) {
				$logger->critical($targetPrimaryKey." = " . intval( $sourceRow[$sourcePrimaryKey] )." , ".$l10n->getLabel( 'Migration', 'update_error' ). chr( 10 ) .
				                  "ERROR=" . $migrationManager->targetDB->pdo->errorInfo()[2]. chr( 10 ). //, $progress);
				                  "SQL=" . $sql . chr( 10 ) . chr( 10 ));
				$migrationManager->updateStatus( $targetPrimaryKey." = " . intval( $sourceRow[$sourcePrimaryKey] )." , ".$l10n->getLabel( 'Migration', 'update_error' ). chr( 10 ) ."ERROR=" . $migrationManager->targetDB->pdo->errorInfo()[2]. chr( 10 ). $l10n->getLabel( 'Migration', 'check_log' ). chr( 10 ), $progress);
				$migrationManager->statusComplete($l10n->getLabel( 'Migration', 'import_exit' ));
				die();
			}

		} else {
			//CREATE

			//prepare SQL
			$sql = "INSERT INTO " . $migrationManager->migrationData['target_table'] . " SET ";

			foreach ( $sourceRow as $keyName => $keyData ) {

				//filtering fields forbiden in the IN rule
				if ( ! in_array( $keyName, $migrationManager->filterfields ) ) {

					if ( isset( $migrationManager->keyProcessors[ $keyName ] ) ) {

						//remap field if necessary
						if ( isset( $migrationManager->remapFields[ $keyName ] ) ) {
							$keyName = $migrationManager->remapFields[ $keyName ];
						}

						//process key for display with optional user supplied processing function in the IN rule
						eval( '$value = ' . $migrationManager->keyProcessors[ $keyName ] . '($keyName,$keyData,$sourceRow["language"],$sourceRow);' );

						//set SQL
						$sql .= "`".$keyName ."` =" . $value . ',';

					} else {
						//remap field if necessary
						if ( isset( $migrationManager->remapFields[ $keyName ] ) ) {
							$keyName = $migrationManager->remapFields[ $keyName ];
						}

						//set SQL
						$sql .= "`".$keyName ."` =" . $migrationManager->targetDB->pdo->quote( $keyData ) . ',';
					}
				}
			}

			$sql = rtrim( $sql, ", " ) . ';';

			try {
				$createStmt = $migrationManager->targetDB->pdo->query( $sql );


			} catch ( PDOException $e ) {
				$logger->critical($targetPrimaryKey." = " . intval( $sourceRow[$sourcePrimaryKey] )." , ".$l10n->getLabel( 'Migration', 'create_error' ). chr( 10 ) .
				                  "ERROR=" . $migrationManager->targetDB->pdo->errorInfo()[2]. chr( 10 ). //, $progress);
				                  "SQL=" . $sql . chr( 10 ) . chr( 10 ));
				$migrationManager->updateStatus( $targetPrimaryKey." = " . intval( $sourceRow[$sourcePrimaryKey] )." , ".$l10n->getLabel( 'Migration', 'create_error' ). chr( 10 ) ."ERROR=" . $migrationManager->targetDB->pdo->errorInfo()[2]. chr( 10 ).$l10n->getLabel( 'Migration', 'check_log' ). chr( 10 ), $progress);
				$migrationManager->statusComplete($l10n->getLabel( 'Migration', 'import_exit' ));
				die();
			}
		}

	}



	/**
	 * Sends a JSON message to the MigrationManager module to update the progress bar and status text
	 * @param string $message text message to be displayed in the remote status
	 * @param int $progress a percent from 0 to 100 for updating the status bar
	 * @param $numRowsSource
	 * @param $numRowsTarget
	 */
	function updateStatus($message, $progress){
		global $l10n, $bufferSize;

		$this->buffer[] = $message;

		if(count($this->buffer) >= $this->bufferSize) {
			$currentTime = microtime( true );
			//date("h:i:s.u", $currentTime - $this->import_start )
			$response = array(
				'text_message' => implode('',$this->buffer),
				'bar_message'  => $progress . '% ' . $l10n->getLabel( 'Migration', 'complete' ) . ' ' . $l10n->getLabel( 'Migration', 'time' ) . ': ' . round( $currentTime - $this->import_start, 1, PHP_ROUND_HALF_EVEN ) . 's',
				'progress'     => $progress,
				'action'       => $this->importAction,
				'target'       => $this->target
			);
			$this->buffer = array();

			echo json_encode( $response ) . PHP_EOL;

			// Pause de 150 ms sinon erreur JSON
			usleep( 150000 );
		}

	}

	function statusComplete($message){

		global $l10n;

		//empty the buffer
		$currentTime = microtime( true );
		//date("h:i:s.u", $currentTime - $this->import_start )
		$response = array(
			'text_message' => implode('',$this->buffer),
			'bar_message'  => '100% ' . $l10n->getLabel( 'Migration', 'complete' ) . ' ' . $l10n->getLabel( 'Migration', 'time' ) . ': ' . round( $currentTime - $this->import_start, 1, PHP_ROUND_HALF_EVEN ) . 's',
			'progress'     => "100",
			'action'       => $this->importAction,
			'target'       => $this->target
		);
		$this->buffer = array();

		echo json_encode( $response ) . PHP_EOL;

		// Pause de 150 ms sinon erreur JSON
		usleep( 150000 );


		//output last message
		$currentTime = microtime(true);
		$response = array(
			'text_message' => $message,
			'bar_message' => $l10n->getLabel('Migration','done').'. '.$l10n->getLabel('Migration','time').': ' .round ($currentTime - $this->import_start, 1,PHP_ROUND_HALF_EVEN).'s',
			'progress' => "100",
			'action' => $this->importAction,
			'target' => $this->target
		);
		$this->buffer = array();
		echo json_encode($response). PHP_EOL;
	}
}

