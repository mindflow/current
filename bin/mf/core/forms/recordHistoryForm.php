<?php

require_once DOC_ROOT.SUB_DIR.'/mf/core/dbForm.php';

class recordHistoryForm extends dbForm
{

    static $postFormHTML = array(); //important declaration, do not remove (would cause bug)

    function init()
    {
        global $mf,$l10n;
	    $l10n->loadL10nFile('recordHistoryForm', '/mf/languages/recordHistoryForm_l10n.php');
        parent::init();

        $locale = $GLOBALS['config']['default_frontend_locale'];


        $this->data = array_merge($this->data, array(
//            'titreForm' => array(
//                'value' => '<h3>' . $l10n->getLabel('importDepartsForm1', 'titreForm') . '</h3><br />
//				<div><div class="row"><div class="col-lg-9 col-lg-offset-3">Sélectionnez un fichier grille de prix de circuits au format Excel pour importer les dates des départs.<br /><br /></div></div>',
//                'dataType' => 'html',
//            ),
	        'target_form_id' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'history_uid' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'history_class' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'history_field' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'mfSetLocale' => array(
		        'value' => $mf->getLocale(),
		        'dataType' => 'hidden',
	        ),
	        'submodule' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'showSaveButton' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'showSaveCloseButton' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'showPreviewButton' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'showCloseButton' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'ajaxEditing' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'callbackFunction' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'recordEditTableID' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'advancedSettings' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),
	        'formAttributes' => array(
		        'value' => '',
		        'dataType' => 'hidden',
	        ),


	        'changes' => array(
		        'value' => '',
		        'dataType' => 'select',
		        'valueType' => 'text',
		        'possibleValues' => array(),
		        'processor' => '',
		        'div_attributes' => array('class' => 'col-lg-4'),
		        'field_attributes' => array(
			        'onchange' => 'getHistoryFields();'
		        ),
	        ),
	        'history_display' => array(
		        'value' => '<div id="history_display"></div>',
		        'dataType' => 'html',
	        ),

        ));


        $this->showInEditForm = array(
            'tab_main' => array('target_form_id','history_uid','history_class','history_field','changes','history_display',
	            'submodule','showSaveButton','showSaveCloseButton','showPreviewButton','showCloseButton',
	            'ajaxEditing','callbackFunction','recordEditTableID','advancedSettings','formAttributes')
        );



        static::$postFormHTML[] = '<script>

		function getHistoryFields(timestamp){
		    formID = $("#record\\\\|changes").closest("form").attr("id");
		    fieldName = $("#"+formID+" #record\\\\|history_field").val();
		    
		    var imgCode = "<div style=\"width:100%;text-align:center;\"><div class=\"savingNotification\"><img src=\"'.SUB_DIR.$mf->xEnd->template.'/img/360.gif\" width=\"24\" height=\"24\" alt=\"\" /></div></div>";

            $("#"+formID+" #history_display").html(imgCode);
		
		    $.ajax({
                url: $("#"+formID).attr("action") ,
                type: $("#"+formID).attr("method"),
                data: $("#"+formID).serialize(),
                dataType: "json",
                success: function(jsonData) {
                    if(jsonData.result == "success"){
                        $("#"+formID+" #history_display").html(jsonData.fieldsHTML);
                    }
                }
		    });
		}
		
		function restoreFieldHistory(fieldName,fieldButton,dataType){
		    targetFormID = $("#record\\\\|target_form_id").val();
		    historyFormID = fieldButton.closest("form").attr("id");
		    
		    //get restored value
		    
		    
		    //change target field
		    switch(dataType){
		        case "input":
		        case "select":
		        case "date":
		        case "datetime":
		        case "datetime.ms":
		        case "time":
		        case "urlinput":
		        case "altLang":
		        case "textarea":
		        
		            restoreFieldValue = $("#"+historyFormID+" #noedit\\\\|"+fieldName).val();
		            console.log("#"+historyFormID+" #noedit\\\\|"+fieldName);
		            console.log("restoreFieldValue="+restoreFieldValue);
		    		$("#"+targetFormID+" #record\\\\|"+fieldName).val(restoreFieldValue);
		    		console.log("#"+targetFormID+" #record\\\\|"+fieldName);
		    		console.log($("#"+targetFormID+" #record\\\\|"+fieldName).val());
		    		break;
		    	
		    	case "checkbox":
		    	    //we need to restore both the checkbox and the hidden field
		    	    restoreFieldValue = $("#"+historyFormID+" #noedit\\\\|"+fieldName).val();
		    	    restoreFieldCheck = $("#"+historyFormID+" #noedit\\\\|"+fieldName+"_check").prop("checked");
		    	    
		    	    $("#"+targetFormID+" #record\\\\|"+fieldName).val(restoreFieldValue);
		    		$("#"+targetFormID+" #record\\\\|"+fieldName+"_check").prop("checked",restoreFieldCheck);
		    	    break;
		    	    
		    	case "rich_text":   
		    	    restoreFieldValue = $("#"+historyFormID+" #noedit\\\\|"+fieldName).html();
		    	    CKEDITOR.instances["record|"+fieldName].setData(restoreFieldValue);
		    	    break;
		    	    
		    	case "radio":
		    	    restoreFieldValue = $("#"+historyFormID+" #noedit\\\\|"+fieldName).val();
		    	    restoreFieldCheck = $("#"+historyFormID+" #'.$mf->mode.'"+fieldName+"_"+restoreFieldValue).prop("checked");
		    	    selectedRadioId="record\\\|"+fieldName+"_"+restoreFieldValue;
		    	    $("#"+targetFormID+" #"+selectedRadioId).prop("checked",restoreFieldCheck);
		    	    $("#"+targetFormID+" #record\\\\|"+fieldName).val(restoreFieldValue);
		    	    break;
		    	
		    	case "record":
		    	    
		    	break;
		    }
		    
		    alert("'.$l10n->getLabel('main','value_restored').'");
		    mf.dialogManager.closeLastDialog();
		}
		
		';


	    $secKeys = array(
		    'action' => 'historyRestoreRecord',
		    'mfSetLocale' => $mf->getLocale()
	    );
	    $sec = getSec($secKeys);


	    static::$postFormHTML[] = '
        function restoreRecord(){
		    formID = $("#record\\\\|changes").closest("form").attr("id");
		    targetFormID = $("#record\\\\|target_form_id").val();
		    selectedTimestamp = $("#"+formID+" #record\\\\|changes").val();
		   
            $("#"+formID+" #action").val("historyRestoreRecord");
            $("#"+formID+" #sec").val("'.$sec->hash.'");
            $("#"+formID+" #fields").val("'.$sec->fields.'");
            
            $("#"+formID+" #record\\\\|submodule").val($("#"+targetFormID+" #submodule").val());
            $("#"+formID+" #record\\\\|showSaveButton").val($("#"+targetFormID+" #showSaveButton").val());
            $("#"+formID+" #record\\\\|showSaveCloseButton").val($("#"+targetFormID+" #showSaveCloseButton").val());
            $("#"+formID+" #record\\\\|showSaveButton").val($("#"+targetFormID+" #showSaveButton").val());
            $("#"+formID+" #record\\\\|showPreviewButton").val($("#"+targetFormID+" #showPreviewButton").val());
            $("#"+formID+" #record\\\\|showCloseButton").val($("#"+targetFormID+" #showCloseButton").val());
            $("#"+formID+" #record\\\\|ajaxEditing").val($("#"+targetFormID+" #ajaxEditing").val());
            $("#"+formID+" #record\\\\|callbackFunction").val($("#"+targetFormID+" #callbackFunction").val());
            $("#"+formID+" #record\\\\|recordEditTableID").val($("#"+targetFormID+" #recordEditTableID").val());
            $("#"+formID+" #record\\\\|advancedSettings").val($("#"+targetFormID+" #advancedSettings").val());
            $("#"+formID+" #record\\\\|formAttributes").val($("#"+targetFormID+" #formAttributes").val());
            
		
		    $.ajax({
                url: $("#"+formID).attr("action") ,
                type: $("#"+formID).attr("method"),
                data: $("#"+formID).serialize(),
                dataType: "json",
                success: function(jsonData) {
                    if(jsonData.result == "success"){
                        console.log(jsonData);
                        //update the form with the AJAX received data 
                        newFormID = substituteForm(targetFormID,jsonData.form,jsonData.uid); 
                        alert("'.$l10n->getLabel('main','value_restored').'");
		    			mf.dialogManager.closeLastDialog();
                    }
                }
		    });
		}

		</script>';
    }



    function getHTML(){

    	global $mf,$l10n;

	    $actionURL = SUB_DIR.'/mf/core/ajax-core-json.php';
	    $formAction = 'selectRecordHistoryEntry';

	    $showSubmitButton = true;
	    $submitButtonLabel = $l10n->getLabel('main','restore_version');
	    $useAJAX = true;
	    $submitButtonClass = 'btn btn-primary col-lg-offset-4';

	    $advancedSettings = array(
		    'showCancelButton' => true,
		    'cancelButtonOnClick' => "mf.dialogManager.closeLastDialog();",
		    'submitButtonOnClick' => "restoreRecord();",
	    );

	    //Nom de la fonction javascript qui prendra le relais après le traitement du formulaire
	    $callBackJSFuncName = 'restoreHistoryStep2';

	    $formsManager = $mf->formsManager;


	    return $formsManager->editForm($actionURL, $formAction, $callBackJSFuncName, $this, 'moduleName', '', $showSubmitButton, $submitButtonLabel,$submitButtonClass,$useAJAX,$advancedSettings); //,$formAttributes=null

    }


}