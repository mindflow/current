<?php
/*
   Copyright 2017 Alban Cousinié <info@mindflow-framework.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * Default configuration file for Monolog logger with MindFlow
 *
 * Defines a PHP Style configuration for the Monolog logger. This default configuration can be overrided by pointing
 * to the relative path of your own configuration inside $GLOBALS['config']['monolog'] variable inside your website / app configuration file.
 *
 * See https://github.com/Seldaek/monolog for configuration instructions
 *
 * The logger is available as global variable named $logger and can be used like this :
 *
 *  $logger->info('An info');
 *  $logger->emergency('An emergency');
 *  $logger->alert('An alert');
 *  $logger->critical('A critical message');
 *  $logger->error('An error');
 *  $logger->warning('A warning');
 *  $logger->notice('A notice');
 *  $logger->debug('A debug');
 */

// Sample file log handler, with rotation over 10 files on 10 days
$logger->pushHandler(new \Monolog\Handler\RotatingFileHandler(DOC_ROOT.SUB_DIR.'/mf_logs/mindflow.log', 10,\Monolog\Logger::DEBUG));


/*
// Sample email handler, more complicated :
// Create the swiftmailer Transport
require_once DOC_ROOT.SUB_DIR.'/mf_librairies/Swift-5.4.5/lib/swift_required.php';

// Error mails will be sent by SMTP
$transport = Swift_SmtpTransport::newInstance($config['siteEmailSMTPServer'], $config['siteEmailSMTPPort'])
    ->setUsername($config['siteEmailSMTPUsername'])
    ->setPassword($config['siteEmailSMTPPassword']);

// SSL/TLS encryption is supported if configured
if(isset($config['siteEmailSMTPEncryption']) && $config['siteEmailSMTPEncryption'] != null)
    $transport->setEncryption($config['siteEmailSMTPEncryption']);

// Create our Mailer using the created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Create a template message without body (the body will be filled by the content of the error message)
$errorMailTemplate = (new Swift_Message('Mindflow error')) // <-- Email subject is defined here
    ->setFrom([$config['siteEmailSMTPEmail'] => $config['siteEmailName']])
    ->setTo([$config['mail_admin']]);

// Handler for sending emails on critical and worst errors
$mailHandler = new \Monolog\Handler\SwiftMailerHandler($mailer, $errorMailTemplate,\Monolog\Logger::INFO,true);

// Handler for grouping all errors in a single email rather than receiving an email per error
$groupedMailHandler = new \Monolog\Handler\BufferHandler($mailHandler, $bufferLimit = 0, \Monolog\Logger::INFO, true, true);

// Send the whole log by email only if a critical or worst error log entry has been met
$fcGroupedMailHandler = new \Monolog\Handler\FingersCrossedHandler($groupedMailHandler, \Monolog\Logger::CRITICAL, $bufferSize = 0, $bubble = true, $stopBuffering = true, $passthruLevel = null);

// Finally, apply our handlers stack defined above
$logger->pushHandler($fcGroupedMailHandler);
*/


/*
// Sample firePHP handler
$logger->pushHandler(new \Monolog\Handler\FirePHPHandler());
*/