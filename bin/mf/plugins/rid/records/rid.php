<?php
/*
   Copyright 2017 Alban Cousinié <info@mindflow-framework.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/records/simpleRecord.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

/**
 * Ressource ID storage acts as a global memory for MindFlow and websites.
 *
 * Ressource ID storage enables the programmer to store arbitrary content either in the session or in the database, and specify an expiration interval. Each content has an 'rid' which is a unique key that allows accessing the content.
 * If the content is stored in the session, it can last at most the session duration. But if it is stored in the database, then it can last forever.
 * Specifying an expiry date allows you to store some content and forget it. It will be automatically deleted when the expiration delay will be met.
 * If the value is expired, the get function will return null, so you must always check the returned value against null.
 * Do not forget to call cron.php in your website hosting environment, as it is required to launch the cleanup on a regular basis.
 *
 * Sample usage :
 * Put the value "test1" into the database with rid 'mytest1' and make it expire after 1 minute
 * rid::put("test","db",'PT1M','mytest1');
 *
 * Now read our value
 * echo "value=".rid::get('mytest1');
 * //before 1 minute delay, outputs "value=test"
 * //after 1 minute delay, outputs "value="
 *
 * It is not mandatory to assign an rid. You can let the class decide one for you by not specifying any when calling the put() function, then reuse it.
 * The rid value is returned when executing function put()
 *
 * $myRid = rid::put("test2","session",'PT1M');
 * echo "value=".rid::get($myRid);
 * //before 1 minute delay, outputs "value=test2"
 *
 *
 */
class rid extends simpleRecord
{
    //table name in the SQL database
    static $tableName = 'mf_rid';

    static $createTableSQL= "
        `rid` varchar(255),
        `content` longblob,
        `expiration` int(11) NOT NULL DEFAULT 0,
    ";

    static $createTableKeysSQL="
            PRIMARY KEY  (`rid`)
        ";


    /**
     * Puts a ressource in memory (session or database). If you want to store a value permanently, use database
     *
     * @param $content whatever variable you want to store
     * @param $type string 'session' or 'db' in order to store the ressource where due.
     * @param $expire string Time interval until the record expires specified at the DateInterval creation format (ex : 'P2Y4DT6H8M'). Use 0 for unlimited storage. If $type = 'session' the maximum expiration for storage is limited by the PHP session duration.
     * @param $userRid string A user defined unique identifier
     * @return string the autogenerated ressource identifier
     */
    static function put($content,$type,$expiration,$userRid=null){

        global $pdo;

        if(!isset($userRid))$rid = time().'_'.rand();
        else $rid = $userRid;

        $entry = new stdClass();
        $entry->rid = $rid;
        $entry->content = $content;

        if($expiration === 0) {
            $entry->expiration = 0;
        }
        else{
            $exp = new DateTime();
            $exp->add(new DateInterval($expiration));
            $entry->expiration = $exp->format('U');
        }

        if($type=='session'){
            if(!isset($_SESSION['rid']))$_SESSION['rid']=array();

            $_SESSION['rid'][$rid] = $entry;
        }
        else if($type=='db'){
            $sql = 'SELECT * FROM '.self::getTableName().' WHERE rid='.$pdo->quote($rid);
            $stmt = $pdo->query($sql);

            if($stmt->rowCount()>0) {
                $sql = 'DELETE FROM '.self::getTableName().' WHERE rid='.$pdo->quote($rid);
                $pdo->query($sql);
            }

            $sql = 'INSERT INTO '.self::getTableName().' SET rid='.$pdo->quote($entry->rid).', content='.$pdo->quote($entry->content).', expiration='.$entry->expiration;
            $pdo->query($sql);
        }

        return $rid;
    }

    /**
     * Returns the content matching the rid
     * @param $rid the rid associated with the content stored
     * @param $delete boolean the rid entry will be deleted after the read operation if set to true
     * @return the content stored or null if the value is expired
     */
    static function get($rid, $delete=false){

        global $pdo;

        if(isset($_SESSION['rid'][$rid])){

            $entry = $_SESSION['rid'][$rid];

            if($entry->expiration === 0 || $entry->expiration >= time()){

                if($delete)unset($_SESSION['rid'][$rid]);

                return $entry->content;
            }
            else{
                unset($_SESSION['rid'][$rid]);
                return null;
            }
        }
        else{
            $sql = 'SELECT * FROM '.self::getTableName().' WHERE rid='.$pdo->quote($rid);
            $stmt = $pdo->query($sql);

            if($stmt->rowCount()>0){
                $entry = $stmt->fetch(PDO::FETCH_OBJ);

                if($entry->expiration === 0 || $entry->expiration >= time()) {
                    if($delete){
                        $sql = 'DELETE FROM '.self::getTableName().' WHERE rid='.$pdo->quote($rid);
                        $pdo->query($sql);
                    }
                    return $entry->content;
                }
                else{
                    $sql = 'DELETE FROM '.self::getTableName().' WHERE rid='.$pdo->quote($rid);
                    $pdo->query($sql);
                    return null;
                }
            }
            else return null;
        }

    }

    /**
     * Deletes expired records. Meant to be launched from a cron task
     */
    static function cleanup(){
        global $pdo;

        //clean up session
        if(isset($_SESSION['rid'])){
            foreach($_SESSION['rid'] as $index => $entry){

                echo "cleanup()".chr(10);
                echo "(entry->expiration !== 0 )=".($entry->expiration !== 0).chr(10);
                echo "(entry->expiration < time() )=".($entry->expiration < time()).chr(10);
                echo "time() =".time().chr(10);

                if($entry->expiration !== 0 && $entry->expiration < time()) unset($_SESSION['rid'][$index]);
            }
        }

        //clean up db
        $sql = 'DELETE FROM '.self::getTableName().' WHERE (expiration <='.time().' AND expiration != 0);';
        $pdo->query($sql);
    }

    /**
     * Completely empties the ressource id storage : deletes all records, even if they are not expired.
     */
    static function cleanAll(){
        global $pdo;

        //clean up session
        $_SESSION['rid'] = array();

        //clean up db
        $sql = 'DELETE FROM '.self::getTableName().' WHERE 1';
        $pdo->query($sql);
    }

}















