<?php

/*
 * Module backend pour l'édition des profils utilisateur.
 */


require_once DOC_ROOT.SUB_DIR.'/mf/core/interfaces/module.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/formsManager.php';
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/my_plugin/records/myContact.php';
require_once DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/my_plugin/forms/myPluginForm.php';

class editMyContacts implements module{

    private $moduleKey = 'editMyContacts';
    private $moduleType = 'backend';

    private $action='list';

    //breadcrumb
    var $section='';
    var $moduleName='';
    var $subModuleName='';


    function __construct(){

    }


    function prepareData(){
        $mf = &$GLOBALS['mf'];
        $l10n = $mf->l10n;
        $this->pageOutput = '';

        if($mf->mode == 'frontend'){

            //process frontend display

        }
        else if($mf->mode == 'backend'){

            $this->userGroup = $mf->currentUser->getUserGroup();
            $this->moduleAccess = ($mf->currentUser->isAdmin() || (class_exists('mfUserGroup') && ($this->userGroup && $this->userGroup->getUserRight('my_plugin','allowEditMyContacts')==1)));

            if($this->moduleAccess){
                //add the plugin to the backend menus
                $mf->pluginManager->addEntryToBackendMenu('<a href="{subdir}/mf/index.php?module=editMyContacts"><span class="glyphicon glyphicon-menu glyphicon-th-large" aria-hidden="true"></span>'.$l10n->getLabel('editMyContacts','menu_title').'</a>','datas', LOW_PRIORITY);

                if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                    $this->localMenu ='<li rel="'.$this->moduleKey.'" class="active"><a href="{subdir}/mf/index.php?module='.$this->moduleKey.'">{menu-title}</a></li>';


                    //breadcrumb
                    $this->section = $l10n->getLabel('backend','datas');
                    $this->moduleName = ' / <span class="glyphicon glyphicon-menu glyphicon-th-large" aria-hidden="true"></span>'.$l10n->getLabel('editMyContacts','menu_title');
                    $this->subModuleName = '';

                    if(isset($_REQUEST['action']))$this->action = $_REQUEST['action'];

                    switch($this->action){

                        case "list":
                        default:

                                //composition du filtre d'affichage
                                $recordsFilter = new myPluginForm();

                                $form = $mf->formsManager;

                                $this->pageOutput .= '<div id="formFilter">';
                                $this->pageOutput .= $form->editForm(SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/my_plugin/my-plugin-json-backend.php', 'myPluginForm', 'updateContent', $recordsFilter, $this->moduleKey, '', true, $l10n->getLabel('backend','search'));
                                $this->pageOutput .= '<script>
                                    function updateContent(jsonData){
                                        $("#recordEditTable_myContact").html(jsonData.message);
                                    }
                                </script></div>';


                                $this->pageOutput .= $this->listRecords('','');
                                break;
                    }
                }
            } else {
                $this->pageOutput .= '<div id="modulePadder" >'.$l10n->getLabel('backend','module_no_access').'</div>';
                $this->localMenu = '';
            }

        }
    }

    function render(&$mainTemplate){
        $mf = &$GLOBALS['mf'];
        $l10n = $mf->l10n;

        if($mf->mode == 'backend'){
            if(isset($_REQUEST['module']) && ($this->moduleKey == $_REQUEST['module'])){

                $moduleBody = file_get_contents(DOC_ROOT.SUB_DIR.$GLOBALS['config']['website_pluginsdir'].'/my_plugin/ressources/templates/module.html');
                $moduleBody = str_replace("{module-body}",$this->pageOutput,$moduleBody);

                $moduleBody = str_replace("{local-menu}",$this->localMenu,$moduleBody);
                $moduleBody = str_replace("{menu-title}",$l10n->getLabel('editMyContacts','menu_title'),$moduleBody);

                $mainTemplate = str_replace("{current_module}",$moduleBody,$mainTemplate);
                //breadcrumb
                $mainTemplate = str_replace("{section}",$this->section,$mainTemplate);
                $mainTemplate = str_replace("{module-name}",$this->moduleName,$mainTemplate);
                $mainTemplate = str_replace("{submodule-name}",$this->subModuleName,$mainTemplate);

            }

        }
        else if($mf->mode == 'frontend'){}
    }



    function getType(){
        return $this->moduleType;
    }




    function listRecords($sqlConditions = '', $dateConditions = '',  $deletedCondition=' AND my_contacts.deleted=0'){


        $mf = &$GLOBALS['mf'];
        $l10n = $mf->l10n;


        //using time to store session SQL in case of multiple open tabs in the browser.
        //$time will allow distincting the good tab because the session now knows when the form was generated
        $sec = md5(microtime());

        //requête pour l'export CSV stockée en $_SESSION
        if(!isset($_SESSION['actions']))$_SESSION['actions']=array();
        $_SESSION['actions'][$sec]=array(
            'action' => 'exportCSV',
            'sql' => 'SELECT * FROM my_contacts WHERE deleted=0'.$sqlConditions,
            'sqlConditions' => $sqlConditions,
            'record_class' => 'myContact',
            'skipKeys' => array('uid', 'deleted', 'hidden', 'sorting', 'creator', 'change_log', 'edit_lock', 'start_time', 'end_time', 'language', 'alt_language', 'password',
            ),
            'print_column_names' => true,
            'keyProcessors' => array(
                'civility'=>'myContact::getCivility',
            ),
        );

        $contact = new myContact();

        $newRecordButtonHTML = '';

        $secKeys = array('action' => 'createRecord', 'record_class' => 'myContact');
        $secHash = formsManager::makeSecValue($secKeys);
        $secFields = implode(',',array_keys($secKeys));


        $newRecordButtonHTML .= '<button class="btn-sm btn-primary mf-btn-new" type="button" onClick="openRecord(\'{subdir}/mf/core/ajax-core-html.php?action=createRecord&record_class=myContact&header=0&footer=0&sec='.$secHash.'&fields='.$secFields.'&ajax=1&showSaveButton=1&showSaveCloseButton=1&showPreviewButton=&showCloseButton=1&mode='.$mf->mode.'\',\'70%\',\'\');">'.$l10n->getLabel('editMyContacts','new_record').'</button>
        <button type="button" class="btn-sm mf-btn-new" id="exportCSV" name="exportCSV" onclick="document.location=\''.getHTTPHost().SUB_DIR.'/mf/core/csv-exporter.php?sec='.$sec.'\';"><span class="glyphicon glyphicon-th"></span> '.$l10n->getLabel('backend','export_to_csv').'</button>';


        return $contact->showRecordEditTable(
            array(
                'SELECT' => 'creation_date,name, email,deleted',
                'FROM' => '',
                'JOIN' => '',
                'WHERE' => '1=1'.$dateConditions.$sqlConditions.$deletedCondition,
                'ORDER BY' => 'name',
                'ORDER DIRECTION' => 'ASC',
            ),
            'my_plugin',
            '',
            'name',
            $keyProcessors = array(
                'creation_date' => 'dbRecord::formatDate',
                'deleted' => 'dbRecord::getDeletedName',
            ),
            $page = NULL,

            array(
                'create' => 1,
                'view' => 1,
                'edit' => 1,
                'delete'=> 1
            ),
            array(
                'ajaxActions' => true,
                'newRecordButtonHTML' => $newRecordButtonHTML,
                'columnClasses' => array(
                    'creation_date' => 'hidden-xs',
                    'email' => 'hidden-xs hidden-sm',
                    'deleted' => 'hidden-xs hidden-sm',
                ),
                'debugSQL' => 0,
                'editRecordButtons' => array(
                    'showSaveButton' => true,
                    'showSaveCloseButton' => true,
                    'showPreviewButton' => false,
                    'showCloseButton' => true
                )
            )
        );


    }




}

