<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


class contact extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_contacts';

    static $createTableSQL= "
        `first_name` varchar(255) NOT NULL,
        `last_name` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `phone` varchar(20) NOT NULL,
        `recipient` varchar(255) NOT NULL,
        `subject` text NOT NULL,
        `message` text NOT NULL,
    ";

    static $createTableKeysSQL="";

    function init(){
        global $config;

        parent::init();

        $this->data = array_merge($this->data, array(
            'first_name'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'last_name'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'email'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'phone'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'recipient'   =>  array(
                'value' => '',
                'possibleValues' => (isset($config['plugins']['contacts']['email-destination']))?array($config['plugins']['contacts']['email-destination'] => $config['plugins']['contacts']['email-destination']):array(),
                'dataType' => 'select',
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'subject'   =>  array(
                'value' => '',
                'dataType' => 'input',
                'valueType' => 'text',
                'validation' => array(
                    'mandatory' => '1',
                ),
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'message'   =>  array(
                'value' => '',
                'dataType' => 'textarea',
                'valueType' => 'text',
                'div_attributes' => array('class' => 'col-lg-6'),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array(''),
                ),
            ),


        ));



        $this->showInEditForm = array(
            'tab_contact'=> array('first_name', 'last_name','email','phone','recipient','subject','message')
        );

    }


}


