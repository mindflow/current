<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

class mfFrance extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_france';

    static $createTableSQL= "
        `insee_com` varchar(5) NOT NULL,
        `postal_code` varchar(5) NOT NULL,
        `nom_comm` varchar(255) NOT NULL,
        `nom_dept` varchar(255) NOT NULL,
        `nom_region` varchar(255) NOT NULL,
        `statut` varchar(255) NOT NULL,
        `z_moyen` DECIMAL(1,1) DEFAULT 0,
        `superficie` DECIMAL(1,1) DEFAULT 0,
        `population` DECIMAL(1,1) DEFAULT 0,
        `geo_point_2d` varchar(255) NOT NULL,
        `geo_shape` text NOT NULL,
        `id_geofla` int(11) NOT NULL,
        `code_comm` int(11) NOT NULL,
        `code_cant` int(11) NOT NULL,
        `code_arr` int(11) NOT NULL,
        `code_dept` varchar(2) NOT NULL,
        `code_reg` int(11) NOT NULL,
    ";

    static $createTableKeysSQL="
        KEY `fk_mf_france_insee_code` (`insee_com`),
        KEY `fk_mf_france_dept_code` (`code_dept`),
        KEY `fk_mf_france_zip_isocode` (`postal_code`),
        KEY `fk_mf_france_city_name` (`nom_comm`),
        KEY `fk_mf_france_region_name` (`nom_dept`),
        KEY `fk_mf_france_dept_name` (`nom_region`),
        FULLTEXT KEY `fk_mf_france_fulltxt` (`postal_code`,`nom_comm`)
    ";

    function init(){

        parent::init();


        $this->data = array_merge($this->data, array(

            'insee_com'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'postal_code'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'nom_comm'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'nom_dept'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'nom_region'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'statut'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'z_moyen'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'superficie'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'population'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'geo_point_2d'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'geo_shape'   =>  array(
                'value' => '',
                'dataType' => 'textarea',
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'id_geofla'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'code_comm'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'code_cant'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'code_arr'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'code_dept'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'code_reg'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),

        ));




        $this->showInEditForm = array(
            'tab_main'=> array('insee_com','postal_code','nom_comm','nom_dept','nom_region','statut','z_moyen','superficie','population','geo_point_2d','geo_shape','id_geofla','code_comm','code_cant','code_arr','code_dept','code_reg')
        );



    }

    static $enableImportInitializationData = 1;

    static function importInitializationData(&$html=array()){

        dbRecord::importSQLDump(dirname(__FILE__).'/mf_france.sql',1,$html);

    }




    /*static function listDepartments(){
        $mf = &$GLOBALS['mf'];
        $pdo = $mf->db->pdo;

        $sql = "SELECT DISTINCT code_dept, nom_dept FROM ".self::$tableName." WHERE 1 ORDER BY nom_dept";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array());
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $items = array(0 => '');

        foreach($rows as $row){
            $items[$row['code_dept']] = $row['nom_dept'];
        }
        return $items;
    }*/

    static function getCodeDepartment($nomDept){
        global $pdo;

        $sql = "SELECT DISTINCT code_dept FROM ".self::$tableName." WHERE nom_dept=:nom_dept ORDER BY nom_dept";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array(':nom_dept'=>$nomDept));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        if($stmt->rowCount() > 0)
            return $stmt->fetch(PDO::FETCH_COLUMN);
        else return 0;
    }


    static function listRegions(){
        global $pdo;

        $sql = "SELECT DISTINCT code_reg, nom_region FROM ".self::$tableName." WHERE 1 ORDER BY nom_region";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array());
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $items = array(0 => '');

        foreach($rows as $row){
            $items[$row['code_reg']] = $row['nom_region'];
        }
        return $items;
    }


    static function listDeptByRegion($code_reg){
        global $pdo;

        $sql = "SELECT DISTINCT code_dept, nom_dept FROM ".self::$tableName." WHERE code_reg=:creg ORDER BY nom_dept";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array(':creg'=>$code_reg));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $items = array(0 => '');

        foreach($rows as $row){
            $items[$row['code_dept']] = $row['nom_dept'];
        }
        return $items;
    }

    static function getCitiesByDepartement($noDepartement){
        global $pdo;

        $sql = "SELECT uid, nom_comm FROM ".self::$tableName." WHERE code_dept=:code_dept ORDER BY nom_comm";
        $stmt = $pdo->prepare($sql);


        try{
            $stmt->execute(array(':code_dept'=>$noDepartement));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $items = array(0 => '');

        foreach($rows as $row){
            $items[$row['uid']] = $row['nom_comm'];
        }
        return $items;
    }


    static function getCitiesTypeahead($noDepartement, $typedChars){
        global $pdo;

        $query = $pdo->quote('%'.$typedChars.'%');
        $dep = '';
        if($noDepartement != '0' && $noDepartement != '') $dep = ' AND code_dept='.$pdo->quote($noDepartement);

        $sql = "SELECT uid, nom_comm FROM ".self::$tableName." WHERE nom_comm LIKE ".$query.$dep." ORDER BY nom_comm";

        $stmt = $pdo->query($sql);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $items = array();
        foreach($rows as $row){
            $items[] = $row['nom_comm'];
        }
        return $items;
    }

    static function checkZipCode($zipCode){
        global $pdo;

        $sql = "SELECT COUNT(postal_code) FROM ".self::$tableName." WHERE postal_code=:postal_code AND DELETED = 0";
        $stmt = $pdo->prepare($sql);

        try{
            $stmt->execute(array(':postal_code'=>$zipCode));
        }
        catch(PDOException $e){
            echo $e->getMessage();
            return 0;
        }

        $zipCount = $stmt->fetch(PDO::FETCH_NUM);

        if(isset($zipCount[0]) && $zipCount[0] > 0)return true;
        else return false;
    }


    static function getZipByCityUid($cityUid){
        global $pdo;

        $sql = "SELECT postal_code FROM ".self::$tableName." WHERE uid=".$pdo->quote($cityUid)." AND DELETED = 0";
        $stmt = $pdo->query($sql);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($stmt->rowCount()>0)return $row['postal_code'];
        else return '';
    }

    static function getZipByCityName($cityName){
        global $pdo;

        $sql = "SELECT postal_code FROM ".self::$tableName." WHERE nom_comm=".$pdo->quote($cityName)." AND DELETED = 0";

        $stmt = $pdo->query($sql);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($stmt->rowCount()>0)return $row['postal_code'];
        else return '';
    }
}


