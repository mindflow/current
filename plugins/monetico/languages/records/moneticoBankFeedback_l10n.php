<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

global $mf;

$l10nText = array(


    'fr' => array(
        'tab_main' => 'Principal',

        'parent_uid' => 'UID enregistrement parent',
        'version' => 'Version du système de paiement',
        'tpe' => 'Numéro de TPE Virtuel',
        'date' => 'Date et heure',
        'devise' => 'Devise',
        'montant' => 'Montant TTC de la commande',
        'reference' => 'Référence de la commande',
        'texte-libre' => 'Texte libre',
        'MAC' => 'Sceau MAC',
        'code-retour' => 'Code retour',
        'payetest' => 'paiement accepté (mode TEST)',
        'paiement' => 'paiement accepté',
        'Annulation' => 'paiement refusé',
        'paiement_pf2' => 'paiement accepté de l’échéance 2',
        'paiement_pf3' => 'paiement accepté de l’échéance 3',
        'paiement_pf4' => 'paiement accepté de l’échéance 4',
        'Annulation_pf2' => 'paiement refusé définitivement de l’échéance 2',
        'Annulation_pf3' => 'paiement refusé définitivement de l’échéance 3',
        'Annulation_pf4' => 'paiement refusé définitivement de l’échéance 4',

        'payetest_short' => '<span class="grey">Accepté (TEST)</span>',
        'paiement_short' => '<span class="green">Accepté (PROD)</span>',
        'Annulation_short' => '<span class="red">Refusé</span>',
        'paiement_pf2_short' => '<span class="green">Accepté échéance 2</span>',
        'paiement_pf3_short' => '<span class="green">Accepté échéance 3</span>',
        'paiement_pf4_short' => '<span class="green">Accepté échéance 4</span>',
        'Annulation_pf2_short' => '<span class="red">Refusé échéance 2</span>',
        'Annulation_pf3_short' => '<span class="red">Refusé échéance 3</span>',
        'Annulation_pf4_short' => '<span class="red">Refusé échéance 4</span>',

        'cvx' => 'Cryptogramme visuel saisi',
        'oui' => 'Oui',
        'non' => 'Non',
        'vld' => 'Date de validité de la carte de crédit',
        'brand' => 'Type de carte',
        'status3ds' => 'Statut 3DSecure',
        'status3ds-1' => 'Transaction non 3DSecure',
        'status3ds1' => 'Transaction 3DSecure, risque faible',
        'status3ds2' => 'Transaction  non 3DSecure, mais porteur identifié 3DS',
        'status3ds3' => 'Transaction 3DSecure, risque élevé',
        'status3ds4' => 'Transaction 3DSecure, risque très élevé',
        'numauto' => 'Numéro d’autorisation par la banque émettrice',
        'motifrefus' => 'Motif du refus de la demande de paiement',

        'Appel Phonie'    => 'Votre banque demande des informations complémentaires',
        'Refus'     => 'Autorisation non accordée par votre banque',
        'Interdit'  => 'Autorisation non accordée par votre banque',
        'filtrage'  => 'Demande de paiement bloquée par le Module Prévention Fraude',
        'scoring'   => 'Demande de paiement bloquée par le scoring du Module Prévention Fraude',
        '3DSecure'  => 'Authentification 3DSecure négative',

        'originecb' => 'Code pays de la banque émettrice',
        'bincb' => 'Code BIN de la banque du porteur CB',
        'hpancb' => 'Hachage du numéro de la CB',
        'ipclient' => 'Adresse IP du client',
        'originetr' => 'Code pays de l’origine de la transaction',
        'veres' => 'État 3DSecure du VERes',
        'pares' => 'État 3DSecure du PARes',
        'montantech' => 'Montant de l’échéance en cours',
        'filtragecause' => 'Filtre bloquant le paiement',
        'filtrage1' => 'Adresse IP',
        'filtrage2' => 'Numéro de carte',
        'filtrage3' => 'BIN de carte',
        'filtrage4' => 'Pays de la carte',
        'filtrage5' => 'Pays de l’IP',
        'filtrage6' => 'Cohérence pays de la carte / pays de l’IP',
        'filtrage7' => 'Email jetable',
        'filtrage8' => 'Montant transcation limité pour cette CB',
        'filtrage9' => 'Nombre de transcations limité pour cette CB',
        'filtrage11' => 'Nombre de transcations limité par alias CB',
        'filtrage12' => 'Montant limité par alias CB',
        'filtrage13' => 'Montant limité par adresse IP',
        'filtrage14' => 'Nombre de transcations limité par adresse IP',
        'filtrage15' => 'Testeurs de cartes',
        'filtrage16' => 'Limitation en nombre d’alias par CB',
        'filtragevaleur' => 'Données ayant engendré le blocage',
        'cbenregistree' => 'Carte associée à l\'alias CB envoyé',
        'cbmasquee' => 'Numéro CB masqué',
        'modepaiement' => 'Moyen de paiement',
        'autre' => 'Autre',




        /*mandatory records*/
        //'no_record' => 'Aucun résultat.',
        'record_name' => 'Retour banque',
        'new_record' => 'Ajout retour banque',

    ),

    'en' => array(
        'tab_main' => 'Main',

        'parent_uid' => 'parent UID save',
        'version' => 'Payment system version',
        'tpe' => 'Virtual POS terminal number',
        'date' => 'Date and hour',
        'devise' => 'Currency',
        'montant' => 'Booking amount incl. VAT',
        'reference' => 'Booking reference',
        'texte-libre' => 'Free text',
        'MAC' => 'MAC',
        'code-retour' => 'Return code',
        'payetest' => 'payment accepted (TEST mode)',
        'paiement' => 'payment accepted (production)',
        'Annulation' => 'payment refused',
        'paiement_pf2' => '2nd installment payment accepted',
        'paiement_pf3' => '3rd installment payment accepted',
        'paiement_pf4' => '4th installment payment accepted',
        'Annulation_pf2' => '2nd installment payment definitively refused',
        'Annulation_pf3' => '3rd installment payment definitively refused',
        'Annulation_pf4' => '4th installment payment definitively refused',

        'payetest_short' => '<span class="grey">Accepted (TEST)</span>',
        'paiement_short' => '<span class="green">Accepted (PROD)</span>',
        'Annulation_short' => '<span class="red">Refused</span>',
        'paiement_pf2_short' => '<span class="green">Accept 2nd installment</span>',
        'paiement_pf3_short' => '<span class="green">Accept 3rd installment</span>',
        'paiement_pf4_short' => '<span class="green">Accept 4th installment</span>',
        'Annulation_pf2_short' => '<span class="red">Refuse 2nd installment</span>',
        'Annulation_pf3_short' => '<span class="red">Refuse 3rd installment</span>',
        'Annulation_pf4_short' => '<span class="red">Refuse 4th installment</span>',

        'cvx' => 'Visual cryptogram entered',
        'oui' => 'Yes',
        'non' => 'No',
        'vld' => 'Credit card expiry date',
        'brand' => 'Type of card',
        'status3ds' => '3DSecure status',
        'status3ds-1' => 'Transaction non 3DSecure',
        'status3ds1' => 'Transaction 3DSecure, low risk',
        'status3ds2' => 'Transaction non 3DSecure, but cardholder identified 3DS',
        'status3ds3' => 'Transaction 3DSecure, high risk',
        'status3ds4' => 'Transaction 3DSecure, very high risk',
        'numauto' => 'Issuing bank authorisation number',
        'motifrefus' => 'Reason for refusal of payment demand',

        'Appel Phonie'    => 'Additional information required by your bank',
        'Refus'     => 'Authorisation refused by your bank',
        'Interdit'  => 'Authorisation refused by your bank',
        'filtrage'  => 'Payment request blocked by Fraud Prevention Module',
        'scoring'   => 'Payment request blocked by Fraud Prevention Module scoring',
        '3DSecure'  => 'Incorrect 3DSecure authentification',

        'originecb' => 'Emitting bank country code',
        'bincb' => 'Card holder\'s bank BIN code',
        'hpancb' => 'Credit card number hashing',
        'ipclient' => 'Customer IP address',
        'originetr' => 'Transaction origin country code',
        'veres' => 'VERes 3DSecure status',
        'pares' => 'PARes 3DSecure status',
        'montantech' => 'Current installment amount',
        'filtragecause' => 'Filter blocking payment',
        'filtrage1' => 'IP address',
        'filtrage2' => 'Card number',
        'filtrage3' => 'Card BIN',
        'filtrage4' => 'Card country',
        'filtrage5' => 'IP country',
        'filtrage6' => 'Card country / IP country consistency',
        'filtrage7' => 'Junk email',
        'filtrage8' => 'Transaction amount limited for this card',
        'filtrage9' => 'Number of transactions limited for this card',
        'filtrage11' => 'Number of transactions limited by card alias',
        'filtrage12' => 'Amount limited by card alias',
        'filtrage13' => 'Amount limited by IP address',
        'filtrage14' => 'Number of transactions limited by IP address',
        'filtrage15' => 'Card testers',
        'filtrage16' => 'Limited number of alias\'s per card',
        'filtragevaleur' => 'Data that caused the blockage',
        'cbenregistree' => 'Card associated to the sent card alias sent',
        'cbmasquee' => 'Card number hidden',
        'modepaiement' => 'payment means',
        'autre' => 'Other',




        /*mandatory records*/
        //no_record' => 'No results.',
        'record_name' => 'Bank feedback',
        'new_record' => 'Add bank feedback',

    ),

    'de' => array(
        'tab_main' => 'Principal',

        'parent_uid' => 'UID enregistrement parent',
        'version' => 'Version du système de paiement',
        'tpe' => 'Numéro de TPE Virtuel',
        'date' => 'Date et heure',
        'devise' => 'Devise',
        'montant' => 'Montant TTC de la commande',
        'reference' => 'Référence de la commande',
        'texte-libre' => 'Texte libre',
        'MAC' => 'Sceau MAC',
        'code-retour' => 'Code retour',
        'payetest' => 'Zahlung akzeptiert (TEST)',
        'paiement' => 'Zahlung akzeptiert',
        'Annulation' => 'Zahlung abgelehnt',
        'paiement_pf2' => 'paiement accepté de l’échéance 2',
        'paiement_pf3' => 'paiement accepté de l’échéance 3',
        'paiement_pf4' => 'paiement accepté de l’échéance 4',
        'Annulation_pf2' => 'paiement refusé définitivement de l’échéance 2',
        'Annulation_pf3' => 'paiement refusé définitivement de l’échéance 3',
        'Annulation_pf4' => 'paiement refusé définitivement de l’échéance 4',

        'payetest_short' => '<span class="grey">Accepté (TEST)</span>',
        'paiement_short' => '<span class="green">Accepté (PROD)</span>',
        'Annulation_short' => '<span class="red">Refusé</span>',
        'paiement_pf2_short' => '<span class="green">Accepté échéance 2</span>',
        'paiement_pf3_short' => '<span class="green">Accepté échéance 3</span>',
        'paiement_pf4_short' => '<span class="green">Accepté échéance 4</span>',
        'Annulation_pf2_short' => '<span class="red">Refusé échéance 2</span>',
        'Annulation_pf3_short' => '<span class="red">Refusé échéance 3</span>',
        'Annulation_pf4_short' => '<span class="red">Refusé échéance 4</span>',

        'cvx' => 'Cryptogramme visuel saisi',
        'oui' => 'Oui',
        'non' => 'Non',
        'vld' => 'Date de validité de la carte de crédit',
        'brand' => 'Type de carte',
        'status3ds' => 'Statut 3DSecure',
        'status3ds-1' => 'Transaction non 3DSecure',
        'status3ds1' => 'Transaction 3DSecure, risque faible',
        'status3ds2' => '3DSecure non 3DSecure, mais porteur identifié 3DS',
        'status3ds3' => 'Transaction 3DSecure, risque élevé',
        'status3ds4' => 'Transaction 3DSecure, risque très élevé',
        'numauto' => 'Numéro d’autorisation par la banque émettrice',
        'motifrefus' => 'Motif du refus de la demande de paiement',

        'Appel Phonie'    => 'Ihre Bank verlangt zusätzliche Informationen',
        'Refus'     => 'Ihre Bank hat die Zahlungsbestätigung abgelehnt',
        'Interdit'  => 'Ihre Bank hat die Zahlungsbestätigung abgelehnt',
        'filtrage'  => 'Zahlungsantrag vom Anti-Fraud-Massnahme Modul gesperrt',
        'scoring'   => 'Zahlungsantrag vom Scoring des Anti-Fraud-Massnahme Modul gesperrt',
        '3DSecure'  => 'Negative Authentifizierung vom 3DSecure',


        'originecb' => 'Code pays de la banque émettrice',
        'bincb' => 'Code BIN de la banque du porteur CB',
        'hpancb' => 'Hachage du numéro de la CB',
        'ipclient' => 'Adresse IP du client',
        'originetr' => 'Code pays de l’origine de la transaction',
        'veres' => 'État 3DSecure du VERes',
        'pares' => 'État 3DSecure du PARes',
        'montantech' => 'Montant de l’échéance en cours',
        'filtragecause' => 'Filtre bloquant le paiement',
        'filtrage1' => 'Adresse IP',
        'filtrage2' => 'Numéro de carte',
        'filtrage3' => 'BIN de carte',
        'filtrage4' => 'Pays de la carte',
        'filtrage5' => 'Pays de l’IP',
        'filtrage6' => 'Cohérence pays de la carte / pays de l’IP',
        'filtrage7' => 'Email jetable',
        'filtrage8' => 'Montant transcation limité pour cette CB',
        'filtrage9' => 'Nombre de transcations limité pour cette CB',
        'filtrage11' => 'Nombre de transcations limité par alias CB',
        'filtrage12' => 'Montant limité par alias CB',
        'filtrage13' => 'Montant limité par adresse IP',
        'filtrage14' => 'Nombre de transcations limité par adresse IP',
        'filtrage15' => 'Testeurs de cartes',
        'filtrage16' => 'Limitation en nombre d’alias par CB',
        'filtragevaleur' => 'Données ayant engendré le blocage',
        'cbenregistree' => 'Carte associée à l\'alias CB envoyé',
        'cbmasquee' => 'Numéro CB masqué',
        'modepaiement' => 'Moyen de paiement',
        'autre' => 'Autre',




        /*mandatory records*/
        //'no_record' => 'Aucun résultat.',
        'record_name' => 'Retour banque',
        'new_record' => 'Ajout retour banque',

    ),


);

?>