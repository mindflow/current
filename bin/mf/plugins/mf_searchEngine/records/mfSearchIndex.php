<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

class mfSearchIndex extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_search_index';

    static $createTableSQL= "
        `class_name` varchar(255) NOT NULL,
        `object_uid` int(11) NOT NULL,
        `title` varchar(255) DEFAULT NULL,
        `digest` text NOT NULL,
        `ressource_url` text NOT NULL,
        `tags` text NOT NULL,
    ";

    static $createTableKeysSQL="
        KEY `fk_mf_search_index_class_name` (`class_name`,`object_uid`,`language`),
        FULLTEXT KEY `fk_mf_search_index_digest` (`title`,`digest`,`tags`)
    ";
    var $categories;

    function init(){

        //load translations
        //$this->loadL10n('/mf/plugins/mf_searchEngine','records');

        parent::init();


        $this->data = array_merge($this->data, array(

            'class_name'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'object_uid'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'title'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
            ),
            'digest'   =>  array(
                'value' => '',
                'dataType' => 'textarea 50 3',
                 'div_attributes' => array('class' => 'col-lg-8'),
            ),
            'ressource_url'   =>  array(
                'value' => '',
                'dataType' => 'urlinput',
                'target' => '_blank',
                'div_attributes' => array('class' => 'col-lg-8'),
            ),
            'tags'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
            ),

        ));


        $this->showInEditForm = array(
            'tab_main'=> array('class_name','title','object_uid','digest','tags','ressource_url')
        );

    }

    static $enableImportInitializationData = 0;

    static function importInitializationData(&$html=array()){}

    /*
    function store($updateDates = true, $forceCreate = false){
        $mf = &$GLOBALS['mf'];
        $l10n = $mf->l10n;

        $this->data['slug']['value'] = GenerateUrl($this->data['nom_etablissement']['value']);
        return parent::store($updateDates,$forceCreate);
    }*/



    function getPreviewUrl(){
        /*if(isset($config['plugins']['revendeurs']['single-revendeur-page-uid'])){
            $urlRewriter = $GLOBALS['mf']->pluginManager->modulesByName['urlRewriter'];
            return $urlRewriter->getPageURL($config['plugins']['revendeurs']['single-revendeur-page-uid'][$mf->getDataEditingLanguage()]).'?revendeur='.$this->data['slug']['value'];

        }
        return 'GLOBALS[config][plugin-revendeurs][single-revendeur-page-uid] is not defined';*/
    }


    function delete(){
        $this->delete_forever();
    }


}


