<?php



require_once '../../../../mf_config/config.php';
require_once DOC_ROOT.SUB_DIR.'/mf/backend/backend.php';
require_once DOC_ROOT.SUB_DIR.'/mf/core/utils.php';

//enlarge time execution of the php script for long transactions
set_time_limit(0);

$execution_start = time();

//create a backend for being able to use all the mindflow objects
$mindFlowBackend = new backend();
$mindFlowBackend->prepareData();

global $mf,$pdo,$l10n,$logger,$config;

$logger->debug( "syncServer START : in connexion from " . $_SERVER['REMOTE_ADDR']);

$syncConfig = $config['plugins']['table_sync'];
$response = '';

if(isset($_POST['request_type'])) {



	switch ( $_POST['request_type'] ) {
		case 'base_sync':
		case 'full_sync':

		//echo "\$_POST = ".print_r($_POST,true);
			//we are serving json
			header( 'Content-type: application/json' );

			$in = new InSync();

			//collecting data
			$in->data['remote_table']['value'] = $_POST['remote_table'];
			$in->data['remote_uid']['value']   = $_POST['remote_uid'];
			$in->data['source']['value']       = $source = $_POST['source'];
			$in->data['remote_data']['value']  = $_POST['remote_data'];
			$password                          = $_POST['password'];


			//log to Monolog
			$log = "syncServer : in connexion of type ".$_POST['request_type'].chr(10).
			       "source = ".$_POST['source'].chr(10).
			       "remote_table = ".$_POST['remote_table'].chr(10).
			       "remote_uid = ".$_POST['remote_uid'];

			$logger->debug($log);

			$in->data['local_log']['value'] .= $log.chr(10);
			$in->data['local_log']['value'] .= $l10n->getLabel( 'SyncManager', 'received_data' ) . chr( 10 );
			$in->data['local_log']['value'] .= $l10n->getLabel( 'SyncManager', 'seeking_rule' ) . chr( 10 );
			$in->data['state']['value']     = 'Receiving';
			$in->data['result']['value']    = '';
			$inUid                          = $in->store();//true, false, true


			//$now = new DateTime('NOW');

			//$response = chr( 10 ) .$l10n->getLabel( 'SyncManager', 'exec_on' ) . ' ' . $now->format('d/m/Y H:i:s') . chr( 10 ) . chr( 10 ) ;

			if ( trim( $source ) != '' ) {
				//request has valid source
				$passwordOK = false;
				$ipOK       = false;
				if ( isset( $syncConfig['remote_hosts'][ $source ] ) ) {

					//check source provided password against local known password for this source
					if ( $password == $syncConfig['remote_hosts'][ $source ]['password'] ) {
						$passwordOK = true;
					} else {
						//log invalid password
						$in->setField('result','Failure');

						$errMsg = $l10n->getLabel( 'SyncManager', 'invalid_password' ) . $_SERVER['REMOTE_ADDR'] . $l10n->getLabel( 'SyncManager', 'from_source' ) . $source;
						$in->addToLocalLog( $errMsg . chr( 10 ) );
						$logger->info($errMsg);

					}

					//check source IP address against local known IP address for this source
					if ( $_SERVER['REMOTE_ADDR'] == $syncConfig['remote_hosts'][ $source ]['ip'] || trim($syncConfig['remote_hosts'][ $source ]['ip'])== '*') {
						$ipOK = true;
					} else {
						//log invalid host IP
						$in->setField('result','Failure');

						$errMsg = $l10n->getLabel( 'SyncManager', 'invalid_ip' ) . $_SERVER['REMOTE_ADDR'] . $l10n->getLabel( 'SyncManager', 'from_source' ) . $source . chr( 10 ) . $l10n->getLabel( 'SyncManager', 'awaited_ip' ) . $syncConfig['remote_hosts'][ $source ]['ip'] . " , " . $l10n->getLabel( 'SyncManager', 'client_ip' ) . $_SERVER['REMOTE_ADDR'] ;

						$in->addToLocalLog( $errMsg . chr( 10 ) );
						$logger->info($errMsg);
					}
				} else {
					$in->setField('result','Failure');

					$errMsg = $source . $l10n->getLabel( 'SyncManager', 'unknown_source' ) . chr( 10 ) ;

					$in->addToLocalLog( $errMsg . chr( 10 ) );
					$logger->info($errMsg);

				}
				if ( $passwordOK && $ipOK ) {
					//seek the right rule to apply

					foreach ( $syncConfig['in_rules'] as $ruleLabel => $ruleData ) {

						if ( $ruleData['remote_table'] == $in->data['remote_table']['value'] ) {
							if ( in_array( $source, $ruleData['sources'] ) ) {
								//rule found
								$ruleName = $ruleLabel;
								$rule = $ruleData;

								//assign the rule to our inSync object
								$in->setField('in_rule', $ruleName);

								//the inSync object must be assigned to the correct locale in order not to appear in the default language !
								$in->setField('language',$rule['set_locale']);
								$logger->info($in);

								$msg = $l10n->getLabel( 'SyncManager', 'found_rule' ) . ' ' . $ruleLabel ;

								$in->addToLocalLog( $msg . chr( 10 ) );
								$logger->info($msg);

								//exit at first matching rule found
								break;
							}
						}

					}

					if ( ! isset( $ruleName ) ) {

						//error, no matching receive rule found, failure to process object
						$in->setField('result','Failure');
						$errMsg = $l10n->getLabel( 'SyncManager', 'rule_not_found' );
						$in->addToLocalLog( $errMsg . chr( 10 ) );
						$logger->info($errMsg);

						$response .= $in->data['local_log']['value'];

					} else {



						if ( isset( $rule['recvFunc'] ) ) {
							//apply user defined function in configuration, for sending XML rather than standard JSON based protocol for example
							$recvFunc = $rule['recvFunc'];
						} else {
							//or use default JSON based protocol function
							$in->addToLocalLog( $l10n->getLabel( 'SyncManager', 'recvFunc_not_set' ) .' '. $ruleName . chr( 10 ) );
							$recvFunc = "SyncManager::jsonInRequest";
						}

						//processing the request data
						try {
							$response .= $recvFunc( $in );


						}
						catch(Exception $e){
							$response .= $e->getMessage().chr(10);
						}

					}
				}
				else {
					$response .= $in->data['local_log']['value'];
				}
			} else {

				//source is empty, no matching rule can be found, abort.
				$in->setField('result','result','Failure');

				$errMsg = $l10n->getLabel( 'SyncManager', 'empty_source' );
				$in->addToLocalLog( $errMsg . chr( 10 ) );
				$logger->info($errMsg);

				$response .= $in->data['local_log']['value'] . chr( 10 );
			}


			break;

		case 'sql_sync':
			header( 'Content-type: application/json' );

			$sqlIn = new SqlInSync();

			//collecting data
			$sqlIn->data['source']['value']       = $source = $_POST['source'];
			$sqlIn->data['sql_request']['value']      = base64_decode($_POST['sql_request']);
			$password                          = $_POST['password'];

			//log to Monolog
			$logger->debug( "tableSync : in connexion of type ".$_POST['request_type']." sql_request=" . $sqlIn->data['sql_request']['value'] );


			$sqlIn->data['local_log']['value'] .= $l10n->getLabel( 'SyncManager', 'received_data' ) . chr( 10 );

			$sqlIn->data['state']['value']     = 'Receiving';
			$sqlIn->data['result']['value']     = '';

			$sqlInUid = $sqlIn->store();//true, false, true


			//$now = new DateTime('NOW');

			//$response = chr( 10 ) .$l10n->getLabel( 'SyncManager', 'exec_on' ) . ' ' . $now->format('d/m/Y H:i:s') . chr( 10 ) . chr( 10 ) ;


			if ( trim( $source ) != '' ) {
				//request has valid source
				$passwordOK = false;
				$ipOK       = false;
				if ( isset( $syncConfig['remote_hosts'][ $source ] ) ) {

					//check source provided password against local known password for this source
					if ( $password == $syncConfig['remote_hosts'][ $source ]['password'] ) {
						$passwordOK = true;
					} else {
						//log invalid password
						$sqlIn->data['result']['value'] = 'Failure';

						$errMsg = $l10n->getLabel( 'SyncManager', 'invalid_password' ) . $_SERVER['REMOTE_ADDR'] . $l10n->getLabel( 'SyncManager', 'from_source' ) . $source;
						$in->addToLocalLog( $errMsg . chr( 10 ) );
						$logger->info($errMsg);
					}

					//check source IP address against local known IP address for this source
					if ( $_SERVER['REMOTE_ADDR'] == $syncConfig['remote_hosts'][ $source ]['ip']  || trim($syncConfig['remote_hosts'][ $source ]['ip'])== '*' ) {
						$ipOK = true;
					} else {
						//log invalid host IP
						$sqlIn->data['result']['value'] = 'Failure';

						$errMsg = $l10n->getLabel( 'SyncManager', 'invalid_ip' ) . $_SERVER['REMOTE_ADDR'] . $l10n->getLabel( 'SyncManager', 'from_source' ) . $source . chr( 10 ) . $l10n->getLabel( 'SyncManager', 'awaited_ip' ) . $syncConfig['remote_hosts'][ $source ]['ip'] . " , " . $l10n->getLabel( 'SyncManager', 'client_ip' ) . $_SERVER['REMOTE_ADDR'];
						$in->addToLocalLog( $errMsg . chr( 10 ) );
						$logger->info($errMsg);
					}
				} else {
					$sqlIn->data['result']['value'] = 'Failure';

					$errMsg = $l10n->getLabel( $source . $l10n->getLabel( 'SyncManager', 'unknown_source' ));
					$in->addToLocalLog( $errMsg . chr( 10 ) );
					$logger->info($errMsg);
				}
				if ( $passwordOK && $ipOK ) {

					try{
						$stmt = $pdo->query($sqlIn->data['sql_request']['value']);

						//$sqlIn->data['local_log']['value'] .= "stmt->rowCount()=".$stmt->rowCount().chr(10);
						//echo "stmt->rowCount()=".$stmt->rowCount().chr(10);

						//fetch the result only if the request begins with a select
						if(strncasecmp ( $sqlIn->data['sql_request']['value']  , "SELECT" , 6 ) == 0) {
							$rows                               = $stmt->fetchAll( PDO::FETCH_ASSOC );
							$sqlIn->data['sql_result']['value'] = serialize( $rows );
						}
						else $sqlIn->data['sql_result']['value'] = '';

						$sqlIn->data['local_log']['value'] .= $l10n->getLabel( 'SyncManager', 'sql_in_success' ) . chr( 10 );
						$sqlIn->data['result']['value']    = 'Success';
					}
					catch(PDOException $e){
						//$sqlIn->data['local_log']['value'] .= "Request : " . chr( 10 );
						//$sqlIn->data['local_log']['value'] .= $sqlIn->data['sql_request']['value'] . chr( 10 );
						$sqlIn->data['local_log']['value'] .= "Error : " . chr( 10 );
						$sqlIn->data['local_log']['value'] .= $e->getMessage(). chr( 10 );
						$sqlIn->data['result']['value']    = 'Failure';
					}


					$sqlIn->store();

				}



			} else {

				//source is empty, no matching rule can be found, abort.
				$sqlIn->data['local_log']['value'] .= $l10n->getLabel( 'SyncManager', 'empty_source' ) . chr( 10 );
				$sqlIn->data['result']['value']    = 'Failure';
				$sqlIn->store();

			}

			switch($sqlIn->data['result']['value']){
				case 'Success':
					$response = '{"jsonrpc" : "2.0", "result" : "success", "log" : '.json_encode($sqlIn->data['local_log']['value'] . chr( 10 )).', "sql_result" : "'.$sqlIn->data['sql_result']['value'].'"}'; //,
					break;

				case 'Failure':
					$response = '{"jsonrpc" : "2.0", "result" : "error", "log" : '.json_encode($sqlIn->data['local_log']['value'] . chr( 10 )).'}';
					break;
			}

			$sqlIn->data['state']['value'] = 'Inactive';
			$sqlIn->store();
			break;

		default:
			$response .= $l10n->getLabel( 'SyncManager', 'unknown_request_type' ) . chr( 10 );
	}
}
else{
	$response .= $l10n->getLabel( 'SyncManager', 'invalid_request_parameters' ) . chr( 10 );
	$logger->debug( "tableSync : in connexion from IP " . $_SERVER['REMOTE_ADDR'] . " but no request_type specified");
	$logger->debug( "\$_POST = ".print_r($_POST,true));

}

echo $response;

$logger->debug( "response = ".$response );
$logger->info("syncServer END duration : ". ((time() - $execution_start)) . " secondes");

$mf->db->closeDB();





