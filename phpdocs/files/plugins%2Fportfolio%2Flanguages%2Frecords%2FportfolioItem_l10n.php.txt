<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


$l10nText = array(


    'en' => array(
        'tab_portfolioItem' => 'Properties',
        'title' => 'Portfolio title',
        'summup' => 'Short portfolio summup',
        'text' => 'Portfolio text',
        'images' => 'Associated pictures',
        'category' => 'Category',
        'portfolio_date' => 'Portfolio date',
        'category' => 'Category',

        /*mandatory records*/
        'no_record' => 'There currently exists no portfolioItem. You can <a href="{subdir}/mf/index.php?module=portfolio&action=create&class=portfolioItem">create an portfolioItem</a>.',

    ),

    'fr' => array(
        'tab_portfolioItem' => 'Propriétés',
        'title' => 'Titre de l\'entrée',
        'summup' => 'Résumé de l\'entrée',
        'text' => 'Texte de l\'entrée',
        'images' => 'Images associées',
        'category' => 'Categorie',
        'portfolio_date' => 'Date de l\'entrée',
        'category' => 'Catégorie',

        /*mandatory records*/
        'no_record' => 'Il n\'existe pas d\'entrée de portfolio pour le moment. Vous pouvez <a href="{subdir}/mf/index.php?module=portfolio&action=create&class=portfolioItem">créer une entrée</a>.',

    ),
);

?>
