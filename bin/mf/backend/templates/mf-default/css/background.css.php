<?php
require_once '../../../../../mf_config/config.php';
header('content-type: text/css');
//header('HTTP/1.0 304 Not Modified');
//header('Cache-Control: max-age=3600, must-revalidate');

global $config;

if(isset($config['backend_template_background'])){
    $backgroundImage=SUB_DIR.$config['backend_template_background'];
}
else $backgroundImage=SUB_DIR.'/mf/backend/templates/mf-default/img/bg1/';

$bgNamePart = strstr($backgroundImage, '.', true);
$bgExtPart = strrchr($backgroundImage, '.');

if(strlen($bgExtPart)==0)$bgIsDir=true;
else $bgIsDir=false;

if($bgIsDir)$bgDir = $backgroundImage;
else $bgDir = '';


echo "


@media screen and (min-width: 2100px) {
    body#login{background:url('".(($bgIsDir)?$bgDir.'bg-3840x2400.jpg':$backgroundImage)."');}
    body#main,body#installer{background-image:url('".(($bgIsDir)?$bgDir.'bg-3840x2400.jpg':$backgroundImage)."');}
}
@media screen and (max-width: 2100px) {
    body#login{background:url('".(($bgIsDir)?$bgDir.'bg-2100x1312.jpg':$backgroundImage)."');}
    body#main,body#installer{background-image:url('".(($bgIsDir)?$bgDir.'bg-2100x1312.jpg':$backgroundImage)."');}
}
@media screen and (max-width: 1280px) {
    body#login{background:url('".(($bgIsDir)?$bgDir.'bg-1280x800.jpg':$backgroundImage)."');}
    body#main,body#installer{background-image:url('".(($bgIsDir)?$bgDir.'bg-1280x800.jpg':$backgroundImage)."');}
}

@media screen and (max-width: 640px) {
    body#login{background:url('".(($bgIsDir)?$bgDir.'bg-640x1024.jpg':$backgroundImage)."');}
    body#main,body#installer{background-image:url('".(($bgIsDir)?$bgDir.'bg-640x1024.jpg':$backgroundImage)."');}
}



";




?>