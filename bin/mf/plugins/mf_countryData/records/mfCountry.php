<?php

/*
   Copyright 2017 Alban Cousinié

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

class mfCountry extends dbRecord
{
    //table name in the SQL database
    static $tableName = 'mf_countries';

    static $createTableSQL= "
        `cn_iso_2` varchar(2) NOT NULL,
        `cn_iso_3` varchar(3) NOT NULL,
        `cn_iso_nr` int(11) DEFAULT 0,
        `zip_format` varchar(255) NOT NULL,
        `zip_regexp` varchar(255) NOT NULL,
        `zip_nocodes` tinyint(4) DEFAULT 0,
        `zip_single` tinyint(4) DEFAULT 0,
        `disable_zip_check` tinyint(4) DEFAULT 0,
        `zip_notes` text NOT NULL,
        `cn_parent_tr_iso_nr` int(11) DEFAULT 0,
        `cn_official_name_local` varchar(255) NOT NULL,
        `cn_short_local` varchar(255) NOT NULL,
        `cn_official_name_en` varchar(255) NOT NULL,
        `cn_short_fr` varchar(255) NOT NULL,
        `cn_short_en` varchar(255) NOT NULL,
        `cn_short_nl` varchar(255) NOT NULL,
        `cn_short_de` varchar(255) NOT NULL,
        `cn_capital` varchar(255) NOT NULL,
        `cn_tldomain` varchar(3) NOT NULL,
        `cn_currency_iso_3` varchar(3) NOT NULL,
        `cn_currency_iso_nr` int(11) DEFAULT 0,
        `cn_phone` int(11) DEFAULT NULL,
        `cn_eu_member` tinyint(4) DEFAULT 0,
        `cn_uno_member` tinyint(4) DEFAULT 0,
        `cn_address_format` tinyint(4) DEFAULT 0,
        `cn_zone_flag` tinyint(4) DEFAULT 0,
        `colissimo_area` varchar(4) NOT NULL,

    ";

    static $createTableKeysSQL="
        KEY `fk_mf_countries_isocode` (`cn_iso_2`),
        FULLTEXT KEY `fk_mf_country_fulltxt` (`cn_iso_2`,`cn_short_fr`)
    ";

    static $countriesList;

    function init(){

        global $mf,$l10n;

        parent::init();


        $this->data = array_merge($this->data, array(

            'cn_iso_2'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
                'validation' => array(
                    'mandatory' => '1',
                ),
            ),
            'cn_iso_3'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'cn_iso_nr'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'cn_short_fr'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
                'validation' => array(
                    'mandatory' => '1',
                ),
            ),
            'cn_short_en'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'cn_short_de'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),

            ),
            'cn_short_nl'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'fieldset_zip'  => array(
                'value' => '',
                'dataType' => 'fieldset',
                'valueType' => 'dummy',
                'closeAfterKey' => 'zip_notes',
            ),
            'zip_format_indication'  => array(
                'value' => '<div id="div_zip_format_hint" class="form-group "><div class="col-lg-3"></div><div class="col-lg-5"><strong>Indications de format de code postal:</strong><br />A = Lettre<br />N = Nombre<br />CC = Code pays ISO 3166-1 alpha-2</div></div>',
                'dataType' => 'html',
                'valueType' => 'dummy',
                'closeAfterKey' => 'zip_notes',
            ),
            'zip_format'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
            ),
            'zip_regexp'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-5'),
            ),
            'zip_nocodes'   =>  array(
                'value' => '',
                'dataType' => 'checkbox',
                 'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'zip_single'   =>  array(
                'value' => '',
                'dataType' => 'checkbox',
                'div_attributes' => array('class' => 'col-lg-4'),
                //'noClearFix' => 1
            ),
            'zip_single_comment'   =>  array(
                'value' => '<div class="form-group"><div class="col-lg-4 col-md-offset-3"><small>En cas de code postal unique, tapez le code postal attendu dans le champ "Expression régulière code postal".</small></div></div>',
                'dataType' => 'html',
            ),
            'disable_zip_check'   =>  array(
                'value' => '',
                'dataType' => 'checkbox',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'zip_notes'   =>  array(
                'value' => '',
                'dataType' => 'textarea',
                'div_attributes' => array('class' => 'col-lg-6'),
            ),
            'cn_parent_tr_iso_nr'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'cn_official_name_local'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'cn_short_local'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'cn_official_name_en'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),

            'cn_capital'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'cn_tldomain'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'cn_currency_iso_3'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'cn_currency_iso_nr'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'cn_phone'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-2'),
            ),
            'cn_eu_member'   =>  array(
                'value' => '',
                'dataType' => 'checkbox',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'cn_uno_member'   =>  array(
                'value' => '',
                'dataType' => 'checkbox',
                'div_attributes' => array('class' => 'col-lg-4'),
            ),
            'cn_address_format'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),
            'cn_zone_flag'   =>  array(
                'value' => '',
                'dataType' => 'input text',
                'div_attributes' => array('class' => 'col-lg-1'),
            ),

            'colissimo_area'  =>  array(
                'value' => '',
                'dataType' => 'select',
                'valueType' => 'text',
                'possibleValues' => array(
                    '0' => '',
                    'FR' => $l10n->getLabel('mfCountry','name_FR'),
                    'OM1' => $l10n->getLabel('mfCountry','name_OM1'),
                    'OM2' => $l10n->getLabel('mfCountry','name_OM2'),
                    'INTA' => $l10n->getLabel('mfCountry','name_INTA'),
                    'INTB' => $l10n->getLabel('mfCountry','name_INTB'),
                    'INTC' => $l10n->getLabel('mfCountry','name_INTC'),
                    'INTD' => $l10n->getLabel('mfCountry','name_INTD'),
                    'SEC' => $l10n->getLabel('mfCountry','name_SEC'),
                ),
                'validation' => array(
                    'mandatory' => '1',
                    'fail_values' => array('','0',0)
                ),
                'div_attributes' => array('class' => 'col-lg-2'),
            ),


    ));


        $this->showInEditForm = array(
            //'tab_main'=> array('cn_iso_2','cn_iso_3','cn_short_fr','cn_official_name_local','cn_currency_iso_3','cn_iso_nr','cn_eu_member','fieldset_zip','zip_format_indication','zip_format','zip_regexp','zip_nocodes','zip_single','zip_single_comment','disable_zip_check','zip_notes','colissimo_area')

             //ALL FIELS :
            'tab_main'=> array('cn_iso_2','cn_iso_3','cn_iso_nr','cn_short_fr','fieldset_zip','zip_format','zip_regexp','zip_nocodes','zip_single','disable_zip_check','zip_notes','cn_parent_tr_iso_nr',
            'cn_official_name_local','cn_short_local','cn_short_en','cn_short_de','cn_short_nl','cn_official_name_en','cn_capital','cn_tldomain','cn_currency_iso_3','cn_currency_iso_nr',
            'cn_phone','cn_eu_member','cn_uno_member','cn_address_format','cn_zone_flag')

        );



    }

    static $enableImportInitializationData = 1;

    static function importInitializationData(&$html=array()){

        dbRecord::importSQLDump(dirname(__FILE__).'/mf_countries.sql',1,$html);

    }

	/**
	 * Returns an array of countries using the cn_iso_2 column as index and 'cn_short_'.$mf->getLocale() as name
	 *
	 * @return array|int
	 */
    static function listCountries(){
        global $mf,$pdo;


        if(!is_array(self::$countriesList)) {

            $sql = "SELECT cn_iso_2, cn_short_" . $mf->getLocale() . " FROM " . self::$tableName . " WHERE deleted=0 ORDER BY cn_short_" . $mf->getLocale();
            $stmt = $pdo->prepare($sql);

            try {
                $stmt->execute(array());
            } catch (PDOException $e) {
                echo $e->getMessage();
                return 0;
            }

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $items = array(0 => '');

            foreach ($rows as $row) {
                $items[$row['cn_iso_2']] = $row['cn_iso_2'].' - '.$row['cn_short_' . $mf->getLocale()];
            }
            self::$countriesList = $items;
            return $items;
        }
        else {
            return self::$countriesList;
        }
    }

	/**
	 * Returns an array of countries using the value of $fieldKey as index and the value of $fieldValue as name
	 * @param $fieldKey column name to extract for creating the indexes of the array
	 * @param $fieldValue field name to extract for the names of the array
	 *
	 * @return array|int
	 */
	static function listCountriesCustom($fieldKey = 'cn_iso_2', $fieldValue = null){
		global $mf,$pdo;

		if($fieldValue == null) $fieldValue = "cn_short_" . $mf->getLocale();

		$sql = "SELECT ".$fieldKey.", ".$fieldValue." FROM " . self::$tableName . " WHERE deleted=0 ORDER BY cn_short_" . $mf->getLocale();
		$stmt = $pdo->prepare($sql);

		try {
			$stmt->execute(array());
		} catch (PDOException $e) {
			echo $e->getMessage();
			return 0;
		}

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$items = array(0 => '');

		foreach ($rows as $row) {
			$items[$row[$fieldKey]] = $row[$fieldValue];
		}
		self::$countriesList = $items;
		return $items;

	}


    function loadByISO2($ISO2){
        global $pdo;

        $sql = "SELECT uid FROM ".static::getTableName()." WHERE deleted=0 AND cn_iso_2=".$pdo->quote($ISO2);
        $stmt = $pdo->query($sql);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if($stmt->rowCount()>0){

            return $this->load($row['uid']);
        }
        else return 0;
    }

    /**
     * returns true if ISO2 country code referes to a European country
     */
    static function isInEuByISO2($ISO2){
        global $pdo;

        $sql = "SELECT cn_eu_member em FROM ".static::getTableName()." WHERE deleted=0 AND LOWER(cn_iso_2)=".$pdo->quote(strtolower($ISO2)) . " LIMIT 1";
        $stmt = $pdo->query($sql);
        $row = $stmt->fetch(PDO::FETCH_NUM);

        if ( $row[0] == "1")
            return true;
        else
          return false;
    }


    static function checkZipCode($zipCode, $coutryISO2Code){

        if(strtoupper($coutryISO2Code) == 'FR'){
            //process French zip codes directly from the cities database
            return mfFrance::checkZipCode($zipCode);
        }
        else{
            //international zip codes
            global $pdo;

            $sql = "SELECT cn_iso_2, zip_regexp, zip_nocodes, zip_single, disable_zip_check FROM ".self::$tableName." WHERE cn_iso_2 = '".$coutryISO2Code."' AND deleted=0";
            $stmt = $pdo->prepare($sql);

            try{
                $stmt->execute(array());
            }
            catch(PDOException $e){
                echo $e->getMessage();
                return 0;
            }

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            if($stmt->rowCount() > 0){
                if($row['disable_zip_check']==1)return true;
                if($row['zip_nocodes']==1 && trim($zipCode)=='')return true;
                if($row['zip_single']==1){
                    if(trim($zipCode) == $row['zip_regexp'])return true;
                    else return false;
                }

                //check the zip code against the regular expression in the database
                if (!preg_match("/".$row['zip_regexp']."/i",trim($zipCode))){
                    //Validation failed, provided zip/postal code is not valid.
                    return false;
                } else {
                    //Validation passed, provided zip/postal code is valid.
                    return true;
                }
            }
            else return false;

        }
    }

    static function getCountryName($isoCode2){
        global $pdo;

        $sql = "SELECT cn_short_fr FROM ".self::$tableName." WHERE cn_iso_2 = ".$pdo->quote($isoCode2)." AND deleted=0";

        $stmt = $pdo->query($sql);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($stmt->rowCount() > 0){
            return $row['cn_short_fr'];
        }
        else return '';
    }

}


